{$ALIGN OFF}
unit UsbPort;

interface

uses
  Windows, Classes, ComCtrls, ExtCtrls, Sysutils, DllDescriptor;

type
 TPortList = array of string;


 TCOM = class
    ComHandle: DWord;
    PortName : String;
    isError  : Boolean;
    function OpenUSB: Boolean;
    function OpenComm(PortName_: string; InQueue, OutQueue, Baud : LongInt): Boolean;
    function SetCommTiming: Boolean;
    function SetCommBuffer(InQueue, OutQueue: LongInt): Boolean;
    function SetCommStatus(Baud: Integer): Boolean;
    function PutByte(Data: Byte): Boolean;
    function GetByte(var Data: Byte): Boolean;
    function PutLongword(Data: Longword): Boolean;
    function GetLongword(var Data: Longword): Boolean;
    procedure CloseComm;
    procedure GetComPortList(var List: TPortList);
    destructor Destroy;
    procedure AddLogData(Dt: Byte);
 end;

var
akp_on: Boolean = FALSE;

implementation


procedure TCOM.AddLogData(Dt: Byte);
begin
  {$IFDEF COMLOG}
  if ComTTT_Pos < ComTTT_Count then
  begin
    ComTTT[ComTTT_Pos].Count:= Dt;
    ComTTT[ComTTT_Pos].Time:= GetTickCount;
    inc( ComTTT_Pos );
  end;
  {$ENDIF}
end;


function TCOM.OpenComm(PortName_: string; InQueue, OutQueue, Baud : LongInt): Boolean;
begin
  PortName:=PortName_;
  Result:= OpenUSB;
end;

function TCOM.OpenUSB: Boolean;
var
    iv: dword;
    tmr: byte;
label m1;
begin
     tmr:= 20;
m1: iv:= initlib;
    if ( ((iv and $01)=0) and (tmr <> 0) ) then
    begin
       Sleep(500);
       tmr:= tmr-1;
       goto m1;
    end;

  if akp_on then
  begin
    if ((iv and $01)=0) then akp_on:= FALSE;
  end
    else if ((iv and $01)=1) then
         begin
            akp_on:= TRUE;
            Sleep(2000);
            pwrswitch($0FF);
         end;

  Result:= akp_on;
  
end;
//-------------------------------------------------------------------
function TCOM.SetCommTiming: Boolean;
var
  Timeouts: TCommTimeOuts;
begin
  with TimeOuts do
  begin
    ReadIntervalTimeout := 1;
    ReadTotalTimeoutMultiplier := 0;
    ReadTotalTimeoutConstant := 1;
    WriteTotalTimeoutMultiplier := 2;
    WriteTotalTimeoutConstant := 2;
  end;
  Result := SetCommTimeouts(ComHandle, Timeouts);
end;


function TCOM.SetCommBuffer(InQueue, OutQueue: LongInt): Boolean;
begin
  Result:= SetupComm(ComHandle, InQueue, OutQueue);
end;

function TCOM.SetCommStatus(Baud: Integer): Boolean;
var
  DCB: TDCB;
begin
  with DCB do
  begin
    DCBlength:=SizeOf(Tdcb);
    BaudRate := Baud;
    Flags:=12305;
    wReserved:=0;
    XonLim:=600;
    XoffLim:=150;
    ByteSize:=8;
    Parity:=0;
    StopBits:=0;
    XonChar:=#17;
    XoffChar:=#19;
    ErrorChar:=#0;
    EofChar:=#0;
    EvtChar:=#0;
    wReserved1:=65;
  end;
//  DCB.Flags:=(DCB.Flags {and not $30}) or (DTR_CONTROL_HandSHAKE shl 4) or (RTS_CONTROL_HandSHAKE shl 4); // <----------------------------------------------------------------------------------
  Result := SetCommState(ComHandle, DCB);
end;
//-------------------------------------------------------------------
function TCOM.PutLongword(Data: Longword): Boolean;
var
  Temp: Longword;

begin
  Temp:= WriteToUSB(@Data,4);
  if Temp = 4 then Result:= TRUE
     else Result:= FALSE;
end;
//--------------------------------------------------------------------
function TCOM.PutByte(Data: Byte): Boolean;
var
  Temp: Longword;
begin
  Temp:= WriteToUSB(@Data,1);
  if Temp <> 0 then Result:= TRUE
     else Result:= FALSE;
end;
//--------------------------------------------------------------------
function TCOM.GetByte(var Data: Byte): Boolean;
var
  Temp: LongWord;

begin
  Temp:= ReadFromUSB(@Data, 1,0);
  if Temp = 1  then Result:= TRUE
    else Result:=FALSE
end;
//--------------------------------------------------------------------
function TCOM.GetLongword(var Data: Longword): Boolean;
var
  Temp: LongWord;

begin
  Temp:= ReadFromUSB(@Data, 4,0);
  if Temp = 4  then Result:= TRUE
    else Result:=FALSE
end;
//--------------------------------------------------------------------
procedure TCOM.CloseComm;
begin
  pwrswitch(0);
//  Sleep(2000);
  finishlib;
  akp_on:= FALSE;
end;
//--------------------------------------------------------------------
procedure TCOM.GetComPortList(var List: TPortList);
var
  DCB: TDCB;
  I: integer;
  Handle: THandle;
  PortName: string;

begin
  SetLength(List, 0);
  for I:= 0 to 31 do
  begin
    PortName:= Format('COM%d', [I + 1]);
    Handle:= CreateFile(PChar(PortName),
                        GENERIC_READ or GENERIC_WRITE,
                        0, nil, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, 0);
    if Handle <> INVALID_HANDLE_VALUE then
    begin
      SetLength(List, Length(List) + 1);
      List[High(List)]:= PortName;
    end;
    CloseHandle(Handle);
  end;
end;

destructor TCOM.Destroy;
begin
 if akp_on = TRUE then 
 begin
  pwrswitch(0);
  Sleep(2000);
  finishlib;
  akp_on:= FALSE;
 end
end;

end.
