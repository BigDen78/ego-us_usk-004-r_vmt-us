unit AviconDataSource;

interface

uses
  Types, Classes, Controls, Windows, Messages, SysUtils, Math, AviconTypes, DataFileConfig, SyncObjs, Dialogs;

type

  TAviconDataSourceErrors = (eIncorrectFileVersion, eIncorrectFile, eFileNotFound, eUnknownError);

  TAviconDataSourceError = set of TAviconDataSourceErrors;

  TMiasParams = record
    Par: TEvalChannelsParams;
    Sensor1: array [TRail] of Boolean;
    Mode: Integer;

    Channel: Boolean; // ���� ������� � ��� ��� ���� ������� ���� � ����� ������
    Rail: TRail;      // ����
    EvalChNum: Byte;  // ����� ������ ������
    Time: Word;       // ����� ���������� � ���������� ������ (���)

  end;

  TKMKontentItem = record
    Pk: Integer;
    Len: Integer;
    Idx: Integer;
  end;

  TCoordListItem = record
    KM: Integer;
    Content: array of TKMKontentItem;
    Len_: Integer;
    Idx_: Integer;
  end;

  TCoordList = array of TCoordListItem;

  // ------------------------< ������ ������� �������� >---------------------------------------

  THSEcho = packed record
    Delay: array [1 .. 8] of Byte;
    Ampl: array [1 .. 8] of Byte;
    Count: Integer;
    Idx: Integer;
  end;

  THSListItem = record
    // HSCh: Integer;
    // HSMode: Integer;
    EventIdx: Integer;
    DisCoord: Integer;
    Header: THSHead;
    Samples: array of THSEcho;
  end;

  THSList = array of THSListItem;

  TimeListItem = record
    H: Integer;
    M: Integer;
    DC: Integer;
  end;

  TBMItem = packed record
    StSkipOff: Integer;
    OkOff: Integer;
    // Pack: TPackData;
    // StDisCrd: Integer;
    // EnDisCrd: Integer;
  end;

  THCListItem = record // 182 �����
    Rail: TRail;             // ����
    Ch: Integer;
    StartDisCoord: Integer;     // ���������� ���������� �������
    EndDisCoord: Integer;       // ���������� ���������� �������
    Valut: Integer;             // ���������� ���������
  end;

  TMediaItem = record

    DataType: Integer;
    Data: TMemoryStream;
    EventIdx: Integer;

  end;

    TVData = record
      Km : Integer;
      Pk : Integer;
      M  : Integer;
      mm : Integer;
      Value: Extended;
    end;

  TAviconDataSource = class
  private
    FConfig: TDataFileConfig;
    FHead: TFileHeader;
    FFileName: string; // ��� �����
    FBody: TMemoryStream;
    FExHeader: TExHeader;
    FExHdrLoadOk: Boolean;
    FEvents: array of TFileEvent;
    FParams: array of TMiasParams;
    FSaveEventsData: array of TFileEvent;
    FNotebook: array of TFileNotebook;
    FTimeList: array of TimeListItem;
    FCoordList: TCoordList;
    FModifyData: Boolean;
    FUnknownFileVersion: Boolean;
    FDataSize: Integer;
    FLastErrorIdx: Integer;
    FLastBMIdx: Integer;
    FMaxSysCoord: Integer;
    FCurSysCoord: Longint;
    FCurDisCoord: Int64;
    FSaveSysCoord: Longint;
    FSaveDisCoord: Int64;
    FCurParams: TMiasParams;
    FCurOffset: Integer;
    FUnPackFlag: array [TRail] of Boolean;
    FGetBMStateIdx: Integer;
    FGetBMStateState: Boolean;
    FGetParamIdx: Integer;
    FRSPSkipSideRail: Boolean;
    FRSPFile: Boolean;
    FSkipBM: Boolean;
    FCanSkipBM: Boolean;
    FBMItems: array of TBMItem;
    // ����� ��� ������ � ���-���������
    FSrcDat: array [0 .. 7] of Byte;
    FDestDat: array [rLeft .. rRight] of TDestDat;
    FFullHeader: Boolean; // ���� � ����������� ����������
    FExtendedChannels: Boolean; // ����������� ������ � �������� > 16

    FSaveFileDate: TDateTime; // ���� �������� ����� - �� ������ �������� �����
    FSaveFileSize: Integer; // ������ ����� - �� ������ �������� �����
    FReadOnlyFlag: Boolean; // ���� ������ ��� ������
    FObmen1: Integer;
    FParDat: TMemoryStream;
    FLoadFromStream: Boolean;
    FCoordSys: TCoordSys;

    FFillEventsData: Boolean;

  protected

    function GetCoordSys: TCoordSys;
    function GetStartMRFCrd: TMRFCrd;
    function GetEndMRFCrd: TMRFCrd;
    function GetStartCaCrd: TCaCrd;
    function GetEndCaCrd: TCaCrd;
    procedure LoadExData;
    procedure SaveExData(CutOldExHeader: Boolean);
    procedure AnalyzeFileBody;
    procedure FillEventsData;
    procedure FillTimeList;

    function FindBSState(StartDisCoord: Integer): Boolean;
    procedure TestBMOffsetFirst;
    function TestBMOffsetNext: Boolean;
    procedure SetSkipBM(NewState: Boolean);
    function GetEventCount: Integer;
    function GetEvent(Index: Integer): TFileEvent;
    function GetParam(Index: Integer): TMiasParams;
    function GetNotebookCount: Integer;
    function GetNotebookP(Index: Integer): pFileNotebook;
    function GetMaxDisCoord: Integer;
    function NormalizeDisCoord(Src: Integer): Integer;
    function SkipEvent(ID: Byte): Boolean; // ������� �������
    function LoadEventData(ID: Byte; Ptr: PEventData): Boolean; // ������� ������ �������
    function GetAirBrushCount: Integer;
    function GetHCCount: Integer;
    function GetLongLabelCount: Integer;
    function GetHCItem(Index: Integer): THCListItem;
    function GetLLItem(Index: Integer): edLongLabel;
    function GetAirBrushItem(Index: Integer): edAirBrush2;
    function GetStartRealCoord: TRealCoord;
    function GetEndRealCoord: TRealCoord;

  public
    FCurEcho: TCurEcho;
    VData: array [0..3] of array of TVData;
    StartFileSensor1: array [TRail] of Boolean;
    Sensor1DataExists: array [TRail] of Boolean;


    Error: TAviconDataSourceError;

    UnpackBottom: Boolean;
    FCDList: array of Integer;
    FAirBrushList: array of edAirBrush2;

    FHCList: array of THCListItem;
    FLongLabelList: array of edLongLabel;
    FMediaList: array of TMediaItem;

    HSList: THSList;
    BackMotion: Boolean;
    CurAK: array [TRail, 1 .. 8] of Boolean;
    StartShift: Integer;
    LabelList: TIntegerDynArray;
    SkipTestDateTime: Boolean;
    CurCrdLongLinc: Boolean;

    GPSPoints: array of TGPSPoint;
    Avicon15Flag: Boolean;

    FCurLoadSysCoord_UMUA_Left: Integer;
    FCurLoadSysCoord_UMUB_Left: Integer;
    FCurLoadSysCoord_UMUA_Right: Integer;
    FCurLoadSysCoord_UMUB_Right: Integer;

    FCurLoadDelta_UMUA_Left: ShortInt;
    FCurLoadDelta_UMUB_Left: ShortInt;
    FCurLoadDelta_UMUA_Right: ShortInt;
    FCurLoadDelta_UMUB_Right: ShortInt;


    constructor Create;
    destructor Destroy; override;
    function LoadFromFile(FileName: string; VengrData: boolean = False): Boolean;
    function LoadFromStream(Stream: TStream): Boolean;
    function LoadHeader(FileName: string): Boolean;
    procedure CloseFile;
    procedure ReAnalyze;
    function TestCheckSumm: Boolean;

    procedure LoadData(StartDisCoord, EndDisCoord, LoadShift: Longint; BlockOk: TDataNotifyEvent);
    function SysToDisCoord(Coord: Integer): Integer;
    function DisToSysCoord(Coord: Integer): Integer;
    procedure DisToDisCoords(Coord: Integer; var Res: TIntegerDynArray);
    procedure DisToDisCoords_(Coord: Integer; var Res: TIntegerDynArray; var StIdx, Count: Integer);
    function TestChangeDirCrd(Coord: Integer): Boolean;
    procedure DisToFileOffset(NeedDisCoord: Integer; var DisCoord: Int64; var SysCoord: Integer; var OffSet: Integer);
    function GetBMStateFirst(DisCoord: Integer): Boolean;
    function GetBMStateNext(var DisCoord: Integer; var State: Boolean): Boolean;
    function GetEventsData(Index: Integer): TEventData;
    function GetParamFirst(DisCoord: Integer; var Params: TMiasParams): Boolean;
    function GetParamNext(DisCoord: Integer; var Params: TMiasParams): Boolean;

    function RailToPathRail(R: TRail): TRail;


  // -------------< ������� ��������� - ����������� �� >-----------------------------------

    function DisToMRFCrdPrm(DisCoord: Integer; var CoordParams: TMRFCrdParams): Boolean;
    function DisToMRFCrd(DisCoord: Longint): TMRFCrd;
    function MRFCrdToDisCrd(RFC: TMRFCrd; var DisCoord: Longint): Boolean;

  // -------------< ������� ��������� - ��������� >----------------------------------------

    function DisToCaCrdPrm(DisCoord: Integer; var CoordParams: TCaCrdParams): Boolean;
    function DisToCaCrd(DisCoord: Longint): TCaCrd;
    function CaCrdToDis(CaCrd: TCaCrd; var DisCoord: Longint): Boolean;

  // --------------------------------------------------------------------------------------

    function DisToCoordParams(DisCoord: Integer; var CrdParams: TCrdParams): Boolean;
    function DisToRealCoord(DisCoord: Longint): TRealCoord;

    function RealToDisCoord(Crd: TRealCoord): Longint;

    function GetTime(DisCoord: Integer): string;
    function GetGPSCoord(DisCoord: Integer): TGPSPoint;
    function GetSpeed(DisCoord: Integer; var Speed: Single): string;
    function NotebookAdd(NewDat: pFileNotebook = nil): Integer;
    procedure NotebookDelete(Index: Integer);
    procedure CutExHeader;
    procedure SetScanStep(New: Word);
    function GetEventIdx(StartDisCoord, EndDisCoord: Integer; var StartIdx, EndIdx: Integer; EmptyPar: Integer): Boolean;
    procedure GetNearestEventIdx(DisCoord: Integer; var LeftIdx, RightIdx: Integer; var SameCoord: TIntegerDynArray);
    function GetLeftEventIdx(DisCoord: Integer): Integer;
    function GetRightEventIdx(DisCoord: Integer): Integer;
    procedure GetEventData(Idx: Integer; var ID: Byte; var pData: PEventData);
    procedure SaveNB;
    procedure SavePiece(StDisCood, EdDisCood: Integer; SaveNB: Boolean; FileName: string);

    function GetTimeList(Index: Integer): TimeListItem;
    function GetTimeListCount: Integer;
    function isSingleRailDevice: Boolean;
    function isEGOUSW: Boolean;
    function isEGOUS: Boolean;
    function isEGOUSW_BHead: Boolean;
    function isUSK: Boolean;

    property EventsData[Index: Integer]: TEventData read GetEventsData;
    property TimeList[Index: Integer]: TimeListItem read GetTimeList;
    property TimeListCount: Integer read GetTimeListCount;

    property FileName: string read FFileName;
    property FileBody: TMemoryStream read FBody;
    property Header: TFileHeader read FHead write FHead;

    property StartMRFCrd: TMRFCrd read GetStartMRFCrd;
    property EndMRFCrd: TMRFCrd read GetEndMRFCrd;
    property StartCaCrd: TCaCrd read GetStartCaCrd;
    property EndCaCrd: TCaCrd read GetEndCaCrd;

    property StartRealCoord: TRealCoord read GetStartRealCoord;
    property EndRealCoord: TRealCoord read GetEndRealCoord;

    property ExHeader: TExHeader read FExHeader;
    property ModifyExData: Boolean read FModifyData write FModifyData;
    property MaxDisCoord: Integer read GetMaxDisCoord;
    property EventCount: Integer read GetEventCount;
    property Event[Index: Integer]: TFileEvent read GetEvent;
    property Params[Index: Integer]: TMiasParams read GetParam;
    property NotebookCount: Integer read GetNotebookCount;
    property Notebook[Index: Integer]: pFileNotebook read GetNotebookP;
    property AirBrushCount: Integer read GetAirBrushCount;
    property AirBrush[Index: Integer]: edAirBrush2 read GetAirBrushItem;
    property HCCount: Integer read GetHCCount;
    property HC[Index: Integer]: THCListItem read GetHCItem;
    property LLCount: Integer read GetLongLabelCount;
    property LL[Index: Integer]: edLongLabel read GetLLItem;
    property SkipBackMotion: Boolean read FSkipBM write SetSkipBM;
    property CurOffset: Integer read FCurOffset;
    property CurSysCoord: Longint read FCurSysCoord;
    property CurDisCoord: Int64 read FCurDisCoord write FCurDisCoord;
    property CurParams: TMiasParams read FCurParams;
    property CurEcho: TCurEcho read FCurEcho write FCurEcho;
    property FullHeader: Boolean read FFullHeader;
    property CoordList: TCoordList read FCoordList;
    property CoordSys: TCoordSys read GetCoordSys;
    property Config: TDataFileConfig read FConfig;

    // FHeader: TAvk11Header2;
    // FBSAmplLink: array of TBSAmplLinkItem;
    // LoadProg: TGauge;
    // DelayKoeff: Integer;
    // CurAmpl: TCurAmpl;
    // function TestOffsetFirst: Boolean; // <--- �������� ���������� �� protected
    // function TestOffsetNext: Boolean; // <--- �������� ���������� �� protected
    // function SideToRail(Side: TSide): TRail;
    // procedure AddRSPInfo(ID: Integer; Text: string);
    // function  GetRSPTextByID(ID: Integer; var Res: Boolean): string;
    procedure GenerateGPSPoints;
    procedure MakeNoteBookFromNordco;
    // procedure TestCanSkipBM;
    // property RSPSkipSideRail: Boolean read FRSPSkipSideRail write FRSPSkipSideRail;
    // property DataRail: RRail read FExHeader.DataRail;
    // property ErrorCount: Integer read GetErrorCount;
    // property Error[Index: Integer]: TFileError read GetError;
    // property FileDecodingCount: Integer read GetFileDecodingCount;
    // property FileDecoding[Index: Integer]: TFileDecoding read GetFileDecoding;
    // property RSPInfoCount: Integer read GetRSPInfoCount;
    // property RSPInfoByIdx[Index: Integer]: TRSPInfo read GetRSPInfoByIndex;
    // property RSPInfoById[Index: Integer]: TRSPInfo read GetRSPInfoByID;
    // property Log: TStringList read FLog;
    // property UnknownFileVersion: Boolean read FUnknownFileVersion;
    // property RSPFile: Boolean read FRSPFile;
    // property UnPackFlag[Index: RRail]: Boolean read GetFUnPackFlag;
    // property BodyModify: Boolean read GetBodyModify write SetBodyModify;
    // property Pack: TPackData read FPack;
  end;

  TAvk11DatSrc = TAviconDataSource;

//const
//  EchoCnt: array [0 .. 11] of Integer = (0, -1, 1, 2, -1, 3, 4, -1, 5, 6, -1, 7);
//  EchoLen: array [0 .. 7] of Integer = (2, 3, 5, 6, 8, 9, 11, 0);

var
  ConfigList: TDataFileConfigList;
  function GetString(Len: Byte; Text: array of Char): string;


implementation

var
  GlobalCount: Integer = 0;

// ------------------------------------------------------------------------------

function GetString(Len: Byte; Text: array of Char): string;
var
  I: Integer;

begin
  Result:= '';
  for I := 0 to Len - 1 do
    if (Text[I] <> Chr($A)) and (Text[I] <> '_') then Result:= Result + Text[I];
end;

// ------------------------< TAviconDataSource >---------------------------------------

constructor TAviconDataSource.Create;
begin
  // FAnalyzeNewCrd:= nil;
  // UnpackBottom:= True;
  // SkipTestDateTime:= False;
  Avicon15Flag:= False;
  FFillEventsData:= False;
  FSkipBM:= False;
  StartShift:= 0;
  FConfig:= nil;
  // FLog:= TStringList.Create;
  // FRSPSkipSideRail:= False;
  // FRSPFile:= False;
  FParDat:= TMemoryStream.Create;
  Sensor1DataExists[rLeft]:= False;
  Sensor1DataExists[rRight]:= False;

  SetLength(FLongLabelList, 0);
  SetLength(FHCList, 0);
end;

destructor TAviconDataSource.Destroy;
var
  I: Integer;

begin
  if FSkipBM then
    SetSkipBM(False);
  if FModifyData then
    SaveExData(FExHdrLoadOk);

  if Assigned(FBody) then
  begin
    FBody.Free;
    FBody:= nil;
  end;

  // SetLength(FErrors, 0);
  SetLength(FEvents, 0);
  SetLength(FNotebook, 0);
  SetLength(FParams, 0);
  // SetLength(FDecoding, 0);
  // SetLength(FEventsData, 0);
  // SetLength(FRSPInfo, 0);
  SetLength(FCDList, 0);
  SetLength(FTimeList, 0);
  SetLength(GPSPoints, 0);
  SetLength(FSaveEventsData, 0);
  SetLength(FBMItems, 0);
  for I:= 0 to High(FCoordList) do SetLength(FCoordList[I].Content, 0);
  SetLength(FCoordList, 0);
  FParDat.Free;
  SetLength(FLongLabelList, 0);
  SetLength(FHCList, 0);
end;

// ------------------------< ������������ �������� ������ >---------------------

function TAviconDataSource.GetEventCount: Integer;
begin
  Result:= Length(FEvents);
end;

function TAviconDataSource.GetEvent(Index: Integer): TFileEvent;
begin
  if (Index >= 0) and (Index < Length(FEvents)) then
    Result:= FEvents[Index];
end;

function TAviconDataSource.GetParam(Index: Integer): TMiasParams;
begin
  if (Index >= 0) and (Index < Length(FEvents)) then
    Result:= FParams[Index];
end;

function TAviconDataSource.GetAirBrushCount: Integer;
begin
  Result:= Length(FAirBrushList);
end;

function TAviconDataSource.GetHCCount: Integer;
begin
  Result:= Length(FHCList);
end;

function TAviconDataSource.GetHCItem(Index: Integer): THCListItem;
begin
  if (Index >= 0) and (Index < Length(FHCList)) then Result:= FHCList[Index];
end;

function TAviconDataSource.GetLongLabelCount: Integer;
begin
  Result:= Length(FLongLabelList);
end;

function TAviconDataSource.GetLLItem(Index: Integer): edLongLabel;
begin
  if (Index >= 0) and (Index < Length(FLongLabelList)) then Result:= FLongLabelList[Index];
end;

function TAviconDataSource.GetAirBrushItem(Index: Integer): edAirBrush2;
begin
  if (Index >= 0) and (Index < Length(FAirBrushList)) then Result:= FAirBrushList[Index];
end;

function TAviconDataSource.GetNotebookCount: Integer;
begin
  Result:= Length(FNotebook);
end;

function TAviconDataSource.GetNotebookP(Index: Integer): pFileNotebook;
begin
  Result:= @FNotebook[Index];
end;


function TAviconDataSource.GetStartRealCoord: TRealCoord;
begin
  Result.Sys:= CoordSys;
  Result.MRFCrd:= GetStartMRFCrd;
  Result.CaCrd:= GetStartCaCrd;
end;

function TAviconDataSource.GetEndRealCoord: TRealCoord;
begin
  Result.Sys:= CoordSys;
  Result.MRFCrd:= GetEndMRFCrd;
  Result.CaCrd:= GetEndCaCrd;
end;

{
  function TAviconDataSource.GetFileDecodingCount: Integer;
  begin
  Result:= Length(FDecoding);
  end;

  function TAviconDataSource.GetFileDecoding(Index: Integer): TFileDecoding;
  begin
  Result:= FDecoding[Index];
  end;

  function TAviconDataSource.GetRSPInfoCount: Integer;
  begin
  Result:= Length(FRSPInfo);
  end;

  function TAviconDataSource.GetRSPInfoByIndex(Index: Integer): TRSPInfo;
  begin
  Result:= FRSPInfo[Index];
  end;

  function TAviconDataSource.GetRSPInfoByID(ID: Integer): TRSPInfo;
  var
  I: Integer;

  begin
  for I:= 0 to High(FRSPInfo) do
  if FRSPInfo[I].ID = ID then
  begin
  Result:= FRSPInfo[I];
  Exit;
  end;
  end;

  function TAviconDataSource.GetRSPTextByID(ID: Integer; var Res: Boolean): string;
  var
  I: Integer;

  begin
  Res:= False;
  for I:= 0 to High(FRSPInfo) do
  if FRSPInfo[I].ID = ID then
  begin
  Result:= FRSPInfo[I].Text;
  Res:= True;
  Exit;
  end;
  end;

  procedure TAviconDataSource.AddRSPInfo(ID: Integer; Text: string);
  begin
  FModifyData:= True;
  SetLength(FRSPInfo, Length(FRSPInfo) + 1);
  FRSPInfo[High(FRSPInfo)].ID:= ID;
  FRSPInfo[High(FRSPInfo)].Text:= Text;
  end;
}
function TAviconDataSource.NotebookAdd(NewDat: pFileNotebook = nil): Integer;
var
  I: Integer;

begin
  FModifyData:= True;
  I:= Length(FNotebook);
  SetLength(FNotebook, I + 1);
  if Assigned(NewDat) then FNotebook[I]:= NewDat^;
  Result:= I;
end;

procedure TAviconDataSource.NotebookDelete(Index: Integer);
var
  I: Integer;

begin
  FModifyData:= True;
  for I:= Index to High(FNotebook) - 1 do FNotebook[I]:= FNotebook[I + 1];
  SetLength(FNotebook, Length(FNotebook) - 1);
end;

function TAviconDataSource.GetMaxDisCoord: Integer;
begin
  if FSkipBM then Result:= FMaxSysCoord
             else Result:= FExHeader.EndDisCoord;
end;
{
  function TAviconDataSource.GetFUnPackFlag(Index: RRail): Boolean;
  begin
  //  Result:= FUnPackFlag[Index];
  end;
}

function TAviconDataSource.GetCoordSys: TCoordSys;
begin
  Result:= TCoordSys(FHead.PathCoordSystem);
end;

function TAviconDataSource.GetStartMRFCrd: TMRFCrd;
begin
  Result.Km:= FHead.StartKM;
  Result.Pk:= FHead.StartPk;
  Result.mm:= FHead.StartMetre * 1000;
end;

function TAviconDataSource.GetEndMRFCrd: TMRFCrd;
begin
  Result:= FExHeader.EndMRFCrd;
end;

function TAviconDataSource.GetStartCaCrd: TCaCrd;
begin
  Result:= FHead.StartChainage;
end;

function TAviconDataSource.GetEndCaCrd: TCaCrd;
begin
  Result:= FExHeader.EndCaCrd;
end;

// ------------------------< ������ � ����������� ���������� >---------------------

procedure TAviconDataSource.LoadExData;
var
  I, J: Integer;
  ID: Byte;
  pData: PEventData;
  tmp: string;
  ExHeadOk: Boolean;
  NeedReAnalyze: Boolean;
  Head: TExHeaderHead;
  DataSize: Integer;
  OldNotebook5: array of TFileNotebook_ver_5;
  OldNotebook6: array of TFileNotebook_ver_6;

begin
  NeedReAnalyze:= False;
  ExHeadOk:= (FHead.TableLink <> 0) and (FHead.TableLink <= FBody.Size);
  FUnknownFileVersion:= False;

  if ExHeadOk then
    try
      FBody.Position:= FHead.TableLink;
      FBody.ReadBuffer(Head, SizeOf(Head));
    except
      // FLog.Add(Language.GetCaption(0150011));
      ExHeadOk:= False;
    end;

  if ExHeadOk then
    for I:= 0 to 13 do
      if Head.FFConst[I] <> $FF
       then
      begin
        // FLog.Add(Language.GetCaption(0150011));
        ExHeadOk:= False;
        Break;
      end;

  if ExHeadOk then
  begin
    FDataSize:= FHead.TableLink;

    case Head.DataVer of
      (*
        3: begin // �� ������ �����������, � ���� �����������������
        end;

        4: begin // �� ������ ������������, � ���� �����������������
        end;

        5: begin
        try
        FBody.Position:= FHead.TableLink;
        FBody.ReadBuffer(FExHeader, SizeOf(FExHeader));
        except
        //             FLog.Add(Language.GetCaption(0150011));
        ExHeadOk:= False;
        end;
        NeedReAnalyze:= True;
        end;
        *)
      7:
        begin
          try
            FBody.Position:= FHead.TableLink;
            FBody.ReadBuffer(FExHeader, SizeOf(FExHeader));
          except
            // FLog.Add(Language.GetCaption(0150011));
            ExHeadOk:= False;
          end;
        end;
      (*
        7..255: begin
        FUnknownFileVersion:= True;
        Exit;

        {  try
        FBody.Position:= FHead.TableLink + 14;
        FBody.ReadBuffer(DataSize, SizeOf(Integer));
        FBody.Position:= FHead.TableLink;
        FBody.ReadBuffer(FExHeader, Min(SizeOf(FExHeader), DataSize));
        except
        FLog.Add(Language.GetCaption(0150011));
        ExHeadOk:= False;
        end; }

        end; *)
    end;
    {
      if FExHeader.ErrorCount <> 0 then
      if FExHeader.ErrorDataVer = 3 then
      begin
      SetLength(FErrors, FExHeader.ErrorCount);
      try
      FBody.Position:= FExHeader.ErrorOffset;
      FBody.ReadBuffer(FErrors[0], FExHeader.ErrorCount * FExHeader.ErrorItemSize);
      except
      //          FLog.Add(Language.GetCaption(0150012));
      NeedReAnalyze:= True;
      SetLength(FErrors, 0);
      end;
      end
      else
      begin
      FUnknownFileVersion:= True;
      Exit;
      end;
      }
    if FExHeader.EventCount <> 0 then
      if FExHeader.EventDataVer = 5 then
      begin
        SetLength(FEvents, FExHeader.EventCount);
        try
          FBody.Position:= FExHeader.EventOffset;
          for I := 0 to High(FEvents) do
            FBody.ReadBuffer(FEvents[I], {FExHeader.EventCount *} FExHeader.EventItemSize);
        except
          // FLog.Add(Language.GetCaption(0150013));
          NeedReAnalyze:= True;
          SetLength(FEvents, 0);
        end;
      end
      else
      begin
        FUnknownFileVersion:= True;
        Exit;
      end;

    if FExHeader.NotebookCount <> 0 then
    begin
      case FExHeader.NotebookDataVer of
        3:
          begin
          end;

        4:
          begin
          end;

        5:
          begin
            SetLength(OldNotebook5, FExHeader.NotebookCount);
            try
              FBody.Position:= FExHeader.NotebookOffset;
              FBody.ReadBuffer(OldNotebook5[0], FExHeader.NotebookCount * FExHeader.NotebookItemSize);
            except
              SetLength(OldNotebook5, 0);
              FModifyData:= True;
            end;

            SetLength(FNotebook, FExHeader.NotebookCount);
            for I := 0 to FExHeader.NotebookCount - 1 do
            begin
              FNotebook[I].ID:= OldNotebook5[I].ID;
              FNotebook[I].Rail:= OldNotebook5[I].Rail;
              FNotebook[I].DisCoord:= OldNotebook5[I].DisCoord;
              FNotebook[I].Defect:= StringToHeaderStr(OldNotebook5[I].Defect);
              FNotebook[I].BlokNotText:= StringToHeaderBigStr(OldNotebook5[I].BlokNotText);
              FNotebook[I].DT:= OldNotebook5[I].DT;
              FNotebook[I].Zoom:= OldNotebook5[I].Zoom;
              FNotebook[I].Reduction:= OldNotebook5[I].Reduction;
              FNotebook[I].ShowChGate_:= OldNotebook5[I].ShowChGate_;
              FNotebook[I].ShowChSettings_:= OldNotebook5[I].ShowChSettings_;
              FNotebook[I].ShowChInfo_:= OldNotebook5[I].ShowChInfo_;
              FNotebook[I].ViewMode:= OldNotebook5[I].ViewMode;
              FNotebook[I].DrawRail:= OldNotebook5[I].DrawRail;
              FNotebook[I].AmplTh:= OldNotebook5[I].AmplTh;
              FNotebook[I].AmplDon:= OldNotebook5[I].AmplDon;
              FNotebook[I].ViewChannel:= OldNotebook5[I].ViewChannel;
              FNotebook[I].ViewLine:= OldNotebook5[I].ViewLine;
            end;

          end;
        6:
          begin

            SetLength(OldNotebook6, FExHeader.NotebookCount);
            try
              FBody.Position:= FExHeader.NotebookOffset;
              FBody.ReadBuffer(OldNotebook6[0], FExHeader.NotebookCount * FExHeader.NotebookItemSize);
            except
              SetLength(OldNotebook6, 0);
              FModifyData:= True;
            end;

            SetLength(FNotebook, FExHeader.NotebookCount);
            for I := 0 to FExHeader.NotebookCount - 1 do
            begin
              FillChar(&FNotebook[I].ID, SizeOf(TFileNotebook_ver_7), 0);

              FNotebook[I].ID:= OldNotebook6[I].ID;
              FNotebook[I].Rail:= OldNotebook6[I].Rail;
              FNotebook[I].DisCoord:= OldNotebook6[I].DisCoord;
              FNotebook[I].Defect:= OldNotebook6[I].Defect;
              FNotebook[I].BlokNotText:= OldNotebook6[I].BlokNotText;
              FNotebook[I].DT:= OldNotebook6[I].DT;
              FNotebook[I].Zoom:= OldNotebook6[I].Zoom;
              FNotebook[I].Reduction:= OldNotebook6[I].Reduction;
              FNotebook[I].ShowChGate_:= OldNotebook6[I].ShowChGate_;
              FNotebook[I].ShowChSettings_:= OldNotebook6[I].ShowChSettings_;
              FNotebook[I].ShowChInfo_:= OldNotebook6[I].ShowChInfo_;
              FNotebook[I].ViewMode:= OldNotebook6[I].ViewMode;
              FNotebook[I].DrawRail:= OldNotebook6[I].DrawRail;
              FNotebook[I].AmplTh:= OldNotebook6[I].AmplTh;
              FNotebook[I].AmplDon:= OldNotebook6[I].AmplDon;
              FNotebook[I].ViewChannel:= OldNotebook6[I].ViewChannel;
              FNotebook[I].ViewLine:= OldNotebook6[I].ViewLine;
            end;

          end;
        7:
          begin
            SetLength(FNotebook, FExHeader.NotebookCount);
            try
              FBody.Position:= FExHeader.NotebookOffset;
              FBody.ReadBuffer(FNotebook[0], FExHeader.NotebookCount * FExHeader.NotebookItemSize);
            except
              SetLength(FNotebook, 0);
              FModifyData:= True;
            end;
          end;

        8 .. 99:
          begin
            FUnknownFileVersion:= True;
            Exit;
          end;
      end;
    end;

    { if FExHeader.DecodingCount <> 0 then
      if FExHeader.DecodingDataVer = 3 then
      begin
      SetLength(FDecoding, FExHeader.DecodingCount);
      try
      FBody.Position:= FExHeader.DecodingOffset;
      FBody.ReadBuffer(FDecoding[0], FExHeader.DecodingCount * FExHeader.DecodingItemSize);
      except
      //          FLog.Add(Language.GetCaption(0150015));
      SetLength(FDecoding, 0);
      FModifyData:= True;
      end;
      end
      else
      begin
      FUnknownFileVersion:= True;
      Exit;
      end;

      if FExHeader.RSPCount <> 0 then
      if FExHeader.RSPDataVer = 3 then
      begin
      SetLength(FRSPInfo, FExHeader.RSPCount);
      try
      FBody.Position:= FExHeader.RSPOffset;
      FBody.ReadBuffer(FRSPInfo[0], FExHeader.RSPCount * FExHeader.RSPItemSize);
      except
      //          FLog.Add(Language.GetCaption(0150016));
      SetLength(FRSPInfo, 0);
      FModifyData:= True;
      end;
      end
      else
      begin
      FUnknownFileVersion:= True;
      Exit;
      end; }
  end
  else
  begin
    NeedReAnalyze:= True;
    // FExHeader.Modify:= $FF;
  end;

  if FExHeader.EventCount = 0 then NeedReAnalyze:= True;
  FExHdrLoadOk:= ExHeadOk;

  if (not ExHeadOk) or NeedReAnalyze then
  begin
    AnalyzeFileBody;
    SaveExData(FExHdrLoadOk);
    FModifyData:= False;
  end;

end;

procedure TAviconDataSource.CutExHeader;
var
  F: file;

begin
  try
    AssignFile(F, FFileName); // �������� ����� ��� ������
    Reset(F, 1);
  except
    // FLog.Add(Language.GetCaption(0150019));
    // ShowMessage(Language.GetCaption(0150006));
    Exit;
  end;

  // ��������� ������� ������������ ���������
  try
    Seek(F, FHead.TableLink);
    Truncate(F);
  except
    // FLog.Add(Language.GetCaption(0150020));
    Exit;
  end;
end;

procedure TAviconDataSource.SaveExData(CutOldExHeader: Boolean);
var
  I: Integer;
  F: file;
  FileDate: TDateTime;
  Evt: TFileEvent;

begin
  if not FFullHeader then Exit;
  if FReadOnlyFlag then Exit;
  if FLoadFromStream then Exit;

  if not SkipTestDateTime then
  begin
    FileDate:= FileDateToDateTime(FileAge(FFileName));
    if (FSaveFileDate <> FileDate) or (FSaveFileSize <> FBody.Size) then
    begin
      //      ShowMessage(Language.GetCaption(0150009));
      Exit;
    end;
  end;

  try
    AssignFile(F, FFileName);                // �������� ����� ��� ������
    Reset(F, 1);
  except
    //    FLog.Add(Language.GetCaption(0150019));
    //    ShowMessage(Language.GetCaption(0150006));
    Exit;
  end;

  if CutOldExHeader and (FHead.TableLink <> $FFFFFFFF) then   // ��������� ������� ������������ ���������
  begin
    try
      Seek(F, FHead.TableLink);
      Truncate(F);
    except
    //      FLog.Add(Language.GetCaption(0150020));
      Exit;
    end;
  end;

  try
    Seek(F, FileSize(F));
    FHead.TableLink:= FileSize(F);         // ����������� ��������� ������ ������������ ���������
    FDataSize:= FileSize(F);
  except
    //    FLog.Add(Language.GetCaption(0150021));
    Exit;
  end;

  try
    Seek(F, 0);
    BlockWrite(F, FHead, Sizeof(TFileHeader_v02)); // ������ ������ �������� ��������� ������������ ���������
  except
    //    FLog.Add(Language.GetCaption(0150022));
    Exit;
  end;

//  FExHeader.EndKM:= 0 {FLastStolb.Km[1]} ;
//  FExHeader.EndPK:= 0 {FLastStolb.Pk[1]} ;
//  FExHeader.EndMM:= 0 {(FCurSaveSysCrd - FLastStolbSysCrd) * FHead.ScanStep div 100};
//  FExHeader.EndSysCoord:= 0 {FCurSaveSysCrd};
//  FExHeader.EndDisCoord:= 0 {FCurSaveDisCrd};
  FillChar(FExHeader.FFConst[0], Length(FExHeader.FFConst), $FF);
  FillChar(FExHeader.Reserv1[0], Length(FExHeader.Reserv1), $FF);
  FillChar(FExHeader.Reserv2[0], Length(FExHeader.Reserv2), $FF);
  FExHeader.DataVer:= 7;
  FExHeader.DataSize:= SizeOf(TExHeader);


  Seek(F, FHead.TableLink); // ������ ������ ������������ ���������
  BlockWrite(F, FExHeader, FExHeader.DataSize);

  FExHeader.EventDataVer:= 5; // ������ ������ ������� �����
  FExHeader.EventItemSize:= 13;
  FExHeader.EventCount:= Length(FEvents);
//  FExHeader.EventOffset:= FileSize(F);
  if FExHeader.EventCount <> 0 then
  begin
    FExHeader.EventOffset:= FilePos(F);

    for I:= 0 to High(FEvents) do
    begin
      // Evt.ID:= FEvents[I].ID;
      // Evt.OffSet:= FEvents[I].OffSet;
      // Evt.SysCoord:= FEvents[I].SysCoord;
      // Evt.DisCoord:= FEvents[I].DisCoord;
      Evt:= FEvents[I];
      BlockWrite(F, Evt, FExHeader.EventItemSize);
    end;
  end;

  FExHeader.NotebookDataVer:= 7; // ������ ������ ������� �������������
//  FExHeader.NotebookOffset:= FileSize(F);
  FExHeader.NotebookItemSize:= SizeOf(TFileNotebook);
  FExHeader.NotebookCount:= Length(FNotebook);
  if FExHeader.NotebookCount <> 0 then
    try
      FExHeader.NotebookOffset:= FilePos(F);
      BlockWrite(F, FNotebook[0], FExHeader.NotebookCount * SizeOf(TFileNotebook));
    except
    //      FLog.Add(Language.GetCaption(0150025));
      Exit;
    end;

                                                  // ������ ������ ������������ ���������
  Seek(F, FHead.TableLink);
  BlockWrite(F, FExHeader, FExHeader.DataSize);
  Close(F);


  (*
    FillChar(FExHeader.FFConst[0], Length(FExHeader.FFConst), $FF);
    FillChar(FExHeader.Reserv1[1], Length(FExHeader.Reserv1), $FF);
    FillChar(FExHeader.Reserv2[1], Length(FExHeader.Reserv2), $FF);

    FExHeader.DataVer:= 6;
    FExHeader.DataSize:= SizeOf(TExHeader);
    FExHeader.Flag:= 0;

    try
    Seek(F, FHead.TableLink);                   // ������ ������������ ���������
    BlockWrite(F, FExHeader, FExHeader.DataSize);
    except
    //    FLog.Add(Language.GetCaption(0150028));
    Exit;
    end;

    FExHeader.ErrorDataVer:= 3;                   // ������ ������ ������ ��������� �����
    FExHeader.ErrorItemSize:= SizeOf(TFileError);
    FExHeader.ErrorCount:= Length(FErrors);
    FExHeader.ErrorOffset:= 0;
    if FExHeader.ErrorCount <> 0 then
    try
    FExHeader.ErrorOffset:= FilePos(F);
    BlockWrite(F, FErrors[0], FExHeader.ErrorCount * FExHeader.ErrorItemSize);
    except
    //      FLog.Add(Language.GetCaption(0150023));
    Exit;
    end;

    FExHeader.EventDataVer:= 3;                   // ������ ������ ������� �����
    FExHeader.EventItemSize:= SizeOf(TFileEvent);
    FExHeader.EventCount:= Length(FEvents);
    FExHeader.EventOffset:= 0;
    if FExHeader.EventCount <> 0 then
    try
    FExHeader.EventOffset:= FilePos(F);
    BlockWrite(F, FEvents[0], FExHeader.EventCount * FExHeader.EventItemSize);
    except
    //      FLog.Add(Language.GetCaption(0150024));
    Exit;
    end;

    FExHeader.NotebookDataVer:= 4;                // ������ ������ ������� �������������
    FExHeader.NotebookOffset:= 0;
    FExHeader.NotebookItemSize:= SizeOf(TFileNotebook_Ver4);
    FExHeader.NotebookCount:= Length(FNotebook);
    if FExHeader.NotebookCount <> 0 then
    try
    FExHeader.NotebookOffset:= FilePos(F);
    BlockWrite(F, FNotebook[0], FExHeader.NotebookCount * SizeOf(TFileNotebook_Ver4));
    except
    //      FLog.Add(Language.GetCaption(0150025));
    Exit;
    end;

    FExHeader.DecodingDataVer:= 3;                 // ������ ������ ������� � ������ �����������
    FExHeader.DecodingOffset:= 0;
    FExHeader.DecodingItemSize:= SizeOf(TFileDecoding);
    FExHeader.DecodingCount:= Length(FDecoding);
    if FExHeader.DecodingCount <> 0 then
    try
    FExHeader.DecodingOffset:= FilePos(F);
    BlockWrite(F, FDecoding[0], FExHeader.DecodingCount * SizeOf(TFileDecoding));
    except
    //      FLog.Add(Language.GetCaption(0150026));
    Exit;
    end;

    FExHeader.RSPDataVer:= 3;                 // ������ ������ ������ ���
    FExHeader.RSPOffset:= 0;
    FExHeader.RSPItemSize:= SizeOf(TRSPInfo);
    FExHeader.RSPCount:= Length(FRSPInfo);
    if FExHeader.RSPCount <> 0 then
    try
    FExHeader.RSPOffset:= FilePos(F);
    BlockWrite(F, FRSPInfo[0], FExHeader.RSPCount * SizeOf(TRSPInfo));
    except
    //      FLog.Add(Language.GetCaption(0150027));
    Exit;
    end;

    FExHeader.UCSDataVer:= 3;
    FExHeader.UCSItemSize:= 0;
    FExHeader.UCSOffset:= 0;
    FExHeader.UCSCount:= 0;

    FExHeader.LabelEditDataVer:= 3;
    FExHeader.LabelEditItemSize:= 0;
    FExHeader.LabelEditOffset:= 0;
    FExHeader.LabelEditCount:= 0;

    try                                             // ������ ������ ������������ ���������
    Seek(F, FHead.TableLink);
    BlockWrite(F, FExHeader, FExHeader.DataSize);
    Close(F);
    except
    //    FLog.Add(Language.GetCaption(0150028));
    Exit;
    end;

    FBody.Clear;
    FBody.LoadFromFile(FFileName);
*)
    FSaveFileDate:= FileDateToDateTime(FileAge(FFileName));
    FSaveFileSize:= FBody.Size;

    FExHdrLoadOk:= True;
end;

// ------< ������ � ��� ��������� - ���������� ������ 1 - �� ������ >-----------
(*
procedure TAviconDataSource.DataToEcho;
begin
  if FSrcDat[0] shr 3 and 7 = 1 then
  begin
    with FDestDat[TRail(FSrcDat[0] shr 6 and 1)] do
    begin
      // Count:= 0;
      case FSrcDat[0] and $07 of
   //     0: ZerroFlag:= True;
        1:
          begin
            if Count < 16 then
              Inc(Count);
            Delay[Count]:= FSrcDat[1];
            Ampl[Count]:= FSrcDat[2] shr 4 and $0F;
          end;
        2:
          begin
            if Count < 16 then
              Inc(Count);
            Delay[Count]:= FSrcDat[1];
            Ampl[Count]:= FSrcDat[3] shr 4 and $0F;
            if Count < 16 then
              Inc(Count);
            Delay[Count]:= FSrcDat[2];
            Ampl[Count]:= FSrcDat[3] and $0F;
          end;
        3:
          begin
            if Count < 16 then
              Inc(Count);
            Delay[Count]:= FSrcDat[1];
            Ampl[Count]:= FSrcDat[4] shr 4 and $0F;
            if Count < 16 then
              Inc(Count);
            Delay[Count]:= FSrcDat[2];
            Ampl[Count]:= FSrcDat[4] and $0F;
            if Count < 16 then
              Inc(Count);
            Delay[Count]:= FSrcDat[3];
            Ampl[Count]:= FSrcDat[5] shr 4 and $0F;
          end;
        4:
          begin
            if Count < 16 then
              Inc(Count);
            Delay[Count]:= FSrcDat[1];
            Ampl[Count]:= FSrcDat[5] shr 4 and $0F;
            if Count < 16 then
              Inc(Count);
            Delay[Count]:= FSrcDat[2];
            Ampl[Count]:= FSrcDat[5] and $0F;
            if Count < 16 then
              Inc(Count);
            Delay[Count]:= FSrcDat[3];
            Ampl[Count]:= FSrcDat[6] shr 4 and $0F;
            if Count < 16 then
              Inc(Count);
            Delay[Count]:= FSrcDat[4];
            Ampl[Count]:= FSrcDat[6] and $0F;
          end;
      end;
    end;
  end;
end;
 *)
// ------< ������ ��������� ����� � ����������� ��������� �������� >-----------

procedure TAviconDataSource.CloseFile;
begin
  SaveExData(FExHdrLoadOk);
end;

//var
//  Log: TStringList = nil;

function TAviconDataSource.SkipEvent(ID: Byte): Boolean; // ������� �������
var
  FDWID: DWord;
  FID: Byte;
  Ch: Byte;

begin
  if ID and 128 = 0 then
  begin
    Ch:= (ID shr 2) and 15;
    FBody.Position:= FBody.Position + EchoLen[ID and $03];
    if FExtendedChannels and (Ch > 14) then FBody.Position:= FBody.Position + 1;
    Result:= True;
  end
  else

//  if Assigned(Log) then Log.Add(Format('0x%x', [ID]));

    case ID of
      EID_HandScan:  // ������
                     begin
                       FBody.ReadBuffer(FDWID, 4);
                       FBody.Position:= FBody.Position + SizeOf(THSHead) - 4 + FDWID;
                     end;
             EID_Ku: FBody.Position:= FBody.Position + 3; // ��������� �������� ����������������
            EID_Att: FBody.Position:= FBody.Position + 3; // ��������� �����������
            EID_TVG: FBody.Position:= FBody.Position + 3; // ��������� ���
          EID_StStr: FBody.Position:= FBody.Position + 3; // ��������� ��������� ������ ������
         EID_EndStr: FBody.Position:= FBody.Position + 3; // ��������� ��������� ����� ������
         EID_HeadPh: FBody.Position:= FBody.Position + 3; // ������ ���������� ���������
           EID_Mode: FBody.Position:= FBody.Position + 7; // ��������� ������
    EID_SetRailType: FBody.Position:= FBody.Position + 0; // ��������� �� ��� ������
     EID_PrismDelay: FBody.Position:= FBody.Position + 4; // ��������� 2�� (word)
          EID_Stolb: FBody.Position:= FBody.Position + 144; // ������� ����������
         EID_Switch: // ����� ����������� ��������
                     begin
                       FBody.ReadBuffer(FID, 1);
                       FBody.Position:= FBody.Position + FID * SizeOf(Char);
                     end;
       EID_DefLabel: // ����� ������� + �����
                     begin
                       FBody.ReadBuffer(FID, 1);
                       FBody.Position:= FBody.Position + 1 + FID * SizeOf(Char);
                     end;
      EID_TextLabel: // ��������� �������
                     begin
                       FBody.ReadBuffer(FID, 1);
                       FBody.Position:= FBody.Position + FID * SizeOf(Char);
                     end;
     EID_StBoltStyk: FBody.Position:= FBody.Position + 0;   // ������� ������ ��
    EID_EndBoltStyk: FBody.Position:= FBody.Position + 0;   // ���������� ������ ��
           EID_Time: FBody.Position:= FBody.Position + 2;   // ������� �������
  EID_StolbChainage: FBody.Position:= FBody.Position + 136; // ������� ����������
        EID_Sensor1: FBody.Position:= FBody.Position + 2;   // ������ ������� �� � �������
  EID_ZerroProbMode: FBody.Position:= FBody.Position + 1;   // ����� ������ �������� 0 ����
      EID_LongLabel: FBody.Position:= FBody.Position + 24;  // ����������� �������
       EID_AirBrush: FBody.Position:= FBody.Position + 182; // ��������������
    EID_AirBrushJob: FBody.Position:= FBody.Position + 8;   // ��������� � ���������� ������� �� ������-�������
      EID_AirBrush2: FBody.Position:= FBody.Position + 246; // �������������� � ���������� �����������
EID_AirBrushTempOff: FBody.Position:= FBody.Position + 5;   // ��������� ���������� ���������������
   EID_AlarmTempOff: FBody.Position:= FBody.Position + 5;   // ��������� ���������� ��� �� ���� �������
       EID_GPSCoord: FBody.Position:= FBody.Position + 8;   // �������������� ����������
       EID_GPSState: FBody.Position:= FBody.Position + 9;   // ��������� ��������� GPS
      EID_GPSCoord2: FBody.Position:= FBody.Position + 12;  // �������������� ���������� �� ���������
     EID_NORDCO_Rec: FBody.Position:= FBody.Position + SizeOf(edNORDCO_Rec); // ������ NORDCO
          EID_Media: begin
                       FBody.ReadBuffer(FID, 1); // ���
                       FBody.ReadBuffer(FDWID, 4); // ������
                       FBody.Position:= FBody.Position + FDWID;   // �����������
                     end;
//       EID_CheckSum: FBody.Position:= FBody.Position + 5; // ����� � ����������� �����
      EID_SysCrd_SS: FBody.Position:= FBody.Position + 2;   // ��������� ���������� ��������
      EID_SysCrd_SF: FBody.Position:= FBody.Position + 5;   // ������ ��������� ���������� � �������� �������
      EID_SysCrd_FS: FBody.Position:= FBody.Position + 5;   // ��������� ���������� ��������
      EID_SysCrd_FF: FBody.Position:= FBody.Position + 8;   // ������ ��������� ���������� � ������ �������
      EID_SysCrd_NS: FBody.Position:= FBody.Position + 1;
      EID_SysCrd_NF: FBody.Position:= FBody.Position + 4;
   EID_SysCrd_UMUA_Left_NF: FBody.Position:= FBody.Position + 4; // ������ ��������� ���������� ��� ������ ��� A, ����� �������
   EID_SysCrd_UMUB_Left_NF: FBody.Position:= FBody.Position + 4; // ������ ��������� ���������� ��� ������ ��� �, ����� �������
  EID_SysCrd_UMUA_Right_NF: FBody.Position:= FBody.Position + 4; // ������ ��������� ���������� ��� ������ ��� A, ������ �������
  EID_SysCrd_UMUB_Right_NF: FBody.Position:= FBody.Position + 4; // ������ ��������� ���������� ��� ������ ��� �, ������ �������

        EID_EndFile: FBody.Position:= FBody.Position + 13;  // ����� �����
    else
//      raise EMyExcept.Create('SkipEvent: Unknown event ID');
      Result:= False;
    end;
end;

function TAviconDataSource.LoadEventData(ID: Byte; Ptr: PEventData): Boolean; // ������� ������ �������
var
  FDWID: DWord;
  FID: Byte;

begin
  case ID of
           EID_Ku: FBody.ReadBuffer(Ptr^, 3);            // ��������� �������� ����������������
          EID_Att: FBody.ReadBuffer(Ptr^, 3);            // ��������� �����������
          EID_TVG: FBody.ReadBuffer(Ptr^, 3);            // ��������� ���
        EID_StStr: FBody.ReadBuffer(Ptr^, 3);            // ��������� ��������� ������ ������
       EID_EndStr: FBody.ReadBuffer(Ptr^, 3);            // ��������� ��������� ����� ������
       EID_HeadPh: FBody.ReadBuffer(Ptr^, 3);            // ������ ���������� ���������
         EID_Mode: FBody.ReadBuffer(Ptr^, 7);            // ��������� ������
  EID_SetRailType: ;                                     // ��������� �� ��� ������
   EID_PrismDelay: FBody.ReadBuffer(Ptr^, 4);            // ��������� 2�� (word)
        EID_Stolb: FBody.ReadBuffer(Ptr^, 144);          // ������� ����������
       EID_Switch: // ����� ����������� ��������
                   begin
                     FBody.ReadBuffer(pedTextLabel(Ptr)^.Len, 1);
                     FBody.ReadBuffer(pedTextLabel(Ptr)^.Text[0], pedTextLabel(Ptr)^.Len * SizeOf(Char));
                   end;
     EID_DefLabel: // ����� �������
                   begin
                     FBody.ReadBuffer(pedDefLabel(Ptr)^.Len, 2);
                     FBody.ReadBuffer(pedDefLabel(Ptr)^.Text[0], pedTextLabel(Ptr)^.Len * SizeOf(Char));
                   end;
    EID_TextLabel: // ��������� �������
                   begin
                     FBody.ReadBuffer(pedTextLabel(Ptr)^.Len, 1);
                     FBody.ReadBuffer(pedTextLabel(Ptr)^.Text[0], pedTextLabel(Ptr)^.Len * SizeOf(Char));
                   end;
   EID_StBoltStyk: ; // ������� ������ ��
  EID_EndBoltStyk: ; // ���������� ������ ��
         EID_Time: FBody.ReadBuffer(Ptr^, 2);   // ������� �������
EID_StolbChainage: FBody.ReadBuffer(Ptr^, 136); // ������� ����������
      EID_Sensor1: FBody.ReadBuffer(Ptr^, 2);   // ������ ������� �� � �������
EID_ZerroProbMode: FBody.ReadBuffer(Ptr^, 1);   // ����� ������ �������� 0 ����
    EID_LongLabel: FBody.ReadBuffer(Ptr^, 24);  // ����������� �������
     EID_AirBrush: FBody.ReadBuffer(Ptr^, 182); // ��������������
    EID_AirBrush2: FBody.ReadBuffer(Ptr^, 246); // �������������� � ���������� �����������
  EID_AirBrushJob: FBody.ReadBuffer(Ptr^, 8);   // ��������� � ���������� ������� �� ������-�������
EID_AirBrushTempOff: FBody.ReadBuffer(Ptr^, 5); // ��������� ���������� ���������������
 EID_AlarmTempOff: FBody.ReadBuffer(Ptr^, 5);   // ��������� ���������� ��� �� ���� �������
     EID_GPSCoord: FBody.ReadBuffer(Ptr^, 8);   // �������������� ����������
     EID_GPSState: FBody.ReadBuffer(Ptr^, 9);   // ��������� ��������� GPS
        EID_Media: FBody.ReadBuffer(Ptr^, 5);   // �����������
    EID_GPSCoord2: FBody.ReadBuffer(Ptr^, 12);  // �������������� ���������� �� ���������
   EID_NORDCO_Rec:
                   FBody.ReadBuffer(Ptr^, SizeOf(edNORDCO_Rec)); // ������ NORDCO
//     EID_CheckSum: FBody.ReadBuffer(Ptr^, 5); // ����� � ����������� �����
    EID_SysCrd_SS: FBody.ReadBuffer(Ptr^, 2);   // ��������� ���������� ��������
    EID_SysCrd_SF: FBody.ReadBuffer(Ptr^, 5);   // ������ ��������� ���������� � �������� �������
    EID_SysCrd_FS: FBody.ReadBuffer(Ptr^, 5);   // ��������� ���������� ��������
    EID_SysCrd_FF: FBody.ReadBuffer(Ptr^, 8);   // ������ ��������� ���������� � ������ �������
    EID_SysCrd_NS: FBody.ReadBuffer(Ptr^, 1);
    EID_SysCrd_NF: FBody.ReadBuffer(Ptr^, 4);
   EID_SysCrd_UMUA_Left_NF: FBody.ReadBuffer(Ptr^, 4); // ������ ��������� ���������� ��� ������ ��� A, ����� �������
   EID_SysCrd_UMUB_Left_NF: FBody.ReadBuffer(Ptr^, 4); // ������ ��������� ���������� ��� ������ ��� �, ����� �������
  EID_SysCrd_UMUA_Right_NF: FBody.ReadBuffer(Ptr^, 4); // ������ ��������� ���������� ��� ������ ��� A, ������ �������
  EID_SysCrd_UMUB_Right_NF: FBody.ReadBuffer(Ptr^, 4); // ������ ��������� ���������� ��� ������ ��� �, ������ �������

      EID_EndFile: begin
                     FBody.ReadBuffer(Ptr^, 13);  // ����� �����
//                     if Assigned(Log) then Log.Add('0xFF');
                   end;
    else
    begin
//      raise EMyExcept.Create('LoadEventData: Unknown event ID');
      result:= false;
      exit;
    end;
  end;
        result:= true;

end;

procedure TAviconDataSource.AnalyzeFileBody; // ���������� - ��������� ���� � �����. - ����� �����
label LoadCont, LoadStop;

var
  HSHead: THSHead;
  HSItem: THSItem;
  CurSysCoord: Integer;
  Load_CurSysCoord: Integer;
  LastSysCoord: Integer;
  LastDisCoord: Integer;
  CurDisCoord: Int64;
  BackMotionFlag: Boolean;
  LastCoordEvt: Integer;
  // CurCoordEvt: Integer;
  SaveOffset: Integer;
  LastSaveOffset: Integer;
  // CurrCoord: mCoord;
  SkipBytes: Integer;
  I: Integer;
  LoadID: Byte;
  LoadByte: array [1 .. 22] of Byte;
  LoadLongInt: array [1 .. 5] of Longint; // absolute LoadByte[1];
  NeedSync: Boolean;
  Card: DWord;
  CheckSumm: Int64;
  ErrorOffset: Integer;
  NewProgress: Integer;
  LoadStopFlag: Boolean;
  FirstCoordAfterError: Boolean;
  // FirstCoordAfterError2: Boolean;
  // CrdFlag: Boolean;
  // ErrorCoord: Integer;
  // DeltaCoord: Integer;
  LastStolbSysCoord: Integer;
  AddErrID: Integer;
  FFF: Boolean;
  Last: Integer;
  Temp1, Temp2: Extended;
  StopSearch: Boolean;

  procedure AddEvent(ID: Byte; SysCoord: Integer = - MaxInt; DisCoord: Integer = -MaxInt);
  begin
    // UnPackEcho;

    SetLength(FEvents, Length(FEvents) + 1);

    FEvents[ High(FEvents)].ID:= ID;
    FEvents[ High(FEvents)].OffSet:= SaveOffset;
    if SysCoord = -MaxInt then FEvents[ High(FEvents)].SysCoord:= CurSysCoord
                          else FEvents[ High(FEvents)].SysCoord:= SysCoord;
    if DisCoord = -MaxInt then FEvents[ High(FEvents)].DisCoord:= CurDisCoord
                          else FEvents[ High(FEvents)].DisCoord:= DisCoord;
    // FEvents[High(FEvents)].Pack:= FPack;

    LastSaveOffset:= SaveOffset;
  end;

  function TestOffset(Pos: Integer): Boolean;
  var
    I: Integer;
    ID: Byte;

  begin
    Result:= True;
    if Pos < 0 then
      Exit;
    I:= FBody.Position;
    if (Pos < 0) or (Pos >= FBody.Size) then
    begin
      Result:= False;
      Exit;
    end;
    FBody.Position:= Pos;
    FBody.ReadBuffer(ID, 1);
    FBody.Position:= I;
    Result:= ID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF, EID_SysCrd_NS, EID_SysCrd_NF];
  end;

var
  // WaitForm: TWaitForm;
  // BSHead: mBSAmplHead;

  SearchBMEnd: Boolean;
  StartBMSysCoord: Integer;


begin
//  Log:= TStringList.Create;

  { WaitForm:= TWaitForm.Create(nil);
    WaitForm.Visible:= True;
    WaitForm.BringToFront;
    WaitForm.Refresh;
    }
  // ����� ������
  BackMotionFlag:= False;
  SearchBMEnd:= False;
//  FLeftFlag:= False;
//  FRightFlag:= False;
  SetLength(FEvents, 0); // ������� ������� ���������
  // SetLength(FErrors, 0); // ������� ������� ������

  LoadStopFlag:= False;
  CurSysCoord:= 0;
  Load_CurSysCoord:= 0;
  CurDisCoord:= 0;
  LastSysCoord:= MaxInt;
  LastSaveOffset:= 0;
  LastCoordEvt:= -1;
  NeedSync:= False;
  LastStolbSysCoord:= 0;

  // CurrCoord.Km[1]:= FHead.StartKM;
  // CurrCoord.Pk[1]:= FHead.StartPk;

  // PackReset;

  if not FFullHeader then FBody.Position:= 17
                     else FBody.Position:= SizeOf(TFileHeader); // ������� ���������
  SaveOffset:= FBody.Position; // ���������� offset �������
  AddEvent(EID_FwdDir, 0);

  // DeltaCoord:= 0;

LoadCont :

  if FBody.Position < FDataSize then

  try
    repeat
      SkipBytes:= -1;
      SaveOffset:= FBody.Position; // ���������� offset �������
      FBody.ReadBuffer(LoadID, 1); // �������� �������������� �������

      // begin
      (* if {((LoadID shr 3 and $07 <> 0) and (} LoadID and $07 < 5 {)) or}
        { (LoadID shr 3 and $07 = 0) } then // (��� ������ � 0 ������) ��� (���������� �����������  > 4)
        begin
        case TRail(LoadID shr 6 and 1) of    // ������� ������ � ������� ������ � ������ ����
        rLeft: FLeftFlag:= True;
        rRight: FRightFlag:= True;
        end;
        if LoadID shr 3 and $07 = 1 then    // ���� ������ ����� �� �������������
        begin
        FSrcDat[0]:= LoadID;
        FBody.ReadBuffer(FSrcDat[1], Avk11EchoLen[LoadID and $07]);
        DataToEcho;
        end
        else if LoadID shr 3 and $07 = 0 then SkipBytes:= 1   // ���� ������� ����� �� ���������� 1 ����
        else SkipBytes:= Avk11EchoLen[LoadID and $07]; // ����� ����������
        end else
        raise EBadID.Create(''); *)
      // SkipBytes:= Avk11EchoLen[LoadID and $03]; // ����������
      // end
      // else                   // ��������� �������
      //begin
        case LoadID of
          (*
            EID_HandScan: begin           // ������
            FBody.ReadBuffer(HSHead, 6);
            repeat
            FBody.ReadBuffer(HSItem, 3);
            if HSItem.Count > 4 then raise EBadLink.Create('');
            FBody.ReadBuffer(HSItem.Data[0], Avk11EchoLen[HSItem.Count]);
            until HSItem.Sample = $FFFF;

            end;
            EID_NewHeader: if FBody.Position = 129 then
            begin
            FBody.ReadBuffer(SkipBytes, 4);
            FBody.Position:= FBody.Position + SkipBytes;
            SkipBytes:= - 1;
            end
            else raise EBadID.Create('');
            {               EID_BSAmpl: begin
            FBody.ReadBuffer(BSHead, SizeOf(mBSAmplHead));
            FBody.Position:= FBody.Position + BSHead.Len * 2;
            SkipBytes:= - 1;
            end; }

            EID_ACLeft      ,   // [$80] ��������� ������������� �������� ����� ���� - ���� ��� ���� �������
            EID_ACRight     ,   // [$81] ��������� ������������� �������� ������ ���� - ���� ��� ���� �������
            EID_Sens        ,   // [$90] ��������� �������� ����������������
            EID_Att         ,   // [$91] ��������� ��������� ���� ����������� (0 �� �������� ����������������)
            EID_VRU         ,   // [$92] ��������� ���
            EID_StStr       ,   // [$93] ��������� ��������� ������ ������
            EID_EndStr      ,   // [$94] ��������� ��������� ����� ������
            EID_HeadPh      ,   // [$95] ������ ���������� ���������
            EID_Mode        ,   // [$96] ��������� ������
            EID_2Tp         ,   // [$99] ��������� 2��
            EID_ZondImp     ,   // [$9A] ��������� ������ ������������ ��������
            EID_SetRailType ,   // [$9B]
            EID_2Tp_Word    ,   // [$9C]
            EID_Strelka     ,   // [$A1] ����� ����������� �������� + �����
            EID_DefLabel    ,   // [$A2] ����� ������� + �����
            EID_TextLabel   ,   // [$A3] ������ ������� (���������) + �����
            EID_StBoltStyk  ,   // [$A4] ������� ������ �� + �����
            EID_EndBoltStyk ,   // [$A5] ���������� ������ �� + �����
            EID_Time        ,   // [$A6] ������� �������
            EID_StLineLabel ,   // [$A7] ������ ����������� ����� + �����
            EID_EndLineLabel,   // [$A8] ����� ����������� ����� + �����
            EID_StSwarStyk  ,   // [$A9] ������� ������ ������� ���� + �����
            EID_EndSwarStyk ,   // [$AA] ���������� ������ ������� ���� + �����
            EID_OpChange    ,   // [$AB] ����� ���������
            EID_PathChange  ,   // [$AC] ����� ������ ����
            EID_MovDirChange,   // [$AD] ����� ����������� ��������
            EID_SezdNo      ,   // [$AE] ����� ������
            EID_LongLabel   ,   // [$AF] ����������� �����
            EID_GPSCoord    ,
            EID_GPSState    ,
            EID_GPSError    :

            SkipBytes:= Avk11EventLen[LoadID];

            EID_Sys1..EID_Sys16: SkipBytes:= 9;
            EID_Stolb: begin
            //FBody.ReadBuffer(CurrCoord, Avk11EventLen[LoadID]);
            LastStolbSysCoord:= CurSysCoord;
            end; *)

          EID_SysCrd_SS:  begin // - �������� ������, �������� ����������
                            FBody.ReadBuffer(LoadByte[1], 2);
                            // if not TestOffset(FBody.Position - 3 - LoadByte[1]) then raise EBadLink.Create('');
                            CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00)
                                or LoadByte[2]);
                          end;
          EID_SysCrd_SF:  begin // - �������� ������, ������ ����������
                            FBody.ReadBuffer(LoadByte[1], 1);
                            // if not TestOffset(FBody.Position - 2 - LoadByte[1]) then raise EBadLink.Create('');
                            FBody.ReadBuffer(LoadLongInt, 4);
                            CurSysCoord:= LoadLongInt[1];
                          end;
          EID_SysCrd_FS:  begin // - ������ ������, �������� ����������
                            FBody.ReadBuffer(LoadLongInt[1], 4);
                            FBody.ReadBuffer(LoadByte[1], 1);
                            // if not TestOffset(FBody.Position - 6 - LoadLongInt[1]) then raise EBadLink.Create('');
                            CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00)
                                or LoadByte[1]);
                          end;
          EID_SysCrd_FF:  begin // - ������ ������, ������ ����������
                            FBody.ReadBuffer(LoadLongInt[1], 4);
                            FBody.ReadBuffer(LoadLongInt[2], 4);
                            // if not TestOffset(FBody.Position - 9 - LoadLongInt[1]) then raise EBadLink.Create('');
                            CurSysCoord:= LoadLongInt[2];
                          end;
          EID_SysCrd_NS:  begin // �������� ����������
                            FBody.ReadBuffer(LoadByte[1], 1);
                            CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[1]);
                          end;
          EID_SysCrd_NF:  begin // - ������ ����������
                            FBody.ReadBuffer(LoadLongInt[2], 4);
                            CurSysCoord:= LoadLongInt[2];
                          end;
          EID_EndFile:    begin // ����� �����
                            FBody.ReadBuffer(LoadByte, 9 + 4);
                            // for I:= 4 + 1 to 4 + 9 do
                            // if LoadByte[I] <> $FF then raise EBadEnd.Create('');
                            AddEvent(EID_EndFile);
                            Break;
                          end


          else // raise EMyExcept.Create('');       if not SkipEvent(LoadID) { LoadID and 128 = 0 } then // �������
            SkipEvent(LoadID);

        end;
      //end;

      if LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF, EID_SysCrd_NS, EID_SysCrd_NF] then // ��������� ����������
      begin
        { if Assigned(FAnalyzeNewCrd) then         // ���������� ������� ��������� ��������� ����������
          begin
          FAnalyzeNewCrd(Pointer(CurSysCoord));
          FAnalyzeNewCrd(Pointer(SaveOffset));
          end; }

        // UnPackEcho;

        if LastSysCoord <> MaxInt then // ���� ��� �� ������ ���������� ����� ...
        begin
          {
            if (LoadID in [EID_SysCrd_SF, EID_SysCrd_FF]) and // ���� ��� ������ ���������� �� ��������� �� ��������
            (CurSysCoord > - 5000000) and
            (CurSysCoord < + 5000000) then
            begin
            FirstCoordAfterError2:= True;                  // ������������� ���� - ������ ������ ���������� ����� ������
            ErrorCoord:= CurSysCoord;                      // ��������� ���������������� �������� ����������
            raise EBadCoord.CreateFmt(' [����: %d �����: %d ]', [LastSysCoord, CurSysCoord]); // ������ ����������
            end;

            CrdFlag:= True;                                     // ������������ - ������� ������ ����������
            if FirstCoordAfterError2 and (LoadID in [EID_SysCrd_SF, EID_SysCrd_FF]) then // ���� ��� ������ ������ ���������� ����� ������ ���������� ...
            begin
            Temp1:= ErrorCoord;
            Temp2:= CurSysCoord;
            if Abs(Temp1 - Temp2) < 1000 then      // � ��� �� ������ ���������� �� ������� ���������� - ������ ��� ���������, � �� ����������� ������
            begin
            FirstCoordAfterError2:= False;                  // ���������� ���� - ������ ������ ���������� ����� ������
            Last:= CurDisCoord;                             // ����������� ����� ���������� �� �������
            Inc(CurDisCoord, Abs(CurSysCoord - LastSysCoord)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)
            LastSysCoord:= ErrorCoord;
            CrdFlag:= False;                                // �������� ������� ������
            end;
            end;
            }
          //if Abs(CurSysCoord - LastSysCoord) > 100000 then    // ��������� �� ������ ����������
          //begin
            // FirstCoordAfterError2:= True;                  // ������������� ���� - ������ ������ ���������� ����� ������
            // ErrorCoord:= CurSysCoord;                      // ��������� ���������������� �������� ����������
            //raise EMyExcept. CreateFmt('[����: %d �����: %d ]', [LastSysCoord, CurSysCoord]); // ������ ����������
          //end;

          { if CrdFlag then                                  // ������� ������ ����������
            begin }
          Last:= CurDisCoord;
          Inc(CurDisCoord, Abs(CurSysCoord - LastSysCoord)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)
          // end;
          {
            if FirstCoordAfterError then
            begin
            if GetErrorCount > 0 then
            begin
            FErrors[GetErrorCount - 1].EndSysCoord:= CurSysCoord;
            FErrors[GetErrorCount - 1].EndDisCoord:= CurDisCoord;
            end;
            FirstCoordAfterError:= False;
            end;
            }
          if (not BackMotionFlag) and (CurSysCoord < LastSysCoord) then
          // ���� ���������� ����������� �������� (������ �� �����)
          begin
            AddEvent(EID_BwdDir, LastSysCoord, Last);
            BackMotionFlag:= True;
            // AddEvent(EID_BwdDir);
            if not SearchBMEnd then
            begin
              StartBMSysCoord:= LastSysCoord;
              SearchBMEnd:= True;
            end;
          end
          else if BackMotionFlag and (CurSysCoord > LastSysCoord) then
          // ���� ���������� ����������� �������� (����� �� ������)
          begin
            AddEvent(EID_FwdDir, LastSysCoord, Last);
            BackMotionFlag:= False;
            // AddEvent(EID_FwdDir);
          end;
          if SearchBMEnd and (CurSysCoord > StartBMSysCoord) then
          begin
            SearchBMEnd:= False;
            if CurSysCoord - LastSysCoord <> 0 then AddEvent(EID_EndBM, StartBMSysCoord, Round(LastDisCoord + (StartBMSysCoord - LastSysCoord) * (CurDisCoord - LastDisCoord) / (CurSysCoord - LastSysCoord)))
                                               else AddEvent(EID_EndBM);
          end;
        end
        else
        begin
          for I:= 0 to High(FEvents) do
          begin
            FEvents[I].SysCoord:= CurSysCoord;
            FEvents[I].DisCoord:= 0;
          end;
        end;
        LastSysCoord:= CurSysCoord;
        LastDisCoord:= CurDisCoord;
      end;

//      if LoadID = EID_Mode then ShowMessage('!');


      if (LoadID in [ EID_HandScan    ,
                      EID_Ku          ,
                      EID_Att         ,
                      EID_TVG         ,
                      EID_StStr       ,
                      EID_EndStr      ,
                      EID_HeadPh      ,
                      EID_Mode        ,
                      EID_SetRailType ,
                      EID_PrismDelay  ,
                      EID_Stolb       ,
                      EID_StolbChainage,
                      EID_Switch      ,
                      EID_DefLabel    ,
                      EID_TextLabel   ,
                      EID_StBoltStyk  ,
                      EID_EndBoltStyk ,
                      EID_Time        ,
                      EID_Sensor1     ,
                      EID_ZerroProbMode,
                      EID_LongLabel   ,
                      EID_AirBrush    ,
                      EID_AirBrushJob ,
                      EID_AirBrushTempOff,
                      EID_AlarmTempOff,
                      EID_AirBrush2   ,
                      EID_GPSCoord    ,
                      EID_GPSState    ,
                      EID_Media       ,
                      EID_GPSCoord2   ,
                      EID_NORDCO_Rec  ,
                      EID_EndFile     ]) then AddEvent(LoadID);

      if FBody.Position - LastSaveOffset >= 16384 then AddEvent(EID_LinkPt);
      if SkipBytes <> -1 then FBody.Position:= FBody.Position + SkipBytes;

      { if Assigned(LoadProg) then
        begin
        NewProgress:= LoadProg.MinValue + Round((LoadProg.MinValue - LoadProg.MaxValue) * FBody.Position / FDataSize);
        if LoadProg.Progress <> NewProgress then LoadProg.Progress:= NewProgress;
        end;
        }
    until FBody.Position + 1 > FDataSize;

  except
    { on EBadID do
      begin
      ErrorOffset:= SaveOffset;
      //      FLog.Add(Format('AnalyzeFileBody: %x �������� ID', [SaveOffset]));
      AddErrID:= EID_BadBody;
      NeedSync:= True;
      end;
      on EBadLink do
      begin
      ErrorOffset:= SaveOffset;
      //      FLog.Add(Format('AnalyzeFileBody: %x �������� ������ � ����������', [SaveOffset]));
      AddErrID:= EID_BadLinc;
      NeedSync:= True;
      end;
      on EBadEnd do
      begin
      ErrorOffset:= SaveOffset;
      //      FLog.Add(Format('AnalyzeFileBody: %x �������� ����� �����', [SaveOffset]));
      AddErrID:= EID_BadBody; // EID_BadEnd;
      NeedSync:= True;
      end;
      on E0: EBadCoord do
      begin
      ErrorOffset:= SaveOffset;
      //      FLog.Add(Format('AnalyzeFileBody: %x ������ ����������', [SaveOffset]) + E0.Message);
      CurSysCoord:= LastSysCoord;
      AddErrID:= EID_BadCoord;
      NeedSync:= True;
      end;
      on EReadError do
      begin
      //      FLog.Add(Format('AnalyzeFileBody: %x ������ ������', [SaveOffset]));
      end;
      //    else ShowMessage(IntToStr(GE)); }
  end;
  (*
    if NeedSync then
    begin
    StopSearch:= False;
    try
    while (FExHdrLoadOk and (FBody.Position + 1 < FHead.TableLink + 1)) or
    ((not FExHdrLoadOk) and (FBody.Position + 1 < FBody.Size)) do
    begin
    if StopSearch then Break;
    SaveOffset:= FBody.Position;

    FBody.ReadBuffer(LoadByte[1], 1);
    case LoadByte[1] of
    EID_EndFile: begin // ����� �����
    FBody.ReadBuffer(LoadByte, 9 + 4);
    for I:= 4 + 1 to 4 + 9 do
    if (LoadByte[4 + 1] = $FF) and
    (LoadByte[4 + 2] = $FF) and
    (LoadByte[4 + 3] = $FF) and
    (LoadByte[4 + 4] = $FF) and
    (LoadByte[4 + 5] = $FF) and
    (LoadByte[4 + 6] = $FF) and
    (LoadByte[4 + 7] = $FF) and
    (LoadByte[4 + 8] = $FF) and
    (LoadByte[4 + 9] = $FF) then
    begin
    AddEvent(EID_EndFile);
    StopSearch:= True;
    Break;
    end;
    end;

    EID_SysCrd_SS,
    EID_SysCrd_SF: begin
    FBody.ReadBuffer(LoadID, 1);
    if FBody.Position - 2 - LoadID > 128 then
    begin
    FBody.Position:= FBody.Position - 2 - LoadID;
    FBody.ReadBuffer(LoadID, 1);
    if LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF] then
    begin
    FBody.Position:= SaveOffset;
    AddError(AddErrID, ErrorOffset, SaveOffset - ErrorOffset, CurSysCoord, 0, CurDisCoord, 0);
    NeedSync:= False;
    Break;
    end;
    end;
    end;

    EID_SysCrd_FS,
    EID_SysCrd_FF: begin
    FBody.ReadBuffer(LoadLongInt[1], 4);
    if FBody.Position - 5 - Abs(LoadLongInt[1]) > 128 then
    begin
    FBody.Position:= FBody.Position - 5 - Abs(LoadLongInt[1]);
    FBody.ReadBuffer(LoadID, 1);
    if LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF] then
    begin
    FBody.Position:= SaveOffset;
    AddError(AddErrID, ErrorOffset, SaveOffset - ErrorOffset, CurSysCoord, 0, CurDisCoord, 0);
    NeedSync:= False;
    Break;
    end;
    end;
    end;
    end;
    FBody.Position:= SaveOffset + 1;
    end;
    except
    //      FLog.Add(Format('AnalyzeFileBody: %x ������� ��� ������������� ����� ����', [SaveOffset]));
    LoadStopFlag:= True;
    end;

    if LoadStopFlag then goto LoadStop;

    if not NeedSync then
    begin
    LastCoordEvt:= - 1;
    FirstCoordAfterError:= True;
    goto LoadCont;
    end;
    end;
    *)
LoadStop :

  // if FLeftFlag and
  // FRightFlag then FExHeader.DataRail:= rBoth else
  // if FLeftFlag  then FExHeader.DataRail:= rLeft else
  // if FRightFlag then FExHeader.DataRail:= rRight;

  if (Length(FEvents) <> 0) and
    (FEvents[ High(FEvents)].DisCoord <> CurDisCoord) then
    AddEvent(EID_LinkPt);

  // FExHeader.EndKM:= CurrCoord.Km[1];
  // FExHeader.EndPK:= CurrCoord.Pk[1];

  if LastStolbSysCoord = 0 then // ���� ��� �� ������ ������ ����� ...
  begin
    // if Header.MoveDir > 0 then FExHeader.EndMM:= FHead.StartMetre * 1000 - (CurSysCoord - LastStolbSysCoord) * Header.ScanStep div 100
    // else FExHeader.EndMM:= FHead.StartMetre * 1000 + (CurSysCoord - LastStolbSysCoord) * Header.ScanStep div 100
  end
  else
  begin
    // if Header.MoveDir > 0 then FExHeader.EndMM:= 100000 - (CurSysCoord - LastStolbSysCoord) * Header.ScanStep div 100
    // else FExHeader.EndMM:= (CurSysCoord - LastStolbSysCoord) * Header.ScanStep div 100;
  end;

  FExHeader.EndSysCoord:= CurSysCoord;
  FExHeader.EndDisCoord:= CurDisCoord;

  FillEventsData;
  if CoordSys = csMetricRF  then FExHeader.EndMRFCrd:= DisToMRFCrd(MaxDisCoord)
                            else FExHeader.EndCaCrd:= DisToCaCrd(MaxDisCoord);


//  Log.SaveToFile('File_Id_List.txt');
//  Log.Free;
//  Log:= nil;
{$OVERFLOWCHECKS OFF}
(*
  // CheckSumm:= CheckSummStartValue;
  FBody.Position:= 0;
  for I:= 1 to 29 do
  begin
    FBody.ReadBuffer(Card, 4);
    CheckSumm:= CheckSumm + Card;
  end;
  FBody.Position:= FBody.Position + 3 * 4;
  while FBody.Position + 4 < FDataSize { FBody.Size } do
  begin
    FBody.ReadBuffer(Card, 4);
    CheckSumm:= CheckSumm + Card;
  end;
  FHead.CheckSumm:= CheckSumm and $00000000FFFFFFFF;
*)
{$OVERFLOWCHECKS ON}
  // WaitForm.Release;
  // SetPOVer(Version);
end;

procedure TAviconDataSource.ReAnalyze;
begin
  AnalyzeFileBody;
  FillEventsData;
  SaveExData(FExHdrLoadOk);
end;

// -------------< �������� ����������� ����� >------------------------------------

function TAviconDataSource.TestCheckSumm: Boolean;
var
  I: Integer;
  Card: DWord;
  CheckSumm: Int64;

begin
  (* {$OVERFLOWCHECKS OFF}
    CheckSumm:= CheckSummStartValue;
    FBody.Position:= 0;
    for I:= 1 to 29 do
    begin
    FBody.ReadBuffer(Card, 4);
    CheckSumm:= CheckSumm + Card;
    end;
    FBody.Position:= FBody.Position + 3 * 4;
    while FBody.Position + 4 < FDataSize {FBody.Size} do
    begin
    FBody.ReadBuffer(Card, 4);
    CheckSumm:= CheckSumm + Card;
    end;
    CheckSumm:= CheckSumm and $00000000FFFFFFFF;
    {$OVERFLOWCHECKS ON}

    Result:= FHead.CheckSumm = CheckSumm; *)
  Result:= True;
end;

// -------------< �������� ��� ������ >------------------------------------
(*
  function TAviconDataSource.TestOffsetFirst: Boolean;
  var
  I: Integer;

  begin
  Result:= True;
  FLastErrorIdx:= 0;
  for I:= 0 to High(FErrors) do
  begin
  if (FBody.Position < FErrors[I].Offset) then
  begin
  Result:= True;
  Break;
  end;
  if (FBody.Position >= FErrors[I].Offset) and
  (FBody.Position < FErrors[I].Offset + FErrors[I].Size) then
  case FErrors[I].Code of
  EID_BadBody,
  EID_BadLinc,
  EID_BadCoord: begin
  FBody.Position:= FErrors[I].Offset + FErrors[I].Size;
  FLastErrorIdx:= I;
  Result:= True;
  Break;
  end;
  EID_BadEnd: begin
  Result:= False;
  Break;
  end;
  end;
  end;
  end;

  function TAviconDataSource.TestOffsetNext: Boolean;
  var
  I: Integer;

  begin
  Result:= True;
  for I:= FLastErrorIdx to High(FErrors) do
  begin
  if (FBody.Position < FErrors[I].Offset) then
  begin
  FLastErrorIdx:= I;
  Result:= True;
  Break;
  end;
  if (FBody.Position >= FErrors[I].Offset) and
  (FBody.Position < FErrors[I].Offset + FErrors[I].Size) then
  case FErrors[I].Code of
  EID_BadBody,
  EID_BadLinc,
  EID_BadCoord: begin
  FBody.Position:= FErrors[I].Offset + FErrors[I].Size;
  FLastErrorIdx:= I;
  Result:= True;
  Break;
  end;
  EID_BadEnd: begin
  Result:= False;
  Break;
  end;
  end;
  end;
  end;
*)
procedure TAviconDataSource.TestBMOffsetFirst;
var
  I: Integer;

begin
  // Result:= False;

  FLastBMIdx:= 0;
  for I:= 0 to High(FBMItems) do
  begin
    if (FBody.Position < FBMItems[I].StSkipOff) then
      Break;
    if (FBody.Position >= FBMItems[I].StSkipOff) and
      (FBody.Position < FBMItems[I].OkOff) then
    begin
      FLastBMIdx:= I;
      Break;

      // FBody.Position:= FBMItems[I].OkOff;
      // FPack:= FBMItems[I].Pack;
      // Result:= True;

    end;
  end;

end;

function TAviconDataSource.TestBMOffsetNext: Boolean;
var
  I: Integer;

begin
  Result:= False;
  for I:= FLastBMIdx to High(FBMItems) do
  begin
    if (FBody.Position < FBMItems[I].StSkipOff) then
    begin
      FLastBMIdx:= I;
      Break;
    end;
    if (FBody.Position >= FBMItems[I].StSkipOff) and
      (FBody.Position < FBMItems[I].OkOff) then
    begin
      FLastBMIdx:= I;
      Result:= True;
      Break;
    end;
  end;
end;


function TAviconDataSource.FindBSState(StartDisCoord: Integer): Boolean;
begin
  if FCurDisCoord < FObmen1 then
  begin
//    FObmen2:= FPack;
    Result:= True;
  end else Result:= False;
end;
{
  procedure TAviconDataSource.TestCanSkipBM;
  begin
  if (not FCanSkipBM) or (FEvents[0].Event.ID <> EID_FwdDir) then // ���� ���� �������� ������ ������ ������ - ��� ������� ����� ���� �������� ����� - ����� ���������������
  begin
  ReAnalyze;
  FillEventsData;
  end;
  end;
}
procedure TAviconDataSource.SetSkipBM(NewState: Boolean);
{
  var
  I: Integer;
  J: Integer;
  K: Integer;
  L: Integer;
  Flag: Boolean;

  GetDisCoord: Int64;
  GetSysCoord: Integer;
  GetOffset: Integer;

  FBMItems_: array of TBMItem;
  ED_: array of TFileEventData;
  }

begin

  (*

    if NewState and (not FSkipBM) then
    begin
    TestCanSkipBM;

    {   for I:= 0 to High(FNotebook) do
    FNotebook[I].SysCoord:= DisToSysCoord(FNotebook[I].DisCoord);  }

    SetLength(FSaveEventsData, Length(FEvents)); // ��������� �������
    Move(FEvents[0], FSaveEventsData[0], Length(FEvents) * SizeOf(TFileEventData));

    SetLength(ED_, Length(FEvents));
    Move(FEvents[0], ED_[0], Length(FEvents) * SizeOf(TFileEventData));

    Flag:= False;
    SetLength(FBMItems, 0);
    SetLength(FBMItems_, 0);

    for I:= 0 to High(FEvents) do
    begin
    if (not Flag) and (FEvents[I].Event.ID = EID_BwdDir) then // ���� �������� �������� ����� - ���������� ��� ��� ��������
    begin
    Flag:= True;
    J:= FEvents[I].Event.SysCoord;
    K:= FEvents[I].Event.OffSet;
    L:= FEvents[I].Event.DisCoord;
    end;

    if Flag and (FEvents[I].Event.ID = EID_EndBM) then  // ���� �������� ����� ����������� - ��������� ������ � ���
    begin
    Flag:= False;
    SetLength(FBMItems_, Length(FBMItems_) + 1);
    FBMItems_[High(FBMItems_)].StSkipOff:= K;
    FBMItems_[High(FBMItems_)].OkOff:= FEvents[I].Event.OffSet;
    FObmen1:= FEvents[I].Event.DisCoord;
    LoadData(L, GetMaxDisCoord, 0, FindBSState);
    FBMItems_[High(FBMItems_)].Pack:= FObmen2;
    end;

    if Flag then
    begin                                             // ���� �� � ���� �������� ����� - ����� ��� ��������� �� ���������� ��� ������
    ED_[I].Event.SysCoord:= J;
    ED_[I].Event.DisCoord:= J;
    end
    else
    begin                                             // ���� �� �� � ���� �������� ����� - ����� ��� ������� ��������� �� �� ��������� ����������
    ED_[I].Event.DisCoord:= ED_[I].Event.SysCoord;
    end
    end;

    if Flag then
    begin
    SetLength(FBMItems_, Length(FBMItems_) + 1);
    FBMItems_[High(FBMItems_)].StSkipOff:= K;
    FBMItems_[High(FBMItems_)].OkOff:= Header.TableLink;
    FObmen1:= Header.TableLink;
    LoadData(L, GetMaxDisCoord, 0, FindBSState);
    FBMItems_[High(FBMItems_)].Pack:= FObmen2;
    end;

    SetLength(FBMItems, Length(FBMItems_));
    Move(FBMItems_[0], FBMItems[0], Length(FBMItems) * SizeOf(TBMItem));
    SetLength(FBMItems_, 0);

    Move(ED_[0], FEvents[0], Length(FEvents) * SizeOf(TFileEventData));
    SetLength(ED_, 0);

    FillTimeList;
    end;

    if (not NewState) and FSkipBM then
    begin
    Move(FSaveEventsData[0], FEvents[0], Length(FSaveEventsData) * SizeOf(TFileEventData));
    SetLength(FSaveEventsData, 0);
    SetLength(FBMItems, 0);
    FillTimeList;
    for I:= 0 to High(FNotebook) do
    if FNotebook[I].DisCoord = - MaxInt then
    FNotebook[I].DisCoord:= SysToDisCoord(FNotebook[I].SysCoord);
    end;

    FSkipBM:= NewState; *)
end;

// -------------< ������ � ������� >------------------------------------

function TAviconDataSource.NormalizeDisCoord(Src: Integer): Integer;
begin
  if Src < 0 then
    Result:= 0
  else if Src > MaxDisCoord then
    Result:= MaxDisCoord
  else
    Result:= Src;
end;

{
  function TAviconDataSource.GetBodyModify: Boolean;
  begin
  Result:= FExHeader.Modify = 0;
  end;

  procedure TAviconDataSource.SetBodyModify(NewState: Boolean);
  begin
  FModifyData:= True;
  case NewState of
  False: FExHeader.Modify:= $FF;
  True: FExHeader.Modify:= 0;
  end;
  end;

  procedure TAviconDataSource.SetPOVer(s: string);
  begin
  //  FExHeader.POVer[0]:= Chr(s[1]);
  //  FExHeader.POVer[1]:= Chr(s[2]);
  //  FExHeader.POVer[2]:= Chr(s[3]);
  //  FExHeader.POVer[3]:= Chr(s[4]);
  end;
}
procedure TAvk11DatSrc.FillTimeList;
var
  I: Integer;
  ID: Byte;
  OldH: Integer;
  OldM: Integer;
  pData: PEventData;

begin
  for I:= 0 to High(FEvents) do
  begin
    GetEventData(I, ID, pData);
    if ID in [EID_Time] then
    begin
      if (I = 0) or
         (pedTime(pData)^.Hour <> OldH) or
         (pedTime(pData)^.Minute <> OldM) then
      begin
        SetLength(FTimeList, Length(FTimeList) + 1);
        FTimeList[High(FTimeList)].H:= pedTime(pData)^.Hour;
        FTimeList[High(FTimeList)].M:= pedTime(pData)^.Minute;
        FTimeList[High(FTimeList)].DC:= FEvents[I].DisCoord;
        OldH:= pedTime(pData)^.Hour;
        OldM:= pedTime(pData)^.Minute;
      end;
    end;
  end;
end;

var
 QQ: Integer;

procedure TAviconDataSource.FillEventsData;
const
  KuRek: array [0 .. 9] of Integer = (14, 14, 12, 12, 16, 16, 14, 14, 16, 16);

var
  I, J: Integer;
  ID: Byte;
  Params_: TMiasParams;
//  Params_: TEvalChannelsParams;
  HSItem: THSItem;
  Echo: array [1 .. 6] of Byte;
  pData: PEventData;
  R: TRail;
  Flag: Boolean;
  LastKm: Integer;
  LastPk: Integer;
  SkipBytes: Integer;
  ReAn: Boolean;
  SkipFlag: Boolean;

  T1: TRail;
  T2: Integer;

  CheckSum: Byte;
  CheckSumData: Byte;
  CheckSumLen: Integer;

  MRFCrdPrm: TMRFCrdParams;
  CaCrdPrm: TCaCrdParams;
  MRFCrd: TMRFCrd;
  CaCrd: TCaCrd;

//  {$DEFINE LOG}

//  {$IFDEF LOG}
//  FLog: TextFile;
//  {$ENDIF}

  tmp: edAirBrush;
  EndOffset: Integer;

  I1, I2: Integer;
  Sensor1FirstFlag: array [TRail] of Boolean;

  tmp_: edAirBrush;

//  Z1, Z2, Z3: Integer;

begin
  if FFillEventsData then Exit;

//  {$IFDEF LOG}
//  AssignFile(FLog, 'Events.LOG');
//  Rewrite(FLog);
//  {$ENDIF}

  Sensor1FirstFlag[rLeft]:= True;
  Sensor1FirstFlag[rRight]:= True;

  // ������������ ������ ��������� �������� - ���������
  for R:= rLeft to rRight do
    for I:= MinEvalChannel to MaxEvalChannel do
      FillChar(Params_.Par[R, I].Ku, SizeOf(TEvalChannelParams), 0);

  for R:= rLeft to rRight do
    Params_.Sensor1[R]:= False;
  Params_.Mode:= 0;

  ReAn:= False;
  FMaxSysCoord:= - MaxInt;
  Flag:= False;
  FCanSkipBM:= False;

  SetLength(FParams, Length(FEvents));

  SkipFlag:= False;

  for I:= 0 to High(FEvents) do
  begin
    FMaxSysCoord:= Max(FMaxSysCoord, FEvents[I].SysCoord);

                                          // ���������� ����������� �������
    if not (FEvents[I].ID in [EID_FwdDir, // ������ ������� ������
                              EID_LinkPt, // ����� ��� ����� ��������� � ������� � �����
                              EID_BwdDir, // ������ ������� �����
                              EID_EndBM]) then
    begin
      if (FEvents[I].ID = EID_HandScan) then  // �������� ������ ������� ��������
      begin
        SetLength(HSList, Length(HSList) + 1);
        FileBody.Position:= FEvents[I].OffSet + 1;
        with HSList[High(HSList)] do
        begin
          EventIdx:= I;
          DisCoord:= FEvents[I].DisCoord;

          begin
            FileBody.ReadBuffer(Header, SizeOf(THSHead));
            repeat
              FileBody.ReadBuffer(HSItem, 3);
              if (Length(Samples) = 0) or
                 (HSItem.Sample <> Samples[ High(Samples)].Idx) then SetLength(Samples, Length(Samples) + 1);

              Samples[ High(Samples)].Count:= 0;
              Samples[ High(Samples)].Idx:= HSItem.Sample;

              with Samples[ High(Samples)] do
                case HSItem.Count of
                  1:begin
                      FileBody.ReadBuffer(Echo, 2);
                      if Count < 16 then Inc(Count);
                      Delay[Count]:= Echo[1];
                      Ampl[Count]:= Echo[2] shr 4 and $0F;
                    end;
                  2:begin
                      FileBody.ReadBuffer(Echo, 3);
                      if Count < 16 then Inc(Count);
                      Delay[Count]:= Echo[1];
                      Ampl[Count]:= Echo[3] shr 4 and $0F;
                      if Count < 16 then Inc(Count);
                      Delay[Count]:= Echo[2];
                      Ampl[Count]:= Echo[3] and $0F;
                    end;
                  3:begin
                      FileBody.ReadBuffer(Echo, 5);
                      if Count < 16 then Inc(Count);
                      Delay[Count]:= Echo[1];
                      Ampl[Count]:= Echo[4] shr 4 and $0F;
                      if Count < 16 then Inc(Count);
                      Delay[Count]:= Echo[2];
                      Ampl[Count]:= Echo[4] and $0F;
                      if Count < 16 then Inc(Count);
                      Delay[Count]:= Echo[3];
                      Ampl[Count]:= Echo[5] shr 4 and $0F;
                    end;
                  4:begin
                      FileBody.ReadBuffer(Echo, 6);
                      if Count < 16 then Inc(Count);
                      Delay[Count]:= Echo[1];
                      Ampl[Count]:= Echo[5] shr 4 and $0F;
                      if Count < 16 then Inc(Count);
                      Delay[Count]:= Echo[2];
                      Ampl[Count]:= Echo[5] and $0F;
                      if Count < 16 then Inc(Count);
                      Delay[Count]:= Echo[3];
                      Ampl[Count]:= Echo[6] shr 4 and $0F;
                      if Count < 16 then Inc(Count);
                      Delay[Count]:= Echo[4];
                      Ampl[Count]:= Echo[6] and $0F;
                    end;
                end;
            until HSItem.Sample = $FFFF;
          end;
        end;
      end
      else                                   // �������� ������ �������
      begin
        FBody.Position:= FEvents[I].OffSet + 1;
        if not LoadEventData(FEvents[I].ID, @FEvents[I].Data) then break; // xxxxx
      end;
    end;

{
    if FEvents[I].Event.ID = EID_Mode then // ��������� ������
    begin
      if FEvents[I].Data[1] in [60 .. 66, 70 .. 76] then
        SkipFlag:= True;
      if FEvents[I].Data[1] in [1, 2, 7, 12, 20 .. 59] then
        SkipFlag:= False;
    end;
}
//    if (not SkipFlag) or (SkipFlag and not(FEvents[I].Event.ID in [EID_Sens,
  //      EID_Att, EID_VRU, EID_StStr, EID_EndStr, EID_2Tp])) then



//      if (FEvents[I].Data[2] >= MinEvalChannel) and
//         (FEvents[I].Data[2] <= MaxEvalChannel) then


    {  if FEvents[I].ID in [        EID_Ku,
                                  EID_Att,
                                  EID_TVG,
                                EID_StStr,
                               EID_EndStr,
                           EID_PrismDelay   ]  then


   //   with FEvents[I] do
     //   if Data[2] > 64 then ShowMessage('!!!');  }
      with FEvents[I] do
        case ID of
           EID_Ku: Params_.Par[TRail(Data[1]), Data[2]].Ku:= ShortInt(Data[3]);
          EID_Att: Params_.Par[TRail(Data[1]), Data[2]].Att:= Data[3];
          EID_TVG: Params_.Par[TRail(Data[1]), Data[2]].TVG:= Data[3];
        EID_StStr:
                   Params_.Par[TRail(Data[1]), Data[2]].StStr:= Data[3];
       EID_EndStr: begin
                     Params_.Par[TRail(Data[1]), Data[2]].EndStr:= Data[3];
                   end;
   EID_PrismDelay: Params_.Par[TRail(Data[1]), Data[2]].PrismDelay:= Data[3] + Data[4] * $100;
         EID_Mode: begin


{                     I1:=
                     FEvents[I].
}

                     Params_.Mode:= Data[1] + Data[2] * $100;
                     Params_.Channel:= Boolean(Data[3]); // ���� ������� � ��� ��� ���� ������� ���� � ����� ������
                     Params_.Rail:= TRail(Data[4]);      // ����
                     Params_.EvalChNum:= Data[5];  // ����� ������ ������
                     Params_.Time:= Data[6] + Data[7] * $100;       // ����� ���������� � ���������� ������ (���)

                   end;
      EID_Sensor1: begin

                     Sensor1DataExists[TRail(Data[1])]:= True;

                     if (Sensor1FirstFlag[TRail(Data[1])]) then
                     begin
                        StartFileSensor1[TRail(Data[1])]:= not(Boolean(Data[2]));
                        Sensor1FirstFlag[TRail(Data[1])]:= False;
                     end;

                     Params_.Sensor1[TRail(Data[1])]:= Boolean(Data[2]);
//                     {$IFDEF LOG}
//                     Writeln(FLog, Format('State: %d; DisCoord: %d; Rail: %d', [Data[2], FEvents[I].DisCoord, Data[1]]) );
//                     if Data[2] = 0 then Writeln(FLog, '');
//                     {$ENDIF}
                   end;
        end;

    FParams[I]:= Params_;
  end;

//  {$IFDEF LOG}
//  System.CloseFile(FLog);
//  {$ENDIF}

  if not Flag then FCanSkipBM:= True;

  SetLength(FCDList, 0);               // ������ ������� ��������� ����������� ��������
  for I:= 0 to High(FEvents) do
  begin
    if (FEvents[I].ID = EID_FwdDir) or (FEvents[I].ID = EID_BwdDir) then
    begin
      SetLength(FCDList, Length(FCDList) + 1);
      FCDList[ High(FCDList)]:= I;
    end;
  end;
  SetLength(FCDList, Length(FCDList) + 1);
  FCDList[ High(FCDList)]:= High(FEvents);

  FillTimeList;


  for I:= 0 to High(FEvents) do
  begin

    // ------------------------------------- // ������ ������� ������������

    if (FEvents[I].ID = EID_AirBrush) then
    begin
      GetEventData(I, ID, pData);
      if pedAirBrush(pData)^.DisCoord = - 1000000 then // HC data
      begin
        tmp:= pedAirBrush(pData)^;
        SetLength(FHCList, Length(FHCList) + 1);
        FHCList[High(FHCList)].Ch:= tmp.Items[0].ScanCh;
        FHCList[High(FHCList)].Rail:= tmp.Rail;
        FHCList[High(FHCList)].StartDisCoord:= tmp.Items[0].StartDisCoord;
        FHCList[High(FHCList)].EndDisCoord:= tmp.Items[0].EndDisCoord;
        FHCList[High(FHCList)].Valut:= Round(PSmallint(@tmp.Items[0].StartDelay)^);
      end
      else
      if pedAirBrush(pData)^.Count <> 0 then   // ��� ����������� ���������� ������� ������������ �� ������
      begin
        SetLength(FAirBrushList, Length(FAirBrushList) + 1);
        tmp_:= pedAirBrush(pData)^;                          // �������������� �� ������� ������� � ����� ������
        FAirBrushList[High(FAirBrushList)].Rail:= tmp_.Rail;
        FAirBrushList[High(FAirBrushList)].DisCoord:= tmp_.DisCoord;
        FAirBrushList[High(FAirBrushList)].Count:= tmp_.Count;

        for J:= 0 to 15 do
        begin
          FAirBrushList[High(FAirBrushList)].Items[J].ScanCh:= tmp_.Items[J].ScanCh;
          FAirBrushList[High(FAirBrushList)].Items[J].StartDisCoord:= tmp_.Items[J].StartDisCoord;
          FAirBrushList[High(FAirBrushList)].Items[J].EndDisCoord:= tmp_.Items[J].EndDisCoord;
          FAirBrushList[High(FAirBrushList)].Items[J].StartDelay:= tmp_.Items[J].StartDelay;
          FAirBrushList[High(FAirBrushList)].Items[J].EndDelay:= tmp_.Items[J].EndDelay;
          FAirBrushList[High(FAirBrushList)].Items[J].Debug:= - 1;
        end;
      end;
    end;

    // ������ 2

    if (FEvents[I].ID = EID_AirBrush2) then
    begin
      GetEventData(I, ID, pData);
      if pedAirBrush2(pData)^.Count <> 0 then   // ��� ����������� ���������� ������� ������������ �� ������
      begin
        SetLength(FAirBrushList, Length(FAirBrushList) + 1);
        FAirBrushList[High(FAirBrushList)]:= pedAirBrush2(pData)^;
	    end;
    end;

    // ------------------------------------- ������ ����������� �������
    if (FEvents[I].ID = EID_LongLabel) then
    begin
      GetEventData(I, ID, pData);
      SetLength(FLongLabelList, Length(FLongLabelList) + 1);
      FLongLabelList[High(FLongLabelList)]:= pLongLabel(pData)^;
    end;

    // ------------------------------------- �����������
    if (FEvents[I].ID = EID_Media) then
    begin
      GetEventData(I, ID, pData);
      SetLength(FMediaList, Length(FMediaList) + 1);
      FMediaList[High(FMediaList)].DataType:= pedMediaData(pData)^.DataType;
      FMediaList[High(FMediaList)].Data:= TMemoryStream.Create();
      FMediaList[High(FMediaList)].EventIdx:= I;
      FileBody.Position:= FEvents[I].OffSet + 6;
      FMediaList[High(FMediaList)].Data.CopyFrom(FileBody, pedMediaData(pData)^.Size);
    end;

    // ------------------------------------- // ������ ����� ������� ������ �������� 0 ����
   // EID_ZerroProbMode

  end;





  // ------------------------------------- // ������ ����� ����������� �����
(*
  for I:= 0 to High(FEvents) do
  begin
    if (FEvents[I].ID = EID_CheckSum) then
    begin
      GetEventData(I, ID, pData);
      CheckSumLen:= FEvents[I].OffSet - pedCheckSum(pData)^.Offset;
      FileBody.Position:= pedCheckSum(pData)^.Offset;
      CheckSum:= 0;
      {$OVERFLOWCHECKS OFF}
      {$RANGECHECKS OFF}
      for J:= 0 to CheckSumLen - 1 do
      begin
        FileBody.ReadBuffer(CheckSumData, 1);
        CheckSum:= CheckSum + CheckSumData;
      end;
      {$RANGECHECKS ON}
      {$OVERFLOWCHECKS ON}
      if pedCheckSum(pData)^.Checksum <> CheckSum then ShowMessage('!');
    end;
  end;
 *)
  // ----------------------------------------------------------------------------------

  SetLength(FCoordList, 1);
  FCoordList[0].KM:= Header.StartKM;
  with FCoordList[High(FCoordList)] do
  begin
    SetLength(Content, Length(Content) + 1);
    Content[High(Content)].Pk:= Header.StartPk;
    Content[High(Content)].Idx:= 0;
    LastKm:= High(FCoordList);
    LastPk:= High(Content);
  end;

  LastKm:= 0;
  LastPk:= 0;

  for I:= 0 to EventCount - 1 do
  begin
    GetEventData(I, ID, pData);
    if ID = EID_Stolb then
    begin
      if pCoordPost(pData)^.Km[0] <> pCoordPost(pData)^.Km[1] then
      begin
        SetLength(FCoordList, Length(FCoordList) + 1);
        FCoordList[High(FCoordList)].KM:= pCoordPost(pData)^.Km[1];
        with FCoordList[High(FCoordList)] do
        begin
          SetLength(Content, Length(Content) + 1);
          if Header.MoveDir < 0 then Content[High(Content)].Pk:= 10
                                else Content[High(Content)].Pk:= 1;
          Content[High(Content)].Idx:= I;
          FCoordList[LastKm].Content[LastPk].Len:= Round((Event[I].SysCoord - Event[FCoordList[LastKm].Content[LastPk].Idx].SysCoord) * Header.ScanStep / 100);
          LastKm:= High(FCoordList);
          LastPk:= High(Content);
        end;
      end;
      if pCoordPost(pData)^.Km[0] = pCoordPost(pData)^.Km[1] then
        with FCoordList[High(FCoordList)] do
        begin
          SetLength(Content, Length(Content) + 1);
          Content[High(Content)].Pk:= pCoordPost(pData)^.Pk[1];
          Content[High(Content)].Idx:= I;
          FCoordList[LastKm].Content[LastPk].Len:= Round((Event[I].SysCoord - Event[FCoordList[LastKm].Content[LastPk].Idx].SysCoord) * Header.ScanStep / 100);
          LastKm:= High(FCoordList);
          LastPk:= High(Content);
        end;
    end;
  end;


{  Z1:= DisToSysCoord(MaxDisCoord);
  Z2:= Event[FCoordList[LastKm].Content[LastPk].Idx].SysCoord;
  Z3:= Round((Z1 - Z2) / 100 * Header.ScanStep);
  FCoordList[LastKm].Content[LastPk].Len:= Z3;   }

  FCoordList[LastKm].Content[LastPk].Len:= Round((DisToSysCoord(MaxDisCoord) - Event[FCoordList[LastKm].Content[LastPk].Idx].SysCoord) / 100 * Header.ScanStep);

  SetLength(LabelList, 0);
  for I:= 0 to EventCount - 1 do
    if Event[I].ID in [EID_Stolb, EID_StolbChainage, EID_Switch, EID_DefLabel, EID_TextLabel, EID_AirBrushTempOff, EID_ZerroProbMode, EID_AlarmTempOff] then
    begin
      SetLength(LabelList, Length(LabelList) + 1);
      LabelList[High(LabelList)]:= I;
    end;

  if ReAn then
  begin
    ReAnalyze;
    FillEventsData;
  end;

  FFillEventsData:= true;
end;

procedure TAviconDataSource.DisToFileOffset(NeedDisCoord: Integer; var DisCoord: Int64; var SysCoord: Integer; var OffSet: Integer);
var
  R: TRail;
  I: Integer;
  LastSysCoord: Integer;
  SaveOffset: Integer;
  SkipBytes: Integer;
  CurSysCoord: Integer;
  CurDisCoord: Int64;
  LoadID: Byte;
  LoadByte: array [1 .. 22] of Byte;
  LoadLongInt: array [1 .. 5] of Longint; // absolute LoadByte[1];
  HSHead: THSHead;
  HSItem: THSItem;
  StartIdx, EndIdx: Integer;
  DisCoord_: Integer;
  SysCoord_: Integer;
  Offset_: Integer;
  CurDisCoord_: Integer;
  CurSysCoord_: Integer;
  CurPosition_: Integer;
//  BSHead: mBSAmplHead;

  SameCoord: TIntegerDynArray;

  BMSkipFlag: Boolean;
  TMP: Boolean;

begin
  // ClearLog;
  NeedDisCoord:= NormalizeDisCoord(NeedDisCoord);
  // GetEventIdx(NeedDisCoord, NeedDisCoord, StartIdx, EndIdx);

  // ���� �� ���������� ������ � �����
  // ��� ����� - ������� ��������� ������� � ������ ��� ������� �����������
  // ToLog(Format('NeedDisCoord: %d', [NeedDisCoord]));                              // ----------------------------------------------------------------------------------------
  StartIdx:= GetLeftEventIdx(NeedDisCoord);

  I:= FEvents[StartIdx].DisCoord;
  while (I = FEvents[StartIdx].DisCoord) and (StartIdx <> 0) do Dec(StartIdx);
  {
    for I:= 1 to 5 do                               // ������ ����� �� ������� � ������ ��������
    begin
    if (StartIdx <> 0) and
    ((FEvents[StartIdx].Event.Pack[rLeft].Ampl = $FF) or
    (FEvents[StartIdx].Event.Pack[rRight].Ampl = $FF)) then Dec(StartIdx)
    else Break;
    end; }
  // ToLog(Format('Event Idx: %d', [StartIdx]));                              // ----------------------------------------------------------------------------------------

  (*
    // ���� ����� ������ ������ ������ ������ ��� �������� ����������
    // ��� ����� ����������� ��� ������� � ������ �����������
    while (NeedDisCoord = FEvents[StartIdx].Event.DisCoord) and (StartIdx <> 0) do Dec(StartIdx);
    I:= FEvents[StartIdx].Event.DisCoord;
    while (I = FEvents[StartIdx].Event.DisCoord) and (StartIdx <> 0) do Dec(StartIdx);
  *)
  // StartIdx:= GetLeftEventIdx(NeedDisCoord - 1);

  // if StartIdx > 0 then Dec(StartIdx);
  // if StartIdx > 0 then Dec(StartIdx);

  // GetNearestEventIdx(NeedDisCoord, StartIdx, EndIdx, SameCoord);
  // if Length(SameCoord) <> 0 then StartIdx:= SameCoord[0];

  if StartIdx = -1 then
  begin
    DisCoord:= 0;
    SysCoord:= 0;
    OffSet:= 0;
    Exit;
  end;

  // StartIdx:= GetLeftEventIdx(0);

  FBody.Position:= FEvents[StartIdx].OffSet;
  LastSysCoord:= FEvents[StartIdx].SysCoord;
  CurSysCoord:= FEvents[StartIdx].SysCoord;
  CurDisCoord:= FEvents[StartIdx].DisCoord;
  // FPack:= FEvents[StartIdx].Event.Pack;

  // ToLog(Format('Start Event: %d', [StartIdx]));                              // ----------------------------------------------------------------------------------------
  // ToLog(Format('Start State Rail: 0, BS: %d', [Ord(FPack[r_Left].Ampl <> 255)]));  // ----------------------------------------------------------------------------------------
  // ToLog(Format('Start State Rail: 1, BS: %d', [Ord(FPack[r_Right].Ampl <> 255)])); // ----------------------------------------------------------------------------------------

  FillChar(FCurEcho, SizeOf(TCurEcho), 0);

  if CurDisCoord >= NeedDisCoord then
  begin
    DisCoord:= CurDisCoord;
    SysCoord:= CurSysCoord;
    OffSet:= FBody.Position;
    Exit;
  end;

//  TestOffsetFirst;
  if FSkipBM then TestBMOffsetFirst;
{
  for R:= rLeft to rRight do
  begin
    FCurEcho[R, 1].Count:= Ord(FPack[R].Ampl <> 255);
    FCurEcho[R, 1].Delay[1]:= FPack[R].Delay;
    FCurEcho[R, 1].Ampl[1]:= FPack[R].Ampl;
  end;
}
  BMSkipFlag:= False;
  repeat
//    if not TestOffsetNext then Exit;
    if FSkipBM then
    begin
      TMP:= BMSkipFlag;
      BMSkipFlag:= TestBMOffsetNext;
      if TMP and (not BMSkipFlag) then
        for R:= rLeft to rRight do
          FCurEcho[R, 1].Count:= 0;
    end;

    SaveOffset:= FBody.Position;

    CurDisCoord_:= CurDisCoord;
    CurSysCoord_:= CurSysCoord;
    CurPosition_:= FBody.Position;

    FBody.ReadBuffer(LoadID, SizeOf(LoadID));
(*    SkipBytes:= -1;
    if LoadID and 128 = 0 then
    begin
      if LoadID shr 3 and $07 = 1 then // ���� ������ ����� �� �������������
      begin
        FSrcDat[0]:= LoadID;
        FBody.ReadBuffer(FSrcDat[1], Avk11EchoLen[LoadID and $07]);
        DataToEcho;
      end
      else
      begin
        if LoadID shr 3 and $07 = 0 then
          SkipBytes:= 1 // ���� ������� �� ���������� 1 ����
        else
          SkipBytes:= Avk11EchoLen[LoadID and $07]; // ����� ����������
      end
    end
    else
    begin
      if not BMSkipFlag then
          *)
      begin
        case LoadID of
                         (*
          EID_HandScan:
            begin // ������
              FBody.ReadBuffer(HSHead, 6);
              repeat
                FBody.ReadBuffer(HSItem, 3);
                if HSItem.Count <= 4 then
                  FBody.ReadBuffer(HSItem.Data[0], Avk11EchoLen[HSItem.Count]);
              until HSItem.Sample = $FFFF;
            end;

          EID_NewHeader:
            begin
              FBody.ReadBuffer(SkipBytes, 4);
              FBody.Position:= FBody.Position + SkipBytes;
              SkipBytes:= -1;
            end;
          {
            EID_BSAmpl:    begin
            FBody.ReadBuffer(BSHead, SizeOf(mBSAmplHead));
            FBody.Position:= FBody.Position + BSHead.Len * 2;
            SkipBytes:= - 1;
            end;
          }
          EID_ACLeft, EID_ACRight, EID_Sens, EID_Att, EID_VRU, EID_StStr,
            EID_EndStr, EID_HeadPh, EID_Mode, EID_2Tp_Word, EID_2Tp,
            EID_ZondImp, EID_SetRailType, EID_Stolb, EID_Strelka, EID_DefLabel,
            EID_TextLabel, EID_StBoltStyk, EID_EndBoltStyk, EID_Time,
            EID_StLineLabel, EID_EndLineLabel, EID_StSwarStyk, EID_EndSwarStyk,
            EID_OpChange, EID_PathChange, EID_MovDirChange, EID_SezdNo,
            EID_LongLabel, EID_GPSCoord, EID_GPSState, EID_GPSError:
            SkipBytes:= Avk11EventLen[LoadID]; *)

          EID_SysCrd_SS:
            begin // ��������� ���������� �������� ������, �������� ����������
              FBody.ReadBuffer(LoadByte[1], 2);
              CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00)
                  or LoadByte[2]);
            end;
          EID_SysCrd_SF:
            begin // ��������� ���������� �������� ������, ������ ����������
              FBody.ReadBuffer(LoadByte[1], 1);
              FBody.ReadBuffer(LoadLongInt, 4);
              CurSysCoord:= LoadLongInt[1];
            end;
          EID_SysCrd_FS:
            begin // ��������� ���������� ������ ������, �������� ����������
              FBody.ReadBuffer(LoadByte[1], 5);
              CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00)
                  or LoadByte[5]);
            end;
          EID_SysCrd_FF:
            begin // ��������� ���������� ������ ������, ������ ����������
              FBody.ReadBuffer(LoadByte[1], 4);
              FBody.ReadBuffer(LoadLongInt, 4);
              CurSysCoord:= LoadLongInt[1];
            end;
          EID_SysCrd_NS:
            begin // ��������� ���������� ������ ������, �������� ����������
              FBody.ReadBuffer(LoadByte[1], 1);
              CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[1]);
            end;
          EID_SysCrd_NF:
            begin // ��������� ���������� ������ ������, ������ ����������
              FBody.ReadBuffer(LoadLongInt, 4);
              CurSysCoord:= LoadLongInt[1];
            end;
          EID_EndFile:
            begin // ����� �����
              DisCoord:= CurDisCoord;
              SysCoord:= CurSysCoord;
              OffSet:= FBody.Position;
              Exit;
            end
          else if not SkipEvent(LoadID) { LoadID and 128 = 0 } then ;// �������
        end

      end;
{      else
      begin
        SkipBytes:= Avk11EventLen[LoadID];
        if LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF] then UnPackEcho;
      end;
    end;
 }
//    if SkipBytes <> -1 then
//      FBody.Position:= FBody.Position + SkipBytes;

    if (not BMSkipFlag) and (LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF, EID_SysCrd_NS, EID_SysCrd_NF]) then
    begin
      Inc(CurDisCoord, Abs(CurSysCoord - LastSysCoord)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)
      LastSysCoord:= CurSysCoord;

      { if not SkipUnPack then }
//      UnPackEcho;
      // SkipUnPack:= False;

      if CurDisCoord > NeedDisCoord then
      begin
        DisCoord:= CurDisCoord_;
        SysCoord:= CurSysCoord_;
        OffSet:= CurPosition_;
        FBody.Position:= OffSet;
        // ToLog(Format('EndState Rail: 0, BS: %d', [Ord(FPack[r_Left].Ampl <> 255)]));  // ----------------------------------------------------------------------------------------
        // ToLog(Format('EndState Rail: 1, BS: %d', [Ord(FPack[r_Right].Ampl <> 255)])); // ----------------------------------------------------------------------------------------
        Break;
      end
      else if CurDisCoord = NeedDisCoord then
      begin
        DisCoord:= CurDisCoord;
        SysCoord:= CurSysCoord;
        OffSet:= FBody.Position;
        // ToLog(Format('EndState Rail: 0, BS: %d', [Ord(FPack[r_Left].Ampl <> 255)]));  // ----------------------------------------------------------------------------------------
        // ToLog(Format('EndState Rail: 1, BS: %d', [Ord(FPack[r_Right].Ampl <> 255)])); // ----------------------------------------------------------------------------------------
        Break;
      end
    end;
  until False;
end;

procedure TAviconDataSource.LoadData(StartDisCoord, EndDisCoord, LoadShift: Longint; BlockOk: TDataNotifyEvent);
var
  R: TRail;
  LoadID, Dat: Byte;
  I, J, SkipBytes, OffSet, Ch, T: Integer;
  LastSysCoord: Longint;
  LastDisCoord: Longint;
  SaveLastSysCoord: Longint;
  LoadByte: array [1 .. 22] of Byte;
  LoadLongInt: array [1 .. 5] of Longint; // absolute LoadByte[1];
  HSHead: THSHead;
  HSItem: THSItem;
  CurEcho_: TCurEcho;
  NewSysCoord: Longint;
  BMSkipFlag: Boolean;
  TMP: Boolean;
  NeedDisCoord: Integer;
  StartIdx: Integer;
  SavePos: Integer;
//  BSHead: mBSAmplHead;

begin
  // ClearLog;
  // ToLog(Format('Need DisCoord %d', [StartDisCoord])); // ----------------------------------------------------------------------------------------
  {
  for R:= rLeft to rRight do
  begin
    for I:= 2 to 7 do
    begin
      FCurEcho[R, I].Count:= 0;
      FCurEcho[R, I].ZerroFlag:= False;
    end;
    CurAmpl[R]:= -1;
  end;
  }
  for R:= FConfig.MinRail to FConfig.MaxRail do // ������� ������� ������� ��������
    for I:= FConfig.MinScanChNumber to FConfig.MaxScanChNumber do
    begin
      FCurEcho[R, I].Count:= 0;
//      FCurEcho[R, I].ZerroFlag:= False;
//      FCurEcho[R, I].ZerroFlag2:= False;
    end;



  //DisToFileOffset(StartDisCoord - LoadShift, FCurDisCoord, FCurSysCoord, OffSet);

  NeedDisCoord:= NormalizeDisCoord(StartDisCoord - LoadShift);
  // GetEventIdx(NeedDisCoord, NeedDisCoord, StartIdx, EndIdx);

  // ���� �� ���������� ������ � �����
  // ��� ����� - ������� ��������� ������� � ������ ��� ������� �����������
  // ToLog(Format('NeedDisCoord: %d', [NeedDisCoord]));                              // ----------------------------------------------------------------------------------------
  StartIdx:= GetLeftEventIdx(NeedDisCoord);

  I:= FEvents[StartIdx].DisCoord;
  while (I = FEvents[StartIdx].DisCoord) and (StartIdx <> 0) do Dec(StartIdx);

  FCurDisCoord:= FEvents[StartIdx].DisCoord;
  FCurSysCoord:= FEvents[StartIdx].SysCoord;
  OffSet:= FEvents[StartIdx].OffSet;

{
  for R:= rLeft to rRight do
  begin
    if not UnpackBottom then
      FCurEcho[R, 1].Ampl[1]:= $FF;

    FCurEcho[R, 1].Count:= Ord(FPack[R].Ampl <> 255);
    FCurEcho[R, 1].Delay[1]:= FPack[R].Delay;
    FCurEcho[R, 1].Ampl[1]:= FPack[R].Ampl;


    // ToLog(Format('Rail: %d, BS: %d', [Ord(R), Ord(FPack[R].Ampl <> 255)])); // ----------------------------------------------------------------------------------------

  end;
}
  // ToLog(Format('Get DisCoord %d', [FCurDisCoord])); // ----------------------------------------------------------------------------------------
  // ToLog(Format('Start Offset %d', [Offset])); // ----------------------------------------------------------------------------------------

  LastSysCoord:= FCurSysCoord;
  FBody.Position:= OffSet;

  GetParamFirst(StartDisCoord - LoadShift, FCurParams);
//  if not TestOffsetFirst then Exit;
  if FSkipBM then TestBMOffsetFirst;
  BMSkipFlag:= False;
  repeat
    CurCrdLongLinc:= False;
//    if not TestOffsetNext then Exit;
    if FSkipBM then
    begin
      TMP:= BMSkipFlag;
      BMSkipFlag:= TestBMOffsetNext;
      if TMP and (not BMSkipFlag) then
        for R:= rLeft to rRight do
          for I:= 1 to 7 do
          begin
            FCurEcho[R, I].Count:= 0;
          //  FCurEcho[R, I].ZerroFlag:= False;
          end;
    end;

    FCurOffset:= FBody.Position;
    SkipBytes:= -1;

    if Header.TableLink <> 0 then
      if FBody.Position >= Header.TableLink then
      begin
//       showmessage('!');
       Exit;

      end;

    FBody.ReadBuffer(LoadID, 1);
(*
    if LoadID and 128 = 0 then
    begin
      FSrcDat[0]:= LoadID;

      Ch:= LoadID shr 3 and 7;
      if Ch = 0 then
      begin
        FBody.ReadBuffer(Dat, 1);
        CurAmpl[SideToRail(TSide(LoadID shr 6 and 1))]:= Dat;
      end
      else
      begin
        FBody.ReadBuffer(FSrcDat[1], Avk11EchoLen[LoadID and $07]);
        if Ch = 1 then
          DataToEcho;
        with FCurEcho[SideToRail(TSide(LoadID shr 6 and 1)), Ch] do
          case LoadID and $07 of
            0:
              ZerroFlag:= True;
            1:
              begin
                if Count < 16 then
                  Inc(Count);
                Delay[Count]:= FSrcDat[1];
                Ampl[Count]:= FSrcDat[2] shr 4 and $0F;
              end;
            2:
              begin
                if Count < 16 then
                  Inc(Count);
                Delay[Count]:= FSrcDat[1];
                Ampl[Count]:= FSrcDat[3] shr 4 and $0F;
                if Count < 16 then
                  Inc(Count);
                Delay[Count]:= FSrcDat[2];
                Ampl[Count]:= FSrcDat[3] and $0F;
              end;
            3:
              begin
                if Count < 16 then
                  Inc(Count);
                Delay[Count]:= FSrcDat[1];
                Ampl[Count]:= FSrcDat[4] shr 4 and $0F;
                if Count < 16 then
                  Inc(Count);
                Delay[Count]:= FSrcDat[2];
                Ampl[Count]:= FSrcDat[4] and $0F;
                if Count < 16 then
                  Inc(Count);
                Delay[Count]:= FSrcDat[3];
                Ampl[Count]:= FSrcDat[5] shr 4 and $0F;
              end;
            4:
              begin
                if Count < 16 then
                  Inc(Count);
                Delay[Count]:= FSrcDat[1];
                Ampl[Count]:= FSrcDat[5] shr 4 and $0F;
                if Count < 16 then
                  Inc(Count);
                Delay[Count]:= FSrcDat[2];
                Ampl[Count]:= FSrcDat[5] and $0F;
                if Count < 16 then
                  Inc(Count);
                Delay[Count]:= FSrcDat[3];
                Ampl[Count]:= FSrcDat[6] shr 4 and $0F;
                if Count < 16 then
                  Inc(Count);
                Delay[Count]:= FSrcDat[4];
                Ampl[Count]:= FSrcDat[6] and $0F;
              end;
          end;
      end;
      {
        if Ch = 1 then
        ToLog(Format('Crd: %d; R: %d; Ch: 1 Echo: %d', [FCurDisCoord, Ord(SideToRail(TSide(LoadID shr 6 and 1))), CurEcho[SideToRail(TSide(LoadID shr 6 and 1)), Ch].Count])); // ----------------------------------------------------------------------------------------
      }
    end *)
    if LoadID and 128 = 0 then
    begin
      FSrcDat[0]:= LoadID;

      Ch:= (LoadID shr 2) and 15;
(*      if Ch = 0 then
      begin
        FFileBody.ReadBuffer(Dat, 1);
        CurAmpl[SideToRail(TSide(LoadID shr 6 and 1))]:= Dat;
      end
      else  *)
      begin

        if FExtendedChannels and (Ch > 14) then
        begin
          FBody.ReadBuffer(Ch, 1);
          Ch:= Ch + 15;
        end;
        SavePos:= FBody.Position;
        FBody.ReadBuffer(FSrcDat[1], EchoLen[LoadID and 3]);

//        if Ch = 1 then DataToEcho;
//        if (Ch < 0) or (Ch > 7) then showmessage('!');
        with FCurEcho[TRail(LoadID shr 6 and 1), Ch] do
        begin
        {  if ZerroFlag then
          begin
            Count:= 0;
            ZerroFlag:= False;
            ZerroFlag2:= False;
          end;
         }
          case LoadID and $03 of
//            0: ZerroFlag:= True;
            0: //if FSrcDat[1] <> $FF then
               begin
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[1];
                 DelayOffset[Count]:= SavePos;
                 Ampl[Count]:= FSrcDat[2] shr 4 and $0F;
               end; // else ZerroFlag2:= True;
            1: begin
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[1];
                 DelayOffset[Count]:= SavePos;
                 Ampl[Count]:= FSrcDat[3] shr 4 and $0F;

                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[2];
                 DelayOffset[Count]:= SavePos + 1;
                 Ampl[Count]:= FSrcDat[3] and $0F;
//                 ZerroFlag2:= False;
               end;
            2: begin
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[1];
                 DelayOffset[Count]:= SavePos;
                 Ampl[Count]:= FSrcDat[4] shr 4 and $0F;

                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[2];
                 DelayOffset[Count]:= SavePos + 1;
                 Ampl[Count]:= FSrcDat[4] and $0F;

                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[3];
                 DelayOffset[Count]:= SavePos + 2;
                 Ampl[Count]:= FSrcDat[5] shr 4 and $0F;
//                 ZerroFlag2:= False;
               end;
            3: begin
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[1];
                 DelayOffset[Count]:= SavePos;
                 Ampl[Count]:= FSrcDat[5] shr 4 and $0F;

                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[2];
                 DelayOffset[Count]:= SavePos + 1;
                 Ampl[Count]:= FSrcDat[5] and $0F;

                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[3];
                 DelayOffset[Count]:= SavePos + 2;
                 Ampl[Count]:= FSrcDat[6] shr 4 and $0F;

                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[4];
                 DelayOffset[Count]:= SavePos + 3;
                 Ampl[Count]:= FSrcDat[6] and $0F;
//                 ZerroFlag2:= False;
               end;
          end;
        end;
      end;
{
      if Ch = 1 then
        ToLog(Format('Crd: %d; R: %d; Ch: 1 Echo: %d', [FCurDisCoord, Ord(SideToRail(TSide(LoadID shr 6 and 1))), CurEcho[SideToRail(TSide(LoadID shr 6 and 1)), Ch].Count])); // ----------------------------------------------------------------------------------------
}
    end
    else
    begin
//      if not BMSkipFlag then
      begin
        case LoadID of
         (*
          EID_HandScan:
            begin // ������
              FBody.ReadBuffer(HSHead, 6);
              repeat
                FBody.ReadBuffer(HSItem, 3);
                if HSItem.Count <= 4 then
                  FBody.ReadBuffer(HSItem.Data[0], EchoLen[HSItem.Count]);
              until HSItem.Sample = $FFFF;
            end;

          EID_NewHeader:
            if FBody.Position < 128 + 128 then
            begin
              FBody.ReadBuffer(SkipBytes, 4);
              FBody.Position:= FBody.Position + SkipBytes;
              SkipBytes:= -1;
            end;
          {
            EID_BSAmpl:    begin
            FBody.ReadBuffer(BSHead, SizeOf(mBSAmplHead));
            FBody.Position:= FBody.Position + BSHead.Len * 2;
            SkipBytes:= - 1;
            end;
          }
          EID_Sens, EID_Att, EID_VRU, EID_StStr, EID_EndStr, EID_HeadPh,
            EID_Mode, EID_2Tp_Word, EID_2Tp, EID_ZondImp, EID_SetRailType,
            EID_Stolb, EID_Strelka, EID_DefLabel, EID_TextLabel,
            EID_StBoltStyk, EID_EndBoltStyk, EID_Time, EID_StLineLabel,
            EID_EndLineLabel, EID_StSwarStyk, EID_EndSwarStyk, EID_OpChange,
            EID_PathChange, EID_MovDirChange, EID_SezdNo, EID_LongLabel,
            EID_GPSCoord, EID_GPSState, EID_GPSError:
            SkipBytes:= Avk11EventLen[LoadID];

          EID_ACLeft:
            begin
              FBody.ReadBuffer(LoadByte[1], 1);
              CurAK[SideToRail(sLeft), 1]:= LoadByte[1] and 1 <> 0;
              CurAK[SideToRail(sLeft), 2]:= LoadByte[1] and 2 <> 0;
              CurAK[SideToRail(sLeft), 3]:= LoadByte[1] and 4 <> 0;
              CurAK[SideToRail(sLeft), 4]:= LoadByte[1] and 8 <> 0;
              CurAK[SideToRail(sLeft), 5]:= LoadByte[1] and 16 <> 0;
              CurAK[SideToRail(sLeft), 6]:= LoadByte[1] and 32 <> 0;
              CurAK[SideToRail(sLeft), 7]:= LoadByte[1] and 64 <> 0;
              CurAK[SideToRail(sLeft), 8]:= LoadByte[1] and 128 <> 0;
            end;
          EID_ACRight:
            begin
              FBody.ReadBuffer(LoadByte[1], 1);
              CurAK[SideToRail(sRight), 1]:= LoadByte[1] and 1 <> 0;
              CurAK[SideToRail(sRight), 2]:= LoadByte[1] and 2 <> 0;
              CurAK[SideToRail(sRight), 3]:= LoadByte[1] and 4 <> 0;
              CurAK[SideToRail(sRight), 4]:= LoadByte[1] and 8 <> 0;
              CurAK[SideToRail(sRight), 5]:= LoadByte[1] and 16 <> 0;
              CurAK[SideToRail(sRight), 6]:= LoadByte[1] and 32 <> 0;
              CurAK[SideToRail(sRight), 7]:= LoadByte[1] and 64 <> 0;
              CurAK[SideToRail(sRight), 8]:= LoadByte[1] and 128 <> 0;
            end; *)
          EID_SysCrd_SS:
            begin // ��������� ���������� �������� ������, �������� ����������
              FBody.ReadBuffer(LoadByte[1], 2);
              NewSysCoord:= Integer((Integer(FCurSysCoord) and $FFFFFF00)
                  or LoadByte[2]);
            end;
          EID_SysCrd_SF:
            begin // ��������� ���������� �������� ������, ������ ����������
              FBody.ReadBuffer(LoadByte[1], 1);
              FBody.ReadBuffer(LoadLongInt, 4);
              NewSysCoord:= LoadLongInt[1];
            end;
          EID_SysCrd_FS:
            begin // ��������� ���������� ������ ������, �������� ����������
              CurCrdLongLinc:= True;
              FBody.ReadBuffer(LoadByte[1], 5);
              NewSysCoord:= Integer((Integer(FCurSysCoord) and $FFFFFF00)
                  or LoadByte[5]);
            end;
          EID_SysCrd_FF:
            begin // ��������� ���������� ������ ������, ������ ����������
              CurCrdLongLinc:= True;
              FBody.ReadBuffer(LoadByte[1], 4);
              FBody.ReadBuffer(LoadLongInt, 4);
              NewSysCoord:= LoadLongInt[1];
            end;
          EID_SysCrd_NS:
            begin // ��������� ���������� �������� ����������
              CurCrdLongLinc:= True;
              FBody.ReadBuffer(LoadByte[1], 1);
              NewSysCoord:= Integer((Integer(FCurSysCoord) and $FFFFFF00) or LoadByte[1]);
            end;
          EID_SysCrd_NF:
            begin // ��������� ���������� ������ ����������
              CurCrdLongLinc:= True;
              FBody.ReadBuffer(LoadLongInt, 4);
              NewSysCoord:= LoadLongInt[1];
            end;
  EID_SysCrd_UMUA_Left_NF: begin // ������ ��������� ���������� ��� ������ ��� A, ����� �������
                              FBody.ReadBuffer(LoadLongInt, 4);
                              if DWORD(LoadLongInt[1]) < $FFFFFF00
                                then FCurLoadSysCoord_UMUA_Left:= LoadLongInt[1]
                                else FCurLoadDelta_UMUA_Left:= ShortInt(LoadLongInt[1] and $FF);
                            end;
  EID_SysCrd_UMUB_Left_NF: begin // ������ ��������� ���������� ��� ������ ��� �, ����� �������
                              FBody.ReadBuffer(LoadLongInt, 4);
                              if DWORD(LoadLongInt[1]) < $FFFFFF00
                                then FCurLoadSysCoord_UMUB_Left:= LoadLongInt[1]
                                else FCurLoadDelta_UMUB_Left:= ShortInt(LoadLongInt[1] and $FF);


                            end;
  EID_SysCrd_UMUA_Right_NF: begin // ������ ��������� ���������� ��� ������ ��� A, ������ �������
                              FBody.ReadBuffer(LoadLongInt, 4);
                              if DWORD(LoadLongInt[1]) < $FFFFFF00
                                then FCurLoadSysCoord_UMUA_Right:= LoadLongInt[1]
                                else FCurLoadDelta_UMUA_Right:= ShortInt(LoadLongInt[1] and $FF);


                            end;
  EID_SysCrd_UMUB_Right_NF: begin // ������ ��������� ���������� ��� ������ ��� �, ������ �������
                              FBody.ReadBuffer(LoadLongInt, 4);
                              if DWORD(LoadLongInt[1]) < $FFFFFF00
                                then FCurLoadSysCoord_UMUB_Right:= LoadLongInt[1]
                                else FCurLoadDelta_UMUB_Right:= ShortInt(LoadLongInt[1] and $FF);


                            end;


          EID_EndFile:
            begin // ����� �����
//              showmessage('!');
              Exit;
            end;
          else SkipEvent(LoadID);
        end;

        if LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF, EID_SysCrd_NS, EID_SysCrd_NF] then
        begin
          {UnPackEcho;
          FUnPackFlag[rLeft]:= False;
          FUnPackFlag[rRight]:= False;

          if (FPack[rLeft].Ampl <> $FF) then
          begin
            FCurEcho[rLeft, 1].Delay[1]:= FPack[rLeft].Delay;
            FCurEcho[rLeft, 1].Ampl[1]:= FPack[rLeft].Ampl;
            FCurEcho[rLeft, 1].Count:= 1;
            FCurEcho[rLeft, 1].ZerroFlag:= False;
            FUnPackFlag[rLeft]:= True;
          end;
          if (FPack[rRight].Ampl <> $FF) then
          begin
            FCurEcho[rRight, 1].Delay[1]:= FPack[rRight].Delay;
            FCurEcho[rRight, 1].Ampl[1]:= FPack[rRight].Ampl;
            FCurEcho[rRight, 1].Count:= 1;
            FCurEcho[rRight, 1].ZerroFlag:= False;
            FUnPackFlag[rRight]:= True;
          end;
           }
          BackMotion:= NewSysCoord < LastSysCoord;

          if Assigned(BlockOk) and (FCurDisCoord >= StartDisCoord - LoadShift)
            and (FCurDisCoord <= EndDisCoord) then
            if not BlockOk(StartDisCoord) then
              Break; // ��������� ������� ��� "������" ����������


//            for R:= FConfig.MinRail to FConfig.MaxRail do // ������� ������� ������� ��������
//              for I:= FConfig.MinScanChNumber to FConfig.MaxScanChNumber do
//                FCurEcho[R, I].ZerroFlag2:= False;


          // ToLog(Format('Crd: %d; R: 1; Ch: 1 Echo: %d; UnPack %d', [FCurDisCoord, CurEcho[r_Left, 1].Count, Ord(FUnPackFlag[r_Left])])); // ----------------------------------------------------------------------------------------
          // ToLog(Format('Crd: %d; R: 2; Ch: 1 Echo: %d; UnPack %d', [FCurDisCoord, CurEcho[r_Right, 1].Count, Ord(FUnPackFlag[r_Right])])); // ----------------------------------------------------------------------------------------

          FCurSysCoord:= NewSysCoord;
          // ���� ���� ������������� ������� ������� ������ �� ����� ����������


//////////////////////////////////////////////////////////////////////////////
         (*

          if (Abs(FCurSysCoord - LastSysCoord) > 1) then // ���������� ���� �� ������ - ������ ���� ��������� �������
//            if ((FPack[rLeft].Ampl <> $FF) or (FPack[rRight].Ampl <> $FF)) then
            begin
              FSaveDisCoord:= FCurDisCoord;
              FSaveSysCoord:= FCurSysCoord;
   {           Move(CurEcho, CurEcho_, SizeOf(TCurEcho));
              FillChar(FCurEcho, SizeOf(TCurEcho), 0);

//              if (FPack[rLeft].Ampl <> $FF) then
              begin
                FCurEcho[rLeft, 1].Delay[1]:= 50; // FPack[rLeft].Delay;
                FCurEcho[rLeft, 1].Ampl[1]:= 12; // FPack[rLeft].Ampl;
                FCurEcho[rLeft, 1].Count:= 1;
                FCurEcho[rLeft, 1].ZerroFlag:= False;
                FUnPackFlag[rLeft]:= True;
              end;
//              if (FPack[rRight].Ampl <> $FF) then
              begin
                FCurEcho[rRight, 1].Delay[1]:= 50; // FPack[rRight].Delay;
                FCurEcho[rRight, 1].Ampl[1]:= 12; // FPack[rRight].Ampl;
                FCurEcho[rRight, 1].Count:= 1;
                FCurEcho[rRight, 1].ZerroFlag:= False;
                FUnPackFlag[rRight]:= True;
              end;     }

              if FCurSysCoord > LastSysCoord then
              begin

                T:= FCurDisCoord;

                // if FCurSysCoord - LastSysCoord < 5000 then
                for J:= LastSysCoord + 1 to FCurSysCoord - 1 do
                begin
                  Inc(FCurDisCoord);
                  FCurSysCoord:= J;
                  if Assigned(BlockOk) and
                    (FCurDisCoord >= StartDisCoord - LoadShift) and
                    (FCurDisCoord <= EndDisCoord) then
                    if not BlockOk(StartDisCoord) then Break;
                  if CurDisCoord >= EndDisCoord then Exit;
                end;

                // ToLog(Format('Unpack St: %d Ed: %d; UnPack: 1', [T + 1, FCurDisCoord]));

              end
              else
              begin

                T:= FCurDisCoord;

                // if LastSysCoord - FCurSysCoord < 5000 then
                for J:= LastSysCoord - 1 downto FCurSysCoord + 1 do
                begin
                  Inc(FCurDisCoord);
                  FCurSysCoord:= J;
                  if Assigned(BlockOk) and (FCurDisCoord >= StartDisCoord) and
                    (FCurDisCoord <= EndDisCoord) then
                    if not BlockOk(StartDisCoord) then Break;
                  if CurDisCoord >= EndDisCoord then Exit;
                end;

                // ToLog(Format('Unpack St: %d Ed: %d; UnPack: 1', [T + 1, FCurDisCoord]));

              end;

           //   Move(CurEcho_, FCurEcho, SizeOf(TCurEcho));
              FCurDisCoord:= FSaveDisCoord;
              FCurSysCoord:= FSaveSysCoord;
            end;
           *)
//////////////////////////////////////////////////////////////////////////////

          Inc(FCurDisCoord, Abs(FCurSysCoord - LastSysCoord)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)
          LastSysCoord:= FCurSysCoord;
          LastDisCoord:= FCurDisCoord;

          GetParamNext(FCurDisCoord, FCurParams);
          if CurDisCoord >= EndDisCoord then Exit;

          for R:= FConfig.MinRail to FConfig.MaxRail do // ������� ������� ������� ��������
//          begin
            for I:= FConfig.MinScanChNumber to FConfig.MaxScanChNumber do
            begin
              FCurEcho[R, I].Count:= 0;
         //     FCurEcho[R, I].ZerroFlag:= True;
         //     FCurEcho[R, I].ZerroFlag2:= False;
            end;
            // CurAmpl[R]:= - 1;
//          end;
        end;
      end
 {     else
      begin
        SkipBytes:= Avk11EventLen[LoadID];
        if LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS,
          EID_SysCrd_FF] then
          UnPackEcho;
      end; }
    end; // else {if not} SkipEvent(LoadID); // { LoadID and 128 = 0 } then // �������

//    if SkipBytes <> -1 then
//      FBody.Position:= FBody.Position + SkipBytes;
  until False;
end;

function TAviconDataSource.GetBMStateFirst(DisCoord: Integer): Boolean;
var
  I: Integer;
  EndIdx: Integer;
  StartIdx: Integer;

begin
  Result:= False;
  FGetBMStateIdx:= 0;
  if FSkipBM then
  begin
    FGetBMStateState:= False;
    Exit;
  end;

  for I:= High(FCDList) downto 0 do
  begin
    if FEvents[FCDList[I]].DisCoord <= DisCoord then
    begin
      if FEvents[FCDList[I]].ID = EID_FwdDir then
      begin
        Result:= False;
        FGetBMStateIdx:= I + 1;
        Break;
      end;
      if FEvents[FCDList[I]].ID = EID_BwdDir { EID_FwdDirEnd } then
      begin
        Result:= True;
        FGetBMStateIdx:= I + 1;
        Break;
      end;
      Break;
    end;
  end;
  FGetBMStateState:= Result;

  {
    FGetBMStateIdx:= 0;
    GetEventIdx(0, DisCoord, StartIdx, EndIdx, 0);

    Result:= False;
    for I:= EndIdx downto 0 do
    begin
    if FEvents[I].Event.ID = EID_FwdDir then
    begin
    Result:= False;
    FGetBMStateIdx:= I + 1;
    Break;
    end;
    if FEvents[I].Event.ID = EID_BwdDir then
    begin
    Result:= True;
    FGetBMStateIdx:= I + 1;
    Break;
    end;
    end;
    FGetBMStateState:= Result;
    }
end;

function TAviconDataSource.GetBMStateNext(var DisCoord: Integer; var State: Boolean): Boolean;
var
  I: Integer;
  EndIdx: Integer;
  StartIdx: Integer;

begin
  Result:= False;
  State:= FGetBMStateState;
  if FSkipBM then Exit;

  for I:= FGetBMStateIdx to High(FCDList) do
  begin
    if FEvents[FCDList[I]].ID = EID_FwdDir then
    begin
      State:= False;
      FGetBMStateIdx:= I + 1;
      FGetBMStateState:= False;
      DisCoord:= FEvents[FCDList[I]].DisCoord;
      Result:= I <> GetEventCount - 1;
      Break;
    end;
    if FEvents[FCDList[I]].ID = EID_BwdDir { EID_FwdDirEnd } then
    begin
      State:= True;
      FGetBMStateIdx:= I + 1;
      FGetBMStateState:= True;
      DisCoord:= FEvents[FCDList[I]].DisCoord;
      Result:= I <> GetEventCount - 1;
      Break;
    end;
  end;

  {
    Result:= False;
    State:= FGetBMStateState;
    for I:= FGetBMStateIdx to GetEventCount - 1 do
    begin
    if FEvents[I].Event.ID = EID_FwdDir then
    begin
    State:= False;
    FGetBMStateIdx:= I + 1;
    FGetBMStateState:= False;
    DisCoord:= FEvents[I].Event.DisCoord;
    Result:= I <> GetEventCount - 1;
    Break;
    end;
    if FEvents[I].Event.ID = EID_BwdDir then
    begin
    State:= True;
    FGetBMStateIdx:= I + 1;
    FGetBMStateState:= True;
    DisCoord:= FEvents[I].Event.DisCoord;
    Result:= I <> GetEventCount - 1;
    Break;
    end;
    end;
    }
end;

procedure TAviconDataSource.SetScanStep(New: Word);
begin
  FHead.ScanStep:= New;
end;

function TAviconDataSource.GetEventIdx(StartDisCoord, EndDisCoord: Integer;
  var StartIdx, EndIdx: Integer; EmptyPar: Integer): Boolean;
var
  I: Integer;
  SameCoord1: TIntegerDynArray;
  SameCoord2: TIntegerDynArray;
  LeftIdx1: Integer;
  LeftIdx2: Integer;
  RightIdx1: Integer;
  RightIdx2: Integer;

begin

  GetNearestEventIdx(StartDisCoord, LeftIdx1, RightIdx1, SameCoord1);
  GetNearestEventIdx(EndDisCoord, LeftIdx2, RightIdx2, SameCoord2);

  if Length(SameCoord1) <> 0 then
    StartIdx:= SameCoord1[0]
  else
    StartIdx:= LeftIdx1 + 1;

  if Length(SameCoord2) <> 0 then
    EndIdx:= SameCoord2[ High(SameCoord2)]
  else
    EndIdx:= RightIdx2 - 1;

  {
    StartIdx:= High(FEvents);
    EndIdx:= 0;

    for I:= 0 to High(FEvents) do
    begin
    if (StartDisCoord <= FEvents[I].Event.DisCoord) and
    (EndDisCoord   >= FEvents[I].Event.DisCoord) then
    begin
    StartIdx:= Min(StartIdx, I);
    EndIdx:=   Max(EndIdx, I);
    end;
    if EndDisCoord < FEvents[I].Event.DisCoord then Break;
    end; }
end;

procedure TAviconDataSource.GetNearestEventIdx(DisCoord: Integer; var LeftIdx,
  RightIdx: Integer; var SameCoord: TIntegerDynArray);
var
  I: Integer;
  CLeft: Integer;
  CRight: Integer;
  CurrPos: Integer;

begin
  if Length(FEvents) = 0 then
    Exit;
  CLeft:= 0; // ������ � ������ � ������
  CRight:= High(FEvents); // ����� �������������� � �����
  repeat
    CurrPos:= (CLeft + CRight) div 2; // ���������� ��������
    begin
      if FEvents[CurrPos].DisCoord < DisCoord then
        CLeft:= CurrPos // ������� ����� �������� ����� ��� ����������� �������
      else
        CRight:= CurrPos;
      if (CLeft = CRight) or (Abs(CLeft - CRight) = 1) then
      // �������� �� ����� ������
      begin
        for I:= CLeft downto 0 do
          if DisCoord = FEvents[I].DisCoord then
          begin
            SetLength(SameCoord, Length(SameCoord) + 1);
            SameCoord[ High(SameCoord)]:= I;
            LeftIdx:= I;
          end
          else
          begin
            if Length(SameCoord) <> 0 then
              LeftIdx:= SameCoord[0]
            else
              LeftIdx:= CLeft;
            Break;
          end;

        for I:= CLeft + 1 to High(FEvents) do
          if DisCoord = FEvents[I].DisCoord then
          begin
            SetLength(SameCoord, Length(SameCoord) + 1);
            SameCoord[ High(SameCoord)]:= I;
            RightIdx:= I;
          end
          else
          begin
            if Length(SameCoord) <> 0 then
              RightIdx:= SameCoord[ High(SameCoord)]
            else
              RightIdx:= I;
            Break;
          end;

        Exit;
      end;
    end;
  until False;

  (*
    F:= True;
    for I:= 0 to High(FEvents) do
    begin
    if DisCoord = FEvents[I].Event.DisCoord then
    begin
    if F then
    begin
    LeftIdx:= I - 1;
    F:= False;
    end;
    SetLength(SameCoord, Length(SameCoord) + 1);
    SameCoord[High(SameCoord)]:= I;
    end;
    if DisCoord < FEvents[I].Event.DisCoord then
    begin
    if F then LeftIdx:= I - 1;
    RightIdx:= I;
    Break;
    end;
    end;
    *)
end;

function TAviconDataSource.GetLeftEventIdx(DisCoord: Integer): Integer;
var
  LeftIdx, RightIdx: Integer;
  SameCoord: TIntegerDynArray;

begin
  GetNearestEventIdx(DisCoord, LeftIdx, RightIdx, SameCoord);
  if Length(SameCoord) <> 0 then Result:= SameCoord[High(SameCoord)]
                            else Result:= LeftIdx;
end;

function TAviconDataSource.GetRightEventIdx(DisCoord: Integer): Integer;
var
  LeftIdx, RightIdx: Integer;
  SameCoord: TIntegerDynArray;

begin
  GetNearestEventIdx(DisCoord, LeftIdx, RightIdx, SameCoord);
  if Length(SameCoord) <> 0 then Result:= SameCoord[0]
                            else Result:= RightIdx;
end;

procedure TAviconDataSource.GetEventData(Idx: Integer; var ID: Byte; var pData: PEventData);
begin
  if (Idx >= 0) and (Idx <= High(FEvents)) then
  begin
    ID:= FEvents[Idx].ID;
    pData:= Addr(FEvents[Idx].Data);
  end;
end;

function TAviconDataSource.GetTimeList(Index: Integer): TimeListItem;
begin
  Result:= FTimeList[Index];
end;

function TAviconDataSource.GetTimeListCount: Integer;
begin
  Result:= Length(FTimeList)
end;

function TAviconDataSource.GetEventsData(Index: Integer): TEventData;
begin
  Result:= FEvents[Index].Data;
end;

function TAviconDataSource.GetParamFirst(DisCoord: Integer; var Params: TMiasParams): Boolean;
{
  var
  StartIdx, EndIdx: Integer;
  }
begin
  DisCoord:= NormalizeDisCoord(DisCoord);
  if DisCoord <= 0 then  DisCoord:= 1;

  FGetParamIdx:= GetLeftEventIdx(DisCoord);
  if FGetParamIdx <> -1 then Params:= FParams[FGetParamIdx];
end;

function TAviconDataSource.GetParamNext(DisCoord: Integer; var Params: TMiasParams): Boolean;
var
  I: Integer;
  StartIdx, EndIdx: Integer;

begin
  if FGetParamIdx = -1 then   Exit;
  // DisCoord:= NormalizeDisCoord(DisCoord);
  // GetEventIdx(DisCoord, DisCoord, StartIdx, EndIdx);
  // FGetParamIdx:= EndIdx;

  for I:= FGetParamIdx to High(FEvents) do
  begin
    if DisCoord = FEvents[I].DisCoord then
    begin
      Params:= FParams[I];
      FGetParamIdx:= I;
      Break;
    end;
    if DisCoord < FEvents[I].DisCoord then
    begin
      Params:= FParams[I - 1];
      FGetParamIdx:= I;
      Break;
    end;
  end;
end;

function TAviconDataSource.RailToPathRail(R: TRail): TRail;
begin
  if CompareMem(@Header.DeviceID, @Avicon15Scheme1, 8) or
     CompareMem(@Header.DeviceID, @USK003R, 8) or
     CompareMem(@Header.DeviceID, @USK004R, 8) then
  begin
    Result:= TRail(Header.WorkRailTypeA);
  end
  else
  begin
    if (Header.UsedItems[uiCorrSides] = 1) and (Header.CorrespondenceSides = csReverse) then
    begin
      case R of
         rLeft: Result:= rRight;
        rRight: Result:= rLeft;
      end;
    end else Result:= R;
  end;
end;

function TAviconDataSource.isUSK: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @USK004R, 8) or
           CompareMem(@FHead.DeviceID, @USK003R, 8);
end;

function TAviconDataSource.isSingleRailDevice: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @RKS_U_Scheme_5, 8) or
           CompareMem(@FHead.DeviceID, @RKS_U_Scheme_8, 8) or
           CompareMem(@FHead.DeviceID, @Avicon15Scheme1, 8) or
           CompareMem(@FHead.DeviceID, @USK003R, 8) or
           CompareMem(@FHead.DeviceID, @USK004R, 8);
end;

function TAviconDataSource.isEGOUSW: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @EGO_USWScheme1, 8) or
           CompareMem(@FHead.DeviceID, @EGO_USWScheme2, 8) or
           CompareMem(@FHead.DeviceID, @EGO_USWScheme3, 8);
end;

function TAviconDataSource.isEGOUS: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @Avicon16Scheme1, 8) or
           CompareMem(@FHead.DeviceID, @Avicon16Scheme2, 8) or
           CompareMem(@FHead.DeviceID, @Avicon16Scheme3, 8) or
           CompareMem(@FHead.DeviceID, @Avicon16Wheel, 8);
end;

function TAviconDataSource.isEGOUSW_BHead: Boolean;
begin
  Result:= isEGOUSW and (Header.ControlDirection = cd_Head_B_Tail_A);
end;


function TAviconDataSource.GetTime(DisCoord: Integer): string;
var
  ID: Byte;
  I: Integer;
  pData: PEventData;
  StartIdx: Integer;

function CodeToTime(Hour, Minute: Byte) : string;
var
  HS, MS: string;
  I: Integer;

begin
  HS:= IntToStr(Hour);
  if Length(HS) = 1 then HS:= '0' + HS;
  MS:= IntToStr(Minute);
  if Length(MS) = 1 then MS:= '0' + MS;
  Result:= Format('%s:%s', [HS, MS]);
end;

begin
  DisCoord:= NormalizeDisCoord(DisCoord);
  StartIdx:= GetLeftEventIdx(DisCoord);

  Result:= CodeToTime(FHead.Hour, FHead.Minute);

  for I:= StartIdx downto 0 do
  begin
    GetEventData(I, ID, pData);
    if ID in [EID_Time] then
    begin
      Result:= CodeToTime(pedTime(pData)^.Hour, pedTime(pData)^.Minute);
      Exit;
    end;
  end;
end;

function TAviconDataSource.GetGPSCoord(DisCoord: Integer): TGPSPoint;
var
  ID: Byte;
  I: Integer;
  pData: PEventData;
  StartIdx: Integer;


begin
  DisCoord:= NormalizeDisCoord(DisCoord);
  Result.DisCoord:= - 1;
  for I:= 0 to High(GPSPoints) - 1 do
  begin
    if (DisCoord >= GPSPoints[I].DisCoord) and
       (DisCoord <= GPSPoints[I + 1].DisCoord) then
    begin
      Result.State:= GPSPoints[I].State;
      Result.Lat:= GPSPoints[I].Lat + (GPSPoints[I + 1].Lat - GPSPoints[I].Lat) * (DisCoord - GPSPoints[I].DisCoord) / (GPSPoints[I + 1].DisCoord - GPSPoints[I].DisCoord);
      Result.Lon:= GPSPoints[I].Lon + (GPSPoints[I + 1].Lon - GPSPoints[I].Lon) * (DisCoord - GPSPoints[I].DisCoord) / (GPSPoints[I + 1].DisCoord - GPSPoints[I].DisCoord);
      Result.Speed:= GPSPoints[I].Speed;
      Result.DisCoord:= DisCoord;
      Exit;
    end;
  end;
end;

function TAviconDataSource.GetSpeed(DisCoord: Integer; var Speed: Single)
  : string;
var
  I: Integer;
  J: Integer;
  K: Integer;
  T: Extended;
  S: Extended;

begin
  for I:= High(FTimeList) downto 0 do
    if FTimeList[I].DC <= DisCoord then
    begin
      if I <> High(FTimeList) then
        J:= I + 1
      else
        J:= I - 1;
      K:= I;
      if K > 0 then
        Dec(K);
      if J < High(FTimeList) then
        Inc(J);
      if K > 0 then
        Dec(K);
      if J < High(FTimeList) then
        Inc(J);
      if K > 0 then
        Dec(K);
      if J < High(FTimeList) then
        Inc(J);
      if K > 0 then
        Dec(K);
      if J < High(FTimeList) then
        Inc(J);

      T:= Abs(FTimeList[J].H + FTimeList[J].M / 60 -
          (FTimeList[K].H + FTimeList[K].M / 60));
      if T <> 0 then
      begin
        S:= Abs(FTimeList[J].DC - FTimeList[K].DC)
          * FHead.ScanStep / 100 / 1000000;
        Speed:= S / T;
        if T <> 0 then Result:= Format('%3.1f', [S / T])
                  else Result:= '?';
      end;
      Exit;
    end;
end;


procedure ScanDir(StartDir: string; Mask: string; List: TStrings);
var
  SearchRec: TSearchRec;

begin
  if Mask = '' then Mask := '*.*';
  if StartDir[Length(StartDir)] <> '\' then
  StartDir := StartDir + '\';
  if FindFirst(StartDir + Mask, faAnyFile, SearchRec) = 0 then
  begin
    repeat
    //  Application.ProcessMessages;
      if (SearchRec.Attr and faDirectory) <> faDirectory
        then List.AddObject(StartDir + SearchRec.Name, Pointer(SearchRec.Size))
        else if (SearchRec.Name <> '..') and (SearchRec.Name <> '.') then
        begin
          List.AddObject(StartDir + SearchRec.Name + '\', Pointer(0));
          ScanDir(StartDir + SearchRec.Name + '\', Mask, List);
        end;
    until FindNext(SearchRec) <> 0;
    FindClose(SearchRec);
  end;
end;

function TAviconDataSource.LoadFromFile(FileName: string; VengrData: boolean = False): Boolean;
var
  FileID: TAviconID;
  DeviceID: TAviconID;
  DeviceVer: Byte;
  ID: Word;
  I, J, K: Integer;
  // OldHeader1: TAvk11Header1;
  FileAttr: Integer;
  FileList: TStringList;
  VFile: TStringList;
  Code: Integer;
  ValStr: string;
  tmp: string;
  idx: integer;
  val2: integer;
  val1: integer;
  val3: Integer;
  tmpp: TVData;

begin

///////////////////////////////////////////////////////

  FileList:= TStringList.Create;
  ScanDir(ExtractFilePath(FileName), 'pk_*.txt', FileList);

//  Decimalseparator:= ',';
  if VengrData then
  begin
    VFile:= TStringList.Create;
    for I := 0 to FileList.Count - 1 do
    begin

      tmp:= ExtractFileName(FileList[I]);
      delete(tmp, length(tmp)-3, 4);
      idx:= StrToInt(tmp[length(tmp)]) - 1;
      VFile.LoadFromFile(FileList[I]);

      for ID:= 1 to VFile.Count - 1 do
      begin
        SetLength(VData[idx], Length(VData[idx]) + 1);

        J:= Pos('km', VFile[ID]);
        TryStrToInt(Copy(VFile[ID], 1, J - 1), VData[idx, High(VData[idx])].Km);

        K:= Pos('pk', VFile[ID]);
        TryStrToInt(Copy(VFile[ID], J + 2, K - (J + 2)), VData[idx, High(VData[idx])].Pk);

        J:= K;
        K:= Pos('m', Copy(VFile[ID], K, 255)) + K - 1;
        TryStrToInt(Copy(VFile[ID], J + 2, K - (J + 2)), VData[idx, High(VData[idx])].m);

        J:= K;
        K:= Pos('mm', VFile[ID]);
        TryStrToInt(Copy(VFile[ID], J + 1, K - (J + 1)), VData[idx, High(VData[idx])].mm);

        J:= J + 1 + K - (J + 1) + 3;
        ValStr:= Copy(VFile[ID], J, 255);
        ValStr[Pos(',', ValStr)]:= '.';
        Val(ValStr, VData[idx, High(VData[idx])].Value, Code);

      end;
    end;
    VFile.Free;
          {
    for Idx := 0 to 3 do
      for I := 0 to High(VData[idx]) do
      begin
        Val1:= VData[idx, I].Km * 1000000 + VData[idx, I].Pk * 100000 + VData[idx, I].m * 1000 + VData[idx, I].mm;
        K:= - 1;
        for J := 0 to High(VData[idx]) do
          if I <> J then
          begin
            Val2:= VData[idx, J].Km * 1000000 + VData[idx, J].Pk * 100000 + VData[idx, J].m * 1000 + VData[idx, J].mm;
            if Val2 < Val1 then
            begin
              K:= J;
              Val1:= Val2;
            end;
          end;
        if K <> - 1 then
        begin
          tmpp:= VData[idx, I];
          VData[idx, I]:= VData[idx, K];
          VData[idx, K]:= tmpp;
        end;
      end;
      }
  end;



//  Decimalseparator:= '.';


{

97km 10pk 0m 952mm	     0,008
97km 10pk 0m 956mm	     0,006
97km 10pk 0m 969mm	     0,019
97km 10pk 0m 972mm	     0,019
97km 10pk 0m 996mm	     0,009
97km 10pk 0m 998mm	     0,011
97km 10pk 1m 7mm	     0,002
97km 10pk 1m 23mm	     0,002
97km 10pk 1m 24mm	     0,002
 }

///////////////////////////////////////////////////////


  Error:= [];
  FLoadFromStream:= False;
  FReadOnlyFlag:= False;
  FileAttr:= FileGetAttr(FileName);
  if FileAttr and faReadOnly <> 0 then
    if FileSetAttr(FileName, FileAttr and $FE) <> 0 then
      FReadOnlyFlag:= True;

  Result:= False;

  if not FileExists(FileName) then
  begin
    Error:= Error + [eFileNotFound];
    Exit;
  end;

  FFileName:= FileName;
  FBody:= TMemoryStream.Create;

  try
    FBody.LoadFromFile(FileName);
  except
    Error:= Error + [eUnknownError];
    Exit;
  end;

  FDataSize:= FBody.Size;
  FSaveFileDate:= FileDateToDateTime(FileAge(FFileName));
  FSaveFileSize:= FBody.Size;

  if FDataSize < 17 then
  begin
    Error:= Error + [eIncorrectFile];
    Exit;
  end;

  FBody.Position:= 0;
  FBody.ReadBuffer(FileID, 8);
  if not CompareMem(@FileID, @AviconFileID, 8) then
  begin
    Error:= Error + [eIncorrectFile];
    Exit;
  end;

  FBody.ReadBuffer(DeviceID, 8);
  FBody.ReadBuffer(DeviceVer, 1);

  if (DeviceVer <> 1) and (DeviceVer <> 2) and (DeviceVer <> 3) then
  begin
    Error:= Error + [eIncorrectFileVersion];
    Exit;
  end;

  FFullHeader:=       (DeviceID[7] and $01) <> 0; // ���� � ����������� ����������
  FExtendedChannels:= (DeviceID[7] and $02) <> 0; // ���������� ���������� �������

  if FFullHeader then
  begin
    if FDataSize < SizeOf(TFileHeader) then
    begin
      Error:= Error + [eIncorrectFile];
      Exit;
    end;

    // �������� ��������� �����, ��� ������ ���������� �� ������ � ������ �� ��������, ���������� ������������ �� HeaderVer

    try
      FBody.Position:= 0;
      FBody.ReadBuffer(FHead, SizeOf(TFileHeader));
      FConfig:= ConfigList.GetItemByID(FHead.DeviceID, FHead.DeviceVer);
    except
      Error:= Error + [eUnknownError];
      Exit;
    end;

    if FHead.HeaderVer = 1 then
    begin
      FHead.PathCoordSystem:= Ord(csMetricRF); // � ������ 1 �� ���� ������ ������ �������� � ��������� ���������� - ������ �����
    end
    else
    if FHead.HeaderVer > 4 then
    begin
      Error:= Error + [eIncorrectFileVersion];
      Exit;
    end;
  end
  else
  begin
    FConfig:= ConfigList.GetItemByID(DeviceID, FHead.DeviceVer);
    FHead.ScanStep:= 100;
  end;

  LoadExData;
  if FUnknownFileVersion then
  begin
    Error:= Error + [eIncorrectFileVersion];
    Exit;
  end;

  FillEventsData;
  GenerateGPSPoints;
  MakeNoteBookFromNordco;
  Result:= True;
end;

function TAviconDataSource.LoadFromStream(Stream: TStream): Boolean;
var
  ID: Word;
  I: Integer;
  DeviceVer: Byte;
  FileAttr: Integer;
  FileID: TAviconID;
  DeviceID: TAviconID;

begin
  FLoadFromStream:= True;
  FReadOnlyFlag:= True;
  Result:= False;
  FBody:= TMemoryStream.Create;
  FBody.LoadFromStream(Stream);
  FDataSize:= FBody.Size;
  FSaveFileSize:= FBody.Size;

  if FDataSize < 17 then
  begin
    Error:= Error + [eIncorrectFile];
    Exit;
  end;

  FBody.Position:= 0;
  FBody.ReadBuffer(FileID, 8);
  if not CompareMem(@FileID, @AviconFileID, 8) then
  begin
    Error:= Error + [eIncorrectFile];
    Exit;
  end;

  FBody.ReadBuffer(DeviceID, 8);
  FBody.ReadBuffer(DeviceVer, 1);
  FFullHeader:= (DeviceID[7] and $01) <> 0; // ���� � ����������� ����������

  if FFullHeader then
  begin
    if FDataSize < SizeOf(TFileHeader) then
    begin
      Error:= Error + [eIncorrectFile];
      Exit;
    end;

    try
      FBody.Position:= 0;
      FBody.ReadBuffer(FHead, SizeOf(TFileHeader));
      FConfig:= ConfigList.GetItemByID(FHead.DeviceID, FHead.DeviceVer);
    except
      Error:= Error + [eUnknownError];
      Exit;
    end;
  end
  else
  begin
    FConfig:= ConfigList.GetItemByID(DeviceID, DeviceVer);
    FHead.ScanStep:= 100;
  end;

  LoadExData;
  if FUnknownFileVersion then
  begin
    Error:= Error + [eIncorrectFileVersion];
    Result:= False;
    Exit;
  end;

  FillEventsData;
  Result:= True;
end;

function TAviconDataSource.LoadHeader(FileName: string): Boolean;
begin
end;

function TAviconDataSource.DisToSysCoord(Coord: Integer): Integer;
var
  LeftIdx, RightIdx: Integer;
  Del: Integer;

begin
  Coord:= NormalizeDisCoord(Coord);
  LeftIdx:= GetLeftEventIdx(Coord);
  if FEvents[LeftIdx].DisCoord = Coord then Result:= FEvents[LeftIdx].SysCoord
  else
  begin
    RightIdx:= GetRightEventIdx(Coord);
    Del:= (FEvents[RightIdx].DisCoord - FEvents[LeftIdx].DisCoord);
    if Del <> 0 then Result:= Round(FEvents[LeftIdx].SysCoord + ((Coord - FEvents[LeftIdx].DisCoord) / Del) * (FEvents[RightIdx].SysCoord - FEvents[LeftIdx].SysCoord))
                else Result:= FEvents[LeftIdx].SysCoord;
  end;
end;

procedure TAviconDataSource.DisToDisCoords(Coord: Integer; var Res: TIntegerDynArray);
var
  I, J: Integer;
begin
  Coord:= DisToSysCoord(NormalizeDisCoord(Coord));
  for I:= 0 to EventCount - 2 do
    if (FEvents[I].SysCoord <= Coord) and (FEvents[I + 1].SysCoord >= Coord) or
       (FEvents[I].SysCoord >= Coord) and (FEvents[I + 1].SysCoord <= Coord) then
    begin
      J:= FEvents[I + 0].DisCoord + Abs(Coord - FEvents[I + 0].SysCoord);
      if (Length(Res) = 0) or (J <> Res[ High(Res)]) then
      begin
        SetLength(Res, Length(Res) + 1);
        Res[ High(Res)]:= J;
      end;
    end;
end;

procedure TAviconDataSource.DisToDisCoords_(Coord: Integer; var Res: TIntegerDynArray; var StIdx, Count: Integer);
var
  I, J: Integer;
  Flg: Boolean;

begin
  Flg:= False;

  Coord:= DisToSysCoord(NormalizeDisCoord(Coord));
  for I:= StIdx to EventCount - 2 do
    if (FEvents[I].SysCoord <= Coord) and
      (FEvents[I + 1].SysCoord >= Coord) or
      (FEvents[I].SysCoord >= Coord) and
      (FEvents[I + 1].SysCoord <= Coord) then
    begin
      if not Flg then
      begin
        StIdx:= I;
        Flg:= True;
      end;
      J:= FEvents[I + 0].DisCoord + Abs(Coord - FEvents[I + 0].SysCoord);
      // if (Length(Res) = 0) or (J <> Res[High(Res)]) then
      if (Count = 0) or ((Count <> 0) and (J <> Res[Count - 1])) then
      begin
        Res[Count]:= J;
        Inc(Count);

        // SetLength(Res, Length(Res) + 1);
        // Res[High(Res)]:= J;
      end;
    end;
end;

function TAviconDataSource.TestChangeDirCrd(Coord: Integer): Boolean;
var
  I: Integer;

begin
  for I:= 0 to EventCount - 1 do
    if (FEvents[I].DisCoord = Coord) and
      (FEvents[I].ID in [EID_FwdDir, EID_BwdDir]) then
    begin
      Result:= True;
      Exit;
    end;
end;

function TAviconDataSource.SysToDisCoord(Coord: Integer): Integer;
var
  I: Integer;

begin
{
  if Coord > FMaxSysCoord then
  begin
    Result:= MaxDisCoord;
    Exit;
  end;
  Result:= 0;
  for I:= 0 to High(FEvents) - 1 do
    if (Coord >= FEvents[I + 0].SysCoord) and (Coord <= FEvents[I + 1].SysCoord) then
    begin
      Result:= FEvents[I + 0].DisCoord + Abs(FEvents[I + 0].SysCoord - Coord);
      Exit;
    end;
}

  Result:= 0;
  for I:= 0 to High(FEvents) - 1 do // ����� �� �������
  begin
    if (Coord >= FEvents[I + 0].SysCoord) and (Coord <= FEvents[I + 1].SysCoord) then
    begin
      Result:= FEvents[I + 0].DisCoord + Abs(FEvents[I + 0].SysCoord - Coord);
      Exit;
    end;
  end;

  for I:= 0 to High(FEvents) - 1 do // ����� �� ������
  begin
    if (Coord <= FEvents[I + 0].SysCoord) and (Coord >= FEvents[I + 1].SysCoord) then
    begin
      Result:= FEvents[I + 0].DisCoord + Abs(FEvents[I + 0].SysCoord - Coord);
      Exit;
    end;
  end;

  if Coord > FMaxSysCoord then Result:= FMaxSysCoord;
//  if Coord < FMinSysCoord then Result:= 0;

end;

// -------------< ������� ��������� - ����������� �� >-----------------------------------

function TAviconDataSource.DisToMRFCrdPrm(DisCoord: Integer; var CoordParams: TMRFCrdParams): Boolean;
var
  ID: Byte;
  I, J: Integer;
  pData: PEventData;
  StartIdx: Integer;
  MMLen: Integer;
  LeftFlg: Boolean;
  RightFlg: Boolean;
  Post: TMRFCrd;

begin
  DisCoord:= NormalizeDisCoord(DisCoord);

                                      // ����� ������ �������������� ����� �� DisCoord
  LeftFlg:= False;
  for I:= GetLeftEventIdx(DisCoord) downto 0 do
  begin
    GetEventData(I, ID, pData);
    if ID = EID_Stolb then
    begin
      CoordParams.LeftStolb[ssLeft].Km:= pCoordPost(pData)^.Km[0];
      CoordParams.LeftStolb[ssLeft].Pk:= pCoordPost(pData)^.Pk[0];
      CoordParams.LeftStolb[ssRight].Km:= pCoordPost(pData)^.Km[1];
      CoordParams.LeftStolb[ssRight].Pk:= pCoordPost(pData)^.Pk[1];
      CoordParams.ToLeftStolbMM:= (DisToSysCoord(DisCoord) - FEvents[I].SysCoord) * FHead.ScanStep div 100;
      CoordParams.LeftIdx:= I;
      LeftFlg:= True;
      Break;
    end;
  end;
                                              // ����� ������ �������������� ������ �� DisCoord
  RightFlg:= False;
  for I:= GetRightEventIdx(DisCoord) to High(FEvents) do
  begin
    GetEventData(I, ID, pData);
    if ID = EID_Stolb then
    begin
      CoordParams.RightStolb[ssLeft].Km:= pCoordPost(pData)^.Km[0];
      CoordParams.RightStolb[ssLeft].Pk:= pCoordPost(pData)^.Pk[0];
      CoordParams.RightStolb[ssRight].Km:= pCoordPost(pData)^.Km[1];
      CoordParams.RightStolb[ssRight].Pk:= pCoordPost(pData)^.Pk[1];
      CoordParams.ToRightStolbMM:= (FEvents[I].SysCoord - DisToSysCoord(DisCoord)) * FHead.ScanStep div 100;
      CoordParams.RightIdx:= I;
      RightFlg:= True;
      Break;
    end;
  end;

  if not LeftFlg then // ��� ������ ����� - ��������� �� ��������� ��������� � ������
  begin
    Post.Km:= FHead.StartKM;
    Post.Pk:= FHead.StartPk;
    Post:= GetPrevMRFPost(Post, FHead.MoveDir);
    CoordParams.LeftStolb[ssLeft].Km:= Post.Km;
    CoordParams.LeftStolb[ssLeft].Pk:= Post.Pk;
    CoordParams.LeftStolb[ssRight].Km:= FHead.StartKM;
    CoordParams.LeftStolb[ssRight].Pk:= FHead.StartPk;
    if FHead.MoveDir > 0 then CoordParams.ToLeftStolbMM:= Round(FHead.StartMetre * 1000 + DisToSysCoord(DisCoord) / 100 * FHead.ScanStep)
                         else CoordParams.ToLeftStolbMM:= (100 - FHead.StartMetre) * 1000 + DisToSysCoord(DisCoord) * FHead.ScanStep div 100;
    CoordParams.LeftIdx:= - 1;
  end;
  if not RightFlg then // ��� ������ ������ - ��������� �� ��������� �������� ��������� � ������
  begin
    CoordParams.RightStolb[ssLeft].Km:= FExHeader.EndMRFCrd.Km;
    CoordParams.RightStolb[ssLeft].Pk:= FExHeader.EndMRFCrd.Pk;
    Post.Km:= FExHeader.EndMRFCrd.Km;
    Post.Pk:= FExHeader.EndMRFCrd.Pk;
    Post:= GetNextMRFPost(Post, FHead.MoveDir);
    CoordParams.RightStolb[ssRight].Km:= Post.Km;
    CoordParams.RightStolb[ssRight].Pk:= Post.Pk;
    CoordParams.ToRightStolbMM:= 100000 - CoordParams.ToLeftStolbMM; // ���� ��� ������ ������ ������ ��� ����� ������ ����� 100 ������
    CoordParams.RightIdx:= - 1;
  end;
end;

function TAviconDataSource.DisToMRFCrd(DisCoord: Longint): TMRFCrd;
var
  CoordParams: TMRFCrdParams;

begin
  DisToMRFCrdPrm(DisCoord, CoordParams);
  Result:= MRFCrdParamsToCrd(CoordParams, Header.MoveDir);
end;

function TAviconDataSource.MRFCrdToDisCrd(RFC: TMRFCrd; var DisCoord: Longint): Boolean;
var
  I, J: Integer;
  ID: Byte;
  pData: PEventData;
  Len: Integer;

begin
  if (FHead.MoveDir > 0) then
  begin
    if (FHead.StartKM = RFC.Km) and
       (FHead.StartPk = RFC.Pk) then
    begin
      Result:= True;
      DisCoord:= SysToDisCoord(Round((RFC.mm - FHead.StartMetre * 1000) / (FHead.ScanStep / 100)));
      Exit;
    end;

    for I:= 0 to EventCount - 1 do
    begin
      GetEventData(I, ID, pData);
      if (ID = EID_Stolb) and
         (pCoordPost(pData)^.Km[1] = RFC.Km) and
         (pCoordPost(pData)^.Pk[1] = RFC.Pk) then
      begin
        Result:= True;
        DisCoord:= SysToDisCoord(DisToSysCoord(Event[I].DisCoord) + Round(RFC.mm / (FHead.ScanStep / 100)));
        Exit;
      end;
    end;
  end
  else
  begin
    if (FHead.StartKM = RFC.Km) and
       (FHead.StartPk = RFC.Pk) then
    begin

      for I:= 0 to EventCount - 1 do // ���� �� ������ ?
      begin
        GetEventData(I, ID, pData);
        if (ID = EID_Stolb) then             // ����
        begin
          if (pCoordPost(pData)^.Km[0] = RFC.Km) and  //
             (pCoordPost(pData)^.Pk[0] = RFC.Pk) then
          begin
            Len:= 0;
            DisCoord:= SysToDisCoord(DisToSysCoord(Event[I].DisCoord) - Round((RFC.mm + Len) / (FHead.ScanStep / 100)));
            Exit;
          end;
        end;
      end;

      Result:= True;
      DisCoord:= SysToDisCoord(Round((FHead.StartMetre * 1000 - RFC.mm) / (FHead.ScanStep / 100)));
      Exit;
    end;

    for I:= 0 to EventCount - 1 do
    begin
      GetEventData(I, ID, pData);
      if (ID = EID_Stolb) and
         (pCoordPost(pData)^.Km[1] = RFC.Km) and
         (pCoordPost(pData)^.Pk[1] = RFC.Pk) then
      begin

        for J:= I + 1 to EventCount - 1 do // ���� �� ������ ����� ?
        begin
          GetEventData(J, ID, pData);
          if ID = EID_Stolb then           // ���� ����� ����������� �� ����
          begin
            Result:= True;
            DisCoord:= SysToDisCoord(DisToSysCoord(Event[J].DisCoord) - Round(RFC.mm / (FHead.ScanStep / 100)));
            Exit;
          end;
        end;
                                           // ���, ����� ������� ��� ����� ������ 100 �
        Result:= True;
        DisCoord:= SysToDisCoord(DisToSysCoord(Event[I].DisCoord) + Round((100000 - RFC.mm) / (FHead.ScanStep / 100)));
        Exit;
      end;
    end;
  end;
end;

  // -------------< ������� ��������� - ��������� >----------------------------------------

function TAviconDataSource.DisToCaCrdPrm(DisCoord: Integer; var CoordParams: TCaCrdParams): Boolean;
var
  StartIdx: Integer;
  MMLen: Integer;
  ID: Byte;
  I, J: Integer;
  LeftFlg: Boolean;
  RightFlg: Boolean;
  pData: PEventData;

begin
  CoordParams.Sys:= CoordSys;
                                    // ����� ������ �������������� ����� �� DisCoord
  LeftFlg:= False;
  for I:= GetLeftEventIdx(DisCoord) downto 0 do
  begin
    GetEventData(I, ID, pData);
    if ID = EID_StolbChainage then
    begin
      CoordParams.LeftPostVal:= pCoordPostChainage(pData)^.Val;
      CoordParams.ToLeftPostMM:= (DisToSysCoord(DisCoord) - FEvents[I].SysCoord) * FHead.ScanStep div 100;
      CoordParams.LeftIdx:= I;
      LeftFlg:= True;
      Break;
    end;
  end;
                                    // ����� ������ �������������� ������ �� DisCoord
  RightFlg:= False;
  for I:= GetRightEventIdx(DisCoord) to High(FEvents) do
  begin
    GetEventData(I, ID, pData);
    if ID = EID_StolbChainage then
    begin
      CoordParams.RightPostVal:= pCoordPostChainage(pData)^.Val;
      CoordParams.ToRightPostMM:= (FEvents[I].SysCoord - DisToSysCoord(DisCoord)) * FHead.ScanStep div 100;
      CoordParams.RightIdx:= I;
      RightFlg:= True;
      Break;
    end;
  end;

  if not LeftFlg then // ��� ������ ����� - ��������� �� ��������� �������
  begin
    if FHead.MoveDir > 0 then
    begin
      CoordParams.LeftPostVal:= GetPrevCaPost(CoordSys,
                                              FHead.StartChainage,
                                              FHead.MoveDir); // �������� ����� ������� ��� �����
      CoordParams.ToLeftPostMM:= GetMMLenFromCaPost(CoordSys,
                                                    FHead.StartChainage) +
                                 DisToSysCoord(DisCoord) * FHead.ScanStep div 100;

      //      ���������
      //        �����                  StartChainage            DisCoord
      //          |                         |                       |
      //     ---- X <---------------------> X <-------------------> X ------------
      //          |     MMLenFromCaPost        DisToSys * ScanStep  |
      //          |                                                 |
      //          x <---------------- ToLeftPostMM ---------------> X

    end
    else
    begin
      CoordParams.LeftPostVal:= FHead.StartChainage; // �������� ����� ������� ��� �����
      CoordParams.LeftPostVal.YYY:= 0;
      CoordParams.LeftPostVal.XXX:= CoordParams.LeftPostVal.XXX + 1;

      CoordParams.ToLeftPostMM:= PostMMLen(CoordSys) -
                                 GetMMLenFromCaPost(CoordSys, FHead.StartChainage) +
                                 DisToSysCoord(DisCoord) * FHead.ScanStep div 100;

      //      LeftPostVal             StartChainage            DisCoord         RightPostVal
      //          |                         |                       |                |
      //     -----|-------------------------|-----------------------|----------------|-------
      //                                    |<- DisToSys*ScanStep ->|
      //                                    |<----------- MMLenFromCaPost ---------->|
      //          |<------------------------------ PostMMLen ----------------------->|
      //          |<----------------- ������� �������� ------------>|

    end;
    CoordParams.LeftIdx:= - 1;
  end;

  if not RightFlg  then // ��� ������ ������
  begin
    if FHead.MoveDir > 0 then
    begin
      CoordParams.RightPostVal:= CoordParams.LeftPostVal;         // �������� ����� - ������
      CoordParams.RightPostVal.XXX:= CoordParams.RightPostVal.XXX + 1;
      CoordParams.RightPostVal.YYY:= 0;
      CoordParams.ToRightPostMM:= PostMMLen(CoordSys) - CoordParams.ToLeftPostMM;

      //
      //        �����                    DisCoord               EndCaCrd         RightPostVal
      //          |                         |                       |                  |
      //     ---- x <---------------------> x-----------------------x------------------x----
      //          |     ToLeftPostMM        |                                          |
      //          x <--------------------------- PostMMLen --------------------------> x
      //                                    |                                          |
      //                                    x <------ PostMMLen - ToLeftPostMM ------> x

    end
    else
    begin
      CoordParams.RightPostVal:= CoordParams.LeftPostVal;
      CoordParams.RightPostVal.YYY:= 0;
      if CoordParams.RightPostVal.YYY = 0 then Dec(CoordParams.RightPostVal.XXX)
                                          else CoordParams.RightPostVal.YYY:= 0;
      CoordParams.ToRightPostMM:= PostMMLen(CoordSys) - CoordParams.ToLeftPostMM;
    end;
    CoordParams.RightIdx:= - 1;
  end;
end;

function TAviconDataSource.DisToCaCrd(DisCoord: Longint): TCaCrd;
var
  CoordParams: TCaCrdParams;

begin
  DisToCaCrdPrm(DisCoord, CoordParams);
  Result:= CaCrdParamsToCrd(CoordParams, Header.MoveDir);
end;

function TAviconDataSource.CaCrdToDis(CaCrd: TCaCrd; var DisCoord: Longint): Boolean;
var
  ID: Byte;
  I, J, Sys, Len: Integer;
  pData: PEventData;
  Val1, Val2: TCaCrd;
  DisCrd: Integer;
  t1: int64;
  t2: int64;
  t3: int64;

begin
  if (FHead.MoveDir > 0) then
  begin
    if CaCrd.XXX < FHead.StartChainage.XXX then // ���������� ������ ��������� ���������� �����
    begin
      DisCoord:= SysToDisCoord(Round((CaCrdToMM(CoordSys, CaCrd) - CaCrdToMM(CoordSys, FHead.StartChainage)) / (FHead.ScanStep / 100)));
      Result:= True;
      Exit;
    end;

    Val1:= CaCrd;
    Val1.XXX:= 0;

    DisCrd:= 0;                                 // ���������� ����� ��������� ���������� �����
    Val2:= FHead.StartChainage;
    if FHead.StartChainage.XXX = CaCrd.XXX then
    begin
      Val2.XXX:= 0;
      DisCoord:= SysToDisCoord(Round((CaCrdToMM(CoordSys, Val1) - CaCrdToMM(CoordSys, Val2)) / (FHead.ScanStep / 100)));
      Result:= True;
      Exit;
    end;

    for I:= 0 to EventCount - 1 do              // ���� ����� �������� "�����" �������� ����������
    begin
      GetEventData(I, ID, pData);
      if ID = EID_StolbChainage then
      begin
        Val2:= pCoordPostChainage(pData)^.Val;
        DisCrd:= Event[I].DisCoord;
        if Val2.XXX = CaCrd.XXX then
        begin
          Val2.XXX:= 0;
          DisCoord:= SysToDisCoord(DisToSysCoord(DisCrd) + Round((CaCrdToMM(CoordSys, Val1) - CaCrdToMM(CoordSys, Val2)) / (FHead.ScanStep / 100)));
          Result:= True;
          Exit;
        end;
      end;
    end;

    if CaCrd.XXX > Val2.XXX then // ���������� ������ ���������� ���������� ������
    begin
      Val1:= CaCrd;
      DisCoord:= DisCrd + SysToDisCoord(Round((CaCrdToMM(CoordSys, Val1) - CaCrdToMM(CoordSys, Val2)) / (FHead.ScanStep / 100)));
      Result:= True;
      Exit;
    end;

  end
  else
  begin

    if CaCrd.XXX > FHead.StartChainage.XXX then // ���������� ������ ��������� ���������� �����
    begin
      DisCoord:= SysToDisCoord(Round((CaCrdToMM(CoordSys, FHead.StartChainage) - CaCrdToMM(CoordSys, CaCrd)) / (FHead.ScanStep / 100)));
      Result:= True;
      Exit;
    end;

    Val1:= CaCrd;
    Val1.XXX:= 0;

    DisCrd:= 0;                                 // ���������� ����� ��������� ���������� �����
    Val2:= FHead.StartChainage;
    if FHead.StartChainage.XXX = CaCrd.XXX then
    begin
      Val2.XXX:= 0;
      DisCoord:= SysToDisCoord(Round((CaCrdToMM(CoordSys, Val2) - CaCrdToMM(CoordSys, Val1)) / (FHead.ScanStep / 100)));
      Result:= True;
      Exit;
    end;

    for I:= 0 to EventCount - 1 do              // ���� ����� �������� "�����" �������� ����������
    begin
      GetEventData(I, ID, pData);
      if ID = EID_StolbChainage then
      begin
        Val2:= pCoordPostChainage(pData)^.Val;
        DisCrd:= Event[I].DisCoord;
        if Val2.XXX = CaCrd.XXX + 1 then
        begin

          for J:= I + 1 to EventCount - 1 do // ���� �� ������ ����� ?
          begin
            GetEventData(J, ID, pData);
            if ID = EID_StolbChainage then           // ���� ����� ����������� �� ����
            begin
              Result:= True;
              Sys:= Round(CaCrdToMM(CoordSys, Val1) / (FHead.ScanStep / 100));
              DisCoord:= SysToDisCoord(DisToSysCoord(Event[J].DisCoord) - Sys);
              Result:= True;
              Exit;
            end;
          end;
                                             // ���, ����� ������� ��� ����� �������� PostMMLen ��
          Val2.XXX:= 0;
          Sys:= Round((PostMMLen(CoordSys) - CaCrdToMM(CoordSys, Val1)) / (FHead.ScanStep / 100));
          DisCoord:= SysToDisCoord(DisToSysCoord(DisCrd) + Sys);
          Result:= True;
          Exit;
        end;
      end;
    end;

    if CaCrd.XXX < Val2.XXX then // ���������� ������ ���������� ���������� ������
    begin
      Val1:= CaCrd;
      DisCoord:= DisCrd + SysToDisCoord(Round((CaCrdToMM(CoordSys, Val2) - CaCrdToMM(CoordSys, Val1)) / (FHead.ScanStep / 100)));
      Result:= True;
      Exit;
    end;

  end;
  Result:= False;
end;

// --------------------------------------------------------------------------------------

function TAviconDataSource.DisToCoordParams(DisCoord: Integer; var CrdParams: TCrdParams): Boolean;
begin
  CrdParams.Sys:= GetCoordSys;
  if GetCoordSys = csMetricRF then Result:= DisToMRFCrdPrm(DisCoord, CrdParams.MRFCrdParams)
                              else Result:= DisToCaCrdPrm(DisCoord, CrdParams.CaCrdParams);
end;

function TAviconDataSource.DisToRealCoord(DisCoord: Longint): TRealCoord;
begin
  Result.Sys:= GetCoordSys;
  if GetCoordSys = csMetricRF then Result.MRFCrd:= DisToMRFCrd(DisCoord)
                              else Result.CaCrd:= DisToCaCrd(DisCoord);
end;

function TAviconDataSource.RealToDisCoord(Crd: TRealCoord): Longint;
begin
  if Crd.Sys = csMetricRF then MRFCrdToDisCrd(Crd.MRFCrd, Result)
                          else CaCrdToDis(Crd.CaCrd, Result);
end;

// --------------------------------------------------------------------------------------

procedure TAviconDataSource.SaveNB;
begin
  if FModifyData then
    SaveExData(FExHdrLoadOk);
end;

// --------------------------------------------------------------------------------------

procedure TAviconDataSource.SavePiece(StDisCood, EdDisCood: Integer; SaveNB: Boolean; FileName: string);
(*
var
  I: Integer;
  DisCoord1: Int64;
  SysCoord1: Integer;
  Offset1: Integer;
  DisCoord2: Int64;
  SysCoord2: Integer;
  Offset2: Integer;
  LoadID: Byte;
  SkipBytes: Integer;
  OutDat: TAvk11DatDst;
  HSItem: THSItem;
  CurSysCoord: Integer;
  ByteOffset: Byte;
  ByteCoord: Byte;
  LongCoord: LongInt;
  LongOffset: LongInt;
  NewCoord: Integer;
  LastSaveOffset: Integer;
  LastSysCrd: Integer;
  BackLink: Integer;
  FID: Byte;
  Params: TMiasParams;
  R: RRail;
  Ch: Integer;
  Rel: TRealCoord;
  OutDat2: TAvk11DatSrc;
  Tmp: TFileNotebook_Ver4;
  CoordParams: TCoordParams;
  Buff: array[5..13] of Byte;
  RightIdx_: Integer;
//  BSHead: mBSAmplHead;
  *)
begin
end;

procedure TAviconDataSource.GenerateGPSPoints;
var
  ID: Byte;
  I, J: Integer;
  New: TGPSPoint;
  Coord: TRealCoord;
  pData: PEventData;
  str: string;

begin

  SetLength(GPSPoints, 0);
  for I:= 0 to GetEventCount - 1 do
  begin
    GetEventData(I, ID, pData);

    if ID = EID_GPSState then
    begin
      New.DisCoord:= Event[I].DisCoord;
      New.State:= Boolean(pedGPSState(pData)^.State);
      New.SatCount:= pedGPSState(pData)^.Reserv[0];
      New.Speed:= pedGPSState(pData)^.Reserv[1];

      SetLength(GPSPoints, Length(GPSPoints) + 1);
      GPSPoints[High(GPSPoints)]:= New;
    end;

    if ID = EID_GPSCoord then
    begin
      New.DisCoord:= Event[I].DisCoord;
      New.Lat:= pedGPSCoord(pData)^.Lat;
      New.Lon:= pedGPSCoord(pData)^.Lon;
      New.Speed:= 0;

      SetLength(GPSPoints, Length(GPSPoints) + 1);
      GPSPoints[High(GPSPoints)]:= New;
    end;

    if ID = EID_GPSCoord2 then
    begin
      New.DisCoord:= Event[I].DisCoord;
      New.Lat:= pedGPSCoord2(pData)^.Lat;
      New.Lon:= pedGPSCoord2(pData)^.Lon;
      New.Speed:= pedGPSCoord2(pData)^.Speed;

      SetLength(GPSPoints, Length(GPSPoints) + 1);
      GPSPoints[High(GPSPoints)]:= New;
    end;

  end;
end;

procedure TAviconDataSource.MakeNoteBookFromNordco;
var
  ID: Byte;
  I, J: Integer;
  pData: PEventData;

begin
  // -------- ������� NORDCO -----------------------------------------------------

  if Length(FNotebook) = 0 then
    for I:= 0 to EventCount - 1 do
      if Event[I].ID in [EID_NORDCO_Rec] then
      begin
        GetEventData(I, ID, pData);

        SetLength(FNotebook, Length(FNotebook) + 1);
        J:= High(FNotebook);

        FillChar(&FNotebook[J].ID, SizeOf(TFileNotebook_ver_7), 0);

        FNotebook[J].ID:= 3;  // ���: 1 - ��������� �������� / 2 - ������ �������� / 3 - NORDCO
        FNotebook[J].DisCoord:= pedNORDCO_Rec(pData)^.DisCoord;
        FNotebook[J].AssetType:= pedNORDCO_Rec(pData)^.AssetType; // NORDCO::AssetType - ��� �������� �������� ���� - �� ������: Left Rail, Right Rail
        FNotebook[J].Defect:= pedNORDCO_Rec(pData)^.Type_; // NORDCO::Type - ��� ������� �� UIC 712
        FNotebook[J].BlokNotText:= pedNORDCO_Rec(pData)^.Comment; // NORDCO::Comment - ����������
        FNotebook[J].DT:= pedNORDCO_Rec(pData)^.SurveyDate; // NORDCO::SurveyDate - ���� ����������� �������
        FNotebook[J].DT:= pedNORDCO_Rec(pData)^.DataEntryDate; // NORDCO::DataEntryDate - ���� ����������� ������ ��������
        FNotebook[J].Line:= pedNORDCO_Rec(pData)^.Line; // �������� ��� ������������� �������������� �����
        FNotebook[J].Track:= pedNORDCO_Rec(pData)^.Track; // ������������� ����, � ������� ��������� ������
        FNotebook[J].LocationFrom:= pedNORDCO_Rec(pData)^.LocationFrom; // ������ ������������� �������
        FNotebook[J].LocationTo:= pedNORDCO_Rec(pData)^.LocationTo; // ����� ������������� �������. � ������ ��������� ������� �� �� �����, ��� ����  "Location From"
        FNotebook[J].LatitudeFrom:= pedNORDCO_Rec(pData)^.LatitudeFrom; // GPS-������ ��� ���� "Location From"
        FNotebook[J].LatitudeTo:= pedNORDCO_Rec(pData)^.LatitudeTo; // GPS-������ ��� ���� "Location To"
        FNotebook[J].LongitudeFrom:= pedNORDCO_Rec(pData)^.LongitudeFrom; // GPS-������� ��� ���� "Location From"
        FNotebook[J].LongitudeTo:= pedNORDCO_Rec(pData)^.LongitudeTo; // GPS-������� ��� ���� "Location To"
        FNotebook[J].Source:= pedNORDCO_Rec(pData)^.Source; // ����� ����������� ������� - �� ������: Ultrasonic Trolley, Visual Inspection
        FNotebook[J].Status:= pedNORDCO_Rec(pData)^.Status; // ��� ���� ���������, ������ �� ������ (�.�. ������ �� ����) - �� ������: Open, Closed
        FNotebook[J].IsFailure:= pedNORDCO_Rec(pData)^.IsFailure; // ���� ���������� ������ ������� �����, �� ��� ���� ������ ����� �������� TRUE. ������� ����� ������� ����� ������, ������� �������� ����������� ��������
        FNotebook[J].Size:= pedNORDCO_Rec(pData)^.Size; // ������ ������� ���  ����������� �������. ��������, ���� ������ ��� �������, �� �� �����.
        FNotebook[J].Size2:= pedNORDCO_Rec(pData)^.Size2; // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
        FNotebook[J].Size3:= pedNORDCO_Rec(pData)^.Size3; // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
        FNotebook[J].UnitofMeasurement:= pedNORDCO_Rec(pData)^.UnitofMeasurement; // ��. ��������� ���� "Size"
        FNotebook[J].UnitofMeasurement2:= pedNORDCO_Rec(pData)^.UnitofMeasurement2; // ��. ��������� ���� "Size2"
        FNotebook[J].UnitofMeasurement3:= pedNORDCO_Rec(pData)^.UnitofMeasurement3; // ��. ��������� ���� "Size3"
        FNotebook[J].Operator_:= pedNORDCO_Rec(pData)^.Operator_; // ��������
        FNotebook[J].Device:= pedNORDCO_Rec(pData)^.Device; // ������

        FNotebook[J].Zoom:= 400; // �������
        FNotebook[J].Reduction:= 2;
        FNotebook[J].ShowChGate_:= True;
        FNotebook[J].ShowChSettings_:= True;
        FNotebook[J].ShowChInfo_:= True;
        FNotebook[J].ViewMode:= 1; // ����� �����������: 1 - � ��������� / 2 - � ���� ������
        FNotebook[J].AmplTh:= 0; // ����� �����������
        FNotebook[J].AmplDon:= 0; // ��������� ������� ������� 0 - ����
        FNotebook[J].ViewChannel:= 0; // ������: 0 - ��� / 1 - ���������� / 2 - �����������
        FNotebook[J].ViewLine:= 0;

        FModifyData:= True;
      end;

end;

{
function TAviconDataSource.GetGPSCoord(var Coord: mGPSCoord; DisCoord: Integer): Boolean;
var
  ID: Byte;
  I, J: Integer;
  New: TGPSPoint;
  pData: PEventData;
  Flg1, Flg2: Boolean;

  LeftIdx, RightIdx: Integer;
  SameCoord: TIntegerDynArray;
  LeftEvent: TFileEventData;
  RightEvent: TFileEventData;

begin
  Result:= False;
  GetNearestEventIdx(DisCoord, LeftIdx, RightIdx, SameCoord);

  Flg1:= False;
  Flg2:= False;
  for I:= LeftIdx downto 0 do
  begin
    GetEventData(I, ID, pData);
    if ID = EID_GPSCoord then
    begin
      LeftEvent:= Event[I];
      Flg1:= True;
      Break;
    end;
  end;

  for I:= LeftIdx + 1 to GetEventCount - 1 do
  begin
    GetEventData(I, ID, pData);
    if ID = EID_GPSCoord then
    begin
      RightEvent:= Event[I];
      Flg2:= True;
      Break;
    end;
  end;

  if Flg1 or Flg2 then
  begin
    if (Abs(DisToSysCoord(DisCoord) - LeftEvent.Event.SysCoord) <
        Abs(DisToSysCoord(DisCoord) - RightEvent.Event.SysCoord)) then
    begin
      Coord.Lat:= InvertGPSSingle(pGPSCoord(@LeftEvent.Data)^.Lat);
      Coord.Lon:= InvertGPSSingle(pGPSCoord(@LeftEvent.Data)^.Lon);
      Result:= True;
    end
    else
    begin
      Coord.Lat:= InvertGPSSingle(pGPSCoord(@RightEvent.Data)^.Lat);
      Coord.Lon:= InvertGPSSingle(pGPSCoord(@RightEvent.Data)^.Lon);
      Result:= True;
    end;
  end;
end;
}

initialization

  ConfigList:= TDataFileConfigList.Create('');

end.
