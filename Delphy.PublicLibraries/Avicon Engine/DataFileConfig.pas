unit DataFileConfig;

{$DEFINE ALLBOTTOM}

interface

uses
  AviconTypes, Classes, Types, Math, SysUtils;

type

(*


//------------------------< �������� ������� ����������� >---------------------------------------

  DBName:  array [0..15] of string =  ('-12', '-10', '-8', '-6', '-4', '-2', '0', '2', '4', '6', '8', '10', '12', '14', '16', '18');
  DBValue: array [0..15] of Integer = ( -12,   -10,   -8,   -6,   -4,   -2,   0,   2,   4,   6,   8,   10,   12,   14,   16,   18);
  FDeviceID: uint64;          // ������������� ������������
  DeviceName: string;
  MinRail: TRail;
  MaxRail: TRail;
  ScanChannels: array of TScanChannelItem;
  EvalChannels: array of TEvalChannelItem;
  ContChannels: array of TContChannelItem;
 *)

{  TEvalInfo = record
    InfoSide: TInfoSide;           // ���������� � ���� ������ ���������� ������� ������
    InfoPos: TInfoPos;
  end; }


(*
  TContChannelItem = record         // ����������� �����
    Number: Integer;                // ����� ������
    ScanChNum: Integer;             // ����� ��������� � ������� ������
  end;
*)

  T_DFC_ZeroProbeChannelsViewMode = (zpAll, zpFirst, zbSecond);

  TDataFileConfig = class
  private
    FDeviceID: TAviconID;       // ������������� �������
    FDeviceVer: Byte;           // ������ �������
    FDeviceName: string;        // �������� ������������
    FMinRail: TRail;
    FMaxRail: TRail;

    // ? - ����� ����������� �� ���������
    // ? - ���� ��������




    FScanChannels: array of TScanChannelItem; // ������ �-���������
    FEvalChannels: array of TEvalChannelItem; // ������ ������
    FHandChannels: array of THandChannelItem; // ������ ������� ��������
    FBScanLines: array of TBScanLine;   // ���� ������ �-���������
//    FEvalInfo: array of TEvalInfo;
    FAmplName: array of TAmplItem;

  protected

    function GetDeviceID: TAviconID;       // ������������� �������
    function GetDeviceVer: Byte;           // ������
    function GetMinRail(): TRail;
    function GetMaxRail(): TRail;
    function GetEvalChannelCount(): Integer;
    function GetScanChannelCount(): Integer;
    function GetHandChannelCount(): Integer;
    function GetEvalChannelItemByIdx(Index: Integer): TEvalChannelItem;
    function GetScanChannelItemByIdx(Index: Integer): TScanChannelItem;
    function GetHandChannelItemByIdx(Index: Integer): THandChannelItem;
    function GetBScanLinesCount: Integer;
    function GetBScanLineItem(Index: Integer): TBScanLine;
//    function GetBScanLineItem(Lines, Item: Integer): TBScanLine;
    function GetMinScanChNumber: Integer;
    function GetMaxScanChNumber: Integer;
    function GetMinEvalChNumber: Integer;
    function GetMaxEvalChNumber: Integer;
    function GetAmplValue(Code: Integer): Integer;
    function GetMinAmplCode: Integer;
    function GetMaxAmplCode: Integer;

    function GetEvalChByNumber(Number: Integer): TEvalChannelItem;
    function GetScanChByNumber(Number: Integer): TScanChannelItem;
    function GetHandChByNumber(Number: Integer): THandChannelItem;

    procedure AddHandScanChannels(var HandChannels: array of THandChannelItem);


//    function GetEvalInfo(Index: Integer): TEvalInfo;
//    function GetEvalInfoCount: Integer;

  public
    ChannelIDList: array [rLeft..rRight, 0..MaxChannel_] of Integer;
    EvalChannels2: array [0..MaxChannel_] of TEvalChannelItem2;

    constructor Create(Temp: TAviconID; DeviceVer: Integer = - 1);
    destructor Destroy();

    procedure SetZeroProbeChannelsViewMode(Mode: T_DFC_ZeroProbeChannelsViewMode);

    property DeviceID: TAviconID read GetDeviceID;          // ��� ������������
    property DeviceVer: Byte read GetDeviceVer;          // ������
    property MinRail: TRail read GetMinRail;
    property MaxRail: TRail read GetMaxRail;

    property EvalChannelByIdx[Index: Integer]: TEvalChannelItem read GetEvalChannelItemByIdx;
    property EvalChannelCount: Integer read GetEvalChannelCount;
    property EvalChannelByNum[Number: Integer]: TEvalChannelItem read GetEvalChByNumber;
    property MinEvalChNumber: Integer read GetMinEvalChNumber;
    property MaxEvalChNumber: Integer read GetMaxEvalChNumber;

    property ScanChannelByIdx[Index: Integer]: TScanChannelItem read GetScanChannelItemByIdx;
    property ScanChannelCount: Integer read GetScanChannelCount;
    property ScanChannelByNum[Number: Integer]: TScanChannelItem read GetScanChByNumber;
    property MinScanChNumber: Integer read GetMinScanChNumber;
    property MaxScanChNumber: Integer read GetMaxScanChNumber;

    function GetEvalsChIdxByScanChIdx(ScanChannelIndex: Integer): TIntegerDynArray;
    function GetEvalsChIdxByScanChNum(ScanChannelNum: Integer): TIntegerDynArray;
    function GetSecondEvalByEval(EvalChannelIndex: Integer; var ResEvalChannelIndex: Integer): Boolean;
    function GetScanChByEvalChNum(EvalChNum: Integer): TScanChannelItem;

    property HandChannelByIdx[Index: Integer]: THandChannelItem read GetHandChannelItemByIdx;
    property HandChannelCount: Integer read GetHandChannelCount;
    property HandChannelByNum[Number: Integer]: THandChannelItem read GetHandChByNumber;

    property BScanLinesCount: Integer read GetBScanLinesCount;
//    property BScanLineItem[Lines, Item: Integer]: TBScanLine read GetBScanLineItem;
    property BScanLineItem[Index: Integer]: TBScanLine read GetBScanLineItem;

    property AmplValue[Value: Integer]: Integer read GetAmplValue;
    property MinAmplCode: Integer read GetMinAmplCode;
    property MaxAmplCode: Integer read GetMaxAmplCode;
    property DeviceName: string read FDeviceName;        // �������� ������������


//    property EvalInfo[Index: Integer]: TEvalInfo read GetEvalInfo;
//    property EvalInfoCount: Integer read GetEvalInfoCount;
//    property MaxScanChannelNumber: Integer read ;
  end;

  TDataFileConfigList = class
  private
    FList: array of TDataFileConfig;

  protected
    function GetItem(Index: Integer): TDataFileConfig;
    function GetCount: Integer;

  public
    constructor Create(FileName: string);
    destructor Destroy();
    function GetItemByID(FDeviceID: TAviconID; DeviceVer: Integer): TDataFileConfig;
    property Item[Index: Integer]: TDataFileConfig read GetItem;
    property Count: Integer read GetCount;
  end;

{
����������� ������:
������ �������
}

implementation

// --------- TDataFileConfig ---------------------------------------------------


constructor TDataFileConfig.Create(Temp: TAviconID; DeviceVer: Integer = - 1);

//  -75 -50 -25  25  50  75

const
  MIGViewEnterAngle: array [0..23] of Integer = (0, 0, 58, 58, 70, 70, 45, 45, 45, 45, 70, 70, 45, 45, 70, 70, 70, 70, 70, 70, 70, 70, 70, 70);
  MIGViewTurnAngle: array [0..23] of Integer = (0, 0,  0,  0,  0,  0,  0,  0, 13, 13,  0,  0, 13, 13,  0,  0, 30, 30, 20,  0, 30, 30, 20,  0);
  MIGViewPosition: array [0..23] of Integer = (187, -187, -62,  62,  12, -12, -32, 32, 112, -112, -12, 12, 112, -112, -12, 12, 87, -87, 62, -62, 87, -87, 62, -62);
  MIGViewBScanGateMax: array [0..23] of Integer = (71, 71, 141, 141, 91, 91, 201, 201, 81, 81, 151, 151, 81, 81, 151, 151, 101, 101, 101, 101, 101, 101, 101, 101);
  MIGViewZone: array [0..23] of TInspectionZone = (izAll, izAll, izHead, izHead, izHead, izHead, izAll, izAll, izHead, izHead, izHead, izHead, izHead, izHead, izHead, izHead, izBase, izBase, izBase, izBase, izBase, izBase, izBase, izBase);

var
  R: TRail;
  I, J: Integer;
  GEISMAR_Shift: Integer;

begin
  GEISMAR_Shift:= 0;

  if CompareMem(@Temp, @Avicon16Scheme1, 8) or
     CompareMem(@Temp, @Avicon16Scheme3, 8) or
     CompareMem(@Temp, @Avicon16Scheme2, 8) then GEISMAR_Shift:= 5;

  if not CompareMem(@Temp, @Avicon15Scheme1, 8) then
  begin

    SetLength(FAmplName, 13);

    FAmplName[0].Code:= 3;
    FAmplName[0].Value:= -6;

    FAmplName[1].Code:= 4;
    FAmplName[1].Value:= -4;

    FAmplName[2].Code:= 5;
    FAmplName[2].Value:= -2;

    FAmplName[3].Code:= 6;
    FAmplName[3].Value:= 0;

    FAmplName[4].Code:= 7;
    FAmplName[4].Value:= 2;

    FAmplName[5].Code:= 8;
    FAmplName[5].Value:= 4;

    FAmplName[6].Code:= 9;
    FAmplName[6].Value:= 6;

    FAmplName[7].Code:= 10;
    FAmplName[7].Value:= 8;

    FAmplName[8].Code:= 11;
    FAmplName[8].Value:= 10;

    FAmplName[9].Code:= 12;
    FAmplName[9].Value:= 12;

    FAmplName[10].Code:= 13;
    FAmplName[10].Value:= 14;

    FAmplName[11].Code:= 14;
    FAmplName[11].Value:= 16;

    FAmplName[12].Code:= 15;
    FAmplName[12].Value:= 18;
  end
  else
  begin

    SetLength(FAmplName, 16);

    FAmplName[0].Code:= 0;
    FAmplName[0].Value:= -12;

    FAmplName[1].Code:= 1;
    FAmplName[1].Value:= -10;

    FAmplName[2].Code:= 2;
    FAmplName[2].Value:= -8;

    FAmplName[3].Code:= 3;
    FAmplName[3].Value:= -6;

    FAmplName[4].Code:= 4;
    FAmplName[4].Value:= -4;

    FAmplName[5].Code:= 5;
    FAmplName[5].Value:= -2;

    FAmplName[6].Code:= 6;
    FAmplName[6].Value:= 0;

    FAmplName[7].Code:= 7;
    FAmplName[7].Value:= 2;

    FAmplName[8].Code:= 8;
    FAmplName[8].Value:= 4;

    FAmplName[9].Code:= 9;
    FAmplName[9].Value:= 6;

    FAmplName[10].Code:= 10;
    FAmplName[10].Value:= 8;

    FAmplName[11].Code:= 11;
    FAmplName[11].Value:= 10;

    FAmplName[12].Code:= 12;
    FAmplName[12].Value:= 12;

    FAmplName[13].Code:= 13;
    FAmplName[13].Value:= 14;

    FAmplName[14].Code:= 14;
    FAmplName[14].Value:= 16;

    FAmplName[15].Code:= 15;
    FAmplName[15].Value:= 18;
  end;

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

  if CompareMem(@Temp, @Avicon16Scheme1, 8) or
     CompareMem(@Temp, @Avicon14Scheme1, 8) then

  begin
    FDeviceID:= Temp;              // ��� ������������
    FDeviceVer:= 3;
    FMinRail:= rLeft;
    FMaxRail:= rRight;

    if CompareMem(@Temp, @Avicon16Scheme1, 8) then FDeviceName:= 'Avicon16Scheme1';
    if CompareMem(@Temp, @Avicon14Scheme1, 8) then FDeviceName:= 'Avicon14Scheme1';

  // ------------------------------------------------------------------------------

    ChannelIDList[rLeft, 0]:= MIRRORSHADOW_0;
    ChannelIDList[rLeft, 1]:= ECHO_0;
    ChannelIDList[rLeft, 2]:= FORWARD_UWORK_ECHO_58;
    ChannelIDList[rLeft, 3]:= BACKWARD_UWORK_ECHO_58;
    ChannelIDList[rLeft, 4]:= FORWARD_ECHO_70;
    ChannelIDList[rLeft, 5]:= BACKWARD_ECHO_70;
    ChannelIDList[rLeft, 6]:= FORWARD_ECHO_42_WEB;
    ChannelIDList[rLeft, 7]:= BACKWARD_ECHO_42_WEB;
    ChannelIDList[rLeft, 8]:= FORWARD_ECHO_42_BASE;
    ChannelIDList[rLeft, 9]:= BACKWARD_ECHO_42_BASE;
    ChannelIDList[rLeft, 10]:= FORWARD_WORK_ECHO_58;
    ChannelIDList[rLeft, 11]:= BACKWARD_WORK_ECHO_58;

    ChannelIDList[rRight, 0]:= MIRRORSHADOW_0;
    ChannelIDList[rRight, 1]:= ECHO_0;
    ChannelIDList[rRight, 2]:= FORWARD_WORK_ECHO_58;
    ChannelIDList[rRight, 3]:= BACKWARD_WORK_ECHO_58;
    ChannelIDList[rRight, 4]:= FORWARD_ECHO_70;
    ChannelIDList[rRight, 5]:= BACKWARD_ECHO_70;
    ChannelIDList[rRight, 6]:= FORWARD_ECHO_42_WEB;
    ChannelIDList[rRight, 7]:= BACKWARD_ECHO_42_WEB;
    ChannelIDList[rRight, 8]:= FORWARD_ECHO_42_BASE;
    ChannelIDList[rRight, 9]:= BACKWARD_ECHO_42_BASE;
    ChannelIDList[rRight, 10]:= FORWARD_UWORK_ECHO_58;
    ChannelIDList[rRight, 11]:= BACKWARD_UWORK_ECHO_58;

  // ------------------------------------------------------------------------------

    with EvalChannels2[FORWARD_WORK_ECHO_58] do   begin InfoSide:= isLeft;  InfoPos:= ipLeft;   end;
    with EvalChannels2[FORWARD_UWORK_ECHO_58] do  begin InfoSide:= isLeft;  InfoPos:= ipRight;  end;

    with EvalChannels2[BACKWARD_WORK_ECHO_58] do  begin InfoSide:= isRight; InfoPos:= ipLeft;   end;
    with EvalChannels2[BACKWARD_UWORK_ECHO_58] do begin InfoSide:= isRight; InfoPos:= ipRight;  end;

    with EvalChannels2[FORWARD_ECHO_70] do        begin InfoSide:= isLeft;  InfoPos:= ipCentre; end;
    with EvalChannels2[BACKWARD_ECHO_70] do       begin InfoSide:= isRight; InfoPos:= ipCentre; end;

    with EvalChannels2[FORWARD_ECHO_42_WEB] do    begin InfoSide:= isLeft;  InfoPos:= ipRight;  end;
    with EvalChannels2[FORWARD_ECHO_42_BASE] do   begin InfoSide:= isLeft;  InfoPos:= ipLeft;   end;
    with EvalChannels2[BACKWARD_ECHO_42_WEB] do   begin InfoSide:= isRight; InfoPos:= ipRight;  end;
    with EvalChannels2[BACKWARD_ECHO_42_BASE] do  begin InfoSide:= isRight; InfoPos:= ipLeft;   end;

    with EvalChannels2[ECHO_0] do                 begin InfoSide:= isRight;  InfoPos:= ipCentre; end;
    with EvalChannels2[MIRRORSHADOW_0] do         begin InfoSide:= isLeft; InfoPos:= ipCentre; end;

    // ------------ ���� ������ �-��������� ------------------

    SetLength(FBScanLines, 8);

    FBScanLines[0].R:= rLeft;
    FBScanLines[0].Use:= True;
    SetLength(FBScanLines[0].Items, 4);

    FBScanLines[0].Items[0].ScanChIdx:= 7;
    FBScanLines[0].Items[1].ScanChIdx:= 8;
    FBScanLines[0].Items[2].ScanChIdx:= 1;
    FBScanLines[0].Items[3].ScanChIdx:= 2;
    FBScanLines[1].R:= rLeft;
    FBScanLines[1].Use:= True;
    SetLength(FBScanLines[1].Items, 2);
    FBScanLines[1].Items[0].ScanChIdx:= 3;
    FBScanLines[1].Items[1].ScanChIdx:= 4;

    FBScanLines[2].R:= rLeft;
    FBScanLines[2].Use:= True;
    SetLength(FBScanLines[2].Items, 2);
    FBScanLines[2].Items[0].ScanChIdx:= 5;
    FBScanLines[2].Items[1].ScanChIdx:= 6;

    FBScanLines[3].R:= rLeft;
    FBScanLines[3].Use:= True;
    SetLength(FBScanLines[3].Items, 1);
    FBScanLines[3].Items[0].ScanChIdx:= 0;

    // ---------------------------------------------------------------------------------

    FBScanLines[4].R:= rRight;
    FBScanLines[4].Use:= True;
    SetLength(FBScanLines[4].Items, 4);
    FBScanLines[4].Items[0].ScanChIdx:= 1;
    FBScanLines[4].Items[1].ScanChIdx:= 2;
    FBScanLines[4].Items[2].ScanChIdx:= 7;
    FBScanLines[4].Items[3].ScanChIdx:= 8;

    FBScanLines[5].R:= rRight;
    FBScanLines[5].Use:= True;
    SetLength(FBScanLines[5].Items, 2);
    FBScanLines[5].Items[0].ScanChIdx:= 3;
    FBScanLines[5].Items[1].ScanChIdx:= 4;

    FBScanLines[6].R:= rRight;
    FBScanLines[6].Use:= True;
    SetLength(FBScanLines[6].Items, 2);
    FBScanLines[6].Items[0].ScanChIdx:= 5;
    FBScanLines[6].Items[1].ScanChIdx:= 6;

    FBScanLines[7].R:= rRight;
    FBScanLines[7].Use:= True;
    SetLength(FBscanLines[7].Items, 1);
    FBScanLines[7].Items[0].ScanChIdx:= 0;

    // ------------ ������ ������� �������� ------------

    AddHandScanChannels(FHandChannels);

    // ------------ ������ �-��������� � ������ ������ ------------

    SetLength(FScanChannels, 9);
    SetLength(FEvalChannels, 12);

    FScanChannels[0].Number:= 1;  //  0 ����
    FScanChannels[0].Position:= 50 + GEISMAR_Shift;
    FScanChannels[0].EnterAngle:= 0;
    FScanChannels[0].TurnAngle:= 0;
    FScanChannels[0].BScanGateMin:= 5;
    FScanChannels[0].BScanGateMax:= 70;
    FScanChannels[0].DelayMultiply:= 3;
    FScanChannels[0].Delta:= 0;
    FScanChannels[0].Gran:= _None;

      FEvalChannels[0].Number:= 0;     // 0 ���
      FEvalChannels[0].ScanChNum:= 1;
      FEvalChannels[0].Method:= imMirrorShadow;
      FEvalChannels[0].Zone:= izAll;
      FEvalChannels[0].ShortName:= 'SHORT_0_MIRRORSHADOW';
      FEvalChannels[0].FullName:= 'FULL_0_MIRRORSHADOW';
      FEvalChannels[0].DirColor:= + 1;

      FEvalChannels[1].Number:= 1;  // 0 ���
      FEvalChannels[1].ScanChNum:= 1;
      FEvalChannels[1].Method:= imEcho;
      FEvalChannels[1].Zone:= izAll;
      FEvalChannels[1].ShortName:= 'SHORT_0_ECHO';
      FEvalChannels[1].FullName:= 'FULL_0_ECHO';
      FEvalChannels[1].DirColor:= - 1;

    FScanChannels[1].Number:= 2;  // 58 ����������
    FScanChannels[1].EnterAngle:= 58;
    FScanChannels[1].TurnAngle:= 34;
    FScanChannels[1].Position:= - 25 - GEISMAR_Shift;
    FScanChannels[1].BScanGateMin:= 20;
    FScanChannels[1].BScanGateMax:= 180;
    FScanChannels[1].DelayMultiply:= 1;
    FScanChannels[1].Delta:= - 90;
    FScanChannels[1].Gran:= _UnWork_Left__Work_Right;

      FEvalChannels[2].Number:= 2;     // 58 ����������
      FEvalChannels[2].ScanChNum:= 2;
      FEvalChannels[2].Method:= imEcho;
      FEvalChannels[2].Zone:= izHead;
      FEvalChannels[2].ShortName:= 'SHORT_+58_ECHO_WORK';
      FEvalChannels[2].FullName:= 'FULL_+58_ECHO_WORK';
      FEvalChannels[2].DirColor:= + 1;

    FScanChannels[2].Number:= 3;  // 58 �����������
    FScanChannels[2].EnterAngle:= - 58;
    FScanChannels[2].TurnAngle:= 34;
    FScanChannels[2].Position:= - 75 - GEISMAR_Shift;
    FScanChannels[2].BScanGateMin:= 20;
    FScanChannels[2].BScanGateMax:= 180;
    FScanChannels[2].DelayMultiply:= 1;
    FScanChannels[2].Delta:= + 90;
    FScanChannels[2].Gran:= _UnWork_Left__Work_Right;

      FEvalChannels[3].Number:= 3;     // 58 �����������
      FEvalChannels[3].ScanChNum:= 3;
      FEvalChannels[3].Method:= imEcho;
      FEvalChannels[3].Zone:= izHead;
      FEvalChannels[3].ShortName:= 'SHORT_-58_ECHO_WORK';
      FEvalChannels[3].FullName:= 'FULL_-58_ECHO_WORK';
      FEvalChannels[3].DirColor:= - 1;

    FScanChannels[3].Number:= 4;  // 70 ���� ����������
    FScanChannels[3].EnterAngle:= 70;
    FScanChannels[3].TurnAngle:= 0;
    FScanChannels[3].Position:= 75 + GEISMAR_Shift;
    FScanChannels[3].BScanGateMin:= 5;
    FScanChannels[3].BScanGateMax:= 95;
    FScanChannels[3].DelayMultiply:= 1;
    FScanChannels[3].Delta:= - 80;
    FScanChannels[3].Gran:= _None;

      FEvalChannels[4].Number:= 4;     // 70 ����������
      FEvalChannels[4].ScanChNum:= 4;
      FEvalChannels[4].Method:= imEcho;
      FEvalChannels[4].Zone:= izHead;
      FEvalChannels[4].ShortName:= 'SHORT_+70_ECHO';
      FEvalChannels[4].FullName:= 'FULL_+70_ECHO';
      FEvalChannels[4].DirColor:= + 1;

    FScanChannels[4].Number:= 5;  // 70 ���� ����������
    FScanChannels[4].EnterAngle:= - 70;
    FScanChannels[4].TurnAngle:= 0;
    FScanChannels[4].Position:= 25 + GEISMAR_Shift;
    FScanChannels[4].BScanGateMin:= 5;
    FScanChannels[4].BScanGateMax:= 95;
    FScanChannels[4].DelayMultiply:= 1;
    FScanChannels[4].Delta:= 80;
    FScanChannels[4].Gran:= _None;

      FEvalChannels[5].Number:= 5;     // 70 �����������
      FEvalChannels[5].ScanChNum:= 5;
      FEvalChannels[5].Method:= imEcho;
      FEvalChannels[5].Zone:= izHead;
      FEvalChannels[5].ShortName:= 'SHORT_-70_ECHO';
      FEvalChannels[5].FullName:= 'FULL_-70_ECHO';
      FEvalChannels[5].DirColor:= - 1;

    FScanChannels[5].Number:= 6;  // 42 ���� ����������
    FScanChannels[5].EnterAngle:= 42;
    FScanChannels[5].TurnAngle:= 0;
    FScanChannels[5].Position:= - 50 - GEISMAR_Shift;
    FScanChannels[5].BScanGateMin:= 10;
    FScanChannels[5].BScanGateMax:= 163;
    FScanChannels[5].DelayMultiply:= 1;
    FScanChannels[5].Delta:= - 30;
    FScanChannels[5].Gran:= _None;

      FEvalChannels[6].Number:= 6;      // 42 ����������
      FEvalChannels[6].ScanChNum:= 6;
      FEvalChannels[6].Method:= imEcho;
      FEvalChannels[6].Zone:= izWeb;
      FEvalChannels[6].ShortName:= 'SHORT_+42_ECHO_NEAR';
      FEvalChannels[6].FullName:= 'FULL_+42_ECHO_NEAR';
      FEvalChannels[6].DirColor:= + 1;

      FEvalChannels[8].Number:= 8;      // 42 ����������
      FEvalChannels[8].ScanChNum:= 6;
      FEvalChannels[8].Method:= imEcho;
      FEvalChannels[8].Zone:= izBase;
      FEvalChannels[8].ShortName:= 'SHORT_+42_ECHO_FAR';
      FEvalChannels[8].FullName:= 'FULL_+42_ECHO_FAR';
      FEvalChannels[8].DirColor:= + 1;

    FScanChannels[6].Number:= 7;  // 42 ���� �����������
    FScanChannels[6].EnterAngle:= - 42;
    FScanChannels[6].TurnAngle:= 0;
    FScanChannels[6].Position:= - 50 - GEISMAR_Shift;
    FScanChannels[6].BScanGateMin:= 10;
    FScanChannels[6].BScanGateMax:= 163;
    FScanChannels[6].DelayMultiply:= 1;
    FScanChannels[6].Delta:= + 30;
    FScanChannels[6].Gran:= _None;

      FEvalChannels[7].Number:= 7;      // 42 �����������
      FEvalChannels[7].ScanChNum:= 7;
      FEvalChannels[7].Method:= imEcho;
      FEvalChannels[7].Zone:= izWeb;
      FEvalChannels[7].ShortName:= 'SHORT_-42_ECHO_NEAR';
      FEvalChannels[7].FullName:= 'FULL_-42_ECHO_NEAR';
      FEvalChannels[7].DirColor:= - 1;

      FEvalChannels[9].Number:= 9;      // 42 �����������
      FEvalChannels[9].ScanChNum:= 7;
      FEvalChannels[9].Method:= imEcho;
      FEvalChannels[9].Zone:= izBase;
      FEvalChannels[9].ShortName:= 'SHORT_-42_ECHO_FAR';
      FEvalChannels[9].FullName:= 'FULL_-42_ECHO_FAR';
      FEvalChannels[9].DirColor:= - 1;

    FScanChannels[7].Number:= 10;  // 58 ����������
    FScanChannels[7].EnterAngle:= 58;
    FScanChannels[7].TurnAngle:= - 34;
    FScanChannels[7].Position:= - 25 - GEISMAR_Shift;
    FScanChannels[7].BScanGateMin:= 20;
    FScanChannels[7].BScanGateMax:= 180;
    FScanChannels[7].DelayMultiply:= 1;
    FScanChannels[7].Delta:= - 90;
    FScanChannels[7].Gran:= _Work_Left__UnWork_Right;

      FEvalChannels[10].Number:= 10;    // 58 ����������
      FEvalChannels[10].ScanChNum:= 10;
      FEvalChannels[10].Method:= imEcho;
      FEvalChannels[10].Zone:= izHead;
      FEvalChannels[10].ShortName:= 'SHORT_+58_ECHO_NOWORK';
      FEvalChannels[10].FullName:= 'FULL_+58_ECHO_NOWORK';
      FEvalChannels[10].DirColor:= + 1;

    FScanChannels[8].Number:= 11;  // 58 �����������
    FScanChannels[8].EnterAngle:= - 58;
    FScanChannels[8].TurnAngle:= - 34;
    FScanChannels[8].Position:= - 75 - GEISMAR_Shift;
    FScanChannels[8].BScanGateMin:= 20;
    FScanChannels[8].BScanGateMax:= 180;
    FScanChannels[8].DelayMultiply:= 1;
    FScanChannels[8].Delta:= + 90;
    FScanChannels[8].Gran:= _Work_Left__UnWork_Right;

      FEvalChannels[11].Number:= 11;    // 58 �����������
      FEvalChannels[11].ScanChNum:= 11;
      FEvalChannels[11].Method:= imEcho;
      FEvalChannels[11].Zone:= izHead;
      FEvalChannels[11].ShortName:= 'SHORT_-58_ECHO_NOWORK';
      FEvalChannels[11].FullName:= 'FULL_-58_ECHO_NOWORK';
      FEvalChannels[11].DirColor:= - 1;

    for I:= 0 to High(FScanChannels) do
      FScanChannels[I].BScanDuration:= FScanChannels[I].BScanGateMax - FScanChannels[I].BScanGateMin + 1;

 end
 else

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

  if CompareMem(@Temp, @Avicon16Scheme2, 8) then
  begin
    FDeviceID:= Avicon16Scheme2; // ��� ������������
    FDeviceVer:= 3;
    FDeviceName:= 'Avicon16Scheme2';

    FMinRail:= rLeft;
    FMaxRail:= rRight;

  // ------------------------------------------------------------------------------

    ChannelIDList[rLeft, 0]:=  MIRRORSHADOW_0;
    ChannelIDList[rLeft, 1]:=  ECHO_0;
    ChannelIDList[rLeft, 2]:=  BACKWARD_UWORK_MIRROR_58;
    ChannelIDList[rLeft, 3]:=  BACKWARD_UWORK_ECHO_58;
    ChannelIDList[rLeft, 4]:=  FORWARD_ECHO_70;
    ChannelIDList[rLeft, 5]:=  BACKWARD_ECHO_70;
    ChannelIDList[rLeft, 6]:=  FORWARD_ECHO_42_WEB;
    ChannelIDList[rLeft, 8]:=  FORWARD_ECHO_42_BASE;
    ChannelIDList[rLeft, 7]:=  BACKWARD_ECHO_42_WEB;
    ChannelIDList[rLeft, 9]:=  BACKWARD_ECHO_42_BASE;
    ChannelIDList[rLeft, 10]:= BACKWARD_WORK_MIRROR_58;
    ChannelIDList[rLeft, 11]:= BACKWARD_WORK_ECHO_58;

    ChannelIDList[rRight, 0]:=  MIRRORSHADOW_0;
    ChannelIDList[rRight, 1]:=  ECHO_0;
    ChannelIDList[rRight, 2]:=  BACKWARD_WORK_MIRROR_58;
    ChannelIDList[rRight, 3]:=  BACKWARD_WORK_ECHO_58;
    ChannelIDList[rRight, 4]:=  FORWARD_ECHO_70;
    ChannelIDList[rRight, 5]:=  BACKWARD_ECHO_70;
    ChannelIDList[rRight, 6]:=  FORWARD_ECHO_42_WEB;
    ChannelIDList[rRight, 8]:=  FORWARD_ECHO_42_BASE;
    ChannelIDList[rRight, 7]:=  BACKWARD_ECHO_42_WEB;
    ChannelIDList[rRight, 9]:=  BACKWARD_ECHO_42_BASE;
    ChannelIDList[rRight, 10]:= BACKWARD_UWORK_MIRROR_58;
    ChannelIDList[rRight, 11]:= BACKWARD_UWORK_ECHO_58;

  // ------------------------------------------------------------------------------

    with EvalChannels2[BACKWARD_WORK_MIRROR_58] do  begin InfoSide:= isLeft;  InfoPos:= ipLeft;   end;
    with EvalChannels2[BACKWARD_UWORK_MIRROR_58] do begin InfoSide:= isLeft;  InfoPos:= ipRight;  end;

    with EvalChannels2[BACKWARD_WORK_ECHO_58] do    begin InfoSide:= isRight; InfoPos:= ipLeft;   end;
    with EvalChannels2[BACKWARD_UWORK_ECHO_58] do   begin InfoSide:= isRight; InfoPos:= ipRight;  end;

    with EvalChannels2[FORWARD_ECHO_70] do          begin InfoSide:= isLeft;  InfoPos:= ipCentre; end;
    with EvalChannels2[BACKWARD_ECHO_70] do         begin InfoSide:= isRight; InfoPos:= ipCentre; end;

    with EvalChannels2[FORWARD_ECHO_42_WEB] do      begin InfoSide:= isLeft;  InfoPos:= ipRight;  end;
    with EvalChannels2[FORWARD_ECHO_42_BASE] do     begin InfoSide:= isLeft;  InfoPos:= ipLeft;   end;
    with EvalChannels2[BACKWARD_ECHO_42_WEB] do     begin InfoSide:= isRight; InfoPos:= ipRight;  end;
    with EvalChannels2[BACKWARD_ECHO_42_BASE] do    begin InfoSide:= isRight; InfoPos:= ipLeft;   end;

    with EvalChannels2[ECHO_0] do                   begin InfoSide:= isRight;  InfoPos:= ipCentre; end;
    with EvalChannels2[MIRRORSHADOW_0] do           begin InfoSide:= isLeft; InfoPos:= ipCentre; end;

    // ------------ ���� ������ �-��������� ------------------

    SetLength(FBScanLines, 8);

    FBScanLines[0].R:= rLeft;
    FBScanLines[0].Use:= True;
    SetLength(FBScanLines[0].Items, 4);
    FBScanLines[0].Items[0].ScanChIdx:= 7;
    FBScanLines[0].Items[1].ScanChIdx:= 8;
    FBScanLines[0].Items[2].ScanChIdx:= 1;
    FBScanLines[0].Items[3].ScanChIdx:= 2;

    FBScanLines[1].R:= rLeft;
    FBScanLines[1].Use:= True;
    SetLength(FBScanLines[1].Items, 2);
    FBScanLines[1].Items[0].ScanChIdx:= 3;
    FBScanLines[1].Items[1].ScanChIdx:= 4;

    FBScanLines[2].R:= rLeft;
    FBScanLines[2].Use:= True;
    SetLength(FBScanLines[2].Items, 2);
    FBScanLines[2].Items[0].ScanChIdx:= 5;
    FBScanLines[2].Items[1].ScanChIdx:= 6;

    FBScanLines[3].R:= rLeft;
    FBScanLines[3].Use:= True;
    SetLength(FBScanLines[3].Items, 1);
    FBScanLines[3].Items[0].ScanChIdx:= 0;

    FBScanLines[4].R:= rRight;
    FBScanLines[4].Use:= True;
    SetLength(FBScanLines[4].Items, 4);
    FBScanLines[4].Items[0].ScanChIdx:= 1;
    FBScanLines[4].Items[1].ScanChIdx:= 2;
    FBScanLines[4].Items[2].ScanChIdx:= 7;
    FBScanLines[4].Items[3].ScanChIdx:= 8;

    FBScanLines[5].R:= rRight;
    FBScanLines[5].Use:= True;
    SetLength(FBScanLines[5].Items, 2);
    FBScanLines[5].Items[0].ScanChIdx:= 3;
    FBScanLines[5].Items[1].ScanChIdx:= 4;

    FBScanLines[6].R:= rRight;
    FBScanLines[6].Use:= True;
    SetLength(FBScanLines[6].Items, 2);
    FBScanLines[6].Items[0].ScanChIdx:= 5;
    FBScanLines[6].Items[1].ScanChIdx:= 6;

    FBScanLines[7].R:= rRight;
    FBScanLines[7].Use:= True;
    SetLength(FBscanLines[7].Items, 1);
    FBScanLines[7].Items[0].ScanChIdx:= 0;

    // ------------ ������ ������� �������� ------------

    AddHandScanChannels(FHandChannels);

    // ------------ ������ �-��������� ------------

    SetLength(FScanChannels, 9);
    for i:= 0 to 8 do FScanChannels[i].Gran:= _None;

    FScanChannels[0].Number:= 1;  //  0
    FScanChannels[0].EnterAngle:= 0;
    FScanChannels[0].TurnAngle:= 0;
    FScanChannels[0].Position:= 50 + GEISMAR_Shift;
    FScanChannels[0].BScanGateMin:= 5;
    FScanChannels[0].BScanGateMax:= 70;
    FScanChannels[0].DelayMultiply:= 3;
    FScanChannels[0].Delta:= 0;

    FScanChannels[1].Number:= 2;  // 58 �����������
    FScanChannels[1].EnterAngle:= - 58;
    FScanChannels[1].TurnAngle:= 34;
    FScanChannels[1].Position:= - 25 - GEISMAR_Shift;
    FScanChannels[1].BScanGateMin:= 20;
    FScanChannels[1].BScanGateMax:= 180;
    FScanChannels[1].DelayMultiply:= 1;
    FScanChannels[1].Delta:= - 90;
    FScanChannels[1].Gran:= _UnWork_Left__Work_Right;

    FScanChannels[2].Number:= 3;  // 58 �����������
    FScanChannels[2].EnterAngle:= - 58;
    FScanChannels[2].TurnAngle:= 34;
    FScanChannels[2].Position:= - 75 - GEISMAR_Shift;
    FScanChannels[2].BScanGateMin:= 20;
    FScanChannels[2].BScanGateMax:= 180;
    FScanChannels[2].DelayMultiply:= 1;
    FScanChannels[2].Delta:= 90;
    FScanChannels[2].Gran:= _UnWork_Left__Work_Right;

    FScanChannels[3].Number:= 4;  // 70 ����������
    FScanChannels[3].EnterAngle:= 70;
    FScanChannels[3].TurnAngle:= 0;
    FScanChannels[3].Position:= 75 + GEISMAR_Shift;
    FScanChannels[3].BScanGateMin:= 5;
    FScanChannels[3].BScanGateMax:= 95;
    FScanChannels[3].DelayMultiply:= 1;
    FScanChannels[3].Delta:= - 80;

    FScanChannels[4].Number:= 5;  // 70 ����������
    FScanChannels[4].EnterAngle:= - 70;
    FScanChannels[4].TurnAngle:= 0;
    FScanChannels[4].Position:= 25 + GEISMAR_Shift;
    FScanChannels[4].BScanGateMin:= 5;
    FScanChannels[4].BScanGateMax:= 95;
    FScanChannels[4].DelayMultiply:= 1;
    FScanChannels[4].Delta:= + 80;

    FScanChannels[5].Number:= 6;  // 42 ����������
    FScanChannels[5].EnterAngle:= 42;
    FScanChannels[5].TurnAngle:= 0;
    FScanChannels[5].Position:= - 50 - GEISMAR_Shift;
    FScanChannels[5].BScanGateMin:= 10;
    FScanChannels[5].BScanGateMax:= 163;
    FScanChannels[5].DelayMultiply:= 1;
    FScanChannels[5].Delta:= - 30;

    FScanChannels[6].Number:= 7;  // 42 �����������
    FScanChannels[6].EnterAngle:= - 42;
    FScanChannels[6].TurnAngle:= 0;
    FScanChannels[6].Position:= - 50 - GEISMAR_Shift;
    FScanChannels[6].BScanGateMin:= 10;
    FScanChannels[6].BScanGateMax:= 163;
    FScanChannels[6].DelayMultiply:= 1;
    FScanChannels[6].Delta:= + 30;

    FScanChannels[7].Number:= 10;  // 58 �����������
    FScanChannels[7].EnterAngle:= - 58;
    FScanChannels[7].TurnAngle:= - 34;
    FScanChannels[7].Position:= - 25 - GEISMAR_Shift;
    FScanChannels[7].BScanGateMin:= 20;
    FScanChannels[7].BScanGateMax:= 180;
    FScanChannels[7].DelayMultiply:= 1;
    FScanChannels[7].Delta:= + 90;
    FScanChannels[7].Gran:= _Work_Left__UnWork_Right;

    FScanChannels[8].Number:= 11;  // 58 �����������
    FScanChannels[8].EnterAngle:= - 58;
    FScanChannels[8].TurnAngle:= - 34;
    FScanChannels[8].Position:= - 75 - GEISMAR_Shift;
    FScanChannels[8].BScanGateMin:= 20;
    FScanChannels[8].BScanGateMax:= 180;
    FScanChannels[8].DelayMultiply:= 1;
    FScanChannels[8].Delta:= + 90;
    FScanChannels[8].Gran:= _Work_Left__UnWork_Right;

    for I:= 0 to High(FScanChannels) do
      FScanChannels[I].BScanDuration:= FScanChannels[I].BScanGateMax - FScanChannels[I].BScanGateMin + 1;

    // ------------  ������ ������ ------------

    SetLength(FEvalChannels, 12);

    FEvalChannels[0].Number:= 0;     // 0 ���
    FEvalChannels[0].ScanChNum:= 1;
    FEvalChannels[0].Method:= imMirrorShadow;
    FEvalChannels[0].ShortName:= 'SHORT_0_MIRRORSHADOW';
    FEvalChannels[0].FullName:= 'FULL_0_MIRRORSHADOW';
    FEvalChannels[0].Zone:= izAll;
    FEvalChannels[0].DirColor:= + 1;

    FEvalChannels[1].Number:= 1;     // 0 ���
    FEvalChannels[1].ScanChNum:= 1;
    FEvalChannels[1].Method:= imEcho;
    FEvalChannels[1].ShortName:= 'SHORT_0_ECHO';
    FEvalChannels[1].FullName:= 'FULL_0_ECHO';
    FEvalChannels[1].Zone:= izAll;
    FEvalChannels[1].DirColor:= - 1;

    FEvalChannels[2].Number:= 2;     // 58 �����������
    FEvalChannels[2].ScanChNum:= 2;
    FEvalChannels[2].Method:= imMirror;
    FEvalChannels[2].ShortName:= 'SHORT_-58_MIRROR_WORK';
    FEvalChannels[2].FullName:= 'FULL_-58_MIRROR_WORK';
    FEvalChannels[2].Zone:= izHead;
    FEvalChannels[2].DirColor:= + 1;

    FEvalChannels[3].Number:= 3;     // 58 �����������
    FEvalChannels[3].ScanChNum:= 3;
    FEvalChannels[3].Method:= imEcho;
    FEvalChannels[3].ShortName:= 'SHORT_-58_ECHO_WORK';
    FEvalChannels[3].FullName:= 'FULL_-58_ECHO_WORK';
    FEvalChannels[3].Zone:= izHead;
    FEvalChannels[3].DirColor:= - 1;

    FEvalChannels[4].Number:= 4;     // 70 ����������
    FEvalChannels[4].ScanChNum:= 4;
    FEvalChannels[4].Method:= imEcho;
    FEvalChannels[4].ShortName:= 'SHORT_+70_ECHO';
    FEvalChannels[4].FullName:= 'FULL_+70_ECHO';
    FEvalChannels[4].Zone:= izHead;
    FEvalChannels[4].DirColor:= + 1;

    FEvalChannels[5].Number:= 5;     // 70 �����������
    FEvalChannels[5].ScanChNum:= 5;
    FEvalChannels[5].Method:= imEcho;
    FEvalChannels[5].ShortName:= 'SHORT_-70_ECHO';
    FEvalChannels[5].FullName:= 'FULL_-70_ECHO';
    FEvalChannels[5].Zone:= izHead;
    FEvalChannels[5].DirColor:= - 1;

    FEvalChannels[6].Number:= 6;      // 42 ����������
    FEvalChannels[6].ScanChNum:= 6;
    FEvalChannels[6].Method:= imEcho;
    FEvalChannels[6].ShortName:= 'SHORT_+42_ECHO_NEAR';
    FEvalChannels[6].FullName:= 'FULL_+42_ECHO_NEAR';
    FEvalChannels[6].Zone:= izWeb;
    FEvalChannels[6].DirColor:= + 1;

    FEvalChannels[7].Number:= 7;      // 42 �����������
    FEvalChannels[7].ScanChNum:= 7;
    FEvalChannels[7].Method:= imEcho;
    FEvalChannels[7].ShortName:= 'SHORT_-42_ECHO_NEAR';
    FEvalChannels[7].FullName:= 'FULL_-42_ECHO_NEAR';
    FEvalChannels[7].Zone:= izWeb;
    FEvalChannels[7].DirColor:= - 1;

    FEvalChannels[8].Number:= 8;      // 42 ����������
    FEvalChannels[8].ScanChNum:= 6;
    FEvalChannels[8].Method:= imEcho;
    FEvalChannels[8].ShortName:= 'SHORT_+42_ECHO_FAR';
    FEvalChannels[8].FullName:= 'FULL_+42_ECHO_FAR';
    FEvalChannels[8].Zone:= izBase;
    FEvalChannels[8].DirColor:= + 1;

    FEvalChannels[9].Number:= 9;      // 42 �����������
    FEvalChannels[9].ScanChNum:= 7;
    FEvalChannels[9].Method:= imEcho;
    FEvalChannels[9].ShortName:= 'SHORT_-42_ECHO_FAR';
    FEvalChannels[9].FullName:= 'FULL_-42_ECHO_FAR';
    FEvalChannels[9].Zone:= izBase;
    FEvalChannels[9].DirColor:= - 1;

    FEvalChannels[10].Number:= 10;    // 58 �����������
    FEvalChannels[10].ScanChNum:= 10;
    FEvalChannels[10].Method:= imMirror;
    FEvalChannels[10].ShortName:= 'SHORT_-58_MIRROR_NOWORK';
    FEvalChannels[10].FullName:= 'FULL_-58_MIRROR_NOWORK';
    FEvalChannels[10].Zone:= izHead;
    FEvalChannels[10].DirColor:= + 1;

    FEvalChannels[11].Number:= 11;    // 58 �����������
    FEvalChannels[11].ScanChNum:= 11;
    FEvalChannels[11].Method:= imEcho;
    FEvalChannels[11].ShortName:= 'SHORT_-58_ECHO_NOWORK';
    FEvalChannels[11].FullName:= 'FULL_-58_ECHO_NOWORK';
    FEvalChannels[11].Zone:= izHead;
    FEvalChannels[11].DirColor:= - 1;
  end
  else

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

  if CompareMem(@Temp, @Avicon16Scheme3, 8) then
  begin
    FDeviceID:= Avicon16Scheme3; // ��� ������������
    FDeviceVer:= 3;
    FDeviceName:= 'Avicon16Scheme3';

    FMinRail:= rLeft;
    FMaxRail:= rRight;

  // ------------------------------------------------------------------------------

    ChannelIDList[rLeft, 0]:= MIRRORSHADOW_0;
    ChannelIDList[rLeft, 1]:= ECHO_0;
    ChannelIDList[rLeft, 2]:= FORWARD_UWORK_ECHO_58;
    ChannelIDList[rLeft, 3]:= BACKWARD_UWORK_ECHO_58;
    ChannelIDList[rLeft, 4]:= FORWARD_ECHO_70;
    ChannelIDList[rLeft, 5]:= BACKWARD_UWORK_MIRROR_58;
    ChannelIDList[rLeft, 6]:= FORWARD_ECHO_42_WEB;
    ChannelIDList[rLeft, 7]:= BACKWARD_ECHO_42_WEB;
    ChannelIDList[rLeft, 8]:= FORWARD_ECHO_42_BASE;
    ChannelIDList[rLeft, 9]:= BACKWARD_ECHO_42_BASE;
    ChannelIDList[rLeft, 10]:= FORWARD_WORK_ECHO_58;
    ChannelIDList[rLeft, 11]:= BACKWARD_WORK_ECHO_58;
    ChannelIDList[rLeft, 12]:= BACKWARD_WORK_MIRROR_58;

    ChannelIDList[rRight, 0]:= MIRRORSHADOW_0;
    ChannelIDList[rRight, 1]:= ECHO_0;
    ChannelIDList[rRight, 2]:= FORWARD_WORK_ECHO_58;
    ChannelIDList[rRight, 3]:= BACKWARD_WORK_ECHO_58;
    ChannelIDList[rRight, 4]:= FORWARD_ECHO_70;
    ChannelIDList[rRight, 5]:= BACKWARD_WORK_MIRROR_58;
    ChannelIDList[rRight, 6]:= FORWARD_ECHO_42_WEB;
    ChannelIDList[rRight, 7]:= BACKWARD_ECHO_42_WEB;
    ChannelIDList[rRight, 8]:= FORWARD_ECHO_42_BASE;
    ChannelIDList[rRight, 9]:= BACKWARD_ECHO_42_BASE;
    ChannelIDList[rRight, 10]:= FORWARD_UWORK_ECHO_58;
    ChannelIDList[rRight, 11]:= BACKWARD_UWORK_ECHO_58;
    ChannelIDList[rRight, 12]:= BACKWARD_UWORK_MIRROR_58;

  // ------------------------------------------------------------------------------

    with EvalChannels2[FORWARD_WORK_ECHO_58] do     begin InfoSide:= isLeft;  InfoPos:= ipLeft;   end;
    with EvalChannels2[FORWARD_UWORK_ECHO_58] do    begin InfoSide:= isLeft;  InfoPos:= ipRight;  end;

    with EvalChannels2[BACKWARD_WORK_ECHO_58] do    begin InfoSide:= isRight; InfoPos:= ipLeft;   end;
    with EvalChannels2[BACKWARD_UWORK_ECHO_58] do   begin InfoSide:= isRight; InfoPos:= ipRight;  end;

    with EvalChannels2[FORWARD_ECHO_70] do          begin InfoSide:= isLeft;  InfoPos:= ipCentre; end;
    with EvalChannels2[BACKWARD_WORK_MIRROR_58] do  begin InfoSide:= isRight; InfoPos:= ipLeft; end;
    with EvalChannels2[BACKWARD_UWORK_MIRROR_58] do begin InfoSide:= isRight; InfoPos:= ipRight; end;

    with EvalChannels2[FORWARD_ECHO_42_WEB] do      begin InfoSide:= isLeft;  InfoPos:= ipRight;  end;
    with EvalChannels2[FORWARD_ECHO_42_BASE] do     begin InfoSide:= isLeft;  InfoPos:= ipLeft;   end;
    with EvalChannels2[BACKWARD_ECHO_42_WEB] do     begin InfoSide:= isRight; InfoPos:= ipRight;  end;
    with EvalChannels2[BACKWARD_ECHO_42_BASE] do    begin InfoSide:= isRight; InfoPos:= ipLeft;   end;

    with EvalChannels2[ECHO_0] do                   begin InfoSide:= isRight;  InfoPos:= ipCentre; end;
    with EvalChannels2[MIRRORSHADOW_0] do           begin InfoSide:= isLeft; InfoPos:= ipCentre; end;


    // ------------ ���� ������ �-��������� ------------------

    SetLength(FBScanLines, 8);

    FBScanLines[0].R:= rLeft;
    FBScanLines[0].Use:= True;
    SetLength(FBScanLines[0].Items, 4);
    FBScanLines[0].Items[0].ScanChIdx:= 7;
    FBScanLines[0].Items[1].ScanChIdx:= 8;
    FBScanLines[0].Items[2].ScanChIdx:= 1;
    FBScanLines[0].Items[3].ScanChIdx:= 2;

    FBScanLines[1].R:= rLeft;
    FBScanLines[1].Use:= True;
    SetLength(FBScanLines[1].Items, 3);
    FBScanLines[1].Items[0].ScanChIdx:= 3;
    FBScanLines[1].Items[1].ScanChIdx:= 9;
    FBScanLines[1].Items[2].ScanChIdx:= 4;

    FBScanLines[2].R:= rLeft;
    FBScanLines[2].Use:= True;
    SetLength(FBScanLines[2].Items, 2);
    FBScanLines[2].Items[0].ScanChIdx:= 5;
    FBScanLines[2].Items[1].ScanChIdx:= 6;

    FBScanLines[3].R:= rLeft;
    FBScanLines[3].Use:= True;
    SetLength(FBScanLines[3].Items, 1);
    FBScanLines[3].Items[0].ScanChIdx:= 0;

/////////////////////////////////////////////////////

    FBScanLines[4].R:= rRight;
    FBScanLines[4].Use:= True;
    SetLength(FBScanLines[4].Items, 4);
    FBScanLines[4].Items[0].ScanChIdx:= 1;
    FBScanLines[4].Items[1].ScanChIdx:= 2;
    FBScanLines[4].Items[2].ScanChIdx:= 7;
    FBScanLines[4].Items[3].ScanChIdx:= 8;

    FBScanLines[5].R:= rRight;
    FBScanLines[5].Use:= True;
    SetLength(FBScanLines[5].Items, 3);
    FBScanLines[5].Items[0].ScanChIdx:= 3;
    FBScanLines[5].Items[1].ScanChIdx:= 4;
    FBScanLines[5].Items[2].ScanChIdx:= 9;

    FBScanLines[6].R:= rRight;
    FBScanLines[6].Use:= True;
    SetLength(FBScanLines[6].Items, 2);
    FBScanLines[6].Items[0].ScanChIdx:= 5;
    FBScanLines[6].Items[1].ScanChIdx:= 6;

    FBScanLines[7].R:= rRight;
    FBScanLines[7].Use:= True;
    SetLength(FBscanLines[7].Items, 1);
    FBScanLines[7].Items[0].ScanChIdx:= 0;

    // ------------ ������ ������� �������� ------------

    AddHandScanChannels(FHandChannels);

    // ------------ ������ �-��������� ------------

    SetLength(FScanChannels, 10);
    for i:= 0 to 9 do FScanChannels[i].Gran:= _None;

    FScanChannels[0].Number:= 1;  //  0
    FScanChannels[0].EnterAngle:= 0;
    FScanChannels[0].TurnAngle:= 0;
    FScanChannels[0].Position:= 75 + GEISMAR_Shift;
    FScanChannels[0].BScanGateMin:= 5;
    FScanChannels[0].BScanGateMax:= 70;
    FScanChannels[0].DelayMultiply:= 3;
    FScanChannels[0].Delta:= 0;
    FScanChannels[0].Gran:= _None;

    FScanChannels[1].Number:= 2;  // 58 ����������
    FScanChannels[1].EnterAngle:= 58;
    FScanChannels[1].TurnAngle:= 34;
    FScanChannels[1].Position:= + 25 + GEISMAR_Shift;
    FScanChannels[1].BScanGateMin:= 20;
    FScanChannels[1].BScanGateMax:= 180;
    FScanChannels[1].DelayMultiply:= 1;
    FScanChannels[1].Delta:= + 90;
    FScanChannels[1].Gran:= _UnWork_Left__Work_Right;

    FScanChannels[2].Number:= 3;  // 58 ����������
    FScanChannels[2].EnterAngle:= - 58;
    FScanChannels[2].TurnAngle:= 34;
    FScanChannels[2].Position:= - 75 - GEISMAR_Shift;
    FScanChannels[2].BScanGateMin:= 20;
    FScanChannels[2].BScanGateMax:= 180;
    FScanChannels[2].DelayMultiply:= 1;
    FScanChannels[2].Delta:= + 90;
    FScanChannels[2].Gran:= _UnWork_Left__Work_Right;

    FScanChannels[3].Number:= 4;  // 70 ����������
    FScanChannels[3].EnterAngle:= 70;
    FScanChannels[3].TurnAngle:= 0;
    FScanChannels[3].Position:= 50 + GEISMAR_Shift;
    FScanChannels[3].BScanGateMin:= 5; //5;
    FScanChannels[3].BScanGateMax:= 180; //95;
    FScanChannels[3].DelayMultiply:= 1;
    FScanChannels[3].Delta:= - 80;

    FScanChannels[4].Number:= 5;  // 58 �����������
    FScanChannels[4].EnterAngle:= - 58;
    FScanChannels[4].TurnAngle:= 34;
    FScanChannels[4].Position:= - 25 - GEISMAR_Shift;
    FScanChannels[4].BScanGateMin:= 5; //20;
    FScanChannels[4].BScanGateMax:= 180; //180;
    FScanChannels[4].DelayMultiply:= 1;
    FScanChannels[4].Delta:= - 90;
    FScanChannels[4].Gran:= _UnWork_Left__Work_Right;

    FScanChannels[5].Number:= 6;  // 42 ���� ����������
    FScanChannels[5].EnterAngle:= 42;
    FScanChannels[5].TurnAngle:= 0;
    FScanChannels[5].Position:= - 50 - GEISMAR_Shift;
    FScanChannels[5].BScanGateMin:= 10;
    FScanChannels[5].BScanGateMax:= 163;
    FScanChannels[5].DelayMultiply:= 1;
    FScanChannels[5].Delta:= - 30;
    FScanChannels[5].Gran:= _None;

    FScanChannels[6].Number:= 7;  // 42 ���� �����������
    FScanChannels[6].EnterAngle:= - 42;
    FScanChannels[6].TurnAngle:= 0;
    FScanChannels[6].Position:= - 50 - GEISMAR_Shift;
    FScanChannels[6].BScanGateMin:= 10;
    FScanChannels[6].BScanGateMax:= 163;
    FScanChannels[6].DelayMultiply:= 1;
    FScanChannels[6].Delta:= + 30;
    FScanChannels[6].Gran:= _None;

    FScanChannels[7].Number:= 10; // 58 ����������
    FScanChannels[7].EnterAngle:= 58;
    FScanChannels[7].TurnAngle:= + 34;
    FScanChannels[7].Position:= + 25 + GEISMAR_Shift;
    FScanChannels[7].BScanGateMin:= 20;
    FScanChannels[7].BScanGateMax:= 180;
    FScanChannels[7].DelayMultiply:= 1;
    FScanChannels[7].Delta:= + 90;
    FScanChannels[7].Gran:= _Work_Left__UnWork_Right;

    FScanChannels[8].Number:= 11; // 58 �����������
    FScanChannels[8].EnterAngle:= - 58;
    FScanChannels[8].TurnAngle:= 34;
    FScanChannels[8].Position:= - 75 - GEISMAR_Shift;
    FScanChannels[8].BScanGateMin:= 20;
    FScanChannels[8].BScanGateMax:= 180;
    FScanChannels[8].DelayMultiply:= 1;
    FScanChannels[8].Delta:= + 90;
    FScanChannels[8].Gran:= _Work_Left__UnWork_Right;

    FScanChannels[9].Number:= 12; // 58 �����������
    FScanChannels[9].EnterAngle:= - 58;
    FScanChannels[9].TurnAngle:= 34;
    FScanChannels[9].Position:= - 25 - GEISMAR_Shift;
    FScanChannels[9].BScanGateMin:= 5;   //20;
    FScanChannels[9].BScanGateMax:= 180; //180;
    FScanChannels[9].DelayMultiply:= 1;
    FScanChannels[9].Delta:= - 90;
    FScanChannels[9].Gran:= _Work_Left__UnWork_Right;

    for I:= 0 to High(FScanChannels) do
      FScanChannels[I].BScanDuration:= FScanChannels[I].BScanGateMax - FScanChannels[I].BScanGateMin + 1;

    // ------------ ������ ������ ------------

    SetLength(FEvalChannels, 13);

    FEvalChannels[0].Number:= 0;     // 0 ���
    FEvalChannels[0].ScanChNum:= 1;
    FEvalChannels[0].Method:= imMirrorShadow;
    FEvalChannels[0].ShortName:= 'SHORT_0_MIRRORSHADOW';
    FEvalChannels[0].FullName:= 'FULL_0_MIRRORSHADOW';
    FEvalChannels[0].Zone:= izAll;
    FEvalChannels[0].DirColor:= + 1;

    FEvalChannels[1].Number:= 1;     // 0 ���
    FEvalChannels[1].ScanChNum:= 1;
    FEvalChannels[1].Method:= imEcho;
    FEvalChannels[1].ShortName:= 'SHORT_0_ECHO';
    FEvalChannels[1].FullName:= 'FULL_0_ECHO';
    FEvalChannels[1].Zone:= izAll;
    FEvalChannels[1].DirColor:= - 1;

    FEvalChannels[2].Number:= 2;     // 58 ����������
    FEvalChannels[2].ScanChNum:= 2;
    FEvalChannels[2].Method:= imEcho;
    FEvalChannels[2].ShortName:= 'SHORT_58_ECHO';
    FEvalChannels[2].FullName:= 'FULL_58_ECHO';
    FEvalChannels[2].Zone:= izHead;
    FEvalChannels[2].DirColor:= + 1;

    FEvalChannels[3].Number:= 3;     // 58 ����������
    FEvalChannels[3].ScanChNum:= 3;
    FEvalChannels[3].Method:= imEcho;
    FEvalChannels[3].ShortName:= 'SHORT_-58_ECHO';
    FEvalChannels[3].FullName:= 'FULL_-58_ECHO';
    FEvalChannels[3].Zone:= izHead;
    FEvalChannels[3].DirColor:= - 1;

    FEvalChannels[4].Number:= 4;     // 70 ����������
    FEvalChannels[4].ScanChNum:= 4;
    FEvalChannels[4].Method:= imEcho;
    FEvalChannels[4].ShortName:= 'SHORT_+70_ECHO';
    FEvalChannels[4].FullName:= 'FULL_+70_ECHO';
    FEvalChannels[4].Zone:= izHead;
    FEvalChannels[4].DirColor:= + 1;

    FEvalChannels[5].Number:= 5;     // 58 �����������
    FEvalChannels[5].ScanChNum:= 5;
    FEvalChannels[5].Method:= imMirror;
    FEvalChannels[5].ShortName:= 'SHORT_-58_MIRROR';
    FEvalChannels[5].FullName:= 'FULL_-58_MIRROR';
    FEvalChannels[5].Zone:= izHead;
    FEvalChannels[5].DirColor:= - 1;

    FEvalChannels[6].Number:= 6;      // 42 ����������
    FEvalChannels[6].ScanChNum:= 6;
    FEvalChannels[6].Method:= imEcho;
    FEvalChannels[6].ShortName:= 'SHORT_+42_ECHO_NEAR';
    FEvalChannels[6].FullName:= 'FULL_+42_ECHO_NEAR';
    FEvalChannels[6].Zone:= izWeb;
    FEvalChannels[6].DirColor:= + 1;

    FEvalChannels[7].Number:= 7;      // 42 ����������
    FEvalChannels[7].ScanChNum:= 7;
    FEvalChannels[7].Method:= imEcho;
    FEvalChannels[7].ShortName:= 'SHORT_-42_ECHO_NEAR';
    FEvalChannels[7].FullName:= 'FULL_-42_ECHO_NEAR';
    FEvalChannels[7].Zone:= izWeb;
    FEvalChannels[7].DirColor:= - 1;

    FEvalChannels[8].Number:= 8;      // 42 ����������
    FEvalChannels[8].ScanChNum:= 6;
    FEvalChannels[8].Method:= imEcho;
    FEvalChannels[8].ShortName:= 'SHORT_+42_ECHO_FAR';
    FEvalChannels[8].FullName:= 'FULL_+42_ECHO_FAR';
    FEvalChannels[8].Zone:= izBase;
    FEvalChannels[8].DirColor:= + 1;

    FEvalChannels[9].Number:= 9;      // 42 ����������
    FEvalChannels[9].ScanChNum:= 7;
    FEvalChannels[9].Method:= imEcho;
    FEvalChannels[9].ShortName:= 'SHORT_-42_ECHO_FAR';
    FEvalChannels[9].FullName:= 'FULL_-42_ECHO_FAR';
    FEvalChannels[9].Zone:= izBase;
    FEvalChannels[9].DirColor:= - 1;

    FEvalChannels[10].Number:= 10;    // 58 ����������
    FEvalChannels[10].ScanChNum:= 10;
    FEvalChannels[10].Method:= imEcho;
    FEvalChannels[10].ShortName:= 'SHORT_58_ECHO_NOWORK';
    FEvalChannels[10].FullName:= 'FULL_58_ECHO_NOWORK';
    FEvalChannels[10].Zone:= izHead;
    FEvalChannels[10].DirColor:= + 1;

    FEvalChannels[11].Number:= 11;    // 58 �����������
    FEvalChannels[11].ScanChNum:= 11;
    FEvalChannels[11].Method:= imEcho;
    FEvalChannels[11].ShortName:= 'SHORT_-58_ECHO_NOWORK';
    FEvalChannels[11].FullName:= 'FULL_-58_ECHO_NOWORK';
    FEvalChannels[11].Zone:= izHead;
    FEvalChannels[11].DirColor:= - 1;

    FEvalChannels[12].Number:= 12;    // 58 �����������
    FEvalChannels[12].ScanChNum:= 12;
    FEvalChannels[12].Method:= imMirror;
    FEvalChannels[12].ShortName:= 'SHORT_-58_MIRROR_NOWORK';
    FEvalChannels[12].FullName:= 'FULL_-58_MIRROR_NOWORK';
    FEvalChannels[12].Zone:= izHead;
    FEvalChannels[12].DirColor:= - 1;

  end
  else

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

  if CompareMem(@Temp, @Avicon16Wheel, 8) or
     CompareMem(@Temp, @Avicon14Wheel, 8) then
  begin
    FDeviceID:= Temp; // ��� ������������
    FDeviceVer:= 3;

    FMinRail:= rLeft;
    FMaxRail:= rRight;

    if CompareMem(@Temp, @Avicon16Wheel, 8) then FDeviceName:= 'Avicon16Wheel';
    if CompareMem(@Temp, @Avicon14Wheel, 8) then FDeviceName:= 'Avicon14Wheel';

    ChannelIDList[rLeft, 0]:= MIRRORSHADOW_0;
    ChannelIDList[rLeft, 1]:= ECHO_0;
    ChannelIDList[rLeft, 2]:= FORWARD_UWORK_ECHO_58;
    ChannelIDList[rLeft, 3]:= BACKWARD_WORK_ECHO_58;
    ChannelIDList[rLeft, 4]:= FORWARD_ECHO_65;
    ChannelIDList[rLeft, 5]:= BACKWARD_ECHO_65;
    ChannelIDList[rLeft, 6]:= FORWARD_ECHO_42_WEB;
    ChannelIDList[rLeft, 7]:= BACKWARD_ECHO_42_WEB;
    ChannelIDList[rLeft, 8]:= FORWARD_ECHO_42_BASE;
    ChannelIDList[rLeft, 9]:= BACKWARD_ECHO_42_BASE;
    ChannelIDList[rLeft, 10]:= MIRRORSHADOW_0;
    ChannelIDList[rLeft, 11]:= ECHO_0;
    ChannelIDList[rLeft, 12]:= FORWARD_WORK_ECHO_58;
    ChannelIDList[rLeft, 13]:= BACKWARD_UWORK_ECHO_58;

    ChannelIDList[rRight, 0]:= MIRRORSHADOW_0;
    ChannelIDList[rRight, 1]:= ECHO_0;
    ChannelIDList[rRight, 2]:= FORWARD_WORK_ECHO_58;
    ChannelIDList[rRight, 3]:= BACKWARD_UWORK_ECHO_58;
    ChannelIDList[rRight, 4]:= FORWARD_ECHO_65;
    ChannelIDList[rRight, 5]:= BACKWARD_ECHO_65;
    ChannelIDList[rRight, 6]:= FORWARD_ECHO_42_WEB;
    ChannelIDList[rRight, 7]:= BACKWARD_ECHO_42_WEB;
    ChannelIDList[rRight, 8]:= FORWARD_ECHO_42_BASE;
    ChannelIDList[rRight, 9]:= BACKWARD_ECHO_42_BASE;
    ChannelIDList[rRight, 10]:= MIRRORSHADOW_0;
    ChannelIDList[rRight, 11]:= ECHO_0;
    ChannelIDList[rRight, 12]:= FORWARD_UWORK_ECHO_58;
    ChannelIDList[rRight, 13]:= BACKWARD_WORK_ECHO_58;

  // ------------------------------------------------------------------------------

    with EvalChannels2[FORWARD_WORK_ECHO_58] do   begin InfoSide:= isLeft;  InfoPos:= ipLeft;   end;
    with EvalChannels2[FORWARD_UWORK_ECHO_58] do  begin InfoSide:= isLeft;  InfoPos:= ipRight;  end;

    with EvalChannels2[BACKWARD_WORK_ECHO_58] do  begin InfoSide:= isRight; InfoPos:= ipLeft;   end;
    with EvalChannels2[BACKWARD_UWORK_ECHO_58] do begin InfoSide:= isRight; InfoPos:= ipRight;  end;

    with EvalChannels2[FORWARD_ECHO_65] do        begin InfoSide:= isLeft;  InfoPos:= ipCentre; end;
    with EvalChannels2[BACKWARD_ECHO_65] do       begin InfoSide:= isRight; InfoPos:= ipCentre; end;

    with EvalChannels2[FORWARD_ECHO_42_WEB] do    begin InfoSide:= isLeft;  InfoPos:= ipRight;  end;
    with EvalChannels2[FORWARD_ECHO_42_BASE] do   begin InfoSide:= isLeft;  InfoPos:= ipLeft;   end;
    with EvalChannels2[BACKWARD_ECHO_42_WEB] do   begin InfoSide:= isRight; InfoPos:= ipRight;  end;
    with EvalChannels2[BACKWARD_ECHO_42_BASE] do  begin InfoSide:= isRight; InfoPos:= ipLeft;   end;

    with EvalChannels2[ECHO_0] do                 begin InfoSide:= isRight;  InfoPos:= ipCentre; end;
    with EvalChannels2[MIRRORSHADOW_0] do         begin InfoSide:= isLeft; InfoPos:= ipCentre; end;

  // ------------ ���� ������ �-��������� ------------------

    SetLength(FBScanLines, 10);

    FBScanLines[0].R:= rLeft;
    FBScanLines[0].Use:= True;
    SetLength(FBScanLines[0].Items, 4);
    FBScanLines[0].Items[0].ScanChIdx:= 7;
    FBScanLines[0].Items[1].ScanChIdx:= 2{8};
    FBScanLines[0].Items[2].ScanChIdx:= 1;
    FBScanLines[0].Items[3].ScanChIdx:= 8{2};

    FBScanLines[1].R:= rLeft;
    FBScanLines[1].Use:= True;
    SetLength(FBScanLines[1].Items, 2);
    FBScanLines[1].Items[0].ScanChIdx:= 3;
    FBScanLines[1].Items[1].ScanChIdx:= 4;

    FBScanLines[2].R:= rLeft;
    FBScanLines[2].Use:= True;
    SetLength(FBScanLines[2].Items, 2);
    FBScanLines[2].Items[0].ScanChIdx:= 5;
    FBScanLines[2].Items[1].ScanChIdx:= 6;

    FBScanLines[3].R:= rLeft;
    FBScanLines[3].Use:= True;
    SetLength(FBScanLines[3].Items, 1);
    FBScanLines[3].Items[0].ScanChIdx:= 0;

    FBScanLines[4].R:= rLeft;
    FBScanLines[4].Use:= True;
    SetLength(FBScanLines[4].Items, 1);
    FBScanLines[4].Items[0].ScanChIdx:= 9;

  //////////////////////////////////////////////////

    FBScanLines[5].R:= rRight;
    FBScanLines[5].Use:= True;
    SetLength(FBScanLines[5].Items, 4);
    FBScanLines[5].Items[0].ScanChIdx:= 1;
    FBScanLines[5].Items[1].ScanChIdx:= 8;
    FBScanLines[5].Items[2].ScanChIdx:= 7;
    FBScanLines[5].Items[3].ScanChIdx:= 2;

    FBScanLines[6].R:= rRight;
    FBScanLines[6].Use:= True;
    SetLength(FBScanLines[6].Items, 2);
    FBScanLines[6].Items[0].ScanChIdx:= 3;
    FBScanLines[6].Items[1].ScanChIdx:= 4;

    FBScanLines[7].R:= rRight;
    FBScanLines[7].Use:= True;
    SetLength(FBScanLines[7].Items, 2);
    FBScanLines[7].Items[0].ScanChIdx:= 5;
    FBScanLines[7].Items[1].ScanChIdx:= 6;

    FBScanLines[8].R:= rRight;
    FBScanLines[8].Use:= True;
    SetLength(FBscanLines[8].Items, 1);
    FBScanLines[8].Items[0].ScanChIdx:= 0;

    FBScanLines[9].R:= rRight;
    FBScanLines[9].Use:= True;
    SetLength(FBscanLines[9].Items, 1);
    FBScanLines[9].Items[0].ScanChIdx:= 9;

    // ------------ ������ ������� �������� ------------

    AddHandScanChannels(FHandChannels);

    // ------------ ������ �-��������� ------------

    SetLength(FScanChannels, 10);
    for i:= 0 to 9 do FScanChannels[i].Gran:= _None;

    FScanChannels[0].Number:= 1;  //  ��� �1: 0 ���� - 1
    FScanChannels[0].EnterAngle:= 0;
    FScanChannels[0].TurnAngle:= 0;
    FScanChannels[0].Position:= - 65;
    FScanChannels[0].BScanGateMin:= 5;
    FScanChannels[0].BScanGateMax:= 70;
    FScanChannels[0].DelayMultiply:= 3;
    FScanChannels[0].Delta:= 0;

    FScanChannels[1].Number:= 2;  // ��� �1: 58 ����������
    FScanChannels[1].EnterAngle:= 58;
    FScanChannels[1].TurnAngle:= 34;
    FScanChannels[1].Position:= 65;
    FScanChannels[1].BScanGateMin:= 20;
    FScanChannels[1].BScanGateMax:= 180;
    FScanChannels[1].DelayMultiply:= 1;
    FScanChannels[1].Delta:= - 80;
    FScanChannels[1].Gran:= _UnWork_Left__Work_Right;

    FScanChannels[2].Number:= 3;  // ��� �2: 58 �����������
    FScanChannels[2].EnterAngle:= - 58;
    FScanChannels[2].TurnAngle:= 34;
    FScanChannels[2].Position:= -65;
    FScanChannels[2].BScanGateMin:= 20;
    FScanChannels[2].BScanGateMax:= 180;
    FScanChannels[2].DelayMultiply:= 1;
    FScanChannels[2].Delta:= 80;
    FScanChannels[2].Gran:= _Work_Left__UnWork_Right;

    FScanChannels[3].Number:= 4;  // ��� �1: 65 ����������
    FScanChannels[3].EnterAngle:= 65;
    FScanChannels[3].TurnAngle:= 0;
    FScanChannels[3].Position:= 65;
    FScanChannels[3].BScanGateMin:= 5;
    FScanChannels[3].BScanGateMax:= 95;
    FScanChannels[3].DelayMultiply:= 1;
    FScanChannels[3].Delta:= - 80;

    FScanChannels[4].Number:= 5;  // ��� �2: 65 �����������
    FScanChannels[4].EnterAngle:= - 65;
    FScanChannels[4].TurnAngle:= 0;
    FScanChannels[4].Position:= -65;
    FScanChannels[4].BScanGateMin:= 5;
    FScanChannels[4].BScanGateMax:= 95;
    FScanChannels[4].DelayMultiply:= 1;
    FScanChannels[4].Delta:= 80;

    FScanChannels[5].Number:= 6;  // ��� �1: 42 ����������
    FScanChannels[5].EnterAngle:= 42;
    FScanChannels[5].TurnAngle:= 0;
    FScanChannels[5].Position:= 65;
    FScanChannels[5].BScanGateMin:= 10;
    FScanChannels[5].BScanGateMax:= 163;
    FScanChannels[5].DelayMultiply:= 1;
    FScanChannels[5].Delta:= - 30;

    FScanChannels[6].Number:= 7;  // 42 ��� �2: �����������
    FScanChannels[6].EnterAngle:= - 42;
    FScanChannels[6].TurnAngle:= 0;
    FScanChannels[6].Position:= -65;
    FScanChannels[6].BScanGateMin:= 10;
    FScanChannels[6].BScanGateMax:= 163;
    FScanChannels[6].DelayMultiply:= 1;
    FScanChannels[6].Delta:= 30;

    FScanChannels[7].Number:= 12;  // ��� �1: 58 ����������
    FScanChannels[7].EnterAngle:= 58;
    FScanChannels[7].TurnAngle:= - 34;
    FScanChannels[7].Position:= 65;
    FScanChannels[7].BScanGateMin:= 20;
    FScanChannels[7].BScanGateMax:= 180;
    FScanChannels[7].DelayMultiply:= 1;
    FScanChannels[7].Delta:= - 80;
    FScanChannels[7].Gran:= _Work_Left__UnWork_Right;

    FScanChannels[8].Number:= 13;  // ��� �2: 58 �����������
    FScanChannels[8].EnterAngle:= - 58;
    FScanChannels[8].TurnAngle:= - 34;
    FScanChannels[8].Position:= - 65;
    FScanChannels[8].BScanGateMin:= 20;
    FScanChannels[8].BScanGateMax:= 180;
    FScanChannels[8].DelayMultiply:= 1;
    FScanChannels[8].Delta:= 80;
    FScanChannels[8].Gran:= _UnWork_Left__Work_Right;

    FScanChannels[9].Number:= 11;  //  ��� �2: 0
    FScanChannels[9].EnterAngle:= 0;
    FScanChannels[9].TurnAngle:= 0;
    FScanChannels[9].Position:= 65;
    FScanChannels[9].BScanGateMin:= 5;
    FScanChannels[9].BScanGateMax:= 70;
    FScanChannels[9].DelayMultiply:= 3;
    FScanChannels[9].Delta:= 0;

    for I:= 0 to High(FScanChannels) do
      FScanChannels[I].BScanDuration:= FScanChannels[I].BScanGateMax - FScanChannels[I].BScanGateMin + 1;

    // ------------  ������ ������ ------------

    SetLength(FEvalChannels, 14);

    FEvalChannels[0].Number:= 0;     // 0 ���
    FEvalChannels[0].ScanChNum:= 1;
    FEvalChannels[0].Method:= imMirrorShadow;
    FEvalChannels[0].Zone:= izAll;
    FEvalChannels[0].ShortName:= 'SHORT_0_MIRRORSHADOW_WP1';
    FEvalChannels[0].FullName:= 'FULL_0_MIRRORSHADOW_WP1';
    FEvalChannels[0].DirColor:= + 1;

    FEvalChannels[1].Number:= 1;     // 0 ���
    FEvalChannels[1].ScanChNum:= 1;
    FEvalChannels[1].Method:= imEcho;
    FEvalChannels[1].Zone:= izAll;
    FEvalChannels[1].ShortName:= 'SHORT_0_ECHO_WP1';
    FEvalChannels[1].FullName:= 'FULL_0_ECHO_WP1';
    FEvalChannels[1].DirColor:= - 1;

    FEvalChannels[2].Number:= 2;     // 58 ����������
    FEvalChannels[2].ScanChNum:= 2;
    FEvalChannels[2].Method:= imEcho;
    FEvalChannels[2].Zone:= izHead;
    FEvalChannels[2].ShortName:= 'SHORT_+58_ECHO_WORK';
    FEvalChannels[2].FullName:= 'FULL_+58_ECHO_WORK';
    FEvalChannels[2].DirColor:= + 1;

    FEvalChannels[3].Number:= 3;     // 58 �����������
    FEvalChannels[3].ScanChNum:= 3;
    FEvalChannels[3].Method:= imEcho;
    FEvalChannels[3].Zone:= izHead;
    FEvalChannels[3].ShortName:= 'SHORT_-58_ECHO_WORK';
    FEvalChannels[3].FullName:= 'FULL_-58_ECHO_WORK';
    FEvalChannels[3].DirColor:= - 1;

    FEvalChannels[4].Number:= 4;     // 65 ����������
    FEvalChannels[4].ScanChNum:= 4;
    FEvalChannels[4].Method:= imEcho;
    FEvalChannels[4].Zone:= izHead;
    FEvalChannels[4].ShortName:= 'SHORT_+65_ECHO';
    FEvalChannels[4].FullName:= 'FULL_+65_ECHO';
    FEvalChannels[4].DirColor:= + 1;

    FEvalChannels[5].Number:= 5;     // 65 ����������
    FEvalChannels[5].ScanChNum:= 5;
    FEvalChannels[5].Method:= imEcho;
    FEvalChannels[5].Zone:= izHead;
    FEvalChannels[5].ShortName:= 'SHORT_-65_ECHO';
    FEvalChannels[5].FullName:= 'FULL_-65_ECHO';
    FEvalChannels[5].DirColor:= - 1;

    FEvalChannels[6].Number:= 6;      // 42 ����������
    FEvalChannels[6].ScanChNum:= 6;
    FEvalChannels[6].Method:= imEcho;
    FEvalChannels[6].Zone:= izWeb;
    FEvalChannels[6].ShortName:= 'SHORT_+42_ECHO_NEAR';
    FEvalChannels[6].FullName:= 'FULL_+42_ECHO_NEAR';
    FEvalChannels[6].DirColor:= + 1;

    FEvalChannels[7].Number:= 7;      // 42 �����������
    FEvalChannels[7].ScanChNum:= 7;
    FEvalChannels[7].Method:= imEcho;
    FEvalChannels[7].Zone:= izWeb;
    FEvalChannels[7].ShortName:= 'SHORT_-42_ECHO_NEAR';
    FEvalChannels[7].FullName:= 'FULL_-42_ECHO_NEAR';
    FEvalChannels[7].DirColor:= - 1;

    FEvalChannels[8].Number:= 8;      // 42 ����������
    FEvalChannels[8].ScanChNum:= 6;
    FEvalChannels[8].Method:= imEcho;
    FEvalChannels[8].Zone:= izBase;
    FEvalChannels[8].ShortName:= 'SHORT_+42_ECHO_FAR';
    FEvalChannels[8].FullName:= 'FULL_+42_ECHO_FAR';
    FEvalChannels[8].DirColor:= + 1;

    FEvalChannels[9].Number:= 9;      // 42 �����������
    FEvalChannels[9].ScanChNum:= 7;
    FEvalChannels[9].Method:= imEcho;
    FEvalChannels[9].Zone:= izBase;
    FEvalChannels[9].ShortName:= 'SHORT_-42_ECHO_FAR';
    FEvalChannels[9].FullName:= 'FULL_-42_ECHO_FAR';
    FEvalChannels[9].DirColor:= - 1;

    FEvalChannels[10].Number:= 12;    // 58 ����������
    FEvalChannels[10].ScanChNum:= 12;
    FEvalChannels[10].Method:= imEcho;
    FEvalChannels[10].Zone:= izHead;
    FEvalChannels[10].ShortName:= 'SHORT_+58_ECHO_NOWORK';
    FEvalChannels[10].FullName:= 'FULL_+58_ECHO_NOWORK';
    FEvalChannels[10].DirColor:= + 1;

    FEvalChannels[11].Number:= 13;    // 58 �����������
    FEvalChannels[11].ScanChNum:= 13;
    FEvalChannels[11].Method:= imEcho;
    FEvalChannels[11].Zone:= izHead;
    FEvalChannels[11].ShortName:= 'SHORT_-58_ECHO_NOWORK';
    FEvalChannels[11].FullName:= 'FULL_-58_ECHO_NOWORK';
    FEvalChannels[11].DirColor:= - 1;

    FEvalChannels[12].Number:= 10;     // 0 ���
    FEvalChannels[12].ScanChNum:= 11;
    FEvalChannels[12].Method:= imMirrorShadow;
    FEvalChannels[12].Zone:= izAll;
    FEvalChannels[12].ShortName:= 'SHORT_0_MIRRORSHADOW_WP2';
    FEvalChannels[12].FullName:= 'FULL_0_MIRRORSHADOW_WP2';
    FEvalChannels[12].DirColor:= + 1;

    FEvalChannels[13].Number:= 11;     // 0 ���
    FEvalChannels[13].ScanChNum:= 11;
    FEvalChannels[13].Method:= imEcho;
    FEvalChannels[13].Zone:= izAll;
    FEvalChannels[13].ShortName:= 'SHORT_0_ECHO_WP2';
    FEvalChannels[13].FullName:= 'FULL_0_ECHO_WP2';
    FEvalChannels[13].DirColor:= - 1;
  end
  else

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

  if CompareMem(@Temp, @EGO_USWScheme1, 8) then
  begin
    FDeviceID:= Temp; // ��� ������������
    FDeviceVer:= 3;

    FMinRail:= rLeft;
    FMaxRail:= rRight;

	  FDeviceName:= 'EGO_USWScheme1';

    ChannelIDList[rLeft, 0]:= MIRRORSHADOW_0;
    ChannelIDList[rLeft, 1]:= ECHO_0;
    ChannelIDList[rLeft, 2]:= FORWARD_UWORK_ECHO_58;
    ChannelIDList[rLeft, 3]:= BACKWARD_WORK_ECHO_58;
    ChannelIDList[rLeft, 4]:= FORWARD_ECHO_65;
    ChannelIDList[rLeft, 5]:= BACKWARD_ECHO_65;
    ChannelIDList[rLeft, 6]:= FORWARD_ECHO_42_WEB;
    ChannelIDList[rLeft, 7]:= BACKWARD_ECHO_42_WEB;
    ChannelIDList[rLeft, 8]:= FORWARD_ECHO_42_BASE;
    ChannelIDList[rLeft, 9]:= BACKWARD_ECHO_42_BASE;
    ChannelIDList[rLeft, 10]:= MIRRORSHADOW_0;
    ChannelIDList[rLeft, 11]:= ECHO_0;
    ChannelIDList[rLeft, 12]:= FORWARD_WORK_ECHO_58;
    ChannelIDList[rLeft, 13]:= BACKWARD_UWORK_ECHO_58;

    ChannelIDList[rRight, 0]:= MIRRORSHADOW_0;
    ChannelIDList[rRight, 1]:= ECHO_0;
    ChannelIDList[rRight, 2]:= FORWARD_WORK_ECHO_58;
    ChannelIDList[rRight, 3]:= BACKWARD_UWORK_ECHO_58;
    ChannelIDList[rRight, 4]:= FORWARD_ECHO_65;
    ChannelIDList[rRight, 5]:= BACKWARD_ECHO_65;
    ChannelIDList[rRight, 6]:= FORWARD_ECHO_42_WEB;
    ChannelIDList[rRight, 7]:= BACKWARD_ECHO_42_WEB;
    ChannelIDList[rRight, 8]:= FORWARD_ECHO_42_BASE;
    ChannelIDList[rRight, 9]:= BACKWARD_ECHO_42_BASE;
    ChannelIDList[rRight, 10]:= MIRRORSHADOW_0;
    ChannelIDList[rRight, 11]:= ECHO_0;
    ChannelIDList[rRight, 12]:= FORWARD_UWORK_ECHO_58;
    ChannelIDList[rRight, 13]:= BACKWARD_WORK_ECHO_58;

  // ------------------------------------------------------------------------------

    with EvalChannels2[FORWARD_WORK_ECHO_58] do   begin InfoSide:= isLeft;  InfoPos:= ipCentre; end;
    with EvalChannels2[FORWARD_UWORK_ECHO_58] do  begin InfoSide:= isLeft;  InfoPos:= ipCentre; end;

    with EvalChannels2[BACKWARD_WORK_ECHO_58] do  begin InfoSide:= isRight; InfoPos:= ipCentre; end;
    with EvalChannels2[BACKWARD_UWORK_ECHO_58] do begin InfoSide:= isRight; InfoPos:= ipCentre; end;

    with EvalChannels2[FORWARD_ECHO_65] do        begin InfoSide:= isLeft;  InfoPos:= ipCentre; end;
    with EvalChannels2[BACKWARD_ECHO_65] do       begin InfoSide:= isRight; InfoPos:= ipCentre; end;

    with EvalChannels2[FORWARD_ECHO_42_WEB] do    begin InfoSide:= isLeft;  InfoPos:= ipRight;  end;
    with EvalChannels2[FORWARD_ECHO_42_BASE] do   begin InfoSide:= isLeft;  InfoPos:= ipLeft;   end;
    with EvalChannels2[BACKWARD_ECHO_42_WEB] do   begin InfoSide:= isRight; InfoPos:= ipRight;  end;
    with EvalChannels2[BACKWARD_ECHO_42_BASE] do  begin InfoSide:= isRight; InfoPos:= ipLeft;   end;

    with EvalChannels2[ECHO_0] do                 begin InfoSide:= isRight;  InfoPos:= ipCentre; end;
    with EvalChannels2[MIRRORSHADOW_0] do         begin InfoSide:= isLeft; InfoPos:= ipCentre; end;

  // ------------ ���� ������ �-��������� ------------------

    SetLength(FBScanLines, 12);

    FBScanLines[0].R:= rLeft;
    FBScanLines[0].Use:= True;
    SetLength(FBScanLines[0].Items, 2);
    FBScanLines[0].Items[0].ScanChIdx:= 7;
    FBScanLines[0].Items[1].ScanChIdx:= 2{8};

    FBScanLines[1].R:= rLeft;
    FBScanLines[1].Use:= True;
    SetLength(FBScanLines[1].Items, 2);
    FBScanLines[1].Items[0].ScanChIdx:= 1;
    FBScanLines[1].Items[1].ScanChIdx:= 8{2};

    FBScanLines[2].R:= rLeft;
    FBScanLines[2].Use:= True;
    SetLength(FBScanLines[2].Items, 2);
    FBScanLines[2].Items[0].ScanChIdx:= 3;
    FBScanLines[2].Items[1].ScanChIdx:= 4;

    FBScanLines[3].R:= rLeft;
    FBScanLines[3].Use:= True;
    SetLength(FBScanLines[3].Items, 2);
    FBScanLines[3].Items[0].ScanChIdx:= 5;
    FBScanLines[3].Items[1].ScanChIdx:= 6;

    FBScanLines[4].R:= rLeft;
    FBScanLines[4].Use:= True;
    SetLength(FBScanLines[4].Items, 1);
    FBScanLines[4].Items[0].ScanChIdx:= 0;

    FBScanLines[5].R:= rLeft;
    FBScanLines[5].Use:= True;
    SetLength(FBScanLines[5].Items, 1);
    FBScanLines[5].Items[0].ScanChIdx:= 9;

  //////////////////////////////////////////////////

    FBScanLines[6].R:= rRight;
    FBScanLines[6].Use:= True;
    SetLength(FBScanLines[6].Items, 2);
    FBScanLines[6].Items[0].ScanChIdx:= 1;
    FBScanLines[6].Items[1].ScanChIdx:= 8;

    FBScanLines[7].R:= rRight;
    FBScanLines[7].Use:= True;
    SetLength(FBScanLines[7].Items, 2);
    FBScanLines[7].Items[0].ScanChIdx:= 7;
    FBScanLines[7].Items[1].ScanChIdx:= 2;

    FBScanLines[8].R:= rRight;
    FBScanLines[8].Use:= True;
    SetLength(FBScanLines[8].Items, 2);
    FBScanLines[8].Items[0].ScanChIdx:= 3;
    FBScanLines[8].Items[1].ScanChIdx:= 4;

    FBScanLines[9].R:= rRight;
    FBScanLines[9].Use:= True;
    SetLength(FBScanLines[9].Items, 2);
    FBScanLines[9].Items[0].ScanChIdx:= 5;
    FBScanLines[9].Items[1].ScanChIdx:= 6;

    FBScanLines[10].R:= rRight;
    FBScanLines[10].Use:= True;
    SetLength(FBscanLines[10].Items, 1);
    FBScanLines[10].Items[0].ScanChIdx:= 0;

    FBScanLines[11].R:= rRight;
    FBScanLines[11].Use:= True;
    SetLength(FBscanLines[11].Items, 1);
    FBScanLines[11].Items[0].ScanChIdx:= 9;

    // ------------ ������ ������� �������� ------------

    AddHandScanChannels(FHandChannels);

    // ------------ ������ �-��������� ------------

    SetLength(FScanChannels, 10);
    for i:= 0 to 9 do FScanChannels[i].Gran:= _None;

    FScanChannels[0].Number:= 1;  //  ��� �1: 0 ���� - 1
    FScanChannels[0].EnterAngle:= 0;
    FScanChannels[0].TurnAngle:= 0;
    FScanChannels[0].Position:= 65;
    FScanChannels[0].BScanGateMin:= 3;
    FScanChannels[0].BScanGateMax:= 68;
    FScanChannels[0].DelayMultiply:= 3;
    FScanChannels[0].Delta:= 0;

    FScanChannels[1].Number:= 2;  // ��� �1: 58 ����������
    FScanChannels[1].EnterAngle:= 58;
    FScanChannels[1].TurnAngle:= 34;
    FScanChannels[1].Position:= 65;
    FScanChannels[1].BScanGateMin:= 10;
    FScanChannels[1].BScanGateMax:= 137;
    FScanChannels[1].DelayMultiply:= 1;
    FScanChannels[1].Delta:= - 80;
    FScanChannels[1].Gran:= _UnWork_Left__Work_Right;

    FScanChannels[2].Number:= 3;  // ��� �2: 58 �����������
    FScanChannels[2].EnterAngle:= - 58;
    FScanChannels[2].TurnAngle:= 34;
    FScanChannels[2].Position:= -65;
    FScanChannels[2].BScanGateMin:= 10;
    FScanChannels[2].BScanGateMax:= 137;
    FScanChannels[2].DelayMultiply:= 1;
    FScanChannels[2].Delta:= 80;
    FScanChannels[2].Gran:= _Work_Left__UnWork_Right;

    FScanChannels[3].Number:= 4;  // ��� �1: 65 ����������
    FScanChannels[3].EnterAngle:= 65;
    FScanChannels[3].TurnAngle:= 0;
    FScanChannels[3].Position:= 65;
    FScanChannels[3].BScanGateMin:= 3;
    FScanChannels[3].BScanGateMax:= 91;
    FScanChannels[3].DelayMultiply:= 1;
    FScanChannels[3].Delta:= - 80;

    FScanChannels[4].Number:= 5;  // ��� �2: 65 �����������
    FScanChannels[4].EnterAngle:= - 65;
    FScanChannels[4].TurnAngle:= 0;
    FScanChannels[4].Position:= -65;
    FScanChannels[4].BScanGateMin:= 3;
    FScanChannels[4].BScanGateMax:= 91;
    FScanChannels[4].DelayMultiply:= 1;
    FScanChannels[4].Delta:= 80;

    FScanChannels[5].Number:= 6;  // ��� �1: 42 ����������
    FScanChannels[5].EnterAngle:= 42;
    FScanChannels[5].TurnAngle:= 0;
    FScanChannels[5].Position:= 65;
    FScanChannels[5].BScanGateMin:= 6;
    FScanChannels[5].BScanGateMax:= 174;
    FScanChannels[5].DelayMultiply:= 1;
    FScanChannels[5].Delta:= - 30;

    FScanChannels[6].Number:= 7;  // 42 ��� �2: �����������
    FScanChannels[6].EnterAngle:= - 42;
    FScanChannels[6].TurnAngle:= 0;
    FScanChannels[6].Position:= -65;
    FScanChannels[6].BScanGateMin:= 6;
    FScanChannels[6].BScanGateMax:= 174;
    FScanChannels[6].DelayMultiply:= 1;
    FScanChannels[6].Delta:= 30;

    FScanChannels[7].Number:= 12;  // ��� �1: 58 ����������
    FScanChannels[7].EnterAngle:= 58;
    FScanChannels[7].TurnAngle:= - 34;
    FScanChannels[7].Position:= 65;
    FScanChannels[7].BScanGateMin:= 10;
    FScanChannels[7].BScanGateMax:= 137;
    FScanChannels[7].DelayMultiply:= 1;
    FScanChannels[7].Delta:= - 80;
    FScanChannels[7].Gran:= _Work_Left__UnWork_Right;

    FScanChannels[8].Number:= 13;  // ��� �2: 58 �����������
    FScanChannels[8].EnterAngle:= - 58;
    FScanChannels[8].TurnAngle:= - 34;
    FScanChannels[8].Position:= - 65;
    FScanChannels[8].BScanGateMin:= 10;
    FScanChannels[8].BScanGateMax:= 137;
    FScanChannels[8].DelayMultiply:= 1;
    FScanChannels[8].Delta:= 80;
    FScanChannels[8].Gran:= _UnWork_Left__Work_Right;

    FScanChannels[9].Number:= 11;  //  ��� �2: 0
    FScanChannels[9].EnterAngle:= 0;
    FScanChannels[9].TurnAngle:= 0;
    FScanChannels[9].Position:= - 65;
    FScanChannels[9].BScanGateMin:= 3;
    FScanChannels[9].BScanGateMax:= 68;
    FScanChannels[9].DelayMultiply:= 3;
    FScanChannels[9].Delta:= 0;

    for I:= 0 to High(FScanChannels) do
      FScanChannels[I].BScanDuration:= FScanChannels[I].BScanGateMax - FScanChannels[I].BScanGateMin + 1;

    // ------------  ������ ������ ------------

    SetLength(FEvalChannels, 14);

    FEvalChannels[0].Number:= 0;     // 0 ���
    FEvalChannels[0].ScanChNum:= 1;
    FEvalChannels[0].Method:= imMirrorShadow;
    FEvalChannels[0].Zone:= izAll;
    FEvalChannels[0].ShortName:= 'SHORT_0_MIRRORSHADOW_WP1';
    FEvalChannels[0].FullName:= 'FULL_0_MIRRORSHADOW_WP1';
    FEvalChannels[0].DirColor:= + 1;

    FEvalChannels[1].Number:= 1;     // 0 ���
    FEvalChannels[1].ScanChNum:= 1;
    FEvalChannels[1].Method:= imEcho;
    FEvalChannels[1].Zone:= izAll;
    FEvalChannels[1].ShortName:= 'SHORT_0_ECHO_WP1';
    FEvalChannels[1].FullName:= 'FULL_0_ECHO_WP1';
    FEvalChannels[1].DirColor:= - 1;

    FEvalChannels[2].Number:= 2;     // 58 ����������
    FEvalChannels[2].ScanChNum:= 2;
    FEvalChannels[2].Method:= imEcho;
    FEvalChannels[2].Zone:= izHead;
    FEvalChannels[2].ShortName:= 'SHORT_+58_ECHO_WORK';
    FEvalChannels[2].FullName:= 'FULL_+58_ECHO_WORK';
    FEvalChannels[2].DirColor:= + 1;

    FEvalChannels[3].Number:= 3;     // 58 �����������
    FEvalChannels[3].ScanChNum:= 3;
    FEvalChannels[3].Method:= imEcho;
    FEvalChannels[3].Zone:= izHead;
    FEvalChannels[3].ShortName:= 'SHORT_-58_ECHO_WORK';
    FEvalChannels[3].FullName:= 'FULL_-58_ECHO_WORK';
    FEvalChannels[3].DirColor:= - 1;

    FEvalChannels[4].Number:= 4;     // 65 ����������
    FEvalChannels[4].ScanChNum:= 4;
    FEvalChannels[4].Method:= imEcho;
    FEvalChannels[4].Zone:= izHead;
    FEvalChannels[4].ShortName:= 'SHORT_+65_ECHO';
    FEvalChannels[4].FullName:= 'FULL_+65_ECHO';
    FEvalChannels[4].DirColor:= + 1;

    FEvalChannels[5].Number:= 5;     // 65 ����������
    FEvalChannels[5].ScanChNum:= 5;
    FEvalChannels[5].Method:= imEcho;
    FEvalChannels[5].Zone:= izHead;
    FEvalChannels[5].ShortName:= 'SHORT_-65_ECHO';
    FEvalChannels[5].FullName:= 'FULL_-65_ECHO';
    FEvalChannels[5].DirColor:= - 1;

    FEvalChannels[6].Number:= 6;      // 42 ����������
    FEvalChannels[6].ScanChNum:= 6;
    FEvalChannels[6].Method:= imEcho;
    FEvalChannels[6].Zone:= izWeb;
    FEvalChannels[6].ShortName:= 'SHORT_+42_ECHO_NEAR';
    FEvalChannels[6].FullName:= 'FULL_+42_ECHO_NEAR';
    FEvalChannels[6].DirColor:= + 1;

    FEvalChannels[7].Number:= 7;      // 42 �����������
    FEvalChannels[7].ScanChNum:= 7;
    FEvalChannels[7].Method:= imEcho;
    FEvalChannels[7].Zone:= izWeb;
    FEvalChannels[7].ShortName:= 'SHORT_-42_ECHO_NEAR';
    FEvalChannels[7].FullName:= 'FULL_-42_ECHO_NEAR';
    FEvalChannels[7].DirColor:= - 1;

    FEvalChannels[8].Number:= 8;      // 42 ����������
    FEvalChannels[8].ScanChNum:= 6;
    FEvalChannels[8].Method:= imEcho;
    FEvalChannels[8].Zone:= izBase;
    FEvalChannels[8].ShortName:= 'SHORT_+42_ECHO_FAR';
    FEvalChannels[8].FullName:= 'FULL_+42_ECHO_FAR';
    FEvalChannels[8].DirColor:= + 1;

    FEvalChannels[9].Number:= 9;      // 42 �����������
    FEvalChannels[9].ScanChNum:= 7;
    FEvalChannels[9].Method:= imEcho;
    FEvalChannels[9].Zone:= izBase;
    FEvalChannels[9].ShortName:= 'SHORT_-42_ECHO_FAR';
    FEvalChannels[9].FullName:= 'FULL_-42_ECHO_FAR';
    FEvalChannels[9].DirColor:= - 1;

    FEvalChannels[10].Number:= 12;    // 58 ����������
    FEvalChannels[10].ScanChNum:= 12;
    FEvalChannels[10].Method:= imEcho;
    FEvalChannels[10].Zone:= izHead;
    FEvalChannels[10].ShortName:= 'SHORT_+58_ECHO_NOWORK';
    FEvalChannels[10].FullName:= 'FULL_+58_ECHO_NOWORK';
    FEvalChannels[10].DirColor:= + 1;

    FEvalChannels[11].Number:= 13;    // 58 �����������
    FEvalChannels[11].ScanChNum:= 13;
    FEvalChannels[11].Method:= imEcho;
    FEvalChannels[11].Zone:= izHead;
    FEvalChannels[11].ShortName:= 'SHORT_-58_ECHO_NOWORK';
    FEvalChannels[11].FullName:= 'FULL_-58_ECHO_NOWORK';
    FEvalChannels[11].DirColor:= - 1;

    FEvalChannels[12].Number:= 10;     // 0 ���
    FEvalChannels[12].ScanChNum:= 11;
    FEvalChannels[12].Method:= imMirrorShadow;
    FEvalChannels[12].Zone:= izAll;
    FEvalChannels[12].ShortName:= 'SHORT_0_MIRRORSHADOW_WP2';
    FEvalChannels[12].FullName:= 'FULL_0_MIRRORSHADOW_WP2';
    FEvalChannels[12].DirColor:= + 1;

    FEvalChannels[13].Number:= 11;     // 0 ���
    FEvalChannels[13].ScanChNum:= 11;
    FEvalChannels[13].Method:= imEcho;
    FEvalChannels[13].Zone:= izAll;
    FEvalChannels[13].ShortName:= 'SHORT_0_ECHO_WP2';
    FEvalChannels[13].FullName:= 'FULL_0_ECHO_WP2';
    FEvalChannels[13].DirColor:= - 1;
  end
  else

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

  if CompareMem(@Temp, @EGO_USWScheme2, 8) then
  begin
    FDeviceID:= Temp; // ��� ������������
    FDeviceVer:= 3;

    FMinRail:= rLeft;
    FMaxRail:= rRight;

    FDeviceName:= 'EGO_USWScheme2';

    ChannelIDList[rLeft, 0]:= MIRRORSHADOW_0;
    ChannelIDList[rLeft, 1]:= ECHO_0;
    ChannelIDList[rLeft, 2]:= FORWARD_UWORK_ECHO_65;
    ChannelIDList[rLeft, 3]:= BACKWARD_WORK_ECHO_65;
    ChannelIDList[rLeft, 4]:= FORWARD_ECHO_65;
    ChannelIDList[rLeft, 5]:= BACKWARD_ECHO_65;
    ChannelIDList[rLeft, 6]:= FORWARD_ECHO_42_WEB;
    ChannelIDList[rLeft, 7]:= BACKWARD_ECHO_42_WEB;
    ChannelIDList[rLeft, 8]:= FORWARD_ECHO_42_BASE;
    ChannelIDList[rLeft, 9]:= BACKWARD_ECHO_42_BASE;
    ChannelIDList[rLeft, 10]:= MIRRORSHADOW_0;
    ChannelIDList[rLeft, 11]:= ECHO_0;
    ChannelIDList[rLeft, 12]:= FORWARD_WORK_ECHO_65;
    ChannelIDList[rLeft, 13]:= BACKWARD_UWORK_ECHO_65;

    ChannelIDList[rRight, 0]:= MIRRORSHADOW_0;
    ChannelIDList[rRight, 1]:= ECHO_0;
    ChannelIDList[rRight, 2]:= FORWARD_WORK_ECHO_65;
    ChannelIDList[rRight, 3]:= BACKWARD_UWORK_ECHO_65;
    ChannelIDList[rRight, 4]:= FORWARD_ECHO_65;
    ChannelIDList[rRight, 5]:= BACKWARD_ECHO_65;
    ChannelIDList[rRight, 6]:= FORWARD_ECHO_42_WEB;
    ChannelIDList[rRight, 7]:= BACKWARD_ECHO_42_WEB;
    ChannelIDList[rRight, 8]:= FORWARD_ECHO_42_BASE;
    ChannelIDList[rRight, 9]:= BACKWARD_ECHO_42_BASE;
    ChannelIDList[rRight, 10]:= MIRRORSHADOW_0;
    ChannelIDList[rRight, 11]:= ECHO_0;
    ChannelIDList[rRight, 12]:= FORWARD_UWORK_ECHO_65;
    ChannelIDList[rRight, 13]:= BACKWARD_WORK_ECHO_65;

  // ------------------------------------------------------------------------------

    with EvalChannels2[FORWARD_WORK_ECHO_65] do   begin InfoSide:= isLeft;  InfoPos:= ipLeft;   end;
    with EvalChannels2[FORWARD_UWORK_ECHO_65] do  begin InfoSide:= isLeft;  InfoPos:= ipRight;  end;

    with EvalChannels2[BACKWARD_WORK_ECHO_65] do  begin InfoSide:= isRight; InfoPos:= ipLeft;   end;
    with EvalChannels2[BACKWARD_UWORK_ECHO_65] do begin InfoSide:= isRight; InfoPos:= ipRight;  end;

    with EvalChannels2[FORWARD_ECHO_65] do        begin InfoSide:= isLeft;  InfoPos:= ipCentre; end;
    with EvalChannels2[BACKWARD_ECHO_65] do       begin InfoSide:= isRight; InfoPos:= ipCentre; end;

    with EvalChannels2[FORWARD_ECHO_42_WEB] do    begin InfoSide:= isLeft;  InfoPos:= ipRight;  end;
    with EvalChannels2[FORWARD_ECHO_42_BASE] do   begin InfoSide:= isLeft;  InfoPos:= ipLeft;   end;
    with EvalChannels2[BACKWARD_ECHO_42_WEB] do   begin InfoSide:= isRight; InfoPos:= ipRight;  end;
    with EvalChannels2[BACKWARD_ECHO_42_BASE] do  begin InfoSide:= isRight; InfoPos:= ipLeft;   end;

    with EvalChannels2[ECHO_0] do                 begin InfoSide:= isRight;  InfoPos:= ipCentre; end;
    with EvalChannels2[MIRRORSHADOW_0] do         begin InfoSide:= isLeft; InfoPos:= ipCentre; end;

  // ------------ ���� ������ �-��������� ------------------

    SetLength(FBScanLines, 10);

    FBScanLines[0].R:= rLeft;
    FBScanLines[0].Use:= True;
    SetLength(FBScanLines[0].Items, 2);
    FBScanLines[0].Items[0].ScanChIdx:= 7;
    FBScanLines[0].Items[1].ScanChIdx:= 2;

    FBScanLines[1].R:= rLeft;
    FBScanLines[1].Use:= True;
    SetLength(FBScanLines[1].Items, 2);
    FBScanLines[1].Items[0].ScanChIdx:= 1;
    FBScanLines[1].Items[1].ScanChIdx:= 8;

    FBScanLines[2].R:= rLeft;
    FBScanLines[2].Use:= True;
    SetLength(FBScanLines[2].Items, 2);
    FBScanLines[2].Items[0].ScanChIdx:= 3;
    FBScanLines[2].Items[1].ScanChIdx:= 4;

    FBScanLines[3].R:= rLeft;
    FBScanLines[3].Use:= True;
    SetLength(FBScanLines[3].Items, 2);
    FBScanLines[3].Items[0].ScanChIdx:= 5;
    FBScanLines[3].Items[1].ScanChIdx:= 6;

    FBScanLines[4].R:= rLeft;
    FBScanLines[4].Use:= True;
    SetLength(FBScanLines[4].Items, 2);
    FBScanLines[4].Items[0].ScanChIdx:= 0;
    FBScanLines[4].Items[1].ScanChIdx:= 9;

  //////////////////////////////////////////////////

    FBScanLines[5].R:= rRight;
    FBScanLines[5].Use:= True;
    SetLength(FBScanLines[5].Items, 2);
    FBScanLines[5].Items[0].ScanChIdx:= 1;
    FBScanLines[5].Items[1].ScanChIdx:= 8;

    FBScanLines[6].R:= rRight;
    FBScanLines[6].Use:= True;
    SetLength(FBScanLines[6].Items, 2);
    FBScanLines[6].Items[0].ScanChIdx:= 7;
    FBScanLines[6].Items[1].ScanChIdx:= 2;

    FBScanLines[7].R:= rRight;
    FBScanLines[7].Use:= True;
    SetLength(FBScanLines[7].Items, 2);
    FBScanLines[7].Items[0].ScanChIdx:= 3;
    FBScanLines[7].Items[1].ScanChIdx:= 4;

    FBScanLines[8].R:= rRight;
    FBScanLines[8].Use:= True;
    SetLength(FBscanLines[8].Items, 2);
    FBScanLines[8].Items[0].ScanChIdx:= 5;
    FBScanLines[8].Items[1].ScanChIdx:= 6;

    FBScanLines[9].R:= rRight;
    FBScanLines[9].Use:= True;
    SetLength(FBscanLines[9].Items, 2);
    FBScanLines[9].Items[0].ScanChIdx:= 0;
    FBScanLines[9].Items[1].ScanChIdx:= 9;

    // ------------ ������ ������� �������� ------------

    AddHandScanChannels(FHandChannels);

    // ------------ ������ �-��������� ------------

    SetLength(FScanChannels, 10);
    for i:= 0 to 9 do FScanChannels[i].Gran:= _None;

    FScanChannels[0].Number:= 1;  //  ��� �1: 0 ���� - 1
    FScanChannels[0].EnterAngle:= 0;
    FScanChannels[0].TurnAngle:= 0;
    FScanChannels[0].Position:= - 65;
    FScanChannels[0].BScanGateMin:= 3;
    FScanChannels[0].BScanGateMax:= 68;
    FScanChannels[0].DelayMultiply:= 3;
    FScanChannels[0].Delta:= 0;

    FScanChannels[1].Number:= 2;  // ��� �1: 58 ����� ����������
    FScanChannels[1].EnterAngle:= 65;
    FScanChannels[1].TurnAngle:= 0;
    FScanChannels[1].Position:= 65;
    FScanChannels[1].BScanGateMin:= 10;
    FScanChannels[1].BScanGateMax:= 137;
    FScanChannels[1].DelayMultiply:= 1;
    FScanChannels[1].Delta:= - 80;
    FScanChannels[1].Gran:= _UnWork_Left__Work_Right;

    FScanChannels[2].Number:= 3;  // ��� �2: 58 ��� �����������
    FScanChannels[2].EnterAngle:= - 65;
    FScanChannels[2].TurnAngle:= 0;
    FScanChannels[2].Position:= -65;
    FScanChannels[2].BScanGateMin:= 10;
    FScanChannels[2].BScanGateMax:= 137;
    FScanChannels[2].DelayMultiply:= 1;
    FScanChannels[2].Delta:= 80;
    FScanChannels[2].Gran:= _Work_Left__UnWork_Right;

    FScanChannels[3].Number:= 4;  // ��� �1: 65 ����������
    FScanChannels[3].EnterAngle:= 65;
    FScanChannels[3].TurnAngle:= 0;
    FScanChannels[3].Position:= 65;
    FScanChannels[3].BScanGateMin:= 3;
    FScanChannels[3].BScanGateMax:= 91;
    FScanChannels[3].DelayMultiply:= 1;
    FScanChannels[3].Delta:= - 80;

    FScanChannels[4].Number:= 5;  // ��� �2: 65 �����������
    FScanChannels[4].EnterAngle:= - 65;
    FScanChannels[4].TurnAngle:= 0;
    FScanChannels[4].Position:= -65;
    FScanChannels[4].BScanGateMin:= 3;
    FScanChannels[4].BScanGateMax:= 91;
    FScanChannels[4].DelayMultiply:= 1;
    FScanChannels[4].Delta:= 80;

    FScanChannels[5].Number:= 6;  // ��� �1: 42 ����������
    FScanChannels[5].EnterAngle:= 42;
    FScanChannels[5].TurnAngle:= 0;
    FScanChannels[5].Position:= 65;
    FScanChannels[5].BScanGateMin:= 6;
    FScanChannels[5].BScanGateMax:= 174;
    FScanChannels[5].DelayMultiply:= 1;
    FScanChannels[5].Delta:= - 30;

    FScanChannels[6].Number:= 7;  // 42 ��� �2: 42 �����������
    FScanChannels[6].EnterAngle:= - 42;
    FScanChannels[6].TurnAngle:= 0;
    FScanChannels[6].Position:= -65;
    FScanChannels[6].BScanGateMin:= 6;
    FScanChannels[6].BScanGateMax:= 174;
    FScanChannels[6].DelayMultiply:= 1;
    FScanChannels[6].Delta:= 30;

    FScanChannels[7].Number:= 12;  // ��� �1: 58 ��� ����������
    FScanChannels[7].EnterAngle:= 65;
    FScanChannels[7].TurnAngle:= 0;
    FScanChannels[7].Position:= 65;
    FScanChannels[7].BScanGateMin:= 10;
    FScanChannels[7].BScanGateMax:= 137;
    FScanChannels[7].DelayMultiply:= 1;
    FScanChannels[7].Delta:= - 80;
    FScanChannels[7].Gran:= _Work_Left__UnWork_Right;

    FScanChannels[8].Number:= 13;  // ��� �2: 58 ����� �����������
    FScanChannels[8].EnterAngle:= - 65;
    FScanChannels[8].TurnAngle:= 0;
    FScanChannels[8].Position:= - 65;
    FScanChannels[8].BScanGateMin:= 10;
    FScanChannels[8].BScanGateMax:= 137;
    FScanChannels[8].DelayMultiply:= 1;
    FScanChannels[8].Delta:= 80;
    FScanChannels[8].Gran:= _UnWork_Left__Work_Right;

    FScanChannels[9].Number:= 11;  //  ��� �2: 0
    FScanChannels[9].EnterAngle:= 0;
    FScanChannels[9].TurnAngle:= 0;
    FScanChannels[9].Position:= 65;
    FScanChannels[9].BScanGateMin:= 3;
    FScanChannels[9].BScanGateMax:= 68;
    FScanChannels[9].DelayMultiply:= 3;
    FScanChannels[9].Delta:= 0;

    for I:= 0 to High(FScanChannels) do
      FScanChannels[I].BScanDuration:= FScanChannels[I].BScanGateMax - FScanChannels[I].BScanGateMin + 1;

    // ------------  ������ ������ ------------

    SetLength(FEvalChannels, 14);

    FEvalChannels[0].Number:= 0;     // 0 ���
    FEvalChannels[0].ScanChNum:= 1;
    FEvalChannels[0].Method:= imMirrorShadow;
    FEvalChannels[0].Zone:= izAll;
    FEvalChannels[0].ShortName:= 'SHORT_0_MIRRORSHADOW_WP1';
    FEvalChannels[0].FullName:= 'FULL_0_MIRRORSHADOW_WP1';
    FEvalChannels[0].DirColor:= + 1;

    FEvalChannels[1].Number:= 1;     // 0 ���
    FEvalChannels[1].ScanChNum:= 1;
    FEvalChannels[1].Method:= imEcho;
    FEvalChannels[1].Zone:= izAll;
    FEvalChannels[1].ShortName:= 'SHORT_0_ECHO_WP1';
    FEvalChannels[1].FullName:= 'FULL_0_ECHO_WP1';
    FEvalChannels[1].DirColor:= - 1;

    FEvalChannels[2].Number:= 2;     // 65 ����� ����������
    FEvalChannels[2].ScanChNum:= 2;
    FEvalChannels[2].Method:= imEcho;
    FEvalChannels[2].Zone:= izHead;
    FEvalChannels[2].ShortName:= 'SHORT_+65_ECHO_NOWORK';
    FEvalChannels[2].FullName:= 'FULL_+65_ECHO_NOWORK';
    FEvalChannels[2].DirColor:= + 1;

    FEvalChannels[3].Number:= 3;     // 65 �����������
    FEvalChannels[3].ScanChNum:= 3;
    FEvalChannels[3].Method:= imEcho;
    FEvalChannels[3].Zone:= izHead;
    FEvalChannels[3].ShortName:= 'SHORT_-65_ECHO_WORK';
    FEvalChannels[3].FullName:= 'FULL_-65_ECHO_WORK';
    FEvalChannels[3].DirColor:= - 1;

    FEvalChannels[4].Number:= 4;     // 65 ����������
    FEvalChannels[4].ScanChNum:= 4;
    FEvalChannels[4].Method:= imEcho;
    FEvalChannels[4].Zone:= izHead;
    FEvalChannels[4].ShortName:= 'SHORT_+65_ECHO';
    FEvalChannels[4].FullName:= 'FULL_+65_ECHO';
    FEvalChannels[4].DirColor:= + 1;

    FEvalChannels[5].Number:= 5;     // 65 ����������
    FEvalChannels[5].ScanChNum:= 5;
    FEvalChannels[5].Method:= imEcho;
    FEvalChannels[5].Zone:= izHead;
    FEvalChannels[5].ShortName:= 'SHORT_-65_ECHO';
    FEvalChannels[5].FullName:= 'FULL_-65_ECHO';
    FEvalChannels[5].DirColor:= - 1;

    FEvalChannels[6].Number:= 6;      // 42 ����������
    FEvalChannels[6].ScanChNum:= 6;
    FEvalChannels[6].Method:= imEcho;
    FEvalChannels[6].Zone:= izWeb;
    FEvalChannels[6].ShortName:= 'SHORT_+42_ECHO_NEAR';
    FEvalChannels[6].FullName:= 'FULL_+42_ECHO_NEAR';
    FEvalChannels[6].DirColor:= + 1;

    FEvalChannels[7].Number:= 7;      // 42 �����������
    FEvalChannels[7].ScanChNum:= 7;
    FEvalChannels[7].Method:= imEcho;
    FEvalChannels[7].Zone:= izWeb;
    FEvalChannels[7].ShortName:= 'SHORT_-42_ECHO_NEAR';
    FEvalChannels[7].FullName:= 'FULL_-42_ECHO_NEAR';
    FEvalChannels[7].DirColor:= - 1;

    FEvalChannels[8].Number:= 8;      // 42 ����������
    FEvalChannels[8].ScanChNum:= 6;
    FEvalChannels[8].Method:= imEcho;
    FEvalChannels[8].Zone:= izBase;
    FEvalChannels[8].ShortName:= 'SHORT_+42_ECHO_FAR';
    FEvalChannels[8].FullName:= 'FULL_+42_ECHO_FAR';
    FEvalChannels[8].DirColor:= + 1;

    FEvalChannels[9].Number:= 9;      // 42 �����������
    FEvalChannels[9].ScanChNum:= 7;
    FEvalChannels[9].Method:= imEcho;
    FEvalChannels[9].Zone:= izBase;
    FEvalChannels[9].ShortName:= 'SHORT_-42_ECHO_FAR';
    FEvalChannels[9].FullName:= 'FULL_-42_ECHO_FAR';
    FEvalChannels[9].DirColor:= - 1;

    FEvalChannels[10].Number:= 12;    // 65 ����������
    FEvalChannels[10].ScanChNum:= 12;
    FEvalChannels[10].Method:= imEcho;
    FEvalChannels[10].Zone:= izHead;
    FEvalChannels[10].ShortName:= 'SHORT_+65_ECHO_WORK';
    FEvalChannels[10].FullName:= 'FULL_+65_ECHO_WORK';
    FEvalChannels[10].DirColor:= + 1;

    FEvalChannels[11].Number:= 13;    // 65 �����������
    FEvalChannels[11].ScanChNum:= 13;
    FEvalChannels[11].Method:= imEcho;
    FEvalChannels[11].Zone:= izHead;
    FEvalChannels[11].ShortName:= 'SHORT_-65_ECHO_NOWORK';
    FEvalChannels[11].FullName:= 'FULL_-65_ECHO_NOWORK';
    FEvalChannels[11].DirColor:= - 1;

    FEvalChannels[12].Number:= 10;     // 0 ���
    FEvalChannels[12].ScanChNum:= 11;
    FEvalChannels[12].Method:= imMirrorShadow;
    FEvalChannels[12].Zone:= izAll;
    FEvalChannels[12].ShortName:= 'SHORT_0_MIRRORSHADOW_WP2';
    FEvalChannels[12].FullName:= 'FULL_0_MIRRORSHADOW_WP2';
    FEvalChannels[12].DirColor:= + 1;

    FEvalChannels[13].Number:= 11;     // 0 ���
    FEvalChannels[13].ScanChNum:= 11;
    FEvalChannels[13].Method:= imEcho;
    FEvalChannels[13].Zone:= izAll;
    FEvalChannels[13].ShortName:= 'SHORT_0_ECHO_WP2';
    FEvalChannels[13].FullName:= 'FULL_0_ECHO_WP2';
    FEvalChannels[13].DirColor:= - 1;
  end
  else
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
  if CompareMem(@Temp, @EGO_USWScheme3, 8) then
  begin
    FDeviceID:= Temp; // ��� ������������
    FDeviceVer:= 3;

    FMinRail:= rLeft;
    FMaxRail:= rRight;

    FDeviceName:= 'EGO_USWScheme3';

    ChannelIDList[rLeft, 0]:= MIRRORSHADOW_0;
    ChannelIDList[rLeft, 1]:= ECHO_0;
    ChannelIDList[rLeft, 2]:= FORWARD_UWORK_ECHO_65;
    ChannelIDList[rLeft, 3]:= BACKWARD_WORK_ECHO_65;
    ChannelIDList[rLeft, 4]:= FORWARD_ECHO_65;
    ChannelIDList[rLeft, 5]:= BACKWARD_ECHO_65;
    ChannelIDList[rLeft, 6]:= FORWARD_ECHO_42_WEB;
    ChannelIDList[rLeft, 7]:= BACKWARD_ECHO_42_WEB;
    ChannelIDList[rLeft, 8]:= FORWARD_ECHO_42_BASE;
    ChannelIDList[rLeft, 9]:= BACKWARD_ECHO_42_BASE;
    ChannelIDList[rLeft, 10]:= MIRRORSHADOW_0;
    ChannelIDList[rLeft, 11]:= ECHO_0;
    ChannelIDList[rLeft, 12]:= FORWARD_WORK_ECHO_65;
    ChannelIDList[rLeft, 13]:= BACKWARD_UWORK_ECHO_65;
    ChannelIDList[rLeft, 14]:= UWORK_MSM_55;
    ChannelIDList[rLeft, 15]:= WORK_MSM_55;

    ChannelIDList[rRight, 0]:= MIRRORSHADOW_0;
    ChannelIDList[rRight, 1]:= ECHO_0;
    ChannelIDList[rRight, 2]:= FORWARD_WORK_ECHO_65;
    ChannelIDList[rRight, 3]:= BACKWARD_UWORK_ECHO_65;
    ChannelIDList[rRight, 4]:= FORWARD_ECHO_65;
    ChannelIDList[rRight, 5]:= BACKWARD_ECHO_65;
    ChannelIDList[rRight, 6]:= FORWARD_ECHO_42_WEB;
    ChannelIDList[rRight, 7]:= BACKWARD_ECHO_42_WEB;
    ChannelIDList[rRight, 8]:= FORWARD_ECHO_42_BASE;
    ChannelIDList[rRight, 9]:= BACKWARD_ECHO_42_BASE;
    ChannelIDList[rRight, 10]:= MIRRORSHADOW_0;
    ChannelIDList[rRight, 11]:= ECHO_0;
    ChannelIDList[rRight, 12]:= FORWARD_UWORK_ECHO_65;
    ChannelIDList[rRight, 13]:= BACKWARD_WORK_ECHO_65;
    ChannelIDList[rRight, 14]:= WORK_MSM_55;
    ChannelIDList[rRight, 15]:= UWORK_MSM_55;

  // ------------------------------------------------------------------------------

    with EvalChannels2[FORWARD_WORK_ECHO_65] do   begin InfoSide:= isLeft;  InfoPos:= ipLeft;   end;
    with EvalChannels2[FORWARD_UWORK_ECHO_65] do  begin InfoSide:= isLeft;  InfoPos:= ipRight;  end;

    with EvalChannels2[BACKWARD_WORK_ECHO_65] do  begin InfoSide:= isRight; InfoPos:= ipLeft;   end;
    with EvalChannels2[BACKWARD_UWORK_ECHO_65] do begin InfoSide:= isRight; InfoPos:= ipRight;  end;

    with EvalChannels2[FORWARD_ECHO_65] do        begin InfoSide:= isLeft;  InfoPos:= ipCentre; end;
    with EvalChannels2[BACKWARD_ECHO_65] do       begin InfoSide:= isRight; InfoPos:= ipCentre; end;

    with EvalChannels2[FORWARD_ECHO_42_WEB] do    begin InfoSide:= isLeft;  InfoPos:= ipRight;  end;
    with EvalChannels2[FORWARD_ECHO_42_BASE] do   begin InfoSide:= isLeft;  InfoPos:= ipLeft;   end;
    with EvalChannels2[BACKWARD_ECHO_42_WEB] do   begin InfoSide:= isRight; InfoPos:= ipRight;  end;
    with EvalChannels2[BACKWARD_ECHO_42_BASE] do  begin InfoSide:= isRight; InfoPos:= ipLeft;   end;

    with EvalChannels2[ECHO_0] do                 begin InfoSide:= isRight;  InfoPos:= ipCentre; end;
    with EvalChannels2[MIRRORSHADOW_0] do         begin InfoSide:= isLeft; InfoPos:= ipCentre; end;

    with EvalChannels2[WORK_MSM_55] do            begin InfoSide:= isLeft; InfoPos:= ipCentre; end;
    with EvalChannels2[UWORK_MSM_55] do           begin InfoSide:= isRight; InfoPos:= ipCentre; end;

  // ------------ ���� ������ �-��������� ------------------

    SetLength(FBScanLines, 10);

    FBScanLines[0].R:= rLeft;
    FBScanLines[0].Use:= True;
    SetLength(FBScanLines[0].Items, 4);
    FBScanLines[0].Items[0].ScanChIdx:= 7;
    FBScanLines[0].Items[1].ScanChIdx:= 2;
    FBScanLines[0].Items[2].ScanChIdx:= 1;
    FBScanLines[0].Items[3].ScanChIdx:= 8;

    FBScanLines[1].R:= rLeft;
    FBScanLines[1].Use:= True;
    SetLength(FBScanLines[1].Items, 2);
    FBScanLines[1].Items[0].ScanChIdx:= 3;
    FBScanLines[1].Items[1].ScanChIdx:= 4;

    FBScanLines[2].R:= rLeft;
    FBScanLines[2].Use:= True;
    SetLength(FBScanLines[2].Items, 2);
    FBScanLines[2].Items[0].ScanChIdx:= 5;
    FBScanLines[2].Items[1].ScanChIdx:= 6;

    FBScanLines[3].R:= rLeft;
    FBScanLines[3].Use:= True;
    SetLength(FBScanLines[3].Items, 2);
    FBScanLines[3].Items[0].ScanChIdx:= 0;
    FBScanLines[3].Items[1].ScanChIdx:= 9;

    FBScanLines[4].R:= rLeft;
    FBScanLines[4].Use:= True;
    SetLength(FBScanLines[4].Items, 2);
    FBScanLines[4].Items[0].ScanChIdx:= 10;
    FBScanLines[4].Items[1].ScanChIdx:= 11;

  //////////////////////////////////////////////////

    FBScanLines[5].R:= rRight;
    FBScanLines[5].Use:= True;
    SetLength(FBScanLines[5].Items, 4);
    FBScanLines[5].Items[0].ScanChIdx:= 1;
    FBScanLines[5].Items[1].ScanChIdx:= 8;
    FBScanLines[5].Items[2].ScanChIdx:= 7;
    FBScanLines[5].Items[3].ScanChIdx:= 2;

    FBScanLines[6].R:= rRight;
    FBScanLines[6].Use:= True;
    SetLength(FBScanLines[6].Items, 2);
    FBScanLines[6].Items[0].ScanChIdx:= 3;
    FBScanLines[6].Items[1].ScanChIdx:= 4;

    FBScanLines[7].R:= rRight;
    FBScanLines[7].Use:= True;
    SetLength(FBScanLines[7].Items, 2);
    FBScanLines[7].Items[0].ScanChIdx:= 5;
    FBScanLines[7].Items[1].ScanChIdx:= 6;

    FBScanLines[8].R:= rRight;
    FBScanLines[8].Use:= True;
    SetLength(FBscanLines[8].Items, 2);
    FBScanLines[8].Items[0].ScanChIdx:= 0;
    FBScanLines[8].Items[1].ScanChIdx:= 9;

    FBScanLines[9].R:= rRight;
    FBScanLines[9].Use:= True;
    SetLength(FBscanLines[9].Items, 2);
    FBScanLines[9].Items[0].ScanChIdx:= 10;
    FBScanLines[9].Items[1].ScanChIdx:= 11;

    // ------------ ������ ������� �������� ------------

    AddHandScanChannels(FHandChannels);

    // ------------ ������ �-��������� ------------

    SetLength(FScanChannels, 12);
    for i:= 0 to 9 do FScanChannels[i].Gran:= _None;

    FScanChannels[0].Number:= 1;  //  ��� �1: 0 ���� - 1
    FScanChannels[0].EnterAngle:= 0;
    FScanChannels[0].TurnAngle:= 0;
    FScanChannels[0].Position:= - 65;
    FScanChannels[0].BScanGateMin:= 5;
    FScanChannels[0].BScanGateMax:= 70;
    FScanChannels[0].DelayMultiply:= 3;
    FScanChannels[0].Delta:= 0;

    FScanChannels[1].Number:= 2;  // ��� �1: 65 ����� ����������
    FScanChannels[1].EnterAngle:= 65;
    FScanChannels[1].TurnAngle:= 0;
    FScanChannels[1].Position:= 65;
    FScanChannels[1].BScanGateMin:= 5;
    FScanChannels[1].BScanGateMax:= 95;
    FScanChannels[1].DelayMultiply:= 1;
    FScanChannels[1].Delta:= - 80;
    FScanChannels[1].Gran:= _UnWork_Left__Work_Right;

    FScanChannels[2].Number:= 3;  // ��� �2: 65 ��� �����������
    FScanChannels[2].EnterAngle:= - 65;
    FScanChannels[2].TurnAngle:= 0;
    FScanChannels[2].Position:= -65;
    FScanChannels[2].BScanGateMin:= 5;
    FScanChannels[2].BScanGateMax:= 95;
    FScanChannels[2].DelayMultiply:= 1;
    FScanChannels[2].Delta:= 80;
    FScanChannels[2].Gran:= _Work_Left__UnWork_Right;

    FScanChannels[3].Number:= 4;  // ��� �1: 65 ����������
    FScanChannels[3].EnterAngle:= 65;
    FScanChannels[3].TurnAngle:= 0;
    FScanChannels[3].Position:= 65;
    FScanChannels[3].BScanGateMin:= 5;
    FScanChannels[3].BScanGateMax:= 95;
    FScanChannels[3].DelayMultiply:= 1;
    FScanChannels[3].Delta:= - 80;

    FScanChannels[4].Number:= 5;  // ��� �2: 65 �����������
    FScanChannels[4].EnterAngle:= - 65;
    FScanChannels[4].TurnAngle:= 0;
    FScanChannels[4].Position:= -65;
    FScanChannels[4].BScanGateMin:= 5;
    FScanChannels[4].BScanGateMax:= 95;
    FScanChannels[4].DelayMultiply:= 1;
    FScanChannels[4].Delta:= 80;

    FScanChannels[5].Number:= 6;  // ��� �1: 42 ����������
    FScanChannels[5].EnterAngle:= 42;
    FScanChannels[5].TurnAngle:= 0;
    FScanChannels[5].Position:= 65;
    FScanChannels[5].BScanGateMin:= 10;
    FScanChannels[5].BScanGateMax:= 163;
    FScanChannels[5].DelayMultiply:= 1;
    FScanChannels[5].Delta:= - 30;

    FScanChannels[6].Number:= 7;  // 42 ��� �2: 42 �����������
    FScanChannels[6].EnterAngle:= - 42;
    FScanChannels[6].TurnAngle:= 0;
    FScanChannels[6].Position:= -65;
    FScanChannels[6].BScanGateMin:= 10;
    FScanChannels[6].BScanGateMax:= 163;
    FScanChannels[6].DelayMultiply:= 1;
    FScanChannels[6].Delta:= 30;

    FScanChannels[7].Number:= 12;  // ��� �1: 65 ��� ����������
    FScanChannels[7].EnterAngle:= 65;
    FScanChannels[7].TurnAngle:= 0;
    FScanChannels[7].Position:= 65;
    FScanChannels[7].BScanGateMin:= 5;
    FScanChannels[7].BScanGateMax:= 95;
    FScanChannels[7].DelayMultiply:= 1;
    FScanChannels[7].Delta:= - 80;
    FScanChannels[7].Gran:= _Work_Left__UnWork_Right;

    FScanChannels[8].Number:= 13;  // ��� �2: 65 ����� �����������
    FScanChannels[8].EnterAngle:= - 65;
    FScanChannels[8].TurnAngle:= 0;
    FScanChannels[8].Position:= - 65;
    FScanChannels[8].BScanGateMin:= 5;
    FScanChannels[8].BScanGateMax:= 95;
    FScanChannels[8].DelayMultiply:= 1;
    FScanChannels[8].Delta:= 80;
    FScanChannels[8].Gran:= _UnWork_Left__Work_Right;

    FScanChannels[9].Number:= 11;  //  ��� �2: 0
    FScanChannels[9].EnterAngle:= 0;
    FScanChannels[9].TurnAngle:= 0;
    FScanChannels[9].Position:= 65;
    FScanChannels[9].BScanGateMin:= 5;
    FScanChannels[9].BScanGateMax:= 70;
    FScanChannels[9].DelayMultiply:= 3;
    FScanChannels[9].Delta:= 0;

    FScanChannels[10].Number:= 14;  //  ��� �1: 55 �����
    FScanChannels[10].EnterAngle:= 55;
    FScanChannels[10].TurnAngle:= -90;
    FScanChannels[10].Position:= 65;  //?
    FScanChannels[10].BScanGateMin:= 3;
    FScanChannels[10].BScanGateMax:= 78;
    FScanChannels[10].DelayMultiply:= 1;
    FScanChannels[10].Delta:= 0;
    FScanChannels[10].Gran:= _UnWork_Left__Work_Right;

    FScanChannels[11].Number:= 15;  //  ��� �2: 55 ���
    FScanChannels[11].EnterAngle:= 55;
    FScanChannels[11].TurnAngle:= 90;
    FScanChannels[11].Position:= 65;     //?
    FScanChannels[11].BScanGateMin:= 3;
    FScanChannels[11].BScanGateMax:= 78;
    FScanChannels[11].DelayMultiply:= 1;
    FScanChannels[11].Delta:= 0;
    FScanChannels[11].Gran:= _Work_Left__UnWork_Right;


    for I:= 0 to High(FScanChannels) do
      FScanChannels[I].BScanDuration:= FScanChannels[I].BScanGateMax - FScanChannels[I].BScanGateMin + 1;

    // ------------  ������ ������ ------------

    SetLength(FEvalChannels, 16);

    FEvalChannels[0].Number:= 0;     // 0 ���
    FEvalChannels[0].ScanChNum:= 1;
    FEvalChannels[0].Method:= imMirrorShadow;
    FEvalChannels[0].Zone:= izAll;
    FEvalChannels[0].ShortName:= 'SHORT_0_MIRRORSHADOW_WP1';
    FEvalChannels[0].FullName:= 'FULL_0_MIRRORSHADOW_WP1';
    FEvalChannels[0].DirColor:= + 1;

    FEvalChannels[1].Number:= 1;     // 0 ���
    FEvalChannels[1].ScanChNum:= 1;
    FEvalChannels[1].Method:= imEcho;
    FEvalChannels[1].Zone:= izAll;
    FEvalChannels[1].ShortName:= 'SHORT_0_ECHO_WP1';
    FEvalChannels[1].FullName:= 'FULL_0_ECHO_WP1';
    FEvalChannels[1].DirColor:= - 1;

    FEvalChannels[2].Number:= 2;     // 65 ����� ����������
    FEvalChannels[2].ScanChNum:= 2;
    FEvalChannels[2].Method:= imEcho;
    FEvalChannels[2].Zone:= izHead;
    FEvalChannels[2].ShortName:= 'SHORT_+65_ECHO_NOWORK';
    FEvalChannels[2].FullName:= 'FULL_+65_ECHO_NOWORK';
    FEvalChannels[2].DirColor:= + 1;

    FEvalChannels[3].Number:= 3;     // 65 �����������
    FEvalChannels[3].ScanChNum:= 3;
    FEvalChannels[3].Method:= imEcho;
    FEvalChannels[3].Zone:= izHead;
    FEvalChannels[3].ShortName:= 'SHORT_-65_ECHO_WORK';
    FEvalChannels[3].FullName:= 'FULL_-65_ECHO_WORK';
    FEvalChannels[3].DirColor:= - 1;

    FEvalChannels[4].Number:= 4;     // 65 ����������
    FEvalChannels[4].ScanChNum:= 4;
    FEvalChannels[4].Method:= imEcho;
    FEvalChannels[4].Zone:= izHead;
    FEvalChannels[4].ShortName:= 'SHORT_+65_ECHO';
    FEvalChannels[4].FullName:= 'FULL_+65_ECHO';
    FEvalChannels[4].DirColor:= + 1;

    FEvalChannels[5].Number:= 5;     // 65 ����������
    FEvalChannels[5].ScanChNum:= 5;
    FEvalChannels[5].Method:= imEcho;
    FEvalChannels[5].Zone:= izHead;
    FEvalChannels[5].ShortName:= 'SHORT_-65_ECHO';
    FEvalChannels[5].FullName:= 'FULL_-65_ECHO';
    FEvalChannels[5].DirColor:= - 1;

    FEvalChannels[6].Number:= 6;      // 42 ����������
    FEvalChannels[6].ScanChNum:= 6;
    FEvalChannels[6].Method:= imEcho;
    FEvalChannels[6].Zone:= izWeb;
    FEvalChannels[6].ShortName:= 'SHORT_+42_ECHO_NEAR';
    FEvalChannels[6].FullName:= 'FULL_+42_ECHO_NEAR';
    FEvalChannels[6].DirColor:= + 1;

    FEvalChannels[7].Number:= 7;      // 42 �����������
    FEvalChannels[7].ScanChNum:= 7;
    FEvalChannels[7].Method:= imEcho;
    FEvalChannels[7].Zone:= izWeb;
    FEvalChannels[7].ShortName:= 'SHORT_-42_ECHO_NEAR';
    FEvalChannels[7].FullName:= 'FULL_-42_ECHO_NEAR';
    FEvalChannels[7].DirColor:= - 1;

    FEvalChannels[8].Number:= 8;      // 42 ����������
    FEvalChannels[8].ScanChNum:= 6;
    FEvalChannels[8].Method:= imEcho;
    FEvalChannels[8].Zone:= izBase;
    FEvalChannels[8].ShortName:= 'SHORT_+42_ECHO_FAR';
    FEvalChannels[8].FullName:= 'FULL_+42_ECHO_FAR';
    FEvalChannels[8].DirColor:= + 1;

    FEvalChannels[9].Number:= 9;      // 42 �����������
    FEvalChannels[9].ScanChNum:= 7;
    FEvalChannels[9].Method:= imEcho;
    FEvalChannels[9].Zone:= izBase;
    FEvalChannels[9].ShortName:= 'SHORT_-42_ECHO_FAR';
    FEvalChannels[9].FullName:= 'FULL_-42_ECHO_FAR';
    FEvalChannels[9].DirColor:= - 1;

    FEvalChannels[10].Number:= 12;    // 65 ����������
    FEvalChannels[10].ScanChNum:= 12;
    FEvalChannels[10].Method:= imEcho;
    FEvalChannels[10].Zone:= izHead;
    FEvalChannels[10].ShortName:= 'SHORT_+65_ECHO_WORK';
    FEvalChannels[10].FullName:= 'FULL_+65_ECHO_WORK';
    FEvalChannels[10].DirColor:= + 1;

    FEvalChannels[11].Number:= 13;    // 65 �����������
    FEvalChannels[11].ScanChNum:= 13;
    FEvalChannels[11].Method:= imEcho;
    FEvalChannels[11].Zone:= izHead;
    FEvalChannels[11].ShortName:= 'SHORT_-65_ECHO_NOWORK';
    FEvalChannels[11].FullName:= 'FULL_-65_ECHO_NOWORK';
    FEvalChannels[11].DirColor:= - 1;

    FEvalChannels[12].Number:= 10;     // 0 ���
    FEvalChannels[12].ScanChNum:= 11;
    FEvalChannels[12].Method:= imMirrorShadow;
    FEvalChannels[12].Zone:= izAll;
    FEvalChannels[12].ShortName:= 'SHORT_0_MIRRORSHADOW_WP2';
    FEvalChannels[12].FullName:= 'FULL_0_MIRRORSHADOW_WP2';
    FEvalChannels[12].DirColor:= + 1;

    FEvalChannels[13].Number:= 11;     // 0 ���
    FEvalChannels[13].ScanChNum:= 11;
    FEvalChannels[13].Method:= imEcho;
    FEvalChannels[13].Zone:= izAll;
    FEvalChannels[13].ShortName:= 'SHORT_0_ECHO_WP2';
    FEvalChannels[13].FullName:= 'FULL_0_ECHO_WP2';
    FEvalChannels[13].DirColor:= - 1;

    FEvalChannels[14].Number:= 14;     // 55 ���
    FEvalChannels[14].ScanChNum:= 14;
    FEvalChannels[14].Method:= imMirrorShadow;
    FEvalChannels[14].Zone:= izAll;
    FEvalChannels[14].ShortName:= 'SHORT_55_MIRRORSHADOW_WP1';
    FEvalChannels[14].FullName:= 'FULL_55_MIRRORSHADOW_WP1';
    FEvalChannels[14].DirColor:= + 1;

    FEvalChannels[15].Number:= 15;     // 55 ���
    FEvalChannels[15].ScanChNum:= 15;
    FEvalChannels[15].Method:= imMirrorShadow;
    FEvalChannels[15].Zone:= izAll;
    FEvalChannels[15].ShortName:= 'SHORT_55_MIRRORSHADOW_WP2';
    FEvalChannels[15].FullName:= 'FULL_55_MIRRORSHADOW_WP2';
    FEvalChannels[15].DirColor:= - 1;

  end
  else

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

  if CompareMem(@Temp, @Avicon11Scheme0, 8) then
  begin
    FDeviceID:= Avicon11Scheme0; // ��� ������������
    FDeviceVer:= 3;
    FDeviceName:= 'Avicon11Scheme0';

    FMinRail:= rLeft;
    FMaxRail:= rRight;


    // ------------ ���� ������ �-��������� ------------------


    SetLength(FBScanLines, 8);

    FBScanLines[0].R:= rLeft;
    FBScanLines[0].Use:= True;
    SetLength(FBScanLines[0].Items, 2);
    FBScanLines[0].Items[0].ScanChIdx:= 1;
//    FBScanLines[0].Items[0].Order:= 1;
    FBScanLines[0].Items[1].ScanChIdx:= 2;
//    FBScanLines[0].Items[1].Order:= 1;

    FBScanLines[1].R:= rLeft;
    FBScanLines[1].Use:= True;
    SetLength(FBScanLines[1].Items, 2);
    FBScanLines[1].Items[0].ScanChIdx:= 3;
//    FBScanLines[1].Items[0].Order:= 1;
    FBScanLines[1].Items[1].ScanChIdx:= 4;
//    FBScanLines[1].Items[1].Order:= 1;

    FBScanLines[2].R:= rLeft;
    FBScanLines[2].Use:= True;
    SetLength(FBScanLines[2].Items, 2);
    FBScanLines[2].Items[0].ScanChIdx:= 5;
//    FBScanLines[2].Items[0].Order:= 1;
    FBScanLines[2].Items[1].ScanChIdx:= 6;
//    FBScanLines[2].Items[1].Order:= 1;

    FBScanLines[3].R:= rLeft;
    FBScanLines[3].Use:= True;
    SetLength(FBScanLines[3].Items, 1);
    FBScanLines[3].Items[0].ScanChIdx:= 0;
//    FBScanLines[3].Items[0].Order:= 1;

  //////////////////////////////////////////////////

    FBScanLines[4].R:= rRight;
    FBScanLines[4].Use:= True;
    SetLength(FBScanLines[4].Items, 2);
    FBScanLines[4].Items[0].ScanChIdx:= 1;
//    FBScanLines[4].Items[0].Order:= 1;
    FBScanLines[4].Items[1].ScanChIdx:= 2;
//    FBScanLines[4].Items[1].Order:= 1;

    FBScanLines[5].R:= rRight;
    FBScanLines[5].Use:= True;
    SetLength(FBScanLines[5].Items, 2);
    FBScanLines[5].Items[0].ScanChIdx:= 3;
//    FBScanLines[5].Items[0].Order:= 1;
    FBScanLines[5].Items[1].ScanChIdx:= 4;
//    FBScanLines[5].Items[1].Order:= 1;

    FBScanLines[6].R:= rRight;
    FBScanLines[6].Use:= True;
    SetLength(FBScanLines[6].Items, 2);
    FBScanLines[6].Items[0].ScanChIdx:= 5;
//    FBScanLines[6].Items[0].Order:= 1;
    FBScanLines[6].Items[1].ScanChIdx:= 6;
//    FBScanLines[6].Items[1].Order:= 1;

    FBScanLines[7].R:= rRight;
    FBScanLines[7].Use:= True;
    SetLength(FBscanLines[7].Items, 1);
    FBScanLines[7].Items[0].ScanChIdx:= 0;
//    FBScanLines[7].Items[0].Order:= 1;

    // ------------ ������ ������� �������� ------------

    AddHandScanChannels(FHandChannels);
{
    SetLength(FHandChannels, 6);
    FHandChannels[0].Number:= 0;
    FHandChannels[0].BScanGateMin:= 3;
    FHandChannels[0].BScanGateMax:= 70;
    FHandChannels[0].DelayMultiply:= 3;
    FHandChannels[0].Method:= imEcho;
    FHandChannels[0].Angle:= 0;

    FHandChannels[1].Number:= 1;
    FHandChannels[1].BScanGateMin:= 6;
    FHandChannels[1].BScanGateMax:= 175;
    FHandChannels[1].DelayMultiply:= 1;
    FHandChannels[1].Method:= imEcho;
    FHandChannels[1].Angle:= 45;

    FHandChannels[2].Number:= 2;
    FHandChannels[2].BScanGateMin:= 6;
    FHandChannels[2].BScanGateMax:= 175;
    FHandChannels[2].DelayMultiply:= 1;
    FHandChannels[2].Method:= imEcho;
    FHandChannels[2].Angle:= 50;

    FHandChannels[3].Number:= 3;
    FHandChannels[3].BScanGateMin:= 6;
    FHandChannels[3].BScanGateMax:= 175;
    FHandChannels[3].DelayMultiply:= 1;
    FHandChannels[3].Method:= imEcho;
    FHandChannels[3].Angle:= 58;

    FHandChannels[4].Number:= 4;
    FHandChannels[4].BScanGateMin:= 3;
    FHandChannels[4].BScanGateMax:= 90;
    FHandChannels[4].DelayMultiply:= 1;
    FHandChannels[4].Method:= imEcho;
    FHandChannels[4].Angle:= 65;

    FHandChannels[5].Number:= 5;
    FHandChannels[5].BScanGateMin:= 3;
    FHandChannels[5].BScanGateMax:= 90;
    FHandChannels[5].DelayMultiply:= 1;
    FHandChannels[5].Method:= imEcho;
    FHandChannels[5].Angle:= 70;
}
    // ------------ ������ �-��������� ------------

    SetLength(FScanChannels, 7);
    for i:= 0 to 6 do FScanChannels[i].Gran:= _None;

    FScanChannels[0].Number:= 1;  //  0
    FScanChannels[0].EnterAngle:= 0;
    FScanChannels[0].TurnAngle:= 0;
    FScanChannels[0].Position:= 75;
    FScanChannels[0].BScanGateMin:= 5;
    FScanChannels[0].BScanGateMax:= 70;
    FScanChannels[0].DelayMultiply:= 3;
    FScanChannels[0].Delta:= 0;

    FScanChannels[1].Number:= 2;  // 58 ����������
    FScanChannels[1].EnterAngle:= 58;
    FScanChannels[1].TurnAngle:= 34;
    FScanChannels[1].Position:= 25;
    FScanChannels[1].BScanGateMin:= 20;
    FScanChannels[1].BScanGateMax:= 180;
    FScanChannels[1].DelayMultiply:= 1;
    FScanChannels[1].Delta:= - 90;

    FScanChannels[2].Number:= 3;  // 58 ����������
    FScanChannels[2].EnterAngle:= - 58;
    FScanChannels[2].TurnAngle:= 34;
    FScanChannels[2].Position:= -75;
    FScanChannels[2].BScanGateMin:= 20;
    FScanChannels[2].BScanGateMax:= 180;
    FScanChannels[2].DelayMultiply:= 1;
    FScanChannels[2].Delta:= + 90;

    FScanChannels[3].Number:= 4;  // 70 ����������
    FScanChannels[3].EnterAngle:= 70;
    FScanChannels[3].TurnAngle:= 0;
    FScanChannels[3].Position:= 50;
    FScanChannels[3].BScanGateMin:= 5;
    FScanChannels[3].BScanGateMax:= 95;
    FScanChannels[3].DelayMultiply:= 1;
    FScanChannels[3].Delta:= - 80;

    FScanChannels[4].Number:= 5;  // 58 ����������  - ����
    FScanChannels[4].EnterAngle:= - 50;
    FScanChannels[4].TurnAngle:= 34;
    FScanChannels[4].Position:= - 25;
    FScanChannels[4].BScanGateMin:= 5;
    FScanChannels[4].BScanGateMax:= 135;
    FScanChannels[4].DelayMultiply:= 1;
    FScanChannels[4].Delta:= + 90;

    FScanChannels[5].Number:= 6;  // 42 ����������
    FScanChannels[5].EnterAngle:= 42;
    FScanChannels[5].TurnAngle:= 0;
    FScanChannels[5].Position:= -50;
    FScanChannels[5].BScanGateMin:= 10;
    FScanChannels[5].BScanGateMax:= 163;
    FScanChannels[5].DelayMultiply:= 1;
    FScanChannels[5].Delta:= - 30;

    FScanChannels[6].Number:= 7;  // 42 ����������
    FScanChannels[6].EnterAngle:= - 42;
    FScanChannels[6].TurnAngle:= 0;
    FScanChannels[6].Position:= -50;
    FScanChannels[6].BScanGateMin:= 10;
    FScanChannels[6].BScanGateMax:= 163;
    FScanChannels[6].DelayMultiply:= 1;
    FScanChannels[6].Delta:= + 30;

    for I:= 0 to High(FScanChannels) do
      FScanChannels[I].BScanDuration:= FScanChannels[I].BScanGateMax - FScanChannels[I].BScanGateMin + 1;

    // ------------  ������ ������ ------------

    SetLength(FEvalChannels, 10);

    FEvalChannels[0].Number:= 0;     // 0 ���
    FEvalChannels[0].ScanChNum:= 1;
    FEvalChannels[0].Method:= imMirrorShadow;
    FEvalChannels[0].ShortName:= 'SHORT_0_MIRRORSHADOW';
    FEvalChannels[0].FullName:= 'FULL_0_MIRRORSHADOW';
    FEvalChannels[0].DirColor:= 1;
//    FEvalChannels[0].InfoSide:= isLeft;
//    FEvalChannels[0].InfoPos:= ipCentre;

    FEvalChannels[1].Number:= 1;     // 0 ���
    FEvalChannels[1].ScanChNum:= 1;
    FEvalChannels[1].Method:= imEcho;
    FEvalChannels[1].ShortName:= 'SHORT_0_ECHO';
    FEvalChannels[1].FullName:= 'FULL_0_ECHO';
    FEvalChannels[1].DirColor:= - 1;
//    FEvalChannels[1].InfoSide:= isRight;
//    FEvalChannels[1].InfoPos:= ipCentre;

    FEvalChannels[2].Number:= 2;     // 58 ����������
    FEvalChannels[2].ScanChNum:= 2;
    FEvalChannels[2].Method:= imEcho;
    FEvalChannels[2].ShortName:= 'SHORT_-58_MIRROR_WORK';
    FEvalChannels[2].FullName:= 'FULL_-58_MIRROR_WORK';
    FEvalChannels[2].DirColor:= 1;
//    FEvalChannels[2].InfoSide:= isLeft;
//    FEvalChannels[2].InfoPos:= ipLeft;

    FEvalChannels[3].Number:= 3;     // 58 ����������
    FEvalChannels[3].ScanChNum:= 3;
    FEvalChannels[3].Method:= imEcho;
    FEvalChannels[3].ShortName:= 'SHORT_-58_ECHO_WORK';
    FEvalChannels[3].FullName:= 'FULL_-58_ECHO_WORK';
    FEvalChannels[3].DirColor:= - 1;
//    FEvalChannels[3].InfoSide:= isRight;
//    FEvalChannels[3].InfoPos:= ipRight;

    FEvalChannels[4].Number:= 4;     // 70 ����������
    FEvalChannels[4].ScanChNum:= 4;
    FEvalChannels[4].Method:= imEcho;
    FEvalChannels[4].ShortName:= 'SHORT_+70_ECHO';
    FEvalChannels[4].FullName:= 'FULL_+70_ECHO';
    FEvalChannels[4].DirColor:= 1;
//    FEvalChannels[4].InfoSide:= isLeft;
//    FEvalChannels[4].InfoPos:= ipCentre;

    FEvalChannels[5].Number:= 5;     // 58 ����������
    FEvalChannels[5].ScanChNum:= 5;
    FEvalChannels[5].Method:= imMirror;
    FEvalChannels[5].ShortName:= 'SHORT_-70_ECHO';
    FEvalChannels[5].FullName:= 'FULL_-70_ECHO';
    FEvalChannels[5].DirColor:= - 1;
//    FEvalChannels[5].InfoSide:= isRight;
//    FEvalChannels[5].InfoPos:= ipCentre;

    FEvalChannels[6].Number:= 6;      // 42 ����������
    FEvalChannels[6].ScanChNum:= 6;
    FEvalChannels[6].Method:= imEcho;
    FEvalChannels[6].ShortName:= 'SHORT_+42_ECHO_NEAR';
    FEvalChannels[6].FullName:= 'FULL_+42_ECHO_NEAR';
    FEvalChannels[6].DirColor:= 1;
//    FEvalChannels[6].InfoSide:= isLeft;
//    FEvalChannels[6].InfoPos:= ipRight;

    FEvalChannels[7].Number:= 7;      // 42 ����������
    FEvalChannels[7].ScanChNum:= 7;
    FEvalChannels[7].Method:= imEcho;
    FEvalChannels[7].ShortName:= 'SHORT_-42_ECHO_NEAR';
    FEvalChannels[7].FullName:= 'FULL_-42_ECHO_NEAR';
    FEvalChannels[7].DirColor:= - 1;
//    FEvalChannels[7].InfoSide:= isRight;
  //  FEvalChannels[7].InfoPos:= ipRight;

    FEvalChannels[8].Number:= 8;      // 42 ����������
    FEvalChannels[8].ScanChNum:= 6;
    FEvalChannels[8].Method:= imEcho;
    FEvalChannels[8].ShortName:= 'SHORT_+42_ECHO_FAR';
    FEvalChannels[8].FullName:= 'FULL_+42_ECHO_FAR';
    FEvalChannels[8].DirColor:= 1;
//    FEvalChannels[8].InfoSide:= isLeft;
//    FEvalChannels[8].InfoPos:= ipLeft;

    FEvalChannels[9].Number:= 9;      // 42 ����������
    FEvalChannels[9].ScanChNum:= 7;
    FEvalChannels[9].Method:= imEcho;
    FEvalChannels[9].ShortName:= 'SHORT_-42_ECHO_FAR';
    FEvalChannels[9].FullName:= 'FULL_-42_ECHO_FAR';
    FEvalChannels[9].DirColor:= - 1;
//    FEvalChannels[9].InfoSide:= isRight;
//    FEvalChannels[9].InfoPos:= ipLeft;
  end
  else


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

  if CompareMem(@Temp, @Avicon15Scheme1, 8) and ((DeviceVer = - 1) or (DeviceVer = 1)) then
  begin
    FDeviceID:= Avicon15Scheme1; // ��� ������������
    FDeviceVer:= 1;
    FDeviceName:= 'Avicon15Scheme1';

    FMinRail:= rLeft;
    FMaxRail:= rLeft;

    // -------------------------------------------------------

    ChannelIDList[rLeft, 0]:= MIRRORSHADOW_0;
    ChannelIDList[rLeft, 1]:= ECHO_0;
    ChannelIDList[rLeft, 2]:= FORWARD_BOTH_ECHO_58;
    ChannelIDList[rLeft, 3]:= BACKWARD_BOTH_ECHO_58;
    ChannelIDList[rLeft, 4]:= FORWARD_ECHO_70;
    ChannelIDList[rLeft, 5]:= BACKWARD_ECHO_70;
    ChannelIDList[rLeft, 6]:= FORWARD_BOTH_MIRROR_58;
    ChannelIDList[rLeft, 7]:= FORWARD_ECHO_42_WEB;
    ChannelIDList[rLeft, 8]:= BACKWARD_ECHO_42_WEB;
    ChannelIDList[rLeft, 9]:= FORWARD_ECHO_42_BASE;
    ChannelIDList[rLeft, 10]:= BACKWARD_ECHO_42_BASE;

    ChannelIDList[rRight, 0]:= MIRRORSHADOW_0;
    ChannelIDList[rRight, 1]:= ECHO_0;
    ChannelIDList[rRight, 2]:= FORWARD_BOTH_ECHO_58;
    ChannelIDList[rRight, 3]:= BACKWARD_BOTH_ECHO_58;
    ChannelIDList[rRight, 4]:= FORWARD_ECHO_70;
    ChannelIDList[rRight, 5]:= BACKWARD_ECHO_70;
    ChannelIDList[rRight, 6]:= FORWARD_BOTH_MIRROR_58;
    ChannelIDList[rRight, 7]:= FORWARD_ECHO_42_WEB;
    ChannelIDList[rRight, 8]:= BACKWARD_ECHO_42_WEB;
    ChannelIDList[rRight, 9]:= FORWARD_ECHO_42_BASE;
    ChannelIDList[rRight, 10]:= BACKWARD_ECHO_42_BASE;

  // ------------------------------------------------------------------------------

    with EvalChannels2[FORWARD_BOTH_ECHO_58] do   begin InfoSide:= isLeft;  InfoPos:= ipCentre; end;
    with EvalChannels2[BACKWARD_BOTH_ECHO_58] do  begin InfoSide:= isRight; InfoPos:= ipLeft; end;
    with EvalChannels2[FORWARD_BOTH_MIRROR_58] do  begin InfoSide:= isRight; InfoPos:= ipRight; end;

    with EvalChannels2[FORWARD_ECHO_70] do        begin InfoSide:= isLeft;  InfoPos:= ipCentre; end;
    with EvalChannels2[BACKWARD_ECHO_70] do       begin InfoSide:= isRight; InfoPos:= ipCentre; end;

    with EvalChannels2[FORWARD_ECHO_42_WEB] do    begin InfoSide:= isLeft;  InfoPos:= ipRight;  end;
    with EvalChannels2[FORWARD_ECHO_42_BASE] do   begin InfoSide:= isLeft;  InfoPos:= ipLeft;   end;
    with EvalChannels2[BACKWARD_ECHO_42_WEB] do   begin InfoSide:= isRight; InfoPos:= ipRight;  end;
    with EvalChannels2[BACKWARD_ECHO_42_BASE] do  begin InfoSide:= isRight; InfoPos:= ipLeft;   end;

    with EvalChannels2[ECHO_0] do                 begin InfoSide:= isRight;  InfoPos:= ipCentre; end;
    with EvalChannels2[MIRRORSHADOW_0] do         begin InfoSide:= isLeft; InfoPos:= ipCentre; end;


    // ------------ ���� ������ �-��������� ------------------

    SetLength(FBScanLines, 4);

    FBScanLines[0].R:= rLeft;
    FBScanLines[0].Use:= True;
    SetLength(FBScanLines[0].Items, 3);
    FBScanLines[0].Items[0].ScanChIdx:= 2;
    FBScanLines[0].Items[1].ScanChIdx:= 3;
    FBScanLines[0].Items[2].ScanChIdx:= 4;

    FBScanLines[1].R:= rLeft;
    FBScanLines[1].Use:= True;
    SetLength(FBScanLines[1].Items, 2);
    FBScanLines[1].Items[0].ScanChIdx:= 7;
    FBScanLines[1].Items[1].ScanChIdx:= 6;

    FBScanLines[2].R:= rLeft;
    FBScanLines[2].Use:= True;
    SetLength(FBScanLines[2].Items, 2);
    FBScanLines[2].Items[0].ScanChIdx:= 0;
    FBScanLines[2].Items[1].ScanChIdx:= 1;

    FBScanLines[3].R:= rLeft;
    FBScanLines[3].Use:= True;
    SetLength(FBScanLines[3].Items, 1);
    FBScanLines[3].Items[0].ScanChIdx:= 5;

  //////////////////////////////////////////////////

    // ------------ ������ ������� �������� ------------

    AddHandScanChannels(FHandChannels);

    // ------------ ������ �-��������� ------------

    SetLength(FScanChannels, 8);
    for i:= 0 to 7 do FScanChannels[i].Gran:= _None;

    FScanChannels[0].Number:= 0;  // 42 ���� ����������
    FScanChannels[0].EnterAngle:= 42;
    FScanChannels[0].TurnAngle:= 0;
    FScanChannels[0].Position:= - 50;
    FScanChannels[0].BScanGateMin:= 5 {10};
    FScanChannels[0].BScanGateMax:= 175 {163};
    FScanChannels[0].DelayMultiply:= 1;
    FScanChannels[0].Gran:= _None;

    FScanChannels[1].Number:= 1;  // 42 ���� ����������
    FScanChannels[1].EnterAngle:= - 42;
    FScanChannels[1].TurnAngle:= 0;
    FScanChannels[1].Position:= - 50;
    FScanChannels[1].BScanGateMin:= 5 {10};
    FScanChannels[1].BScanGateMax:= 175 {163};
    FScanChannels[1].DelayMultiply:= 1;
    FScanChannels[1].Gran:= _None;

    FScanChannels[2].Number:= 2;  // 58 ���� ����������
    FScanChannels[2].EnterAngle:= 58;
    FScanChannels[2].TurnAngle:= 34;
    FScanChannels[2].Position:= - 25;
    FScanChannels[2].BScanGateMin:= 9 {20};
    FScanChannels[2].BScanGateMax:= 180;
    FScanChannels[2].DelayMultiply:= 1;
    FScanChannels[2].Gran:= _None;

    FScanChannels[3].Number:= 3;  // 58 ���� ����������
    FScanChannels[3].EnterAngle:= - 58;
    FScanChannels[3].TurnAngle:= 34;
    FScanChannels[3].Position:= - 75;
    FScanChannels[3].BScanGateMin:= 9 {20};
    FScanChannels[3].BScanGateMax:= 180;
    FScanChannels[3].DelayMultiply:= 1;
    FScanChannels[3].Gran:= _None;

    FScanChannels[4].Number:= 4;  // 58 ���� ���������� ���
    FScanChannels[4].EnterAngle:= - 58;
    FScanChannels[4].TurnAngle:= 34;
    FScanChannels[4].Position:= - 25;
    FScanChannels[4].BScanGateMin:= 9 {20};
    FScanChannels[4].BScanGateMax:= 180;
    FScanChannels[4].DelayMultiply:= 1;
    FScanChannels[4].Gran:= _None;

    FScanChannels[5].Number:= 5;  //  0 ����
    FScanChannels[5].EnterAngle:= 0;
    FScanChannels[5].TurnAngle:= 0;
    FScanChannels[5].Position:= 75;
    FScanChannels[5].BScanGateMin:= 1 {5};
    FScanChannels[5].BScanGateMax:= 70;
    FScanChannels[5].DelayMultiply:= 3;
    FScanChannels[5].Gran:= _None;

    FScanChannels[6].Number:= 6;  // 70 ���� ����������
    FScanChannels[6].EnterAngle:= - 70;
    FScanChannels[6].TurnAngle:= 0;
    FScanChannels[6].Position:= 25;
    FScanChannels[6].BScanGateMin:= 1 {5};
    FScanChannels[6].BScanGateMax:= 100 {135};
    FScanChannels[6].DelayMultiply:= 1;
    FScanChannels[6].Gran:= _None;

    FScanChannels[7].Number:= 7;  // 70 ���� ����������
    FScanChannels[7].EnterAngle:= 70;
    FScanChannels[7].TurnAngle:= 0;
    FScanChannels[7].Position:= 50;
    FScanChannels[7].BScanGateMin:= 1 {5};
    FScanChannels[7].BScanGateMax:= 100 {135};
    FScanChannels[7].DelayMultiply:= 1;
    FScanChannels[7].Gran:= _None;

(*
    FScanChannels[6].Number:= 7;  // 70 ���� ����������
    FScanChannels[6].EnterAngle:= - 70;
    FScanChannels[6].TurnAngle:= 0;
    FScanChannels[6].Position:= 50;
    FScanChannels[6].BScanGateMin:= 5;
    FScanChannels[6].BScanGateMax:= 135;
    FScanChannels[6].DelayMultiply:= 1;
    FScanChannels[6].Gran:= _None;

    FScanChannels[7].Number:= 6;  // 70 ���� ����������
    FScanChannels[7].EnterAngle:= 70;
    FScanChannels[7].TurnAngle:= 0;
    FScanChannels[7].Position:= 25;
    FScanChannels[7].BScanGateMin:= 5;
    FScanChannels[7].BScanGateMax:= 135;
    FScanChannels[7].DelayMultiply:= 1;
    FScanChannels[7].Gran:= _None;
 *)
    for I:= 0 to High(FScanChannels) do
      FScanChannels[I].BScanDuration:= FScanChannels[I].BScanGateMax - FScanChannels[I].BScanGateMin + 1;

    // ------------  ������ ������ ------------

    SetLength(FEvalChannels, 11);

    FEvalChannels[0].Number:= 0;     // 0 ���� ���
    FEvalChannels[0].ScanChNum:= 5;
    FEvalChannels[0].Method:= imMirrorShadow;
    FEvalChannels[0].ShortName:= 'SHORT_0_MIRRORSHADOW';
    FEvalChannels[0].FullName:= 'FULL_0_MIRRORSHADOW';
    FEvalChannels[0].Zone:= izAll;
    FEvalChannels[0].DirColor:= + 1;

    FEvalChannels[1].Number:= 1;     // 0 ���� ���
    FEvalChannels[1].ScanChNum:= 5;
    FEvalChannels[1].Method:= imEcho;
    FEvalChannels[1].ShortName:= 'SHORT_0_ECHO';
    FEvalChannels[1].FullName:= 'FULL_0_ECHO';
    FEvalChannels[1].Zone:= izAll;
    FEvalChannels[1].DirColor:= - 1;

    FEvalChannels[2].Number:= 2;     // 58 ���� ����������
    FEvalChannels[2].ScanChNum:= 2;
    FEvalChannels[2].Method:= imEcho;
    FEvalChannels[2].ShortName:= ''; // 'SHORT_-58_MIRROR_WORK';
    FEvalChannels[2].FullName:= ''; // 'FULL_-58_MIRROR_WORK';
    FEvalChannels[2].Zone:= izHead;
    FEvalChannels[2].DirColor:= + 1;

    FEvalChannels[3].Number:= 3;     // 58 ���� ����������
    FEvalChannels[3].ScanChNum:= 3;
    FEvalChannels[3].Method:= imEcho;
    FEvalChannels[3].ShortName:= ''; // 'SHORT_-58_ECHO_WORK';
    FEvalChannels[3].FullName:= ''; // 'FULL_-58_ECHO_WORK';
    FEvalChannels[3].Zone:= izHead;
    FEvalChannels[3].DirColor:= - 1;

    FEvalChannels[4].Number:= 4;     // 70 ���� ����������
    FEvalChannels[4].ScanChNum:= 7;
    FEvalChannels[4].Method:= imEcho;
    FEvalChannels[4].ShortName:= 'SHORT_+70_ECHO';
    FEvalChannels[4].FullName:= 'FULL_+70_ECHO';
    FEvalChannels[4].Zone:= izHead;
    FEvalChannels[4].DirColor:= + 1;

    FEvalChannels[5].Number:= 5;     // 70 ���� ����������
    FEvalChannels[5].ScanChNum:= 6;
    FEvalChannels[5].Method:= imEcho;
    FEvalChannels[5].ShortName:= 'SHORT_-70_ECHO';
    FEvalChannels[5].FullName:= 'FULL_-70_ECHO';
    FEvalChannels[5].Zone:= izHead;
    FEvalChannels[5].DirColor:= - 1;

    FEvalChannels[6].Number:= 6;     // 58 ���� ����������
    FEvalChannels[6].ScanChNum:= 4;
    FEvalChannels[6].Method:= imMirror;
    FEvalChannels[6].ShortName:= ''; // 'SHORT_-70_ECHO';
    FEvalChannels[6].FullName:= ''; // 'FULL_-70_ECHO';
    FEvalChannels[6].Zone:= izHead;
    FEvalChannels[6].DirColor:= - 1;

    FEvalChannels[7].Number:= 7;      // 42 ���� ���������� �����
    FEvalChannels[7].ScanChNum:= 0;
    FEvalChannels[7].Method:= imEcho;
    FEvalChannels[7].ShortName:= 'SHORT_+42_ECHO_NEAR';
    FEvalChannels[7].FullName:= 'FULL_+42_ECHO_NEAR';
    FEvalChannels[7].Zone:= izWeb;
    FEvalChannels[7].DirColor:= + 1;

    FEvalChannels[8].Number:= 8;      // 42 ���� ���������� �����
    FEvalChannels[8].ScanChNum:= 1;
    FEvalChannels[8].Method:= imEcho;
    FEvalChannels[8].ShortName:= 'SHORT_-42_ECHO_NEAR';
    FEvalChannels[8].FullName:= 'FULL_-42_ECHO_NEAR';
    FEvalChannels[8].Zone:= izWeb;
    FEvalChannels[8].DirColor:= - 1;

    FEvalChannels[9].Number:= 9;      // 42 ���� ���������� �������
    FEvalChannels[9].ScanChNum:= 0;
    FEvalChannels[9].Method:= imEcho;
    FEvalChannels[9].ShortName:= 'SHORT_+42_ECHO_FAR';
    FEvalChannels[9].FullName:= 'FULL_+42_ECHO_FAR';
    FEvalChannels[9].Zone:= izBase;
    FEvalChannels[9].DirColor:= + 1;

    FEvalChannels[10].Number:= 10;      // 42 ���� ���������� �������
    FEvalChannels[10].ScanChNum:= 1;
    FEvalChannels[10].Method:= imEcho;
    FEvalChannels[10].ShortName:= 'SHORT_-42_ECHO_FAR';
    FEvalChannels[10].FullName:= 'FULL_-42_ECHO_FAR';
    FEvalChannels[10].Zone:= izBase;
    FEvalChannels[10].DirColor:= - 1;
  end
  else

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

  if CompareMem(@Temp, @Avicon15Scheme1, 8) and (DeviceVer = 2) then
  begin
    FDeviceID:= Avicon15Scheme1; // ��� ������������
    FDeviceVer:= 2;
    FDeviceName:= 'Avicon15Scheme1';

    FMinRail:= rLeft;
    FMaxRail:= rLeft;

    // -------------------------------------------------------

    ChannelIDList[rLeft, 0]:=  MIRRORSHADOW_0;
    ChannelIDList[rLeft, 1]:=  ECHO_0;
    ChannelIDList[rLeft, 2]:=  FORWARD_BOTH_ECHO_58;
    ChannelIDList[rLeft, 3]:=  BACKWARD_BOTH_ECHO_58;
    ChannelIDList[rLeft, 4]:=  FORWARD_ECHO_70;
    ChannelIDList[rLeft, 5]:=  BACKWARD_ECHO_70;
    ChannelIDList[rLeft, 6]:=  FORWARD_ECHO_42_WEB;
    ChannelIDList[rLeft, 7]:=  BACKWARD_ECHO_42_WEB;
    ChannelIDList[rLeft, 8]:=  FORWARD_ECHO_42_BASE;
    ChannelIDList[rLeft, 9]:=  BACKWARD_ECHO_42_BASE;
    ChannelIDList[rLeft, 10]:= BACKWARD_BOTH_MIRROR_58;

    ChannelIDList[rRight, 0]:=  MIRRORSHADOW_0;
    ChannelIDList[rRight, 1]:=  ECHO_0;
    ChannelIDList[rRight, 2]:=  FORWARD_BOTH_ECHO_58;
    ChannelIDList[rRight, 3]:=  BACKWARD_BOTH_ECHO_58;
    ChannelIDList[rRight, 4]:=  FORWARD_ECHO_70;
    ChannelIDList[rRight, 5]:=  BACKWARD_ECHO_70;
    ChannelIDList[rRight, 6]:=  FORWARD_ECHO_42_WEB;
    ChannelIDList[rRight, 7]:=  BACKWARD_ECHO_42_WEB;
    ChannelIDList[rRight, 8]:=  FORWARD_ECHO_42_BASE;
    ChannelIDList[rRight, 9]:=  BACKWARD_ECHO_42_BASE;
    ChannelIDList[rRight, 10]:= BACKWARD_BOTH_MIRROR_58;

  // ------------------------------------------------------------------------------

    with EvalChannels2[FORWARD_BOTH_ECHO_58] do   begin InfoSide:= isLeft;  InfoPos:= ipCentre; end;
    with EvalChannels2[BACKWARD_BOTH_ECHO_58] do  begin InfoSide:= isRight; InfoPos:= ipLeft; end;
    with EvalChannels2[BACKWARD_BOTH_MIRROR_58] do  begin InfoSide:= isRight; InfoPos:= ipRight; end;

    with EvalChannels2[FORWARD_ECHO_70] do        begin InfoSide:= isLeft;  InfoPos:= ipCentre; end;
    with EvalChannels2[BACKWARD_ECHO_70] do       begin InfoSide:= isRight; InfoPos:= ipCentre; end;

    with EvalChannels2[FORWARD_ECHO_42_WEB] do    begin InfoSide:= isLeft;  InfoPos:= ipRight;  end;
    with EvalChannels2[FORWARD_ECHO_42_BASE] do   begin InfoSide:= isLeft;  InfoPos:= ipLeft;   end;
    with EvalChannels2[BACKWARD_ECHO_42_WEB] do   begin InfoSide:= isRight; InfoPos:= ipRight;  end;
    with EvalChannels2[BACKWARD_ECHO_42_BASE] do  begin InfoSide:= isRight; InfoPos:= ipLeft;   end;

    with EvalChannels2[ECHO_0] do                 begin InfoSide:= isRight;  InfoPos:= ipCentre; end;
    with EvalChannels2[MIRRORSHADOW_0] do         begin InfoSide:= isLeft; InfoPos:= ipCentre; end;


    // ------------ ���� ������ �-��������� ------------------

    SetLength(FBScanLines, 4);

    FBScanLines[0].R:= rLeft;
    FBScanLines[0].Use:= True;
    SetLength(FBScanLines[0].Items, 3);
    FBScanLines[0].Items[0].ScanChIdx:= 3;
    FBScanLines[0].Items[1].ScanChIdx:= 2;
    FBScanLines[0].Items[2].ScanChIdx:= 4;

    FBScanLines[1].R:= rLeft;
    FBScanLines[1].Use:= True;
    SetLength(FBScanLines[1].Items, 2);
    FBScanLines[1].Items[0].ScanChIdx:= 7;
    FBScanLines[1].Items[1].ScanChIdx:= 6;

    FBScanLines[2].R:= rLeft;
    FBScanLines[2].Use:= True;
    SetLength(FBScanLines[2].Items, 2);
    FBScanLines[2].Items[0].ScanChIdx:= 1;
    FBScanLines[2].Items[1].ScanChIdx:= 0;

    FBScanLines[3].R:= rLeft;
    FBScanLines[3].Use:= True;
    SetLength(FBScanLines[3].Items, 1);
    FBScanLines[3].Items[0].ScanChIdx:= 5;

  //////////////////////////////////////////////////

    // ------------ ������ ������� �������� ------------

    AddHandScanChannels(FHandChannels);

    // ------------ ������ �-��������� ------------

    SetLength(FScanChannels, 8);
    for i:= 0 to 7 do FScanChannels[i].Gran:= _None;

    FScanChannels[0].Number:= 0;  // 42 ���� ����������
    FScanChannels[0].EnterAngle:= - 42;
    FScanChannels[0].TurnAngle:= 0;
    FScanChannels[0].Position:= - 50;
    FScanChannels[0].BScanGateMin:= 10;
    FScanChannels[0].BScanGateMax:= 163;
    FScanChannels[0].DelayMultiply:= 1;
    FScanChannels[0].Gran:= _None;

    FScanChannels[1].Number:= 1;  // 42 ���� ����������
    FScanChannels[1].EnterAngle:= + 42;
    FScanChannels[1].TurnAngle:= 0;
    FScanChannels[1].Position:= - 50;
    FScanChannels[1].BScanGateMin:= 10;
    FScanChannels[1].BScanGateMax:= 163;
    FScanChannels[1].DelayMultiply:= 1;
    FScanChannels[1].Gran:= _None;

    FScanChannels[2].Number:= 2;  // 58 ���� �����������
    FScanChannels[2].EnterAngle:= - 58;
    FScanChannels[2].TurnAngle:= 34;
    FScanChannels[2].Position:= - 25;
    FScanChannels[2].BScanGateMin:= 20;
    FScanChannels[2].BScanGateMax:= 180;
    FScanChannels[2].DelayMultiply:= 1;
    FScanChannels[2].Gran:= _None;

    FScanChannels[3].Number:= 3;  // 58 ���� ����������
    FScanChannels[3].EnterAngle:= 58;
    FScanChannels[3].TurnAngle:= 34;
    FScanChannels[3].Position:= - 75;
    FScanChannels[3].BScanGateMin:= 20;
    FScanChannels[3].BScanGateMax:= 180;
    FScanChannels[3].DelayMultiply:= 1;
    FScanChannels[3].Gran:= _None;

    FScanChannels[4].Number:= 4;  // 58 ���� ���������� ���
    FScanChannels[4].EnterAngle:= - 58;
    FScanChannels[4].TurnAngle:= 34;
    FScanChannels[4].Position:= - 25;
    FScanChannels[4].BScanGateMin:= 20;
    FScanChannels[4].BScanGateMax:= 180;
    FScanChannels[4].DelayMultiply:= 1;
    FScanChannels[4].Gran:= _None;

    FScanChannels[5].Number:= 5;  //  0 ����
    FScanChannels[5].EnterAngle:= 0;
    FScanChannels[5].TurnAngle:= 0;
    FScanChannels[5].Position:= 75;
    FScanChannels[5].BScanGateMin:= 5;
    FScanChannels[5].BScanGateMax:= 70;
    FScanChannels[5].DelayMultiply:= 3;
    FScanChannels[5].Gran:= _None;

    FScanChannels[6].Number:= 6;  // 70 ���� ����������
    FScanChannels[6].EnterAngle:= - 70;
    FScanChannels[6].TurnAngle:= 0;
    FScanChannels[6].Position:= 25;
    FScanChannels[6].BScanGateMin:= 5;
    FScanChannels[6].BScanGateMax:= 135;
    FScanChannels[6].DelayMultiply:= 1;
    FScanChannels[6].Gran:= _None;

    FScanChannels[7].Number:= 7;  // 70 ���� ����������
    FScanChannels[7].EnterAngle:= 70;
    FScanChannels[7].TurnAngle:= 0;
    FScanChannels[7].Position:= 50;
    FScanChannels[7].BScanGateMin:= 5;
    FScanChannels[7].BScanGateMax:= 135;
    FScanChannels[7].DelayMultiply:= 1;
    FScanChannels[7].Gran:= _None;

    for I:= 0 to High(FScanChannels) do
      FScanChannels[I].BScanDuration:= FScanChannels[I].BScanGateMax - FScanChannels[I].BScanGateMin + 1;

    // ------------  ������ ������ ------------

    SetLength(FEvalChannels, 11);

    FEvalChannels[0].Number:= 0;     // 0 ���� ���
    FEvalChannels[0].ScanChNum:= 5;
    FEvalChannels[0].Method:= imMirrorShadow;
    FEvalChannels[0].ShortName:= 'SHORT_0_MIRRORSHADOW';
    FEvalChannels[0].FullName:= 'FULL_0_MIRRORSHADOW';
    FEvalChannels[0].Zone:= izAll;
    FEvalChannels[0].DirColor:= + 1;

    FEvalChannels[1].Number:= 1;     // 0 ���� ���
    FEvalChannels[1].ScanChNum:= 5;
    FEvalChannels[1].Method:= imEcho;
    FEvalChannels[1].ShortName:= 'SHORT_0_ECHO';
    FEvalChannels[1].FullName:= 'FULL_0_ECHO';
    FEvalChannels[1].Zone:= izAll;
    FEvalChannels[1].DirColor:= - 1;

    FEvalChannels[2].Number:= 2;     // 58 ���� ����������
    FEvalChannels[2].ScanChNum:= 3;
    FEvalChannels[2].Method:= imEcho;
    FEvalChannels[2].ShortName:= ''; // 'SHORT_-58_MIRROR_WORK';
    FEvalChannels[2].FullName:= ''; // 'FULL_-58_MIRROR_WORK';
    FEvalChannels[2].Zone:= izHead;
    FEvalChannels[2].DirColor:= + 1;

    FEvalChannels[3].Number:= 3;     // 58 ���� ����������
    FEvalChannels[3].ScanChNum:= 2;
    FEvalChannels[3].Method:= imEcho;
    FEvalChannels[3].ShortName:= ''; // 'SHORT_-58_ECHO_WORK';
    FEvalChannels[3].FullName:= ''; // 'FULL_-58_ECHO_WORK';
    FEvalChannels[3].Zone:= izHead;
    FEvalChannels[3].DirColor:= - 1;

    FEvalChannels[4].Number:= 4;     // 70 ���� ����������
    FEvalChannels[4].ScanChNum:= 7;
    FEvalChannels[4].Method:= imEcho;
    FEvalChannels[4].ShortName:= 'SHORT_+70_ECHO';
    FEvalChannels[4].FullName:= 'FULL_+70_ECHO';
    FEvalChannels[4].Zone:= izHead;
    FEvalChannels[4].DirColor:= + 1;

    FEvalChannels[5].Number:= 5;     // 70 ���� �����������
    FEvalChannels[5].ScanChNum:= 6;
    FEvalChannels[5].Method:= imEcho;
    FEvalChannels[5].ShortName:= 'SHORT_-70_ECHO';
    FEvalChannels[5].FullName:= 'FULL_-70_ECHO';
    FEvalChannels[5].Zone:= izHead;
    FEvalChannels[5].DirColor:= - 1;

    FEvalChannels[6].Number:= 6;      // 42 ���� ���������� �����
    FEvalChannels[6].ScanChNum:= 1;
    FEvalChannels[6].Method:= imEcho;
    FEvalChannels[6].ShortName:= 'SHORT_+42_ECHO_NEAR';
    FEvalChannels[6].FullName:= 'FULL_+42_ECHO_NEAR';
    FEvalChannels[6].Zone:= izWeb;
    FEvalChannels[6].DirColor:= + 1;

    FEvalChannels[7].Number:= 7;      // 42 ���� ���������� �����
    FEvalChannels[7].ScanChNum:= 0;
    FEvalChannels[7].Method:= imEcho;
    FEvalChannels[7].ShortName:= 'SHORT_-42_ECHO_NEAR';
    FEvalChannels[7].FullName:= 'FULL_-42_ECHO_NEAR';
    FEvalChannels[7].Zone:= izWeb;
    FEvalChannels[7].DirColor:= - 1;

    FEvalChannels[8].Number:= 8;      // 42 ���� ���������� �������
    FEvalChannels[8].ScanChNum:= 1;
    FEvalChannels[8].Method:= imEcho;
    FEvalChannels[8].ShortName:= 'SHORT_+42_ECHO_FAR';
    FEvalChannels[8].FullName:= 'FULL_+42_ECHO_FAR';
    FEvalChannels[8].Zone:= izBase;
    FEvalChannels[8].DirColor:= + 1;

    FEvalChannels[9].Number:= 9;      // 42 ���� ���������� �������
    FEvalChannels[9].ScanChNum:= 0;
    FEvalChannels[9].Method:= imEcho;
    FEvalChannels[9].ShortName:= 'SHORT_-42_ECHO_FAR';
    FEvalChannels[9].FullName:= 'FULL_-42_ECHO_FAR';
    FEvalChannels[9].Zone:= izBase;
    FEvalChannels[9].DirColor:= - 1;

    FEvalChannels[10].Number:= 10;     // 58 ���� ���������� ����
    FEvalChannels[10].ScanChNum:= 4;
    FEvalChannels[10].Method:= imMirror;
    FEvalChannels[10].ShortName:= ''; // 'SHORT_-70_ECHO';
    FEvalChannels[10].FullName:= ''; // 'FULL_-70_ECHO';
    FEvalChannels[10].Zone:= izHead;
    FEvalChannels[10].DirColor:= - 1;

  end
  else

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////


  if CompareMem(@Temp, @AviconHandScan, 8) then
  begin
    FDeviceID:= AviconHandScan;             // ��� ������������
    FDeviceVer:= 3;
    FMinRail:= rLeft;
    FMaxRail:= rLeft;
    FDeviceName:= 'HandScan';

    // ------------ ���� ������ �-��������� ------------------

    SetLength(FBScanLines, 1);

    I:= 0;
    FBScanLines[I].R:= rLeft;
    FBScanLines[I].Use:= True;
    SetLength(FBScanLines[I].Items, 1);
    FBScanLines[I].Items[0].ScanChIdx:= 0;


    // ------------ ������ ������� �������� ------------

    SetLength(FHandChannels, 0);

    // ------------ ������ �-��������� ------------

    SetLength(FScanChannels, 1);

    I := 0;
    FScanChannels[I].Number:= I;
    FScanChannels[I].EnterAngle:= 0;
    FScanChannels[I].TurnAngle:= 0;
    FScanChannels[I].Position:= 0;
    FScanChannels[I].BScanGateMin:= 0;
    FScanChannels[I].BScanGateMax:= 255;
    FScanChannels[I].DelayMultiply:= 1;

    for I:= 0 to High(FScanChannels) do
      FScanChannels[I].BScanDuration:= FScanChannels[I].BScanGateMax - FScanChannels[I].BScanGateMin + 1;

    // ------------  ������ ������ ------------

    SetLength(FEvalChannels, 1);

    I := 0;
    FEvalChannels[I].Number:= I;     // 0 ���� ���
    FEvalChannels[I].ScanChNum:= I;
  end
  (*
  if CompareMem(@Temp, @AviconHandScan, 8) then
  begin
    FDeviceID:= AviconHandScan;
    FDeviceVer:= 0;
    FDeviceName:= 'AviconHandScan';

    FMinRail:= rLeft;
    FMaxRail:= rLeft;

    SetLength(FScanChannels, 1);
    for i:= 0 to 0 do FScanChannels[i].Gran:= _None;

    FScanChannels[0].Number:= 0;  // ����� ����
  ( *
    ScanChannels[0].BScanGateMin
    ScanChannels[0].BScanGateMax
    ScanChannels[0].DataType
    ScanChannels[0].DelayMultiply
  * )
    SetLength(FEvalChannels, 12);

    FEvalChannels[0].Number:= 0;     // 0 ���
    FEvalChannels[0].ScanChNum:= 0;

    FEvalChannels[1].Number:= 1;     // 0 ���
    FEvalChannels[1].ScanChNum:= 0;

    FEvalChannels[2].Number:= 2;     // 58��
    FEvalChannels[2].ScanChNum:= 1;

    FEvalChannels[3].Number:= 3;     // 58��
    FEvalChannels[3].ScanChNum:= 2;

    FEvalChannels[4].Number:= 4;     // 70�
    FEvalChannels[4].ScanChNum:= 3;

    FEvalChannels[5].Number:= 5;     // 70�
    FEvalChannels[5].ScanChNum:= 4;

    FEvalChannels[6].Number:= 6;      // 42�
    FEvalChannels[6].ScanChNum:= 7;

    FEvalChannels[7].Number:= 7;      // 42�
    FEvalChannels[7].ScanChNum:= 8;

    FEvalChannels[8].Number:= 8;      // 42�
    FEvalChannels[8].ScanChNum:= 7;

    FEvalChannels[9].Number:= 9;      // 42�
    FEvalChannels[9].ScanChNum:= 8;

    FEvalChannels[10].Number:= 10;    // 58��
    FEvalChannels[10].ScanChNum:= 5;

    FEvalChannels[11].Number:= 11;    // 58��
    FEvalChannels[11].ScanChNum:= 6;

  { EvalChannels[0].Method
    EvalChannels[0].ShortName
    EvalChannels[0].FullName }
  end *)
  else

  if CompareMem(@Temp, @Avicon17, 8) then
  begin
    FDeviceID:= Avicon17;        // ��� ������������
    FDeviceVer:= 3;

    FMinRail:= rLeft;
    FMaxRail:= rRight;

    // ------------ ���� ������ �-��������� ------------------

    SetLength(FBScanLines, 0);

    // ------------ ������ ������� �������� ------------

    SetLength(FHandChannels, 0);

    // ------------ ������ �-��������� ------------

    SetLength(FScanChannels, 0);

    // ------------  ������ ������ ------------

    SetLength(FEvalChannels, 0);

  end
  else

  if CompareMem(@Temp, @MIGREG, 8) then
  begin
    FDeviceID:= MIGREG;             // ��� ������������
    FDeviceVer:= 2;
    FMinRail:= rLeft;
    FMaxRail:= rRight;
    FDeviceName:= 'MIG';

    // ------------ ���� ������ �-��������� ------------------

    SetLength(FBScanLines, 24);

    for I := 0 to 11 do
    begin
      FBScanLines[I].R:= rLeft;
      FBScanLines[I].Use:= True;
      SetLength(FBScanLines[I].Items, 1);
      FBScanLines[I].Items[0].ScanChIdx:= I;
//      FBScanLines[I].Items[0].Order:= 1;
    end;

  //////////////////////////////////////////////////

    for I := 12 to 23 do
    begin
      FBScanLines[I].R:= rRight;
      FBScanLines[I].Use:= True;
      SetLength(FBScanLines[I].Items, 1);
      FBScanLines[I].Items[0].ScanChIdx:= I - 12;
//      FBScanLines[I].Items[0].Order:= 1;
    end;

    // ------------ ������ ������� �������� ------------

    SetLength(FHandChannels, 0);

    // ------------ ������ �-��������� ------------

    SetLength(FScanChannels, 12);

    for I := 0 to 11 do
    begin
      FScanChannels[I].Number:= I;
      FScanChannels[I].EnterAngle:= 0;
      FScanChannels[I].TurnAngle:= 0;
      FScanChannels[I].Position:= 0;
      FScanChannels[I].BScanGateMin:= 0;
      FScanChannels[I].BScanGateMax:= 255;
      FScanChannels[I].DelayMultiply:= 1;
    end;

    for I:= 0 to High(FScanChannels) do
      FScanChannels[I].BScanDuration:= FScanChannels[I].BScanGateMax - FScanChannels[I].BScanGateMin + 1;

    // ------------  ������ ������ ------------

    SetLength(FEvalChannels, 12);

    for I := 0 to 11 do
    begin
      FEvalChannels[I].Number:= I;     // 0 ���� ���
      FEvalChannels[I].ScanChNum:= I;
    end;
  end
  else
  if CompareMem(@Temp, @MIGView, 8) then
  begin

    FDeviceID:= MIGView;             // ��� ������������
    FDeviceVer:= 2;
    FMinRail:= rLeft;
    FMaxRail:= rLeft;
    FDeviceName:= 'MIG View';


  // ------------------------------------------------------------------------------

    ChannelIDList[rLeft, 0]:= FORWARD_ECHO_70;
    ChannelIDList[rLeft, 1]:= BACKWARD_ECHO_70;
    ChannelIDList[rLeft, 2]:= FORWARD_ECHO_70;
    ChannelIDList[rLeft, 3]:= BACKWARD_ECHO_70;
    ChannelIDList[rLeft, 4]:= FORWARD_ECHO_70;
    ChannelIDList[rLeft, 5]:= BACKWARD_ECHO_70;
    ChannelIDList[rLeft, 6]:= FORWARD_ECHO_70;
    ChannelIDList[rLeft, 7]:= BACKWARD_ECHO_70;
    ChannelIDList[rLeft, 8]:= FORWARD_ECHO_70;
    ChannelIDList[rLeft, 9]:= BACKWARD_ECHO_70;
    ChannelIDList[rLeft, 10]:= FORWARD_ECHO_70;
    ChannelIDList[rLeft, 11]:= BACKWARD_ECHO_70;
    ChannelIDList[rLeft, 12]:= FORWARD_ECHO_70;
    ChannelIDList[rLeft, 13]:= BACKWARD_ECHO_70;
    ChannelIDList[rLeft, 14]:= FORWARD_ECHO_70;
    ChannelIDList[rLeft, 15]:= BACKWARD_ECHO_70;
    ChannelIDList[rLeft, 16]:= FORWARD_ECHO_70;
    ChannelIDList[rLeft, 17]:= BACKWARD_ECHO_70;
    ChannelIDList[rLeft, 18]:= FORWARD_ECHO_70;
    ChannelIDList[rLeft, 19]:= BACKWARD_ECHO_70;
    ChannelIDList[rLeft, 20]:= FORWARD_ECHO_70;
    ChannelIDList[rLeft, 21]:= BACKWARD_ECHO_70;
    ChannelIDList[rLeft, 22]:= FORWARD_ECHO_70;
    ChannelIDList[rLeft, 23]:= BACKWARD_ECHO_70;

  // ------------------------------------------------------------------------------

    with EvalChannels2[FORWARD_WORK_ECHO_58] do   begin InfoSide:= isLeft;  InfoPos:= ipLeft;   end;
    with EvalChannels2[FORWARD_UWORK_ECHO_58] do  begin InfoSide:= isLeft;  InfoPos:= ipRight;  end;

    with EvalChannels2[BACKWARD_WORK_ECHO_58] do  begin InfoSide:= isRight; InfoPos:= ipLeft;   end;
    with EvalChannels2[BACKWARD_UWORK_ECHO_58] do begin InfoSide:= isRight; InfoPos:= ipRight;  end;

    with EvalChannels2[FORWARD_ECHO_70] do        begin InfoSide:= isLeft;  InfoPos:= ipCentre; end;
    with EvalChannels2[BACKWARD_ECHO_70] do       begin InfoSide:= isRight; InfoPos:= ipCentre; end;

    with EvalChannels2[FORWARD_ECHO_42_WEB] do    begin InfoSide:= isLeft;  InfoPos:= ipRight;  end;
    with EvalChannels2[FORWARD_ECHO_42_BASE] do   begin InfoSide:= isLeft;  InfoPos:= ipLeft;   end;
    with EvalChannels2[BACKWARD_ECHO_42_WEB] do   begin InfoSide:= isRight; InfoPos:= ipRight;  end;
    with EvalChannels2[BACKWARD_ECHO_42_BASE] do  begin InfoSide:= isRight; InfoPos:= ipLeft;   end;

    with EvalChannels2[ECHO_0] do                 begin InfoSide:= isRight;  InfoPos:= ipCentre; end;
    with EvalChannels2[MIRRORSHADOW_0] do         begin InfoSide:= isLeft; InfoPos:= ipCentre; end;

    // ------------ ���� ������ �-��������� ------------------

    SetLength(FBScanLines, 12);

    for I := 0 to 11 do
    begin
      FBScanLines[I].R:= rLeft;
      FBScanLines[I].Use:= True;
      SetLength(FBScanLines[I].Items, 2);
      FBScanLines[I].Items[0].ScanChIdx:= I * 2;
      FBScanLines[I].Items[1].ScanChIdx:= I * 2 + 1;
//      FBScanLines[I].Items[0].Order:= 1;
    end;

    // ------------ ������ ������� �������� ------------

    SetLength(FHandChannels, 0);

    // ------------ ������ �-��������� ------------

    SetLength(FScanChannels, 24);

    for I := 0 to 23 do
    begin
      FScanChannels[I].Number:= I;
      FScanChannels[I].EnterAngle:= MIGViewEnterAngle[I];
      if Odd(I) then FScanChannels[I].EnterAngle:= FScanChannels[I].EnterAngle * - 1;

      FScanChannels[I].TurnAngle:= MIGViewTurnAngle[I];
      FScanChannels[I].Position:= MIGViewPosition[I];
      FScanChannels[I].BScanGateMin:= 0;
      FScanChannels[I].BScanGateMax:= MIGViewBScanGateMax[I];
      FScanChannels[I].DelayMultiply:= 1;
    end;

    for I:= 0 to High(FScanChannels) do
      FScanChannels[I].BScanDuration:= FScanChannels[I].BScanGateMax - FScanChannels[I].BScanGateMin + 1;

    // ------------  ������ ������ ------------

    SetLength(FEvalChannels, 24);

    for I := 0 to 23 do
    begin
      FEvalChannels[I].Number:= I;
      FEvalChannels[I].ScanChNum:= I;
      FEvalChannels[I].Method:= imEcho;
      FEvalChannels[I].ShortName:= ''; // 'SHORT_-70_ECHO';
      FEvalChannels[I].FullName:= ''; // 'FULL_-70_ECHO';
      FEvalChannels[I].Zone:= MIGViewZone[I];
      if Odd(I) then FEvalChannels[I].DirColor:= - 1
                else FEvalChannels[I].DirColor:= + 1;
    end;



(*
    FDeviceID:= MIGView;           // ��� ������������
    FDeviceVer:= 2;
    FMinRail:= rLeft;
    FMaxRail:= rRight;
    FDeviceName:= 'MIG View';

    // ------------ ���� ������ �-��������� ------------------

    SetLength(FBScanLines, 24);

    for I := 0 to 11 do
    begin
      FBScanLines[I].R:= rLeft;
      FBScanLines[I].Use:= True;
      SetLength(FBScanLines[I].Items, 1);
      FBScanLines[I].Items[0].ScanChIdx:= I;
    end;

  //////////////////////////////////////////////////

    for I := 12 to 23 do
    begin
      FBScanLines[I].R:= rRight;
      FBScanLines[I].Use:= True;
      SetLength(FBScanLines[I].Items, 1);
      FBScanLines[I].Items[0].ScanChIdx:= I - 12;
    end;

    // ------------ ������ ������� �������� ------------

    SetLength(FHandChannels, 0);

    // ------------ ������ �-��������� ------------

    SetLength(FScanChannels, 12);

    for I := 0 to 11 do
    begin
      FScanChannels[I].Number:= I;
      FScanChannels[I].EnterAngle:= 0;
      FScanChannels[I].TurnAngle:= 0;
      FScanChannels[I].Position:= 0;
      FScanChannels[I].BScanGateMin:= 0;
      FScanChannels[I].BScanGateMax:= 255;
      FScanChannels[I].DelayMultiply:= 1;
    end;

    for I:= 0 to High(FScanChannels) do
      FScanChannels[I].BScanDuration:= FScanChannels[I].BScanGateMax - FScanChannels[I].BScanGateMin + 1;

    // ------------  ������ ������ ------------

    SetLength(FEvalChannels, 12);

    for I := 0 to 11 do
    begin
      FEvalChannels[I].Number:= I;     // 0 ���� ���
      FEvalChannels[I].ScanChNum:= I;
    end;
*)
  end
  else
//////////////////////////////////////////////////////////////////////////////////////////////////////////

  if CompareMem(@Temp, @USK004R, 8) then
  begin
    FDeviceID:= Temp;              // ��� ������������
    FDeviceVer:= 3;
    FMinRail:= rLeft;
    FMaxRail:= rLeft;
    FDeviceName:= 'USK004R';

  // ------------------------------------------------------------------------------

    ChannelIDList[rLeft, 0]:= MIRRORSHADOW_0;
    ChannelIDList[rLeft, 1]:= ECHO_0;
    ChannelIDList[rLeft, 2]:= FORWARD_UWORK_ECHO_50;
    ChannelIDList[rLeft, 3]:= BACKWARD_UWORK_ECHO_50;
    ChannelIDList[rLeft, 4]:= FORWARD_ECHO_70;
    ChannelIDList[rLeft, 5]:= BACKWARD_ECHO_70;
    ChannelIDList[rLeft, 6]:= FORWARD_ECHO_45_WEB;
    ChannelIDList[rLeft, 7]:= BACKWARD_ECHO_45_WEB;
    ChannelIDList[rLeft, 8]:= FORWARD_ECHO_45_BASE;
    ChannelIDList[rLeft, 9]:= BACKWARD_ECHO_45_BASE;
    ChannelIDList[rLeft, 10]:= FORWARD_WORK_ECHO_50;
    ChannelIDList[rLeft, 11]:= BACKWARD_WORK_ECHO_50;

    ChannelIDList[rRight, 0]:= MIRRORSHADOW_0;
    ChannelIDList[rRight, 1]:= ECHO_0;
    ChannelIDList[rRight, 2]:= FORWARD_WORK_ECHO_50;
    ChannelIDList[rRight, 3]:= BACKWARD_WORK_ECHO_50;
    ChannelIDList[rRight, 4]:= FORWARD_ECHO_70;
    ChannelIDList[rRight, 5]:= BACKWARD_ECHO_70;
    ChannelIDList[rRight, 6]:= FORWARD_ECHO_45_WEB;
    ChannelIDList[rRight, 7]:= BACKWARD_ECHO_45_WEB;
    ChannelIDList[rRight, 8]:= FORWARD_ECHO_45_BASE;
    ChannelIDList[rRight, 9]:= BACKWARD_ECHO_45_BASE;
    ChannelIDList[rRight, 10]:= FORWARD_UWORK_ECHO_50;
    ChannelIDList[rRight, 11]:= BACKWARD_UWORK_ECHO_50;

  // ------------------------------------------------------------------------------

    with EvalChannels2[FORWARD_WORK_ECHO_50] do   begin InfoSide:= isLeft;  InfoPos:= ipLeft;   end;
    with EvalChannels2[FORWARD_UWORK_ECHO_50] do  begin InfoSide:= isLeft;  InfoPos:= ipRight;  end;

    with EvalChannels2[BACKWARD_WORK_ECHO_50] do  begin InfoSide:= isRight; InfoPos:= ipLeft;   end;
    with EvalChannels2[BACKWARD_UWORK_ECHO_50] do begin InfoSide:= isRight; InfoPos:= ipRight;  end;

    with EvalChannels2[FORWARD_ECHO_70] do        begin InfoSide:= isLeft;  InfoPos:= ipCentre; end;
    with EvalChannels2[BACKWARD_ECHO_70] do       begin InfoSide:= isRight; InfoPos:= ipCentre; end;

    with EvalChannels2[FORWARD_ECHO_45_WEB] do    begin InfoSide:= isLeft;  InfoPos:= ipRight;  end;
    with EvalChannels2[FORWARD_ECHO_45_BASE] do   begin InfoSide:= isLeft;  InfoPos:= ipLeft;   end;
    with EvalChannels2[BACKWARD_ECHO_45_WEB] do   begin InfoSide:= isRight; InfoPos:= ipRight;  end;
    with EvalChannels2[BACKWARD_ECHO_45_BASE] do  begin InfoSide:= isRight; InfoPos:= ipLeft;   end;

    with EvalChannels2[ECHO_0] do                 begin InfoSide:= isRight; InfoPos:= ipCentre; end;
    with EvalChannels2[MIRRORSHADOW_0] do         begin InfoSide:= isLeft; InfoPos:= ipCentre; end;

    // ------------ ���� ������ �-��������� ------------------

    SetLength(FBScanLines, 6);

    FBScanLines[0].R:= rLeft;
    FBScanLines[0].Use:= True;
    SetLength(FBScanLines[0].Items, 2);
    FBScanLines[0].Items[0].ScanChIdx:= 7;
    FBScanLines[0].Items[1].ScanChIdx:= 8;

    FBScanLines[1].R:= rLeft;
    FBScanLines[1].Use:= True;
    SetLength(FBScanLines[1].Items, 2);
    FBScanLines[1].Items[0].ScanChIdx:= 1;
    FBScanLines[1].Items[1].ScanChIdx:= 2;

    FBScanLines[2].R:= rLeft;
    FBScanLines[2].Use:= True;
    SetLength(FBScanLines[2].Items, 2);
    FBScanLines[2].Items[0].ScanChIdx:= 3;
    FBScanLines[2].Items[1].ScanChIdx:= 4;

    FBScanLines[3].R:= rLeft;
    FBScanLines[3].Use:= True;
    SetLength(FBScanLines[3].Items, 2);
    FBScanLines[3].Items[0].ScanChIdx:= 5;
    FBScanLines[3].Items[1].ScanChIdx:= 6;

    FBScanLines[4].R:= rLeft;
    FBScanLines[4].Use:= True;
    SetLength(FBScanLines[4].Items, 1);
    FBScanLines[4].Items[0].ScanChIdx:= 0;

    // ------------ ������ ������� �������� ------------

    AddHandScanChannels(FHandChannels);

// ���� �����      ����.����.       B-���.���.    B-���.���.   ���� ���������
//   0               70              5             70            0
//   70              93              5             92            0
//   45              186             6             184           0
//   50              116             20            114           45

    // ------------ ������ �-��������� � ������ ������ ------------

    SetLength(FScanChannels, 9);
    SetLength(FEvalChannels, 12);

    FScanChannels[0].Number:= 1;  //  0 ����
    FScanChannels[0].Position:= 87;
    FScanChannels[0].EnterAngle:= 0;
    FScanChannels[0].TurnAngle:= 0;
    FScanChannels[0].BScanGateMin:= 5;
    FScanChannels[0].BScanGateMax:= 70;
    FScanChannels[0].DelayMultiply:= 3;
    FScanChannels[0].Delta:= 0;
    FScanChannels[0].Gran:= _None;

      FEvalChannels[0].Number:= 0;     // 0 ���
      FEvalChannels[0].ScanChNum:= 1;
      FEvalChannels[0].Method:= imMirrorShadow;
      FEvalChannels[0].Zone:= izAll;
      FEvalChannels[0].ShortName:= 'SHORT_0_MIRRORSHADOW';
      FEvalChannels[0].FullName:= 'FULL_0_MIRRORSHADOW';
      FEvalChannels[0].DirColor:= + 1;

      FEvalChannels[1].Number:= 1;  // 0 ���
      FEvalChannels[1].ScanChNum:= 1;
      FEvalChannels[1].Method:= imEcho;
      FEvalChannels[1].Zone:= izAll;
      FEvalChannels[1].ShortName:= 'SHORT_0_ECHO';
      FEvalChannels[1].FullName:= 'FULL_0_ECHO';
      FEvalChannels[1].DirColor:= - 1;

    FScanChannels[1].Number:= 2;  // 50 ����������
    FScanChannels[1].EnterAngle:= 50;
    FScanChannels[1].TurnAngle:= 45;
    FScanChannels[1].Position:= 62;
    FScanChannels[1].BScanGateMin:= 20;
    FScanChannels[1].BScanGateMax:= 114;
    FScanChannels[1].DelayMultiply:= 1;
    FScanChannels[1].Delta:= - 90;
    FScanChannels[1].Gran:= _UnWork_Left__Work_Right;

      FEvalChannels[2].Number:= 2;     // 50 ����������
      FEvalChannels[2].ScanChNum:= 2;
      FEvalChannels[2].Method:= imEcho;
      FEvalChannels[2].Zone:= izHead;
      FEvalChannels[2].ShortName:= 'SHORT_+50_ECHO_WORK';
      FEvalChannels[2].FullName:= 'FULL_+50_ECHO_WORK';
      FEvalChannels[2].DirColor:= + 1;

    FScanChannels[2].Number:= 3;  // 50 �����������
    FScanChannels[2].EnterAngle:= - 50;
    FScanChannels[2].TurnAngle:= 45;
    FScanChannels[2].Position:= -62;
    FScanChannels[2].BScanGateMin:= 20;
    FScanChannels[2].BScanGateMax:= 114;
    FScanChannels[2].DelayMultiply:= 1;
    FScanChannels[2].Delta:= + 90;
    FScanChannels[2].Gran:= _UnWork_Left__Work_Right;

      FEvalChannels[3].Number:= 3;     // 50 �����������
      FEvalChannels[3].ScanChNum:= 3;
      FEvalChannels[3].Method:= imEcho;
      FEvalChannels[3].Zone:= izHead;
      FEvalChannels[3].ShortName:= 'SHORT_-50_ECHO_WORK';
      FEvalChannels[3].FullName:= 'FULL_-50_ECHO_WORK';
      FEvalChannels[3].DirColor:= - 1;

    FScanChannels[3].Number:= 4;  // 70 ���� ����������
    FScanChannels[3].EnterAngle:= 70;
    FScanChannels[3].TurnAngle:= 0;
    FScanChannels[3].Position:= {70;} 112; // 122
    FScanChannels[3].BScanGateMin:= 5;
    FScanChannels[3].BScanGateMax:= 92;
    FScanChannels[3].DelayMultiply:= 1;
    FScanChannels[3].Delta:= - 80;
    FScanChannels[3].Gran:= _None;

      FEvalChannels[4].Number:= 4;     // 70 ����������
      FEvalChannels[4].ScanChNum:= 4;
      FEvalChannels[4].Method:= imEcho;
      FEvalChannels[4].Zone:= izHead;
      FEvalChannels[4].ShortName:= 'SHORT_+70_ECHO';
      FEvalChannels[4].FullName:= 'FULL_+70_ECHO';
      FEvalChannels[4].DirColor:= + 1;

    FScanChannels[4].Number:= 5;  // 70 ���� ����������
    FScanChannels[4].EnterAngle:= - 70;
    FScanChannels[4].TurnAngle:= 0;
    FScanChannels[4].Position:= {-70;} -112; // -122
    FScanChannels[4].BScanGateMin:= 5;
    FScanChannels[4].BScanGateMax:= 92;
    FScanChannels[4].DelayMultiply:= 1;
    FScanChannels[4].Delta:= 80;
    FScanChannels[4].Gran:= _None;

      FEvalChannels[5].Number:= 5;     // 70 �����������
      FEvalChannels[5].ScanChNum:= 5;
      FEvalChannels[5].Method:= imEcho;
      FEvalChannels[5].Zone:= izHead;
      FEvalChannels[5].ShortName:= 'SHORT_-70_ECHO';
      FEvalChannels[5].FullName:= 'FULL_-70_ECHO';
      FEvalChannels[5].DirColor:= - 1;

    FScanChannels[5].Number:= 6;  // 45 ���� ����������
    FScanChannels[5].EnterAngle:= 45;
    FScanChannels[5].TurnAngle:= 0;
    FScanChannels[5].Position:= -87;
    FScanChannels[5].BScanGateMin:= 6;
    FScanChannels[5].BScanGateMax:= 184;
    FScanChannels[5].DelayMultiply:= 1;
    FScanChannels[5].Delta:= - 30;
    FScanChannels[5].Gran:= _None;

      FEvalChannels[6].Number:= 6;      // 45 ����������
      FEvalChannels[6].ScanChNum:= 6;
      FEvalChannels[6].Method:= imEcho;
      FEvalChannels[6].Zone:= izWeb;
      FEvalChannels[6].ShortName:= 'SHORT_+45_ECHO_NEAR';
      FEvalChannels[6].FullName:= 'FULL_+45_ECHO_NEAR';
      FEvalChannels[6].DirColor:= + 1;

      FEvalChannels[8].Number:= 8;      // 42 ����������
      FEvalChannels[8].ScanChNum:= 6;
      FEvalChannels[8].Method:= imEcho;
      FEvalChannels[8].Zone:= izBase;
      FEvalChannels[8].ShortName:= 'SHORT_+45_ECHO_FAR';
      FEvalChannels[8].FullName:= 'FULL_+45_ECHO_FAR';
      FEvalChannels[8].DirColor:= + 1;

    FScanChannels[6].Number:= 7;  // 42 ���� �����������
    FScanChannels[6].EnterAngle:= - 45;
    FScanChannels[6].TurnAngle:= 0;
    FScanChannels[6].Position:= -87;
    FScanChannels[6].BScanGateMin:= 6;
    FScanChannels[6].BScanGateMax:= 184;
    FScanChannels[6].DelayMultiply:= 1;
    FScanChannels[6].Delta:= + 30;
    FScanChannels[6].Gran:= _None;

      FEvalChannels[7].Number:= 7;      // 42 �����������
      FEvalChannels[7].ScanChNum:= 7;
      FEvalChannels[7].Method:= imEcho;
      FEvalChannels[7].Zone:= izWeb;
      FEvalChannels[7].ShortName:= 'SHORT_-45_ECHO_NEAR';
      FEvalChannels[7].FullName:= 'FULL_-45_ECHO_NEAR';
      FEvalChannels[7].DirColor:= - 1;

      FEvalChannels[9].Number:= 9;      // 42 �����������
      FEvalChannels[9].ScanChNum:= 7;
      FEvalChannels[9].Method:= imEcho;
      FEvalChannels[9].Zone:= izBase;
      FEvalChannels[9].ShortName:= 'SHORT_-45_ECHO_FAR';
      FEvalChannels[9].FullName:= 'FULL_-45_ECHO_FAR';
      FEvalChannels[9].DirColor:= - 1;

    FScanChannels[7].Number:= 10;  // 50 ����������
    FScanChannels[7].EnterAngle:= 50;
    FScanChannels[7].TurnAngle:= - 45;
    FScanChannels[7].Position:= 62;
    FScanChannels[7].BScanGateMin:= 20;
    FScanChannels[7].BScanGateMax:= 100;
    FScanChannels[7].DelayMultiply:= 1;
    FScanChannels[7].Delta:= - 90;
    FScanChannels[7].Gran:= _Work_Left__UnWork_Right;

      FEvalChannels[10].Number:= 10;    // 58 ����������
      FEvalChannels[10].ScanChNum:= 10;
      FEvalChannels[10].Method:= imEcho;
      FEvalChannels[10].Zone:= izHead;
      FEvalChannels[10].ShortName:= 'SHORT_+50_ECHO_NOWORK';
      FEvalChannels[10].FullName:= 'FULL_+50_ECHO_NOWORK';
      FEvalChannels[10].DirColor:= + 1;

    FScanChannels[8].Number:= 11;  // 50 �����������
    FScanChannels[8].EnterAngle:= - 50;
    FScanChannels[8].TurnAngle:= - 45;
    FScanChannels[8].Position:= - 62;
    FScanChannels[8].BScanGateMin:= 20;
    FScanChannels[8].BScanGateMax:= 100;
    FScanChannels[8].DelayMultiply:= 1;
    FScanChannels[8].Delta:= + 90;
    FScanChannels[8].Gran:= _Work_Left__UnWork_Right;

      FEvalChannels[11].Number:= 11;    // 50 �����������
      FEvalChannels[11].ScanChNum:= 11;
      FEvalChannels[11].Method:= imEcho;
      FEvalChannels[11].Zone:= izHead;
      FEvalChannels[11].ShortName:= 'SHORT_-50_ECHO_NOWORK';
      FEvalChannels[11].FullName:= 'FULL_-50_ECHO_NOWORK';
      FEvalChannels[11].DirColor:= - 1;

    for I:= 0 to High(FScanChannels) do
      FScanChannels[I].BScanDuration:= FScanChannels[I].BScanGateMax - FScanChannels[I].BScanGateMin + 1;

 end
 else
  if CompareMem(@Temp, @RKS_U_Scheme_5, 8) then
  begin
    FDeviceID:= Temp;              // ��� ������������
    FDeviceVer:= 1;
    FMinRail:= rLeft;
    FMaxRail:= rLeft;
    FDeviceName:= 'RKS-U';

  // ------------------------------------------------------------------------------

    ChannelIDList[rLeft, 0]:= ECHO_0;
    ChannelIDList[rLeft, 1]:= SIDE_ECHO_65;
    ChannelIDList[rLeft, 2]:= SIDE_ECHO_65;
    ChannelIDList[rLeft, 3]:= SIDE_ECHO_65;
    ChannelIDList[rLeft, 4]:= SIDE_ECHO_65;

    ChannelIDList[rRight, 0]:= ECHO_0;
    ChannelIDList[rRight, 1]:= SIDE_ECHO_65;
    ChannelIDList[rRight, 2]:= SIDE_ECHO_65;
    ChannelIDList[rRight, 3]:= SIDE_ECHO_65;
    ChannelIDList[rRight, 4]:= SIDE_ECHO_65;

  // ------------------------------------------------------------------------------

    with EvalChannels2[ECHO_0] do  begin InfoSide:= isLeft;  InfoPos:= ipCentre; end;
    with EvalChannels2[SIDE_ECHO_65] do  begin InfoSide:= isLeft;  InfoPos:= ipCentre; end;

    // ------------ ���� ������ �-��������� ------------------

    SetLength(FBScanLines, 5);

    FBScanLines[0].R:= rLeft;
    FBScanLines[0].Use:= True;
    SetLength(FBScanLines[0].Items, 1);
    FBScanLines[0].Items[0].ScanChIdx:= 0;

    FBScanLines[1].R:= rLeft;
    FBScanLines[1].Use:= True;
    SetLength(FBScanLines[1].Items, 1);
    FBScanLines[1].Items[0].ScanChIdx:= 1;

    FBScanLines[2].R:= rLeft;
    FBScanLines[2].Use:= True;
    SetLength(FBScanLines[2].Items, 1);
    FBScanLines[2].Items[0].ScanChIdx:= 2;

    FBScanLines[3].R:= rLeft;
    FBScanLines[3].Use:= True;
    SetLength(FBScanLines[3].Items, 1);
    FBScanLines[3].Items[0].ScanChIdx:= 3;

    FBScanLines[4].R:= rLeft;
    FBScanLines[4].Use:= True;
    SetLength(FBScanLines[4].Items, 1);
    FBScanLines[4].Items[0].ScanChIdx:= 4;

    // ------------ ������ ������� �������� ------------

    AddHandScanChannels(FHandChannels);

    // ------------ ������ �-��������� � ������ ������ ------------

    SetLength(FScanChannels, 5);
    SetLength(FEvalChannels, 5);

    FScanChannels[0].Number:= 0;  //  0 ����
    FScanChannels[0].Position:= 0;
    FScanChannels[0].EnterAngle:= 0;
    FScanChannels[0].TurnAngle:= 0;
    FScanChannels[0].BScanGateMin:= 0;
    FScanChannels[0].BScanGateMax:= 48;
    FScanChannels[0].DelayMultiply:= 3;
    FScanChannels[0].Delta:= 0;
    FScanChannels[0].Gran:= _None;

      FEvalChannels[0].Number:= 0;     // 0 ���
      FEvalChannels[0].ScanChNum:= 1;
      FEvalChannels[0].Method:= imMirrorShadow;
      FEvalChannels[0].Zone:= izAll;
      FEvalChannels[0].ShortName:= 'SHORT_0_MIRRORSHADOW';
      FEvalChannels[0].FullName:= 'FULL_0_MIRRORSHADOW';
      FEvalChannels[0].DirColor:= + 1;

    FScanChannels[1].Number:= 1;  // 65
    FScanChannels[1].Position:= 0;
    FScanChannels[1].EnterAngle:= 65;
    FScanChannels[1].TurnAngle:= 90;
    FScanChannels[1].BScanGateMin:= 0;
    FScanChannels[1].BScanGateMax:= 48;
    FScanChannels[1].DelayMultiply:= 1;
    FScanChannels[1].Delta:= 0;
    FScanChannels[1].Gran:= _None;

      FEvalChannels[1].Number:= 1;     // 65
      FEvalChannels[1].ScanChNum:= 2;
      FEvalChannels[1].Method:= imEcho;
      FEvalChannels[1].Zone:= izAll;
      FEvalChannels[1].ShortName:= '';
      FEvalChannels[1].FullName:= '';
      FEvalChannels[1].DirColor:= + 1;

    FScanChannels[2].Number:= 2;  // 65
    FScanChannels[2].Position:= 0;
    FScanChannels[2].EnterAngle:= 65;
    FScanChannels[2].TurnAngle:= 90;
    FScanChannels[2].BScanGateMin:= 0;
    FScanChannels[2].BScanGateMax:= 48;
    FScanChannels[2].DelayMultiply:= 1;
    FScanChannels[2].Delta:= 0;
    FScanChannels[2].Gran:= _None;

      FEvalChannels[2].Number:= 2;     // 65
      FEvalChannels[2].ScanChNum:= 3;
      FEvalChannels[2].Method:= imEcho;
      FEvalChannels[2].Zone:= izAll;
      FEvalChannels[2].ShortName:= '';
      FEvalChannels[2].FullName:= '';
      FEvalChannels[2].DirColor:= - 1;

    FScanChannels[3].Number:= 3;  // 65
    FScanChannels[3].Position:= 0;
    FScanChannels[3].EnterAngle:= 65;
    FScanChannels[3].TurnAngle:= 90;
    FScanChannels[3].BScanGateMin:= 0;
    FScanChannels[3].BScanGateMax:= 48;
    FScanChannels[3].DelayMultiply:= 1;
    FScanChannels[3].Delta:= 0;
    FScanChannels[3].Gran:= _None;

      FEvalChannels[3].Number:= 3;     // 65
      FEvalChannels[3].ScanChNum:= 4;
      FEvalChannels[3].Method:= imEcho;
      FEvalChannels[3].Zone:= izAll;
      FEvalChannels[3].ShortName:= '';
      FEvalChannels[3].FullName:= '';
      FEvalChannels[3].DirColor:= + 1;

    FScanChannels[4].Number:= 4;  // 65
    FScanChannels[4].Position:= 0;
    FScanChannels[4].EnterAngle:= 65;
    FScanChannels[4].TurnAngle:= 90;
    FScanChannels[4].BScanGateMin:= 0;
    FScanChannels[4].BScanGateMax:= 48;
    FScanChannels[4].DelayMultiply:= 1;
    FScanChannels[4].Delta:= 0;
    FScanChannels[4].Gran:= _None;

      FEvalChannels[4].Number:= 4;     // 65
      FEvalChannels[4].ScanChNum:= 5;
      FEvalChannels[4].Method:= imEcho;
      FEvalChannels[4].Zone:= izAll;
      FEvalChannels[4].ShortName:= '';
      FEvalChannels[4].FullName:= '';
      FEvalChannels[4].DirColor:= - 1;

    for I:= 0 to High(FScanChannels) do
      FScanChannels[I].BScanDuration:= FScanChannels[I].BScanGateMax - FScanChannels[I].BScanGateMin + 1;

 end
 else
  if CompareMem(@Temp, @RKS_U_Scheme_8, 8) then
  begin
    FDeviceID:= Temp;              // ��� ������������
    FDeviceVer:= 1;
    FMinRail:= rLeft;
    FMaxRail:= rLeft;
    FDeviceName:= 'RKS-U';

  // ------------------------------------------------------------------------------

    ChannelIDList[rLeft, 0]:= ECHO_0;
    ChannelIDList[rLeft, 1]:= SIDE_ECHO_65;
    ChannelIDList[rLeft, 2]:= SIDE_ECHO_65;
    ChannelIDList[rLeft, 3]:= SIDE_ECHO_65;
    ChannelIDList[rLeft, 4]:= SIDE_ECHO_65;
    ChannelIDList[rLeft, 5]:= SIDE_ECHO_65;
    ChannelIDList[rLeft, 6]:= SIDE_ECHO_65;
    ChannelIDList[rLeft, 7]:= SIDE_ECHO_65;

    ChannelIDList[rRight, 0]:= ECHO_0;
    ChannelIDList[rRight, 1]:= SIDE_ECHO_65;
    ChannelIDList[rRight, 2]:= SIDE_ECHO_65;
    ChannelIDList[rRight, 3]:= SIDE_ECHO_65;
    ChannelIDList[rRight, 4]:= SIDE_ECHO_65;
    ChannelIDList[rRight, 5]:= SIDE_ECHO_65;
    ChannelIDList[rRight, 6]:= SIDE_ECHO_65;
    ChannelIDList[rRight, 7]:= SIDE_ECHO_65;

  // ------------------------------------------------------------------------------

    with EvalChannels2[ECHO_0] do  begin InfoSide:= isLeft;  InfoPos:= ipCentre; end;
    with EvalChannels2[SIDE_ECHO_65] do  begin InfoSide:= isLeft;  InfoPos:= ipCentre; end;

    // ------------ ���� ������ �-��������� ------------------

    SetLength(FBScanLines, 8);

    FBScanLines[0].R:= rLeft;
    FBScanLines[0].Use:= True;
    SetLength(FBScanLines[0].Items, 1);
    FBScanLines[0].Items[0].ScanChIdx:= 0;

    FBScanLines[1].R:= rLeft;
    FBScanLines[1].Use:= True;
    SetLength(FBScanLines[1].Items, 1);
    FBScanLines[1].Items[0].ScanChIdx:= 1;

    FBScanLines[2].R:= rLeft;
    FBScanLines[2].Use:= True;
    SetLength(FBScanLines[2].Items, 1);
    FBScanLines[2].Items[0].ScanChIdx:= 2;

    FBScanLines[3].R:= rLeft;
    FBScanLines[3].Use:= True;
    SetLength(FBScanLines[3].Items, 1);
    FBScanLines[3].Items[0].ScanChIdx:= 3;

    FBScanLines[4].R:= rLeft;
    FBScanLines[4].Use:= True;
    SetLength(FBScanLines[4].Items, 1);
    FBScanLines[4].Items[0].ScanChIdx:= 4;

    FBScanLines[5].R:= rLeft;
    FBScanLines[5].Use:= True;
    SetLength(FBScanLines[5].Items, 1);
    FBScanLines[5].Items[0].ScanChIdx:= 5;

    FBScanLines[6].R:= rLeft;
    FBScanLines[6].Use:= True;
    SetLength(FBScanLines[6].Items, 1);
    FBScanLines[6].Items[0].ScanChIdx:= 6;

    FBScanLines[7].R:= rLeft;
    FBScanLines[7].Use:= True;
    SetLength(FBScanLines[7].Items, 1);
    FBScanLines[7].Items[0].ScanChIdx:= 7;

    // ------------ ������ ������� �������� ------------

    AddHandScanChannels(FHandChannels);

    // ------------ ������ �-��������� � ������ ������ ------------

    SetLength(FScanChannels, 8);
    SetLength(FEvalChannels, 8);

    FScanChannels[0].Number:= 0;  //  0 ����
    FScanChannels[0].Position:= 0;
    FScanChannels[0].EnterAngle:= 0;
    FScanChannels[0].TurnAngle:= 0;
    FScanChannels[0].BScanGateMin:= 0;
    FScanChannels[0].BScanGateMax:= 50;
    FScanChannels[0].DelayMultiply:= 3;
    FScanChannels[0].Delta:= 0;
    FScanChannels[0].Gran:= _None;

      FEvalChannels[0].Number:= 0;     // 0 ���
      FEvalChannels[0].ScanChNum:= 0;
      FEvalChannels[0].Method:= imMirrorShadow;
      FEvalChannels[0].Zone:= izAll;
      FEvalChannels[0].ShortName:= 'SHORT_0_MIRRORSHADOW';
      FEvalChannels[0].FullName:= 'FULL_0_MIRRORSHADOW';
      FEvalChannels[0].DirColor:= 1;

    FScanChannels[1].Number:= 1;  // 65
    FScanChannels[1].EnterAngle:= 65;
    FScanChannels[1].TurnAngle:= 90;
    FScanChannels[1].Position:= 0;
    FScanChannels[1].BScanGateMin:= 0;
    FScanChannels[1].BScanGateMax:= 50;
    FScanChannels[1].DelayMultiply:= 1;
    FScanChannels[1].Delta:= 0;
    FScanChannels[1].Gran:= _None;

      FEvalChannels[1].Number:= 1;     // 65
      FEvalChannels[1].ScanChNum:= 1;
      FEvalChannels[1].Method:= imEcho;
      FEvalChannels[1].Zone:= izAll;
      FEvalChannels[1].ShortName:= '';
      FEvalChannels[1].FullName:= '';
      FEvalChannels[1].DirColor:= 1;

    FScanChannels[2].Number:= 2;  // 65
    FScanChannels[2].EnterAngle:= 65;
    FScanChannels[2].TurnAngle:= 90;
    FScanChannels[2].Position:= 0;
    FScanChannels[2].BScanGateMin:= 0;
    FScanChannels[2].BScanGateMax:= 50;
    FScanChannels[2].DelayMultiply:= 1;
    FScanChannels[2].Delta:= 0;
    FScanChannels[2].Gran:= _None;

      FEvalChannels[2].Number:= 2;     // 65
      FEvalChannels[2].ScanChNum:= 2;
      FEvalChannels[2].Method:= imEcho;
      FEvalChannels[2].Zone:= izAll;
      FEvalChannels[2].ShortName:= '';
      FEvalChannels[2].FullName:= '';
      FEvalChannels[2].DirColor:= 1;

    FScanChannels[3].Number:= 3;  // 65
    FScanChannels[3].EnterAngle:= 65;
    FScanChannels[3].TurnAngle:= 90;
    FScanChannels[3].Position:= 0;
    FScanChannels[3].BScanGateMin:= 0;
    FScanChannels[3].BScanGateMax:= 50;
    FScanChannels[3].DelayMultiply:= 1;
    FScanChannels[3].Delta:= 0;
    FScanChannels[3].Gran:= _None;

      FEvalChannels[3].Number:= 3;     // 65
      FEvalChannels[3].ScanChNum:= 3;
      FEvalChannels[3].Method:= imEcho;
      FEvalChannels[3].Zone:= izAll;
      FEvalChannels[3].ShortName:= '';
      FEvalChannels[3].FullName:= '';
      FEvalChannels[3].DirColor:= 1;

    FScanChannels[4].Number:= 4;  // 65
    FScanChannels[4].EnterAngle:= 65;
    FScanChannels[4].TurnAngle:= 90;
    FScanChannels[4].Position:= 0;
    FScanChannels[4].BScanGateMin:= 0;
    FScanChannels[4].BScanGateMax:= 50;
    FScanChannels[4].DelayMultiply:= 1;
    FScanChannels[4].Delta:= 0;
    FScanChannels[4].Gran:= _None;

      FEvalChannels[4].Number:= 4;     // 65
      FEvalChannels[4].ScanChNum:= 4;
      FEvalChannels[4].Method:= imEcho;
      FEvalChannels[4].Zone:= izAll;
      FEvalChannels[4].ShortName:= '';
      FEvalChannels[4].FullName:= '';
      FEvalChannels[4].DirColor:= 1;

    FScanChannels[5].Number:= 5;  // 65
    FScanChannels[5].EnterAngle:= 65;
    FScanChannels[5].TurnAngle:= 90;
    FScanChannels[5].Position:= 0;
    FScanChannels[5].BScanGateMin:= 0;
    FScanChannels[5].BScanGateMax:= 50;
    FScanChannels[5].DelayMultiply:= 1;
    FScanChannels[5].Delta:= 0;
    FScanChannels[5].Gran:= _None;

      FEvalChannels[5].Number:= 5;     // 65
      FEvalChannels[5].ScanChNum:= 5;
      FEvalChannels[5].Method:= imEcho;
      FEvalChannels[5].Zone:= izAll;
      FEvalChannels[5].ShortName:= '';
      FEvalChannels[5].FullName:= '';
      FEvalChannels[5].DirColor:= 1;

    FScanChannels[6].Number:= 6;  // 65
    FScanChannels[6].EnterAngle:= 65;
    FScanChannels[6].TurnAngle:= 90;
    FScanChannels[6].Position:= 0;
    FScanChannels[6].BScanGateMin:= 0;
    FScanChannels[6].BScanGateMax:= 50;
    FScanChannels[6].DelayMultiply:= 1;
    FScanChannels[6].Delta:= 0;
    FScanChannels[6].Gran:= _None;

      FEvalChannels[6].Number:= 6;     // 65
      FEvalChannels[6].ScanChNum:= 6;
      FEvalChannels[6].Method:= imEcho;
      FEvalChannels[6].Zone:= izAll;
      FEvalChannels[6].ShortName:= '';
      FEvalChannels[6].FullName:= '';
      FEvalChannels[6].DirColor:= 1;

    FScanChannels[7].Number:= 7;  // 65
    FScanChannels[7].EnterAngle:= 65;
    FScanChannels[7].TurnAngle:= 90;
    FScanChannels[7].Position:= 0;
    FScanChannels[7].BScanGateMin:= 0;
    FScanChannels[7].BScanGateMax:= 50;
    FScanChannels[7].DelayMultiply:= 1;
    FScanChannels[7].Delta:= 0;
    FScanChannels[7].Gran:= _None;

      FEvalChannels[7].Number:= 7;     // 65
      FEvalChannels[7].ScanChNum:= 7;
      FEvalChannels[7].Method:= imEcho;
      FEvalChannels[7].Zone:= izAll;
      FEvalChannels[7].ShortName:= '';
      FEvalChannels[7].FullName:= '';
      FEvalChannels[7].DirColor:= 1;

    for I:= 0 to High(FScanChannels) do
      FScanChannels[I].BScanDuration:= FScanChannels[I].BScanGateMax - FScanChannels[I].BScanGateMin + 1;

 end
 else

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

  if CompareMem(@Temp, @Avicon31Scheme1, 8) then

  begin
    FDeviceID:= Temp;              // ��� ������������
    FDeviceVer:= 3;
    FMinRail:= rLeft;
    FMaxRail:= rRight;

    if CompareMem(@Temp, @Avicon16Scheme1, 8) then FDeviceName:= 'Avicon16Scheme1';
    if CompareMem(@Temp, @Avicon14Scheme1, 8) then FDeviceName:= 'Avicon14Scheme1';

  // ------------------------------------------------------------------------------

    ChannelIDList[rLeft, 0]:= MIRRORSHADOW_0;
    ChannelIDList[rLeft, 1]:= ECHO_0;
    ChannelIDList[rLeft, 2]:= FORWARD_UWORK_ECHO_58;
    ChannelIDList[rLeft, 3]:= BACKWARD_UWORK_ECHO_58;
    ChannelIDList[rLeft, 4]:= FORWARD_ECHO_70;
    ChannelIDList[rLeft, 5]:= BACKWARD_ECHO_70;
    ChannelIDList[rLeft, 6]:= FORWARD_ECHO_42_WEB;
    ChannelIDList[rLeft, 7]:= BACKWARD_ECHO_42_WEB;
    ChannelIDList[rLeft, 8]:= FORWARD_ECHO_42_BASE;
    ChannelIDList[rLeft, 9]:= BACKWARD_ECHO_42_BASE;
    ChannelIDList[rLeft, 10]:= FORWARD_WORK_ECHO_58;
    ChannelIDList[rLeft, 11]:= BACKWARD_WORK_ECHO_58;

    ChannelIDList[rRight, 0]:= MIRRORSHADOW_0;
    ChannelIDList[rRight, 1]:= ECHO_0;
    ChannelIDList[rRight, 2]:= FORWARD_WORK_ECHO_58;
    ChannelIDList[rRight, 3]:= BACKWARD_WORK_ECHO_58;
    ChannelIDList[rRight, 4]:= FORWARD_ECHO_70;
    ChannelIDList[rRight, 5]:= BACKWARD_ECHO_70;
    ChannelIDList[rRight, 6]:= FORWARD_ECHO_42_WEB;
    ChannelIDList[rRight, 7]:= BACKWARD_ECHO_42_WEB;
    ChannelIDList[rRight, 8]:= FORWARD_ECHO_42_BASE;
    ChannelIDList[rRight, 9]:= BACKWARD_ECHO_42_BASE;
    ChannelIDList[rRight, 10]:= FORWARD_UWORK_ECHO_58;
    ChannelIDList[rRight, 11]:= BACKWARD_UWORK_ECHO_58;

  // ------------------------------------------------------------------------------

    with EvalChannels2[FORWARD_WORK_ECHO_58] do   begin InfoSide:= isLeft;  InfoPos:= ipLeft;   end;
    with EvalChannels2[FORWARD_UWORK_ECHO_58] do  begin InfoSide:= isLeft;  InfoPos:= ipRight;  end;

    with EvalChannels2[BACKWARD_WORK_ECHO_58] do  begin InfoSide:= isRight; InfoPos:= ipLeft;   end;
    with EvalChannels2[BACKWARD_UWORK_ECHO_58] do begin InfoSide:= isRight; InfoPos:= ipRight;  end;

    with EvalChannels2[FORWARD_ECHO_70] do        begin InfoSide:= isLeft;  InfoPos:= ipCentre; end;
    with EvalChannels2[BACKWARD_ECHO_70] do       begin InfoSide:= isRight; InfoPos:= ipCentre; end;

    with EvalChannels2[FORWARD_ECHO_42_WEB] do    begin InfoSide:= isLeft;  InfoPos:= ipRight;  end;
    with EvalChannels2[FORWARD_ECHO_42_BASE] do   begin InfoSide:= isLeft;  InfoPos:= ipLeft;   end;
    with EvalChannels2[BACKWARD_ECHO_42_WEB] do   begin InfoSide:= isRight; InfoPos:= ipRight;  end;
    with EvalChannels2[BACKWARD_ECHO_42_BASE] do  begin InfoSide:= isRight; InfoPos:= ipLeft;   end;

    with EvalChannels2[ECHO_0] do                 begin InfoSide:= isRight;  InfoPos:= ipCentre; end;
    with EvalChannels2[MIRRORSHADOW_0] do         begin InfoSide:= isLeft; InfoPos:= ipCentre; end;

    // ------------ ���� ������ �-��������� ------------------

    SetLength(FBScanLines, 8);

    FBScanLines[0].R:= rLeft;
    FBScanLines[0].Use:= True;
    SetLength(FBScanLines[0].Items, 4);

    FBScanLines[0].Items[0].ScanChIdx:= 7;
    FBScanLines[0].Items[1].ScanChIdx:= 8;
    FBScanLines[0].Items[2].ScanChIdx:= 1;
    FBScanLines[0].Items[3].ScanChIdx:= 2;
    FBScanLines[1].R:= rLeft;
    FBScanLines[1].Use:= True;
    SetLength(FBScanLines[1].Items, 2);
    FBScanLines[1].Items[0].ScanChIdx:= 3;
    FBScanLines[1].Items[1].ScanChIdx:= 4;

    FBScanLines[2].R:= rLeft;
    FBScanLines[2].Use:= True;
    SetLength(FBScanLines[2].Items, 2);
    FBScanLines[2].Items[0].ScanChIdx:= 5;
    FBScanLines[2].Items[1].ScanChIdx:= 6;

    FBScanLines[3].R:= rLeft;
    FBScanLines[3].Use:= True;
    SetLength(FBScanLines[3].Items, 1);
    FBScanLines[3].Items[0].ScanChIdx:= 0;

    // ---------------------------------------------------------------------------------

    FBScanLines[4].R:= rRight;
    FBScanLines[4].Use:= True;
    SetLength(FBScanLines[4].Items, 4);
    FBScanLines[4].Items[0].ScanChIdx:= 1;
    FBScanLines[4].Items[1].ScanChIdx:= 2;
    FBScanLines[4].Items[2].ScanChIdx:= 7;
    FBScanLines[4].Items[3].ScanChIdx:= 8;

    FBScanLines[5].R:= rRight;
    FBScanLines[5].Use:= True;
    SetLength(FBScanLines[5].Items, 2);
    FBScanLines[5].Items[0].ScanChIdx:= 3;
    FBScanLines[5].Items[1].ScanChIdx:= 4;

    FBScanLines[6].R:= rRight;
    FBScanLines[6].Use:= True;
    SetLength(FBScanLines[6].Items, 2);
    FBScanLines[6].Items[0].ScanChIdx:= 5;
    FBScanLines[6].Items[1].ScanChIdx:= 6;

    FBScanLines[7].R:= rRight;
    FBScanLines[7].Use:= True;
    SetLength(FBscanLines[7].Items, 1);
    FBScanLines[7].Items[0].ScanChIdx:= 0;

    // ------------ ������ ������� �������� ------------

    AddHandScanChannels(FHandChannels);

    // ------------ ������ �-��������� � ������ ������ ------------

    SetLength(FScanChannels, 9);
    SetLength(FEvalChannels, 12);

    FScanChannels[0].Number:= 1;  //  0 ����
    FScanChannels[0].Position:= 50 + GEISMAR_Shift;
    FScanChannels[0].EnterAngle:= 0;
    FScanChannels[0].TurnAngle:= 0;
    FScanChannels[0].BScanGateMin:= 5;
    FScanChannels[0].BScanGateMax:= 70;
    FScanChannels[0].DelayMultiply:= 3;
    FScanChannels[0].Delta:= 0;
    FScanChannels[0].Gran:= _None;

      FEvalChannels[0].Number:= 0;     // 0 ���
      FEvalChannels[0].ScanChNum:= 1;
      FEvalChannels[0].Method:= imMirrorShadow;
      FEvalChannels[0].Zone:= izAll;
      FEvalChannels[0].ShortName:= 'SHORT_0_MIRRORSHADOW';
      FEvalChannels[0].FullName:= 'FULL_0_MIRRORSHADOW';
      FEvalChannels[0].DirColor:= + 1;

      FEvalChannels[1].Number:= 1;  // 0 ���
      FEvalChannels[1].ScanChNum:= 1;
      FEvalChannels[1].Method:= imEcho;
      FEvalChannels[1].Zone:= izAll;
      FEvalChannels[1].ShortName:= 'SHORT_0_ECHO';
      FEvalChannels[1].FullName:= 'FULL_0_ECHO';
      FEvalChannels[1].DirColor:= - 1;

    FScanChannels[1].Number:= 2;  // 58 ����������
    FScanChannels[1].EnterAngle:= 58;
    FScanChannels[1].TurnAngle:= 34;
    FScanChannels[1].Position:= - 25 - GEISMAR_Shift;
    FScanChannels[1].BScanGateMin:= 20;
    FScanChannels[1].BScanGateMax:= 180;
    FScanChannels[1].DelayMultiply:= 1;
    FScanChannels[1].Delta:= - 90;
    FScanChannels[1].Gran:= _UnWork_Left__Work_Right;

      FEvalChannels[2].Number:= 2;     // 58 ����������
      FEvalChannels[2].ScanChNum:= 2;
      FEvalChannels[2].Method:= imEcho;
      FEvalChannels[2].Zone:= izHead;
      FEvalChannels[2].ShortName:= 'SHORT_+58_ECHO_WORK';
      FEvalChannels[2].FullName:= 'FULL_+58_ECHO_WORK';
      FEvalChannels[2].DirColor:= + 1;

    FScanChannels[2].Number:= 3;  // 58 �����������
    FScanChannels[2].EnterAngle:= - 58;
    FScanChannels[2].TurnAngle:= 34;
    FScanChannels[2].Position:= - 75 - GEISMAR_Shift;
    FScanChannels[2].BScanGateMin:= 20;
    FScanChannels[2].BScanGateMax:= 180;
    FScanChannels[2].DelayMultiply:= 1;
    FScanChannels[2].Delta:= + 90;
    FScanChannels[2].Gran:= _UnWork_Left__Work_Right;

      FEvalChannels[3].Number:= 3;     // 58 �����������
      FEvalChannels[3].ScanChNum:= 3;
      FEvalChannels[3].Method:= imEcho;
      FEvalChannels[3].Zone:= izHead;
      FEvalChannels[3].ShortName:= 'SHORT_-58_ECHO_WORK';
      FEvalChannels[3].FullName:= 'FULL_-58_ECHO_WORK';
      FEvalChannels[3].DirColor:= - 1;

    FScanChannels[3].Number:= 4;  // 70 ���� ����������
    FScanChannels[3].EnterAngle:= 70;
    FScanChannels[3].TurnAngle:= 0;
    FScanChannels[3].Position:= 75 + GEISMAR_Shift;
    FScanChannels[3].BScanGateMin:= 5;
    FScanChannels[3].BScanGateMax:= 95;
    FScanChannels[3].DelayMultiply:= 1;
    FScanChannels[3].Delta:= - 80;
    FScanChannels[3].Gran:= _None;

      FEvalChannels[4].Number:= 4;     // 70 ����������
      FEvalChannels[4].ScanChNum:= 4;
      FEvalChannels[4].Method:= imEcho;
      FEvalChannels[4].Zone:= izHead;
      FEvalChannels[4].ShortName:= 'SHORT_+70_ECHO';
      FEvalChannels[4].FullName:= 'FULL_+70_ECHO';
      FEvalChannels[4].DirColor:= + 1;

    FScanChannels[4].Number:= 5;  // 70 ���� ����������
    FScanChannels[4].EnterAngle:= - 70;
    FScanChannels[4].TurnAngle:= 0;
    FScanChannels[4].Position:= 25 + GEISMAR_Shift;
    FScanChannels[4].BScanGateMin:= 5;
    FScanChannels[4].BScanGateMax:= 95;
    FScanChannels[4].DelayMultiply:= 1;
    FScanChannels[4].Delta:= 80;
    FScanChannels[4].Gran:= _None;

      FEvalChannels[5].Number:= 5;     // 70 �����������
      FEvalChannels[5].ScanChNum:= 5;
      FEvalChannels[5].Method:= imEcho;
      FEvalChannels[5].Zone:= izHead;
      FEvalChannels[5].ShortName:= 'SHORT_-70_ECHO';
      FEvalChannels[5].FullName:= 'FULL_-70_ECHO';
      FEvalChannels[5].DirColor:= - 1;

    FScanChannels[5].Number:= 6;  // 42 ���� ����������
    FScanChannels[5].EnterAngle:= 42;
    FScanChannels[5].TurnAngle:= 0;
    FScanChannels[5].Position:= - 50 - GEISMAR_Shift;
    FScanChannels[5].BScanGateMin:= 10;
    FScanChannels[5].BScanGateMax:= 163;
    FScanChannels[5].DelayMultiply:= 1;
    FScanChannels[5].Delta:= - 30;
    FScanChannels[5].Gran:= _None;

      FEvalChannels[6].Number:= 6;      // 42 ����������
      FEvalChannels[6].ScanChNum:= 6;
      FEvalChannels[6].Method:= imEcho;
      FEvalChannels[6].Zone:= izWeb;
      FEvalChannels[6].ShortName:= 'SHORT_+42_ECHO_NEAR';
      FEvalChannels[6].FullName:= 'FULL_+42_ECHO_NEAR';
      FEvalChannels[6].DirColor:= + 1;

      FEvalChannels[8].Number:= 8;      // 42 ����������
      FEvalChannels[8].ScanChNum:= 6;
      FEvalChannels[8].Method:= imEcho;
      FEvalChannels[8].Zone:= izBase;
      FEvalChannels[8].ShortName:= 'SHORT_+42_ECHO_FAR';
      FEvalChannels[8].FullName:= 'FULL_+42_ECHO_FAR';
      FEvalChannels[8].DirColor:= + 1;

    FScanChannels[6].Number:= 7;  // 42 ���� �����������
    FScanChannels[6].EnterAngle:= - 42;
    FScanChannels[6].TurnAngle:= 0;
    FScanChannels[6].Position:= - 50 - GEISMAR_Shift;
    FScanChannels[6].BScanGateMin:= 10;
    FScanChannels[6].BScanGateMax:= 163;
    FScanChannels[6].DelayMultiply:= 1;
    FScanChannels[6].Delta:= + 30;
    FScanChannels[6].Gran:= _None;

      FEvalChannels[7].Number:= 7;      // 42 �����������
      FEvalChannels[7].ScanChNum:= 7;
      FEvalChannels[7].Method:= imEcho;
      FEvalChannels[7].Zone:= izWeb;
      FEvalChannels[7].ShortName:= 'SHORT_-42_ECHO_NEAR';
      FEvalChannels[7].FullName:= 'FULL_-42_ECHO_NEAR';
      FEvalChannels[7].DirColor:= - 1;

      FEvalChannels[9].Number:= 9;      // 42 �����������
      FEvalChannels[9].ScanChNum:= 7;
      FEvalChannels[9].Method:= imEcho;
      FEvalChannels[9].Zone:= izBase;
      FEvalChannels[9].ShortName:= 'SHORT_-42_ECHO_FAR';
      FEvalChannels[9].FullName:= 'FULL_-42_ECHO_FAR';
      FEvalChannels[9].DirColor:= - 1;

    FScanChannels[7].Number:= 10;  // 58 ����������
    FScanChannels[7].EnterAngle:= 58;
    FScanChannels[7].TurnAngle:= - 34;
    FScanChannels[7].Position:= - 25 - GEISMAR_Shift;
    FScanChannels[7].BScanGateMin:= 20;
    FScanChannels[7].BScanGateMax:= 180;
    FScanChannels[7].DelayMultiply:= 1;
    FScanChannels[7].Delta:= - 90;
    FScanChannels[7].Gran:= _Work_Left__UnWork_Right;

      FEvalChannels[10].Number:= 10;    // 58 ����������
      FEvalChannels[10].ScanChNum:= 10;
      FEvalChannels[10].Method:= imEcho;
      FEvalChannels[10].Zone:= izHead;
      FEvalChannels[10].ShortName:= 'SHORT_+58_ECHO_NOWORK';
      FEvalChannels[10].FullName:= 'FULL_+58_ECHO_NOWORK';
      FEvalChannels[10].DirColor:= + 1;

    FScanChannels[8].Number:= 11;  // 58 �����������
    FScanChannels[8].EnterAngle:= - 58;
    FScanChannels[8].TurnAngle:= - 34;
    FScanChannels[8].Position:= - 75 - GEISMAR_Shift;
    FScanChannels[8].BScanGateMin:= 20;
    FScanChannels[8].BScanGateMax:= 180;
    FScanChannels[8].DelayMultiply:= 1;
    FScanChannels[8].Delta:= + 90;
    FScanChannels[8].Gran:= _Work_Left__UnWork_Right;

      FEvalChannels[11].Number:= 11;    // 58 �����������
      FEvalChannels[11].ScanChNum:= 11;
      FEvalChannels[11].Method:= imEcho;
      FEvalChannels[11].Zone:= izHead;
      FEvalChannels[11].ShortName:= 'SHORT_-58_ECHO_NOWORK';
      FEvalChannels[11].FullName:= 'FULL_-58_ECHO_NOWORK';
      FEvalChannels[11].DirColor:= - 1;

    for I:= 0 to High(FScanChannels) do
      FScanChannels[I].BScanDuration:= FScanChannels[I].BScanGateMax - FScanChannels[I].BScanGateMin + 1;

 end;
end;


procedure TDataFileConfig.AddHandScanChannels(var HandChannels: array of THandChannelItem);
begin
  SetLength(FHandChannels, 7);
  FHandChannels[0].Number:= 0;
  FHandChannels[0].BScanGateMin:= 3;
  FHandChannels[0].BScanGateMax:= 70;
  FHandChannels[0].DelayMultiply:= 3;
  FHandChannels[0].Method:= imMirrorShadow;
  FHandChannels[0].Angle:= 0;
  FHandChannels[0].ShortName:= 'SHORT_0_MIRRORSHADOW';

  FHandChannels[1].Number:= 1;
  FHandChannels[1].BScanGateMin:= 6;
  FHandChannels[1].BScanGateMax:= 175;
  FHandChannels[1].DelayMultiply:= 1;
  FHandChannels[1].Method:= imEcho;
  FHandChannels[1].Angle:= 45;
  FHandChannels[1].ShortName:= 'SHORT_0_ECHO';

  FHandChannels[2].Number:= 2;
  FHandChannels[2].BScanGateMin:= 6;
  FHandChannels[2].BScanGateMax:= 175;
  FHandChannels[2].DelayMultiply:= 1;
  FHandChannels[2].Method:= imEcho;
  FHandChannels[2].Angle:= 45;
  FHandChannels[2].ShortName:= 'SHORT_45_ECHO';

  FHandChannels[3].Number:= 3;
  FHandChannels[3].BScanGateMin:= 6;
  FHandChannels[3].BScanGateMax:= 175;
  FHandChannels[3].DelayMultiply:= 1;
  FHandChannels[3].Method:= imEcho;
  FHandChannels[3].Angle:= 50;
  FHandChannels[3].ShortName:= 'SHORT_50_ECHO';

  FHandChannels[4].Number:= 4;
  FHandChannels[4].BScanGateMin:= 6;
  FHandChannels[4].BScanGateMax:= 175;
  FHandChannels[4].DelayMultiply:= 1;
  FHandChannels[4].Method:= imEcho;
  FHandChannels[4].Angle:= 58;
  FHandChannels[4].ShortName:= 'SHORT_58_ECHO';

  FHandChannels[5].Number:= 5;
  FHandChannels[5].BScanGateMin:= 3;
  FHandChannels[5].BScanGateMax:= 90;
  FHandChannels[5].DelayMultiply:= 1;
  FHandChannels[5].Method:= imEcho;
  FHandChannels[5].Angle:= 65;
  FHandChannels[5].ShortName:= 'SHORT_65_ECHO';

  FHandChannels[6].Number:= 6;
  FHandChannels[6].BScanGateMin:= 3;
  FHandChannels[6].BScanGateMax:= 90;
  FHandChannels[6].DelayMultiply:= 1;
  FHandChannels[6].Method:= imEcho;
  FHandChannels[6].Angle:= 70;
  FHandChannels[6].ShortName:= 'SHORT_70_ECHO';
end;

destructor TDataFileConfig.Destroy();
begin
  SetLength(FEvalChannels, 0);
  SetLength(FScanChannels, 0);
  SetLength(FHandChannels, 0);
  SetLength(FBScanLines, 0);
end;

procedure TDataFileConfig.SetZeroProbeChannelsViewMode(Mode: T_DFC_ZeroProbeChannelsViewMode);
var
  I: Integer;

begin

  if CompareMem(@FDeviceID, @Avicon16Wheel, 8) or
     CompareMem(@FDeviceID, @Avicon14Wheel, 8) or
     CompareMem(@FDeviceID, @EGO_USWScheme1, 8) or
     CompareMem(@FDeviceID, @EGO_USWScheme2, 8) or
     CompareMem(@FDeviceID, @EGO_USWScheme3, 8) then
  begin
    for I:= 0 to High(FBScanLines) do
    case Mode of
      zpAll: FBScanLines[I].Use:= True;
      zpFirst: if (FScanChannels[FBScanLines[I].Items[0].ScanChIdx].Number in [10, 11]) then FBScanLines[I].Use:= False
                                                                                    else FBScanLines[I].Use:= True;
      zbSecond: if (FScanChannels[FBScanLines[I].Items[0].ScanChIdx].Number in [0, 1]) then FBScanLines[I].Use:= False
                                                                                   else FBScanLines[I].Use:= True;
    end;
  end;
end;

function TDataFileConfig.GetDeviceID: TAviconID; // ������������� �������
begin
  Result:= FDeviceID;
end;

function TDataFileConfig.GetDeviceVer: Byte; // ������
begin
  Result:= FDeviceVer;
end;

function TDataFileConfig.GetMinRail(): TRail;
begin
  Result:= FMinRail;
end;

function TDataFileConfig.GetMaxRail(): TRail;
begin
  Result:= FMaxRail;
end;

function TDataFileConfig.GetEvalChannelCount(): Integer;
begin
  Result:= Length(FEvalChannels);
end;

function TDataFileConfig.GetScanChannelCount(): Integer;
begin
  Result:= Length(FScanChannels);
end;

function TDataFileConfig.GetHandChannelCount(): Integer;
begin
  Result:= Length(FHandChannels);
end;

function TDataFileConfig.GetEvalChannelItemByIdx(Index: Integer): TEvalChannelItem;
begin
  Result:= FEvalChannels[Index];
end;

function TDataFileConfig.GetScanChannelItemByIdx(Index: Integer): TScanChannelItem;
begin
  Result:= FScanChannels[Index];
end;

function TDataFileConfig.GetHandChannelItemByIdx(Index: Integer): THandChannelItem;
begin
  Result:= FHandChannels[Index];
end;

function TDataFileConfig.GetBScanLinesCount: Integer;
var
  I: Integer;

begin
//  Result:= Length(FBScanLines);
  Result:= 0;
  for I:= 0 to High(FBScanLines) do
    if FBScanLines[I].Use then Result:= Result + 1;
end;

function TDataFileConfig.GetBScanLineItem(Index: Integer): TBScanLine;
var
  I, J: Integer;

begin
//  Result:= FBScanLines[Index];
  J:= 0;
  for I:= 0 to High(FBScanLines) do
    if FBScanLines[I].Use then
    begin
      if J = Index then Result:= FBScanLines[I];
      J:= J + 1;
    end;
end;
{
function TDataFileConfig.GetBScanLineItem(Lines, Item: Integer): TBScanLine;
begin
  Result:= FBScanLines[Lines].Items[Item];
end;
}
function TDataFileConfig.GetMinScanChNumber: Integer;
begin
  Result:= FScanChannels[0].Number;
end;

function TDataFileConfig.GetMaxScanChNumber: Integer;
{begin
  Result:= FScanChannels[High(FScanChannels)].Number;
}
var
  I, Max_: Integer;

begin
  if High(FScanChannels) <> - 1 then
  begin
    Max_:= 0;
    for I := 0 to High(FScanChannels) do Max_:= Max(Max_, FScanChannels[I].Number);
    Result:=  Max_;
  end else Result:=  - 1;
end;

function TDataFileConfig.GetMinEvalChNumber: Integer;
begin
  Result:= FEvalChannels[0].Number;
end;

function TDataFileConfig.GetMaxEvalChNumber: Integer;
var
  I, Max_: Integer;

begin
  if High(FEvalChannels) <> - 1 then
  begin
    Max_:= 0;
    for I := 0 to High(FEvalChannels) do Max_:= Max(Max_, FEvalChannels[I].Number);
    Result:=  Max_;
  end else Result:=  - 1;


//  if High(FEvalChannels) <> - 1 then Result:= FEvalChannels[High(FEvalChannels)].Number
//                                else Result:=  - 1;
end;

function TDataFileConfig.GetAmplValue(Code: Integer): Integer;
var
  I: Integer;

begin
  for I := 0 to Length(FAmplName) - 1 do
    if FAmplName[I].Code = Code then
    begin
      Result:= FAmplName[I].Value;
      Exit;
    end;
end;

function TDataFileConfig.GetMinAmplCode: Integer;
begin
  Result:= FAmplName[0].Code;
end;

function TDataFileConfig.GetMaxAmplCode: Integer;
begin
  Result:= FAmplName[Length(FAmplName) - 1].Code;
end;

{
function TDataFileConfig.GetEvalInfo(Index: Integer): TEvalInfo;
begin
  Result:= FEvalInfo[Index];
end;

function TDataFileConfig.GetEvalInfoCount: Integer;
begin
  Result:= Length(FEvalInfo);
end;
}
{
function TDataFileConfig.GetScanByEval(EvalChannelIndex: Integer): Integer;
begin
  Result:= 0;
end;
}

function TDataFileConfig.GetEvalsChIdxByScanChIdx(ScanChannelIndex: Integer): TIntegerDynArray;
var
  I: Integer;

begin
  SetLength(Result, 0);
  for I := 0 to High(FEvalChannels) do
    if FEvalChannels[I].ScanChNum = FScanChannels[ScanChannelIndex].Number then
    begin
      SetLength(Result, Length(Result) + 1);
      Result[High(Result)]:= I;
    end;
end;

function TDataFileConfig.GetEvalsChIdxByScanChNum(ScanChannelNum: Integer): TIntegerDynArray;
var
  I: Integer;

begin
  SetLength(Result, 0);
  for I := 0 to High(FEvalChannels) do
    if FEvalChannels[I].ScanChNum = ScanChannelNum then
    begin
      SetLength(Result, Length(Result) + 1);
      Result[High(Result)]:= I;
    end;
end;

function TDataFileConfig.GetSecondEvalByEval(EvalChannelIndex: Integer; var ResEvalChannelIndex: Integer): Boolean;
var
  I: Integer;

begin
  for I := 0 to Length(FEvalChannels) - 1 do
    if (FEvalChannels[I].ScanChNum = FEvalChannels[EvalChannelIndex].ScanChNum) and
       (I <> EvalChannelIndex) then
    begin
      ResEvalChannelIndex:= I;
      Result:= True;
      Exit;
    end;
  Result:= False;
end;

function TDataFileConfig.GetEvalChByNumber(Number: Integer): TEvalChannelItem;
var
  I: Integer;

begin
  for I := 0 to Length(FEvalChannels) - 1 do
    if FEvalChannels[I].Number = Number then
    begin
      Result:= FEvalChannels[I];
      Exit;
    end;
end;

function TDataFileConfig.GetScanChByNumber(Number: Integer): TScanChannelItem;
var
  I: Integer;

begin
  for I := 0 to Length(FScanChannels) - 1 do
    if FScanChannels[I].Number = Number then
    begin
      Result:= FScanChannels[I];
      Exit;
    end;
end;

function TDataFileConfig.GetHandChByNumber(Number: Integer): THandChannelItem;
var
  I: Integer;

begin
  for I := 0 to Length(FHandChannels) - 1 do
    if FHandChannels[I].Number = Number then
    begin
      Result:= FHandChannels[I];
      Exit;
    end;
end;

function TDataFileConfig.GetScanChByEvalChNum(EvalChNum: Integer): TScanChannelItem;
begin
  Result:= GetScanChByNumber(GetEvalChByNumber(EvalChNum).ScanChNum)
end;

// --------- TDataFileConfigList -----------------------------------------------

constructor TDataFileConfigList.Create(FileName: string);
begin
  SetLength(FList, Length(FList) + 1);
  FList[High(FList)]:= TDataFileConfig.Create(Avicon16Scheme1);  // 0

  SetLength(FList, Length(FList) + 1);
  FList[High(FList)]:= TDataFileConfig.Create(Avicon16Scheme2);  // 1

  SetLength(FList, Length(FList) + 1);
  FList[High(FList)]:= TDataFileConfig.Create(Avicon16Scheme3);  // 2

  SetLength(FList, Length(FList) + 1);
  FList[High(FList)]:= TDataFileConfig.Create(Avicon16Wheel);    // 3

  SetLength(FList, Length(FList) + 1);
  FList[High(FList)]:= TDataFileConfig.Create(Avicon11Scheme0);  // 4

  SetLength(FList, Length(FList) + 1);
  FList[High(FList)]:= TDataFileConfig.Create(Avicon15Scheme1);  // 5

  SetLength(FList, Length(FList) + 1);
  FList[High(FList)]:= TDataFileConfig.Create(Avicon15Scheme1, 2); // 6

  SetLength(FList, Length(FList) + 1);
  FList[High(FList)]:= TDataFileConfig.Create(AviconHandScan);   // 7

  SetLength(FList, Length(FList) + 1);
  FList[High(FList)]:= TDataFileConfig.Create(Avicon17);         // 8

  SetLength(FList, Length(FList) + 1);
  FList[High(FList)]:= TDataFileConfig.Create(Avicon14Wheel);    // 9

//  SetLength(FList, Length(FList) + 1);
//  FList[High(FList)]:= TDataFileConfig.Create(FilusX27Wheel);

  SetLength(FList, Length(FList) + 1);
  FList[High(FList)]:= TDataFileConfig.Create(Avicon14Scheme1);  // 10

  SetLength(FList, Length(FList) + 1);
  FList[High(FList)]:= TDataFileConfig.Create(MIGReg);           // 11

  SetLength(FList, Length(FList) + 1);
  FList[High(FList)]:= TDataFileConfig.Create(MIGView);          // 12

  SetLength(FList, Length(FList) + 1);
  FList[High(FList)]:= TDataFileConfig.Create(USK004R);          // 13

  SetLength(FList, Length(FList) + 1);
  FList[High(FList)]:= TDataFileConfig.Create(RKS_U_Scheme_5);          // 14

  SetLength(FList, Length(FList) + 1);
  FList[High(FList)]:= TDataFileConfig.Create(RKS_U_Scheme_8);          // 15

  SetLength(FList, Length(FList) + 1);
  FList[High(FList)]:= TDataFileConfig.Create(Avicon31Scheme1);          // 16
  SetLength(FList, Length(FList) + 1);
  FList[High(FList)]:= TDataFileConfig.Create(EGO_USWScheme1);  // 16

  SetLength(FList, Length(FList) + 1);
  FList[High(FList)]:= TDataFileConfig.Create(EGO_USWScheme2);  // 17

  SetLength(FList, Length(FList) + 1);
  FList[High(FList)]:= TDataFileConfig.Create(EGO_USWScheme3);  // 18

  end;

destructor TDataFileConfigList.Destroy();
begin
  SetLength(FList, 0);
end;

function TDataFileConfigList.GetItem(Index: Integer): TDataFileConfig;
begin
  Result:= FList[Index];
end;

function TDataFileConfigList.GetCount: Integer;
begin
  Result:= Length(FList);
end;

function TDataFileConfigList.GetItemByID(FDeviceID: TAviconID; DeviceVer: Integer): TDataFileConfig;
var
  I: Integer;

begin

  if CompareMem(@USK003R, @FDeviceID, 7) then FDeviceID:= USK004R; // ������� ID ��� ������ ������ USK

  for I:= 0 to GetCount - 1 do
    if (CompareMem(@FList[I].FDeviceID, @FDeviceID, 7) and (FList[I].DeviceVer = DeviceVer)) or
       (CompareMem(@FList[I].FDeviceID, @FDeviceID, 7) and (not (I = 5) and (not (I = 6)))) then
    begin
      Result:= FList[i];
      Exit;
    end;
  Result:= nil;
end;

end.
