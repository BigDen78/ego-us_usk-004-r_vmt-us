﻿// Использование: TAirBrush
//
// Для начала работы - создать экземпляр класса TAirBrush указав объект
// регистрации TAviconDataContainer и обработчик события краскоотметки
// TJobEvent. Модуль краскопульта начнет работу с текущей координаты
// объекта регистрации TAviconDataContainer
//
// Для окончания работы - разрушить экземпляр класса TAirBrush
//
// Ни какие другие методы класса не вызывать !

// Вопросы
// В каких каналах делать 2 Эхо ?
// Отладочная информация
// При использовании резиновых массивов поток зависает !
//
// Известные проблеммы
// 1. При загрузке данных выпадает координата. Для предотвращения этого загрузка выполняется так - последнеее значение координаты минус единица
// 2.

// ---- 2015 ----
//
// [+] Изменить все названия и суть DisCoord на SysCoord
// [+] Ввести зону поиска дефектов 53 по задержке
// [+] АСД
// [+] Новый формат файла в DataContainer и DataSource
// [+] Константы перенести в конфиг //!!! - константы которые нужно брать из конфига
// [+] MMToSysCrd(}FParams.MSMPBMinLen{)} - ?
// [+] Cтробы поиска дефектов по каналам
// [+] Новый краскоотметчик в описание формата файла
// [+] Чертеж - геометрия краскопульта
// [+] Определить моменты принятия решения - 1100 мм от центра ИС
// [+] Зона срабатывания АСД
// [+] Задержка срабатывания краскопульта
// [+] Проверить загрузку пораметров КП
// [+] Загрузка процессора в методе Tick - регулируется по загрузке данных
// [ ] Не верно отрабатывается зона КЭ - протестить на имитированных сигналах
// [ ] Проверить на файлах с ЗДН
// [+] Непонятные пустые пачки
// [T] Учитывать режим работы датчиков 0 град в краскопульте
// [ ] Неустойчевый ДС - при полном отсутствии ДС ?
// [ ] Хреново ловит некоторые пачки: горонтальные в 0 град
// [+] LOG срабатывания АСД
// [ ] ??? - вопрос к этому месту
// [ ] ПОД - Фигня при отображении зон пачек на В-разветке при вкл. сведении
// [ ] ПОД - Ручная отметка краскоотметчика
// [ ] ПОД - Отображение зон: Плохой ДС [+], Режим работы датчиков 0 град, Датчики металла [+], Выключение КП, зона КЭ [+], Выкл АСД
// [ ] ПОД - Добавить перевод - отметки: NOLANG
// [F] Проблемма в том что зоны неустойчивости ДС формируются позже формирования пачек
// [F] Переставить стробы поиска пачек на конца алгоритма в начало, там где загружаются сигналы
// [ ] Неустойчевый ДС игнорирует выбор прямого канала ?
// [ ] АСД игнорирует выбор прямого канала ?
// [ ] Проблеммы с соединением пачек в один блок

{$DEFINE LOG}
{ $ DEFINE ONLY_PB}
{ $ DEFINE NOCONSOLIDATION}
{ $ DEFINE _DEBUG}

unit AirBrushUnit;

{$M 163840,1048576}

interface

uses
  Classes, SyncObjs, Windows, Forms, AviconTypes, DataFileConfig, AviconDataContainer, Dialogs;

const
  ABStr = 2;         // Канал загрузки данных из TAviconDataContainer

var
  GCount: Integer = 0;

type

  TAirBrushParams = record                  //  | Номер строки в конфиг файле
    LoadStep: Integer;                      //  2 Шаг загрузки данных в обработку [мм]
    Slide_SensorPos_: Integer;              //  3 Положение датчика металла относительно центра ИС для системы скольжения [мм]
    Slide_AirBrushPos_: Integer;            //  4 Положение краскоотметчика относительно центра ИС для системы скольжения [мм]
    SensorWorkZone: Integer;                //  5 Протяженность зоны хранения данных дачика металла [мм]
    MaxLenBetweenABMember: Integer;         //  6 Максимальное расстояние между центрами пачек принимаемых за одну отметку [мм]
    MSMPBMinLen: Integer;                   //  7 Минимальная протяженнось проподания ДС принимаемого за пачку [мм]
{?} ECHOPBMaxGapLen: Integer;               //  8 Максимально допустимый разрыв в пачке эхо сигналов [ШДП]
    TwoEchoMinDelay: Integer;               //  9 Минимальная разница задержек пачек [мкс] (определения признака 2 Эхо)
    TwoEchoMaxDelay: Integer;               // 10 Максимальная разница задержек пачек [мкс] (определения признака 2 Эхо)
{?} DecisionMakingBoundary: Integer;        // 11 ? Граница принятия решения - от центра ИС
    TwoEchoZoneLen: Integer;                // 12 Протяженность зоны КЭ, в ней происходит принятие решения по признаку 2 Эхо
    MinCrdDelayRatio: Single;               // 13 Минимальный допустимый наклон пачки [мкс / мм]
    MaxCrdDelayRatio: Single;               // 14 Максимальный допустимый наклон пачки [мкс / мм]
    MaxAmplToLen: array [0..15] of Integer; // 15...30 Минимальная протяженность группы сигналов (мм) принимаемых за пачку в зависимости от максимальной амплитуды среди сигналов группы
    BSStateFilterLen: Integer;              // 31 Размерность фильтра колебаний состояния ДС [ШДП]
    BSStateParam: Integer;                  // 32 not use
    HeadZone1: Integer;                     // 33 ? Зона головки №1
    HeadZone2: Integer;                     // 34 ? Зона головки №2
    HeadZoneSize: Integer;                  // 35 ? Размер зоны головки
    Wheel_SensorPos_: Integer;              // 36 Положение датчика металла относительно центра ИС для КИС [мм]
    Wheel_AirBrushPos_: Integer;            // 37 Положение краскоотметчика относительно центра ИС для КИС [мм]
    EGOUSW_SensorPos_HeadA: Integer;        // 38 Положение датчика металла относительно центра ИС [мм] EGO USW работа в сторону A
    EGOUSW_AirBrushPos_HeadA: Integer;      // 39 Положение краскоотметчика относительно центра ИС [мм] EGO USW работа в сторону A
    EGOUSW_SensorPos_HeadB: Integer;        // 40 Положение датчика металла относительно центра ИС [мм] EGO USW работа в сторону B
    EGOUSW_AirBrushPos_HeadB: Integer;      // 41 Положение краскоотметчика относительно центра ИС [мм] EGO USW работа в сторону B
    AirBrush_Shift: Integer;                // 42 Корректировка момента срабатывания краскопульта [мм]
    UseChannel_0d: Boolean;                 // 43 Поиск пачек в канале 0°
    UseChannel_42d: Boolean;                // 44 Поиск пачек в канале 42°
    UseChannel_42dTwoEcho: Boolean;         // 45 Поиск пачек в канале 42°, режим 2 Эхо
    UseChannel_58d: Boolean;                // 46 Поиск пачек в канале 58°
    UseChannel_65d: Boolean;                // 47 Поиск пачек в канале 65°
    Channel_0d_PB_MinLen: Integer;          // 48 Минимальная протяженность группы сигналов принимаемых за пачку в канале 0° эхо [мм]
    Channel_42d_PB_MinLen: Integer;         // 49 Минимальная протяженность группы сигналов принимаемых за пачку в канале 42° [мм]
    Channel_58d_PB_MinLen: Integer;         // 50 Минимальная протяженность группы сигналов принимаемых за пачку в канале 58° [мм]
    Channel_65d_PB_MinLen: Integer;         // 51 Минимальная протяженность группы сигналов принимаемых за пачку в канале 65° [мм]
    BH_45d_MinDelay: Integer;               // 52 Начало зоны поиска дефекта 53 [мкс]
    BH_45d_MaxDelay: Integer;               // 53 Конец зоны поиска дефекта 53 [мкс]
    Channel_0d_StGate: Integer;             // 54 Строб поиска пачек в канале 0° [мкс/3]
    Channel_0d_EdGate: Integer;             // 55 Строб поиска пачек в канале 0° [мкс/3]
    Channel_42d_StGate: Integer;            // 56 Строб поиска пачек в канале 42° [мкс]
    Channel_42d_EdGate: Integer;            // 57 Строб поиска пачек в канале 42° [мкс]
    Channel_58d_StGate: Integer;            // 58 Строб поиска пачек в канале 58° [мкс]
    Channel_58d_EdGate: Integer;            // 59 Строб поиска пачек в канале 58° [мкс]
    Channel_65d_StGate: Integer;            // 60 Строб поиска пачек в канале 65° [мкс]
    Channel_65d_EdGate: Integer;            // 61 Строб поиска пачек в канале 65° [мкс]
    BSNoiseAnalyzeZoneLen: Integer;         // 62 Размер зоны поиска неустойчивого ДС [шдп] {100 }
    BSNoiseAnalyzeTreshold: Integer;        // 63 Порог поиска неустойчивого ДС (отсутствий (шт) на зону [п.62]) [шдп] {60 }
    BSNoiseAnalyzeZoneExLen: Integer;       // 64 Расширение найденной зоны неустойчивого ДС [шдп]200
    BSSearchTreshold: Single;               // 65 Порог поиска проподания ДС [% отсутствия ДС] {0.45}
    BSSearchGlueZone: Integer;              // 66 Размер зоны склеивания пачек канала 0° {12 шдп} then // !!! эта величина есть ниже
    AlarmEventLifeTime: Integer;            // 67 Время жизни данных АСД [мкс]
    TickNoDataSleepValue: Integer;          // 68 Параметр функции Sleep в методе Tick [мкс]
  end;

type

  TJobEvent = procedure(Rail: TRail; DisCoord, SysCoord: Integer) of object; //
  TJobEvent2 = procedure(Rail: TRail; DisCoord, SysCoord: Integer; RealCrd: Integer; ABItems: TABItemsList2) of object;
  TAlarmEvent = procedure(Rail: TRail; Channel: Integer; State: Boolean) of object; // CallBack функция АСД

  // ------ Буффер данных АСД ---------------

  TAlarmDataItemState = (aisWait, aisWork);

  TAlarmDataItem = record
    Rail: TRail;                // Нить
    EvalCh: Integer;            // Канал АСД
    RealDisCrd: Integer;        // Координата
    Time: DWord;                // Время
    State: TAlarmDataItemState; // Состояние: aisWait - ожидает координаты сработывание; aisWork - работает, ждет выключения
    Free: Boolean;              // Ячейка свободна
  end;

const
  AlarmDataListSize = 512;

type
  TAlarmDataList = array [0..AlarmDataListSize - 1] of TAlarmDataItem;


  // ------ Датчик металла ---------------

  TSensorItem = record   // Зона определяемая датчиком металла
    Free: Boolean;       // Флаг - элемент масива свободен
    StRealDisCrd: Integer;  // Координата события изменения состояния - Появился сигнал (лампочка погасла) Дисплейная координата [ШДП]
    EdRealDisCrd: Integer;  // Координата события изменения состояния - Пропал сигнал
  end;

  // -------------- Пачка ----------------

  TBScanRect = record        // Зона пачки на В-развертке
    StartDisCrd: Integer;
    EndDisCrd: Integer;
    StartDelay: Integer;     // [у.е.]
    EndDelay: Integer;       // [у.е.]
  end;

  TBPItem = record            // Пачка
    Free: Boolean;            // Флаг - элемент масива свободен
    ScanChNum: Integer;       // Номер канала
    BScanRect: TBScanRect;    // Зона пачки на В-развертке
    CentrDisCrd: Integer;     // Дисплейная координата центра пачки [ШДП]
    CentrRealDisCrd: Integer; // Пересчтинанная координата
    TwoEcho: Boolean;         // Есть признак - Два Эхо
    CentrSysCrd: Integer;
    CentrDel: Integer;
    Angle: Single;
    Debug_1: Integer;
  end;

  // ------ Отметка краскопульта ---------

  TABItem = record
    Free: Boolean;                       // Флаг - элемент масива свободен
    RealCrd: Integer;                    // Дисплейная координата центра отметки [ШДП]
    PBList: array [0..1024] of TBPItem;  // Пачки по которым составленная отметка
    PBCount: Integer;                    // Количество пачек
  end;

  // -------------- Поиск пачек (ЭХО каналы) ----------------

  TMaskItem = record                     // Элемент пачки
    DisCrd: Integer;                     // Координата
    Delay: Integer;                      // Задержка
    Ampl: Integer;                       // Амплитуда
  end;

  TMaskItemRec = record                  // Маска поиска пачки
    Del: Boolean;                        // Флаг - элемент масива свободен
//    TwoEcho_: Boolean;                   // Два Эхо - флаг
    TwoEchoCount: Integer;               // Два Эхо - количество сигналов (точек)
    Item: array of TMaskItem;            // Элементы пачки
    DebugIndex: Integer;
  end;

  // -------------- Поиск пачек (ЭТМ каналы) ------------------

  TBSSerachState = record
    Delay: Integer;                     // Задержка ДС
    DisCrd: Integer;                    // Дисплейная координата начала пачки (дырки)
    State: Boolean;                     // Флаг - наличие ДС
    BackMotion: Boolean;                // Флаг что точка найденна в зоне движения назад
  end;

  TNoiseInfoItem = record // Элемент массива поиска неустойчевого ДС
    StartDisCrd: Integer;
    EndDisCrd: Integer;
    State: Boolean;
  end;

const
  FBSNoiseInfoLen = 100;

type

  TAlarmResetInfo = record
    State: Boolean;
    Time: DWord;
  end;

  // ----------------------------------------------------------

  TAirBrush = class(TThread)
  private
    FLog: TextFile;
    FCfg: TDataFileConfig;
    FDataCont: TAviconDataContainer;

      // ------------- Параметры -------------------------

    FSearchStGate: array [0..13] of Integer;
    FSearchEdGate: array [0..13] of Integer;
    FUseChannel: array [0..13] of Boolean;
    FPBLen: array [0..13] of Integer;
    FParams: TAirBrushParams;

      // ------------- Загрузка данных -------------------

    FLastLoadDisCoord: Integer;       // Последнаая загруженная дисплейная координата
    FLastSysCoord: Integer;           // Последнаая системная координата - для отброда зон движения назад

      // ------------- Датчик металла -----------------------

    FSensor1List: array [TRail] of array of TSensorItem; // Зоны отмеченные датчиком металла
    FOldSensor1: array [TRail] of Boolean;               // Последнее значния датчика металла
//    FSensor1StSysCrd: array [TRail] of Integer;          // Координата последнего срабатывания датчика металла
    FSensor1StDisCrd: array [TRail] of Integer;          // Координата последнего срабатывания датчика металла

      // ------------- АСД -----------------------

    FAlarmData: TAlarmDataList; // Массив событий АСД ожидающих свою координату
    FAlarmEventData: array [rLeft..rRight, 0..13] of TAlarmResetInfo; // Массив событий АСД ожидающих окончания работы

      // ------------- Поиск пачек, пачки -------------------

    FMSChLink: array [0..15] of Integer;        // Связь номера канала сканирования (индекс масива) с номером ЗТМ канала оценки (значение) Возможные значения: -1 нет ЗТМ или >= 0 номер ЗТМ канала оценки)

    // Донный сигнал и дырки в нем
    FBSStateFilter: array [rLeft..rRight, 0..15] of array of Boolean; // Фильтр колебаний состояния ДС
    FBSStateLastDisCrd: array [rLeft..rRight, 0..15] of Integer; // Последняя координата ДС
                                // Определение зон с неустойчевым ДС
    FOldBSState: array [rLeft..rRight, 0..15] of Boolean; // Состояние ДС - для подсчета колебаний состояния ДС
    FOldBSCrd: array [rLeft..rRight, 0..15] of Integer; // Координата ДС - для подсчета колебаний состояния ДС
    FBSNoiseInfo: array [rLeft..rRight, 0..15, 0..FBSNoiseInfoLen] of TNoiseInfoItem; // Массив поиска неустойчевого ДС (циклический)
    FBSNoiseInfoStIdx: array [rLeft..rRight, 0..15] of Integer; // Начальный индекс элементов массива с данными
    FBSNoiseInfoEndIdx: array [rLeft..rRight, 0..15] of Integer; // Конечный индекс элементов массива с данными

    FStateCrd: array [rLeft..rRight, 0..15] of Integer;
    FStateCnt: array [rLeft..rRight, 0..15] of Integer;
    FStateExCnt: array [rLeft..rRight, 0..15] of Integer;
    FStateAbCnt: array [rLeft..rRight, 0..15] of Integer;

    FMask: array [rLeft..rRight, 1..15] of array of TMaskItemRec; // Маски поиска пачек в ЭХО и ЗЕРК каналах
    FBSSerachState: array [TRail, 0..15] of TBSSerachState; // Маски поиска пачек в ЭТМ каналах
    FBSBPItem: array [TRail, 0..15] of TBPItem; // Пачки в ЭТМ каналах

    FBSDelay: array [TRail, 0..15] of Integer; // Последняя задержка ДС
//    FBSState: array [TRail, 0..15] of Boolean; // Последнее состояние ДС
//    FBSDisCrd: array [TRail, 0..15] of Integer; // Координата последнего изменения донного сигнала

    FBPList: array [TRail] of array of TBPItem; // Массив пачек

      // ------ Отметка краскопульта ---------

    FABList: array [TRail] of array of TABItem;

      // ------------- Поток -------------------

    FStop: Boolean;
    FNeedStop: Boolean; // Флаг - необходимо выйти из цикла выполнения потока

      // ------------- Выгрузка данных -------------------

    FJobEvent: TJobEvent;
    FJobEvent2: TJobEvent2;
    FAlarmEvent: TAlarmEvent;
//    FFlg: Boolean; // Debug

    FStartAnalyzTime: DWord;
    FStopAnalyzTime: DWord;

    // --------------- Отладка -------------------------

    FGlobalBPCount: Integer;
    FGlobalMaskCount: Integer;
//    FGlobalCount: Integer;

    FSkipEvents: Boolean;

  protected
    function Sensor1Test(R: TRail; RealCrd: Integer): Boolean;   // Получение состояния для заданной координаты (учет координат происходит внутри)
    function SysCrdToMM(Crd: Integer): Single;                   // Пересчет ШДП в милиметры
    function MMToSysCrd(MM: Integer): Integer;                   // Пересчет милиметров в ШДП

    procedure AddSensor(R: TRail; StRealDisCrd, EdRealDisCrd: Integer);// Добавление отметок датчика металла
    procedure AddPB(R: TRail; ScanChNum: Integer; New: TBPItem); // Добавление пачки и расчет ее параметров
    function AddAB(R: TRail; New: TABItem): Integer;             // Добавление отметки краскопульта

    function BlockOK(StartDisCoord: Integer): Boolean;           // Загрузка данных
    procedure Execute; override;                                 // Метод потока
    procedure Tick;
  public

    SummAnalyzTime: Integer;
    SummWaitTime: Integer;
    SensorPos: Integer;
    AirBrushPos: Integer;
    CS: TCriticalSection;

    constructor Create(DataCont: TAviconDataContainer; JobEvent: TJobEvent; JobEvent2: TJobEvent2; ParamsFileName: string; CreateSuspended: Boolean; Priority: TThreadPriority);
    destructor Destroy; override;
    procedure Tick_;
    procedure Stop;
    function GetAllocSize: Integer;
    procedure SetZerroProbMode(Mode: TZerroProbMode);            // Режим работы датчиков 0 град
    property AlarmEvent: TAlarmEvent read FAlarmEvent write FAlarmEvent;
    property Params: TAirBrushParams read FParams;
    property SkipEvents: Boolean read FSkipEvents write FSkipEvents; // Отключение генерации событий JobEvent
  end;

implementation

uses
  Types, Math, SysUtils;

constructor TAirBrush.Create(DataCont: TAviconDataContainer; JobEvent: TJobEvent; JobEvent2: TJobEvent2; ParamsFileName: string; CreateSuspended: Boolean; Priority: TThreadPriority);
var
  R: TRail;
  I: Integer;
  ScanChNum: Integer;
  EvCh: Integer;
  EvalChIdx: Integer;
  ParamList: TStringList;
  ParamFileVer: Integer;
  Temp: TAviconID;

function GetParamText(Str: string): string;
begin
  Str:= Copy(Str, 1, 20);
  while Str[Length(Str)] = ' ' do SetLength(Str, Length(Str) - 1);
  Result:= Str;
end;

begin
  CS:= TCriticalSection.Create;

  FSkipEvents:= False;
  FGlobalBPCount:= 0;
  FGlobalMaskCount:= 0;
//  FGlobalCount:= 0;
  SummAnalyzTime:= 0;
  SummWaitTime:= 0;
  FStartAnalyzTime:= 0;
  FStopAnalyzTime:= 0;

  {$IFDEF LOG}
  AssignFile(FLog, ExtractFilePath(Application.ExeName) + 'AirBrush.LOG');
  Rewrite(FLog);
  {$ENDIF}

  // Список версий
  // http://forum.sources.ru/index.php?showtopic=340770
  {$IFDEF VER260} // Delphi XE5
  FormatSettings.DecimalSeparator:= '.';                   // Загрузка конфигурационного файла
  {$ENDIF}
  {$IFDEF VER210} // Delphi XE5
  DecimalSeparator:= '.';                   // Загрузка конфигурационного файла
  {$ENDIF}
  ParamList:= TStringList.Create;
  ParamList.LoadFromFile(ParamsFileName);

  ParamFileVer:=                     StrToInt(GetParamText(ParamList[0])); // Версия файла

  if ParamFileVer in [2, 3, 4, 5] then
  begin
    FParams.LoadStep:=               StrToInt(GetParamText(ParamList[1])); // Шаг загрузки данных в обработку [мм]
    FParams.Slide_SensorPos_:=       StrToInt(GetParamText(ParamList[2])); // Положение датчика металла относительно центра ИС [мм]
    FParams.Slide_AirBrushPos_:=     StrToInt(GetParamText(ParamList[3])); // Положение краскоотметчика относительно центра ИС [мм]
    FParams.SensorWorkZone:=         StrToInt(GetParamText(ParamList[4])); // Протяженность зоны хранения данных дачика металла [мм]
    FParams.MaxLenBetweenABMember:=  StrToInt(GetParamText(ParamList[5])); // ? Максимальное расстояние между центрами пачек принимаемых за одну отметку [мм]
    FParams.MSMPBMinLen:=            StrToInt(GetParamText(ParamList[6])); // Минимальная протяженнось проподания ДС принимаемого за пачку [мм]
    FParams.ECHOPBMaxGapLen:=        StrToInt(GetParamText(ParamList[7])); // Максимально допустимый разрыв в пачке эхо сигналов [ШДП]
    FParams.TwoEchoMinDelay:=        StrToInt(GetParamText(ParamList[8])); // Минимальная разница задержек пачек [мкс] (определения признака 2 Эхо)
    FParams.TwoEchoMaxDelay:=        StrToInt(GetParamText(ParamList[9])); // Максимальная разница задержек пачек [мкс] (определения признака 2 Эхо)
    FParams.DecisionMakingBoundary:= StrToInt(GetParamText(ParamList[10])); // Граница принятия решения о наличии дефекта- от центра ИС [мм]
    FParams.TwoEchoZoneLen:=         StrToInt(GetParamText(ParamList[11])); // Протяженность зоны конструктивного элемента, в которой принятие решения происходить по признаку 2 Эхо
    FParams.MinCrdDelayRatio:=       StrToFloat(GetParamText(ParamList[12])); // мкс / мм
    FParams.MaxCrdDelayRatio:=       StrToFloat(GetParamText(ParamList[13])); // мкс / мм
    FParams.MaxAmplToLen[0]:=        StrToInt(GetParamText(ParamList[14])); // -12 дБ | Минимальная протяженность группы сигналов (мм)
    FParams.MaxAmplToLen[1]:=        StrToInt(GetParamText(ParamList[15])); // -10 дБ | принимаемых за пачку в зависимости от максимальной
    FParams.MaxAmplToLen[2]:=        StrToInt(GetParamText(ParamList[16])); //  -8 дБ | амплитуды среди сигналов группы
    FParams.MaxAmplToLen[3]:=        StrToInt(GetParamText(ParamList[17])); //  -6 дБ
    FParams.MaxAmplToLen[4]:=        StrToInt(GetParamText(ParamList[18])); //  -4 дБ
    FParams.MaxAmplToLen[5]:=        StrToInt(GetParamText(ParamList[19])); //  -2 дБ
    FParams.MaxAmplToLen[6]:=        StrToInt(GetParamText(ParamList[20])); //   0 дБ
    FParams.MaxAmplToLen[7]:=        StrToInt(GetParamText(ParamList[21])); //   2 дБ
    FParams.MaxAmplToLen[8]:=        StrToInt(GetParamText(ParamList[22])); //   4 дБ
    FParams.MaxAmplToLen[9]:=        StrToInt(GetParamText(ParamList[23])); //   6 дБ
    FParams.MaxAmplToLen[10]:=       StrToInt(GetParamText(ParamList[24])); //   8 дБ
    FParams.MaxAmplToLen[11]:=       StrToInt(GetParamText(ParamList[25])); //  10 дБ
    FParams.MaxAmplToLen[12]:=       StrToInt(GetParamText(ParamList[26])); //  12 дБ
    FParams.MaxAmplToLen[13]:=       StrToInt(GetParamText(ParamList[27])); //  14 дБ
    FParams.MaxAmplToLen[14]:=       StrToInt(GetParamText(ParamList[28])); //  16 дБ
    FParams.MaxAmplToLen[15]:=       StrToInt(GetParamText(ParamList[29])); //  18 дБ
    FParams.BSStateFilterLen:=       StrToInt(GetParamText(ParamList[30])); // Размерность фильтра колебаний состояния ДС [ШДП]
    FParams.BSStateParam:=           StrToInt(GetParamText(ParamList[31]));
  end;

  if ParamFileVer in [3, 4, 5] then
  begin
    FParams.HeadZone1:=              StrToInt(GetParamText(ParamList[32])); // Зона головки №1
    FParams.HeadZone2:=              StrToInt(GetParamText(ParamList[33])); // Зона головки №2
    FParams.HeadZoneSize:=           StrToInt(GetParamText(ParamList[34])); // Размер зоны головки
  end;

  if ParamFileVer in [4, 5] then
  begin
    FParams.Wheel_SensorPos_:=       StrToInt(GetParamText(ParamList[35])); // Положение датчика металла относительно центра ИС для КИС [мм]
    FParams.Wheel_AirBrushPos_:=     StrToInt(GetParamText(ParamList[36])); // Положение краскоотметчика относительно центра ИС для КИС [мм]
  end;

  if ParamFileVer in [5] then
  begin
    FParams.EGOUSW_SensorPos_HeadA    := StrToInt(GetParamText(ParamList[37]));     // 38 Положение датчика металла относительно центра ИС [мм] EGO USW работа в сторону A
    FParams.EGOUSW_AirBrushPos_HeadA  := StrToInt(GetParamText(ParamList[38]));     // 39 Положение краскоотметчика относительно центра ИС [мм] EGO USW работа в сторону A
    FParams.EGOUSW_SensorPos_HeadB    := StrToInt(GetParamText(ParamList[39]));     // 40 Положение датчика металла относительно центра ИС [мм] EGO USW работа в сторону B
    FParams.EGOUSW_AirBrushPos_HeadB  := StrToInt(GetParamText(ParamList[40]));     // 41 Положение краскоотметчика относительно центра ИС [мм] EGO USW работа в сторону B
    FParams.AirBrush_Shift            := StrToInt(GetParamText(ParamList[41]));     // 42 Корректировка момента срабатывания краскопульта [мм]
    FParams.UseChannel_0d             := StrToInt(GetParamText(ParamList[42])) = 1; // 43 Поиск пачек в канале 0°
    FParams.UseChannel_42d            := StrToInt(GetParamText(ParamList[43])) = 1; // 44 Поиск пачек в канале 42°
    FParams.UseChannel_42dTwoEcho     := StrToInt(GetParamText(ParamList[44])) = 1; // 45 Поиск пачек в канале 42°, режим 2 Эхо
    FParams.UseChannel_58d            := StrToInt(GetParamText(ParamList[45])) = 1; // 46 Поиск пачек в канале 58°
    FParams.UseChannel_65d            := StrToInt(GetParamText(ParamList[46])) = 1; // 47 Поиск пачек в канале 65°
    FParams.Channel_0d_PB_MinLen      := StrToInt(GetParamText(ParamList[47]));     // 48 Минимальная протяженность группы сигналов принимаемых за пачку в канале 0° эхо [мм]
    FParams.Channel_42d_PB_MinLen     := StrToInt(GetParamText(ParamList[48]));     // 49 Минимальная протяженность группы сигналов принимаемых за пачку в канале 42° [мм]
    FParams.Channel_58d_PB_MinLen     := StrToInt(GetParamText(ParamList[49]));     // 50 Минимальная протяженность группы сигналов принимаемых за пачку в канале 58° [мм]
    FParams.Channel_65d_PB_MinLen     := StrToInt(GetParamText(ParamList[50]));     // 51 Минимальная протяженность группы сигналов принимаемых за пачку в канале 65° [мм]
    FParams.BH_45d_MinDelay           := StrToInt(GetParamText(ParamList[51]));     // 52 Начало зоны поиска дефекта 53 [мкс]
    FParams.BH_45d_MaxDelay           := StrToInt(GetParamText(ParamList[52]));     // 53 Конец зоны поиска дефекта 53 [мкс]
    FParams.Channel_0d_StGate         := StrToInt(GetParamText(ParamList[53]));     // 54 Строб (начало) поиска пачек в канале 0°
    FParams.Channel_0d_EdGate         := StrToInt(GetParamText(ParamList[54]));     // 55 Строб (конец) поиска пачек в канале 0°
    FParams.Channel_42d_StGate        := StrToInt(GetParamText(ParamList[55]));     // 56 Строб (начало) поиска пачек в канале 42°
    FParams.Channel_42d_EdGate        := StrToInt(GetParamText(ParamList[56]));     // 57 Строб (конец) поиска пачек в канале 42°
    FParams.Channel_58d_StGate        := StrToInt(GetParamText(ParamList[57]));     // 58 Строб (начало) поиска пачек в канале 58°
    FParams.Channel_58d_EdGate        := StrToInt(GetParamText(ParamList[58]));     // 59 Строб (конец) поиска пачек в канале 58°
    FParams.Channel_65d_StGate        := StrToInt(GetParamText(ParamList[59]));     // 60 Строб (начало) поиска пачек в канале 65°
    FParams.Channel_65d_EdGate        := StrToInt(GetParamText(ParamList[60]));     // 61 Строб (конец) поиска пачек в канале 65°
    FParams.BSNoiseAnalyzeZoneLen     := StrToInt(GetParamText(ParamList[61]));     // 62 Размер зоны поиска неустойчивого ДС [шдп]
    FParams.BSNoiseAnalyzeTreshold    := StrToInt(GetParamText(ParamList[62]));     // 63 Порог поиска неустойчивого ДС (отсутствий (шт) на зону [п.62]) [шдп]
    FParams.BSNoiseAnalyzeZoneExLen   := StrToInt(GetParamText(ParamList[63]));     // 64 Расширение найденной зоны неустойчивого ДС [шдп]
    FParams.BSSearchTreshold          := StrToFloat(GetParamText(ParamList[64]));   // 65 Порог поиска проподания ДС [% отсутствия ДС]
    FParams.BSSearchGlueZone          := StrToInt(GetParamText(ParamList[65]));     // 66 Размер зоны склеивания пачек канала 0° [шдп]
    FParams.AlarmEventLifeTime        := StrToInt(GetParamText(ParamList[66]));     // 67 Время жизни данных АСД [мкс]
    FParams.TickNoDataSleepValue      := StrToInt(GetParamText(ParamList[67]));     // 68 Параметр функции Sleep в методе Tick [мкс]
  end;

  ParamList.Free;

  // Стробы поиска пачек сигналов

  FSearchStGate[0]:= FParams.Channel_0d_StGate; // Строб поиска пачек в канале 0°
  FSearchEdGate[0]:= FParams.Channel_0d_EdGate;
  FSearchStGate[1]:= FParams.Channel_0d_StGate;
  FSearchEdGate[1]:= FParams.Channel_0d_EdGate;
  FSearchStGate[10]:= FParams.Channel_0d_StGate;
  FSearchEdGate[10]:= FParams.Channel_0d_EdGate;
  FSearchStGate[11]:= FParams.Channel_0d_StGate;
  FSearchEdGate[11]:= FParams.Channel_0d_EdGate;

  FSearchStGate[6]:= FParams.Channel_42d_StGate; // Строб поиска пачек в канале 42°
  FSearchEdGate[6]:= FParams.Channel_42d_EdGate;
  FSearchStGate[7]:= FParams.Channel_42d_StGate;
  FSearchEdGate[7]:= FParams.Channel_42d_EdGate;
  FSearchStGate[8]:= FParams.Channel_42d_StGate;
  FSearchEdGate[8]:= FParams.Channel_42d_EdGate;
  FSearchStGate[9]:= FParams.Channel_42d_StGate;
  FSearchEdGate[9]:= FParams.Channel_42d_EdGate;

  FSearchStGate[2]:= FParams.Channel_58d_StGate; // Строб поиска пачек в канале 58°
  FSearchEdGate[2]:= FParams.Channel_58d_EdGate;
  FSearchStGate[3]:= FParams.Channel_58d_StGate;
  FSearchEdGate[3]:= FParams.Channel_58d_EdGate;
  FSearchStGate[12]:= FParams.Channel_58d_StGate;
  FSearchEdGate[12]:= FParams.Channel_58d_EdGate;
  FSearchStGate[13]:= FParams.Channel_58d_StGate;
  FSearchEdGate[13]:= FParams.Channel_58d_EdGate;

  FSearchStGate[4]:= FParams.Channel_65d_StGate; // Строб поиска пачек в канале 65°
  FSearchEdGate[4]:= FParams.Channel_65d_EdGate; // Строб поиска пачек в канале 65°
  FSearchStGate[5]:= FParams.Channel_65d_StGate;
  FSearchEdGate[5]:= FParams.Channel_65d_EdGate;

  // Используемын каналы при поиске пачек сигналов

  FUseChannel[ 0]:= FParams.UseChannel_0d;   //  MIRRORSHADOW_0;
  FUseChannel[ 1]:= FParams.UseChannel_0d;   //  ECHO_0;
  FUseChannel[ 2]:= FParams.UseChannel_58d;  //  FORWARD_UWORK_ECHO_58;
  FUseChannel[ 3]:= FParams.UseChannel_58d;  //  BACKWARD_WORK_ECHO_58;
  FUseChannel[ 4]:= FParams.UseChannel_65d;  //  FORWARD_ECHO_65;
  FUseChannel[ 5]:= FParams.UseChannel_65d;  //  BACKWARD_ECHO_65;
  FUseChannel[ 6]:= FParams.UseChannel_42d;  //  FORWARD_ECHO_42_WEB;
  FUseChannel[ 7]:= FParams.UseChannel_42d;  //  BACKWARD_ECHO_42_WEB;
  FUseChannel[ 8]:= FParams.UseChannel_42d;  //  FORWARD_ECHO_42_BASE;
  FUseChannel[ 9]:= FParams.UseChannel_42d;  //  BACKWARD_ECHO_42_BASE;
  FUseChannel[10]:= FParams.UseChannel_0d;   //  MIRRORSHADOW_0;
  FUseChannel[11]:= FParams.UseChannel_0d;   //  ECHO_0;
  FUseChannel[12]:= FParams.UseChannel_58d;  //  FORWARD_WORK_ECHO_58;
  FUseChannel[13]:= FParams.UseChannel_58d;  //  BACKWARD_UWORK_ECHO_58;
// FParams.UseChannel_42dTwoEcho

  // Минимальные длины пачек по каналам

  FPBLen[ 0]:= FParams.Channel_0d_PB_MinLen;
  FPBLen[ 1]:= FParams.Channel_0d_PB_MinLen;
  FPBLen[ 2]:= FParams.Channel_58d_PB_MinLen;
  FPBLen[ 3]:= FParams.Channel_58d_PB_MinLen;
  FPBLen[ 4]:= FParams.Channel_65d_PB_MinLen;
  FPBLen[ 5]:= FParams.Channel_65d_PB_MinLen;
  FPBLen[ 6]:= FParams.Channel_42d_PB_MinLen;
  FPBLen[ 7]:= FParams.Channel_42d_PB_MinLen;
  FPBLen[ 8]:= FParams.Channel_42d_PB_MinLen;
  FPBLen[ 9]:= FParams.Channel_42d_PB_MinLen;
  FPBLen[10]:= FParams.Channel_0d_PB_MinLen;
  FPBLen[11]:= FParams.Channel_0d_PB_MinLen;
  FPBLen[12]:= FParams.Channel_58d_PB_MinLen;
  FPBLen[13]:= FParams.Channel_58d_PB_MinLen;


  Temp:= DataCont.Config.DeviceID;
  if (ParamFileVer >= 4) and
      (CompareMem(@Temp, @Avicon16Wheel, 8) or
       CompareMem(@Temp, @Avicon14Wheel, 8)) then
  begin                                   // Колеса
    SensorPos:= FParams.Wheel_SensorPos_;
    AirBrushPos:= FParams.Wheel_AirBrushPos_;
  end
  else
  if (ParamFileVer >= 4) and
      (CompareMem(@Temp, @Avicon16Scheme1, 8) or
       CompareMem(@Temp, @Avicon16Scheme2, 8) or
       CompareMem(@Temp, @Avicon16Scheme3, 8)) then
  begin                                   // Скольжение
    SensorPos:= FParams.Slide_SensorPos_;
    AirBrushPos:= FParams.Slide_AirBrushPos_;
  end
  else
  if (ParamFileVer >= 5) and
      (CompareMem(@Temp, @EGO_USWScheme1, 8) or
       CompareMem(@Temp, @EGO_USWScheme2, 8) or
       CompareMem(@Temp, @EGO_USWScheme3, 8)) then
  begin                                   // EGO USW
    if DataCont.Header.ControlDirection = cd_Tail_B_Head_A then
    begin
      SensorPos:= FParams.EGOUSW_SensorPos_HeadA;
      AirBrushPos:= FParams.EGOUSW_AirBrushPos_HeadA;
    end
    else
    begin
      SensorPos:= FParams.EGOUSW_SensorPos_HeadB;
      AirBrushPos:= FParams.EGOUSW_AirBrushPos_HeadB;
    end;
  end;

  FDataCont:= nil;
  FCfg:= nil;
  FJobEvent:= JobEvent;
  FJobEvent2:= JobEvent2;
  FAlarmEvent:= nil;
  if not Assigned(DataCont) then Exit;

  FDataCont:= DataCont;
  FCfg:= FDataCont.Config;

  FLastLoadDisCoord:= FDataCont.CurSaveDisCoord - 1; // Берем координаты начиная с которых будем работать
  FLastSysCoord:= FDataCont.CurSaveSysCoord;

                                 // Определяем связь номеров каналов сканирования с номерами ЗТМ каналов оценки
  for ScanChNum := FCfg.MinScanChNumber to FCfg.MaxScanChNumber do // "Обнуляем" массив - заносим значение - 1
    FMSChLink[ScanChNum] := - 1;

  for EvalChIdx := 0 to FCfg.EvalChannelCount - 1 do // Перебираем каналы оценки
    if (FCfg.EvalChannelByIdx[EvalChIdx].Method = imMirrorShadow) then // Если канал ЗТМ то ..
      FMSChLink[FCfg.EvalChannelByIdx[EvalChIdx].ScanChNum] := FCfg.EvalChannelByIdx[EvalChIdx].Number; // .. сохраняем его номер

                                 // Обнуление рабочих масивов
  for R:= FCfg.MinRail to FCfg.MaxRail do
  begin
    for I:= 0 to 15 do
    begin
      SetLength(FBSStateFilter[R, I], FParams.BSStateFilterLen);
      FBSStateLastDisCrd[R, I]:= 0;
      FBSSerachState[R, I].State:= False;
      FOldBSState[R, I]:= False;
      FOldBSCrd[R, I]:= 0;
      FStateCrd[R, I]:= 0;
      FStateCnt[R, I]:= 0;
      FStateExCnt[R, I]:= 0;
      FStateAbCnt[R, I]:= 0;
      FBSBPItem[R, I].Free:= True;
      FBSNoiseInfoStIdx[R, I]:= 0; // Начальный индекс элементов массива с данными
      FBSNoiseInfoEndIdx[R, I]:= 0; // Конечный индекс элементов массива с данными
    end;
    FOldSensor1[R]:= True;         // По умолчанию считаем что сиганала датчика металла есть
  end;

  FStop:= False;
  FNeedStop:= False;

  for R:= FCfg.MinRail to FCfg.MaxRail do
  begin
      for ScanChNum:= FCfg.MinScanChNumber to FCfg.MaxScanChNumber do
      begin
        FBSDelay[R, ScanChNum]:= 0;
///        FBSState[R, ScanChNum]:= True;
      end;
      for EvCh:= FCfg.MinEvalChNumber to FCfg.MaxEvalChNumber do
        FAlarmEventData[R, EvCh].State:= False;
  end;

  for I := 0 to AlarmDataListSize - 1 do
    FAlarmData[I].Free:= True;

  inherited Create(CreateSuspended);         // Создаем поток включенным
  self.Priority:= Priority;
end;

destructor TAirBrush.Destroy;
var
  R: TRail;
  ScanChNum: Integer;

begin
                                  // Остановка патока
//  FNeedStop:= True;
//  while not FStop do Sleep(0);
//  Terminate;
                                  // Удаление массивов
  for R:= FCfg.MinRail to FCfg.MaxRail do
  begin
    SetLength(FBPList[R], 0);
    for ScanChNum:= FCfg.MinScanChNumber to FCfg.MaxScanChNumber do
      SetLength(FMask[R, ScanChNum], 0);
    SetLength(FABList[R], 0);
  end;

  inherited Destroy;
  CS.Free;

  {$IFDEF LOG}
  CloseFile(FLog);
  {$ENDIF}
end;

procedure TAirBrush.Tick_;
begin
  Tick;
end;

procedure TAirBrush.Stop;
begin
                                  // Остановка потока
  FNeedStop:= True;
  //a[1]:= 3;
  while not FStop do Sleep(0);
  Terminate;
end;

function TAirBrush.GetAllocSize: Integer;
var
  I: Integer;
  Size: array [1..10] of Integer;

begin
  Size[1]:= (Length(FSensor1List[rLeft]) + Length(FSensor1List[rRight])) * SizeOf(TSensorItem);
  Size[2]:= 0;
  for I := 1 to 15 do
    Size[2]:= Size[2] + (Length(FMask[rLeft, I]) + Length(FMask[rRight, I])) * SizeOf(TMaskItemRec);
  Size[3]:= (Length(FBPList[rLeft]) + Length(FBPList[rRight])) * SizeOf(TBPItem);
  Size[4]:= (Length(FABList[rLeft]) + Length(FABList[rRight])) * SizeOf(TABItem);
  Result:= Size[1] + Size[2] +  Size[3] +  Size[4];
end;

function TAirBrush.SysCrdToMM(Crd: Integer): Single; // Пересчет ШДП в милиметры
begin
  Result:= Crd * FDataCont.Header.ScanStep / 100;
end;

function TAirBrush.MMToSysCrd(MM: Integer): Integer; // Пересчет милиметров в ШДП
begin
  Result:= Round(MM / (FDataCont.Header.ScanStep / 100));
end;

procedure TAirBrush.AddSensor(R: TRail; StRealDisCrd, EdRealDisCrd: Integer); // Добавление отметок датчика металла
var
  I: Integer;

begin
  {$IFDEF LOG}
//  Writeln(FLog, Format('AddSensor: R = %d; St = %d; Ed = %d', [Ord(R), StRealCrd, EdRealCrd]));
  {$ENDIF}

  for I:= 0 to Length(FSensor1List[R]) - 1 do   // Ищем свободный элемент массива и заносим данные в него
    if FSensor1List[R, I].Free then
    begin
      FSensor1List[R, I].StRealDisCrd:= StRealDisCrd; // Заносим данные
      FSensor1List[R, I].EdRealDisCrd:= EdRealDisCrd;
      FSensor1List[R, I].Free:= False;          // Устанавливаем флаг - элемент занят
      Exit;
    end;
                                                // Если свободных элементов массива нет - добавляем новый элемент
  I:= Length(FSensor1List[R]);
  SetLength(FSensor1List[R], I + 1);
  FSensor1List[R, I].StRealDisCrd:= StRealDisCrd;     // Заносим данные
  FSensor1List[R, I].EdRealDisCrd:= EdRealDisCrd;
  FSensor1List[R, I].Free:= False;              // Устанавливаем флаг - элемент занят
end;

procedure TAirBrush.AddPB(R: TRail; ScanChNum: Integer; New: TBPItem); // Добавление пачки
var
  I, EvalCh, EvalCh_In, EvalCh_Out, StStr, EndStr: Integer;

begin

                                                     // Определяем координату пачки
  New.CentrDisCrd:= (New.BScanRect.StartDisCrd + New.BScanRect.EndDisCrd) div 2;
  New.CentrDel:= (New.BScanRect.StartDelay + New.BScanRect.EndDelay) div 2;
  New.CentrRealDisCrd:= Round(New.CentrDisCrd + GetRealCrd_inSysCrd(FCfg.ScanChannelByNum[ScanChNum],
                                                                    FDataCont.Header.ScanStep,
                                                                    New.CentrDel, FDataCont.isEGOUSW_BHead));

  // Обработка АСД - внесение данных в буфер

  // Получение канала оценки из канала сканирования
  if ScanChNum = 1 then
  begin
    StStr:= 0;
    EndStr:= FDataCont.CurSaveParams[R, 1].EndStr;
    EvalCh_In:= 1;
    EvalCh_Out:= 0;
  end else
  if ScanChNum = 11 then
  begin
    StStr:= 0;
    EndStr:= FDataCont.CurSaveParams[R, 11].EndStr;
    EvalCh_In:= 11;
    EvalCh_Out:= 10;
  end else
  if ScanChNum = 6 then
  begin
    StStr:= 0;
    EndStr:= FDataCont.CurSaveParams[R, 6].EndStr;
    EvalCh_In:= 6;
    EvalCh_Out:= 7;
  end else
  if ScanChNum = 7 then
  begin
    StStr:= 0;
    EndStr:= FDataCont.CurSaveParams[R, 7].EndStr;
    EvalCh_In:= 7;
    EvalCh_Out:= 8;
  end
  else EvalCh:= - 1;

  if EvalCh = - 1 then EvalCh:= ScanChNum else
  begin
    if (New.CentrDel >= StStr) and (New.CentrDel <= EndStr) then EvalCh:= EvalCh_In
    else EvalCh:= EvalCh_Out;
  end;

  // Внесение данных в буфер ACД
 {
  for I := 0 to AlarmDataListSize - 1 do
    if FAlarmData[I].Free then
    begin
      FAlarmData[I].Rail:= R;
      FAlarmData[I].EvalCh:= EvalCh;
      FAlarmData[I].RealDisCrd:= New.CentrRealDisCrd;
      FAlarmData[I].Free:= False;
      Break;
    end;
     }
  for I:= 0 to Length(FBPList[R]) - 1 do             // Ищем свободный элемент массива и заносим данные в него
    if FBPList[R, I].Free then
    begin
      FBPList[R, I]:= New;                           // Заносим данные
      FBPList[R, I].Free:= False;                    // Устанавливаем флаг - элемент занят
      Exit;
    end;
  I:= Length(FBPList[R]);                            // Если свободных элементов массива нет - добавляем новый элемент
  SetLength(FBPList[R], I + 1);
  FBPList[R, I]:= New;                               // Заносим данные
  FBPList[R, I].Free:= False;                        // Устанавливаем флаг - элемент занят
end;

function TAirBrush.AddAB(R: TRail; New: TABItem): Integer;  // Добавление отметки краскопульта
var
  I: Integer;

begin
  for I:= 0 to Length(FABList[R]) - 1 do            // Ищем свободный элемент массива и заносим данные в него
    if FABList[R, I].Free then
    begin
      FABList[R, I]:= New;                          // Заносим данные
      FABList[R, I].Free:= False;                   // Устанавливаем флаг - элемент занят
      Result:= I;
      Exit;
    end;
  I:= Length(FABList[R]);                           // Если свободных элементов массива нет - добавляем новый элемент
  SetLength(FABList[R], I + 1);
  FABList[R, I]:= New;                              // Заносим данные
  FABList[R, I].Free:= False;                       // Устанавливаем флаг - элемент занят
  Result:= I;
end;

procedure TAirBrush.SetZerroProbMode(Mode: TZerroProbMode);            // Режим работы датчиков 0 град
begin
  //
end;

function TAirBrush.BlockOK(StartDisCoord: Integer): Boolean; // ---- Поиск пачек ----------
type
  TEchoDataItem = record
    MaskIdx: Integer;
    Value: Integer;
  end;

const
  Mask: array [1..3, 1..10] of TPoint = (((X: 1; Y:   0),   // Прямой
                                          (X: 1; Y:   1),
                                          (X: 1; Y: - 1),
                                          (X: 1; Y:   2),
                                          (X: 1; Y: - 2),
                                          (X: 2; Y:   0),
                                          (X: 2; Y:   1),
                                          (X: 2; Y: - 1),
                                          (X: 2; Y:   2),
                                          (X: 2; Y: - 2)),

                                         ((X: 1; Y: - 1),   // Наезжающие
                                          (X: 1; Y:   0),
                                          (X: 0; Y: - 1),
                                          (X: 2; Y: - 2),
                                          (X: 2; Y: - 1),
                                          (X: 1; Y: - 2),
                                          (X: 2; Y:   0),
                                          (X: 0; Y:   2),
                                          (X: 3; Y: - 3),
                                          (X: 0; Y:   0)),

                                         ((X: 1; Y:   1),   // Отъезжающие
                                          (X: 1; Y:   0),
                                          (X: 0; Y:   1),
                                          (X: 2; Y:   2),
                                          (X: 2; Y:   1),
                                          (X: 1; Y:   2),
                                          (X: 2; Y:   0),
                                          (X: 0; Y:   2),
                                          (X: 3; Y:   3),
                                          (X: 0; Y:   0)));

var
  R: TRail;
  ScanChNum: Integer;
  EchoIdx: Integer;
  MaskIdx: Integer;
  Cnt, Idx: Integer;
  K, L, M, ML, Q: Integer;
  BSNoiseFlag: Boolean;
  MaxAmpl: Integer;
  CurEcho: AviconTypes.TCurEcho; // Сигналы текущей координаты
  EchoData: array [1..16] of TEchoDataItem;
  SkipEchoIdx: array [1..16] of Boolean; // Флаги для отметки сигналов принятых как донный сигнал
  BSStateCnt: Integer;        // Количество ШДП где есть ДС
  MIdx: Integer;
  CrdLenMM: Single;
  DlyLen: Single;
  DlyCentr: Single;
  New: TBPItem;
  HeadFlag: Boolean;
  BackMotion: Boolean;
  CentrSysCrd: Integer;

begin

  // Кривизна - При загрузке данных выпадает координата. Для предотвращения этого загрузка выполняется так -
  // последнеее значение координаты минус единица. Но тогда происходят зоны движения назад что то же плохо
  // Поэтому здест стоит проверка которая пропускает уже загруженные координаты
  if FDataCont.CurLoadDisCoord[ABStr] <= FLastLoadDisCoord then
  begin
    Result:= True;
    Exit;
  end;

  BackMotion:= FDataCont.CurLoadSysCoord[ABStr] < FLastSysCoord;
//  if FDataCont.CurLoadSysCoord[ABStr] > FLastSysCoord then // Берем только зоны движения вперед
//  if FDataCont.CurLoadSysCoord[ABStr] > 500 then // DEBUG
  begin

    CurEcho:= FDataCont.CurLoadEcho[ABStr];                // Получаем сигналы текущей координаты
    for R:= FCfg.MinRail to FCfg.MaxRail do // NODEBUG
//    for R:= FCfg.MinRail to FCfg.MinRail do // DEBUG
    begin

      // -------------------------------------------------------------------------------------------------------------------

      // Формируем зоны - ищем вначале начало зоны, затем конец зоны

      if FOldSensor1[R] and (not FDataCont.CurLoadSensor1[ABStr][R]) then // Произошло изменение - Из True -> False = конец
      begin
        FSensor1StDisCrd[R]:= FDataCont.CurLoadDisCoord[ABStr];
        FOldSensor1[R]:= False;
      end;
      if (not FOldSensor1[R]) and FDataCont.CurLoadSensor1[ABStr][R] then // Произошло изменение - Из False -> True = начало
      begin
        AddSensor(R,
                  - MMToSysCrd(SensorPos) + FSensor1StDisCrd[R]             ,
                  - MMToSysCrd(SensorPos) + FDataCont.CurLoadDisCoord[ABStr]); // Зона сформированна

        // Добавляется протяженная метка - зона КЭ
        FDataCont.AddLongLabel(R, 255, 1, 2,
                               - MMToSysCrd(SensorPos) + FSensor1StDisCrd[R] - MMToSysCrd(FParams.TwoEchoZoneLen) div 2,
                               - MMToSysCrd(SensorPos) + FDataCont.CurLoadDisCoord[ABStr] + MMToSysCrd(FParams.TwoEchoZoneLen) div 2,
                               0, 0, 0);
        FOldSensor1[R]:= True;
      end;

       // -------------------------------------------------------------------------------------

      for ScanChNum:= FCfg.MinScanChNumber to FCfg.MaxScanChNumber do
//      if (R = FCfg.MinRail) and (ScanChNum in [{0,  1, 2, 3, }12 {, 5, 6}]) then // DEBUG
      begin


                                              // Анализ сигналов - на предмет попадания в фомируемые пачки
        if CurEcho[R, ScanChNum].Count <> 0 then
        begin

          // ----------------------------- Поиск пачек в каналах ЗМТ, без учета зон движения назад ----------------------------------------

//          BSEchoIdx:= - 1;                    // Пометка сигналов попадающих в ЗТМ канал. Пока сигналов ЗТМ нет
          for EchoIdx:= 1 to 16 do SkipEchoIdx[EchoIdx]:= False;

          if (FMSChLink[ScanChNum] <> - 1) and // Если это канал с ЗТМ - то ищем донный сигнал
             ((FDataCont.CurLoadZPMode[ABStr] = zpmBoth) or   // Учет режима работы датчиков 0 град
              ((FDataCont.CurLoadZPMode[ABStr] = zpmOnlyA) and (ScanChNum = 1)) or
              ((FDataCont.CurLoadZPMode[ABStr] = zpmOnlyB) and (ScanChNum = 11))) then
//          if ScanChNum = 1 then // DEBUG Отладка - берем только один ДС
//          if ScanChNum = 11 then // DEBUG Отладка - берем только один ДС
          begin

           // -------- Определение наличия ДС и его параметров --------

            // -------- Текущий ДС --------

            MaxAmpl:= - 1; // Амплитуда максимального сигнала в стробе ЗТМ + флаг что есть сигнал в стробе ЗТМ
            with CurEcho[R, ScanChNum] do
              for EchoIdx:= 1 to Count do           // Просматриваем сигналы для текущей координаты
              // выбрасывание сигналов за границей строба В-развертки
              if (Delay[EchoIdx] / FCfg.ScanChannelByNum[ScanChNum].DelayMultiply >= FCfg.ScanChannelByNum[ScanChNum].BScanGateMin) and
                 (Delay[EchoIdx] / FCfg.ScanChannelByNum[ScanChNum].DelayMultiply <= FCfg.ScanChannelByNum[ScanChNum].BScanGateMax) then
              begin
                                                    // Отбираем сигналы попадающие в строб канала ЗТМ
                if (Delay[EchoIdx] / FCfg.ScanChannelByNum[ScanChNum].DelayMultiply >= FDataCont.CurLoadParams[ABStr][R, FMSChLink[ScanChNum]].StStr) and
                   (Delay[EchoIdx] / FCfg.ScanChannelByNum[ScanChNum].DelayMultiply <= FDataCont.CurLoadParams[ABStr][R, FMSChLink[ScanChNum]].EndStr) then
                begin                               // Сигнал в стробе ЗТМ канала
                  if Ampl[EchoIdx] > MaxAmpl then   // Выбираем сигнал с большей амплитудой
                  begin
                    MaxAmpl:= Ampl[EchoIdx];
                    FBSDelay[R, ScanChNum]:= Delay[EchoIdx]; // Запоминаем сигнал
                  end;
                end;
                                                    // Пометка (для дальнейшего отбрасывания) сигналов задержка которых больше или равна началу строба ЗТМ канала
                if (Delay[EchoIdx] / FCfg.ScanChannelByNum[ScanChNum].DelayMultiply >= FDataCont.CurLoadParams[ABStr][R, FMSChLink[ScanChNum]].StStr) then
                  SkipEchoIdx[EchoIdx]:= True;
              end;

              {$IFDEF LOG}    //XXX
              Writeln(FLog, Format('%d', [FDataCont.CurLoadSysCoord[ABStr]]));
              Writeln(FLog, Format('%d', [Ord(MaxAmpl <> - 1)]));
              {$ENDIF}


             // -------- Заполнение массива фильтрации ДС --------

                                                   // Сдвигаем отсчеты буфера фильтра - на столько на сколько с прошлого раза изменилась координата
              if FBSStateLastDisCrd[R, ScanChNum] <> - 1 then // Если есть что сдвигать
                for Cnt := FBSStateLastDisCrd[R, ScanChNum] + 1 to FDataCont.CurLoadDisCoord[ABStr] do
                begin                              // Сдвиг на 1 шаг
                  for Idx := 1 to FParams.BSStateFilterLen - 1 do // Сдвиг
                    FBSStateFilter[R, ScanChNum, Idx - 1]:= FBSStateFilter[R, ScanChNum, Idx];
                  Idx := FParams.BSStateFilterLen - 1;            // Внесение нового значения
                  if Cnt <> FDataCont.CurLoadDisCoord[ABStr] then
                  begin
                    FBSStateFilter[R, ScanChNum, Idx]:= False;           // Скачек кординаты - ДС нет
                    FStateAbCnt[R, ScanChNum]:= FStateAbCnt[R, ScanChNum] + 1; // Подсчет протяженности без ДС
                  end
                  else
                  begin
                    FBSStateFilter[R, ScanChNum, Idx]:= MaxAmpl <> - 1; // Флаг наличия донного сигнала - Значение для загруженной координаты
                    if MaxAmpl <> - 1 then FStateExCnt[R, ScanChNum]:= FStateExCnt[R, ScanChNum] + 1  // Подсчет протяженности с ДС
                                      else FStateAbCnt[R, ScanChNum]:= FStateAbCnt[R, ScanChNum] + 1; // Подсчет протяженности без ДС
                  end;
                end;
              FBSStateLastDisCrd[R, ScanChNum]:= FDataCont.CurLoadDisCoord[ABStr]; // Последняя координата ДС


               // Подсчет колибаний ДС - (поиск зон неустойчивого ДС)
              if FOldBSState[R, ScanChNum] <> (MaxAmpl <> - 1) then // Есть смена состояния ДС
              begin
                FOldBSState[R, ScanChNum]:= (MaxAmpl <> - 1);  // Запоминаем новое состояние ДС
                FStateCnt[R, ScanChNum]:= FStateCnt[R, ScanChNum] + 1; // Увеличиваем счетчик количества смены ДС
              end;
              if FDataCont.CurLoadDisCoord[ABStr] - FOldBSCrd[R, ScanChNum] > 1  then // Скачек координаты
                FStateCnt[R, ScanChNum]:= FStateCnt[R, ScanChNum] + 1; // Увеличиваем счетчик количества смены ДС
              FOldBSCrd[R, ScanChNum]:= FDataCont.CurLoadDisCoord[ABStr]; // Последняя координата ДС

//              if MaxAmpl <> - 1 then FStateExCnt[R, ScanChNum]:= FStateExCnt[R, ScanChNum] + 1  // Подсчет протяженности с ДС
//                                else FStateAbCnt[R, ScanChNum]:= FStateAbCnt[R, ScanChNum] + 1; // Подсчет протяженности без ДС

              if FDataCont.CurLoadDisCoord[ABStr] - FStateCrd[R, ScanChNum] > FParams.BSNoiseAnalyzeZoneLen then
              begin
                                // Определение зоны неустойчивого ДС

                BSNoiseFlag:= {(FStateCnt[R, ScanChNum] > 51) or} (FStateAbCnt[R, ScanChNum] > FParams.BSNoiseAnalyzeTreshold);

                if BSNoiseFlag then
                begin
                  // Состояние запоминаеся в цикличиском массиве на FBSNoiseInfoLen
                  FBSNoiseInfo[R, ScanChNum, FBSNoiseInfoEndIdx[R, ScanChNum]].StartDisCrd:= FStateCrd[R, ScanChNum] - FParams.BSNoiseAnalyzeZoneExLen div 2;
                  FBSNoiseInfo[R, ScanChNum, FBSNoiseInfoEndIdx[R, ScanChNum]].EndDisCrd:= FDataCont.CurLoadDisCoord[ABStr] + FParams.BSNoiseAnalyzeZoneExLen div 2;
                  FBSNoiseInfo[R, ScanChNum, FBSNoiseInfoEndIdx[R, ScanChNum]].State:= BSNoiseFlag;
                  // Изменение индексов
                  Inc(FBSNoiseInfoEndIdx[R, ScanChNum]);
                  if FBSNoiseInfoEndIdx[R, ScanChNum] = FBSNoiseInfoLen then FBSNoiseInfoEndIdx[R, ScanChNum]:= 0;
                  if FBSNoiseInfoEndIdx[R, ScanChNum] = FBSNoiseInfoStIdx[R, ScanChNum] then
                    FBSNoiseInfoStIdx[R, ScanChNum] := FBSNoiseInfoEndIdx[R, ScanChNum] + 1;
                  if FBSNoiseInfoStIdx[R, ScanChNum] = FBSNoiseInfoLen then FBSNoiseInfoStIdx[R, ScanChNum]:= 0;

                  // Добавляется протяженная метка - неустойчевый ДС
                  //if BSNoiseFlag then

                  if  ((FDataCont.CurLoadZPMode[ABStr] = zpmBoth) and ((ScanChNum = 1) or (ScanChNum = 11)) ) or   // Учет режима работы датчиков 0 град
                      ((FDataCont.CurLoadZPMode[ABStr] = zpmOnlyA) and (ScanChNum = 1)) or
                      ((FDataCont.CurLoadZPMode[ABStr] = zpmOnlyB) and (ScanChNum = 11)) then
                    FDataCont.AddLongLabel(R, 0, ScanChNum, 1, FStateCrd[R, ScanChNum], FDataCont.CurLoadDisCoord[ABStr], 0, 0, 0);
                end;

                FStateCrd[R, ScanChNum]:= FDataCont.CurLoadDisCoord[ABStr];
                FStateCnt[R, ScanChNum]:= 0;
                FStateExCnt[R, ScanChNum]:= 0;
                FStateAbCnt[R, ScanChNum]:= 0;
              end;

             // -------- Фильтрация колебаний сотояние ДС --------

              if FDataCont.CurLoadDisCoord[ABStr] > FParams.BSStateFilterLen then // Ищим дырки когда заполнится буффер
              begin

                BSStateCnt:= 0;
                for Idx := 0 to FParams.BSStateFilterLen - 1 do //
                  if FBSStateFilter[R, ScanChNum, Idx] then BSStateCnt:= BSStateCnt + 1;

                if BSStateCnt / FParams.BSStateFilterLen < FParams.BSSearchTreshold then // дырка в ДС
                begin
                  if not FBSSerachState[R, ScanChNum].State then // Пачка началась ?
                  begin                                          // Да пачка началась - запоминаем параметры
                    FBSSerachState[R, ScanChNum].Delay:= FBSDelay[R, ScanChNum];
                    FBSSerachState[R, ScanChNum].DisCrd:= FDataCont.CurLoadDisCoord[ABStr];
                    FBSSerachState[R, ScanChNum].State:= True;
                    FBSSerachState[R, ScanChNum].BackMotion:= BackMotion;
                  end;
                end
                else    // Нет дырки
                if (FBSSerachState[R, ScanChNum].State) then // Пачка закончилась ?
                begin
                  FBSSerachState[R, ScanChNum].State:= False;
                                                  // Новая пачка в ЗТМ канале

                  if (not BackMotion) and (not FBSSerachState[R, ScanChNum].BackMotion) then // Проверяем не в зоне движения назад лиона ?
                                                  // Проверяем длину пачки - не короткая ли она ? // ??? - соединение из за этого будет работать не хорошо
                  if (FDataCont.CurLoadDisCoord[ABStr] - FBSSerachState[R, ScanChNum].DisCrd > MMToSysCrd(FParams.MSMPBMinLen)) then
                  begin
                    if FBSBPItem[R, ScanChNum].Free then // Если нет старой еще не сохраненной пачки по запоминает текущую
                    begin
                      FBSBPItem[R, ScanChNum].ScanChNum:= ScanChNum;
                      FBSBPItem[R, ScanChNum].BScanRect.StartDisCrd:= FBSSerachState[R, ScanChNum].DisCrd;
                      FBSBPItem[R, ScanChNum].BScanRect.EndDisCrd:= FDataCont.CurLoadDisCoord[ABStr];
                      FBSBPItem[R, ScanChNum].CentrDisCrd:= (FBSBPItem[R, ScanChNum].BScanRect.StartDisCrd + FBSBPItem[R, ScanChNum].BScanRect.EndDisCrd) div 2;
                      FBSBPItem[R, ScanChNum].CentrSysCrd:= FDataCont.DisToSysCoord(FBSBPItem[R, ScanChNum].CentrDisCrd);
                      FBSBPItem[R, ScanChNum].BScanRect.StartDelay:= Max(0, FBSSerachState[R, ScanChNum].Delay - 5);
                      FBSBPItem[R, ScanChNum].BScanRect.EndDelay:= Max(0, FBSSerachState[R, ScanChNum].Delay + 5);
                      FBSBPItem[R, ScanChNum].Free:= False;
                    end
                    else  // Есть старая не сохраненная пачка, определяем расстояние между ней и новой пачкой
                    begin
                      if FBSSerachState[R, ScanChNum].DisCrd - FBSBPItem[R, ScanChNum].BScanRect.EndDisCrd < FParams.BSSearchGlueZone then
                      begin
                        // Если растояние мало, тогда соединаем эти пачки
                        FBSBPItem[R, ScanChNum].BScanRect.EndDisCrd:= FDataCont.CurLoadDisCoord[ABStr];
                      end
                      else
                      begin
                                                          // Проверка не попадает ли пачка в зону неустойчивого ДС
                                                          //
                        BSNoiseFlag:= False;
                        Q:= FBSNoiseInfoStIdx[R, ScanChNum];
//                        for Q:= FBSNoiseInfoStIdx[R, ScanChNum] to FBSNoiseInfoEndIdx[R, ScanChNum] - 1 do

                        Inc(GCount);
                        while Q <> (FBSNoiseInfoEndIdx[R, ScanChNum]) do
                        begin
                          if (FBSBPItem[R, ScanChNum].CentrDisCrd >= FBSNoiseInfo[R, ScanChNum, Q].StartDisCrd) and
                             (FBSBPItem[R, ScanChNum].CentrDisCrd <= FBSNoiseInfo[R, ScanChNum, Q].EndDisCrd  ) and
                             (FBSNoiseInfo[R, ScanChNum, Q].State) then
                            begin
                              BSNoiseFlag:= True;
                              Break;
                            end;
                          Inc(Q);
                          if Q = FBSNoiseInfoLen then Q:= 0;
                        end;


                        if not BSNoiseFlag // Если мы не в зоне неустойчивого ДС
                          // Если растояние велико, тогда сохраняем/ (добовляем как готовую) старую пачку
                          then
                          begin

                            FBSBPItem[R, ScanChNum].Debug_1:= FGlobalBPCount;
                            if FUseChannel[ScanChNum] then
                            begin
                              Inc(FGlobalBPCount);
                              AddPB(R, ScanChNum, FBSBPItem[R, ScanChNum]);            // Добавляем пачку
                            end;
                          end;
                        // и запоминаем новую
                        FBSBPItem[R, ScanChNum].ScanChNum:= ScanChNum;
                        FBSBPItem[R, ScanChNum].BScanRect.StartDisCrd:= FBSSerachState[R, ScanChNum].DisCrd;
                        FBSBPItem[R, ScanChNum].BScanRect.EndDisCrd:= FDataCont.CurLoadDisCoord[ABStr];
                        FBSBPItem[R, ScanChNum].CentrDisCrd:= (FBSBPItem[R, ScanChNum].BScanRect.StartDisCrd + FBSBPItem[R, ScanChNum].BScanRect.EndDisCrd) div 2;
                        FBSBPItem[R, ScanChNum].CentrSysCrd:= FDataCont.DisToSysCoord(FBSBPItem[R, ScanChNum].CentrDisCrd);
                        FBSBPItem[R, ScanChNum].BScanRect.StartDelay:= Max(0, FBSSerachState[R, ScanChNum].Delay - 5);
                        FBSBPItem[R, ScanChNum].BScanRect.EndDelay:= Max(0, FBSSerachState[R, ScanChNum].Delay + 5);
                        FBSBPItem[R, ScanChNum].Free:= False;
                      end;
                    end;
                  end;
                end;

                // Поскольку есть запаздывание с записью найденой пачки (для склеивания рядом находящихся пачек)
                // проверяем что бы пачка не залежалась
                if not FBSBPItem[R, ScanChNum].Free then // Если есть старая не сохраненная пачка
                                                         // Прооверяем не удалилась ли текущая координата на значительное расстония исключающее склеивание пачек
                  if FDataCont.CurLoadDisCoord[ABStr] - FBSBPItem[R, ScanChNum].BScanRect.EndDisCrd > FParams.BSSearchGlueZone then
                  begin
                                                          // Повтор, это было выше нужен рефакторинг
                                                          // Проверка не попадает ли пачка в зону неустойчивого ДС
                    BSNoiseFlag:= False;
                    Q:= FBSNoiseInfoStIdx[R, ScanChNum];
  //                for Q:= FBSNoiseInfoStIdx[R, ScanChNum] to FBSNoiseInfoEndIdx[R, ScanChNum] - 1 do

                    Inc(GCount);
                    while Q <> (FBSNoiseInfoEndIdx[R, ScanChNum]) do
                    begin
                      if (FBSBPItem[R, ScanChNum].CentrDisCrd >= FBSNoiseInfo[R, ScanChNum, Q].StartDisCrd) and
                         (FBSBPItem[R, ScanChNum].CentrDisCrd <= FBSNoiseInfo[R, ScanChNum, Q].EndDisCrd  ) and
                         (FBSNoiseInfo[R, ScanChNum, Q].State) then
                        begin
                          BSNoiseFlag:= True;
                          Break;
                        end;
                      Inc(Q);
                      if Q = FBSNoiseInfoLen then Q:= 0;
                    end;

                    if not BSNoiseFlag then // Если мы не в зоне неустойчивого ДС
                    begin
                      FBSBPItem[R, ScanChNum].Debug_1:= FGlobalBPCount;
                      if FUseChannel[ScanChNum] then
                      begin
                        Inc(FGlobalBPCount);
                        AddPB(R, ScanChNum, FBSBPItem[R, ScanChNum]);            // Добавляем пачку
                      end;
                    end;
                    FBSBPItem[R, ScanChNum].Free:= True;
                  end;

              end;
          end;

          // ----------------------------- Поиск пачек в ЭХО каналах ----------------------------------------

     //     if FMSChLink[ScanChNum] = - 1 then // Если это канал с ЗТМ - то ищем донный сигнал //DEBUG

//          if ScanChNum in [2]  then // Если это канал с ЗТМ - то ищем донный сигнал //DEBUG
          if  ((FDataCont.CurLoadZPMode[ABStr] = zpmBoth) and ((ScanChNum = 1) or (ScanChNum = 11)) ) or   // Учет режима работы датчиков 0 град
              ((FDataCont.CurLoadZPMode[ABStr] = zpmOnlyA) and (ScanChNum = 1)) or
              ((FDataCont.CurLoadZPMode[ABStr] = zpmOnlyB) and (ScanChNum = 11)) or
              ((ScanChNum <> 1) and (ScanChNum <> 11)) then

          if FDataCont.CurLoadSysCoord[ABStr] > FLastSysCoord then // Только в зонах движения вперед
          begin
                                              // Определяем индекс массива поиска в зависимости от канала

            if (FDataCont.Header.ControlDirection = cd_Tail_B_Head_A) then  // 0 - B >>> A (по ходу / головой вперед)
            begin
              if FCfg.ScanChannelByNum[ScanChNum].EnterAngle = 0 then MIdx:= 1 else
              if FCfg.ScanChannelByNum[ScanChNum].EnterAngle > 0 then MIdx:= 2 else
              if FCfg.ScanChannelByNum[ScanChNum].EnterAngle < 0 then MIdx:= 3;
            end
            else
            begin
              if FCfg.ScanChannelByNum[ScanChNum].EnterAngle = 0 then MIdx:= 1 else
              if FCfg.ScanChannelByNum[ScanChNum].EnterAngle > 0 then MIdx:= 3 else
              if FCfg.ScanChannelByNum[ScanChNum].EnterAngle < 0 then MIdx:= 2;
            end;

            with CurEcho[R, ScanChNum] do      // Анализ сигналов текущей координаты
            begin
              for EchoIdx:= 1 to Count do      // Перекрестный анализ - каждый сигнал (цикл EchoIdx) с каждой формируемой пачкой (цикл MaskIdx)
              // выбрасывание сигналов за границей строба В-развертки
                if not SkipEchoIdx[EchoIdx] then // Отбрасываем донный сигнал
              if (Delay[EchoIdx] / FCfg.ScanChannelByNum[ScanChNum].DelayMultiply >= FCfg.ScanChannelByNum[ScanChNum].BScanGateMin) and
                 (Delay[EchoIdx] / FCfg.ScanChannelByNum[ScanChNum].DelayMultiply <= FCfg.ScanChannelByNum[ScanChNum].BScanGateMax) then
                begin
                  EchoData[EchoIdx].Value:= MaxInt;                       // Сбрасываем индекс найденного подходящего сигнала
                  for MaskIdx:= 0 to Length(FMask[R, ScanChNum]) - 1 do   // Перекрестный анализ - каждый сигнал (цикл EchoIdx) с каждой формируемой пачкой (цикл MaskIdx)
                    if not FMask[R, ScanChNum][MaskIdx].Del then          // Если маска поиска пачки активна
                    begin
                      K:= High(FMask[R, ScanChNum][MaskIdx].Item);  // Берем индекс последнего сигнала входящего в пачку
                      for L:= 1 to 10 do                            // Ищем соответствие DX, DY в массиве поиска
                        if (Mask[MIdx, L].X = (FDataCont.CurLoadDisCoord[ABStr] - FMask[R, ScanChNum][MaskIdx].Item[K].DisCrd)) and
                           (Mask[MIdx, L].Y = (Delay[EchoIdx] - FMask[R, ScanChNum][MaskIdx].Item[K].Delay)) and
                           (L < EchoData[EchoIdx].Value)  and      // Выбор лучшего варианта
                         { ((L = 1) or ((L <> 1) and (Mask[FR, FCh][J].Dir[L] < 2))) } //then
                           (EchoData[EchoIdx].Value > L) then
                        begin
                          EchoData[EchoIdx].MaskIdx:= MaskIdx;
                          EchoData[EchoIdx].Value:= L;
                        //  Break;
                        end;
                    end;
                end;

      {IFDEF _DEBUG}
//     if (R = FCfg.MaxRail) and (ScanChNum in [{0,  1, 2, 3, 4, 5, 6, 7 ]) then
//      if ScanChNum <> 1 then
      {ENDIF}

              for EchoIdx:= 1 to Count do
                if not SkipEchoIdx[EchoIdx] then                              // Отбрасываем донный сигнал
              // выбрасывание сигналов за границей строба В-развертки
              if (Delay[EchoIdx] / FCfg.ScanChannelByNum[ScanChNum].DelayMultiply >= FCfg.ScanChannelByNum[ScanChNum].BScanGateMin) and
                 (Delay[EchoIdx] / FCfg.ScanChannelByNum[ScanChNum].DelayMultiply <= FCfg.ScanChannelByNum[ScanChNum].BScanGateMax) then
                begin

                  if EchoData[EchoIdx].Value <> MaxInt then                   // Смотрим входит ли сигнал в одну из создоваемых пачек
                  begin                                                       // Входит - Вставляем текущий сигнал в пачки
                    MaskIdx:= EchoData[EchoIdx].MaskIdx;

                    K:= Length(FMask[R, ScanChNum][MaskIdx].Item);
                    SetLength(FMask[R, ScanChNum][MaskIdx].Item, K + 1);
                    FMask[R, ScanChNum][MaskIdx].Item[High(FMask[R, ScanChNum][MaskIdx].Item)].DisCrd:= FDataCont.CurLoadDisCoord[ABStr];
                    FMask[R, ScanChNum][MaskIdx].Item[High(FMask[R, ScanChNum][MaskIdx].Item)].Delay:= Delay[EchoIdx];
                    FMask[R, ScanChNum][MaskIdx].Item[High(FMask[R, ScanChNum][MaskIdx].Item)].Ampl:= Ampl[EchoIdx];

//                    FMask[R, ScanChNum][MaskIdx].TwoEcho:= False;

//                    if FMask[R, ScanChNum][MaskIdx].DebugIndex = 1760 then ShowMessage('!'); // DEBUG

                    if FParams.UseChannel_42dTwoEcho and
                       (Abs(FCfg.ScanChannelByNum[ScanChNum].EnterAngle) >= 40) and // Ищим сигнал "2 эхо"
                       (Abs(FCfg.ScanChannelByNum[ScanChNum].EnterAngle) <= 50) and
                       (Delay[EchoIdx] >= FParams.BH_45d_MinDelay) and
                       (Delay[EchoIdx] <= FParams.BH_45d_MaxDelay) then
                    begin
                      for M:= 1 to Count do
                        if (M <> EchoIdx) and (not SkipEchoIdx[M]) then        // Отбрасываем донный сигнал
                        begin
                          if (Abs(Delay[M] - Delay[EchoIdx]) >= FParams.TwoEchoMinDelay) and
                             (Abs(Delay[M] - Delay[EchoIdx]) <= FParams.TwoEchoMaxDelay) then
                          begin
                        //    FMask[R, ScanChNum][MaskIdx].TwoEcho_:= True;
                            Inc(FMask[R, ScanChNum][MaskIdx].TwoEchoCount);
                          end;
//                          if FMask[R, ScanChNum][MaskIdx].TwoEcho then ShowMessage('!');
                        end;
                    end;
                  end
                  else
                  begin                                                       // Не входит - Создаем новую пачку
                    ML:= - 1;
                    for K:= 0 to High(FMask[R, ScanChNum]) do                 // Ищем свободное место в массиве создоваемых пачек
                      if FMask[R, ScanChNum][K].Del then
                      begin
                        ML:= K;
                        Break;
                      end;

                    if ML = - 1 then                                          // Не нашли увеличиваем размер масива
                    begin
                      ML:= Length(FMask[R, ScanChNum]);
                      SetLength(FMask[R, ScanChNum], ML + 1);
                    end;
                                                                              // Вносим новую запись в элемент - ML
                    FMask[R, ScanChNum][ML].Del:= False;
                    FMask[R, ScanChNum][ML].DebugIndex:= FGlobalMaskCount;    // DEBUG
                    Inc(FGlobalMaskCount);
      //            FMask[R, ScanChNum][ML].TwoEcho_:= False;
                    FMask[R, ScanChNum][ML].TwoEchoCount:= 0;
      //              for J:= 1 to 6 do Mask[FR, FCh][ML].Dir[J]:= 0;
                    SetLength(FMask[R, ScanChNum][ML].Item, 1);
                    FMask[R, ScanChNum][ML].Item[0].DisCrd:= FDataCont.CurLoadDisCoord[ABStr];
                    FMask[R, ScanChNum][ML].Item[0].Delay:= Delay[EchoIdx];
                    FMask[R, ScanChNum][ML].Item[0].Ampl:= Ampl[EchoIdx];

                  end;
                end;
            end;
          end
//          else ShowMessage('!');
        end;
      end;
    end;

    // -------------------------------------------------------------------------------------
    // Проверка на окончание формирования пачки
    // -------------------------------------------------------------------------------------

    for R:= FCfg.MinRail to FCfg.MaxRail do
    begin
      for ScanChNum:= FCfg.MinScanChNumber to FCfg.MaxScanChNumber do
      begin
        for MaskIdx:= 0 to High(FMask[R, ScanChNum]) do
          if not FMask[R, ScanChNum][MaskIdx].Del then
            if (FDataCont.CurLoadSysCoord[ABStr] <= FLastSysCoord) or // Если ЗДН то прирываем формирование пачек
               (FDataCont.CurLoadDisCoord[ABStr] -        // Проверяем формруемые пачки на максимально допустимый разрыв - если больше то прирываем формирование пачки
                FMask[R, ScanChNum][MaskIdx].Item[High(FMask[R, ScanChNum][MaskIdx].Item)].DisCrd > FParams.ECHOPBMaxGapLen) then
            begin
              begin
                with FMask[R, ScanChNum][MaskIdx] do        // Проверка годности пачки
                begin
                  MaxAmpl:= - 1;                            // Определение максимальной амплитуды
                  for K:= 0 to High(Item) do MaxAmpl:= Max(MaxAmpl, Item[K].Ampl);

                  CrdLenMM:= SysCrdToMM(Item[High(Item)].DisCrd - Item[0].DisCrd + 1);        // Определение прямоугольности пачки
                  DlyLen:= (Abs(Item[High(Item)].Delay - Item[0].Delay) + 1) / FCfg.ScanChannelByNum[ScanChNum].DelayMultiply;

                  DlyCentr:= (Item[High(Item)].Delay + Item[0].Delay) / 2;

                  HeadFlag:= (Abs(DlyCentr - FParams.HeadZone1) <= FParams.HeadZoneSize) or
                             (Abs(DlyCentr - FParams.HeadZone2) <= FParams.HeadZoneSize);


                  // Выбрасываем пачки для 0 град если мы в зоне неустойчивого ДС

                  if (FCfg.ScanChannelByNum[ScanChNum].EnterAngle = 0) then
                  begin

                    CentrSysCrd:= FDataCont.DisToSysCoord((New.BScanRect.StartDisCrd + New.BScanRect.EndDisCrd) div 2);

                    BSNoiseFlag:= False;
                    Q:= FBSNoiseInfoStIdx[R, ScanChNum];
                    Inc(GCount);
                    while Q <> (FBSNoiseInfoEndIdx[R, ScanChNum]) do
                    begin
                      if (CentrSysCrd >= FBSNoiseInfo[R, ScanChNum, Q].StartDisCrd {- 100}  ) and // Зону расширяем на 100 у.е. // ???
                         (CentrSysCrd <= FBSNoiseInfo[R, ScanChNum, Q].EndDisCrd   {+ 100}  ) and // Зону расширяем на 100 у.е. // ???
                         (FBSNoiseInfo[R, ScanChNum, Q].State) then
                        begin
                          BSNoiseFlag:= True;
                          Break;
                        end;
                      Inc(Q);
                      if Q = FBSNoiseInfoLen then Q:= 0;
                    end;
                  end;
                  if  ((Abs(FCfg.ScanChannelByNum[ScanChNum].EnterAngle) = 58) and  // Для наклонный каналов контроля головки
                       (DlyLen / CrdLenMM >= FParams.MinCrdDelayRatio) and     // проверяем соотношение DL [мм] к DH [мкс]
                       (DlyLen / CrdLenMM <= FParams.MaxCrdDelayRatio) and
                       (CrdLenMM >= {(Ord(HeadFlag) * 2 + 1) *} FParams.MaxAmplToLen[MaxAmpl])) // и длинну на минимально допустимое значени в зависимости от максимальной амплитуды сигнала входящего в пачку
                       or
                      ((FCfg.ScanChannelByNum[ScanChNum].EnterAngle <> 0) and  // Для наклонный каналов
                       (Abs(FCfg.ScanChannelByNum[ScanChNum].EnterAngle) <> 58) and
                       (DlyLen / CrdLenMM >= FParams.MinCrdDelayRatio) and     // проверяем соотношение DL [мм] к DH [мкс]
                       (DlyLen / CrdLenMM <= FParams.MaxCrdDelayRatio) and
                       (CrdLenMM >= FParams.MaxAmplToLen[MaxAmpl]))            // и длинну на минимально допустимое значени в зависимости от максимальной амплитуды сигнала входящего в пачку
                       or
                      ((FCfg.ScanChannelByNum[ScanChNum].EnterAngle = 0) and   // Для прямых каналов
                       (CrdLenMM >= FParams.MaxAmplToLen[MaxAmpl]) and
                       not BSNoiseFlag) then       // проверяем длинну на минимально допустимое значени в зависимости от максимальной амплитуды сигнала входящего в пачку

                  begin                            // Новая пачка в ЭХО канале
                    New.ScanChNum:=             ScanChNum;
                    New.BScanRect.StartDisCrd:= Item[0].DisCrd;          // Координаты начала и конца
                    New.BScanRect.EndDisCrd:=   Item[High(Item)].DisCrd;
                    New.BScanRect.StartDelay:=  512;                     // Определяем координаты по задержке
                    New.BScanRect.EndDelay:=    0;
                    for K:= 0 to Length(Item) - 1 do
                    begin
                      New.BScanRect.StartDelay:=  Min(New.BScanRect.StartDelay, Item[K].Delay);
                      New.BScanRect.EndDelay:=    Max(New.BScanRect.EndDelay, Item[K].Delay);
                    end;
                    New.Angle:= DlyLen / CrdLenMM;
                    New.CentrDisCrd:= (New.BScanRect.StartDisCrd + New.BScanRect.EndDisCrd) div 2;
                    New.CentrDel:= (New.BScanRect.StartDelay + New.BScanRect.EndDelay) div 2;
//                    New.TwoEcho:= FMask[R, ScanChNum][MaskIdx].TwoEcho_;
                    New.TwoEcho:= FMask[R, ScanChNum][MaskIdx].TwoEchoCount > TwoEchoCount {5  шдп}; // !!!
//                    New.Debug_1:= ScanChNum;
//                    New.Debug_1:= Ord(New.TwoEcho); // DEBUG - Проверка определения флага 2-эхо
//                    New.Debug_1:= DebugIndex; // Индекс Маски по которой создавалась пачка // DEBUG
                    New.Debug_1:= FGlobalBPCount; // Индекс пачки // DEBUG
                    Inc(FGlobalBPCount);                          // DEBUG

                    if  FUseChannel[ScanChNum] and                     // Проверка - нужно ли искать пачки в этом канале
                      { (CrdLenMM >= FPBLen[ScanChNum]) and }            // Проверка - на минимальную длину пачки
                       (New.CentrDel >= FSearchStGate[ScanChNum]) and  // Проверка - пападаем ли в строб поиска
                       (New.CentrDel <= FSearchEdGate[ScanChNum]) then // Проверка - пападаем ли в строб поиска
                    begin
                      AddPB(R, ScanChNum, New);                     // Добавляем пачку
                    end;
                  end;
                end;
              end;

              SetLength(FMask[R, ScanChNum][MaskIdx].Item, 0); // Очистка ячейки поиска для нового поиска
              FMask[R, ScanChNum][MaskIdx].Del:= True;
              FMask[R, ScanChNum][MaskIdx].DebugIndex:= FGlobalMaskCount; // DEBUG
        //    FMask[R, ScanChNum][MaskIdx].TwoEcho_:= False;
              FMask[R, ScanChNum][MaskIdx].TwoEchoCount:= 0;
            end;
      end;
    end;
  end;

  FLastLoadDisCoord:= FDataCont.CurLoadDisCoord[ABStr];
  FLastSysCoord:= FDataCont.CurLoadSysCoord[ABStr];      // Сохраняем последнеюю загруженную системную координату
  Result:= True;
end;

function TAirBrush.Sensor1Test(R: TRail; RealCrd: Integer): Boolean;
var
  SIdx, St, Ed: Integer;
  CenterDisCrd: Integer;

begin
  Result:= False;
  {$IFDEF LOG}
//  Writeln(FLog, Format('In Sensor1Test: R: %d; Count: %d', [Ord(R), Length(FSensor1List[R])]));
  {$ENDIF}
  for SIdx:= 0 to Length(FSensor1List[R]) - 1 do // Проверяем на попадание в уже сформированные зоны отметок датчика металла
    if (not FSensor1List[R, SIdx].Free) then
    begin

      {$IFDEF LOG}
//      Writeln(FLog, Format('FParams.TwoEchoZoneLen = %d мм', [FParams.TwoEchoZoneLen]));
//      Writeln(FLog, Format('FParams.TwoEchoZoneLen = %d шдп', [MMToSysCrd(FParams.TwoEchoZoneLen)]));
      {$ENDIF}

      St:= FSensor1List[R, SIdx].StRealDisCrd - MMToSysCrd(FParams.TwoEchoZoneLen) div 2;
      Ed:= FSensor1List[R, SIdx].EdRealDisCrd + MMToSysCrd(FParams.TwoEchoZoneLen) div 2;

      {$IFDEF LOG}
//      Writeln(FLog, Format('In Sensor1Test: Idx = %d; St = %d; Ed = %d; Ok? = %d', [SIdx, St, Ed, Ord((RealCrd >= St) and (RealCrd <= Ed))]));
      {$ENDIF}

      if (RealCrd >= St) and (RealCrd <= Ed) then
//      if (RealCrd >= FSensor1List[R, SIdx].StRealCrd) and
//         (RealCrd <= FSensor1List[R, SIdx].EdRealCrd) then
      begin
        Result:= True;
        Exit;
      end;

    end;
//  else
//  begin
//    {$IFDEF LOG}
//    Writeln(FLog, Format('In Sensor1Test: Idx: %d; id Free', [SIdx]));
//    {$ENDIF}
//  end;

  if not FOldSensor1[R] then // Проверяем на попадание в формиремую зону отметки датчика металла
  begin
    if (RealCrd >= - MMToSysCrd(SensorPos) + FSensor1StDisCrd[R] - MMToSysCrd(FParams.TwoEchoZoneLen) div 2) then
      Result:= True;
  end;
end;

procedure TAirBrush.Execute;
begin
  while not Terminated do
  begin
    if FNeedStop then Break;
    Tick;
  end;
  FStop:= True;
end;

procedure TAirBrush.Tick;
var
  R: TRail;
  I, J, EvCh: Integer;
  MaxDisCrd: Integer;
  PBIdx, ABIdx, SIdx: Integer;
  ABIdx1, ABIdx2: Integer;
  PBCentrCrd: Integer;
  PBCentrDel: Integer;
  ABSysCrd: Single;                // Системная координата отметки
  Flag: Boolean;
  New: TABItem;
  ABItems: TABItemsList2; // array of edAirBrushItem;
  Flg: Boolean;

begin
  if FDataCont.MaxDisCoord - FLastLoadDisCoord < MMToSysCrd(FParams.LoadStep) then
  begin
    Sleep(FParams.TickNoDataSleepValue);
    Exit;
  end;

  CS.Enter;
  FStartAnalyzTime:= GetTickCount();
  SummWaitTime:= SummWaitTime + FStopAnalyzTime - FStartAnalyzTime;

{
  sleep(0);
  if FDataCont.MaxDisCoord - t > 500 then
  begin
    FDataCont.AddAirBrushMark(R, FDataCont.MaxDisCoord, []);
    t:= FDataCont.MaxDisCoord;
  end;
}
                                 // Загрузка и первичный анализ данных - формирование пачек

  MaxDisCrd:= FDataCont.MaxDisCoord;
  if Assigned(FDataCont) then
    if MaxDisCrd - FLastLoadDisCoord > MMToSysCrd(FParams.LoadStep) then
    begin

      {$IFDEF LOG}
//      Writeln(FLog, Format('LoadData: %d...%d', [FLastLoadDisCoord - 1, MaxDisCrd]));
      {$ENDIF}

      FDataCont.LoadData(ABStr, FLastLoadDisCoord - 1, MaxDisCrd, 0, BlockOk);
    end;

  {$IFDEF ONLY_PB}
  for R:= FCfg.MinRail to FCfg.MaxRail do        // Debug - Пометка всех пачек
    for PBIdx:= 0 to Length(FBPList[R]) - 1 do
      if not FBPList[R, PBIdx].Free then         // Пометка найденных пачек
      begin
        FBPList[R, PBIdx].Free:= True;

      //  if (not FFlg) and (SysCrdToMM(FBPList[R, PBIdx].BScanRect.EndDisCrd - FBPList[R, PBIdx].BScanRect.StartDisCrd) > 45) then
      //  if not Sensor1Test(R, FBPList[R, PBIdx].CentrRealCrd) then // Проверка попадания в зону отмеченную датчиком металла
      //  if FBPList[R, PBIdx].TwoEcho then

        begin
          I:= 0;
          SetLength(ABItems, 1);
          ABItems[I].ScanCh       := FBPList[R, PBIdx].ScanChNum;
          ABItems[I].StartDisCoord:= FBPList[R, PBIdx].BScanRect.StartDisCrd;
          ABItems[I].EndDisCoord  := FBPList[R, PBIdx].BScanRect.EndDisCrd;
          ABItems[I].StartDelay   := FBPList[R, PBIdx].BScanRect.StartDelay;
          ABItems[I].EndDelay     := FBPList[R, PBIdx].BScanRect.EndDelay;
          ABItems[I].Debug        := FBPList[R, PBIdx].Debug_1;

          FDataCont.AddAirBrushMark2(R, FBPList[R, PBIdx].CentrRealDisCrd, ABItems);
          SetLength(ABItems, 0);
       //   FDataCont.AddTextLabel(Format('%3.3f', [FBPList[R, PBIdx].Angle]));
       //   FFlg:= True;
        end;
      end;
  {$ENDIF}

  {$IFNDEF ONLY_PB}
                                 // Вторичный анализ данных
                                 // Внесение новых пачек в блоки КП или создание новых блоков КП

  for R:= FCfg.MinRail to FCfg.MaxRail do
    for PBIdx:= 0 to Length(FBPList[R]) - 1 do
      if not FBPList[R, PBIdx].Free then         // Просматриваем имеющиеся пачки
      begin
        Flag:= False;
        {$IFNDEF NOCONSOLIDATION}
        for ABIdx:= 0 to Length(FABList[R]) - 1 do
          if not FABList[R, ABIdx].Free then     // Просматриваем имеющиеся блоки КП
          begin

            {$IFDEF LOG}
//            Writeln(FLog, Format('TryAdd: R = %d; BlockIdx = %d; BlockCrd = %d; PBIdx = %d, PBCrd = %d, Delta = %d, Th = %d', [Ord(R), ABIdx, FABList[R, ABIdx].RealCrd, PBIdx, FBPList[R, PBIdx].CentrRealCrd,FABList[R, ABIdx].RealCrd - FBPList[R, PBIdx].CentrRealCrd, MMToSysCrd(FParams.MaxLenBetweenABMember)]));
            {$ENDIF}

            if Abs(FABList[R, ABIdx].RealCrd - FBPList[R, PBIdx].CentrRealDisCrd) < MMToSysCrd(FParams.MaxLenBetweenABMember) then   // Проверяем входит ли пачка в этот блок КП
            begin                                                // Входит
              if FABList[R, ABIdx].PBCount <= 1023 then
                FABList[R, ABIdx].PBList[FABList[R, ABIdx].PBCount]:= FBPList[R, PBIdx];
              Inc(FABList[R, ABIdx].PBCount);

//              if FBPList[R, PBIdx].Debug_1 = 40 then ShowMessage('40');

              FABList[R, ABIdx].RealCrd:= 0;
              for I:= 0 to FABList[R, ABIdx].PBCount - 1 do
                FABList[R, ABIdx].RealCrd:= FABList[R, ABIdx].RealCrd + FABList[R, ABIdx].PBList[I].CentrDisCrd;
              FABList[R, ABIdx].RealCrd:= Round(FABList[R, ABIdx].RealCrd / FABList[R, ABIdx].PBCount);

              Flag:= True;

              {$IFDEF LOG}
//              Writeln(FLog, 'Add OK');
              {$ENDIF}

              Break;                                            // Пачку пристроили - с этой пачкой все
            end
          end;
        {$ENDIF}

        if not Flag then // Пачку не пристроили - для нее создаем новый блок
        begin
          New.RealCrd:= FBPList[R, PBIdx].CentrRealDisCrd;
          New.PBList[0]:= FBPList[R, PBIdx];
          New.PBCount:= 1;
          I:= AddAB(R, New);

          {$IFDEF LOG}
//          Writeln(FLog, Format('AddBlock: R = %d; PBIdx = %d; PBCrd = %d -> New Block: Idx = %d', [Ord(R), PBIdx, FBPList[R, PBIdx].CentrRealCrd, I]));
          {$ENDIF}
        end;

        FBPList[R, PBIdx].Free:= True;
      end;

   // --------------- АСД ----------------------

  for I := 0 to AlarmDataListSize - 1 do
    if (not FAlarmData[I].Free) and (FLastLoadDisCoord - FAlarmData[I].RealDisCrd > MMToSysCrd(FParams.DecisionMakingBoundary)) then
    begin
      // Вызов Callback функции АСД
//      if Assigned(FAlarmEvent) then FAlarmEvent(FAlarmData[I].Rail, FAlarmData[I].EvalCh, True);
      // Установка массива отключения АСД
      FAlarmEventData[FAlarmData[I].Rail, FAlarmData[I].EvalCh].Time:= GetTickCount();
      FAlarmEventData[FAlarmData[I].Rail, FAlarmData[I].EvalCh].State:= True;
      FAlarmData[I].Free:= True;
    end;

  ///////////////////////


  // Принятие решения по блокам КП

  for R:= FCfg.MinRail to FCfg.MaxRail do
    for ABIdx:= 0 to Length(FABList[R]) - 1 do
      if (not FABList[R, ABIdx].Free) and
         (FLastLoadDisCoord - FABList[R, ABIdx].RealCrd > MMToSysCrd(FParams.DecisionMakingBoundary)) then
      begin
        Flg:= False;

        {$IFDEF LOG}
//          Writeln(FLog, Format('Call Sensor1Test: R = %d; RealCrd = %d', [Ord(R), FABList[R, ABIdx].RealCrd]));
        {$ENDIF}

//        Inc(FGlobalCount);
        if Sensor1Test(R, FABList[R, ABIdx].RealCrd) then // Проверка попадания в зону отмеченную датчиком металла
        begin // В зоне фиксируем только пачки 2 эхо
          SetLength(ABItems, 0);
          for PBIdx:= 0 to FABList[R, ABIdx].PBCount - 1 do  // Если в зоне то ищем признак 2ЭХО
            if FABList[R, ABIdx].PBList[PBIdx].TwoEcho then
            begin
              SetLength(ABItems, Length(ABItems) + 1);
{              for I:= 0 to FABList[R, ABIdx].PBCount - 1 do
              begin }
                J:= Length(ABItems) - 1;
                ABItems[J].ScanCh       := FABList[R, ABIdx].PBList[PBIdx].ScanChNum;
                ABItems[J].StartDisCoord:= FABList[R, ABIdx].PBList[PBIdx].BScanRect.StartDisCrd;
                ABItems[J].EndDisCoord  := FABList[R, ABIdx].PBList[PBIdx].BScanRect.EndDisCrd;
                ABItems[J].StartDelay   := FABList[R, ABIdx].PBList[PBIdx].BScanRect.StartDelay;
                ABItems[J].EndDelay     := FABList[R, ABIdx].PBList[PBIdx].BScanRect.EndDelay;
                ABItems[J].Debug        := FABList[R, ABIdx].PBList[PBIdx].Debug_1;
//              end;
              Flg:= True;
            end;
        end
        else
        begin // Вне зоны фиксируем все пачки
          Flg:= True;
          SetLength(ABItems, FABList[R, ABIdx].PBCount);
          for I:= 0 to FABList[R, ABIdx].PBCount - 1 do
          begin

//            if (FABList[R, ABIdx].PBList[I].BScanRect.StartDisCrd >= 9490) and
//               (FABList[R, ABIdx].PBList[I].BScanRect.StartDisCrd <= 9565) then ShowMessage('!!!');

            ABItems[I].ScanCh       := FABList[R, ABIdx].PBList[I].ScanChNum;
            ABItems[I].StartDisCoord:= FABList[R, ABIdx].PBList[I].BScanRect.StartDisCrd;
            ABItems[I].EndDisCoord  := FABList[R, ABIdx].PBList[I].BScanRect.EndDisCrd;
            ABItems[I].StartDelay   := FABList[R, ABIdx].PBList[I].BScanRect.StartDelay;
            ABItems[I].EndDelay     := FABList[R, ABIdx].PBList[I].BScanRect.EndDelay;
            ABItems[I].Debug        := FABList[R, ABIdx].PBList[I].Debug_1;
          end;
        end;

        if Flg and (not FSkipEvents) then
        begin
          if Assigned(FJobEvent2) then
          begin
            FJobEvent2(R,
                       FABList[R, ABIdx].RealCrd + MMToSysCrd(AirBrushPos + FParams.AirBrush_Shift),
                       FDataCont.DisToSysCoord(FABList[R, ABIdx].RealCrd + MMToSysCrd(AirBrushPos + FParams.AirBrush_Shift)),
                       FABList[R, ABIdx].RealCrd,
                       ABItems);
          end
          else
          if Assigned(FJobEvent) then
          begin
            FJobEvent(R,
                      FABList[R, ABIdx].RealCrd + MMToSysCrd(AirBrushPos + FParams.AirBrush_Shift),
                      FDataCont.DisToSysCoord(FABList[R, ABIdx].RealCrd + MMToSysCrd(AirBrushPos + FParams.AirBrush_Shift)));
            FDataCont.AddAirBrushMark2(R, FABList[R, ABIdx].RealCrd, ABItems);
          end
          else FDataCont.AddAirBrushMark2(R, FABList[R, ABIdx].RealCrd, ABItems);
        end;

        FABList[R, ABIdx].Free:= True;
        {$IFDEF LOG}
//        Writeln(FLog, Format('Free Block: R: = %d; Idx = %d; PBCount = %d', [Ord(R), ABIdx, FABList[R, ABIdx].PBCount]));
        {$ENDIF}

        //AddToLog(Format('Принятие решения по блокам КП: dL = %d', [FLastLoadDisCoord - FABList[R, ABIdx].Coord]));
      end;

                                          // Удаление устаревших отметок датчика металла
  for R:= FCfg.MinRail to FCfg.MaxRail do
    for SIdx:= 0 to Length(FSensor1List[R]) - 1 do
    begin
      {$IFDEF LOG}
//        Writeln(FLog, Format('Analyze Free Sensor: R = %d; Idx = %d, EdRealCrd = %d, CurrRealCrd = %d, Delta = %d; Th = %d', [Ord(R), SIdx, FSensor1List[R, SIdx].EdRealCrd, FLastLoadDisCoord, FLastLoadDisCoord - FSensor1List[R, SIdx].EdRealCrd, MMToSysCrd(FParams.WorkZone)]));
      {$ENDIF}
      if (not FSensor1List[R, SIdx].Free) and
         (FLastLoadDisCoord - FSensor1List[R, SIdx].EdRealDisCrd > MMToSysCrd(FParams.SensorWorkZone)) then
      begin
        FSensor1List[R, SIdx].Free:= True;
        {$IFDEF LOG}
//          Writeln(FLog, Format('Free Sensor: R = %d; Idx = %d', [Ord(R), SIdx]));
        {$ENDIF}
      end;
    end;
  {$ENDIF}

  // АСД посылка события ВЫК по прошествии AlarmEventLifeTime секунд

{  if Assigned(FAlarmEvent) then
    for R:= FCfg.MinRail to FCfg.MaxRail do
      for EvCh:= FCfg.MinEvalChNumber to FCfg.MaxEvalChNumber do
        if (FAlarmEventData[R, EvCh].State) and
           (GetTickCount() - FAlarmEventData[R, EvCh].Time > FParams.AlarmEventLifeTime) then
        begin
           FAlarmEvent(R, EvCh, False);
           FAlarmEventData[R, EvCh].State:= False;
        end;
 }

  FStopAnalyzTime:= GetTickCount();
  SummAnalyzTime:= SummAnalyzTime + (FStopAnalyzTime - FStartAnalyzTime);
  CS.Leave;
end;


end.



  // Проверка склейки блоков

 {
   for R:= FCfg.MinRail to FCfg.MaxRail do
   begin
     Flg:= False;
     for ABIdx1:= 0 to Length(FABList[R]) - 1 do
       for ABIdx2:= 0 to Length(FABList[R]) - 1 do
        if (ABIdx1 <> ABIdx2) and
           (not FABList[R, ABIdx1].Free) and
           (not FABList[R, ABIdx2].Free) then
        begin
          FABList[R, ABIdx1].DisCrd
        end;
          if Flg then
          begin
            if Assigned(FSetJobEvent) then FSetJobEvent(R, FDataCont.DisToSysCoord(FABList[R, ABIdx].DisCrd));
            SetLength(ABItems, FABList[R, ABIdx].PBCount);
            for I:= 0 to FABList[R, ABIdx].PBCount - 1 do
            begin
            end;
            FDataCont.AddAirBrushMark(R, FABList[R, ABIdx].DisCrd, ABItems);
          end;
          FABList[R, ABIdx].Free:= True;
          //AddToLog(Format('Принятие решения по блокам КП: dL = %d', [FLastLoadDisCoord - FABList[R, ABIdx].Coord]));
        end;
   }




















