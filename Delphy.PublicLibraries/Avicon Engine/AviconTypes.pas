unit AviconTypes;
 
interface

uses
  Types, Windows, SysUtils, Classes;

type
  TAviconID = array [0..7] of Byte;

const
{
A 65
B 66
C 67
D 68
E 69
F 70
G 71
H 72
I 73
J 74
K 75
L 76
M 77
N 78
O 79
P 80
Q 81
R 82
S 83
T 84
U 85
W 86
X 87
Y 88
Z 89

R 82
K 75
S 83
U 85
}
  AviconFileID   : TAviconID = ($52, $53, $50, $42, $52, $41, $46, $44); // ������������� ����� [RSPBRAFD]
  Avicon11Scheme0: TAviconID = ($DE, $FE, $41, $31, $31, $53, $30, $01); // ������-11 ����� 0

  Avicon15Scheme1: TAviconID = ($DE, $FE, $41, $31, $35, $53, $31, $01); // ������-15 ����� 1 - ������
  Avicon16Scheme1: TAviconID = ($DE, $FE, $41, $31, $36, $53, $31, $01); // ������-16 ����� 1 - ���������� ������� GEISMAR
  Avicon16Scheme2: TAviconID = ($DE, $FE, $41, $31, $36, $53, $32, $01); // ������-16 ����� 2 - ���������� ������� GEISMAR
  Avicon16Scheme3: TAviconID = ($DE, $FE, $41, $31, $36, $53, $33, $01); // ������-16 ����� 3 - ���������� ������� GEISMAR
  Avicon16Wheel  : TAviconID = ($DE, $FE, $41, $31, $36, $57, $31, $01); // ������-16 �������� ����� - ���������� ������� GEISMAR
  EGO_USWScheme1 : TAviconID = ($DE, $FE, $41, $32, $36, $57, $31, $01); // EGO-USW �������� ����� 1 - ���������� ������� (�������) GEISMAR �������
  EGO_USWScheme2 : TAviconID = ($DE, $FE, $41, $32, $36, $57, $32, $01); // EGO-USW �������� ����� 2 - ���������� ������� GEISMAR �������
  EGO_USWScheme3 : TAviconID = ($DE, $FE, $41, $32, $36, $57, $33, $01); // EGO-USW �������� ����� 3 - ���������� ������� GEISMAR �������
  Avicon14Scheme1: TAviconID = ($DE, $FE, $41, $31, $34, $53, $31, $01); // ������-14 ���������� - ������� �������
  Avicon14Wheel  : TAviconID = ($DE, $FE, $41, $31, $34, $57, $31, $01); // ������-14 �������� ����� - ������� �������
  USK003R        : TAviconID = ($DE, $FE, $55, $53, $4B, $33, $52, $01); // ������-12 ��� ������� - ������ ������ (�����)
  USK004R        : TAviconID = ($DE, $FE, $55, $53, $4B, $34, $52, $01); // ������-12 ��� �������
  Avicon31Scheme1: TAviconID = ($DE, $FE, $41, $33, $31, $53, $31, $01); // ������-31 ����� 1

  RKS_U_Scheme_5: TAviconID = ($DE, $FE, $52, $4B, $53, $55, $31, $01); // ���-� ����� 1 (5 �������)
  RKS_U_Scheme_8: TAviconID = ($DE, $FE, $52, $4B, $53, $55, $32, $01); // ���-� ����� 2 (8 �������)

//  FilusX27Wheel  : TAviconID = ($DE, $FE, $46, $32, $37, $57, $30, $01); // FilusX27 �������� �����
  Avicon17       : TAviconID = ($DE, $FE, $41, $31, $37, $30, $30, $01); // ������-17
  MIGReg         : TAviconID = ($DE, $FE, $4D, $49, $47, $30, $30, $00); // ���
  MIGView        : TAviconID = ($DE, $FE, $4D, $49, $47, $30, $31, $00); // ���
  AviconHandScan : TAviconID = ($FF, $FF, $00, $00, $00, $00, $00, $00); // ������ ������� ������

  cd_Tail_B_Head_A = 0; // 0 - B >>> A (�� ���� / ������� ������)
  cd_Head_B_Tail_A = 1; // 1 - B <<< A (������ ���� / ������� �����)

const

  // ------------------------< �������������� ������� ����� >---------------------------------------

  EID_HandScan = $82; // ������
  EID_Ku = $90; // ��������� �������� ����������������
  EID_Att = $91; // ��������� �����������
  EID_TVG = $92; // ��������� ���
  EID_StStr = $93; // ��������� ��������� ������ ������
  EID_EndStr = $94; // ��������� ��������� ����� ������
  EID_HeadPh = $95; // ������ ���������� ���������
  EID_Mode = $96; // ��������� ������
  EID_SetRailType = $9B; // ��������� �� ��� ������
  EID_PrismDelay = $9C; // ��������� 2�� (word)
  EID_Stolb = $A0; // ������� ����������
  EID_Switch = $A1; // ����� ����������� ��������
  EID_DefLabel = $A2; // ������� �������
  EID_TextLabel = $A3; // ��������� �������
  EID_StBoltStyk = $A4; // ������� ������ ��
  EID_EndBoltStyk = $A5; // ���������� ������ ��
  EID_Time = $A6; // ������� �������
  EID_StolbChainage = $A7; // ������� ���������� Chainage
  EID_ZerroProbMode = $A8; // ����� ������ �������� 0 ����
  EID_LongLabel = $A9; // ����������� �������
  EID_Sensor1 = $B0; // ������ ������� �� � �������
  EID_AirBrush = $B1; // ��������������
  EID_AirBrushJob = $B2; // ��������� � ���������� ������� �� ������-�������
  EID_AirBrushTempOff = $B3; // ��������� ���������� ���������������
  EID_AirBrush2 = $B4; // �������������� � ���������� �����������
  EID_AlarmTempOff = $B5; // ��������� ���������� ��� �� ���� �������
  EID_GPSCoord = $C0; // �������������� ����������
  EID_GPSState = $C1; // ��������� ��������� GPS
  EID_Media = $C2; // �����������
  EID_GPSCoord2 = $C3; // �������������� ���������� �� ���������
  EID_NORDCO_Rec = $C4; // ������ NORDCO

//  EID_CheckSum = $F0; // ����������� �����
  EID_SysCrd_SS = $F1; // ��������� ���������� ��������
  EID_SysCrd_SF = $F4; // ������ ��������� ���������� � �������� �������
  EID_SysCrd_FS = $F9; // ��������� ���������� ��������
  EID_SysCrd_FF = $FC; // ������ ��������� ���������� � ������ �������
  EID_SysCrd_NS = $F2; // ��������� ���������� �����, ��������
  EID_SysCrd_NF = $F3; // ��������� ���������� �����, ������
  EID_SysCrd_UMUA_Left_NF = $F5; // ������ ��������� ���������� ��� ������ ��� A, ����� �������
  EID_SysCrd_UMUB_Left_NF = $F6; // ������ ��������� ���������� ��� ������ ��� �, ����� �������
  EID_SysCrd_UMUA_Right_NF = $F7; // ������ ��������� ���������� ��� ������ ��� A, ������ �������
  EID_SysCrd_UMUB_Right_NF = $F8; // ������ ��������� ���������� ��� ������ ��� �, ������ �������

  EID_EndFile = $FF; // ����� �����

  // ------------------------< �������������� �������������� ������� >---------------------------------------

  EID_FwdDir = 1; // ������ ������� ������
  EID_LinkPt = 2; // ����� ��� ����� ��������� � ������� � �����
  EID_BwdDir = 3; // ������ ������� �����
  EID_EndBM = 4;  //

  // ------------------------< ��������� ����� >---------------------------------------

  // ����� ������������ ������������ ������ � ��������� �����

  uiUnitsCount = 1;       // ���������� ������ �������
  uiRailRoadName = 2;     // �������� �������� ������
  uiOrganization = 3;     // �������� ����������� �������������� ��������
  uiDirectionCode = 4;    // ��� �����������
  uiPathSectionName = 5;  // �������� ������� ������
  uiRailPathNumber = 6;   // ����� �/� ����
  uiRailSection = 7;      // ����� ������
  uiDateTime = 8;         // ���� ����� ��������
  uiOperatorName = 9;     // ��� ���������
  uiCheckSumm = 10;       // ����������� �����
  uiStartMetric = 11;     // ��������� ���������� - �����������
  uiMoveDir = 12;         // ����������� ��������
  uiPathCoordSystem = 13; // ������� ��������� ���������
  uiWorkRailTypeA = 14;   // ������� ���� (��� ������������ ��������)
  uiStartChainage = 15;   // ��������� ���������� � XXX.YYY
  uiWorkRailTypeB = 16;   // ������� ���� ������� �2
  uiTrackDirection = 17;  // ��� �����������
  uiTrackID = 18;         // TrackID
  uiCorrSides = 19;       // ������������ ������� ������� ����� ����
  uiControlDir = 20;      // ����������� ��������
  uiGaugeSideLeftSide = 21;  // ������������ ������� ������� ������� / �� ������� ����� ������� ������
  uiGaugeSideRightSide = 22; // ������������ ������� ������� ������� / �� ������� ����� ������� ������
  uiGPSTrackinDegrees = 23;  // ������ GPS ����� (�������� � ��������)

  // ------------------------< ���������� ������� ������ >---------------------------------------

  MinEvalChannel = 0;
  MaxEvalChannel = 64;

  // --------------------------------------------------------------------------------------------


//  TChannelID = Integer;
  MaxChannel_ = 37;

  MIRRORSHADOW_0 = 0;
  ECHO_0 = 17;

  FORWARD_BOTH_ECHO_58 = 1;
  FORWARD_WORK_ECHO_58 = 2;
  FORWARD_UWORK_ECHO_58 = 3;

  BACKWARD_BOTH_ECHO_58 = 4;
  BACKWARD_WORK_ECHO_58 = 5;
  BACKWARD_UWORK_ECHO_58 = 6;

  FORWARD_BOTH_MIRROR_58 = 7;
  FORWARD_WORK_MIRROR_58 = 8;
  FORWARD_UWORK_MIRROR_58 = 9;

  BACKWARD_BOTH_MIRROR_58 = 10;
  BACKWARD_WORK_MIRROR_58 = 11;
  BACKWARD_UWORK_MIRROR_58 = 12;

  FORWARD_ECHO_70 = 13;
  BACKWARD_ECHO_70 = 14;

  FORWARD_ECHO_42_WEB = 15;
  BACKWARD_ECHO_42_WEB = 16;

  FORWARD_ECHO_42_BASE = 18;
  BACKWARD_ECHO_42_BASE = 19;

  FORWARD_ECHO_65 = 20;
  BACKWARD_ECHO_65 = 21;

  SIDE_ECHO_65 = 22;

  WORK_MSM_55 = 23;
  UWORK_MSM_55 = 24;

  FORWARD_WORK_ECHO_65 = 25;
  FORWARD_UWORK_ECHO_65 = 26;

  BACKWARD_WORK_ECHO_65 = 27;
  BACKWARD_UWORK_ECHO_65 = 28;

  FORWARD_WORK_ECHO_50 = 29;
  FORWARD_UWORK_ECHO_50 = 30;

  BACKWARD_WORK_ECHO_50 = 31;
  BACKWARD_UWORK_ECHO_50 = 32;

  FORWARD_ECHO_45_WEB = 33;
  BACKWARD_ECHO_45_WEB = 34;
  FORWARD_ECHO_45_BASE = 35;
  BACKWARD_ECHO_45_BASE = 36;



//  Colors: array [0..MaxChannel_] of TColor;

const
  EchoLen: array [0 .. 7] of Integer = (2, 3, 5, 6, 8, 9, 11, 0);
  HSEchoLen: array [1 .. 4] of Integer = (2, 3, 5, 6);


  // ������������ ����� ������             //  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13
  // � ������ � EGO USW                    //  0 - WPA
                                           //  1 - WPB
  EGOUSW_WP_Data: array [0..13] of Integer = ( 0,  0,  0,  1,  0,  1,  0,  1,  0,  1,  1,  1,  0,  1);

type


           // ��������� ����������

  TCoordSys = (csMetricRF, csImperial, csMetric0_1km, csMetric1km, csMetric1kmTurkish); // ������� ������� ��������� ����������

  TCaCrd = record  // ������ 8 ����
    XXX: Integer;
    YYY: Integer;
  end;

  TMRFCrd = record // ������ 12 ����
    Km: Integer;
    Pk: Integer;
    mm: Integer;
  end;

  TRail = (rLeft, rRight, rNotSet);

  TCorrespondenceSides = (csDirect, csReverse); // ������������ ������� ������� ����� ����: ������ - ����� ������� = ����� ����
                                                // ������������ ������� ������� ����� ����: ������ - ����: ID 0, ID 1 = ����� ���� ����; �������� - ID 0, ID 1 = ������ ���� ����

  TZerroProbMode = (zpmBoth = 0, zpmOnlyA = 1, zpmOnlyB = 2); // ����� ������ �������� 0 ����

  TInspectionMethod = (imEcho,         // ��� �����
                       imMirrorShadow, // ��������� ������� �����
                       imMirror,       // ���������� �����
                       imShadow);      // ������� �����

  TInspectionZone = (izAll, izHead, izWeb, izBase);

                                         // ���� ������ �������
  TDeviceUnitType = (dutUpUnit = 1,      // ������� ����
                     dutDownUnit = 2,    // ������ ����
                     dutMiddleUnit = 3); // �������������� ����

  TAmplItem = record  // ������� ������� ������������� �������� � �������� ���������
    Code: Integer;
    Value: Integer;
  end;

  TInfoSide = (isLeft, isRight); // ������� ���� ������ �-���������
  TInfoPos = (ipLeft, ipCentre, ipRight); // ��������� � ���� ������ ������

  TEvalChannelItem = record      // ����� ������
    Number: Integer;             // ����� ������
    Method: TInspectionMethod;   // ����� ��������
    Zone: TInspectionZone;       // ���� �������� �� ������
    ShortName: string;           // ������� �������� ������
    FullName: string;            // ������ �������� ������
    DirColor: Integer;
    // ?                         // ������ ���������� - �������
    ScanChNum: Integer;          // ����� ��������� � ������� ������
                                 // ���������� � ���� ������ ���������� ������� ������
//    InfoSide: TInfoSide;         // �������
//    InfoPos: TInfoPos;           // �������
  end;

  TEvalChannelItem2 = record     // ����� ������
    InfoSide: TInfoSide;         // �������
    InfoPos: TInfoPos;           // �������
  end;


  TScanChannelDataType = (scdtNormal,  // �������
                          scdtPacked); // � ���������

  TGran = (_None,
           _Both,
           _Work,
           _UnWork,
           _Work_Left__UnWork_Right,
           _UnWork_Left__Work_Right);


  TScanChannelItem = record         // ����� ���������
    Number: Integer;                // ����� ������
    EnterAngle: Integer;            // ���� ����� (�� ����������� ��������) + ���� ���������� / �����������
    TurnAngle: Integer;             // ���� ��������� (�� ����������� ��������)
    Position: Integer;              // ��������� � ���� (��)
    BScanGateMin: Integer;          // ������ ������ �-��������� (���)
    BScanGateMax: Integer;          // ����� ������ �-��������� (���)
    BScanDuration: Integer;         //
//    DataType: TScanChannelDataType; // ��� ������ (������� ��� / �������� ������)
    DelayMultiply: Integer;         // ��������� �������� (�������� �� ������� ����������� �������� �������� ����� ����������� � byte)
    Delta: Integer;                 //
    Gran: TGran;

//    Order: Integer;                 //
  end;

  THandChannelItem = record         // ����� ������� ��������
    Number: Integer;                // ����� ������
    BScanGateMin: Integer;          // ������ ������ �-��������� (���)
    BScanGateMax: Integer;          // ����� ������ �-��������� (���)
    DelayMultiply: Integer;         // ��������� �������� (�������� �� ������� ����������� �������� �������� ����� ����������� � byte)
    Method: TInspectionMethod;      // ����� ��������
    Angle: Integer;                 // ���� ����� (�� ����������� ��������)
    ShortName: string;              // ������� �������� ������
    FullName: string;               // ������ �������� ������
    // ?                            // ������ ���������� - �������
  end;

  TBScanLineItem = record
    ScanChIdx: Integer;             // ������ ������ ���������
//    Order: Integer;                 // ������ ������: 1 - ������� ����� 2 - �� ������� �����

//    ChNum: Integer;               // ����� ������ ���������
//    ChType: Integer;                // ?
//    ColorIdx: Integer;
  end;

  TBScanLine = record               // ������ ������ �-���������
    R: TRail;                       // ����
    Use: Boolean;                   // ������������ ��� ���
    Items: array of TBScanLineItem; // ������ ��������� �� ������ ������
  end;

  TDataNotifyEvent = function(StartDisCoord: Integer): Boolean of object;

  TEvalChannelParams = packed record //9bytes
    Ku: ShortInt;      // �������� ���������������� [��]
    Att: Byte;         // ���������� [��]
    TVG: Byte;         // ��� [���]
    StStr: Byte;       // ������ ������ [���]
    EndStr: Byte;      // ����� ������ [���]
    PrismDelay: Word;  // ����� � ������� [��� x 100]
    HeadPh: Boolean;   // ���
  end;

//!!!AK  TEvalChannelsParams = array [rLeft..rRight] of array [MinEvalChannel..255 {MaxEvalChannel}] of TEvalChannelParams;
  TEvalChannelsParams = array [rLeft..rRight] of array [MinEvalChannel..{255} MaxEvalChannel] of TEvalChannelParams;
  TSensor1Data = array [TRail] of Boolean;

  EMyExcept = class(Exception);

  THeaderStr = array [0..64] of Char;

  THeaderBigStr = array [0..255] of Char;

  TUnitInfo = packed record  // ���������� � ����� �������
    UnitType: TDeviceUnitType; // ��� �����
    WorksNumber: THeaderStr;   // ��������� ����� �����
    FirmwareVer: THeaderStr;   // ������ ������������ ����������� �����
  end;

  TCardinalPoints = array [0..1] of WChar; // ������� �����

  TUsedItemsList = array [1 .. 64] of Byte; // ����� ������������ ������������ ������

  TUnitsInfoList = array [0 .. 9] of TUnitInfo; // ���������� � ������ �������

  TFileHeader_v01 = packed record // ��������� �����
{x}    FileID: uint64; // RSPBRAFD
{+}    DeviceID: uint64; // ������������� �������
{x}    DeviceVer: Byte; // ������ �������
{x}    HeaderVer: Byte; // ������ ���������
{+}    MoveDir: Integer; // ����������� ��������: + 1 � ������� ���������� ��������� ����������; - 1 � ������� ���������� ��������� ����������
{x}    ScanStep: Word; // ��� ������� ���� (�� * 100)
{?}    PathCoordSystem: Byte; // ������� ������� ��������� ����������

    UsedItems: TUsedItemsList; // ����� ������������ ������������ ������
    CheckSumm: Cardinal; // ����������� ����� (unsigned 32-bit)
{+}    UnitsCount: Byte; // ���������� ������ �������
{+}    UnitsInfo: TUnitsInfoList; // ���������� � ������ �������
{+}    Organization: THeaderStr; // �������� ����������� �������������� ��������
{+}    RailRoadName: THeaderStr; // �������� �������� ������
{+}    DirectionCode: DWord; // ��� �����������
{+}    PathSectionName: THeaderStr; // �������� ������� ������
{+}    OperatorName: THeaderStr; // ��� ���������
{+}    RailPathNumber: Integer; // ����� �/� ����
{+}    RailSection: Integer; // ����� ������
{+}    Year: Word; // ���� ����� ��������
{+}    Month: Word;
{+}    Day: Word;
{+}    Hour: Word;
{+}    Minute: Word;
{+}    StartKM: Integer; // ��������� ���������� - �����������
{+}    StartPk: Integer;
{+}    StartMetre: Integer;
    WorkRail: TRail; // ������� ���� (��� ������������ ��������)
    Reserv: array [1 .. 2047] of Byte; // ������
    TableLink: Cardinal; // ������ �� ����������� ���������
  end;

  TFileHeader_v02 = packed record // ��������� �����
    FileID: TAviconID; // RSPBRAFD
    DeviceID: TAviconID; // ������������� �������
    DeviceVer: Byte; // ������ �������
    HeaderVer: Byte; // ������ ���������
    MoveDir: Integer; // ����������� ��������: + 1 � ������� ���������� ��������� ����������; - 1 � ������� ���������� ��������� ����������
    ScanStep: Word; // ��� ������� ���� (�� * 100)
    PathCoordSystem: Byte; // ������� ������� ��������� ����������
    UsedItems: TUsedItemsList; // ����� ������������ ������������ ������
    CheckSumm: Cardinal; // ����������� ����� (unsigned 32-bit)
    UnitsCount: Byte; // ���������� ������ �������
    UnitsInfo: TUnitsInfoList; // ���������� � ������ �������
    Organization: THeaderStr; // �������� ����������� �������������� ��������
    RailRoadName: THeaderStr; // �������� �������� ������
    DirectionCode: DWord; // ��� �����������
    PathSectionName: THeaderStr; // �������� ������� ������
    OperatorName: THeaderStr; // ��� ���������
    RailPathNumber: Integer; // ����� �/� ����
    RailSection: Integer; // ����� ������
    Year: Word; // ���� ����� ��������
    Month: Word;
    Day: Word;
    Hour: Word;
    Minute: Word;
    StartKM: Integer; // ��������� ���������� - �����������
    StartPk: Integer;
    StartMetre: Integer;
    WorkRailTypeA: Byte;  // ������� ���� (��� ������������ ��������): 0 � �����, 1 � ������
    StartChainage: TCaCrd;  // ��������� ���������� � ����������� �� PathCoordSystem
    WorkRailTypeB: TCardinalPoints;  // ������� ���� (��� ������������ ��������): NR, SR, WR, ER
    TrackDirection: TCardinalPoints; // ��� �����������:                          NB, SB, EB, WB, CT, PT, RA, TT
    TrackID: TCardinalPoints;        // Track ID:                                 NR, SR, ER, WR
    CorrespondenceSides: TCorrespondenceSides;
	  HCThreshold: Double;
    Reserv: array [1 .. 2031 - 5 - 8] of Byte; // ������
    TableLink: Cardinal; // ������ �� ����������� ���������
  end;

  TLinkData = array [0..15] of Word;

  TFileHeader_v03 = packed record // ��������� �����
    FileID: TAviconID; // RSPBRAFD
    DeviceID: TAviconID; // ������������� �������
    DeviceVer: Byte; // ������ �������
    HeaderVer: Byte; // ������ ���������
    MoveDir: Integer; // ����������� ��������: + 1 � ������� ���������� ��������� ����������; - 1 � ������� ���������� ��������� ����������
    ScanStep: Word; // ��� ������� ���� (�� * 100)
    PathCoordSystem: Byte; // ������� ������� ��������� ����������
    UsedItems: TUsedItemsList; // ����� ������������ ������������ ������
    CheckSumm: Cardinal; // ����������� ����� (unsigned 32-bit)
    UnitsCount: Byte; // ���������� ������ �������
    UnitsInfo: TUnitsInfoList; // ���������� � ������ �������
    Organization: THeaderStr; // �������� ����������� �������������� ��������
    RailRoadName: THeaderStr; // �������� �������� ������
    DirectionCode: DWord; // ��� �����������
    PathSectionName: THeaderStr; // �������� ������� ������
    OperatorName: THeaderStr; // ��� ���������
    RailPathNumber: Integer; // ����� �/� ����
    RailSection: Integer; // ����� ������
    Year: Word; // ���� ����� ��������
    Month: Word;
    Day: Word;
    Hour: Word;
    Minute: Word;
    StartKM: Integer; // ��������� ���������� - �����������
    StartPk: Integer;
    StartMetre: Integer;
    WorkRailTypeA: Byte;  // ������� ���� (��� ������������ ��������): 0 � �����, 1 � ������
    StartChainage: TCaCrd;  // ��������� ���������� � ����������� �� PathCoordSystem
    WorkRailTypeB: TCardinalPoints;  // ������� ���� (��� ������������ ��������): NR, SR, WR, ER
    TrackDirection: TCardinalPoints; // ��� �����������:                          NB, SB, EB, WB, CT, PT, RA, TT
    TrackID: TCardinalPoints;        // Track ID:                                 NR, SR, ER, WR
    CorrespondenceSides: TCorrespondenceSides;
	  HCThreshold: Double;
    ChIdxtoCID: TLinkData;     // ������ ����� ������� ������ � ����� � CID
    ChIdxtoGateIdx: TLinkData; // ������ ����� ������� ������ � ����� � ������� ������
    Reserv: array [1..1954] of Byte; // ������
    TableLink: Cardinal; // ������ �� ����������� ���������
  end;

  TFileHeader_v04 = packed record // ��������� �����
    FileID: TAviconID; // RSPBRAFD
    DeviceID: TAviconID; // ������������� �������
    DeviceVer: Byte; // ������ �������
    HeaderVer: Byte; // ������ ���������
    MoveDir: Integer; // ����������� ��������: + 1 � ������� ���������� ��������� ����������; - 1 � ������� ���������� ��������� ����������
    ScanStep: Word; // ��� ������� ���� (�� * 100)
    PathCoordSystem: Byte; // ������� ������� ��������� ����������
    UsedItems: TUsedItemsList; // ����� ������������ ������������ ������
    CheckSumm: Cardinal; // ����������� ����� (unsigned 32-bit)
    UnitsCount: Byte; // ���������� ������ �������
    UnitsInfo: TUnitsInfoList; // ���������� � ������ �������
    Organization: THeaderStr; // �������� ����������� �������������� ��������
    RailRoadName: THeaderStr; // �������� �������� ������
    DirectionCode: DWord; // ��� �����������
    PathSectionName: THeaderStr; // �������� ������� ������
    OperatorName: THeaderStr; // ��� ���������
    RailPathNumber: Integer; // ����� �/� ����
    RailSection: Integer; // ����� ������
    Year: Word; // ���� ����� ��������
    Month: Word;
    Day: Word;
    Hour: Word;
    Minute: Word;
    StartKM: Integer; // ��������� ���������� - �����������
    StartPk: Integer;
    StartMetre: Integer;
    WorkRailTypeA: Byte;  // ������� ���� (��� ������������ ��������): 0 � �����, 1 � ������
    StartChainage: TCaCrd;  // ��������� ���������� � ����������� �� PathCoordSystem
    WorkRailTypeB: TCardinalPoints;  // ������� ���� (��� ������������ ��������): NR, SR, WR, ER
    TrackDirection: TCardinalPoints; // ��� �����������:                          NB, SB, EB, WB, CT, PT, RA, TT
    TrackID: TCardinalPoints;        // Track ID:                                 NR, SR, ER, WR
    CorrespondenceSides: TCorrespondenceSides;
	  HCThreshold: Double;
    ChIdxtoCID: TLinkData;     // ������ ����� ������� ������ � ����� � CID
    ChIdxtoGateIdx: TLinkData; // ������ ����� ������� ������ � ����� � ������� ������
    ControlDirection: Byte;    // ����������� ��������: 0 - B >>> A (�� ���� / ������� ������); 1 - B <<< A (������ ���� / ������� �����)
    Reserv: array [1..1954 - 1] of Byte; // ������
    TableLink: Cardinal; // ������ �� ����������� ���������
  end;

  TFileHeader = TFileHeader_v04;


  // ----[ NORDCO ]---------------------------------------------------------------

  TNStatus = (nsOpen, nsClosed); // ��� ���� ���������, ������ �� ������ (�.�. ������ �� ����)

  TSourceType = (stUltrasonicTrolley, stVisualInspection); // ����� ����������� �������


  TNordcoCSVRecordLine = record

    Line              : string;                                                     // �������� ��� ������������� �������������� �����
    Track             : string;                                                     // ������������� ����, � ������� ��������� ������
    LocationFrom      : Single;                                                     // ������ ������������� �������
    LocationTo        : Single;                                                     // ����� ������������� �������. � ������ ��������� ������� �� �� �����, ��� ����  "Location From"
    LatitudeFrom      : Double;                                                     // GPS-������ ��� ���� "Location From"
    LatitudeTo        : Double;                                                     // GPS-������ ��� ���� "Location To"
    LongitudeFrom     : Double;                                                     // GPS-������� ��� ���� "Location From"
    LongitudeTo       : Double;                                                     // GPS-������� ��� ���� "Location To"
    Type_             : string;                                                     // ��� ������� �� UIC 712
    SurveyDate        : TDateTime;                                                  // ���� ����������� �������
    Source            : string;                                                     // ����� ����������� ������� - �� ������: Ultrasonic Trolley, Visual Inspection
    Status            : string;                                                     // ��� ���� ���������, ������ �� ������ (�.�. ������ �� ����) - �� ������: Open, Closed
    IsFailure         : Boolean;                                                    // ���� ���������� ������ ������� �����, �� ��� ���� ������ ����� �������� TRUE. ������� ����� ������� ����� ������, ������� �������� ����������� ��������
    AssetType         : string;                                                     // ��� �������� �������� ���� - �� ������: Left Rail, Right Rail
    DataEntryDate     : TDateTime;                                                  // ���� ����������� ������ ��������
    Size              : Single;                                                     // ������ ������� ���  ����������� �������. ��������, ���� ������ ��� �������, �� �� �����.
    Size2             : Single;                                                     // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
    Size3             : Single;                                                     // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
    UnitofMeasurement : string;                                                     // ��. ��������� ���� "Size"
    UnitofMeasurement2: string;                                                     // ��. ��������� ���� "Size2"
    UnitofMeasurement3: string;                                                     // ��. ��������� ���� "Size3"
    Comment           : string;                                                     // ����������
    Operator_         : string;                                                     // ��������
    Device            : string;                                                     // ������
  end;

  // ----[ ������ ������� ����� ]-------------------------------------------------

  // �������: EID_HandScan

  THSHead = packed record // ��������� ������ �-��������� ������� ������
    DataSize: DWord;      // ������ ������ ������
    Rail: TRail;          // ����
    ScanSurface: Byte;    // ����� ����������� ������������ ������, �������� ������ �� � config
    HandChNum: Byte;      // ����� ������
    Att: ShortInt;        // ����������
    Ku: Byte;             // �������� �������� ����������������
    TVG: Byte;            // �������� ���
  end;

  THSItem = packed record // ������� �-�������� ������� ������ (���� ���� ������ ��� - ����������� 4 �������)
    Sample: Word;                 // ����� ����� ������������
    Count: Byte;                  // ���������� ���-��������
    Data: array [0 .. 51] of Byte; // ������ ��������
  end;

//  TEventData = packed array [1 .. 516] of Byte;
  TEventData = packed array [1 .. 2015] of Byte;
  PEventData = ^TEventData;

  // �������: EID_Sens   - ShortInt
  // EID_Att    - Byte
  // EID_TVG    - Byte
  // EID_StStr  - Byte
  // EID_EdStr  - Byte
  // EID_HeadPh - Boolean

  edEvalChByteParam = packed record // 3 �����
    Rail: TRail;
    EvalCh: Byte;
    Value: Byte;
  end;

  pedEvalChByteParam = ^edEvalChByteParam;

  // �������: EID_PrismDelay

  edEvalChWordParam = packed record // 4 �����
    Rail: TRail;
    EvalCh: Byte;
    Value: Word;
  end;

  pedEvalChWordParam = ^edEvalChWordParam;

  // �������: EID_Mode

  edMode = packed record // 7 ����
    ModeIdx: Word;    // �����
    Channel: Boolean; // ���� ������� � ��� ��� ���� ������� ���� � ����� ������
    Rail: TRail;      // ����
    EvalChNum: Byte;  // ����� ������ ������
    Time: Word;       // ����� ���������� � ���������� ������ (���)
  end;

  pedMode = ^edMode;

  // �������: EID_SetRailType
  // ��� ������

  // �������: EID_Stolb

  edCoordPost = packed record     // ������: 144 �����
    Km: array [0..1] of Integer;
    Pk: array [0..1] of Integer;
    Reserv: array [0..31] of Integer;
  end;
  pedCoordPost = ^edCoordPost;

  pCoordPost = pedCoordPost;

  // �������: EID_StolbChainage

  edCoordPostChainage = packed record  // ������: 136 �����
    Val: TCaCrd;                       // ���������� � ������� XXX.YYY
    Reserv: array [0..31] of Integer;
  end;
  pedCoordPostChainage = ^edCoordPostChainage;

  pCoordPostChainage = pedCoordPostChainage;

  // �������: EID_ZerroProbMode - ����� ������ �������� 0 ����

  edZerroProbMode = packed record // 7 ���� - TZerroProbMode
    Mode: Byte; // �����: 0 � ��� ������; 1 � ���������� �� / ��A; 2 � ����������� �� / ��B
  end;

  pedZerroProbMode = ^edZerroProbMode;
  pZerroProbMode = pedZerroProbMode;

  // �������: EID_LongLabel ����������� �������

  edLongLabel = packed record // 24 ����
    Rail: TRail;      // ����
    EvalCh: Byte;
    ScanCh: Byte; // ����� ���������
    Type_: Byte;  // ���: 1 - ������������ ��; 2 - ���� ��
    Data1: Integer;
    Data2: Integer;
    Data3: Integer;
    StartDisCoord: Integer; // ���������� ���������� ������ �������
    EndDisCoord: Integer; // ���������� ���������� ����� �������
  end;

  pedLongLabel = ^edLongLabel;
  pLongLabel = pedLongLabel;

  // �������: EID_TextLabel

  edTextLabel = packed record // max 513 ����
    Len: Byte;
    Text: array [0 .. 255] of Char;
  end;

  pedTextLabel = ^edTextLabel;

  // �������: EID_Switch

  edSwitch = edTextLabel;
  pedSwitch = ^edSwitch;

  // �������: EID_DefLabel

  edDefLabel = packed record // max 514 ����
    Len: Byte;
    Rail: TRail;
    Text: array [0 .. 255] of Char;
  end;

  pedDefLabel = ^edDefLabel;

  // �������: EID_StBoltStyk
  //          EID_EndBoltStyk
  // ��� ������

  // �������: EID_Time

  edTime = packed record // 2 �����
    Hour: Byte;
    Minute: Byte;
  end;

  pedTime = ^edTime;

  // �������: EID_Sensor1

  edSensor1 = packed record // 2 �����
    Rail: TRail;
    State: Boolean;
  end;
  pedSensor1 = ^edSensor1;

  // EID_CheckSum

  edChecksum = packed record // 5 ����
    Offset: Integer; // �������� ����������� ������� ��� 0 ���� ������� ������ � �����
    Checksum: Byte;
  end;

  pedChecksum = ^edChecksum;

  // �������: EID_AirBrush

  edAirBrushItem = packed record // 11 ���� (���� �� �-���������)
    ScanCh: Byte; // ����� ���������
    StartDisCoord: Integer; // ���������� ���������� ������ ����
    EndDisCoord: Integer; // ���������� ���������� ����� ����
    StartDelay: Byte; // �������� ������ ����
    EndDelay: Byte; // �������� ����� ����
  end;

  TABItemsList = array of edAirBrushItem;

  edAirBrush = packed record // 182 �����
    Rail: TRail;             // ����
    DisCoord: Integer;       // ���������� ���������� �������
    Count: Byte;             // ���������� ���������
    Items: array [0..15] of edAirBrushItem; // ���� �� �-��������� �� ������� ���� ������� �������
  end;
  pedAirBrush = ^edAirBrush;

  // �������: EID_AirBrush2

  edAirBrushItem2 = packed record // 15 ���� (���� �� �-���������)
    ScanCh: Byte; // ����� ���������
    StartDisCoord: Integer; // ���������� ���������� ������ ����
    EndDisCoord: Integer; // ���������� ���������� ����� ����
    StartDelay: Byte; // �������� ������ ����
    EndDelay: Byte; // �������� ����� ����
    Debug: Integer; // ��� �������
  end;

  TABItemsList2 = array of edAirBrushItem2;

  edAirBrush2 = packed record // 246 ����
    Rail: TRail;             // ����
    DisCoord: Integer;       // ���������� ���������� �������
    Count: Byte;             // ���������� ���������
    Items: array [0..15] of edAirBrushItem2; // ���� �� �-��������� �� ������� ���� ������� �������
  end;
  pedAirBrush2 = ^edAirBrush2;


  // �������: EID_AirBrushJob

  edAirBrushJob = packed record // 8 ����
    Rail: TRail;             // ����
    DisCoord: Integer;       // ���������� ���������� �������
    ErrCod: Byte;            // ��� ������
    Delta: SmallInt;         // ������ �� ����������
  end;
  pedAirBrushJob = ^edAirBrushJob;

  // �������: EID_AirBrushTempOff - ��������� ���������� ���������������

  edAirBrushTempOff = packed record // 5 ����
    State: Byte;             // ���������
    DisCoord: Integer;       // ���������� ���������� �������
  end;
  pedAirBrushTempOff = ^edAirBrushTempOff;

  // �������: EID_AlarmTempOff - ��������� ���������� ��� �� ���� �������

  edAlarmTempOff = packed record // 5 ����
    State: Byte;             // ���������: 0 - ����, 1 - ���
    DisCoord: Integer;       // ���������� ���������� �������
  end;
  pedAlarmTempOff = ^edAlarmTempOff;

  // �������: EID_GPSCoord - �������������� ����������

  edGPSCoord = packed record // 8 ����
    Lat:	Single; //	������
    Lon: Single;  // �������
  end;
  pedGPSCoord = ^edGPSCoord;

  // �������: EID_GPSCoord2 - �������������� ����������

  edGPSCoord2 = packed record // 8 ����
    Lat:	Single;  //	������
    Lon: Single;   // �������
    Speed: Single; // ��������
  end;
  pedGPSCoord2 = ^edGPSCoord2;


  // �������: EID_NORDCO_Rec - ������ NORDCO

  edNORDCO_Rec = packed record // ? ����

    DisCoord          : Integer;       // ���������� ���������� �������
    Line              : THeaderStr;    // �������� ��� ������������� �������������� �����
    Track             : THeaderStr;    // ������������� ����, � ������� ��������� ������
    LocationFrom      : Single;        // ������ ������������� �������
    LocationTo        : Single;        // ����� ������������� �������. � ������ ��������� ������� �� �� �����, ��� ����  "Location From"
    LatitudeFrom      : Double;        // GPS-������ ��� ���� "Location From"
    LatitudeTo        : Double;        // GPS-������ ��� ���� "Location To"
    LongitudeFrom     : Double;        // GPS-������� ��� ���� "Location From"
    LongitudeTo       : Double;        // GPS-������� ��� ���� "Location To"
    Type_             : THeaderStr;    // ��� ������� �� UIC 712
    SurveyDate        : TDateTime;     // ���� ����������� �������
    Source            : THeaderStr;    // ����� ����������� ������� - �� ������: Ultrasonic Trolley, Visual Inspection
    Status            : THeaderStr;    // ��� ���� ���������, ������ �� ������ (�.�. ������ �� ����) - �� ������: Open, Closed
    IsFailure         : Boolean;       // ���� ���������� ������ ������� �����, �� ��� ���� ������ ����� �������� TRUE. ������� ����� ������� ����� ������, ������� �������� ����������� ��������
    AssetType         : THeaderStr;    // ��� �������� �������� ���� - �� ������: Left Rail, Right Rail
    DataEntryDate     : TDateTime;     // ���� ����������� ������ ��������
    Size              : Single;        // ������ ������� ���  ����������� �������. ��������, ���� ������ ��� �������, �� �� �����.
    Size2             : Single;        // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
    Size3             : Single;        // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
    UnitofMeasurement : THeaderStr;    // ��. ��������� ���� "Size"
    UnitofMeasurement2: THeaderStr;    // ��. ��������� ���� "Size2"
    UnitofMeasurement3: THeaderStr;    // ��. ��������� ���� "Size3"
    Comment           : THeaderBigStr; // ����������
    Operator_         : THeaderStr;    // ��������
    Device            : THeaderStr;    // ������
  end;
  pedNORDCO_Rec = ^edNORDCO_Rec;

  // �������: EID_GPSState - ��������� ��������� GPS

  GPSStateReserv = array [0..5] of Byte;

  edGPSState = packed record // 9 ����
    State:	Byte; //	���������
    UseSatCount: Byte; // ���������� ������������ ���������
    AntennaConnected: Byte; // �������: 0 � �� ���������� 1 - ����������
    Reserv: GPSStateReserv; // ������
  end;
  pedGPSState = ^edGPSState;

  // �������: EID_Media - �����������

  TMediaType = ( mtAudio = 1,
                 mtPhoto = 2,
                 mtVideo = 3);

  edMediaData = packed record
    DataType:	Byte; // ��� ������
    Size: DWord;
  end;
  pedMediaData = ^edMediaData;

  // ------------------------< ������ ������������ � ����� ����� >---------------------------------------

  TFileEvent = packed record
    ID: Byte;
    OffSet: Integer;
    SysCoord: Integer;
    DisCoord: Integer;
    Data: TEventData;
  end;

  // ?
  TFileNotebook_ver_5 = packed record
    ID: Byte; // ���: 1 - ��������� �������� / 2 - ������ ��������.
    Rail: TRail; // ����: 0 - ����� / 1 - ������
    DisCoord: Integer; // ���������� ����������
    StDisCoord: Integer; // ���������� ����������
    EdDisCoord: Integer; // ���������� ����������

    Defect: string[64]; // ������ - ��� / �������� ���
//    Defect: THeaderStr;
    BlokNotText: string[250]; // ����� ��������

    ManName: string[64]; // ������������ ���
    Privazka: string[100]; // ��������
    Prim: string[100]; // ������ - ����������
    DT: TDateTime;

    LeftKm1: Integer; // ��������
    LeftKm2: Integer; // ��������
    LeftPk1: Integer; // �����
    LeftPk2: Integer; // �����

    RightKm1: Integer; // ��������
    RightKm2: Integer; // ��������
    RightPk1: Integer; // �����
    RightPk2: Integer; // �����

    Zveno1: Integer; // �����1
    SysCoord: Integer; // �����2
    LeftMM: Integer; // ��������� (�� ������)
    RightMM: Integer; // ��������� (�� ������)
    Chema: Integer; // ����� ������: 1 - ������; 2 - ��������; 3 - ������.
    Zoom: Integer; // �������
    Reduction: Byte; // ��������: 0 - ��������� / 1 - ��� �1 / 1 - ��� �2

    ShowChGate_: Boolean;
    ShowChSettings_: Boolean;
    ShowChInfo_: Boolean;

    ViewMode: Byte; // ����� �����������: 1 - � ��������� / 2 - � ���� ������
    DrawRail: Byte; { TDrawData } // ������������ ���� 0 - ����� 1 - ������ 2 - ���
    AmplTh: Byte; // ����� �����������
    AmplDon: Byte; // ��������� ������� ������� 0 - ����
    ViewChannel: Byte; // ������: 0 - ��� / 1 - ���������� / 2 - �����������
    ViewLine: Byte;

    PathNum: Integer; // ����
    Peregon: string[20];
    FindAt: Integer; // ��������� ��� ..   0 - ������; 1 - �������; 2 - � ������� �����; 3 - 3-5 ����� ; 4 - ��������
    ViewType: Integer; // ������ ..          0 - ��� �����������; 1 - ���������� � ����

    REZERW: array [0..31] of Byte;
  end;

  TFileNotebook_ver_6 = packed record
    ID: Byte; // ���: 1 - ��������� �������� / 2 - ������ ��������.
    Rail: TRail; // ����: 0 - ����� / 1 - ������
    DisCoord: Integer; // ���������� ����������
    Defect: THeaderStr;
    BlokNotText: THeaderBigStr; // ����� ��������
    DT: TDateTime;
    SysCoord: Integer; // �����2
    Zoom: Integer; // �������
    Reduction: Byte; // ��������: 0 - ��������� / 1 - ��� �1 / 1 - ��� �2
    ShowChGate_: Boolean;
    ShowChSettings_: Boolean;
    ShowChInfo_: Boolean;
    ViewMode: Byte; // ����� �����������: 1 - � ��������� / 2 - � ���� ������
    DrawRail: Byte; { TDrawData } // ������������ ���� 0 - ����� 1 - ������ 2 - ���
    AmplTh: Byte; // ����� �����������
    AmplDon: Byte; // ��������� ������� ������� 0 - ����
    ViewChannel: Byte; // ������: 0 - ��� / 1 - ���������� / 2 - �����������
    ViewLine: Byte;
  end;

  TFileNotebook_ver_7 = packed record
    ID: Byte; // ���: 1 - ��������� �������� / 2 - ������ �������� / 3 - NORDCO
    Rail: TRail; // ����: 0 - ����� / 1 - ������
    DisCoord: Integer; // ���������� ����������
    Defect: THeaderStr;
                      // NORDCO::Type - ��� ������� �� UIC 712
    BlokNotText: THeaderBigStr; // ����� ��������
                      // NORDCO::Comment - ����������
    DT: TDateTime;
                 // NORDCO::SurveyDate - ���� ����������� �������
                 // NORDCO::DataEntryDate - ���� ����������� ������ ��������
    SysCoord: Integer; // �����2
    Zoom: Integer; // �������
    Reduction: Byte; // ��������: 0 - ��������� / 1 - ��� �1 / 1 - ��� �2
    ShowChGate_: Boolean;
    ShowChSettings_: Boolean;
    ShowChInfo_: Boolean;
    ViewMode: Byte; // ����� �����������: 1 - � ��������� / 2 - � ���� ������
    DrawRail: Byte; { TDrawData } // ������������ ���� 0 - ����� 1 - ������ 2 - ���
    AmplTh: Byte; // ����� �����������
    AmplDon: Byte; // ��������� ������� ������� 0 - ����
    ViewChannel: Byte; // ������: 0 - ��� / 1 - ���������� / 2 - �����������
    ViewLine: Byte;

                   // NORDCO ---------------------
    Line              : THeaderStr; // �������� ��� ������������� �������������� �����
    Track             : THeaderStr; // ������������� ����, � ������� ��������� ������
    LocationFrom      : Single; // ������ ������������� �������
    LocationTo        : Single; // ����� ������������� �������. � ������ ��������� ������� �� �� �����, ��� ����  "Location From"
    LatitudeFrom      : Double; // GPS-������ ��� ���� "Location From"
    LatitudeTo        : Double; // GPS-������ ��� ���� "Location To"
    LongitudeFrom     : Double; // GPS-������� ��� ���� "Location From"
    LongitudeTo       : Double; // GPS-������� ��� ���� "Location To"
    AssetType         : THeaderStr; // ��� �������� �������� ���� - �� ������: Left Rail, Right Rail
    Source            : THeaderStr; // ����� ����������� ������� - �� ������: Ultrasonic Trolley, Visual Inspection
    Status            : THeaderStr; // ��� ���� ���������, ������ �� ������ (�.�. ������ �� ����) - �� ������: Open, Closed
    IsFailure         : Boolean; // ���� ���������� ������ ������� �����, �� ��� ���� ������ ����� �������� TRUE. ������� ����� ������� ����� ������, ������� �������� ����������� ��������
    Size              : Single;                                                     // ������ ������� ���  ����������� �������. ��������, ���� ������ ��� �������, �� �� �����.
    Size2             : Single;                                                     // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
    Size3             : Single;                                                     // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
    UnitofMeasurement : THeaderStr;                                                     // ��. ��������� ���� "Size"
    UnitofMeasurement2: THeaderStr;                                                     // ��. ��������� ���� "Size2"
    UnitofMeasurement3: THeaderStr;                                                     // ��. ��������� ���� "Size3"
    Operator_         : THeaderStr;                                                     // ��������
    Device            : THeaderStr;                                                     // ������
  end;

  TFileNotebook = TFileNotebook_ver_7;

  pFileNotebook = ^TFileNotebook;

  TExHeaderHead = packed record
    FFConst: array [0 .. 13] of Byte; // ��������� - 14 x 0xFFH
    DataSize: Integer;                // ������ ���������
    DataVer: Integer;                 // ������ ������ = 5
  end;

  TExHeader_Ver_7 = packed record
    FFConst: array [0..13] of Byte;     // ��������� - 14 x 0xFFH
    DataSize: Integer;                  // ������ ���������
    DataVer: Integer;                   // ������ ������ = 7
    EndKM: Integer;                     // �������� �������� ��� �������� Chainage
    EndPK: Integer;                     // �������� �����
    EndMM: Integer;                     // ��������� �� ���������� ������
    CoordReserv: array [0..63] of Byte; // ������ 0
    EndSysCoord: Integer;               // �������� ��������� ����������
    EndDisCoord: Integer;               // �������� ���������� ����������
    Reserv1: array [0..63] of Byte;     // ������ 1

    EventDataVer: Integer;  // ������ ������� ������ ������� �������� ������� ����� (������: 5)
    EventItemSize: Integer; // ������ 1 �������� ������� �������� ������� �����
    EventOffset: Integer;   // ������ �� ������� �������� ������� �����
    EventCount: Integer;    // ���������� ��������� ������� �������� ������� �����

    NotebookDataVer: Integer;  // ������ ������� ������ ������� ���������� �������� � ������� �������� (������: 5)
    NotebookItemSize: Integer; // ������ 1 �������� ������� ���������� �������� � ������� ��������
    NotebookOffset: Integer;   // ������ �� ������� ���������� �������� � ������� ��������
    NotebookCount: Integer;    // ���������� ��������� ������� ���������� �������� � ������� ��������

    Reserv2: array [0..127] of Byte; // ������ 2
  end;

  TExHeader_Ver_8 = packed record
    FFConst: array [0..13] of Byte;     // ��������� - 14 x 0xFFH
    DataSize: Integer;                  // ������ ���������
    DataVer: Integer;                   // ������ ������ = 7
    EndMRFCrd: TMRFCrd;                 // ������ 12 ����
    EndCaCrd: TCaCrd;                   // ������ 8 ����
    CoordReserv: array [0..55] of Byte; // ������ 0
    EndSysCoord: Integer;               // �������� ��������� ����������
    EndDisCoord: Integer;               // �������� ���������� ����������
    Reserv1: array [0..63] of Byte;     // ������ 1

    EventDataVer: Integer;  // ������ ������� ������ ������� �������� ������� ����� (������: 5)
    EventItemSize: Integer; // ������ 1 �������� ������� �������� ������� �����
    EventOffset: Integer;   // ������ �� ������� �������� ������� �����
    EventCount: Integer;    // ���������� ��������� ������� �������� ������� �����

    NotebookDataVer: Integer;  // ������ ������� ������ ������� ���������� �������� � ������� �������� (������: 5)
    NotebookItemSize: Integer; // ������ 1 �������� ������� ���������� �������� � ������� ��������
    NotebookOffset: Integer;   // ������ �� ������� ���������� �������� � ������� ��������
    NotebookCount: Integer;    // ���������� ��������� ������� ���������� �������� � ������� ��������

    Reserv2: array [0..127] of Byte; // ������ 2
  end;

  TExHeader = TExHeader_Ver_8;


//------------------------< GPS >-----------------------------------------------------------

  TGPSPoint = record
    State: Boolean;
    Lat: Single;
    Lon: Single;
    SatCount: Integer;
    Speed: Single;
    DisCoord: Integer;
    {
    KM: Integer;
    Metre: Single;
    LabelExists: Boolean;
    LabelText: string;
    StolbExists: Boolean;
    LeftKm: Smallint;
    LeftPk: Byte;
    RightKm: Smallint;
    RightPk: Byte;
    }
  end;

// ---------< ������������� ����� >--------------------------------------------------------------------

  TStolbSide = (ssLeft, ssRight);

  TStolbSideData = record
    Km: Integer;
    Pk: Integer;
  end;

  TStolb = array [TStolbSide] of TStolbSideData;

  TMRFCrdParams = record // ���������� � ���� ������� ������������� ����� � ������ �� ����� ��� ����������� �� ������� ���������
    LeftStolb: TStolb;
    ToLeftStolbMM: Integer;
    LeftIdx: Integer;
    RightStolb: TStolb;
    ToRightStolbMM: Integer;
    RightIdx: Integer;
  end;

  TCaCrdParams = record // ���������� � ���� ������� ������������� ����� � ������ �� ����� ��� ����������� �� ������� ���������
    LeftPostVal: TCaCrd;
    ToLeftPostMM: Extended;
    LeftIdx: Integer;
    RightPostVal: TCaCrd;
    ToRightPostMM: Extended;
    RightIdx: Integer;
    Sys: TCoordSys;
  end;

  TCrdParams = record // ���������� � ���� ������� ������������� ����� � ������ �� �����
    CaCrdParams: TCaCrdParams;
    MRFCrdParams: TMRFCrdParams;
    Sys: TCoordSys;
  end;

  TRealCoord = record
    MRFCrd: TMRFCrd;
    CaCrd: TCaCrd;
    Sys: TCoordSys;
  end;

  TEchoBlock = array[0..7] of packed record
                                T: Byte;
                                A: Byte;
                              end;

  TDestDat = packed record
    DelayOffset: array [1..16] of Integer;
    Delay: array [1..16] of Byte;
    Ampl: array [1..16] of Byte;
    Count: Integer;
//    ZerroFlag: Boolean;
//    ZerroFlag2: Boolean;
  end;

  TCurEcho = array [TRail, 0..255] of TDestDat;

  // -------------< ������� ��������� >-----------------------------------

  procedure SetCrdToStrCaptions(KM, PK, Metr, SantiMetr, Milimetr, Imperial, Metric0_1km, Metric1km: string);
  function PostMMLen(CoordSys: TCoordSys): Integer;

  // -------------< ������� ���������, ����������� �� >-----------------------------------

  function GetPrevMRFPost(Post: TMRFCrd; MoveDir: Integer): TMRFCrd; // �������� ���������� �����
  function GetNextMRFPost(Post: TMRFCrd; MoveDir: Integer): TMRFCrd;
  function MRFCrdParamsToCrd(CoordParams: TMRFCrdParams; MoveDir: Integer): TMRFCrd;
  function MRFCrdToStr(Data: TMRFCrd; Index: Integer = 2) : string;

  // -------------< ������� ���������, ��������� >----------------------------------------------------

  function GetPrevCaPost(Sys: TCoordSys; Val: TCaCrd; MoveDir: Integer): TCaCrd; // �������� ���������� �����
  function GetNextCaPost(Sys: TCoordSys; Val: TCaCrd; MoveDir: Integer): TCaCrd;
  function CaCrdParamsToCrd(CoordParams: TCaCrdParams; MoveDir: Integer): TCaCrd;
  function CaCrdToStr(Sys: TCoordSys; Val: TCaCrd): string;
  function CaCrdToVal(Sys: TCoordSys; Val: TCaCrd): Single;
//  function CaCrdToStr(Val: TCaCrd): string; overload;
  function CaCrdToStr0(Sys: TCoordSys; Val: TCaCrd): string;
  function GetMMLenFromCaPost(Sys: TCoordSys; Post: TCaCrd): Extended; // ��������� �� ������
  function CaCrdToMM(Sys: TCoordSys; Val: TCaCrd): Extended;
  function GetMMLenFromTwoCaCrd(Sys: TCoordSys; FromCaCrd, ToCaCrd: TCaCrd): Extended;
  function StrToCaCrd(CoordSys: TCoordSys; Text: string): TCaCrd;

  // -------------------------------------------------------------------------------------------------

  function CrdParamsToRealCrd(CrdParams: TCrdParams; MoveDir: Integer): TRealCoord;
  function RealCrdToStr(Crd: TRealCoord; Index: Integer = 2) : string;
  function RealCoordToReal_(Crd: TRealCoord): Integer;

  // -----------< ������ � ������� >-------------------------------------------------------

  function HeaderStrToString(Text: THeaderStr): string;
  function StringToHeaderStr(Text: string): THeaderStr;
  function HeaderBigStrToString(Text: THeaderBigStr): string;
  function StringToHeaderBigStr(Text: string): THeaderBigStr;
  function GetDeviceNameTextId(DeviceID: TAviconID): string;
  function GetInspMethodTextId(Metod: TInspectionMethod; FullName: Boolean): string;
  function GetInspZoneTextId(Zone: TInspectionZone; FullName: Boolean): string;

  // ---------- Tools ----------------------------------------------------------------------

  function GetRealCrd_inMM(ScanCh: TScanChannelItem; Delay: Byte; isEGOUSW_BHead: Boolean): Single;  // ��������� ���������� ���������� �� ������ �� [��]
  function GetRealCrd_inSysCrd(ScanCh: TScanChannelItem; ScanStep: Integer; Delay: Integer; isEGOUSW_BHead: Boolean): Single; // ��������� ���������� ���������� �� ������ �� [����. �����.]
  function GetMoveCrd_inSysCrd(ScanCh: TScanChannelItem; ScanStep: Integer; Delay: Integer): Single;
  function DelayToMMLen(ScanCh: TScanChannelItem; Delay: Byte): Single; // �� ����
  function UpgradeStr(Src: string; Ch: Char; Len: Integer): string;
  function UpgradeInt(Int: Integer; Ch: Char; Len: Integer): string;

  // ---------------------------------------------------------------------------------------

  function GetSurf(ChID: Integer): Integer;

  function InvertGPSSingle(Src: Single): Single;
  function GPSSingleToText( A: Single; Lat: Boolean ): string; //  ������ = Latitude


const
  DeviceUnitTypeToTextId: array [TDeviceUnitType] of string = ('UpUnit', 'DownUnit', 'MiddleUnit');

implementation

uses
  MMSystem, Math;

var
 FKM_Name: string;
 FPK_Name: string;
 FMetr_Name: string;
 FSantiMetr_Name: string;
 FMilimetr_Name: string;
 FImperial_Name: string;
 FMetric0_1km_Name: string;
 FMetric1km_Name: string;

procedure SetCrdToStrCaptions(KM, PK, Metr, SantiMetr, Milimetr, Imperial, Metric0_1km, Metric1km: string);
begin
 FKM_Name:= KM;
 FPK_Name:= PK;
 FMetr_Name:= Metr;
 FSantiMetr_Name:= SantiMetr;
 FMilimetr_Name:= Milimetr;
 FImperial_Name:= Imperial;
 FMetric0_1km_Name:= Metric0_1km;
 FMetric1km_Name:= Metric1km;
end;

function PostMMLen(CoordSys: TCoordSys): Integer;
begin
  case CoordSys of
     csMetricRF: Result:= 100000;
     csImperial: Result:= 30480;
  csMetric0_1km: Result:= 100000;
    csMetric1km: Result:= 100000;
 csMetric1kmTurkish: Result:= 100000;
          else Result:= 100000;
  end;
end;

  // -------------< ������� ���������, ����������� �� >-----------------------------------

function GetPrevMRFPost(Post: TMRFCrd; MoveDir: Integer): TMRFCrd; // �������� ���������� �����
begin
  if MoveDir > 0 then
  begin
    Dec(Post.Pk);
    if Post.Pk = 0 then
    begin
      Post.Pk:= 10;
      Dec(Post.Km);
    end;
  end
  else
  begin
    Inc(Post.Pk);
    if Post.Pk = 11 then
    begin
      Post.Pk:= 1;
      Inc(Post.Km);
    end;
  end;
  Result:= Post;
end;

function GetNextMRFPost(Post: TMRFCrd; MoveDir: Integer): TMRFCrd;
begin
  if MoveDir > 0 then
  begin
    Inc(Post.Pk);
    if Post.Pk = 11 then
    begin
      Post.Pk:= 1;
      Inc(Post.Km);
    end;
  end
  else
  begin
    Dec(Post.Pk);
    if Post.Pk = 0 then
    begin
      Post.Pk:= 10;
      Dec(Post.Km);
    end;
  end;
  Result:= Post;
end;

function MRFCrdParamsToCrd(CoordParams: TMRFCrdParams; MoveDir: Integer): TMRFCrd;
begin
  Result.Km:= CoordParams.LeftStolb[ssRight].Km;
  Result.Pk:= CoordParams.LeftStolb[ssRight].Pk;
  if MoveDir > 0 then Result.mm:= CoordParams.ToLeftStolbMM
                 else Result.mm:= CoordParams.ToRightStolbMM;
end;

function MRFCrdToStr(Data: TMRFCrd; Index: Integer = 2) : string;
var
  M, MM: Integer;

begin
//  M:= Round(1000 * Frac(Data.mm div 1000 / 1000));
  M:= Data.mm div 1000;
  MM:= Round(1000 * Frac(Data.mm / 1000));

  case Index of
    0: Result:= Format('%D ' + FKM_Name + ' %D ' + FPK_Name + ' %D ' + FMetr_Name + ' %D ' + FMilimetr_Name, [Data.Km, Data.Pk, M, MM]);
    1: Result:= Format('%D ' + FKM_Name + ' %D ' + FPK_Name + ' %D ' + FMetr_Name + ' %D ' + FSantiMetr_Name, [Data.Km, Data.Pk, M, MM div 10]);
    2: Result:= Format('%D ' + FKM_Name + ' %D ' + FPK_Name + ' %D ' + FMetr_Name + '', [Data.Km, Data.Pk, M]);
    3: Result:= Format('%D / %D / %D', [Data.Km, Data.Pk, M]);
  end;
end;

  // -------------< ������� ���������, ��������� >----------------------------------------------------

function GetPrevCaPost(Sys: TCoordSys; Val: TCaCrd; MoveDir: Integer): TCaCrd; // �������� ����� ����� �� �����
begin
  if MoveDir > 0 then
  begin                       // OK
    Result.XXX:= Val.XXX;
    Result.YYY:= 0;
  end
  else
  begin                       // OK
    Result.XXX:= Val.XXX;
    Result.YYY:= 0;
  end;
end;

function GetNextCaPost(Sys: TCoordSys; Val: TCaCrd; MoveDir: Integer): TCaCrd;
var
  S: string;
  I: Integer;

begin
  if MoveDir > 0 then
  begin
    Result.XXX:= Val.XXX + 1; // OK
    Result.YYY:= 0;
  end
  else
  begin
    Result:= Val;
    if Result.YYY = 0 then Result.XXX:= Val.XXX - 1
                      else Result.YYY:= 0;
  end;
end;

function CaCrdToMM(Sys: TCoordSys; Val: TCaCrd): Extended;
var
  t1: Extended;
  t2: Extended;

begin
  case Sys of
     csImperial:
                Result:= {Trunc(}Int64(Val.XXX) *   30480 + Val.YYY * 30.480{)};
     csMetric0_1km, csMetric1km, csMetric1kmTurkish:
                Result:= {Trunc(}Int64(Val.XXX) *  100000 + Val.YYY *    100{)};
     else
                Result:= 0;
  end;
end;

function GetMMLenFromTwoCaCrd(Sys: TCoordSys; FromCaCrd, ToCaCrd: TCaCrd): Extended;
begin
  Result:=  CaCrdToMM(Sys, ToCaCrd) - CaCrdToMM(Sys, FromCaCrd);
end;


function MMToCaCrd(CoordSys: TCoordSys; MMVal: Extended): TCaCrd;
begin
  case CoordSys of
     csImperial: begin
                   Result.XXX:= Trunc(MMVal / 30480);
                   Result.YYY:= Round(1000 * Frac(MMVal / 30480));
                   if Result.YYY > 999 then Result.YYY:= 999;
                 end;
  csMetric0_1km,csMetric1kmTurkish,csMetric1km:
                 begin
                   Result.XXX:= Trunc(MMVal / 100000);
                   Result.YYY:= Round(1000 * Frac(MMVal / 100000));
                   if Result.YYY > 999 then Result.YYY:= 999;
                 end;
            else begin
                   Result.XXX:= 0;
                   Result.YYY:= 0;
                 end;
  end;
end;

function AddMMToCaCrd(CoordSys: TCoordSys; Chainage: TCaCrd; MMVal: Extended): TCaCrd;
begin
  Result:= MMToCaCrd(CoordSys, CaCrdToMM(CoordSys, Chainage) + MMVal);
end;

function CaCrdParamsToCrd(CoordParams: TCaCrdParams; MoveDir: Integer): TCaCrd;
begin
  if MoveDir > 0 then Result:= AddMMToCaCrd(CoordParams.Sys, CoordParams.LeftPostVal, CoordParams.ToLeftPostMM)
                 else Result:= AddMMToCaCrd(CoordParams.Sys, CoordParams.RightPostVal, CoordParams.ToRightPostMM);
end;

function CaCrdToStr(Sys: TCoordSys; Val: TCaCrd): string;
var
  XXX: string;
  YYY: string;
  X: string;
  Y: string;
  Sign: Integer;

begin
  if { (Val.XXX < 0) or } (Val.YYY < 0) then Sign:= - 1 else Sign:= + 1;
  if Sys = csMetric1kmTurkish then begin
    XXX:= UpgradeInt(Abs(Val.XXX), '0', 4);
    if Val.XXX < 0 then XXX:='-' + XXX;
  end
  else
    XXX:= IntToStr(Val.XXX);
  YYY:= UpgradeInt(Abs(Val.YYY), '0', 3);

  case Sys of
     csImperial: Result:= XXX + '.' + YYY;
  csMetric0_1km: Result:= XXX + '.' + YYY;
    csMetric1km,csMetric1kmTurkish:
                 begin
                   X:= Copy(XXX, 1, Length(XXX) - 1);
                   if X='' then X:='0';
                   if X='-' then X:='-0';

                   Y:= Copy(XXX, Length(XXX), 1) + Copy(YYY, 1, 2);
                   if Sys = csMetric1km then
                     Result:= X + '.' + Y
                   else
                     Result:= X + '+' + Y;
                 end;
  end;
  if (Sign < 0) and (Val.XXX >= 0) then Result:= '-' + Result;
end;

function CaCrdToStr0(Sys: TCoordSys; Val: TCaCrd): string;
var
  XXX: string;
  YYY: string;
  X: string;
  Y: string;
  Sign: Integer;

begin
  if { (Val.XXX < 0) or } (Val.YYY < 0) then Sign:= - 1 else Sign:= + 1;
  if Sys = csMetric1kmTurkish then
    XXX:= UpgradeInt(Abs(Val.XXX), '0', 4)
  else
    XXX:= UpgradeInt(Abs(Val.XXX), '0', 5);
  YYY:= UpgradeInt(Abs(Val.YYY), '0', 3);

  case Sys of
     csImperial: Result:= XXX + '.' + YYY;
  csMetric0_1km: Result:= XXX + '.' + YYY;
    csMetric1km,csMetric1kmTurkish:
                 begin
                   X:= Copy(XXX, 1, Length(XXX) - 1);
//                   if X='' then X:='0';
//                   if X='-' then X:='-0';
                   Y:= Copy(XXX, Length(XXX), 1) + Copy(YYY, 1, 2);
                   if Sys = csMetric1km then
                     Result:= X + '.' + Y
                   else
                     Result:= X + '+' + Y;
                 end;
  end;
  if (Sign < 0) and (Val.XXX >= 0) then Result:= '-' + Result;
end;

function CaCrdToVal(Sys: TCoordSys; Val: TCaCrd): Single;
var
  XXX: string;
  YYY: string;
  X: string;
  Y: string;
  Result_: string;
  Sign: Integer;
  SaveDecimalSeparator: Char;

begin
  if { (Val.XXX < 0) or } (Val.YYY < 0) then Sign:= - 1 else Sign:= + 1;
  if Sys = csMetric1kmTurkish then
    XXX:= UpgradeInt(Abs(Val.XXX), '0', 4)
  else
    XXX:= IntToStr(Val.XXX);
  YYY:= UpgradeInt(Abs(Val.YYY), '0', 3);

  case Sys of
     csImperial: Result_:= XXX + '.' + YYY;
  csMetric0_1km: Result_:= XXX + '.' + YYY;
    csMetric1km,csMetric1kmTurkish:
                 begin
                   X:= Copy(XXX, 1, Length(XXX) - 1);
                   Y:= Copy(XXX, Length(XXX), 1) + Copy(YYY, 1, 2);
                   if Sys = csMetric1km then
                     Result_:= X + '.' + Y
                   else
                     Result_:= X + '.' + Y;
                 end;
  end;
  if (Sign < 0) and (Val.XXX >= 0) then Result_:= '-' + Result_;

  {$IFDEF VER210}
  SaveDecimalSeparator:= DecimalSeparator;
  DecimalSeparator:= '.';
  Result:= StrToFloat(Result_);
  DecimalSeparator:= SaveDecimalSeparator;
  {$ENDIF}

  {$IFDEF VER260}
  SaveDecimalSeparator:= FormatSettings.DecimalSeparator;
  FormatSettings.DecimalSeparator:= '.';
  Result:= StrToFloat(Result_);
  FormatSettings.DecimalSeparator:= SaveDecimalSeparator;
  {$ENDIF}

  (*
{$IFDEF VER220} - Delphi XE
{$IFDEF VER230} - Delphi XE2
{$IFDEF VER240} - Delphi XE3
{$IFDEF VER250} - Delphi XE4
{$IFDEF VER260} - Delphi XE5
{$IFDEF VER265} - Appmethod 1.0
{$IFDEF VER270} - Delphi XE6
{$IFDEF VER280} - Delphi XE7
{$IFDEF VER290} - Delphi XE8
{$IFDEF VER300} - Delphi 10 Seattle
  *)
end;

function GetMMLenFromCaPost(Sys: TCoordSys; Post: TCaCrd): Extended;
begin
  case Sys of
     csImperial: Result:= 30480 * Post.YYY / 1000;
  csMetric0_1km, csMetric1km, csMetric1kmTurkish : Result:= 100000 * Post.YYY / 1000;
    else Result:= 0;
  end;
end;

function StrToCaCrd(CoordSys: TCoordSys; Text: string): TCaCrd;
var
  I: Integer;

begin
  Result.XXX:= 0;
  Result.YYY:= 0;

  if CoordSys = csMetric1kmTurkish then
    I:= Pos('+', Text)
  else
    I:= Pos('.', Text);
  if I = 0 then Exit;
  Delete(Text, I, 1);
 try
  case CoordSys of
     csImperial,
  csMetric0_1km:
                begin
                   Result.XXX:= StrToInt(Copy(Text, 1, I - 1));
                   Result.YYY:= StrToInt(Copy(Text, I, Length(Text)));
                end;

    csMetric1km,csMetric1kmTurkish :
                 begin
                   Result.XXX:= StrToInt(Copy(Text, 1, I - 1) + Copy(Text, I, 1));
                   Result.YYY:= StrToInt(Copy(Text, I + 1, Length(Text)) + '0');
                 end;
            else begin
                   Result.XXX:= 0;
                   Result.YYY:= 0;
                 end;

{    csMetric1km: begin
                   Result.XXX:= StrToInt(Copy(Text, 1, I));
                   Result.YYY:= StrToInt(Copy(Text, I + 1, Length(Text)));
                 end; }
  end;
 except
                   Result.XXX:= 0;
                   Result.YYY:= 0;
 end;
end;

// -------------------------------------------------------------------------------------------------

function CrdParamsToRealCrd(CrdParams: TCrdParams; MoveDir: Integer): TRealCoord;
begin
  Result.Sys:= CrdParams.Sys;
  if CrdParams.Sys = csMetricRF then Result.MRFCrd:= MRFCrdParamsToCrd(CrdParams.MRFCrdParams, MoveDir)
                                else Result.CaCrd:= CaCrdParamsToCrd(CrdParams.CaCrdParams, MoveDir);
end;

function RealCrdToStr(Crd: TRealCoord; Index: Integer = 2) : string;
begin
  if Crd.Sys = csMetricRF then Result:= MRFCrdToStr(Crd.MRFCrd, Index)
                          else Result:= CaCrdToStr(Crd.Sys, Crd.CaCrd);
end;

function RealCoordToReal_(Crd: TRealCoord): Integer;
begin
  case Crd.Sys of
     csMetricRF: Result:= Crd.MRFCrd.Km * 1000 + (Crd.MRFCrd.Pk - 1) * 100 + Crd.MRFCrd.mm div 1000;
     csImperial,
  csMetric0_1km,
    csMetric1km,
csMetric1kmTurkish: Result:= Round(CaCrdToMM(Crd.Sys, Crd.CaCrd) / 1000);
  end;
end;


// -----------< ������ � ������� >--------------------------------------------------------------------



function GetDeviceNameTextId(DeviceID: TAviconID): string;
begin
  if CompareMem(@DeviceID, @Avicon16Scheme1, 8) then Result:= 'Avicon16Scheme1';  // ������-16 ����� 1
  if CompareMem(@DeviceID, @Avicon16Scheme2, 8) then Result:= 'Avicon16Scheme2';  // ������-16 ����� 2
  if CompareMem(@DeviceID, @Avicon16Scheme3, 8) then Result:= 'Avicon16Scheme3';  // ������-16 ����� 3
  if CompareMem(@DeviceID, @Avicon16Wheel, 8) then Result:= 'Avicon16Wheel';      // ������-16 �������� �����
  if CompareMem(@DeviceID, @Avicon11Scheme0, 8) then Result:= 'Avicon11Scheme0';  // ������-11
  if CompareMem(@DeviceID, @Avicon15Scheme1, 8) then Result:= 'Avicon15Scheme1';  // ������-15
  if CompareMem(@DeviceID, @Avicon14Scheme1, 8) then Result:= 'Avicon14Scheme1';  // ������-14 ����������
  if CompareMem(@DeviceID, @Avicon14Wheel, 8) then Result:= 'Avicon14Wheel';      // ������-14 �������� �����
  if CompareMem(@DeviceID, @USK003R, 8) then Result:= 'USK004R';                  // USK004R (�������) - ������ ������ (�����)
  if CompareMem(@DeviceID, @USK004R, 8) then Result:= 'USK004R';                  // USK004R (�������)
  if CompareMem(@DeviceID, @EGO_USWScheme1, 8) then Result:= 'EGO_USWScheme1';  // EGO_USW ����� 1
  if CompareMem(@DeviceID, @EGO_USWScheme2, 8) then Result:= 'EGO_USWScheme2';  // EGO_USW ����� 2
  if CompareMem(@DeviceID, @EGO_USWScheme3, 8) then Result:= 'EGO_USWScheme3';  // EGO_USW ����� 3
  if CompareMem(@DeviceID, @Avicon31Scheme1, 8) then Result:= 'Avicon31Scheme1';                  // ������-12 ��� �������

//  if CompareMem(@DeviceID, @FilusX27Wheel, 8) then Result:= 'FilusX27Wheel';      // FilusX27 �������� �����
end;

function GetInspMethodTextId(Metod: TInspectionMethod; FullName: Boolean): string;
begin
  if FullName then
    case Metod of
            imEcho: Result:= 'InspMethod_FULL:Echo';
    imMirrorShadow: Result:= 'InspMethod_FULL:MirrorShadow';
          imMirror: Result:= 'InspMethod_FULL:Mirror';
          imShadow: Result:= 'InspMethod_FULL:Shadow';
    end
  else
    case Metod of
            imEcho: Result:= 'InspMethod_SHORT:Echo';
    imMirrorShadow: Result:= 'InspMethod_SHORT:MirrorShadow';
          imMirror: Result:= 'InspMethod_SHORT:Mirror';
          imShadow: Result:= 'InspMethod_SHORT:Shadow';
    end;
end;

function GetInspZoneTextId(Zone: TInspectionZone; FullName: Boolean): string;
begin
  if FullName then
    case Zone of
       izAll: Result:= 'InspZone_SHORT:All';
      izHead: Result:= 'InspZone_SHORT:Head';
       izWeb: Result:= 'InspZone_SHORT:Web';
      izBase: Result:= 'InspZone_SHORT:Base';
    end
  else
    case Zone of
       izAll: Result:= 'InspZone_SHORT:All';
      izHead: Result:= 'InspZone_SHORT:Head';
       izWeb: Result:= 'InspZone_SHORT:Web';
      izBase: Result:= 'InspZone_SHORT:Base';
    end
end;

function HeaderStrToString(Text: THeaderStr): string;
var
  I: Integer;

begin
  Result:= '';
  for I:= 1 to Word(Text[0]) do
    if (Text[I] <> '_') and (Text[I] <> Chr($A)) then Result:= Result + Text[I];
end;

function StringToHeaderStr(Text: string): THeaderStr;
var
  I: Integer;

begin
  Result[0]:= Char(Length(Text));
  for I:= 1 to Length(Text) do Result[I]:= Text[I];
end;

function HeaderBigStrToString(Text: THeaderBigStr): string;
var
  I: Integer;

begin
  Result:= '';
  for I:= 1 to Word(Text[0]) do Result:= Result + Text[I];
end;

function StringToHeaderBigStr(Text: string): THeaderBigStr;
var
  I: Integer;

begin
  Result[0]:= Char(Length(Text));
  for I:= 1 to Length(Text) do Result[I]:= Text[I];
end;

// ---------- Tools ------------------------------------------------------------

function GetRealCrd_inMM(ScanCh: TScanChannelItem; Delay: Byte; isEGOUSW_BHead: Boolean): Single;

var
  Test1: single;
  Test2: single;
  Test3: single;

begin
  with ScanCh do
  begin
    Test1:= sin(Abs(EnterAngle) * pi / 180);
    Test2:= cos(TurnAngle * pi / 180);
    Test3:= DelayToMMLen(ScanCh, Delay);
    Result:= Test3 * Test1 * Test2;

    if not isEGOUSW_BHead then
    begin
      if EnterAngle > 0 then Result:= Position + Result
                        else Result:= Position - Result;
    end
    else
    begin
      if EnterAngle > 0 then Result:= Position - Result
                        else Result:= Position + Result;
    end;
  end;
end;

function GetRealCrd_inSysCrd(ScanCh: TScanChannelItem; ScanStep: Integer; Delay: Integer; isEGOUSW_BHead: Boolean): Single;
begin
  if Delay < 1000 then Result:= GetRealCrd_inMM(ScanCh, Delay, isEGOUSW_BHead) / (ScanStep / 100)
                  else Result:= 0;
end;

function GetMoveCrd_inSysCrd(ScanCh: TScanChannelItem; ScanStep: Integer; Delay: Integer): Single;
begin
  with ScanCh do
  begin
//    Result:= DelayToMMLen(ScanCh, Delay) * sin(Abs(EnterAngle) * pi / 180) * cos(TurnAngle * pi / 180);
    if EnterAngle > 0 then Result:= - Position //+ Result
                      else Result:= - Position; //- Result;
  end;
  Result:= Result / (ScanStep / 100);
end;

function DelayToMMLen(ScanCh: TScanChannelItem; Delay: Byte): Single; // �� ����
begin
  if Abs(ScanCh.EnterAngle) < 20 then Result:= 5.9 * Delay / 2
                                 else Result:= 3.26 * Delay / 2;
end;

function UpgradeStr(Src: string; Ch: Char; Len: Integer): string;
begin
  Result:= Src;
  while Length(Result) < Len do Result:= Ch + Result;
end;

function UpgradeInt(Int: Integer; Ch: Char; Len: Integer): string;
begin
  Result:= IntToStr(Int);
  while Length(Result) < Len do Result:= Ch + Result;
end;

// -------------------------------------------------------------------------------------------

              {
function GetDir(ChID: Integer): Integer;
begin
  case ChID of
    MIRRORSHADOW_0           : Result:= ;
    ECHO_0                   : Result:= ;
    FORWARD_BOTH_ECHO_58     : Result:= ;
    FORWARD_WORK_ECHO_58     : Result:= ;
    FORWARD_UWORK_ECHO_58    : Result:= ;
    BACKWARD_BOTH_ECHO_58    : Result:= ;
    BACKWARD_WORK_ECHO_58    : Result:= ;
    BACKWARD_UWORK_ECHO_58   : Result:= ;
    FORWARD_BOTH_MIRROR_58   : Result:= ;
    FORWARD_WORK_MIRROR_58   : Result:= ;
    FORWARD_UWORK_MIRROR_58  : Result:= ;
    BACKWARD_BOTH_MIRROR_58  : Result:= ;
    BACKWARD_WORK_MIRROR_58  : Result:= ;
    BACKWARD_UWORK_MIRROR_58 : Result:= ;
    FORWARD_ECHO_70          : Result:= ;
    BACKWARD_ECHO_70         : Result:= ;
    FORWARD_ECHO_42_WEB      : Result:= ;
    BACKWARD_ECHO_42_WEB     : Result:= ;
    FORWARD_ECHO_42_BASE     : Result:= ;
    BACKWARD_ECHO_42_BASE    : Result:= ;
  end;
end;   }

function GetSurf(ChID: Integer): Integer;
begin
  case ChID of
    MIRRORSHADOW_0           : Result:= 1;
    ECHO_0                   : Result:= 1;
    FORWARD_BOTH_ECHO_58     : Result:= 1;
    FORWARD_WORK_ECHO_58     : Result:= 1;
    FORWARD_UWORK_ECHO_58    : Result:= 2;
    BACKWARD_BOTH_ECHO_58    : Result:= 1;
    BACKWARD_WORK_ECHO_58    : Result:= 1;
    BACKWARD_UWORK_ECHO_58   : Result:= 2;
    FORWARD_BOTH_MIRROR_58   : Result:= 1;
    FORWARD_WORK_MIRROR_58   : Result:= 1;
    FORWARD_UWORK_MIRROR_58  : Result:= 2;
    BACKWARD_BOTH_MIRROR_58  : Result:= 1;
    BACKWARD_WORK_MIRROR_58  : Result:= 1;
    BACKWARD_UWORK_MIRROR_58 : Result:= 2;
    FORWARD_ECHO_70          : Result:= 1;
    BACKWARD_ECHO_70         : Result:= 1;
    FORWARD_ECHO_42_WEB      : Result:= 1;
    BACKWARD_ECHO_42_WEB     : Result:= 1;
    FORWARD_ECHO_42_BASE     : Result:= 1;
    BACKWARD_ECHO_42_BASE    : Result:= 1;
    FORWARD_ECHO_65          : Result:= 1;
    BACKWARD_ECHO_65         : Result:= 1;
    SIDE_ECHO_65             : Result:= 1;
    WORK_MSM_55              : Result:= 1;
    UWORK_MSM_55             : Result:= 1;
    FORWARD_WORK_ECHO_65     : Result:= 1;
    FORWARD_UWORK_ECHO_65    : Result:= 2;
    BACKWARD_WORK_ECHO_65    : Result:= 1;
    BACKWARD_UWORK_ECHO_65   : Result:= 2;
    FORWARD_WORK_ECHO_50     : Result:= 1;
    FORWARD_UWORK_ECHO_50    : Result:= 2;
    BACKWARD_WORK_ECHO_50    : Result:= 1;
    BACKWARD_UWORK_ECHO_50   : Result:= 2;
    FORWARD_ECHO_45_WEB      : Result:= 1;
    BACKWARD_ECHO_45_WEB     : Result:= 1;
    FORWARD_ECHO_45_BASE     : Result:= 1;
    BACKWARD_ECHO_45_BASE    : Result:= 1;
  end;


end;


function InvertGPSSingle(Src: Single): Single;
var
  Tmp1: array [1..4] of Byte;
  Tmp2: array [1..4] of Byte;

begin
  Move(Src, Tmp1, 4);
  Tmp2[1]:= Tmp1[4];
  Tmp2[2]:= Tmp1[3];
  Tmp2[3]:= Tmp1[2];
  Tmp2[4]:= Tmp1[1];
  Move(Tmp2, Result, 4);

//  Result:= Src;
end;

function GPSSingleToText( A: Single; Lat: Boolean ): string; //  ������ = Latitude
var
  D, M, S: LongInt;
  SW: string;

begin
  try

    if Lat then //  ������ = Latitude
    begin

      if (A >= -90) and (A <= 90) then
      begin
        D:= Trunc( A );
        M:= Trunc( ( A - D ) * 60 );
        S:= Trunc( ((( A - D ) * 3600 ) - M * 60) * 100 );
        if A >= 0 then SW:= 'N' else SW:= 'S';
        Result:= Format( '%s:%d�%d.%d', [SW, D, M, S ] );
      end else Result:= '';
    end
    else
    begin
      if (A >= -180) and (A <= 180) then
      begin
        D:= Trunc( A );
        M:= Trunc( ( A - D ) * 60 );
        S:= Trunc( ((( A - D ) * 3600 ) - M * 60) * 100 );
        if A >= 0 then SW:= 'E' else SW:= 'W';
        Result:= Format( '%s:%d�%d.%d', [SW, D, M, S ] );
      end else Result:= '';
    end;

  except

    Result:= '';

  end;


end;
{
function GPSSingleToText( A: Single; Lat: Boolean ): string; //  ������ = Latitude
var
  D, M, S: LongInt;
  SW: string;

begin
  try

    if Lat then //  ?????? = Latitude
    begin

      if (A >= -90) and (A <= 90) then
      begin
        D:= Trunc( A );
        M:= Trunc( ( A - D ) * 60 );
        S:= Trunc( ((( A - D ) * 3600 ) - M * 60) * 100 );
        if A >= 0 then SW:= 'N' else SW:= 'S';
        Result:= Format( '%s:%d�%d.%d', [SW, D, M, S ] );
      end else Result:= '';
    end
    else
    begin
      if (A >= -180) and (A <= 180) then
      begin
        D:= Trunc( A );
        M:= Trunc( ( A - D ) * 60 );
        S:= Trunc( ((( A - D ) * 3600 ) - M * 60) * 100 );
        if A >= 0 then SW:= 'E' else SW:= 'W';
        Result:= Format( '%s:%d�%d.%d', [SW, D, M, S ] );
      end else Result:= '';
    end;

  except

    Result:= '';

  end;


end;
}

// -------------------------------------------------------------------------------------------


initialization

  FKM_Name:= '��';
  FPK_Name:= '��';
  FMetr_Name:= '�';
  FSantiMetr_Name:= '��';
  FMilimetr_Name:= '��';
  FImperial_Name:= 'Imperial';
  FMetric0_1km_Name:= 'Metric 100m';
  FMetric1km_Name:= 'Metric 1 km';

end.
