﻿
{ $ DEFINE LOG}
{ $ DEFINE ONLY_PB}
{ $ DEFINE NOCONSOLIDATION}
{ $ DEFINE _DEBUG}

unit HeadCheckingUnit;

{$M 163840, 1048576}

interface

uses
  Classes, SyncObjs, Windows, Forms, AviconTypes, DataFileConfig, AviconDataContainer, Dialogs;

const
  ABStr = 2;         // Канал загрузки данных из TAviconDataContainer

type

  THCParams = record                        //  | Номер строки в конфиг файле
    LoadStep: Integer;                      //  2 Шаг загрузки данных в обработку [мм]
    Slide_SensorPos_: Integer;              //  3  - Не используется // Положение датчика металла относительно центра ИС для системы скольжения [мм]
    Slide_AirBrushPos_: Integer;            //  4  - Не используется // Положение краскоотметчика относительно центра ИС для системы скольжения [мм]
    WorkZone: Integer;                      //  5  - Не используется // Протяженность зоны хранения данных дачика металла [мм]
    CalcWidth: Integer;                     //  6  - Зона (по пути) расчета Khc
    MSMPBMinLen: Integer;                   //  7  - Не используется // Минимальная протяженнось проподания ДС принимаемого за пачку [мм]
    ECHOPBMaxGapLen: Integer;               //  8 Максимально допустимый разрыв в пачке эхо сигналов [ШДП]
    TwoEchoMinDelay: Integer;               //  9  - Не используется // Минимальная разница задержек пачек [мкс] (определения признака 2 Эхо)
    TwoEchoMaxDelay: Integer;               // 10  - Не используется // Максимальная разница задержек пачек [мкс] (определения признака 2 Эхо)
    DecisionMakingBoundary: Integer;        // 11  - Не используется, расчитывается //  Граница принятия решения - от центра ИС
    MaxPBLen: Integer;                      // 12 Максимальный допустимый размер пачки [мм]
    MinCrdDelayRatio: Single;               // 13 Минимальный допустимый наклон пачки [мкс / мм]
    MaxCrdDelayRatio: Single;               // 14 Максимальный допустимый наклон пачки [мкс / мм]
    MaxAmplToLen: array [0..15] of Integer; // 15...30 Минимальная протяженность группы сигналов (мм) принимаемых за пачку в зависимости от максимальной амплитуды среди сигналов группы
    BSStateFilterLen: Integer;              // 31  - Не используется // Размерность фильтра колебаний состояния ДС [ШДП]
    BSStateParam: Integer;                  // 32
    HeadZone1: Integer;                     // 33 Зона поиска пачек в головке (начало)
    HeadZone2: Integer;                     // 34 Зона поиска пачек в головке (конец)
    HeadZoneSize: Integer;                  // 35  - Не используется // Размер зоны головки
    Wheel_SensorPos_: Integer;              // 36  - Не используется // Положение датчика металла относительно центра ИС для КИС [мм]
    Wheel_AirBrushPos_: Integer;            // 37  - Не используется // Положение краскоотметчика относительно центра ИС для КИС [мм]
  end;

type

  TJobEvent = procedure(Rail: TRail; DisCoord, SysCoord: Integer) of object;
  TJobEvent2 = procedure(Rail: TRail; DisCoord, SysCoord: Integer; RealCrd: Integer; ABItems: TABItemsList) of object;


  // ------ Датчик металла ---------------
 {
  TSensorItem = record   // Зона определяемая датчиком металла
    Free: Boolean;       // Флаг - элемент масива свободен
    StRealCrd: Integer;  // Координата события изменения состояния - Появился сигнал (датчик сработал)
    EdRealCrd: Integer;  // Координата события изменения состояния - Пропал сигнал
  end;
 }
  // -------------- Пачка ----------------

  TBScanRect = record        // Зона пачки на В-развертке
    StartDisCrd: Integer;
    EndDisCrd: Integer;
    StartDelay: Integer;     // [у.е.]
    EndDelay: Integer;       // [у.е.]
  end;

  TBPItem = record            // Пачка
    Free: Boolean;            // Флаг - элемент масива свободен
    ScanChNum: Integer;       // Номер канала
    BScanRect: TBScanRect;    // Зона пачки на В-развертке
    CentrDisCrd: Integer;     // Дисплейная координата центра пачки [ШДП]
    CentrRealCrd: Integer;    //
    TwoEcho: Boolean;         // Есть признак - Два Эхо
    CentrSysCrd: Integer;
    CentrDel: Integer;
    Angle: Single;
  end;

  // ------ Отметка краскопульта ---------

  TABItem = record
    Free: Boolean;                       // Флаг - элемент масива свободен
    RealCrd: Integer;                    // Дисплейная координата начала зоны расчета Khc
    PBList: array [0..1024] of TBPItem;  // Пачки по которым составленная отметка
    PBCount: Integer;                    // Количество пачек
  end;

  // -------------- Поиск пачек (ЭХО каналы) ----------------

  TMaskItem = record                     // Элемент пачки
    DisCrd: Integer;                     // Координата
    Delay: Integer;                      // Задержка
    Ampl: Integer;                       // Амплитуда
  end;

  TMaskItemRec = record                  // Маска поиска пачки
    Del: Boolean;                        // Флаг - элемент масива свободен
    TwoEcho: Boolean;                    // Флаг - признак Два Эхо
    Item: array of TMaskItem;            // Элементы пачки
  end;

  // -------------- Поиск пачек (ЭТМ каналы) ------------------
 (*
  TBSState = record
//    DisCrdCount: Integer;              //
    LastDisCrd: Integer;                 //
    DisCrd: Integer;                     // Дисплейная координата изменения состояние ДС [ШДП]
    Exist: Boolean;                      // Флаг - наличие ДС
  end;
  *)
  // ----------------------------------------------------------

  THeadChecking = class(TThread)
  private
    FLog: TextFile;
    FCfg: TDataFileConfig;
    FDataCont: TAviconDataContainer;

      // ------------- Загрузка данных -------------------

    FLastLoadDisCoord: Integer;       // Последнаая загруженная дисплейная координата
    FLastSysCoord: Integer;           // Последнаая системная координата - для отброда зон движения назад

      // ------------- Датчик металла -----------------------

//    FSensor1List: array [TRail] of array of TSensorItem; // Зоны отмеченные датчиком металла
//    FOldSensor1: array [TRail] of Boolean;               // Последнее значния датчика металла
//    FSensor1StDisCrd: array [TRail] of Integer;          // Координата последнего срабатывания датчика металла

      // ------------- Поиск пачек, пачки -------------------

    FMSChLink: array [0..15] of Integer;        // Связь номера канала сканирования (индекс масива) с номером ЗТМ канала оценки (значение) Возможные значения: -1 нет ЗТМ или >= 0 номер ЗТМ канала оценки)
   // FBSStateFilter: array [rLeft..rRight] of array of Boolean; // Фильтр колебаний состояния ДС

    FMask: array [rLeft..rRight, 1..15] of array of TMaskItemRec; // Маски поиска пачек в ЭХО и ЗЕРК каналах
   // FBSState: array [TRail, 0..15] of TBSState; // Маски поиска пачек в ЭТМ каналах

    FBPList: array [TRail] of array of TBPItem; // Массив пачек

      // ------ Отметка краскопульта ---------

    FABList: array [TRail] of array of TABItem;

      // ------------- Поток -------------------

    FStop: Boolean;
    FNeedStop: Boolean; // Флаг - необходимо выйти из цикла выполнения потока

      // ------------- Выгрузка данных -------------------

    FJobEvent: TJobEvent;
    FJobEvent2: TJobEvent2;
//    FFlg: Boolean; // Debug

    FParams: THCParams;
    FStartAnalyzTime: DWord;
    FStopAnalyzTime: DWord;

  protected
//    function Sensor1Test(R: TRail; RealCrd: Integer): Boolean;   // Получение состояния для заданной координаты (учет координат происходит внутри)
    function SysCrdToMM(Crd: Integer): Single;                   // Пересчет ШДП в милиметры
    function MMToSysCrd(MM: Integer): Integer;                   // Пересчет милиметров в ШДП

//    procedure AddSensor(R: TRail; StRealCrd, EdRealCrd: Integer);// Добавление отметок датчика металла
    procedure AddPB(R: TRail; ScanChNum: Integer; New: TBPItem); // Добавление пачки и расчет ее параметров
    function AddAB(R: TRail; New: TABItem): Integer;             // Добавление отметки краскопульта

    function BlockOK(StartDisCoord: Integer): Boolean;           // Загрузка данных
    procedure Execute; override;                                 // Метод потока
    procedure Tick;
  public

    SummAnalyzTime: Integer;
    SummWaitTime: Integer;
    SensorPos: Integer;
    AirBrushPos: Integer;
//    t: Integer;
    CS: TCriticalSection;

    constructor Create(DataCont: TAviconDataContainer; JobEvent: TJobEvent; JobEvent2: TJobEvent2; ParamsFileName: string; CreateSuspended: Boolean; Priority: TThreadPriority);
    destructor Destroy; override;
    procedure Tick_;
    procedure Stop;
    function GetAllocSize: Integer;
  end;

  TAirBrush = THeadChecking;

implementation

uses
  Types, Math, SysUtils;

constructor THeadChecking.Create(DataCont: TAviconDataContainer; JobEvent: TJobEvent; JobEvent2: TJobEvent2; ParamsFileName: string; CreateSuspended: Boolean; Priority: TThreadPriority);
var
  R: TRail;
  I: Integer;
  ScanChNum: Integer;
  EvalChIdx: Integer;
  ParamList: TStringList;
  ParamFileVer: Integer;
  Temp: TAviconID;

begin
  CS:= TCriticalSection.Create;

  SummAnalyzTime:= 0;
  SummWaitTime:= 0;
  FStartAnalyzTime:= 0;
  FStopAnalyzTime:= 0;

  {$IFDEF LOG}
  AssignFile(FLog, ExtractFilePath(Application.ExeName) + 'AirBrush.LOG');
  Rewrite(FLog);
  {$ENDIF}

  DecimalSeparator:= '.';                   // Загрузка конфигурационного файла
  ParamList:= TStringList.Create;
  ParamList.LoadFromFile(ParamsFileName);

  ParamFileVer:=                     StrToInt(ParamList[0]); // Версия файла

  if ParamFileVer in [2, 3, 4] then
  begin
    FParams.LoadStep:=               StrToInt(ParamList[1]); // Шаг загрузки данных в обработку [мм]
    FParams.Slide_SensorPos_:=       StrToInt(ParamList[2]); // Положение датчика металла относительно центра ИС [мм]
    FParams.Slide_AirBrushPos_:=     StrToInt(ParamList[3]); // Положение краскоотметчика относительно центра ИС [мм]
    FParams.WorkZone:=               StrToInt(ParamList[4]); // Протяженность зоны хранения данных дачика металла [мм]
    FParams.CalcWidth:=              StrToInt(ParamList[5]); // Зона расчета Khc
    FParams.MSMPBMinLen:=            StrToInt(ParamList[6]); // Минимальная протяженнось проподания ДС принимаемого за пачку [мм]
    FParams.ECHOPBMaxGapLen:=        StrToInt(ParamList[7]); // Максимально допустимый разрыв в пачке эхо сигналов [ШДП]
    FParams.TwoEchoMinDelay:=        StrToInt(ParamList[8]); // Минимальная разница задержек пачек [мкс] (определения признака 2 Эхо)
    FParams.TwoEchoMaxDelay:=        StrToInt(ParamList[9]); // Максимальная разница задержек пачек [мкс] (определения признака 2 Эхо)

//    FParams.DecisionMakingBoundary:= StrToInt(ParamList[10]); // ? Граница принятия решения - от центра ИС

    FParams.MaxPBLen:=               StrToInt(ParamList[11]); // ? Протяженность зоны в которой принятие решения происходить по признаку 2 Эхо
    FParams.MinCrdDelayRatio:=       StrToFloat(ParamList[12]); // мкс / мм
    FParams.MaxCrdDelayRatio:=       StrToFloat(ParamList[13]); // мкс / мм
    FParams.MaxAmplToLen[0]:=        StrToInt(ParamList[14]); // -12 дБ | Минимальная протяженность группы сигналов (мм)
    FParams.MaxAmplToLen[1]:=        StrToInt(ParamList[15]); // -10 дБ | принимаемых за пачку в зависимости от максимальной
    FParams.MaxAmplToLen[2]:=        StrToInt(ParamList[16]); //  -8 дБ | амплитуды среди сигналов группы
    FParams.MaxAmplToLen[3]:=        StrToInt(ParamList[17]); //  -6 дБ
    FParams.MaxAmplToLen[4]:=        StrToInt(ParamList[18]); //  -4 дБ
    FParams.MaxAmplToLen[5]:=        StrToInt(ParamList[19]); //  -2 дБ
    FParams.MaxAmplToLen[6]:=        StrToInt(ParamList[20]); //   0 дБ
    FParams.MaxAmplToLen[7]:=        StrToInt(ParamList[21]); //   2 дБ
    FParams.MaxAmplToLen[8]:=        StrToInt(ParamList[22]); //   4 дБ
    FParams.MaxAmplToLen[9]:=        StrToInt(ParamList[23]); //   6 дБ
    FParams.MaxAmplToLen[10]:=       StrToInt(ParamList[24]); //   8 дБ
    FParams.MaxAmplToLen[11]:=       StrToInt(ParamList[25]); //  10 дБ
    FParams.MaxAmplToLen[12]:=       StrToInt(ParamList[26]); //  12 дБ
    FParams.MaxAmplToLen[13]:=       StrToInt(ParamList[27]); //  14 дБ
    FParams.MaxAmplToLen[14]:=       StrToInt(ParamList[28]); //  16 дБ
    FParams.MaxAmplToLen[15]:=       StrToInt(ParamList[29]); //  18 дБ
    FParams.BSStateFilterLen:=       StrToInt(ParamList[30]); // Размерность фильтра колебаний состояния ДС [ШДП]
    FParams.BSStateParam:=           StrToInt(ParamList[31]);
  end;

  if ParamFileVer in [3, 4] then
  begin
    FParams.HeadZone1:=              StrToInt(ParamList[32]); // Зона головки №1
    FParams.HeadZone2:=              StrToInt(ParamList[33]); // Зона головки №2
    FParams.HeadZoneSize:=           StrToInt(ParamList[34]); // Размер зоны головки
  end;

  if ParamFileVer = 4 then
  begin
    FParams.Wheel_SensorPos_:=       StrToInt(ParamList[35]); // Положение датчика металла относительно центра ИС для КИС [мм]
    FParams.Wheel_AirBrushPos_:=     StrToInt(ParamList[36]); // Положение краскоотметчика относительно центра ИС для КИС [мм]
  end;

  ParamList.Free;

  Temp:= DataCont.Config.DeviceID;
  if (ParamFileVer >= 4) and
      (CompareMem(@Temp, @Avicon16Wheel, 8) or
       CompareMem(@Temp, @Avicon14Wheel, 8)) then
  begin                                   // Колеса
    SensorPos:= FParams.Wheel_SensorPos_;
    AirBrushPos:= FParams.Wheel_AirBrushPos_;
  end
  else
  begin                                   // Скольжение
    SensorPos:= FParams.Slide_SensorPos_;
    AirBrushPos:= FParams.Slide_AirBrushPos_;
  end;

  FDataCont:= nil;
  FCfg:= nil;
  FJobEvent:= JobEvent;
  FJobEvent2:= JobEvent2;
  if not Assigned(DataCont) then Exit;

  FDataCont:= DataCont;
  FCfg:= FDataCont.Config;

  FLastLoadDisCoord:= FDataCont.CurSaveDisCoord - 1; // Берем координаты начиная с которых будем работать
  FLastSysCoord:= FDataCont.CurSaveSysCoord;

                                 // Определяем связь номеров каналов сканирования с номерами ЗТМ каналов оценки
  for ScanChNum := FCfg.MinScanChNumber to FCfg.MaxScanChNumber do // "Обнуляем" массив - заносим значение - 1
    FMSChLink[ScanChNum] := - 1;

  for EvalChIdx := 0 to FCfg.EvalChannelCount - 1 do // Перебираем каналы оценки
    if (FCfg.EvalChannelByIdx[EvalChIdx].Method = imMirrorShadow) then // Если канал ЗТМ то ..
      FMSChLink[FCfg.EvalChannelByIdx[EvalChIdx].ScanChNum] := FCfg.EvalChannelByIdx[EvalChIdx].Number; // .. сохраняем его номер

                                 // Обнуление рабочих масивов
  (*
  for R:= FCfg.MinRail to FCfg.MaxRail do
  begin
    SetLength(FBSStateFilter[R], FParams.BSStateFilterLen);
    for I:= 0 to 15 do
    begin
      FBSState[R, I].Exist:= True;  // По умолчанию считаем что ДС есть
      FBSState[R, I].DisCrd:= 0;
    end;
    FOldSensor1[R]:= False;         // По умолчанию считаем что сиганала датчика металла нет
  end;
  *)

  FStop:= False;
  FNeedStop:= False;

  FParams.DecisionMakingBoundary:= Round(Abs(GetRealCrd_inSysCrd(FCfg.ScanChannelByNum[3], FDataCont.Header.ScanStep, FParams.HeadZone2)) + 10);

  inherited Create(CreateSuspended);         // Создаем поток включенным
  self.Priority:= Priority;
end;

destructor THeadChecking.Destroy;
var
  R: TRail;
  ScanChNum: Integer;

begin
                                  // Остановка патока
//  FNeedStop:= True;
//  while not FStop do Sleep(0);
//  Terminate;
                                  // Удаление массивов
  for R:= FCfg.MinRail to FCfg.MaxRail do
  begin
    SetLength(FBPList[R], 0);
    for ScanChNum:= FCfg.MinScanChNumber to FCfg.MaxScanChNumber do
      SetLength(FMask[R, ScanChNum], 0);
    SetLength(FABList[R], 0);
  end;

  inherited Destroy;
  CS.Free;

  {$IFDEF LOG}
  CloseFile(FLog);
  {$ENDIF}
end;

procedure THeadChecking.Tick_;
begin
  Tick;
end;

procedure THeadChecking.Stop;
begin
                                  // Остановка потока
  FNeedStop:= True;
  //a[1]:= 3;
  while not FStop do Sleep(0);
  Terminate;
end;

function THeadChecking.GetAllocSize: Integer;
var
  I: Integer;
  Size: array [1..10] of Integer;

begin
//  Size[1]:= (Length(FSensor1List[rLeft]) + Length(FSensor1List[rRight])) * SizeOf(TSensorItem);
  Size[1]:= 0;
  Size[2]:= 0;
  for I := 1 to 15 do
    Size[2]:= Size[2] + (Length(FMask[rLeft, I]) + Length(FMask[rRight, I])) * SizeOf(TMaskItemRec);
  Size[3]:= (Length(FBPList[rLeft]) + Length(FBPList[rRight])) * SizeOf(TBPItem);
  Size[4]:= (Length(FABList[rLeft]) + Length(FABList[rRight])) * SizeOf(TABItem);
  Result:= Size[1] + Size[2] +  Size[3] +  Size[4];
end;

function THeadChecking.SysCrdToMM(Crd: Integer): Single; // Пересчет ШДП в милиметры
begin
  Result:= Crd * FDataCont.Header.ScanStep / 100;
end;

function THeadChecking.MMToSysCrd(MM: Integer): Integer; // Пересчет милиметров в ШДП
begin
  Result:= Round(MM / (FDataCont.Header.ScanStep / 100));
end;
(*
procedure THeadChecking.AddSensor(R: TRail; StRealCrd, EdRealCrd: Integer); // Добавление отметок датчика металла
var
  I: Integer;

begin
  {$IFDEF LOG}
//  Writeln(FLog, Format('AddSensor: R = %d; St = %d; Ed = %d', [Ord(R), StRealCrd, EdRealCrd]));
  {$ENDIF}

  for I:= 0 to Length(FSensor1List[R]) - 1 do   // Ищем свободный элемент массива и заносим данные в него
    if FSensor1List[R, I].Free then
    begin
      FSensor1List[R, I].StRealCrd:= StRealCrd; // Заносим данные
      FSensor1List[R, I].EdRealCrd:= EdRealCrd;
      FSensor1List[R, I].Free:= False;          // Устанавливаем флаг - элемент занят
      Exit;
    end;
                                                // Если свободных элементов массива нет - добавляем новый элемент
  I:= Length(FSensor1List[R]);
  SetLength(FSensor1List[R], I + 1);
  FSensor1List[R, I].StRealCrd:= StRealCrd;     // Заносим данные
  FSensor1List[R, I].EdRealCrd:= EdRealCrd;
  FSensor1List[R, I].Free:= False;              // Устанавливаем флаг - элемент занят
end;
*)
procedure THeadChecking.AddPB(R: TRail; ScanChNum: Integer; New: TBPItem); // Добавление пачки
var
  I: Integer;

begin
                                                     // Определяем координату пачки
  New.CentrDisCrd:= (New.BScanRect.StartDisCrd + New.BScanRect.EndDisCrd) div 2;
  New.CentrDel:= (New.BScanRect.StartDelay + New.BScanRect.EndDelay) div 2;
  New.CentrRealCrd:= Round(New.CentrDisCrd + GetRealCrd_inSysCrd(FCfg.ScanChannelByNum[ScanChNum],
                                                                 FDataCont.Header.ScanStep,
                                                                 New.CentrDel));

  for I:= 0 to Length(FBPList[R]) - 1 do             // Ищем свободный элемент массива и заносим данные в него
    if FBPList[R, I].Free then
    begin
      FBPList[R, I]:= New;                           // Заносим данные
      FBPList[R, I].Free:= False;                    // Устанавливаем флаг - элемент занят
      Exit;
    end;
  I:= Length(FBPList[R]);                            // Если свободных элементов массива нет - добавляем новый элемент
  SetLength(FBPList[R], I + 1);
  FBPList[R, I]:= New;                               // Заносим данные
  FBPList[R, I].Free:= False;                        // Устанавливаем флаг - элемент занят
end;

function THeadChecking.AddAB(R: TRail; New: TABItem): Integer;  // Добавление отметки краскопульта
var
  I: Integer;

begin
  for I:= 0 to Length(FABList[R]) - 1 do            // Ищем свободный элемент массива и заносим данные в него
    if FABList[R, I].Free then
    begin
      FABList[R, I]:= New;                          // Заносим данные
      FABList[R, I].Free:= False;                   // Устанавливаем флаг - элемент занят
      Result:= I;
      Exit;
    end;
  I:= Length(FABList[R]);                           // Если свободных элементов массива нет - добавляем новый элемент
  SetLength(FABList[R], I + 1);
  FABList[R, I]:= New;                              // Заносим данные
  FABList[R, I].Free:= False;                       // Устанавливаем флаг - элемент занят
  Result:= I;
end;

function THeadChecking.BlockOK(StartDisCoord: Integer): Boolean; // ---- Поиск пачек ----------
type
  TEchoDataItem = record
    MaskIdx: Integer;
    Value: Integer;
  end;

const
  Mask: array [1..3, 1..10] of TPoint = (((X: 1; Y:   0),   // Прямой
                                          (X: 1; Y:   1),
                                          (X: 1; Y: - 1),
                                          (X: 1; Y:   2),
                                          (X: 1; Y: - 2),
                                          (X: 2; Y:   0),
                                          (X: 2; Y:   1),
                                          (X: 2; Y: - 1),
                                          (X: 2; Y:   2),
                                          (X: 2; Y: - 2)),

                                         ((X: 1; Y: - 1),   // Наезжающие
                                          (X: 1; Y:   0),
                                          (X: 0; Y: - 1),
                                          (X: 2; Y: - 2),
                                          (X: 2; Y: - 1),
                                          (X: 1; Y: - 2),
                                          (X: 3; Y: - 3),
                                          (X: 0; Y:   0),
                                          (X: 0; Y:   0),
                                          (X: 0; Y:   0)),

                                         ((X: 1; Y:   1),   // Отъезжающие
                                          (X: 1; Y:   0),
                                          (X: 0; Y:   1),
                                          (X: 2; Y:   2),
                                          (X: 2; Y:   1),
                                          (X: 1; Y:   2),
                                          (X: 0; Y:   0),
                                          (X: 0; Y:   0),
                                          (X: 0; Y:   0),
                                          (X: 0; Y:   0)));

var
  R: TRail;
  ScanChNum: Integer;
  EchoIdx: Integer;
  MaskIdx: Integer;
  Cnt, Idx: Integer;
  K, L, M, ML, Q: Integer;
  MaxAmpl: Integer;
  CurEcho: AviconTypes.TCurEcho; // Сигналы текущей координаты
  EchoData: array [1..16] of TEchoDataItem;
  BSDelay: Integer;           // Задержка сигнала от подошвы рельса
//  BSEchoIdx: Integer;         // Номер сигнала от подошвы рельса (выбирается сигнал максимальной амплитуды)
//  SkipEchoIdx: array [1..16] of Boolean;
  BSState: Boolean;           // Состояние донного сигнала
  BSStateCnt: Integer;           // Состояние донного сигнала
  MIdx: Integer;
  CrdLenMM: Single;
  DlyLen: Single;
  DlyCentr: Single;
  New: TBPItem;
  HeadFlag: Boolean;

//  ABItems: array [0..0] of edAirBrushItem;
//  A: array [0..10] of Single;

begin

  {IFDEF _DEBUG}
{  if (FDataCont.CurLoadDisCoord[ABStr] < 6000) or
     (FDataCont.CurLoadDisCoord[ABStr] > 8500) then
  begin
    FLastLoadDisCoord:= FDataCont.CurLoadDisCoord[ABStr];
    FLastSysCoord:= FDataCont.CurLoadSysCoord[ABStr];      // Сохраняем последнеюю загруженную системную координату
    Result:= True;
    exit;
  end; }
  {ENDIF}

  if FDataCont.CurLoadDisCoord[ABStr] <= FLastLoadDisCoord then
  begin
  //    ShowMessage('Load Error');
    Result:= True;
    Exit;
  end;


//  if FDataCont.CurLoadSysCoord[ABStr] > FLastSysCoord then // Берем только зоны движения вперед
  begin

    CurEcho:= FDataCont.CurLoadEcho[ABStr];                // Получаем сигналы текущей координаты
    for R:= FCfg.MinRail to FCfg.MaxRail do
    begin

      // -------------------------------------------------------------------------------------------------------------------

      // Формируем зоны - ищем вначале начало зоны, затем конец зоны
     (*
      if (not FOldSensor1[R]) and FDataCont.CurLoadSensor1[ABStr][R] then // Произошло изменение - Из False -> True = начало
      begin
        FSensor1StDisCrd[R]:= FDataCont.CurLoadDisCoord[ABStr];
        FOldSensor1[R]:= True;
      end;
      if FOldSensor1[R] and (not FDataCont.CurLoadSensor1[ABStr][R]) then // Произошло изменение - Из True -> False = конец
      begin
        AddSensor(R,
                  MMToSysCrd(SensorPos) + FSensor1StDisCrd[R]             ,
                  MMToSysCrd(SensorPos) + FDataCont.CurLoadDisCoord[ABStr]); // Зона сформированна
        FOldSensor1[R]:= False;
      end;
     *)
       // -------------------------------------------------------------------------------------

      for ScanChNum:= FCfg.MinScanChNumber to FCfg.MaxScanChNumber do

      {IFDEF _DEBUG}
//      if (R = FCfg.MaxRail) and (ScanChNum in [0, { 1,} 2, 3, 4, 5, 6]) then
      {ENDIF}

      if ScanChNum in [2, 3, 10, 11] then
      begin
                                              // Анализ сигналов - на предмет попадания в фомируемые пачки
   //     if CurEcho[R, ScanChNum].Count <> 0 then
        begin

          // ----------------------------- Поиск пачек в каналах ЗМТ, без учета зон движения назад ----------------------------------------

//          BSEchoIdx:= - 1;                    // Пока сигналов ЗТМ нет
          //for EchoIdx:= 1 to 16 do SkipEchoIdx[EchoIdx]:= False;
          (*
          if FMSChLink[ScanChNum] <> - 1 then // Если канал с ЗТМ - то ищем донный сигнал
          begin
            MaxAmpl:= - 1;
            with CurEcho[R, ScanChNum] do
              for EchoIdx:= 1 to Count do           // Отбираем сигналы попадающие в строб канала ЗТМ
              begin
                if (Delay[EchoIdx] / FCfg.ScanChannelByNum[ScanChNum].DelayMultiply >= FDataCont.CurLoadParams[ABStr][R, FMSChLink[ScanChNum]].StStr) and
                   (Delay[EchoIdx] / FCfg.ScanChannelByNum[ScanChNum].DelayMultiply <= FDataCont.CurLoadParams[ABStr][R, FMSChLink[ScanChNum]].EndStr) then
                begin                               // Сигнал в стробе ЗТМ канала
                  if Ampl[EchoIdx] > MaxAmpl then   // Выбираем сигнал с большей амплитудой
                  begin
                    MaxAmpl:= Ampl[EchoIdx];        // Запоминаем сигнал
                    BSDelay:= Delay[EchoIdx];
                  //  BSEchoIdx:= EchoIdx;            // Запоминаем его индекс
                  end;
                end;
                if (Delay[EchoIdx] / FCfg.ScanChannelByNum[ScanChNum].DelayMultiply >= FDataCont.CurLoadParams[ABStr][R, FMSChLink[ScanChNum]].StStr) then
                  SkipEchoIdx[EchoIdx]:= True;
              end;

            BSState:= MaxAmpl <> - 1;         // Флаг наличия донного сигнала

            if FDataCont.CurLoadSysCoord[ABStr] < FLastSysCoord then BSState:= True;


            if FParams.BSStateFilterLen > 1 then // Фильтрация колебаний сотояние ДС - если задан размер буфера
            begin
                                                 // Сдвигаем отсчеты буфера фильтра - на столько на сколько с прошлого раза изменилась координата
              for Cnt := FBSState[R, ScanChNum].LastDisCrd + 1 to FDataCont.CurLoadDisCoord[ABStr] do
              begin                              // Сдвиг на 1 шаг
                for Idx := 1 to FParams.BSStateFilterLen - 1 do // Сдвиг
                  FBSStateFilter[R, Idx - 1]:= FBSStateFilter[R, Idx];
                Idx := FParams.BSStateFilterLen - 1;            // Внесение нового значения
                if Cnt <> FDataCont.CurLoadDisCoord[ABStr]
                  then FBSStateFilter[R, Idx]:= False           // Скачек кординаты - ДС нет
                  else FBSStateFilter[R, Idx]:= BSState;        // Значение для загруженной координаты
              end;

             {                                                  // Определяем значение текущего состояния ДС
              if FBSState[R, ScanChNum].Exist then              // Если на данный момент ДС есть то
              begin
                BSState:= False;                                // Складываем элементы массива фильтра по OR
                for Idx := 1 to FParams.BSStateFilterLen - 1 do // Если все элементы False то ДС нет!
                  BSState:= BSState or FBSStateFilter[R, Idx];
              end
              else                                              // Если на данный момент ДС нет
              begin
                BSState:= True;                                 // Складываем элементы массива фильтра по AND
                for Idx := 1 to FParams.BSStateFilterLen - 1 do // Если все элементы True то ДС есть!
                  BSState:= BSState and FBSStateFilter[R, Idx];
              end;  }

                                                                // Определяем значение текущего состояния ДС
              if FBSState[R, ScanChNum].Exist then              // Если на данный момент ДС есть то
              begin
                BSState:= True;
                BSStateCnt:= 0;
                for Idx := 1 to FParams.BSStateFilterLen - 1 do
                  if not FBSStateFilter[R, Idx] then Inc(BSStateCnt);
                if BSStateCnt >= FParams.BSStateParam then BSState:= False;
              end
              else                                              // Если на данный момент ДС нет
              begin
                BSState:= False;
                BSStateCnt:= 0;
                for Idx := 1 to FParams.BSStateFilterLen - 1 do
                  if FBSStateFilter[R, Idx] then Inc(BSStateCnt);
                if BSStateCnt >= FParams.BSStateParam then BSState:= True;
              end;
            end;
                                              // Поиск дырки в линии донного сигнала

            if FBSState[R, ScanChNum].Exist and                    // Скачок координаты - ДС пропал - начало пачки
               (FDataCont.CurLoadDisCoord[ABStr] - FBSState[R, ScanChNum].LastDisCrd > 2) then
            begin
              FBSState[R, ScanChNum].DisCrd:= FBSState[R, ScanChNum].LastDisCrd;
              FBSState[R, ScanChNum].Exist:= False;                // Запоминаем состояние ДС
            end
            else
            if FBSState[R, ScanChNum].Exist and (not BSState) then // ДС пропал - начало пачки
            begin
              FBSState[R, ScanChNum].DisCrd:= FDataCont.CurLoadDisCoord[ABStr];
              FBSState[R, ScanChNum].Exist:= False;                // Запоминаем состояние ДС
            end
            else
            if (not FBSState[R, ScanChNum].Exist) and BSState then // ДС появился  - конец пачки
            begin
                                                                   // Проверка протяженности зоны пропадания ДС
              if FDataCont.CurLoadDisCoord[ABStr] - FBSState[R, ScanChNum].DisCrd >= MMToSysCrd(FParams.MSMPBMinLen) then
              begin                           // Новая пачка в ЗТМ канале
                New.ScanChNum:= ScanChNum;
                New.BScanRect.StartDisCrd:= FBSState[R, ScanChNum].DisCrd - FParams.BSStateParam;
                New.BScanRect.EndDisCrd:= FDataCont.CurLoadDisCoord[ABStr] - FParams.BSStateParam;
{?}                New.BScanRect.StartDelay:= BSDelay - 5;
{?}                New.BScanRect.EndDelay:= BSDelay + 5;
                AddPB(R, ScanChNum, New);                          // Добавляем пачку
              end;
              FBSState[R, ScanChNum].Exist:= True;                 // Запоминаем состояние ДС
            end;
            FBSState[R, ScanChNum].LastDisCrd:= FDataCont.CurLoadDisCoord[ABStr];
          end;
          *)
          // ----------------------------- Поиск пачек в ЭХО каналах ----------------------------------------

          if FDataCont.CurLoadSysCoord[ABStr] > FLastSysCoord then // Только в зонах движения вперед
          begin
                                              // Определяем индекс массива поиска в зависимости от канала
            if FCfg.ScanChannelByNum[ScanChNum].EnterAngle = 0 then MIdx:= 1 else
            if FCfg.ScanChannelByNum[ScanChNum].EnterAngle > 0 then MIdx:= 2 else
            if FCfg.ScanChannelByNum[ScanChNum].EnterAngle < 0 then MIdx:= 3;
            with CurEcho[R, ScanChNum] do      // Анализ сигналов текущей координаты
            begin
              for EchoIdx:= 1 to Count do      // Перекрестный анализ - каждый сигнал (цикл EchoIdx) с каждой формируемой пачкой (цикл MaskIdx)
//                if not SkipEchoIdx[EchoIdx] then // Отбрасываем донный сигнал
                begin
                  EchoData[EchoIdx].Value:= MaxInt;                       // Сбрасываем индекс найденного подходящего сигнала
                  for MaskIdx:= 0 to Length(FMask[R, ScanChNum]) - 1 do   // Перекрестный анализ - каждый сигнал (цикл EchoIdx) с каждой формируемой пачкой (цикл MaskIdx)
                    if not FMask[R, ScanChNum][MaskIdx].Del then          // Если маска поиска пачки активна
                    begin
                      K:= High(FMask[R, ScanChNum][MaskIdx].Item);  // Берем индекс последнего сигнала входящего в пачку
                      for L:= 1 to 10 do                            // Ищем соответствие DX, DY в массиве поиска
                        if (Mask[MIdx, L].X = (FDataCont.CurLoadDisCoord[ABStr] - FMask[R, ScanChNum][MaskIdx].Item[K].DisCrd)) and
                           (Mask[MIdx, L].Y = (Delay[EchoIdx] - FMask[R, ScanChNum][MaskIdx].Item[K].Delay)) and
                           (L < EchoData[EchoIdx].Value) { and      // Выбор лучшего варианта
                           ((L = 1) or ((L <> 1) and (Mask[FR, FCh][J].Dir[L] < 2))) } then
                        begin
                          EchoData[EchoIdx].MaskIdx:= MaskIdx;
                          EchoData[EchoIdx].Value:= L;
                          Break;
                        end;
                    end;
                end;

      {IFDEF _DEBUG}
//     if (R = FCfg.MaxRail) and (ScanChNum in [{0,  1, 2, 3, 4, 5, 6, 7 ]) then
//      if ScanChNum <> 1 then
      {ENDIF}

              for EchoIdx:= 1 to Count do
//                if not SkipEchoIdx[EchoIdx] then                              // Отбрасываем донный сигнал
                begin
                  if EchoData[EchoIdx].Value <> MaxInt then                   // Смотрим входит ли сигнал в одну из создоваемых пачек
                  begin                                                       // Входит - Вставляем текущий сигнал в пачки
                    MaskIdx:= EchoData[EchoIdx].MaskIdx;

                    K:= Length(FMask[R, ScanChNum][MaskIdx].Item);
                    SetLength(FMask[R, ScanChNum][MaskIdx].Item, K + 1);
                    FMask[R, ScanChNum][MaskIdx].Item[High(FMask[R, ScanChNum][MaskIdx].Item)].DisCrd:= FDataCont.CurLoadDisCoord[ABStr];
                    FMask[R, ScanChNum][MaskIdx].Item[High(FMask[R, ScanChNum][MaskIdx].Item)].Delay:= Delay[EchoIdx];
                    FMask[R, ScanChNum][MaskIdx].Item[High(FMask[R, ScanChNum][MaskIdx].Item)].Ampl:= Ampl[EchoIdx];

                    FMask[R, ScanChNum][MaskIdx].TwoEcho:= False;

                    if (FCfg.ScanChannelByNum[ScanChNum].EnterAngle >= 40) and
                       (FCfg.ScanChannelByNum[ScanChNum].EnterAngle <= 50) then
                    begin
                      for M:= 1 to Count do
                      //  if not SkipEchoIdx[M] then        // Отбрасываем донный сигнал
                          FMask[R, ScanChNum][MaskIdx].TwoEcho:= FMask[R, ScanChNum][MaskIdx].TwoEcho or
                                                                (Abs(Delay[M] - Delay[EchoIdx]) >= FParams.TwoEchoMinDelay) and
                                                                (Abs(Delay[M] - Delay[EchoIdx]) <= FParams.TwoEchoMaxDelay);
                    end;
                  end
                  else
                  begin                                                       // Не входит - Создаем новую пачку
                    ML:= - 1;
                    for K:= 0 to High(FMask[R, ScanChNum]) do                 // Ищем свободное место в массиве создоваемых пачек
                      if FMask[R, ScanChNum][K].Del then
                      begin
                        ML:= K;
                        Break;
                      end;

                    if ML = - 1 then                                          // Не нашли увеличиваем размер масива
                    begin
                      ML:= Length(FMask[R, ScanChNum]);
                      SetLength(FMask[R, ScanChNum], ML + 1);
                    end;
                                                                              // Вносим новую запись в элемент - ML
                    FMask[R, ScanChNum][ML].Del:= False;
                    FMask[R, ScanChNum][ML].TwoEcho:= False;
      //              for J:= 1 to 6 do Mask[FR, FCh][ML].Dir[J]:= 0;
                    SetLength(FMask[R, ScanChNum][ML].Item, 1);
                    FMask[R, ScanChNum][ML].Item[0].DisCrd:= FDataCont.CurLoadDisCoord[ABStr];
                    FMask[R, ScanChNum][ML].Item[0].Delay:= Delay[EchoIdx];
                    FMask[R, ScanChNum][ML].Item[0].Ampl:= Ampl[EchoIdx];

                  end;
                end;
            end;

          end;
        end;
      end;
    end;

    // -------------------------------------------------------------------------------------
    // Проверка на окончание формирования пачки
    // -------------------------------------------------------------------------------------

    for R:= FCfg.MinRail to FCfg.MaxRail do
    begin
      for ScanChNum:= FCfg.MinScanChNumber to FCfg.MaxScanChNumber do
      begin
        for MaskIdx:= 0 to High(FMask[R, ScanChNum]) do
          if not FMask[R, ScanChNum][MaskIdx].Del then
            if (FDataCont.CurLoadSysCoord[ABStr] <= FLastSysCoord) or // Если ЗДН то прирываем формирование пачек
               (FDataCont.CurLoadDisCoord[ABStr] -        // Проверяем формруемые пачки на максимально допустимый разрыв - если больше то прирываем формирование пачки
                FMask[R, ScanChNum][MaskIdx].Item[High(FMask[R, ScanChNum][MaskIdx].Item)].DisCrd > FParams.ECHOPBMaxGapLen) then
            begin
              begin
                with FMask[R, ScanChNum][MaskIdx] do        // Проверка годности пачки
                begin
                  MaxAmpl:= - 1;                            // Определение максимальной амплитуды
                  for K:= 0 to High(Item) do MaxAmpl:= Max(MaxAmpl, Item[K].Ampl);

                  CrdLenMM:= SysCrdToMM(Item[High(Item)].DisCrd - Item[0].DisCrd + 1);        // Определение прямоугольности пачки
                  DlyLen:= (Abs(Item[High(Item)].Delay - Item[0].Delay) + 1) / FCfg.ScanChannelByNum[ScanChNum].DelayMultiply;

                  DlyCentr:= (Item[High(Item)].Delay + Item[0].Delay) / 2;

//                  HeadFlag:= (Abs(DlyCentr - FParams.HeadZone1) <= FParams.HeadZoneSize) or
//                             (Abs(DlyCentr - FParams.HeadZone2) <= FParams.HeadZoneSize);

                  HeadFlag:= (DlyCentr >= FParams.HeadZone1) and (DlyCentr <= FParams.HeadZone2);

                  if  ((FCfg.ScanChannelByNum[ScanChNum].EnterAngle = 58) and  // Для наклонный каналов контроля головки
                       (DlyLen / CrdLenMM >= FParams.MinCrdDelayRatio) and     // проверяем соотношение DL [мм] к DH [мкс]
                       (DlyLen / CrdLenMM <= FParams.MaxCrdDelayRatio) and
                       HeadFlag and
                       (CrdLenMM <= FParams.MaxPBLen) and
                       (CrdLenMM >= FParams.MaxAmplToLen[MaxAmpl]))   // и длинну на минимально допустимое значени в зависимости от максимальной амплитуды сигнала входящего в пачку
                (*     or
                      ((FCfg.ScanChannelByNum[ScanChNum].EnterAngle <> 0) and  // Для наклонный каналов
                       (FCfg.ScanChannelByNum[ScanChNum].EnterAngle <> 58) and
                       (DlyLen / CrdLenMM >= FParams.MinCrdDelayRatio) and     // проверяем соотношение DL [мм] к DH [мкс]
                       (DlyLen / CrdLenMM <= FParams.MaxCrdDelayRatio) and
                       (CrdLenMM >= FParams.MaxAmplToLen[MaxAmpl]))            // и длинну на минимально допустимое значени в зависимости от максимальной амплитуды сигнала входящего в пачку
                       or
                      ((FCfg.ScanChannelByNum[ScanChNum].EnterAngle = 0) and   // Для прямых каналов
                       (CrdLenMM >= FParams.MaxAmplToLen[MaxAmpl])) *) then       // проверяем длинну на минимально допустимое значени в зависимости от максимальной амплитуды сигнала входящего в пачку

                  begin                            // Новая пачка в ЭХО канале
                    New.ScanChNum:=             ScanChNum;

//                    if Item[0].DisCrd = 23387 then
//                      DlyLen:= 10;

                    New.BScanRect.StartDisCrd:= Item[0].DisCrd;          // Координаты начала и конца
                    New.BScanRect.EndDisCrd:=   Item[High(Item)].DisCrd;
                    New.BScanRect.StartDelay:=  512;                     // Определяем координаты по задержке
                    New.BScanRect.EndDelay:=    0;
                    for K:= 0 to Length(Item) - 1 do
                    begin
                      New.BScanRect.StartDelay:=  Min(New.BScanRect.StartDelay, Item[K].Delay);
                      New.BScanRect.EndDelay:=    Max(New.BScanRect.EndDelay, Item[K].Delay);
                    end;
                    New.Angle:= DlyLen / CrdLenMM;
                    New.CentrDisCrd:= (New.BScanRect.StartDisCrd + New.BScanRect.EndDisCrd) div 2;
                    New.CentrDel:= (New.BScanRect.StartDelay + New.BScanRect.EndDelay) div 2;
                    New.TwoEcho:= FMask[R, ScanChNum][MaskIdx].TwoEcho;
                    AddPB(R, ScanChNum, New);                            // Добавляем пачку
                  end;
                end;
              end;

              SetLength(FMask[R, ScanChNum][MaskIdx].Item, 0); // Очистка ячейки поиска для нового поиска
              FMask[R, ScanChNum][MaskIdx].Del:= True;
            end;
      end;
    end;
  end;

  FLastLoadDisCoord:= FDataCont.CurLoadDisCoord[ABStr];
  FLastSysCoord:= FDataCont.CurLoadSysCoord[ABStr];      // Сохраняем последнеюю загруженную системную координату
  Result:= True;
end;
(*
function THeadChecking.Sensor1Test(R: TRail; RealCrd: Integer): Boolean;
var
  SIdx, St, Ed: Integer;
  CenterDisCrd: Integer;

begin
  Result:= False;
  {$IFDEF LOG}
//  Writeln(FLog, Format('In Sensor1Test: R: %d; Count: %d', [Ord(R), Length(FSensor1List[R])]));
  {$ENDIF}
  for SIdx:= 0 to Length(FSensor1List[R]) - 1 do // Проверяем на попадание в уже сформированные зоны отметок датчика металла
    if (not FSensor1List[R, SIdx].Free) then
    begin

      {$IFDEF LOG}
//      Writeln(FLog, Format('FParams.TwoEchoZoneLen = %d мм', [FParams.TwoEchoZoneLen]));
//      Writeln(FLog, Format('FParams.TwoEchoZoneLen = %d шдп', [MMToSysCrd(FParams.TwoEchoZoneLen)]));
      {$ENDIF}

      St:= FSensor1List[R, SIdx].StRealCrd - MMToSysCrd(FParams.TwoEchoZoneLen) div 2;
      Ed:= FSensor1List[R, SIdx].EdRealCrd + MMToSysCrd(FParams.TwoEchoZoneLen) div 2;

      {$IFDEF LOG}
//      Writeln(FLog, Format('In Sensor1Test: Idx = %d; St = %d; Ed = %d; Ok? = %d', [SIdx, St, Ed, Ord((RealCrd >= St) and (RealCrd <= Ed))]));
      {$ENDIF}

      if (RealCrd >= St) and (RealCrd <= Ed) then
//      if (RealCrd >= FSensor1List[R, SIdx].StRealCrd) and
//         (RealCrd <= FSensor1List[R, SIdx].EdRealCrd) then
      begin
        Result:= True;
        Exit;
      end;

    end;
//  else
//  begin
//    {$IFDEF LOG}
//    Writeln(FLog, Format('In Sensor1Test: Idx: %d; id Free', [SIdx]));
//    {$ENDIF}
//  end;

  if FOldSensor1[R] then // Проверяем на попадание в формиремую зону отметки датчика металла
  begin
    if (RealCrd >= MMToSysCrd(SensorPos) + FSensor1StDisCrd[R] - MMToSysCrd(FParams.TwoEchoZoneLen) div 2) then
      Result:= True;
  end;
end;
*)
procedure THeadChecking.Execute;
begin
  while not Terminated do
  begin
    if FNeedStop then Break;
    Tick;
  end;
  FStop:= True;
end;

procedure THeadChecking.Tick;
var
  R: TRail;
  I, J: Integer;
  MaxDisCrd: Integer;
  PBIdx, ABIdx, SIdx: Integer;
  ABIdx1, ABIdx2: Integer;
  PBCentrCrd: Integer;
  PBCentrDel: Integer;
  ABSysCrd: Single;                // Системная координата отметки
  Flag: Boolean;
  New: TABItem;
  ABItems: TABItemsList; // array of edAirBrushItem;
  Flg: Boolean;

  Ch_: Integer;
  Summ_: array [0..16] of Integer;
  Count_: array [0..16] of Integer;
  Res_: array [0..16] of Single;

  Ne: TBPItem;


begin
  if FDataCont.MaxDisCoord - FLastLoadDisCoord < MMToSysCrd(FParams.LoadStep) then Exit;

  CS.Enter;
  FStartAnalyzTime:= GetTickCount();
  SummWaitTime:= SummWaitTime + FStopAnalyzTime - FStartAnalyzTime;

                                 // Загрузка и первичный анализ данных - формирование пачек

  MaxDisCrd:= FDataCont.MaxDisCoord;
  if Assigned(FDataCont) then
    if MaxDisCrd - FLastLoadDisCoord > MMToSysCrd(FParams.LoadStep) then
    begin

      {$IFDEF LOG}
      Writeln(FLog, Format('LoadData: %d...%d', [FLastLoadDisCoord - 1, MaxDisCrd]));
      {$ENDIF}

      FDataCont.LoadData(ABStr, FLastLoadDisCoord - 1, MaxDisCrd, 0, BlockOk);
     {
      FLastLoadDisCoord:= MaxDisCrd;
      I:= Random(400);
      if I > 300 then Ne.ScanChNum:= 11 else
        if I > 200 then Ne.ScanChNum:= 10 else
          if I > 100 then Ne.ScanChNum:= 3 else Ne.ScanChNum:= 2;

      Ne.BScanRect.StartDisCrd:= FLastLoadDisCoord;          // Координаты начала и конца
      Ne.BScanRect.EndDisCrd:=   FLastLoadDisCoord - 10;
      Ne.BScanRect.StartDelay:=  60;                     // Определяем координаты по задержке
      Ne.BScanRect.EndDelay:=    66;
      Ne.Angle:= 45;
      Ne.CentrDisCrd:= FLastLoadDisCoord - 5;
      Ne.CentrDel:= 63;
      Ne.TwoEcho:= False;
      AddPB(R, Ne.ScanChNum, Ne);                            // Добавляем пачку
      }
    end;

  {$IFDEF ONLY_PB}
  for R:= FCfg.MinRail to FCfg.MaxRail do        // Debug - Пометка всех пачек
    for PBIdx:= 0 to Length(FBPList[R]) - 1 do
      if not FBPList[R, PBIdx].Free then         // Пометка найденных пачек
      begin
        FBPList[R, PBIdx].Free:= True;

      //  if (not FFlg) and (SysCrdToMM(FBPList[R, PBIdx].BScanRect.EndDisCrd - FBPList[R, PBIdx].BScanRect.StartDisCrd) > 45) then
      //  if not Sensor1Test(R, FBPList[R, PBIdx].CentrRealCrd) then // Проверка попадания в зону отмеченную датчиком металла
      //  if FBPList[R, PBIdx].TwoEcho then

        begin
          I:= 0;
          SetLength(ABItems, 1);
          ABItems[I].ScanCh       := FBPList[R, PBIdx].ScanChNum;
          ABItems[I].StartDisCoord:= FBPList[R, PBIdx].BScanRect.StartDisCrd;
          ABItems[I].EndDisCoord  := FBPList[R, PBIdx].BScanRect.EndDisCrd;
          ABItems[I].StartDelay   := FBPList[R, PBIdx].BScanRect.StartDelay;
          ABItems[I].EndDelay     := FBPList[R, PBIdx].BScanRect.EndDelay;

          FDataCont.AddAirBrushMark(R, FBPList[R, PBIdx].CentrRealCrd, ABItems);
          SetLength(ABItems, 0);
       //   FDataCont.AddTextLabel(Format('%3.3f', [FBPList[R, PBIdx].Angle]));
       //   FFlg:= True;
        end;
      end;
  {$ENDIF}


  {$IFNDEF ONLY_PB}
                                 // Вторичный анализ данных
                                 // Внесение новых пачек в блоки КП или создание новых блоков КП

  for R:= FCfg.MinRail to FCfg.MaxRail do
    for PBIdx:= 0 to Length(FBPList[R]) - 1 do
      if not FBPList[R, PBIdx].Free then         // Просматриваем имеющиеся пачки
      begin
        Flag:= False;
        {$IFNDEF NOCONSOLIDATION}
        for ABIdx:= 0 to Length(FABList[R]) - 1 do
          if not FABList[R, ABIdx].Free then     // Просматриваем имеющиеся блоки КП
          begin

            {$IFDEF LOG}
            Writeln(FLog, Format('TryAdd: R = %d; BlockIdx = %d; BlockCrd = %d; PBIdx = %d, PBCrd = %d, Delta = %d, Th = %d', [Ord(R), ABIdx, FABList[R, ABIdx].RealCrd, PBIdx, FBPList[R, PBIdx].CentrRealCrd,FABList[R, ABIdx].RealCrd - FBPList[R, PBIdx].CentrRealCrd, MMToSysCrd(FParams.MaxLenBetweenABMember)]));
            {$ENDIF}

            if Abs(FABList[R, ABIdx].RealCrd - FBPList[R, PBIdx].CentrRealCrd) < MMToSysCrd(FParams.CalcWidth) then   // Проверяем входит ли пачка в этот блок КП
            begin                                                // Входит
              if FABList[R, ABIdx].PBCount <= 1023 then
                FABList[R, ABIdx].PBList[FABList[R, ABIdx].PBCount]:= FBPList[R, PBIdx];
              Inc(FABList[R, ABIdx].PBCount);
             {
              FABList[R, ABIdx].RealCrd:= 0;
              for I:= 0 to FABList[R, ABIdx].PBCount - 1 do
                FABList[R, ABIdx].RealCrd:= FABList[R, ABIdx].RealCrd + FABList[R, ABIdx].PBList[I].CentrDisCrd;
              FABList[R, ABIdx].RealCrd:= Round(FABList[R, ABIdx].RealCrd / FABList[R, ABIdx].PBCount);
             }
              Flag:= True;

              {$IFDEF LOG}
              Writeln(FLog, 'Add OK');
              {$ENDIF}

              Break;                                            // Пачку пристроили - с этой пачкой все
            end
          end;
        {$ENDIF}

        if not Flag then // Пачку не пристроили - для нее создаем новый блок
        begin
          New.RealCrd:= Trunc(FBPList[R, PBIdx].CentrRealCrd / MMToSysCrd(FParams.CalcWidth)) * MMToSysCrd(FParams.CalcWidth);
          New.PBList[0]:= FBPList[R, PBIdx];
          New.PBCount:= 1;
          I:= AddAB(R, New);

          {$IFDEF LOG}
          Writeln(FLog, Format('AddBlock: R = %d; PBIdx = %d; PBCrd = %d -> New Block: Idx = %d', [Ord(R), PBIdx, FBPList[R, PBIdx].CentrRealCrd, I]));
          {$ENDIF}
        end;

        FBPList[R, PBIdx].Free:= True;
      end;
                                          // Принятие решения

 for R:= FCfg.MinRail to FCfg.MaxRail do
    for ABIdx:= 0 to Length(FABList[R]) - 1 do
      if (not FABList[R, ABIdx].Free) and
         (FLastLoadDisCoord -
          (FABList[R, ABIdx].RealCrd + MMToSysCrd(FParams.CalcWidth)) // Конечная координата зоны поиска
                                                            >
           MMToSysCrd(FParams.DecisionMakingBoundary)) then
      begin
        Flg:= False;

        {$IFDEF LOG}
//          Writeln(FLog, Format('Call Sensor1Test: R = %d; RealCrd = %d', [Ord(R), FABList[R, ABIdx].RealCrd]));
        {$ENDIF}

      (*
        if Sensor1Test(R, FABList[R, ABIdx].RealCrd) then // Проверка попадания в зону отмеченную датчиком металла
        begin // В зоне фиксируем только пачки 2 эхо
          SetLength(ABItems, 0);
          for PBIdx:= 0 to FABList[R, ABIdx].PBCount - 1 do  // Если в зоне то ищем признак 2ЭХО
            if FABList[R, ABIdx].PBList[PBIdx].TwoEcho then
            begin
              SetLength(ABItems, Length(ABItems) + 1);
              for I:= 0 to FABList[R, ABIdx].PBCount - 1 do
              begin
                J:= Length(ABItems) - 1;
                ABItems[J].ScanCh       := FABList[R, ABIdx].PBList[I].ScanChNum;
                ABItems[J].StartDisCoord:= FABList[R, ABIdx].PBList[I].BScanRect.StartDisCrd;
                ABItems[J].EndDisCoord  := FABList[R, ABIdx].PBList[I].BScanRect.EndDisCrd;
                ABItems[J].StartDelay   := FABList[R, ABIdx].PBList[I].BScanRect.StartDelay;
                ABItems[J].EndDelay     := FABList[R, ABIdx].PBList[I].BScanRect.EndDelay;
              end;
              Flg:= True;
            end;
        end
        else *)
        begin // Вне зоны фиксируем все пачки

          for Ch_ := 0 to 16 do
          begin
            Summ_[Ch_]:= 0;
            Count_[Ch_]:= 0;
          end;

          for I:= 0 to FABList[R, ABIdx].PBCount - 1 do
          begin
            Inc(Count_[FABList[R, ABIdx].PBList[I].ScanChNum]);
            Summ_[FABList[R, ABIdx].PBList[I].ScanChNum]:= Summ_[FABList[R, ABIdx].PBList[I].ScanChNum] + Abs(FABList[R, ABIdx].PBList[I].BScanRect.EndDelay - FABList[R, ABIdx].PBList[I].BScanRect.StartDelay);
          end;

          if Count_[2] + Count_[3] + Count_[10] + Count_[11] <> 0 then
          begin
            Flg:= True;
            SetLength(ABItems, 1);
            for Ch_ := 0 to 16 do
              if Count_[Ch_] <> 0 then Res_[Ch_]:= Summ_[Ch_] / Count_[Ch_]
                                  else Res_[Ch_]:= 0;

            I:= 0;
            ABItems[I].ScanCh       := 0;
            ABItems[I].StartDisCoord:= FABList[R, ABIdx].RealCrd;
            ABItems[I].EndDisCoord  := FABList[R, ABIdx].RealCrd + MMToSysCrd(FParams.CalcWidth);

            J:= Round((Res_[10] + Res_[11]) - (Res_[2] + Res_[3]));
            if (J < -32768) and (J > 32767) then J:= 0;
            (PSmallint(@ABItems[I].StartDelay))^:= J;
          end
          else Flg:= False;
        end;

        if Flg then
        begin
        {
          if Assigned(FJobEvent2) then
          begin
            FJobEvent2(R,
                       FABList[R, ABIdx].RealCrd + MMToSysCrd(AirBrushPos),
                       FDataCont.DisToSysCoord(FABList[R, ABIdx].RealCrd + MMToSysCrd(AirBrushPos)),
                       FABList[R, ABIdx].RealCrd,
                       ABItems);
          end
          else
          if Assigned(FJobEvent) then
          begin
            FJobEvent(R, FABList[R, ABIdx].RealCrd + MMToSysCrd(AirBrushPos), FDataCont.DisToSysCoord(FABList[R, ABIdx].RealCrd + MMToSysCrd(AirBrushPos)));
            FDataCont.AddAirBrushMark(R, FABList[R, ABIdx].RealCrd, ABItems);
          end
          else }
          FDataCont.AddAirBrushMark(R, - 1000000, ABItems);
        end;

        FABList[R, ABIdx].Free:= True;
        {$IFDEF LOG}
        Writeln(FLog, Format('Free Block: R: = %d; Idx = %d; PBCount = %d', [Ord(R), ABIdx, FABList[R, ABIdx].PBCount]));
        {$ENDIF}

        //AddToLog(Format('Принятие решения по блокам КП: dL = %d', [FLastLoadDisCoord - FABList[R, ABIdx].Coord]));
      end;

  (*                                       // Удаление устаревших отметок датчика металла
  for R:= FCfg.MinRail to FCfg.MaxRail do
    for SIdx:= 0 to Length(FSensor1List[R]) - 1 do
    begin
      {$IFDEF LOG}
//        Writeln(FLog, Format('Analyze Free Sensor: R = %d; Idx = %d, EdRealCrd = %d, CurrRealCrd = %d, Delta = %d; Th = %d', [Ord(R), SIdx, FSensor1List[R, SIdx].EdRealCrd, FLastLoadDisCoord, FLastLoadDisCoord - FSensor1List[R, SIdx].EdRealCrd, MMToSysCrd(FParams.WorkZone)]));
      {$ENDIF}
      if (not FSensor1List[R, SIdx].Free) and
         (FLastLoadDisCoord - FSensor1List[R, SIdx].EdRealCrd > MMToSysCrd(FParams.WorkZone)) then
      begin
        FSensor1List[R, SIdx].Free:= True;
        {$IFDEF LOG}
//          Writeln(FLog, Format('Free Sensor: R = %d; Idx = %d', [Ord(R), SIdx]));
        {$ENDIF}
      end;
    end;
   *)
  {$ENDIF}

  FStopAnalyzTime:= GetTickCount();

  SummAnalyzTime:= SummAnalyzTime + (FStopAnalyzTime - FStartAnalyzTime);
  CS.Leave;
end;


end.
