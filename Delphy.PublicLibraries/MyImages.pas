unit MyImages;

{
  ������ ��� �������������� ������ � ��������� ��� �� Canvas.
  ������������ ������������� �����,
  �������������� ��������� (� �.�. �� ������ ��������),
  ������������ ��������� (������ ��� ������������� ������),
  ����������� ������ � �����, ������ ��� ������,
  ������������ ����� (��������� �� ��������, ����������� �� ���������).

  �������:

  1. ����� ���������������� ������ � �������� � ������.

        S:= '�� ��������� ������� �������� ����� ������� �� ' +
            '�������������� ��������: <br>' + S + '<br>' +
            '<img=minus> - ��������, <img=plus> - ���������';

        with TExtString.Create( S ) do
        begin
          Indention:= 0;  // ��� ������.
          DrawMultiline( Canvas, 20, 320, Butt.Left - 20, BP.Top - 320 );
          Free;
        end;

  2. ����� ����� ������ � �������������� ���������� � �������� � �����.

        Caption:= TExtString.Create( '������� <img=plus> ��� �����������' )
        Caption.Align:= aCenter;
        Caption.Indention:= 0;
        Caption.Draw(Buffer.Canvas, 100, 50 );


  ��� �������� ������ ��� �� ������ � ������:
       Images.Load( 'minus', 'tip_minus.bmp' );
       Images.Load( 'plus', 'tip_plus.bmp' );

  ��� ����� ������ � ������:

       <br> - ������� �� ����. ������ ��� ������������� ������.
       <img=ID> - ������� � ����� ������.

  ����� �� ������ ����� ����� �������� �������? ---> ��, �������� SourceString.

  ��������������� �����:

  ����������� ����� TPageOut ��� ����������� ���������������� ������. �������� ������ �������� �� ���������� '
    � ��������� ��������.

}


interface
uses Classes, Dialogs, Graphics, Forms, Windows, ExtCtrls, StrUtils, SysUtils, RegExpr, Math, PublicData, CfgTablesUnit;


const
  RefChar = #8;

type

  TAlign = ( aLeft = 0, aCenter = 1, aRight = 2, aJustify = 3 );
  TVAlign = ( vaTop = 0, vaCenter = 1, vaBottom = 2 );

  TMyImageList = array of
  record
    ID: string;
    Image: TImage;
  end;

  TTextRefs = array of Integer;

  TExtChar =
  record
    case CharType: ( cChar, cImg, cBR ) of
      cChar: ( Ch: Char );
      cImg: ( Img: TImage );
  end;
  PExtChar = ^TExtChar;

  const ecSpace: TExtChar = ( CharType: cChar; Ch: ' ' );
  type

  TCharList = class( TList )
  private
    function Get(Index: Integer): PExtChar;
    procedure Put(Index: Integer; Item: PExtChar);

  public
    property Items[Index: Integer]: PExtChar read Get write Put; default;
    function Add: PExtChar;
    procedure Clear; override;
  end;

  TExtString = class
  private
    SourceStr: string;
    Data: TCharList;
    IsParsed: Boolean;
    SpaceWidth: Integer;

    procedure Parse;
    procedure SetSource( S: string );
    procedure SimpleDrawLine( Canvas: TCanvas; X, Y, IdxFrom, IdxTo: Integer );
    procedure DrawLine( Canvas: TCanvas; X, Y, BoxWidth, BoxHeight, IdxFrom, IdxTo: Integer );

    function TrimLeft(Index: Integer): Integer;
    function TrimRight(Index: Integer): Integer;
    function IsSpace( Index: Integer ): Boolean;

  protected


  public
    Align: TAlign;
    VAlign: TVAlign;
    Spacing: Integer;
    Indention: Integer; // ������ ������.
    BorderWidth: Integer;
    BorderColor: TColor;
    LineHeight: Integer;
//    BoxWidth: Integer;
    property SourceString: string read SourceStr write SetSource;
    procedure Concat( S: string );
    constructor Create; overload;
    constructor Create( S: string; Translate: Boolean = False ); overload;
    destructor Destroy; override;
    function GetCharWidth(Canvas: TCanvas; Index: Integer): Integer;
    function GetCharHeight(Canvas: TCanvas; Index: Integer): Integer;
    function GetWidth( Canvas: TCanvas; IdxFrom: Integer = 0; IdxTo: Integer = -1 ): Integer;
    function GetHeight(Canvas: TCanvas): Integer;
    function CountOf( Char: TExtChar; IdxFrom: Integer = 0; IdxTo: Integer = -1 ): Integer;

    procedure Draw( Canvas: TCanvas; X, Y, Width, Height: Integer ); overload;
    procedure Draw( Canvas: TCanvas; X, Y: Integer ); overload;
    procedure DrawMultiline( Canvas: TCanvas; X, Y: Integer ); overload; // ������������� ����� ��� ���������.
    procedure DrawMultiline( Canvas: TCanvas; X, Y, BoxWidth, BoxHeight: Integer ); overload; // ������������� ����� � ������������ ���������.
    function DrawPage(Canvas: TCanvas; BoxLeft, BoxTop, BoxWidth, BoxHeight, StartIdx: Integer; out EndIdx: Integer ): Boolean;
  end;

  TPageOut = class
  private
    StartIdx: array of Integer;
    Text: TExtString;
    CPage: Integer;
    Left: Integer;
    Top: Integer;
    Width: Integer;
    Height: Integer;
    FLastPage: Boolean;
    IsDraw: Boolean;

    function GetLastPage: Boolean;

  public
    Canvas: TCanvas;
    property LastPage: Boolean read GetLastPage;
    property Page: Integer read CPage;
    constructor Create( Canvas: TCanvas; Left, Top, Width, Height: Integer; Text: TExtString );
    procedure Draw;
    function NextPage: Boolean;
    function PrevPage: Boolean;
  end;


  TImages = class
  private
    Images: TMyImageList;

    function GetOfs(ID: string): Integer;

  public
    function Load( ID: string; Filename: string ): Boolean;
    function Get( ID: string ): TImage;
  end;


var
  Images: TImages;

function CompareChar( Char1, Char2: TExtChar ): Boolean;

implementation

uses Controls;

function CompareChar( Char1, Char2: TExtChar ): Boolean;
begin
  Result:= False;
  if Char1.CharType <> Char2.CharType then Exit;
  case Char1.CharType of
    cChar: Result:= Char1.Ch = Char2.Ch;
    cImg: Result:= Char1.Img = Char2.Img;
    cBR: Result:= True;
  end;
end;


{ TImages }

function TImages.GetOfs(ID: string): Integer;
var
  i: Integer;

begin
  Result:= -1;
  for i:= 0 to High( Images ) do
  if Images[i].ID = ID then
  begin
    Result:= i;
    Break;
  end;
end;

function TImages.Get(ID: string): TImage;
var
  Ofs: Integer;

begin
  Ofs:= GetOfs( ID );
  if Ofs = -1 then Result:= nil else Result:= Images[Ofs].Image;  
end;

function TImages.Load(ID, Filename: string): Boolean;
begin
  Result:= False;
  SetLength( Images, Length( Images ) + 1 );
  Images[ High( Images ) ].ID:= ID;
  Images[ High( Images ) ].Image:= TImage.Create( nil );
  try
    Images[ High( Images ) ].Image.Picture.LoadFromFile( Filename );
  except
    Exit;
  end;
  Result:= True;
end;

{ TExtString }

procedure TExtString.Concat(S: string);
begin
  SourceStr:= SourceStr + S;
  IsParsed:= False;
end;

constructor TExtString.Create(S: string; Translate: Boolean = False);
begin
  if Translate then SourceStr:= TLng.Caption(S) else SourceStr:= S;
  IsParsed:= False;
  Data:= TCharList.Create;
  Spacing:= 0;
  BorderWidth:= 0;
  BorderColor:= clBlack;
  Align:= aLeft;
  VAlign:= vaTop;
  LineHeight:= -1;
  Indention:= 3;
end;

constructor TExtString.Create();
begin
  Create('');
end;

function TExtString.GetCharWidth(Canvas: TCanvas; Index: Integer): Integer;
begin
  Parse;
  case Data[Index].CharType of
    cChar:
      if ( Align = aJustify ) and ( SpaceWidth <> -1 ) and ( Data[Index].Ch = ' ' ) then
        Result:= SpaceWidth
      else
        Result:= Canvas.TextWidth( Data[Index].Ch ) + Spacing;
    cImg: Result:= Data[Index].Img.Picture.Width + Spacing;
  end;
end;

function TExtString.GetCharHeight(Canvas: TCanvas; Index: Integer): Integer;
begin
  Parse;
  case Data[Index].CharType of
    cChar: Result:= Canvas.TextHeight( Data[Index].Ch );
    cImg: Result:= Data[Index].Img.Picture.Height;
  end;
end;

function TExtString.CountOf( Char: TExtChar; IdxFrom: Integer = 0; IdxTo: Integer = -1 ): Integer;
var
  i: Integer;
begin
  Parse;
  if IdxTo = -1 then IdxTo:= Data.Count - 1;
  Result:= 0;
  for i:= IdxFrom to IdxTo do
  if CompareChar( Char, Data[i]^ ) then Inc( Result );
end;

function TExtString.GetWidth(Canvas: TCanvas; IdxFrom: Integer = 0; IdxTo: Integer = -1): Integer;
var
  i: Integer;

begin
  Parse;
  if IdxTo = -1 then IdxTo:= Data.Count - 1;
  Result:= 0;
  for i:= IdxFrom to IdxTo do
    Result:= Result + GetCharWidth(Canvas, i);
end;

function TExtString.GetHeight(Canvas: TCanvas): Integer;
var
  i: Integer;

begin
  Parse;
  Result:= 0;
  for i:= 0 to Data.Count - 1 do
    Result:= Max( Result, GetCharHeight(Canvas, i) );
end;

procedure TExtString.Parse;
var
  r: TRegExpr;
  i: Integer;
  i0: Integer;
  Char: TExtChar;

begin
  if IsParsed then Exit;
  Data.Clear;

  i0:= 1;
  r:= TRegExpr.Create;
//  r.Expression:= '<img=([^>]+)>';
  r.Expression:= '<img=([^>]+)>|(<br> *)';
  if r.Exec( SourceStr ) then
  repeat

    // ������� �������.
    for i:= i0 to r.MatchPos[0] - 1 do
    with Data.Add^ do
    begin
      CharType:= cChar;
      Ch:= SourceStr[i];
    end;

    // <img>
    if r.Match[1] <> '' then
    with Data.Add^ do
    begin
      Img:= Images.Get( r.Match[1] );
      if Assigned( Img ) then CharType:= cImg else
      begin
        CharType:= cChar;
        Ch:= '?';
      end;
    end else

    // <br>
    if r.Match[2] <> '' then
    with Data.Add^ do
    begin
      CharType:= cBR;
    end;

    i0:= r.MatchPos[0] + r.MatchLen[0];
  until not r.ExecNext;

  // ������� ������.
  for i:= i0 to Length( SourceStr ) do
  with Data.Add^ do
  begin
    CharType:= cChar;
    Ch:= SourceStr[i];
  end;

  r.Free;
  IsParsed:= True;
end;

procedure TExtString.SetSource(S: string);
begin
  if SourceStr <> S then
  begin
    SourceStr:= S;
    IsParsed:= False;
  end;
end;

procedure TExtString.DrawLine(Canvas: TCanvas; X, Y, BoxWidth, BoxHeight, IdxFrom, IdxTo: Integer );
var
  Color: TColor;
  BStyle: TBrushStyle;
  cX, cY: Integer;

begin
  Parse;
  case Align of
    aCenter: X:= X + BoxWidth div 2 - GetWidth( Canvas, IdxFrom, IdxTo ) div 2;
    aRight: X:= X + BoxWidth - GetWidth( Canvas, IdxFrom, IdxTo );
    aJustify:
      if BoxWidth <> 0 then
      begin
        SpaceWidth:= 0;
        if CountOf( ecSpace, IdxFrom, IdxTo ) <> 0 then SpaceWidth:= ( BoxWidth - GetWidth( Canvas, IdxFrom, IdxTo ) ) div CountOf( ecSpace, IdxFrom, IdxTo ) else SpaceWidth:= -1;
      end;
  end;

  case VAlign of
    vaCenter: Y:= Y + BoxHeight div 2;
    vaBottom: Y:= Y + BoxHeight;
  end;

  if BorderWidth = 0 then
    SimpleDrawLine( Canvas, X, Y, IdxFrom, IdxTo )
  else
  begin
    BStyle:= Canvas.Brush.Style;
    Color:= Canvas.Font.Color;
    Canvas.Font.Color:= BorderColor;
    Canvas.Brush.Style:= bsClear;
    for cX:= X - BorderWidth to X + BorderWidth do
    for cY:= Y - BorderWidth to Y + BorderWidth do
      SimpleDrawLine( Canvas, X, Y, IdxFrom, IdxTo );
    Canvas.Font.Color:= Color;
    SimpleDrawLine( Canvas, X, Y, IdxFrom, IdxTo );
    Canvas.Brush.Style:= BStyle;
  end;

  SpaceWidth:= -1;
end;

procedure TExtString.SimpleDrawLine(Canvas: TCanvas; X, Y, IdxFrom, IdxTo: Integer);
var
  i: Integer;
  dx: Integer;
  cY: Integer;

begin
  dX:= 0;
  for i:= IdxFrom to IdxTo do
  begin
    case VAlign of
      vaTop: cY:= Y;
      vaCenter: cY:= Y - GetCharHeight( Canvas, i ) div 2;
      vaBottom: cY:= Y - GetCharHeight( Canvas, i );
    end;
    case Data[i].CharType of
      cChar: Canvas.TextOut( X + dX, cY, Data[i].Ch );
      cImg: Canvas.Draw( X + dX, cY, Data[i].Img.Picture.Graphic );
    end;
    dX:= dX + GetCharWidth( Canvas, i );
  end;
end;

destructor TExtString.Destroy;
begin
  Data.Free;
  inherited;
end;

procedure TExtString.Draw(Canvas: TCanvas; X, Y, Width, Height: Integer);
begin
  Parse;
  DrawLine( Canvas, X, Y, Width, Height, 0, Data.Count - 1 );
end;

function TExtString.TrimLeft(Index: Integer): Integer;
begin
  Result:= Index;
  while IsSpace( Result ) do Inc( Result );
end;

function TExtString.TrimRight(Index: Integer): Integer;
begin
  Result:= Index;
  while IsSpace( Result ) do Dec( Result );
end;


function TExtString.IsSpace( Index: Integer ): Boolean;
begin
  if ( Index < 0 ) or ( Index > Data.Count - 1 ) then
    Result:= False
  else
    Result:= ( Data[Index].CharType = cBR ) or
             ( ( Data[Index].CharType = cChar ) and ( Data[Index].Ch = ' ' ) );
end;


function TExtString.DrawPage(Canvas: TCanvas; BoxLeft, BoxTop, BoxWidth, BoxHeight, StartIdx: Integer; out EndIdx: Integer): Boolean;
var
  W: Integer;
  i: Integer;
  cX: Integer;
  cY: Integer;
  WordIdx: Integer;
  WordIdxPrev: Integer;
  cLineHeight: Integer;
  Align_: TAlign;
  BR: Boolean;
  PrevBR: Boolean;
  cBoxWidth: Integer;
  Ind: Integer;

begin
  Parse;
  if LineHeight = -1 then cLineHeight:= Canvas.TextHeight('A') else cLineHeight:= LineHeight;
  Ind:= Canvas.TextWidth( StringOfChar( 'A', Indention ) );

  if VAlign <> vaTop then VAlign:= vaTop;

  SpaceWidth:= -1;
  Align_:= Align;
  W:= 0;
  cY:= BoxTop;
  WordIdxPrev:= StartIdx;
  WordIdx:= 0;                          
  i:= StartIdx;
  PrevBR:= StartIdx = 0;
  if PrevBR then cBoxWidth:= BoxWidth - Ind else cBoxWidth:= BoxWidth;
  while i <= Data.Count - 1 do
  begin
    W:= W + GetCharWidth( Canvas, i );
    if ( Data[i].CharType = cChar ) and ( Data[i].Ch = ' ' ) then WordIdx:= i + 1;
    BR:= Data[i].CharType = cBR;
    if BR then
    begin
      if Align = aJustify then Align:= aLeft;
      WordIdx:= i + 1;
    end;
    if BR or ( W > cBoxWidth ) then
    begin
      if WordIdx = WordIdxPrev then Exit;
      if PrevBR then cX:= BoxLeft + Ind else cX:= BoxLeft;
      DrawLine( Canvas, cX, cY, cBoxWidth, BoxHeight, TrimLeft( WordIdxPrev ), TrimRight( WordIdx - 1 ) );
      Align:= Align_;
      cY:= cY + cLineHeight;
      if cY + cLineHeight > BoxTop + BoxHeight then
      begin
        EndIdx:= WordIdx;
        Result:= True;
        Exit;
      end;
      WordIdxPrev:= WordIdx;
      i:= WordIdx;
      W:= 0;
      PrevBR:= BR;
      if BR then cBoxWidth:= BoxWidth - Ind else cBoxWidth:= BoxWidth;
    end else
      Inc( i );
  end;

  if Align = aJustify then Align:= aLeft;
  DrawLine( Canvas, BoxLeft, cY, cBoxWidth, BoxHeight, WordIdxPrev, Data.Count - 1 );
  Align:= Align_;
  EndIdx:= Data.Count - 1;
  Result:= False;
end;

procedure TExtString.Draw(Canvas: TCanvas; X, Y: Integer);
begin
  Draw( Canvas, X, Y, 0, 0 );
end;

procedure TExtString.DrawMultiline(Canvas: TCanvas; X, Y: Integer);
var
  Idx: Integer;
  
begin
  Align:= aLeft;
  VAlign:= vaTop;
  DrawPage( Canvas, X, Y, 10000, 10000, 0, Idx );
end;

procedure TExtString.DrawMultiline(Canvas: TCanvas; X, Y, BoxWidth, BoxHeight: Integer);
var
  Idx: Integer;

begin
  DrawPage( Canvas, X, Y, BoxWidth, BoxHeight, 0, Idx );
end;


{ TCharList }

function TCharList.Add: PExtChar;
begin
  New( Result );
  inherited Add( Result );
end;

procedure TCharList.Clear;
var
  i: Integer;

begin
  for i:= 0 to Count - 1 do
    Dispose( Items[i] );
  inherited;
end;

function TCharList.Get(Index: Integer): PExtChar;
begin
  Result:= inherited Get(Index);
end;

procedure TCharList.Put(Index: Integer; Item: PExtChar);
begin
  inherited Put(Index, Item);
end;

{ TPageOut }

constructor TPageOut.Create(Canvas: TCanvas; Left, Top, Width, Height: Integer; Text: TExtString);
begin
  Self.Canvas:= Canvas;
  Self.Text:= Text;
  Self.Left:= Left;
  Self.Top:= Top;
  Self.Width:= Width;
  Self.Height:= Height;
  CPage:= 0;
  SetLength( StartIdx, 1 );
  StartIdx[0]:= 0;
  IsDraw:= False;
end;

procedure TPageOut.Draw;
var
  Idx: Integer;

begin
  FLastPage:= not Text.DrawPage( Canvas, Left, Top, Width, Height, StartIdx[CPage], Idx );

  if not FLastPage then
  begin
    if Length( StartIdx ) <> CPage + 2 then SetLength( StartIdx, CPage + 2 );
    StartIdx[CPage + 1]:= Idx;
  end;

  IsDraw:= True;
end;

function TPageOut.GetLastPage: Boolean;
begin
  // ��������� �������� ��� ���, ������������ �� ����������� ���������.
  if not IsDraw then Draw;
  Result:= FLastPage;
end;

function TPageOut.NextPage: Boolean;
begin
  Result:= False;
  if CPage + 1 > High( StartIdx ) then Exit;
  Inc( CPage );
  Result:= True;
  IsDraw:= False;
end;

function TPageOut.PrevPage: Boolean;
begin
  Result:= False;
  if CPage <= 0 then Exit;
  Dec( CPage );
  Result:= True;
  IsDraw:= False;
end;

initialization
  Images:= TImages.Create;

finalization
  Images.Free;

end.
