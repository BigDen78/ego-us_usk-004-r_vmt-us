object VKTempForm: TVKTempForm
  Left = 920
  Top = 440
  Width = 870
  Height = 572
  Caption = 'VKTempForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object VirtualKeyboardPanel: TPanel
    Left = 93
    Top = 66
    Width = 608
    Height = 362
    BorderStyle = bsSingle
    TabOrder = 0
    object PSysButtons: TPanel
      Left = 1
      Top = 1
      Width = 128
      Height = 356
      Align = alLeft
      TabOrder = 0
      object PLanguage: TPanel
        Left = 1
        Top = 1
        Width = 126
        Height = 145
        Align = alTop
        TabOrder = 0
        object BSetLangRus: TSpeedButton
          Left = 6
          Top = 6
          Width = 116
          Height = 63
          GroupIndex = 1
          Down = True
          Caption = #1056#1059#1057'.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object BSetLangEng: TSpeedButton
          Left = 6
          Top = 75
          Width = 116
          Height = 63
          GroupIndex = 1
          Caption = #1040#1053#1043#1051'.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object PEdit: TPanel
        Left = 1
        Top = 146
        Width = 126
        Height = 148
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object bReset: TSpeedButton
          Left = 6
          Top = 74
          Width = 116
          Height = 63
          Caption = #1057#1041#1056#1054#1057
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -19
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object bDelete: TSpeedButton
          Left = 6
          Top = 6
          Width = 116
          Height = 63
          Caption = #1059#1044#1040#1051'.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -19
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object PFreeSpace: TPanel
        Left = 1
        Top = 294
        Width = 126
        Height = 61
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 2
      end
    end
    object PMainKeys: TPanel
      Left = 129
      Top = 1
      Width = 474
      Height = 356
      Align = alClient
      TabOrder = 1
    end
  end
end
