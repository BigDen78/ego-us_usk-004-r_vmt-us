unit Buffers;

interface
uses Classes, SysUtils, SyncObjs,
     PublicFunc;

const
  BufferSize = 1000000;

type
  TDataBuffer = array[ 0..BufferSize - 1 ] of Byte;

  TOnBufferOverflow = procedure of object;

  TRingBuffer = class
  private
    Buffer: TDataBuffer;
    FReadPosition: Integer;
    FWritePosition: Integer;
    CS: TCriticalSection;
    BufferName: string;
    procedure AddLog( Msg: string );
  public
    AddLogProc: TAddLogProc;
    OnBufferOverflow: TOnBufferOverflow;
    property ReadPosition: Integer read FReadPosition;
    property WritePosition: Integer read FWritePosition;

    constructor Create( BufferName_: string );
    destructor Destroy; override;

    procedure Add( B: Byte );
    function Get( out B: Byte ): Boolean;
    function Read( Offset: Integer ): Byte;
    procedure Clear;
    function DataCount: Integer;
  end;

  TBlockBuffer = class( TRingBuffer )
  private
  public
    procedure AddBlock( Block: TBlock );
    function GetBlock( out Block: TBlock ): Boolean;
  end;

  TPacketBuffer = class( TRingBuffer )
  private
    FPacketSize: Integer;
  public
    constructor Create( BufferName_: string; PacketSize: Integer );
    procedure AddPacket( P: Pointer );
    function GetPacket( P: Pointer ): Boolean;
  end;




implementation

// -----------------------------------------------------------------------------
// -----------------------------  TRingBuffer  ---------------------------------

constructor TRingBuffer.Create;
begin
  AddLogProc:= nil;
  CS:= TCriticalSection.Create;
  BufferName:= BufferName_;
  Clear;
  OnBufferOverflow:= nil;
end;

procedure TRingBuffer.Clear;
begin
  CS.Enter;
  FReadPosition:= 0;
  FWritePosition:= 0;
  CS.Leave;
end;

procedure TRingBuffer.AddLog(Msg: string);
begin
  if Assigned( AddLogProc ) then AddLogProc( BufferName + ': ' + Msg );
end;

procedure TRingBuffer.Add( B: Byte );
begin
  CS.Enter;

  Buffer[ FWritePosition ]:= B;
  Inc( FWritePosition );
  if ( FWritePosition = FReadPosition ) then
  begin
    AddLog( 'BUFFER OVERFLOW!' );
    if Assigned( OnBufferOverflow ) then OnBufferOverflow;
  end;
  if FWritePosition >= BufferSize then FWritePosition:= 0;

  CS.Leave;
end;

function TRingBuffer.Get( out B: Byte ): Boolean;
begin
  CS.Enter;

  if FWritePosition = FReadPosition then Result:= False else
  begin
    if FReadPosition >= BufferSize then FReadPosition:= 0; // �� ������ ������.
    B:= Buffer[ FReadPosition ];
    Result:= True;
    Inc( FReadPosition );
    if FReadPosition >= BufferSize then FReadPosition:= 0;
  end;

  CS.Leave;
end;


function TRingBuffer.Read( Offset: Integer ): Byte;
var
  Address: Integer;

begin
  CS.Enter;

  if FReadPosition + Offset >= BufferSize then Address:= FReadPosition + Offset - BufferSize else Address:= FReadPosition + Offset;
  Result:= Buffer[ Address ];

  CS.Leave;
end;

function TRingBuffer.DataCount: Integer;
begin
  CS.Enter;
  if FReadPosition <= FWritePosition then
    Result:= FWritePosition - FReadPosition
  else
    Result:= BufferSize - FReadPosition + FWritePosition;
  CS.Leave;
end;

// -----------------------------------------------------------------------------
// -----------------------------  TBlockBuffer  --------------------------------



procedure TBlockBuffer.AddBlock(Block: TBlock);
var
  BlockSize: Integer;
  i: Integer;

begin
  if TestBlock( Block ) then
  for i:= 1 to 10 do
    Add( Block[i] );
end;

function TBlockBuffer.GetBlock( out Block: TBlock ): Boolean;
var
  i: Integer;

begin
  Result:= False;
  if DataCount < 10 then Exit;
  for i:= 1 to 10 do
    Get( Block[i] );
  Result:= True;
end;


{ TPacketBuffer }


constructor TPacketBuffer.Create(BufferName_: string; PacketSize: Integer);
begin
  FPacketSize:= PacketSize;
  inherited Create( BufferName_ );
end;

procedure TPacketBuffer.AddPacket(P: Pointer);
var
  i: Integer;

begin
  for i:= 1 to FPacketSize do
  begin
    Add( Byte( P^ ) );
    P:= Pointer( Integer( P ) + 1 );
  end;
end;


function TPacketBuffer.GetPacket(P: Pointer): Boolean;
var
  i: Integer;
  B: Byte;

begin
  Result:= False;
  if DataCount < FPacketSize then Exit;
  for i:= 1 to FPacketSize do
  begin
    Get( B );
    Byte( P^ ):= B;
    P:= Pointer( Integer( P ) + 1 );
  end;
  Result:= True;
end;

destructor TRingBuffer.Destroy;
begin
  CS.Free;
  inherited;
end;

end.
