unit StringConversionEx;

interface


      // �������� �������� ��������� (Dt) �� ������ (��������� ��������� ���������)
      // ��������� �� ����� ������ �������� ������� ���������.
      procedure GetS(var S, Dt: string); overload;
      procedure GetS(var S: string; out Dt: Single); overload;
      procedure GetS(var S: string; out Dt: Integer); overload;
      procedure GetS(var S: string; out Dt: ShortInt); overload;
      procedure GetS(var S: string; out Dt: Byte); overload;
      procedure GetS(var S: string; out Dt: Word); overload;
      procedure GetS(var S: string; out Dt: TDateTime); overload;

      // �������� �������� ��������� (Dt) � ������ (S)
      procedure AddS(var S : string; Dt: string); overload;
      procedure AddS(var S: string;  Dt: Single); overload;
      procedure AddS(var S: string;  Dt: Integer); overload;
      procedure AddS(var S: string;  Dt: SmallInt); overload;
      procedure AddS(var S: string;  Dt: Byte); overload;
      procedure AddS(var S: string;  Dt: Word); overload;
      Procedure AddS(var S: string;  Dt: TDateTime); overload;

implementation

uses StrUtils, SysUtils;

Procedure GetS(var S: String; var Dt: String);
var
 Ch : Char;
 P,
 i  : Integer;

begin
Dt:='';

i:=1;
If S='' then exit;
While S[i]=' ' do
   begin
     inc(i);
     If i>length(S) then
       begin
         S:='';
         exit;
       end;
   end;

If S[i]='"' then
  begin
   Ch:='"';
   inc(i);
  end else Ch:=' ';
P:=PosEx(Ch, S, i);
If P=0 then
  begin
    Dt:=Copy(S, i, 100);
    S:='';
  end else begin
    Dt:=Copy(S, i, P-i);
    S:=Copy(S, P+1, length(S)-P);
  end;
end;

procedure GetS(var S: String; out Dt: Integer);
var
  DtS : string;
  E   : Integer;
begin
 GetS( S, DtS);
 Val(DtS, Dt, E);
end;

procedure GetS(var S: string; out Dt: ShortInt);
var
  DtS : string;
  E   : Integer;
begin
 GetS( S, DtS);
 Val(DtS, Dt, E);
end;

procedure GetS(var S: String; out Dt: Byte);
var
  DtS : String;
  E   : Integer;
begin
 GetS( S, DtS);
 Val(DtS, Dt, E);
end;

procedure GetS(var S: String; out Dt: Word);
var
  DtS : String;
  E   : Integer;
begin
 GetS( S, DtS);
 Val(DtS, Dt, E);
end;

procedure GetS(var S: String; out Dt: Single);
var
  DtS : string;

begin
 GetS( S, DtS);
 TryStrToFloat(DtS, Dt)
end;

Procedure GetS(var S: String; out Dt: TDateTime); overload;
var
  DtS : String;
  Y,
  M,
  D   : Word;
begin
 try
  GetS(S, DtS);
  if DtS = '' then
    Dt:= 0
  else
  begin
    D:= StrToInt( Copy(DtS,1,2) );
    M:= StrToInt( Copy(DtS,4,2) );
    Y:= StrToInt( Copy(DtS,7,4) );
    Dt:= EncodeDate(Y, M, D);
  end;
 except
 end;
end;

procedure AddS(var S: String; Dt: Single);
var
  SS : string;
 
begin
  SS:= Format( '%.5f', [ Dt ] ); // FloatToStr(Dt);
  AddS(S, SS);
end;

procedure AddS(var S: String; Dt: String);
begin
  If Pos(' ', Dt)<>0 then Dt:='"' + Dt + '"';
  While length(Dt)<15 do
    Dt:=Dt+' ';
  S:=S+Dt;
end;

procedure AddS(var S: String; Dt: Byte);
var
 SS : String;
begin
  SS:=IntToStr(Dt);
  AddS(S, SS);
end;

procedure AddS(var S: String; Dt: Word);
var
 SS : String;
begin
  SS:=IntToStr(Dt);
  AddS(S, SS);
end;

procedure AddS(var S: String; Dt: Integer);
var
 SS : String;
begin
  SS:=IntToStr(Dt);
  AddS(S, SS);
end;

procedure AddS(var S: String; Dt: SmallInt);
var
 SS : String;
begin
  SS:=IntToStr(Dt);
  AddS(S, SS);
end;

Procedure AddS(var S: String;  Dt: TDateTime);
var
 SS : String;
begin
  DateTimeToString(SS, 'dd.mm.yyyy', Dt);
  AddS(S, SS);
end;

end.


