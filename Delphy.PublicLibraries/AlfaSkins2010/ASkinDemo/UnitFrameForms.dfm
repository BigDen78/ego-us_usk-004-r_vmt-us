inherited Frame_Forms: TFrame_Forms
  Width = 535
  Height = 431
  object sGroupBox7: TsGroupBox [0]
    Left = 23
    Top = 16
    Width = 125
    Height = 205
    Caption = 'Border style'
    TabOrder = 0
    CaptionLayout = clTopCenter
    SkinData.SkinSection = 'GROUPBOX'
    Checked = False
    object sRadioButton12: TsRadioButton
      Tag = 5
      Left = 14
      Top = 25
      Width = 60
      Height = 20
      Caption = 'bsDialog'
      TabOrder = 0
      OnClick = sRadioButton12Click
    end
    object sRadioButton13: TsRadioButton
      Tag = 5
      Left = 14
      Top = 53
      Width = 56
      Height = 20
      Caption = 'bsNone'
      TabOrder = 1
      OnClick = sRadioButton12Click
    end
    object sRadioButton14: TsRadioButton
      Tag = 5
      Left = 14
      Top = 82
      Width = 59
      Height = 20
      Caption = 'bsSingle'
      TabOrder = 2
      OnClick = sRadioButton12Click
    end
    object sRadioButton15: TsRadioButton
      Tag = 5
      Left = 14
      Top = 111
      Width = 70
      Height = 20
      Caption = 'bsSizeable'
      Checked = True
      TabOrder = 3
      TabStop = True
      OnClick = sRadioButton12Click
    end
    object sRadioButton16: TsRadioButton
      Tag = 5
      Left = 14
      Top = 140
      Width = 88
      Height = 20
      Caption = 'bsSizeToolWin'
      TabOrder = 4
      OnClick = sRadioButton12Click
    end
    object sRadioButton17: TsRadioButton
      Tag = 5
      Left = 14
      Top = 169
      Width = 89
      Height = 20
      Caption = 'bsToolWindow'
      TabOrder = 5
      OnClick = sRadioButton12Click
    end
  end
  object sComboBox1: TsComboBox [1]
    Tag = 5
    Left = 279
    Top = 225
    Width = 174
    Height = 21
    Alignment = taLeftJustify
    BoundLabel.Active = True
    BoundLabel.Caption = 'SkinSection name:'
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    SkinData.SkinSection = 'COMBOBOX'
    VerticalAlignment = taAlignTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 15
    ItemIndex = -1
    ParentFont = False
    TabOrder = 8
    Text = 'DIALOG'
    Items.Strings = (
      'FORM'
      'DIALOG'
      'BUTTON'
      'PAGECONTROL')
  end
  object sCheckBox2: TsCheckBox [2]
    Tag = 5
    Left = 313
    Top = 100
    Width = 162
    Height = 20
    Caption = 'Make menu for skins choosing'
    Checked = True
    State = cbChecked
    TabOrder = 9
    ImgChecked = 0
    ImgUnchecked = 0
  end
  object sCheckBox1: TsCheckBox [3]
    Tag = 5
    Left = 313
    Top = 21
    Width = 109
    Height = 20
    Caption = 'Resize border only'
    TabOrder = 10
    OnClick = sCheckBox1Change
    ImgChecked = 0
    ImgUnchecked = 0
  end
  object sCheckBox20: TsCheckBox [4]
    Tag = 5
    Left = 313
    Top = 48
    Width = 67
    Height = 20
    Caption = 'Show grip'
    Checked = True
    State = cbChecked
    TabOrder = 5
    ImgChecked = 0
    ImgUnchecked = 0
  end
  object sSpinEdit4: TsSpinEdit [5]
    Tag = 5
    Left = 404
    Top = 254
    Width = 49
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    Text = '3'
    Visible = False
    SkinData.SkinSection = 'EDIT'
    BoundLabel.Active = True
    BoundLabel.Caption = 'Count of user buttons in title:'
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    MaxValue = 8
    MinValue = 0
    Value = 3
  end
  object sCheckBox4: TsCheckBox [6]
    Left = 289
    Top = 344
    Width = 142
    Height = 20
    Caption = 'Blackout of the main form'
    TabOrder = 12
    ImgChecked = 0
    ImgUnchecked = 0
  end
  object sGroupBox8: TsGroupBox [7]
    Left = 23
    Top = 242
    Width = 125
    Height = 151
    Caption = 'Border icons'
    TabOrder = 2
    CaptionLayout = clTopCenter
    SkinData.SkinSection = 'GROUPBOX'
    Checked = False
    object sCheckBox9: TsCheckBox
      Tag = 5
      Left = 16
      Top = 28
      Width = 89
      Height = 20
      Caption = 'biSystemMenu'
      Checked = True
      State = cbChecked
      TabOrder = 0
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object sCheckBox15: TsCheckBox
      Tag = 5
      Left = 16
      Top = 57
      Width = 71
      Height = 20
      Caption = 'biMaximize'
      Checked = True
      State = cbChecked
      TabOrder = 1
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object sCheckBox16: TsCheckBox
      Tag = 5
      Left = 16
      Top = 86
      Width = 67
      Height = 20
      Caption = 'biMinimize'
      Checked = True
      State = cbChecked
      TabOrder = 2
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object sCheckBox17: TsCheckBox
      Tag = 5
      Left = 16
      Top = 116
      Width = 49
      Height = 20
      Caption = 'biHelp'
      Checked = True
      State = cbChecked
      TabOrder = 3
      ImgChecked = 0
      ImgUnchecked = 0
    end
  end
  object sCheckBox19: TsCheckBox [8]
    Tag = 5
    Left = 313
    Top = 74
    Width = 89
    Height = 20
    Caption = 'Show app icon'
    Checked = True
    State = cbChecked
    TabOrder = 4
    OnClick = sCheckBox19Change
    ImgChecked = 0
    ImgUnchecked = 0
  end
  object sGroupBox9: TsGroupBox [9]
    Left = 173
    Top = 16
    Width = 116
    Height = 113
    Caption = 'Caption alignment'
    TabOrder = 3
    SkinData.SkinSection = 'GROUPBOX'
    Checked = False
    object sRadioButton18: TsRadioButton
      Tag = 5
      Left = 14
      Top = 24
      Width = 81
      Height = 20
      Caption = 'taLeftJustify'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = sRadioButton18Change
    end
    object sRadioButton19: TsRadioButton
      Tag = 5
      Left = 14
      Top = 52
      Width = 63
      Height = 20
      Caption = 'taCenter'
      TabOrder = 1
      OnClick = sRadioButton19Change
    end
    object sRadioButton20: TsRadioButton
      Tag = 5
      Left = 14
      Top = 80
      Width = 87
      Height = 20
      Caption = 'taRightJustify'
      TabOrder = 2
      OnClick = sRadioButton20Change
    end
  end
  object sButton1: TsButton [10]
    Tag = 5
    Left = 280
    Top = 300
    Width = 169
    Height = 30
    Caption = 'Create'
    TabOrder = 1
    OnClick = sButton1Click
  end
  object sEdit2: TsEdit [11]
    Tag = 5
    Left = 279
    Top = 196
    Width = 173
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    Text = 'Test form with custom title'
    SkinData.SkinSection = 'EDIT'
    BoundLabel.Active = True
    BoundLabel.Caption = 'Caption text:'
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
  end
  object sCheckBox3: TsCheckBox [12]
    Tag = 5
    Left = 281
    Top = 163
    Width = 86
    Height = 20
    Caption = 'Allow skinning'
    Checked = True
    State = cbChecked
    TabOrder = 11
    ImgChecked = 0
    ImgUnchecked = 0
  end
  inherited sFrameAdapter1: TsFrameAdapter
    Left = 424
    Top = 16
  end
end
