inherited Frame_TabControls: TFrame_TabControls
  Width = 620
  Height = 440
  object sPageControl1: TsPageControl [0]
    Left = 0
    Top = 0
    Width = 620
    Height = 440
    ActivePage = sTabSheet1
    Align = alClient
    HotTrack = True
    Images = MainForm.ImageList16
    MultiLine = True
    TabHeight = 28
    TabOrder = 0
    OnChange = sPageControl1Change
    ActiveIsBold = True
    ShowCloseBtns = True
    OnCloseBtnClick = sPageControl1CloseBtnClick
    object sTabSheet1: TsTabSheet
      Caption = '1'
      ImageIndex = 1
      SkinData.CustomColor = False
      SkinData.CustomFont = False
      SkinData.SkinSection = 'TABSHEET'
      UseCloseBtn = False
      object sPageControl2: TsPageControl
        Tag = 2
        Left = 60
        Top = 28
        Width = 301
        Height = 189
        ActivePage = sTabSheet8
        HotTrack = True
        Images = MainForm.ImageList24
        MultiLine = True
        TabHeight = 100
        TabOrder = 0
        TabPosition = tpLeft
        TabWidth = 27
        AccessibleDisabledPages = False
        ActiveIsBold = True
        RotateCaptions = True
        ShowCloseBtns = True
        ShowFocus = False
        TabAlignment = taLeftJustify
        object sTabSheet8: TsTabSheet
          Caption = 'Blue'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          SkinData.CustomColor = False
          SkinData.CustomFont = True
          DesignSize = (
            193
            181)
          object sPanel1: TsPanel
            Tag = 10
            Left = 52
            Top = 62
            Width = 89
            Height = 57
            Anchors = []
            BevelOuter = bvNone
            Color = clBlue
            TabOrder = 0
            SkinData.CustomColor = True
          end
        end
        object sTabSheet9: TsTabSheet
          Caption = 'Red'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ImageIndex = 1
          ParentFont = False
          SkinData.CustomColor = False
          SkinData.CustomFont = True
          DesignSize = (
            193
            181)
          object sPanel2: TsPanel
            Left = 52
            Top = 62
            Width = 89
            Height = 57
            Anchors = []
            BevelOuter = bvNone
            Color = clRed
            TabOrder = 0
            SkinData.CustomColor = True
          end
        end
        object sTabSheet10: TsTabSheet
          Caption = 'Green'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ImageIndex = 4
          ParentFont = False
          SkinData.CustomColor = False
          SkinData.CustomFont = True
          DesignSize = (
            193
            181)
          object sPanel3: TsPanel
            Left = 52
            Top = 62
            Width = 89
            Height = 57
            Anchors = []
            BevelOuter = bvNone
            Color = clGreen
            TabOrder = 0
            SkinData.CustomColor = True
          end
        end
        object sTabSheet11: TsTabSheet
          Caption = 'Maroon'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ImageIndex = 5
          ParentFont = False
          SkinData.CustomColor = False
          SkinData.CustomFont = True
          DesignSize = (
            193
            181)
          object sPanel4: TsPanel
            Left = 52
            Top = 62
            Width = 89
            Height = 57
            Anchors = []
            BevelOuter = bvNone
            Color = clMaroon
            TabOrder = 0
            SkinData.CustomColor = True
          end
        end
      end
      object sCheckBox17: TsCheckBox
        Left = 48
        Top = 268
        Width = 95
        Height = 20
        Caption = 'RotateCaptions'
        Checked = True
        State = cbChecked
        TabOrder = 1
        OnClick = sCheckBox17Click
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object sCheckBox4: TsCheckBox
        Left = 48
        Top = 292
        Width = 108
        Height = 20
        Caption = 'Use the Font color'
        Checked = True
        State = cbChecked
        TabOrder = 2
        OnClick = sCheckBox4Click
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object sCheckBox5: TsCheckBox
        Left = 48
        Top = 316
        Width = 114
        Height = 20
        Caption = 'Active tab enlarged'
        Checked = True
        State = cbChecked
        TabOrder = 3
        OnClick = sCheckBox5Click
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object sCheckBox6: TsCheckBox
        Left = 48
        Top = 340
        Width = 83
        Height = 20
        Caption = 'Active is bold'
        Checked = True
        State = cbChecked
        TabOrder = 4
        OnClick = sCheckBox6Click
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object sRadioGroup1: TsRadioGroup
        Left = 438
        Top = 168
        Width = 140
        Height = 95
        Caption = 'Tab alignment'
        Enabled = False
        TabOrder = 5
        OnClick = sRadioGroup1Click
        SkinData.SkinSection = 'GROUPBOX'
        Checked = False
        ItemIndex = 0
        Items.Strings = (
          'taLeftJustify'
          'taRightJustify'
          'taCenter')
      end
      object sComboBox1: TsComboBox
        Left = 438
        Top = 22
        Width = 140
        Height = 21
        Alignment = taLeftJustify
        BoundLabel.Active = True
        BoundLabel.Caption = 'SkinSection'
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclTopLeft
        VerticalAlignment = taAlignTop
        Style = csDropDownList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 15
        ItemIndex = -1
        ParentFont = False
        TabOrder = 6
        OnChange = sComboBox1Change
        Items.Strings = (
          'TRANSPARENT'
          'PAGECONTROL'
          'PANEL'
          'PANEL_LOW'
          'GROUPBOX'
          'TOOLBAR'
          'BARPANEL')
      end
      object sCheckBox15: TsCheckBox
        Left = 48
        Top = 244
        Width = 115
        Height = 20
        Caption = 'Show Close buttons'
        Checked = True
        State = cbChecked
        TabOrder = 7
        OnClick = sCheckBox15Click
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object sCheckBox8: TsCheckBox
        Tag = 5
        Left = 212
        Top = 244
        Width = 58
        Height = 20
        Caption = 'Multiline'
        Checked = True
        Enabled = False
        State = cbChecked
        TabOrder = 8
        OnClick = sCheckBox8Change
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object sRadioGroup2: TsRadioGroup
        Tag = 5
        Left = 438
        Top = 48
        Width = 140
        Height = 112
        Caption = 'Tabs position'
        TabOrder = 9
        OnClick = sRadioGroup2Click
        SkinData.SkinSection = 'GROUPBOX'
        Checked = False
        ItemIndex = 2
        Items.Strings = (
          'tpTop'
          'tpBottom'
          'tpLeft'
          'tpRight')
      end
      object sRadioGroup3: TsRadioGroup
        Left = 438
        Top = 270
        Width = 140
        Height = 95
        Caption = 'Tab style'
        Enabled = False
        TabOrder = 10
        OnClick = sRadioGroup3Click
        SkinData.SkinSection = 'GROUPBOX'
        Checked = False
        ItemIndex = 0
        Items.Strings = (
          'tsTabs'
          'tsButtons'
          'tsFlatButtons')
      end
      object sGroupBox1: TsGroupBox
        Left = 204
        Top = 292
        Width = 201
        Height = 69
        Caption = 'Disable last 2 tabs'
        TabOrder = 11
        SkinData.SkinSection = 'GROUPBOX'
        CheckBoxVisible = True
        Checked = False
        OnCheckBoxChanged = sGroupBox1CheckBoxChanged
        object sCheckBox3: TsCheckBox
          Left = 32
          Top = 28
          Width = 143
          Height = 20
          Caption = 'Accessible disabled pages'
          TabOrder = 0
          OnClick = sCheckBox3Click
          ImgChecked = 0
          ImgUnchecked = 0
        end
      end
    end
    object sTabSheet3: TsTabSheet
      Caption = '2'
      SkinData.CustomColor = False
      SkinData.CustomFont = False
    end
    object sTabSheet4: TsTabSheet
      Caption = '3'
      ImageIndex = 3
      SkinData.CustomColor = False
      SkinData.CustomFont = False
    end
    object sTabSheet2: TsTabSheet
      Caption = '+'
      ImageIndex = -1
      TabType = ttButton
      TabSkin = 'SPEEDBUTTON'
      SkinData.CustomColor = False
      SkinData.CustomFont = False
      SkinData.SkinSection = 'TABSHEET'
      UseCloseBtn = False
      OnClickBtn = sTabSheet2ClickBtn
    end
  end
end
