inherited Frame_OuterEffects: TFrame_OuterEffects
  Width = 596
  Height = 412
  object sSpeedButton1: TsSpeedButton [0]
    Left = 352
    Top = 72
    Width = 113
    Height = 29
    Caption = 'sSpeedButton1'
    Flat = True
    SkinData.SkinSection = 'SPEEDBUTTON'
    SkinData.OuterEffects.Visibility = ovAlways
    ImageIndex = 11
    Images = MainForm.ImageList16
  end
  object sEdit1: TsEdit [1]
    Left = 48
    Top = 32
    Width = 130
    Height = 21
    TabOrder = 0
    Text = 'sEdit1'
    SkinData.SkinSection = 'EDIT'
    SkinData.OuterEffects.Visibility = ovAlways
  end
  object sDecimalSpinEdit1: TsDecimalSpinEdit [2]
    Left = 196
    Top = 32
    Width = 130
    Height = 21
    TabOrder = 1
    Text = '777,77'
    SkinData.SkinSection = 'EDIT'
    SkinData.OuterEffects.Visibility = ovAlways
    Increment = 1.000000000000000000
    Value = 777.770000000000000000
  end
  object sComboBox1: TsComboBox [3]
    Left = 345
    Top = 32
    Width = 130
    Height = 21
    Alignment = taLeftJustify
    SkinData.SkinSection = 'COMBOBOX'
    SkinData.OuterEffects.Visibility = ovAlways
    VerticalAlignment = taAlignTop
    ItemHeight = 15
    ItemIndex = -1
    TabOrder = 2
    Text = 'sComboBox1'
  end
  object sBitBtn1: TsBitBtn [4]
    Left = 48
    Top = 72
    Width = 86
    Height = 29
    Caption = 'sBitBtn1'
    TabOrder = 3
    ImageIndex = 0
    Images = MainForm.ImageList16
    Reflected = True
    SkinData.OuterEffects.Visibility = ovAlways
  end
  object sBitBtn2: TsBitBtn [5]
    Left = 144
    Top = 72
    Width = 86
    Height = 29
    Caption = 'sBitBtn2'
    TabOrder = 4
    ImageIndex = 1
    Images = MainForm.ImageList16
    Reflected = True
    SkinData.OuterEffects.Visibility = ovAlways
  end
  object sBitBtn3: TsBitBtn [6]
    Left = 240
    Top = 72
    Width = 86
    Height = 29
    Caption = 'sBitBtn3'
    TabOrder = 5
    ImageIndex = 2
    Images = MainForm.ImageList16
    Reflected = True
    SkinData.OuterEffects.Visibility = ovAlways
  end
  object sPanel1: TsPanel [7]
    Left = 48
    Top = 328
    Width = 425
    Height = 57
    TabOrder = 6
    SkinData.SkinSection = 'PANEL'
    SkinData.OuterEffects.Visibility = ovAlways
    object sGauge1: TsGauge
      Left = 225
      Top = 16
      Width = 180
      Height = 25
      SkinData.SkinSection = 'GAUGE'
      SkinData.OuterEffects.Visibility = ovAlways
      ForeColor = clBlack
      MaxValue = 100
      Progress = 47
      Suffix = '%'
    end
    object sProgressBar1: TsProgressBar
      Left = 16
      Top = 16
      Width = 180
      Height = 25
      Position = 47
      TabOrder = 0
      SkinData.SkinSection = 'GAUGE'
      SkinData.OuterEffects.Visibility = ovAlways
    end
  end
  object sPanel2: TsPanel [8]
    Left = 48
    Top = 204
    Width = 425
    Height = 113
    BevelOuter = bvLowered
    TabOrder = 7
    SkinData.SkinSection = 'PANEL_LOW'
    SkinData.OuterEffects.Visibility = ovAlways
    object sColorBox1: TsColorBox
      Left = 18
      Top = 16
      Width = 129
      Height = 22
      SkinData.OuterEffects.Visibility = ovAlways
      DropDownCount = 8
      ItemHeight = 16
      TabOrder = 0
    end
    object sCalcEdit1: TsCalcEdit
      Left = 18
      Top = 48
      Width = 129
      Height = 21
      AutoSize = False
      TabOrder = 1
      SkinData.SkinSection = 'EDIT'
      SkinData.OuterEffects.Visibility = ovAlways
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
    end
    object sDirectoryEdit1: TsDirectoryEdit
      Left = 18
      Top = 80
      Width = 129
      Height = 21
      AutoSize = False
      MaxLength = 255
      TabOrder = 2
      CheckOnExit = True
      SkinData.SkinSection = 'EDIT'
      SkinData.OuterEffects.Visibility = ovAlways
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
      Root = 'rfDesktop'
    end
    object sListBox1: TsListBox
      Left = 160
      Top = 16
      Width = 137
      Height = 85
      ItemHeight = 16
      Items.Strings = (
        'Item 1'
        'Item 2'
        'Item 3'
        'Item 4'
        'Item 5'
        'Item 6'
        'Item 7'
        'Item 8'
        'Item 9'
        'Item 10'
        'Item 11'
        'Item 12'
        'Item 13'
        'Item 14'
        'Item 15'
        'Item 16')
      TabOrder = 3
      SkinData.SkinSection = 'ALPHAEDIT'
      SkinData.OuterEffects.Visibility = ovAlways
    end
    object sBitBtn6: TsBitBtn
      Left = 313
      Top = 17
      Width = 97
      Height = 34
      Caption = 'sBitBtn6'
      TabOrder = 4
      ImageIndex = 1
      Images = MainForm.ImageList16
      Reflected = True
      SkinData.OuterEffects.Visibility = ovAlways
    end
    object sBitBtn7: TsBitBtn
      Left = 313
      Top = 63
      Width = 97
      Height = 34
      Caption = 'sBitBtn7'
      TabOrder = 5
      ImageIndex = 2
      Images = MainForm.ImageList16
      Reflected = True
      SkinData.OuterEffects.Visibility = ovAlways
    end
  end
  object sBitBtn8: TsBitBtn [9]
    Left = 504
    Top = 40
    Width = 64
    Height = 64
    Caption = 'sBitBtn8'
    TabOrder = 8
    ImageIndex = 0
    Images = MainForm.ImageList24
    Reflected = True
    ShowCaption = False
    SkinData.SkinSection = 'BUTTON_HUGE'
    SkinData.OuterEffects.Visibility = ovAlways
  end
  object sBitBtn9: TsBitBtn [10]
    Left = 504
    Top = 128
    Width = 64
    Height = 64
    Caption = 'sBitBtn9'
    TabOrder = 9
    ImageIndex = 1
    Images = MainForm.ImageList24
    Reflected = True
    ShowCaption = False
    SkinData.SkinSection = 'BUTTON_HUGE'
    SkinData.OuterEffects.Visibility = ovAlways
  end
  object sBitBtn10: TsBitBtn [11]
    Left = 504
    Top = 216
    Width = 64
    Height = 64
    Caption = 'sBitBtn10'
    TabOrder = 10
    ImageIndex = 2
    Images = MainForm.ImageList24
    Reflected = True
    ShowCaption = False
    SkinData.SkinSection = 'BUTTON_HUGE'
    SkinData.OuterEffects.Visibility = ovAlways
  end
  object sBitBtn11: TsBitBtn [12]
    Left = 504
    Top = 304
    Width = 64
    Height = 64
    Caption = 'sBitBtn10'
    TabOrder = 11
    ImageIndex = 5
    Images = MainForm.ImageList24
    Reflected = True
    ShowCaption = False
    SkinData.SkinSection = 'BUTTON_HUGE'
    SkinData.OuterEffects.Visibility = ovAlways
  end
  object sGroupBox1: TsGroupBox [13]
    Left = 48
    Top = 118
    Width = 425
    Height = 71
    Caption = 'Controlling of outer effects'
    TabOrder = 12
    SkinData.SkinSection = 'GROUPBOX'
    Checked = False
    object sCheckBox1: TsCheckBox
      Left = 248
      Top = 28
      Width = 119
      Height = 20
      Caption = 'Use on this frame'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 0
      OnClick = sCheckBox1Click
      Margin = 8
      SkinData.OuterEffects.Visibility = ovAlways
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object sCheckBox2: TsCheckBox
      Left = 64
      Top = 28
      Width = 128
      Height = 20
      Caption = 'Allow in application'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      OnClick = sCheckBox2Click
      Margin = 8
      SkinData.OuterEffects.Visibility = ovAlways
      ImgChecked = 0
      ImgUnchecked = 0
    end
  end
  inherited sFrameAdapter1: TsFrameAdapter
    Left = 504
    Top = 24
  end
end
