inherited Frame_Hints: TFrame_Hints
  Width = 613
  Height = 394
  object sLabelFX1: TsLabelFX [0]
    Left = 47
    Top = 31
    Width = 371
    Height = 83
    Alignment = taCenter
    AutoSize = False
    Caption = 
      'The TsAlphaHints component redefines properties of standard hint' +
      's in application and adds some additional possibilities.'#13#13'For a ' +
      'component using it is enough to place it to the main form of app' +
      'lication, that'#39's all.'
    ParentFont = False
    WordWrap = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    Angle = 0
    Shadow.OffsetKeeper.LeftTop = -3
    Shadow.OffsetKeeper.RightBottom = 5
  end
  object sRadioGroup1: TsRadioGroup [1]
    Left = 64
    Top = 128
    Width = 353
    Height = 65
    Caption = 'Show hints'
    ParentBackground = False
    TabOrder = 0
    OnClick = sRadioGroup1Click
    SkinData.SkinSection = 'GROUPBOX'
    CheckBoxVisible = True
    Checked = True
    OnCheckBoxChanged = sRadioGroup1CheckBoxChanged
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Skinned'
      'Templated'
      'Standard')
  end
  object sListBox1: TsListBox [2]
    Left = 64
    Top = 200
    Width = 353
    Height = 97
    Columns = 3
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ItemHeight = 18
    ParentFont = False
    TabOrder = 1
    OnClick = sListBox1Click
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    SkinData.SkinSection = 'EDIT'
  end
  object sBitBtn1: TsBitBtn [3]
    Left = 64
    Top = 300
    Width = 353
    Height = 29
    Caption = 'Open templates editor'
    TabOrder = 2
    OnClick = sBitBtn1Click
    ShowFocus = False
  end
  object sCheckBox1: TsCheckBox [4]
    Left = 68
    Top = 348
    Width = 65
    Height = 20
    Caption = 'Animated'
    Checked = True
    State = cbChecked
    TabOrder = 3
    OnClick = sCheckBox1Click
    ImgChecked = 0
    ImgUnchecked = 0
  end
  object sCheckBox2: TsCheckBox [5]
    Left = 188
    Top = 348
    Width = 90
    Height = 20
    Caption = 'AutoAlignment'
    Checked = True
    State = cbChecked
    TabOrder = 4
    OnClick = sCheckBox2Click
    ImgChecked = 0
    ImgUnchecked = 0
  end
  object sCheckBox3: TsCheckBox [6]
    Left = 332
    Top = 348
    Width = 67
    Height = 20
    Caption = 'Use HTML'
    Checked = True
    State = cbChecked
    TabOrder = 5
    OnClick = sCheckBox3Click
    ImgChecked = 0
    ImgUnchecked = 0
  end
  inherited sFrameAdapter1: TsFrameAdapter
    Left = 488
  end
end
