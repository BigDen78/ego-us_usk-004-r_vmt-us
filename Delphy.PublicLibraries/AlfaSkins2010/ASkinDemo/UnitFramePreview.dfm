inherited Frame_Preview: TFrame_Preview
  Width = 618
  Height = 419
  object sStickyLabel1: TsStickyLabel [0]
    Left = 16
    Top = 328
    Width = 129
    Height = 13
    Alignment = taCenter
    AutoSize = False
    Caption = 'HUE Offset:   '
    ParentFont = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    AlignTo = altTop
    AttachTo = sTrackBar1
    Gap = 0
  end
  object sStickyLabel3: TsStickyLabel [1]
    Left = 159
    Top = 328
    Width = 130
    Height = 13
    Alignment = taCenter
    AutoSize = False
    Caption = 'Saturation:   '
    ParentFont = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    AlignTo = altTop
    AttachTo = sTrackBar2
    Gap = 0
  end
  object sLabel2: TsLabel [2]
    Left = 250
    Top = 328
    Width = 6
    Height = 13
    Caption = '0'
    ParentFont = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object sLabel1: TsLabel [3]
    Left = 110
    Top = 328
    Width = 6
    Height = 13
    Caption = '0'
    ParentFont = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object sStickyLabel2: TsStickyLabel [4]
    Left = 303
    Top = 326
    Width = 130
    Height = 13
    Alignment = taCenter
    AutoSize = False
    Caption = 'Brightness:    '
    AlignTo = altTop
    AttachTo = sTrackBar3
  end
  object sLabel3: TsLabel [5]
    Left = 393
    Top = 326
    Width = 6
    Height = 13
    Caption = '0'
    ParentFont = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object sBitBtn1: TsBitBtn [6]
    Left = 240
    Top = 374
    Width = 196
    Height = 38
    Caption = 'Apply this skin'
    Enabled = False
    TabOrder = 6
    OnClick = sBitBtn1Click
    Margin = 16
    Spacing = 12
    Blend = 25
    Grayed = True
    ImageIndex = 0
    Images = sAlphaImageList1
    Reflected = True
    SkinData.SkinSection = 'SPEEDBUTTON'
  end
  object sListBox1: TsListBox [7]
    Left = 456
    Top = 48
    Width = 128
    Height = 361
    ItemHeight = 16
    TabOrder = 0
    OnClick = sListBox1Click
    SkinData.SkinSection = 'EDIT'
  end
  object sTrackBar3: TsTrackBar [8]
    Left = 303
    Top = 341
    Width = 129
    Height = 24
    Max = 100
    Min = -100
    PageSize = 24
    Frequency = 20
    TabOrder = 7
    TickStyle = tsNone
    OnChange = sTrackBar3Change
    SkinData.SkinSection = 'TRACKBAR'
    BarOffsetV = 0
    BarOffsetH = 0
  end
  object Panel_Preview: TsPanel [9]
    Left = 16
    Top = 48
    Width = 425
    Height = 265
    BevelInner = bvLowered
    BorderWidth = 4
    Caption = 'Preview area'
    Enabled = False
    TabOrder = 5
    SkinData.SkinSection = 'GROUPBOX'
  end
  object sBitBtn2: TsBitBtn [10]
    Left = 24
    Top = 374
    Width = 196
    Height = 38
    Caption = 'Test in new form'
    Enabled = False
    TabOrder = 4
    OnClick = sBitBtn2Click
    Margin = 16
    Spacing = 12
    Blend = 25
    Grayed = True
    ImageIndex = 1
    Images = sAlphaImageList1
    Reflected = True
    SkinData.SkinSection = 'SPEEDBUTTON'
  end
  object sTrackBar2: TsTrackBar [11]
    Left = 159
    Top = 341
    Width = 129
    Height = 24
    Max = 100
    Min = -100
    PageSize = 24
    Frequency = 20
    TabOrder = 3
    TickStyle = tsNone
    OnChange = sTrackBar2Change
    SkinData.SkinSection = 'TRACKBAR'
    BarOffsetV = 0
    BarOffsetH = 0
  end
  object sDirectoryEdit1: TsDirectoryEdit [12]
    Left = 16
    Top = 16
    Width = 568
    Height = 21
    AutoSize = False
    MaxLength = 255
    TabOrder = 1
    OnChange = sDirectoryEdit1Change
    CheckOnExit = True
    BoundLabel.Active = True
    BoundLabel.Caption = 'Path to external skins:'
    BoundLabel.Layout = sclTopLeft
    SkinData.SkinSection = 'EDIT'
    GlyphMode.Blend = 0
    GlyphMode.Grayed = False
    Root = 'rfDesktop'
  end
  object sTrackBar1: TsTrackBar [13]
    Left = 16
    Top = 341
    Width = 129
    Height = 24
    Max = 360
    PageSize = 24
    Frequency = 36
    TabOrder = 2
    TickStyle = tsNone
    OnChange = sTrackBar1Change
    SkinData.SkinSection = 'TRACKBAR'
    BarOffsetV = 0
    BarOffsetH = 0
  end
  inherited sFrameAdapter1: TsFrameAdapter
    Left = 664
    Top = 40
  end
  object sAlphaImageList1: TsAlphaImageList
    Height = 24
    Width = 24
    Items = <
      item
        ImageFormat = ifPNG
        ImgData = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F80000001974455874536F6674776172650041646F626520496D616765526561
          647971C9653C0000036669545874584D4C3A636F6D2E61646F62652E786D7000
          000000003C3F787061636B657420626567696E3D22EFBBBF222069643D225735
          4D304D7043656869487A7265537A4E54637A6B633964223F3E203C783A786D70
          6D65746120786D6C6E733A783D2261646F62653A6E733A6D6574612F2220783A
          786D70746B3D2241646F626520584D5020436F726520352E332D633031312036
          362E3134353636312C20323031322F30322F30362D31343A35363A3237202020
          2020202020223E203C7264663A52444620786D6C6E733A7264663D2268747470
          3A2F2F7777772E77332E6F72672F313939392F30322F32322D7264662D73796E
          7461782D6E7323223E203C7264663A4465736372697074696F6E207264663A61
          626F75743D222220786D6C6E733A786D704D4D3D22687474703A2F2F6E732E61
          646F62652E636F6D2F7861702F312E302F6D6D2F2220786D6C6E733A73745265
          663D22687474703A2F2F6E732E61646F62652E636F6D2F7861702F312E302F73
          547970652F5265736F75726365526566232220786D6C6E733A786D703D226874
          74703A2F2F6E732E61646F62652E636F6D2F7861702F312E302F2220786D704D
          4D3A4F726967696E616C446F63756D656E7449443D22786D702E6469643A4342
          4231443844334642383045323131413132463942463739424136434431432220
          786D704D4D3A446F63756D656E7449443D22786D702E6469643A314143314136
          33374144383431314532384435344245353538413738454431352220786D704D
          4D3A496E7374616E636549443D22786D702E6969643A31414331413633364144
          383431314532384435344245353538413738454431352220786D703A43726561
          746F72546F6F6C3D2241646F62652050686F746F73686F702043533620285769
          6E646F777329223E203C786D704D4D3A4465726976656446726F6D2073745265
          663A696E7374616E636549443D22786D702E6969643A32464532333245383833
          41444532313142424132443332383035463141394338222073745265663A646F
          63756D656E7449443D22786D702E6469643A4342423144384433464238304532
          313141313246394246373942413643443143222F3E203C2F7264663A44657363
          72697074696F6E3E203C2F7264663A5244463E203C2F783A786D706D6574613E
          203C3F787061636B657420656E643D2272223F3E47640AB40000022D49444154
          78DAB455BD4B1C41147F7B37EB8969847411247F492A416CED43B04B11FF8E74
          A90221681302B6168A96629D2204420A1121E40844E3E9B1BAEEECCCBCF1BDD9
          99BBBDCD656F2F5E06DEBEB7F3F1FBBDAF9D8D764F2FDF68C45739DA581A0BD2
          20DC69849C748E00062DF0B0852A6C08735EFB790CEB85A168DF5BA10C6E3E5B
          5A140C9C690BA932901151A6102481230E01060481CD8E0287093F1DEF1C7DDE
          140422D8E31F89827E66E082E43A43B8960692DC80360C30EA69C0E7E06C8524
          44B7B2BC40D1A368317842B9B825D1FE80A607A7C6708AE80003B1F03B13B256
          BC4673EF579FB83D5BAC29DCEDD52550863381A41144AAD96303E77706DA5154
          803210216AA7C77B8858581B875D47FEE2A0EB9C7BBEDF75EB2979AB89484832
          7EF573F8D99344C0001164949A24D570935B4714723E2CA21DD8E5340DD365E1
          EA519B223020B85BF64EFAF061FD297985A345AC19E53DD5FD711CC3DAC753E7
          9CF89EE434D58669C6A03D6B48B886AE065F2F536825737FDD382D78B0B9D02E
          02FAC8E0EC229B083E09B06A33B8A4061275404D89C69DF94D4DD2EBDD8C1234
          391C512B57D7ABD2E974E0FC564326F59F114CCA73E8B471EB41CAA3350D785D
          EE1B11CC02BC4A20A62D621D38A7AF96A0698FD781FF530455BB7CA554C11BD5
          E021E013BBE8A1E0589722FE40663D9840B37EFCFA0BFC87A1C5B7E34FEFA02D
          5EF2353E6370054A6EF1C5324FC2F775C7EBB9D27BEC45786979B1FE07A7BD28
          92BCA465D0F7020C0044E78F9C35C7C7AC0000000049454E44AE426082}
      end
      item
        ImageFormat = ifPNG
        ImgData = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F8000003AA49444154785EED967D4C5B6514C6CFBD524A81E26DA5A5B4030A58
          0371600204C4485118A595647E0D9665B205E7FC4E20C6B9641B0871336E0C26
          CAA863C3254A35B8E91C53AA58F65149A8B0C896294BA0B71FD84A53192A58DD
          84EE31D1A490B8666E737F98F84BCEFBDF799E9C9393735E0600DD4C58BAC9FC
          6F1022D6C8E42535C84F25BD2655FDEB06D10798DCF4E98CAF0ED71CD5A6B29A
          3ED1338C72D1E0C6C5F3D27D9AE1B655EDECE1E11EDA6CD89ACD04D8CF85957F
          9944D00D10D3C9E4A4FAD26D6D551D6CEFB92374CE7F863A46F6D2F6753B9637
          B637998506A6F8BA0D623A985CF585B4E137D718D9DEF31FD23781B334343744
          97954466D73112B3DC2DBFCEFB88008442FEBE901337093A6526610E000A1722
          23E566BCA20E5A9C9FA1766023561CB91BE2FD04E97B1148698D87AA32D19E58
          295501A0C5A47D2496360B4F8EF86CC8EC507BA20FD25D5712E7DEA5BCCC9DA9
          418BC78C3ADB53307CA185E20311D47D1CD2DAE4882C6327A20CA404402183F8
          9648B17A6782E5C4643F861C5678E73C48DC237173DD4CD65271C521514E56AB
          2678DCD78F4DA3CFE3D1C17268CC72645B9391F95612043A9617EA69D9D21C56
          D4C0C46916B27A9B1F6C2D4590C8F5BB8BBE9C3A41C31BCF24C7CD4AFB541FC5
          2E2722521F93E424F85246DAAB8DECC08C99BEC777E4C004C91432A2F148B2F7
          7A1D6CEC65ED45333CB414E14B9456B6EBBEE9B59687D130BA09879C2674D98D
          E8E28D70079C50BE2AF1DED39F5D5AF176D9C2A9190B1AED9BB1E17C158A46B3
          A19B28407ECF9D10AC62F998174875A596FEF914ECCA2AAFD8ADFB21FF930CD4
          7C5D89FDCE7674BADE8071720F9C011EBA7D25B0CD0D62FBE4163CEBAC867EBC
          108F784B50D4978BA8EA08FBAD7B1955B88108ADEB9417E586A8C8E86E4E1725
          5D264FA202F1BD248E88A320BB40857145649935931F3E7205798A1689C8CFFF
          44435DA71D9C5E503CF9D0ACE7AABBC8BDDB6FF606DD6BDDC7A72E04E6036423
          2BB9590705238364BD34403F0B67C82F9C22D96DF1F4E374804EF79C75681E53
          6A43E261F85B49B12D549EF032375D657F00EB7E5989BAB9C7B1EDB75A3C7971
          35EA508395FC0AC8B64A78DD587E98B68469D152E4EF44E90493E26EFD132532
          4E21268048C270F4AD6B9C060F0EF3C5CFE5169BE49F7AE91F10D6F9F68FE5FA
          E466C5F496993A1CC0EB78DABD1E7734A5F3F5F3B5A139BFDE0A42E40F66E867
          47164CF797164B4F1EB53A9AEAB769AB68BD97AE81AB1EFDF2B1C28A3193BB7E
          C38E35AB1BA9C54DD7C87FFF57F107D053633346FE8A2D0000000049454E44AE
          426082}
      end>
    Left = 48
    Top = 80
    Bitmap = {}
  end
  object SkinManager_Preview: TsSkinManager
    Effects.AllowGlowing = False
    IsDefault = False
    InternalSkins = <>
    MenuSupport.IcoLineSkin = 'ICOLINE'
    MenuSupport.ExtraLineFont.Charset = DEFAULT_CHARSET
    MenuSupport.ExtraLineFont.Color = clWindowText
    MenuSupport.ExtraLineFont.Height = -11
    MenuSupport.ExtraLineFont.Name = 'Tahoma'
    MenuSupport.ExtraLineFont.Style = []
    Options.NativeBordersMaximized = True
    SkinDirectory = 'c:\Skins'
    SkinInfo = 'N/A'
    ThirdParty.ThirdEdits = ' '
    ThirdParty.ThirdButtons = 'TButton'
    ThirdParty.ThirdBitBtns = ' '
    ThirdParty.ThirdCheckBoxes = ' '
    ThirdParty.ThirdGroupBoxes = ' '
    ThirdParty.ThirdListViews = ' '
    ThirdParty.ThirdPanels = ' '
    ThirdParty.ThirdGrids = ' '
    ThirdParty.ThirdTreeViews = ' '
    ThirdParty.ThirdComboBoxes = ' '
    ThirdParty.ThirdWWEdits = ' '
    ThirdParty.ThirdVirtualTrees = ' '
    ThirdParty.ThirdGridEh = ' '
    ThirdParty.ThirdPageControl = ' '
    ThirdParty.ThirdTabControl = ' '
    ThirdParty.ThirdToolBar = ' '
    ThirdParty.ThirdStatusBar = ' '
    ThirdParty.ThirdSpeedButton = ' '
    ThirdParty.ThirdScrollControl = ' '
    ThirdParty.ThirdUpDown = ' '
    ThirdParty.ThirdScrollBar = ' '
    ThirdParty.ThirdStaticText = ' '
    ThirdParty.ThirdNativePaint = ' '
    Left = 80
    Top = 80
  end
end
