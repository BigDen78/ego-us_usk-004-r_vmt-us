object BarPanel_Visible: TBarPanel_Visible
  Tag = 5
  Left = 0
  Top = 0
  Width = 202
  Height = 288
  TabOrder = 0
  DesignSize = (
    202
    288)
  object sTreeView1: TsTreeViewEx
    Left = 4
    Top = 6
    Width = 191
    Height = 275
    Anchors = [akLeft, akTop, akRight]
    BorderStyle = bsNone
    HideSelection = False
    HotTrack = True
    Indent = 19
    ParentColor = True
    TabOrder = 0
    OnChange = sTreeView1Change
    SkinData.SkinSection = 'TRANSPARENT'
  end
  object sFrameAdapter1: TsFrameAdapter
    SkinData.SkinSection = 'BARPANEL'
    Left = 107
    Top = 70
  end
end
