unit UnitFramePreview;

interface


uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, Mask,
  Buttons, ComCtrls, ExtCtrls, ImgList, 

  sFrameAdapter, StdCtrls, sMaskEdit, sCustomComboEdit, sToolEdit, sListBox, sBitBtn, sLabel,
  sTrackBar, sPanel, acAlphaImageList, acSkinPreview, sSkinManager,
  
  UnitFrameCustom;


type
  TFrame_Preview = class(TCustomInfoFrame)
    sListBox1: TsListBox;
    sDirectoryEdit1: TsDirectoryEdit;
    sTrackBar1: TsTrackBar;
    sStickyLabel1: TsStickyLabel;
    sStickyLabel3: TsStickyLabel;
    sTrackBar2: TsTrackBar;
    sLabel2: TsLabel;
    sLabel1: TsLabel;
    sBitBtn2: TsBitBtn;
    Panel_Preview: TsPanel;
    sBitBtn1: TsBitBtn;
    sAlphaImageList1: TsAlphaImageList;
    SkinManager_Preview: TsSkinManager;
    sTrackBar3: TsTrackBar;
    sStickyLabel2: TsStickyLabel;
    sLabel3: TsLabel;
    procedure sDirectoryEdit1Change(Sender: TObject);
    procedure sListBox1Click(Sender: TObject);
    procedure sTrackBar1Change(Sender: TObject);
    procedure sTrackBar2Change(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure sTrackBar3Change(Sender: TObject);
  public
    acPreviewForm: TFormSkinPreview;
    procedure AfterCreation; override;
    destructor Destroy; override;
  end;


implementation

uses
  MainUnit,
  sSkinProvider;


{$R *.dfm}

procedure TFrame_Preview.AfterCreation;
begin
  sDirectoryEdit1.Text := MainForm.sSkinManager1.SkinDirectory;
  sDirectoryEdit1Change(nil);
end;


procedure TFrame_Preview.sDirectoryEdit1Change(Sender: TObject);
begin
  SkinManager_Preview.SkinDirectory := sDirectoryEdit1.Text;
  SkinManager_Preview.GetExternalSkinNames(sListBox1.Items);
  sListBox1Click(nil);
end;


procedure TFrame_Preview.sListBox1Click(Sender: TObject);
begin
  sBitBtn1.Enabled := sListBox1.ItemIndex >= 0;
  if sBitBtn1.Enabled then begin
    if acPreviewForm = nil then begin
      acPreviewForm := TFormSkinPreview.Create(nil); // Form will be freed automatically together with this frame
      acPreviewForm.Align := alClient;
      acPreviewForm.Parent := Panel_Preview;
      acPreviewForm.Name := 'FormSkinPreview';
    end;
    acPreviewForm.PreviewManager.SkinDirectory := sDirectoryEdit1.Text;
    acPreviewForm.PreviewManager.SkinName := sListBox1.Items[sListBox1.ItemIndex];
    acPreviewForm.PreviewManager.Active := True;
    acPreviewForm.Visible := True;

    sTrackBar3.Min := acPreviewForm.PreviewManager.CommonSkinData.BrightMin;
    sTrackBar3.Max := acPreviewForm.PreviewManager.CommonSkinData.BrightMax;
    sTrackBar3.Position := 0;
  end
  else begin
    sBitBtn1.Enabled := False;
    sBitBtn2.Enabled := False;
    if acPreviewForm <> nil then
      acPreviewForm.Close;
  end;
  sBitBtn2.Enabled := sBitBtn1.Enabled;
  sTrackBar1.Enabled := sBitBtn1.Enabled;
  sTrackBar2.Enabled := sBitBtn1.Enabled;
  sTrackBar3.Enabled := sBitBtn1.Enabled;    
end;


procedure TFrame_Preview.sTrackBar1Change(Sender: TObject);
begin
  acPreviewForm.PreviewManager.HueOffset := sTrackBar1.Position;
  sLabel1.Caption := IntToStr(sTrackBar1.Position);
end;


procedure TFrame_Preview.sTrackBar2Change(Sender: TObject);
begin
  acPreviewForm.PreviewManager.Saturation := sTrackBar2.Position;
  sLabel2.Caption := IntToStr(sTrackBar2.Position);
end;


procedure TFrame_Preview.sTrackBar3Change(Sender: TObject);
begin
  acPreviewForm.PreviewManager.Brightness := sTrackBar3.Position;
  sLabel3.Caption := IntToStr(sTrackBar3.Position);
end;


procedure TFrame_Preview.sBitBtn2Click(Sender: TObject);
var
  NewPrevForm: TForm;
begin
  NewPrevForm := TForm.Create(nil);
  NewPrevForm.Caption := 'Preview form';
  NewPrevForm.Position := poScreenCenter;
  acPreviewForm.PreviewManager.ExtendedBorders := True;
  with TsSkinProvider.Create(NewPrevForm) do
    SkinData.SkinManager := acPreviewForm.PreviewManager;

  NewPrevForm.ShowModal;
  NewPrevForm.Free
end;


procedure TFrame_Preview.sBitBtn1Click(Sender: TObject);
begin
  Mainform.sSkinManager1.BeginUpdate;
  Mainform.sSkinManager1.SkinDirectory := acPreviewForm.PreviewManager.SkinDirectory;
  Mainform.sSkinManager1.SkinName      := acPreviewForm.PreviewManager.SkinName;
  Mainform.sSkinManager1.HueOffset     := acPreviewForm.PreviewManager.HueOffset;
  Mainform.sSkinManager1.Saturation    := acPreviewForm.PreviewManager.Saturation;
  Mainform.sSkinManager1.Brightness    := acPreviewForm.PreviewManager.Brightness;
  Mainform.sSkinManager1.EndUpdate(True);
end;


destructor TFrame_Preview.Destroy;
begin
  if acPreviewForm <> nil then begin
    acPreviewForm.Visible := False;
    acPreviewForm.Free;
  end;
  inherited;
end;  

end.
