inherited Frame_ScrollBars: TFrame_ScrollBars
  Width = 606
  Height = 412
  object sTreeView1: TsTreeView [0]
    Tag = 5
    Left = 30
    Top = 20
    Width = 264
    Height = 140
    HideSelection = False
    Indent = 19
    TabOrder = 0
    Items.Data = {
      090000001F0000000300000000000000FFFFFFFFFFFFFFFF0000000003000000
      064974656D2031200000000300000000000000FFFFFFFFFFFFFFFF0000000000
      000000074974656D203131200000000300000000000000FFFFFFFFFFFFFFFF00
      00000000000000074974656D203132200000000300000000000000FFFFFFFFFF
      FFFFFF0000000004000000074974656D203133210000000300000000000000FF
      FFFFFFFFFFFFFF0000000000000000084974656D203133312100000003000000
      00000000FFFFFFFFFFFFFFFF0000000000000000084974656D20313332210000
      000300000000000000FFFFFFFFFFFFFFFF0000000000000000084974656D2031
      3333210000000300000000000000FFFFFFFFFFFFFFFF00000000040000000849
      74656D20313334220000000300000000000000FFFFFFFFFFFFFFFF0000000000
      000000094974656D2031333431220000000300000000000000FFFFFFFFFFFFFF
      FF0000000000000000094974656D2031333432220000000300000000000000FF
      FFFFFFFFFFFFFF0000000000000000094974656D203133343322000000030000
      0000000000FFFFFFFFFFFFFFFF0000000004000000094974656D203133343423
      0000000300000000000000FFFFFFFFFFFFFFFF00000000000000000A4974656D
      203133343431230000000300000000000000FFFFFFFFFFFFFFFF000000000000
      00000A4974656D203133343432230000000300000000000000FFFFFFFFFFFFFF
      FF00000000000000000A4974656D203133343433230000000300000000000000
      FFFFFFFFFFFFFFFF00000000010000000A4974656D2031333434344900000000
      00000000000000FFFFFFFFFFFFFFFF0000000000000000304C6F6E6720737562
      6974656D20666F722073686F77696E67206F6620686F72697A6F6E74616C2073
      63726F6C6C6261721F0000000300000000000000FFFFFFFFFFFFFFFF00000000
      00000000064974656D20321F0000000300000000000000FFFFFFFFFFFFFFFF00
      00000004000000064974656D2033200000000300000000000000FFFFFFFFFFFF
      FFFF0000000000000000074974656D203331200000000300000000000000FFFF
      FFFFFFFFFFFF0000000000000000074974656D20333220000000030000000000
      0000FFFFFFFFFFFFFFFF0000000000000000074974656D203333200000000300
      000000000000FFFFFFFFFFFFFFFF0000000000000000074974656D2033341F00
      00000300000000000000FFFFFFFFFFFFFFFF0000000000000000064974656D20
      341F0000000300000000000000FFFFFFFFFFFFFFFF0000000000000000064974
      656D20351F0000000300000000000000FFFFFFFFFFFFFFFF0000000000000000
      064974656D20361F0000000300000000000000FFFFFFFFFFFFFFFF0000000000
      000000064974656D20371F0000000300000000000000FFFFFFFFFFFFFFFF0000
      000000000000064974656D20381F0000000300000000000000FFFFFFFFFFFFFF
      FF0000000000000000064974656D2039}
    BoundLabel.Layout = sclTopLeft
    SkinData.SkinSection = 'EDIT'
  end
  object sGroupBox1: TsGroupBox [1]
    Left = 30
    Top = 176
    Width = 264
    Height = 213
    Caption = 'Scrolls in all application'
    TabOrder = 1
    CaptionLayout = clTopCenter
    SkinData.SkinSection = 'PANEL_LOW'
    Checked = False
    object sGroupBox2: TsGroupBox
      Left = 16
      Top = 24
      Width = 233
      Height = 69
      Caption = 'Arrow buttons visible'
      TabOrder = 0
      CaptionLayout = clTopCenter
      SkinData.SkinSection = 'GROUPBOX'
      CheckBoxVisible = True
      Checked = True
      OnCheckBoxChanged = sGroupBox2CheckBoxChanged
      object sCheckBox3: TsCheckBox
        Left = 21
        Top = 30
        Width = 76
        Height = 20
        Caption = 'Default size'
        Checked = True
        State = cbChecked
        TabOrder = 0
        OnClick = sCheckBox3Click
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object sTrackBar2: TsTrackBar
        Left = 104
        Top = 28
        Width = 113
        Height = 27
        Enabled = False
        Max = 36
        Min = 12
        Position = 18
        TabOrder = 1
        TickStyle = tsNone
        OnChange = sTrackBar2Change
        SkinData.SkinSection = 'TRACKBAR'
        ShowProgress = True
        BarOffsetV = 0
        BarOffsetH = 0
      end
    end
    object sGroupBox3: TsGroupBox
      Left = 16
      Top = 108
      Width = 233
      Height = 89
      Caption = 'Default scroll width'
      TabOrder = 1
      CaptionLayout = clTopCenter
      SkinData.SkinSection = 'GROUPBOX'
      CheckBoxVisible = True
      Checked = False
      OnCheckBoxChanged = sGroupBox3CheckBoxChanged
      object sTrackBar1: TsTrackBar
        Left = 24
        Top = 35
        Width = 185
        Height = 27
        Max = 36
        Min = 12
        Position = 24
        TabOrder = 0
        TickStyle = tsNone
        OnChange = sTrackBar1Change
        SkinData.SkinSection = 'TRACKBAR'
        ShowProgress = True
        BarOffsetV = 0
        BarOffsetH = 0
      end
    end
  end
  object sTreeView2: TsTreeView [2]
    Tag = 5
    Left = 318
    Top = 20
    Width = 264
    Height = 140
    HideSelection = False
    Indent = 19
    TabOrder = 2
    Items.Data = {
      090000001F0000000300000000000000FFFFFFFFFFFFFFFF0000000003000000
      064974656D2031200000000300000000000000FFFFFFFFFFFFFFFF0000000000
      000000074974656D203131200000000300000000000000FFFFFFFFFFFFFFFF00
      00000000000000074974656D203132200000000300000000000000FFFFFFFFFF
      FFFFFF0000000004000000074974656D203133210000000300000000000000FF
      FFFFFFFFFFFFFF0000000000000000084974656D203133312100000003000000
      00000000FFFFFFFFFFFFFFFF0000000000000000084974656D20313332210000
      000300000000000000FFFFFFFFFFFFFFFF0000000000000000084974656D2031
      3333210000000300000000000000FFFFFFFFFFFFFFFF00000000040000000849
      74656D20313334220000000300000000000000FFFFFFFFFFFFFFFF0000000000
      000000094974656D2031333431220000000300000000000000FFFFFFFFFFFFFF
      FF0000000000000000094974656D2031333432220000000300000000000000FF
      FFFFFFFFFFFFFF0000000000000000094974656D203133343322000000030000
      0000000000FFFFFFFFFFFFFFFF0000000004000000094974656D203133343423
      0000000300000000000000FFFFFFFFFFFFFFFF00000000000000000A4974656D
      203133343431230000000300000000000000FFFFFFFFFFFFFFFF000000000000
      00000A4974656D203133343432230000000300000000000000FFFFFFFFFFFFFF
      FF00000000000000000A4974656D203133343433230000000300000000000000
      FFFFFFFFFFFFFFFF00000000010000000A4974656D2031333434344900000000
      00000000000000FFFFFFFFFFFFFFFF0000000000000000304C6F6E6720737562
      6974656D20666F722073686F77696E67206F6620686F72697A6F6E74616C2073
      63726F6C6C6261721F0000000300000000000000FFFFFFFFFFFFFFFF00000000
      00000000064974656D20321F0000000300000000000000FFFFFFFFFFFFFFFF00
      00000004000000064974656D2033200000000300000000000000FFFFFFFFFFFF
      FFFF0000000000000000074974656D203331200000000300000000000000FFFF
      FFFFFFFFFFFF0000000000000000074974656D20333220000000030000000000
      0000FFFFFFFFFFFFFFFF0000000000000000074974656D203333200000000300
      000000000000FFFFFFFFFFFFFFFF0000000000000000074974656D2033341F00
      00000300000000000000FFFFFFFFFFFFFFFF0000000000000000064974656D20
      341F0000000300000000000000FFFFFFFFFFFFFFFF0000000000000000064974
      656D20351F0000000300000000000000FFFFFFFFFFFFFFFF0000000000000000
      064974656D20361F0000000300000000000000FFFFFFFFFFFFFFFF0000000000
      000000064974656D20371F0000000300000000000000FFFFFFFFFFFFFFFF0000
      000000000000064974656D20381F0000000300000000000000FFFFFFFFFFFFFF
      FF0000000000000000064974656D2039}
    BoundLabel.Layout = sclTopLeft
    SkinData.SkinSection = 'EDIT'
    SkinData.VertScrollData.ScrollWidth = 24
    SkinData.VertScrollData.ButtonsSize = 0
    SkinData.HorzScrollData.ScrollWidth = 12
    SkinData.HorzScrollData.ButtonsSize = 0
  end
  object sGroupBox4: TsGroupBox [3]
    Left = 318
    Top = 176
    Width = 264
    Height = 213
    Caption = 'Scrolls in custom control'
    TabOrder = 3
    CaptionLayout = clTopCenter
    SkinData.SkinSection = 'PANEL_LOW'
    Checked = False
    object sGroupBox5: TsGroupBox
      Left = 16
      Top = 24
      Width = 233
      Height = 69
      Caption = 'Arrow buttons visible'
      TabOrder = 0
      CaptionLayout = clTopCenter
      SkinData.SkinSection = 'GROUPBOX'
      CheckBoxVisible = True
      Checked = False
      OnCheckBoxChanged = sGroupBox5CheckBoxChanged
      object sCheckBox5: TsCheckBox
        Left = 21
        Top = 30
        Width = 76
        Height = 20
        Caption = 'Default size'
        Checked = True
        Enabled = False
        State = cbChecked
        TabOrder = 0
        OnClick = sCheckBox5Click
        ImgChecked = 0
        ImgUnchecked = 0
      end
      object sTrackBar3: TsTrackBar
        Left = 104
        Top = 28
        Width = 113
        Height = 27
        Enabled = False
        Max = 36
        Min = 12
        Position = 16
        TabOrder = 1
        TickStyle = tsNone
        OnChange = sTrackBar3Change
        SkinData.SkinSection = 'TRACKBAR'
        ShowProgress = True
        BarOffsetV = 0
        BarOffsetH = 0
      end
    end
    object sGroupBox6: TsGroupBox
      Left = 16
      Top = 108
      Width = 233
      Height = 89
      Caption = 'Default scroll width'
      TabOrder = 1
      CaptionLayout = clTopCenter
      SkinData.SkinSection = 'GROUPBOX'
      CheckBoxVisible = True
      Checked = False
      OnCheckBoxChanged = sGroupBox6CheckBoxChanged
      object sStickyLabel3: TsStickyLabel
        Left = 28
        Top = 28
        Width = 39
        Height = 13
        Caption = 'Vertical:'
        AttachTo = sTrackBar4
      end
      object sStickyLabel4: TsStickyLabel
        Left = 15
        Top = 58
        Width = 52
        Height = 13
        Caption = 'Horizontal:'
        AttachTo = sTrackBar5
      end
      object sTrackBar4: TsTrackBar
        Left = 69
        Top = 21
        Width = 148
        Height = 27
        Max = 36
        Min = 12
        Position = 24
        TabOrder = 0
        TickStyle = tsNone
        OnChange = sTrackBar4Change
        SkinData.SkinSection = 'TRACKBAR'
        ShowProgress = True
        BarOffsetV = 0
        BarOffsetH = 0
      end
      object sTrackBar5: TsTrackBar
        Left = 69
        Top = 51
        Width = 148
        Height = 27
        Max = 36
        Min = 12
        Position = 12
        TabOrder = 1
        TickStyle = tsNone
        OnChange = sTrackBar5Change
        SkinData.SkinSection = 'TRACKBAR'
        ShowProgress = True
        BarOffsetV = 0
        BarOffsetH = 0
      end
    end
  end
end
