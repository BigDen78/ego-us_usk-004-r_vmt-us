inherited Frame_Buttons2: TFrame_Buttons2
  Width = 604
  Height = 429
  object sCheckBox1: TsCheckBox [0]
    Left = 472
    Top = 40
    Width = 84
    Height = 20
    Caption = 'Show caption'
    Checked = True
    State = cbChecked
    TabOrder = 3
    OnClick = sCheckBox1Click
    ImgChecked = 0
    ImgUnchecked = 0
  end
  object sGroupBox2: TsGroupBox [1]
    Left = 32
    Top = 264
    Width = 153
    Height = 129
    Caption = 'Glyph layout'
    TabOrder = 7
    SkinData.SkinSection = 'GROUPBOX'
    Checked = False
    object sRadioButton1: TsRadioButton
      Left = 16
      Top = 26
      Width = 74
      Height = 20
      Caption = 'blGlyphLeft'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = sRadioButton1Click
    end
    object sRadioButton2: TsRadioButton
      Tag = 1
      Left = 16
      Top = 50
      Width = 80
      Height = 20
      Caption = 'blGlyphRight'
      TabOrder = 1
      OnClick = sRadioButton1Click
    end
    object sRadioButton3: TsRadioButton
      Tag = 2
      Left = 16
      Top = 74
      Width = 73
      Height = 20
      Caption = 'blGlyphTop'
      TabOrder = 2
      OnClick = sRadioButton1Click
    end
    object sRadioButton4: TsRadioButton
      Tag = 3
      Left = 16
      Top = 98
      Width = 89
      Height = 20
      Caption = 'blGlyphBottom'
      TabOrder = 3
      OnClick = sRadioButton1Click
    end
  end
  object sRadioGroup2: TsRadioGroup [2]
    Left = 224
    Top = 132
    Width = 154
    Height = 105
    Caption = 'Vertical alignment'
    TabOrder = 2
    OnClick = sRadioGroup2Click
    SkinData.SkinSection = 'GROUPBOX'
    Checked = False
    ItemIndex = 2
    Items.Strings = (
      'taAlignTop'
      'taAlignBottom'
      'taVerticalCenter')
  end
  object sCheckBox2: TsCheckBox [3]
    Left = 472
    Top = 72
    Width = 75
    Height = 20
    Caption = 'Show glyph'
    Checked = True
    State = cbChecked
    TabOrder = 5
    OnClick = sCheckBox2Click
    ImgChecked = 0
    ImgUnchecked = 0
  end
  object sBitBtn2: TsBitBtn [4]
    Tag = 5
    Left = 408
    Top = 264
    Width = 185
    Height = 97
    Caption = 'Button with child '#13#10'controls'
    TabOrder = 10
    Margin = 16
    Spacing = 16
    AcceptsControls = True
    DisabledGlyphKind = []
    ImageIndex = 7
    Images = MainForm.ImageList32
    VerticalAlignment = taAlignTop
    Reflected = True
    ShowFocus = False
    SkinData.SkinSection = 'SPEEDBUTTON'
    object sWebLabel1: TsWebLabel
      Left = 68
      Top = 47
      Width = 100
      Height = 13
      Caption = 'www.alphaskins.com'
      ParentFont = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      HoverFont.Charset = DEFAULT_CHARSET
      HoverFont.Color = clWindowText
      HoverFont.Height = -11
      HoverFont.Name = 'Tahoma'
      HoverFont.Style = []
      URL = 'http://www.alphaskins.com'
    end
    object sGauge1: TsGauge
      Left = 16
      Top = 70
      Width = 154
      Height = 15
      Animated = False
      SkinData.SkinSection = 'GAUGE'
      ForeColor = clBlack
      MaxValue = 100
      Progress = 47
      Suffix = '%'
    end
  end
  object sSpinEdit2: TsSpinEdit [5]
    Left = 352
    Top = 72
    Width = 81
    Height = 21
    TabOrder = 9
    Text = '4'
    OnChange = sSpinEdit2Change
    SkinData.SkinSection = 'EDIT'
    BoundLabel.Active = True
    BoundLabel.Caption = 'Spacing:'
    MaxValue = 14
    MinValue = 0
    Value = 4
  end
  object sSpinEdit1: TsSpinEdit [6]
    Left = 352
    Top = 40
    Width = 81
    Height = 21
    Enabled = False
    TabOrder = 8
    Text = '-1'
    OnChange = sSpinEdit1Change
    SkinData.SkinSection = 'EDIT'
    BoundLabel.Active = True
    BoundLabel.Caption = 'Margin:'
    MaxValue = 22
    MinValue = -1
    Value = -1
  end
  object sBitBtn1: TsBitBtn [7]
    Tag = 5
    Left = 32
    Top = 16
    Width = 242
    Height = 96
    Caption = 'Test of content alignment'
    TabOrder = 0
    Margin = 8
    DisabledGlyphKind = []
    ImageIndex = 7
    Images = MainForm.ImageList32
    Reflected = True
    ShowFocus = False
  end
  object sRadioGroup3: TsRadioGroup [8]
    Left = 416
    Top = 132
    Width = 154
    Height = 105
    Caption = 'Text alignment'
    Enabled = False
    TabOrder = 4
    OnClick = sRadioGroup3Click
    SkinData.SkinSection = 'GROUPBOX'
    Checked = False
    ItemIndex = 2
    Items.Strings = (
      'taLeftJustify'
      'taRightJustify'
      'taCenter')
  end
  object sGroupBox1: TsGroupBox [9]
    Left = 224
    Top = 264
    Width = 154
    Height = 129
    Caption = 'Disable control'
    TabOrder = 6
    SkinData.SkinSection = 'GROUPBOX'
    CheckBoxVisible = True
    Checked = False
    OnCheckBoxChanged = sGroupBox1CheckBoxChanged
    object sCheckBox3: TsCheckBox
      Left = 14
      Top = 26
      Width = 85
      Height = 20
      Caption = 'AlphaBlended'
      Checked = True
      Enabled = False
      State = cbChecked
      TabOrder = 0
      OnClick = sCheckBox3Click
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object sCheckBox4: TsCheckBox
      Left = 14
      Top = 50
      Width = 55
      Height = 20
      Caption = 'Grayed'
      Enabled = False
      TabOrder = 1
      OnClick = sCheckBox4Click
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object sCheckBox5: TsCheckBox
      Left = 14
      Top = 74
      Width = 114
      Height = 20
      Caption = 'AlphaBlended glyph'
      Enabled = False
      TabOrder = 2
      OnClick = sCheckBox5Click
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object sCheckBox6: TsCheckBox
      Left = 14
      Top = 98
      Width = 84
      Height = 20
      Caption = 'Grayed glyph'
      Enabled = False
      TabOrder = 3
      OnClick = sCheckBox6Click
      ImgChecked = 0
      ImgUnchecked = 0
    end
  end
  object sComboBox1: TsComboBox [10]
    Left = 464
    Top = 375
    Width = 121
    Height = 21
    Alignment = taLeftJustify
    BoundLabel.Active = True
    BoundLabel.Caption = 'Section:'
    VerticalAlignment = taAlignTop
    Style = csDropDownList
    ItemHeight = 15
    ItemIndex = -1
    TabOrder = 11
    OnChange = sComboBox1Change
    Items.Strings = (
      'TOOLBUTTON'
      'SPEEDBUTTON'
      'BUTTON')
  end
  object sRadioGroup1: TsRadioGroup [11]
    Left = 32
    Top = 132
    Width = 154
    Height = 105
    Caption = 'Alignment'
    TabOrder = 1
    OnClick = sRadioGroup1Click
    SkinData.SkinSection = 'GROUPBOX'
    Checked = False
    ItemIndex = 2
    Items.Strings = (
      'taLeftJustify'
      'taRightJustify'
      'taCenter')
  end
end
