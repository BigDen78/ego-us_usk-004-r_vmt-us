unit UnitFrameHints;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,

  sFrameAdapter, sComboBox, sBitBtn, sLabel, sListBox, sGroupBox, sPanel,

  UnitFrameCustom, sCheckBox;


type
  TFrame_Hints = class(TCustomInfoFrame)
    sBitBtn1: TsBitBtn;
    sLabelFX1: TsLabelFX;
    sListBox1: TsListBox;
    sCheckBox1: TsCheckBox;
    sCheckBox2: TsCheckBox;
    sCheckBox3: TsCheckBox;
    sRadioGroup1: TsRadioGroup;
    procedure sBitBtn1Click    (Sender: TObject);
    procedure sListBox1Click   (Sender: TObject);
    procedure sCheckBox1Click  (Sender: TObject);
    procedure sCheckBox2Click  (Sender: TObject);
    procedure sCheckBox3Click  (Sender: TObject);
    procedure sRadioGroup1Click(Sender: TObject);
    procedure sRadioGroup1CheckBoxChanged(Sender: TObject);
  public
    InUpdating: boolean;
    procedure UpdateStates;
    procedure AfterCreation; override;
  end;


implementation

uses sConst, MainUnit, acAlphaHintsEdit, sVCLUtils;

{$R *.DFM}

procedure TFrame_Hints.sBitBtn1Click(Sender: TObject);
var
  i: integer;
begin
  EditHints(MainForm.sAlphaHints1);
  // Generate new list
  sListBox1.Items.Clear;
  sListBox1.Items.Add('Default internal');
  for i := 0 to MainForm.sAlphaHints1.Templates.Count - 1 do
    sListBox1.Items.Add(MainForm.sAlphaHints1.Templates[i].Name);

  sListBox1.ItemIndex := 0
end;


procedure TFrame_Hints.sRadioGroup1Click(Sender: TObject);
begin
  case sRadioGroup1.ItemIndex of
    0: MainForm.ActionHintsSkinned.Execute;
    1: MainForm.ActionHintsCustom.Execute;
    2: MainForm.ActionHintsStd.Execute;
  end;
  sListBox1.Enabled := MainForm.ActionHintsCustom.Checked;
  sBitBtn1.Enabled  := MainForm.ActionHintsCustom.Checked;
end;


procedure TFrame_Hints.UpdateStates;
begin
  InUpdating := True;
  sRadioGroup1.Checked := not MainForm.ActionHintsDisable.Checked;
  SetControlsEnabled(sRadioGroup1, sRadioGroup1.Checked);

  if MainForm.ActionHintsSkinned.Checked then
    sRadioGroup1.ItemIndex := 0
  else
    if MainForm.ActionHintsCustom.Checked then
      sRadioGroup1.ItemIndex := 1
    else
      if MainForm.ActionHintsStd.Checked then
        sRadioGroup1.ItemIndex := 2;

  sListBox1.Enabled := MainForm.ActionHintsCustom.Checked;
  sBitBtn1.Enabled  := MainForm.ActionHintsCustom.Checked;
  InUpdating := False;
end;


procedure TFrame_Hints.sListBox1Click(Sender: TObject);
begin
  MainForm.sAlphaHints1.TemplateName := sListBox1.Items[sListBox1.ItemIndex]
end;


procedure TFrame_Hints.sRadioGroup1CheckBoxChanged(Sender: TObject);
begin
  if not InUpdating then begin
    if sRadioGroup1.Checked then
      sRadioGroup1Click(sRadioGroup1)
    else
      MainForm.ActionHintsDisable.Execute;

    MainForm.sSpeedButton4.Down := sRadioGroup1.Checked;
    SetControlsEnabled(sRadioGroup1, sRadioGroup1.Checked);
    sListBox1.Enabled := sRadioGroup1.Checked and MainForm.ActionHintsCustom.Checked;
  end;
end;


procedure TFrame_Hints.sCheckBox1Click(Sender: TObject);
begin
  MainForm.sAlphaHints1.Animated := sCheckBox1.Checked;
end;


procedure TFrame_Hints.sCheckBox2Click(Sender: TObject);
begin
  MainForm.sAlphaHints1.AutoAlignment := sCheckBox2.Checked;
end;


procedure TFrame_Hints.sCheckBox3Click(Sender: TObject);
begin
  MainForm.sAlphaHints1.HTMLMode := sCheckBox3.Checked;
end;


procedure TFrame_Hints.AfterCreation;
var
  i: integer;
begin
  sListBox1.Items.Clear;
  sListBox1.Items.Add('Default internal');
  for i := 0 to MainForm.sAlphaHints1.Templates.Count - 1 do
    sListBox1.Items.Add(MainForm.sAlphaHints1.Templates[i].Name);

  sListBox1.ItemIndex := 0;
  UpdateStates;
end;

end.
