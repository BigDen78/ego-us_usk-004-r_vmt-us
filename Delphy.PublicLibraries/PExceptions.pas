unit PExceptions;

interface
uses Classes, SysUtils, Forms, ExtCtrls;

type

{
                                     *=========================*
                                     * ������������ ���������� *
                                     *         v. 1.1          *
                                     *=========================*
}

{  ������ ������������ ���������� � ���������� ������ � ��������� ���� � ��������� ������ ���� ��������� ������ ���������,
   � ������� ��������� ����������.

   Written by: Mike Keskinov, 2007.
   Based on: JCL library.

  ����� � ����� �� ������ ������ ���������� � ������ ����, ��� ��� ���������,
  � ������ ������� �� ������� Linker ���������� ����� MapFile = Detailed.
  ��� ���� ������ � ���������� ����� ����������� ���� *.map.

  ��� ��������� �������������� (� ������� try..except) ���������� � �������� ������ ���������, ����� ������
  ������� ��������� ������� TProcessException � ��� ������������� (�� �����������!) ������ �������:
  OnExceptionGetInfo, OnExceptionSaveList, OnExceptionFinal.

  ��� ������������ ���������� � ������ �������, ������ �������������� ����� ��������� � try..except:
  procedure <ThreadName>.Execute;
  begin
  try
    while not Terminated do
    begin
      <...>
    end;
  except
    on E: Exception do ProcessException.RaiseException( <ThreadName>, E );
  end;
  end;
  
  ��� ���� � �������� ������ ���� ������������ �������� ������� TestExceptionAnotherThread.
}



  TExceptionDescription =
  record
    ThreadName: string;
    IsException: Boolean;
    EMessage: string;
    EStack: TStringList;
  end;

  TOnExceptionGetInfo = procedure ( InfoList: TStringList ) of object;
  TOnExceptionSaveList = function ( InfoList: TStringList ): Boolean of object;
  TOnExceptionFinal = procedure( LogSaved: Boolean ) of object;

  TProcessException = class
    private
      ExceptionAnotherThread: TExceptionDescription;
      AnotherThreadMonitorTimer: TTimer;
      FMonitorAnotherThreads: Boolean;
      procedure OnException(Sender: TObject; E: Exception);
      function GetExceptionStackList: TStringList;
      procedure ProcessException( ExeptionMessage, ExeptionThread: string; StackData: TStringList );
      procedure SetMonitorAnotherThreads( B: Boolean );
      procedure MonitorAnotherThread( Sender: TObject );
    public
      OnExceptionGetInfo: TOnExceptionGetInfo;    // � ��� ������� ����� �������� �������������� ���������� � ����� �� ������.
      OnExceptionSaveList: TOnExceptionSaveList;  // ���������� ������ �� ������.
      OnExceptionFinal:  TOnExceptionFinal;       // ����� ��������� �� ������ � ���������� ���������.
      ExceptionLogFolder: string;
      constructor Create;
      destructor Destroy; override;

      // ��������� ���������� � ������ �������.
      procedure RaiseException( ThreadName: string; E: Exception );
      function TestExceptionAnotherThread: Boolean;    // ���������� True, ���� ���� ����������.
      property MonitorAnotherThreads: Boolean read FMonitorAnotherThreads write SetMonitorAnotherThreads;

  end;


implementation
uses JclDebug;




{ TProcessException }

constructor TProcessException.Create;
begin
  ExceptionAnotherThread.IsException:= False;
  ExceptionLogFolder:= 'ERRORS';
  Include(JclStackTrackingOptions, stRawMode);
  Include(JclStackTrackingOptions, stStaticModuleList);
  JclStartExceptionTracking;
  Application.OnException:= OnException;
  AnotherThreadMonitorTimer:= nil;
  FMonitorAnotherThreads:= False;
end;

procedure TProcessException.OnException(Sender: TObject; E: Exception);
begin
  ProcessException( E.Message, 'MAIN', GetExceptionStackList );
end;

function TProcessException.GetExceptionStackList: TStringList;
begin
  try
    Result:= TStringList.Create;
    JclLastExceptStackListToStrings( Result );
  except
    Result.Add( 'STACK REQUEST IS FAILED!' );
  end;
end;


procedure TProcessException.RaiseException( ThreadName: string; E: Exception );
begin
  ExceptionAnotherThread.EMessage:= E.Message;
  ExceptionAnotherThread.EStack:= GetExceptionStackList;
  ExceptionAnotherThread.IsException:= True;
  ExceptionAnotherThread.ThreadName:= ThreadName;
end;

function TProcessException.TestExceptionAnotherThread: Boolean;
begin
  if ExceptionAnotherThread.IsException then
  begin
    if Assigned( AnotherThreadMonitorTimer ) then AnotherThreadMonitorTimer.Enabled:= False;
    Result:= True;
    ExceptionAnotherThread.IsException:= False;
    ProcessException( ExceptionAnotherThread.EMessage, ExceptionAnotherThread.ThreadName, ExceptionAnotherThread.EStack );
  end else
    Result:= False;
end;

procedure TProcessException.ProcessException( ExeptionMessage, ExeptionThread: string; StackData: TStringList );
var
  SL: TStringList;
  isSaved: Boolean;
  S: string;
  b:boolean;
begin
  isSaved:= False;
  try
    SL:= TStringList.Create;
    try
      SL.Add( FormatDateTime( 'dd.mm.yy hh:nn:ss', Now ) );
      SL.Add( 'EXEPTION: ' + ExeptionMessage );
      SL.Add( 'THREAD: ' + ExeptionThread );
    except
      SL.Add( 'MAIN INFORMATION REQUEST FAILED!' );
    end;

    if Assigned( OnExceptionGetInfo ) then OnExceptionGetInfo( SL );

    try
      // ��������� ������ �����.
      SL.Add('');
      SL.Add('STACK LIST:');
      SL.AddStrings( StackData );
      StackData.Free;
    except
      SL.Add('CAN''T ADD STACK DATA!');
    end;

    try
      if Assigned( OnExceptionSaveList ) then isSaved:= OnExceptionSaveList( SL ) else
      begin
        // ������� ���������� ������ ��-���������.
//        S:=ExcludeTrailingBackslash(ExtractFilePath( Application.ExeName ));
        S:=ExtractFilePath( Application.ExeName );
        b:=SetCurrentDir( S );
//        ChDir( ExtractFilePath( Application.ExeName ) );
        if not DirectoryExists( ExceptionLogFolder ) then MkDir( ExceptionLogFolder );
//        ChDir( ExceptionLogFolder );
        SetCurrentDir( ExceptionLogFolder );
        SL.SaveToFile( FormatDateTime( 'ddmmyy_hhnnss', Now ) + '.log' );
        isSaved:= True;
      end;
    finally
      SL.Free;
    end;

  except
  end;

  if Assigned( OnExceptionFinal ) then OnExceptionFinal( isSaved ) else
  try
    if isSaved then S:= ' ' else S:= ' �� ';
    Application.MessageBox( PChar( '���������� ������ ���������.'#13#10#10'����� �� ������' + S + '�������� � ����� ' + ExceptionLogFolder + '.' ), '���������� ������', 16 );
  finally
    Halt;
  end;

end;


destructor TProcessException.Destroy;
begin
  JclStopExceptionTracking;
end;

procedure TProcessException.SetMonitorAnotherThreads(B: Boolean);
begin
  if B = FMonitorAnotherThreads then Exit;
  if not B then
  begin
    // ��������� ������.
    if Assigned( AnotherThreadMonitorTimer ) then AnotherThreadMonitorTimer.Enabled:= False;
  end else
  begin
    // �������� ������.
    if not Assigned( AnotherThreadMonitorTimer ) then
    begin
      AnotherThreadMonitorTimer:= TTimer.Create( nil );
      AnotherThreadMonitorTimer.Interval:= 3000;
      AnotherThreadMonitorTimer.OnTimer:= MonitorAnotherThread;
    end;
    AnotherThreadMonitorTimer.Enabled:= True;
  end;
end;

procedure TProcessException.MonitorAnotherThread(Sender: TObject);
begin
  TestExceptionAnotherThread;
end;

end.
