unit LanguageUnit;

interface

uses
  Classes, Forms, SysUtils, Dialogs;

type

 TProgLanguage = class
   WorkLang: Integer;
   constructor Create;
   destructor Destroy; override;
   function GetCaption(ID: Integer): string;
 end;

var
  Language: TProgLanguage;

implementation

constructor TProgLanguage.Create;
begin
  //
end;

destructor TProgLanguage.Destroy;
begin
  inherited Destroy;
end;

function TProgLanguage.GetCaption(ID: Integer): string;
begin
  Result:= '';
end;

initialization

  Language:= TProgLanguage.Create;

finalization

  Language.Free;
  Language:= nil;

end.


