unit MyTypes;

interface

uses
  Types, Windows, Graphics, Chart, Classes;

type

  TFilterMode = (moNoCalc, moOff, moMark, moHide);

  TProgressMess = procedure (Msg: string; Pas: Integer = 1) of object;
(*
  TFlashPageLog = packed record
    Page: Word;
    Chip: Byte;
    ID: Byte;
    ChectSum: Byte;
    ChectSumError: Boolean;
    FileOffset: Integer;
    NextPage: Word;
    NextChip: Byte;
  end;
*)

  TMinMax = record
    Min: Integer;
    Max: Integer;
  end;

  TRange = record
    StCrd: Integer;
    EdCrd: Integer;
  end;

  TEchoPixel = array [0..16] of TPoint;

  TBScanDelayZone = record
    BegDelay: Integer;
    EndDelay: Integer;
    Dur: Integer;
  end;

  RRail = (r_Left, r_Right, r_Both);
  TSide = (sLeft, sRight);            // � �����

  TBScanDelayZoneList = array [0..7] of TBScanDelayZone;

  TViewMode = (ddBothRail,
               ddLeftRail, ddRightRail,
               ddLeftTape1, ddLeftTape2, ddLeftTape3, ddLeftTape4, ddRightTape1, ddRightTape2, ddRightTape3, ddRightTape4);

  TViewChannel = (vcAll, vcOtezd, vcNaezd);

  TRealCoord = record
    Km: Integer;
    Pk: Integer;
    MM: Integer;
  end;

// ---------<  >----------------------------------------------------------------

  TDisplayColors = record
    Fon        : TColor; // ��� ��������
    Border     : TColor; // ������
    BackMotion : TColor; // ���� ���������� �����
    NaezdCh    : TColor; // ���������� ������
    OtezdCh    : TColor; // ����������� ������
    CrossedCh  : TColor; // �������������� ������
    Text       : TColor; // �������
    Event      : TColor; // �������
    Defect     : TColor; // ������
    CoordNet   : TColor; // ����� ���������
    Ruler      : TColor; // �������
    EnvelopeBS : TColor; // ��������� ��
    LongLab    : TColor; // ����������� �����
  end;

  TPalItem = record
    Name: string;
    DC: TDisplayColors;
    PC: TDisplayColors;
  end;

  TPalList = array of TPalItem;

  TBScanViewZoneItem = record
    BegDelay: Integer;
    EndDelay: Integer;
    Dur: Integer;
  end;

  TDisplayConfig = record
    ReducePos: array [1..2, 1..2, 0..7] of Integer;
    ReduceDel: array [1..2, 1..2, 0..7] of Extended;
    PixelSize: array [0..15] of TPoint;
    BScanViewZone: array [0..7] of TBScanViewZoneItem;
    Ruler: Boolean;
    CoordNet: Boolean;
    ShowEvent: array [$90..$AF] of Boolean;
  end;

// ---------< ������������� ����� >--------------------------------------------------------------------

  TStolbSide = (ssLeft, ssRight);

  TStolbSizeData = record
    Km: Integer;
    Pk: Integer;
  end;

  TStolb = array [TStolbSide] of TStolbSizeData;

  TCoordParams = record
    LeftStolb: TStolb;
    ToLeftStolbMM: Integer;
    LeftIdx: Integer;
    RightStolb: TStolb;
    ToRightStolbMM: Integer;
    RightIdx: Integer;
  end;

////////////////////////////////////////////////////////////////////////////////

  TMySet = class
    FCap: Integer;
    FMaxIdx: Integer;
    FItems: array of Integer;
    function GetItem(Index: Integer): Integer;
    function GetCount: Integer;
    constructor Create;
    destructor Destroy; override;
    function AddVal(Val: Integer): Boolean;
    procedure AddSet(MySet: TMySet);
    function InSet(Val: Integer): Boolean;
    property Item[Idx: Integer]: Integer read GetItem; default;
    property Count: Integer read GetCount;
  end;

  TMySetList = class
    FCap: Integer;
    FMaxIdx: Integer;
    FItems: array of TMySet;
    function GetItem(Index: Integer): TMySet;
    function GetCount: Integer;
    constructor Create;
    destructor Destroy; override;
    function AddSet(NewSet: TMySet = nil): Integer;
    function InSets(Val: Integer; var Idx: Integer): Boolean;
    function TestCross(SetList: TMySetList): Boolean;
    procedure Delete(Idx: Integer);
    property Item[Idx: Integer]: TMySet read GetItem; default;
    property Count: Integer read GetCount;
  end;


const
  ZoomList: array [0..15] of Integer = (100, 200, 300, 400, 500, 1000, 2000, 3000, 4000, 5000, 10000, 20000, 30000, 40000, 50000, 100000);

  SpeedList: array [-7..7] of Integer = (- 1500, - 1200, - 900, - 600, - 300, - 120, - 40, 0, 40, 120, 300, 600, 900, 1200, 1500);

  function ByteToSide(EventID, Data: Byte): TSide;
  function RailToStr(R: RRail): string;
  function CoordParamsToRealCoord(CoordParams: TCoordParams; MoveDir: Integer): TRealCoord;
  function RealCoordToStr(Data: TRealCoord; Index: Integer) : string;
  function GetRealCoordKm(Data: TRealCoord) : string;
  function GetRealCoordPk(Data: TRealCoord) : string;
  function GetRealCoordMetre(Data: TRealCoord) : string;
  function GetRealCoordMM(Data: TRealCoord) : string;
  function LineCoordToStr(Coord_: Extended; Index: Integer): string;
  function ByteToCh(EventID, Data: Byte): Integer;
  function BCDtoInt(Src: Byte): Integer;
  function ShiftDown: Boolean;
  function RealCoordToReal(Km, Pk, Metre: Integer): Integer;
  function InvertGPSSingle(Src: Single): Single;
  function GetCentr(Src: TRange): Integer;
  function GetLen(Src: TRange): Integer;
  function Range(StCrd, EdCrd: Integer): TRange;
  function InRange(RIn, ROut: TRange): Boolean;
  function PtInRange(Crd: Integer; Range: TRange): Boolean;
  function OutRange(R1, R2: TRange): TRange;
  function TestRangeIntersec(R1, R2: TRange): Boolean;
  function IntersecLen(R1, R2: TRange): Integer;
  function IntersecSquare(ACrd, ADepth, BCrd, BDepth: TRange): Integer;
  function TestIntersec(ACrd, ADepth, BCrd, BDepth: TRange): Boolean;
  function StickOut(R1, R2: TRange): Boolean;

  procedure StopTone;
  procedure SetTone(Hz500Flash, Hz500, Hz1000, Hz2000: Boolean);

implementation

uses

  SysUtils, MMSystem, Avk11Engine, LanguageUnit, Math;


function GetCentr(Src: TRange): Integer;
begin
  Result:= (Src.StCrd + Src.EdCrd) div 2;
end;

function GetLen(Src: TRange): Integer;
begin
  Result:= Abs(Src.EdCrd - Src.StCrd) + 1;
end;

function Range(StCrd, EdCrd: Integer): TRange;
begin
  Result.StCrd:= StCrd;
  Result.EdCrd:= EdCrd;
end;

function InRange(RIn, ROut: TRange): Boolean;
begin
//  Result:= (RIn.StCrd >= ROut.StCrd) and (RIn.EdCrd <= ROut.EdCrd);
  Result:= (Min(RIn.StCrd, RIn.EdCrd) >= Min(ROut.StCrd, ROut.EdCrd)) and
           (Max(RIn.StCrd, RIn.EdCrd) <= Max(ROut.StCrd, ROut.EdCrd));
end;

function PtInRange(Crd: Integer; Range: TRange): Boolean;
begin
  Result:= (Crd >= Range.StCrd) and (Crd <= Range.EdCrd);
end;

function OutRange(R1, R2: TRange): TRange;
begin
  Result.StCrd:= Min(R1.StCrd, R2.StCrd);
  Result.EdCrd:= Max(R1.EdCrd, R2.EdCrd);
end;
{
function TestRangeIntersec(R1, R2: TRange): Boolean;
begin
  Result:= (R1.StCrd <= R2.EdCrd) and (R1.EdCrd >= R2.StCrd);
end;

function IntersecLen(R1, R2: TRange): Integer;
begin
  if not TestRangeIntersec(R1, R2) then Result:= 0 else Result:= Min(R1.EdCrd, R2.EdCrd) - Max(R1.StCrd, R2.StCrd) + 1;
end;
}
function IntersecSquare(ACrd, ADepth, BCrd, BDepth: TRange): Integer;
var
  ASquare: Integer;
  BSquare: Integer;
  ABSquare: Integer;

begin
  ABSquare:= IntersecLen(ACrd, BCrd) * IntersecLen(ADepth, BDepth);
  ASquare:= GetLen(ACrd) * GetLen(ADepth);
  BSquare:= GetLen(BCrd) * GetLen(BDepth);
  Result:= Round(100 * ABSquare / Min(ASquare, BSquare));
end;

function TestIntersec(ACrd, ADepth, BCrd, BDepth: TRange): Boolean;
begin
  Result:= TestRangeIntersec(ACrd, BCrd) and TestRangeIntersec(ADepth, BDepth);
end;

function TestRangeIntersec(R1, R2: TRange): Boolean;
begin
  Result:= (Min(R1.StCrd, R1.EdCrd) <= Max(R2.StCrd, R2.EdCrd)) and
           (Max(R1.StCrd, R1.EdCrd) >= Min(R2.StCrd, R2.EdCrd))
end;


function StickOut(R1, R2: TRange): Boolean;
begin
  Result:= ((PtInRange(R1.StCrd, R2) and not PtInRange(R1.EdCrd, R2)) or
            (not PtInRange(R1.StCrd, R2) and PtInRange(R1.EdCrd, R2))) and
           ((PtInRange(R2.StCrd, R1) and not PtInRange(R2.EdCrd, R1)) or
            (not PtInRange(R2.StCrd, R1) and PtInRange(R2.EdCrd, R1)));
end;

function IntersecLen(R1, R2: TRange): Integer;
begin
  if not TestRangeIntersec(R1, R2)
    then Result:= 0
    else Result:= Min(Max(R1.StCrd, R1.EdCrd), Max(R2.StCrd, R2.EdCrd)) -
                  Max(Min(R1.StCrd, R1.EdCrd), Min(R2.StCrd, R2.EdCrd));
end;

  //-----------------------------------------------------------------------------------

function InvertGPSSingle(Src: Single): Single;
var
  Tmp1: array [1..4] of Byte;
  Tmp2: array [1..4] of Byte;

begin
  Move(Src, Tmp1, 4);
  Tmp2[1]:= Tmp1[4];
  Tmp2[2]:= Tmp1[3];
  Tmp2[3]:= Tmp1[2];
  Tmp2[4]:= Tmp1[1];
  Move(Tmp2, Result, 4);
end;

function RealCoordToReal(Km, Pk, Metre: Integer): Integer;
begin
  Result:= Km * 1000 + (Pk - 1) * 100 + Metre;
end;

function CoordParamsToRealCoord(CoordParams: TCoordParams; MoveDir: Integer): TRealCoord;
begin
  Result.Km:= CoordParams.LeftStolb[ssRight].Km;
  Result.Pk:= CoordParams.LeftStolb[ssRight].Pk;

//  Result.Km:= CoordParams.RightStolb[ssRight].Km;
//  Result.Pk:= CoordParams.RightStolb[ssRight].Pk;

  if MoveDir = 1 then Result.MM:= CoordParams.ToLeftStolbMM
                 else Result.MM:= CoordParams.ToRightStolbMM;
end;

function ByteToSide(EventID, Data: Byte): TSide;
begin
  case EventID of
            0: Result:= TSide(Ord(Data and 64 <> 0));
     EID_Sens,
      EID_Att,
      EID_VRU,
    EID_StStr,
   EID_EndStr,
      EID_2Tp,
 EID_2Tp_Word,      
  EID_ZondImp: Result:= TSide(Ord(Data and 16 <> 0));
  end;
end;

function RailToStr(R: RRail): string;
begin
  case R of
    r_Left: Result:= Language.GetCaption(0000007);
   r_Right: Result:= Language.GetCaption(0000008);
    r_Both: Result:= '���';
  end;
end;

function ByteToCh(EventID, Data: Byte): Integer;
begin
  case EventID of
            0: Result:= Data shr 3 and $07;
     EID_Sens,
      EID_Att,
      EID_VRU,
    EID_StStr,
   EID_EndStr,
      EID_2Tp,
 EID_2Tp_Word,
  EID_ZondImp: begin
                 if Data and 8 = 0
                   then Result:= Data and 7
                   else
                   begin
                     Result:= Data and 7;
                     case Data and 7 of
                       1: Result:= 0;
                       2: Result:= 10;
                       3: Result:= 11;
                       6: Result:= 8;
                       7: Result:= 9;
                     end;
                   end;
               end;
  end;
end;

function BCDtoInt(Src: Byte): Integer;
begin
  Result:= Src and $0F + 10 * (Src and $F0 shr 4);
end;

function ShiftDown : Boolean;
var
  State : TKeyboardState;

begin
  GetKeyboardState(State);
  Result := ((State[vk_Shift] and 128) <> 0);
end;

function LineCoordToStr(Coord_: Extended; Index: Integer): string;
var
  Coord: Int64;

begin
  Coord:= Trunc(Coord_ * 1000);
  if Coord div 1000000 <> 0 then
  begin
    case Index of
      0: Result:= IntToStr(Coord div 1000000) + ' ' + Language.GetCaption(0000001) + ' ' +
                  IntToStr(Round(1000 * Frac(Coord div 1000 / 1000))) + ' ' + Language.GetCaption(0000003) + ' ' +
                  IntToStr(Round(1000 * Frac(Coord / 1000))) + ' ' + Language.GetCaption(0000003) + ' ';
      1: Result:= IntToStr(Coord div 1000000) + ' ' + Language.GetCaption(0000001) + ' ' +
                  IntToStr(Round(1000 * Frac(Coord div 1000 / 1000))) + ' ' + Language.GetCaption(0000003) +' ' +
                  IntToStr(Round(1000 * Frac(Coord / 1000)) div 10) + ' ' + Language.GetCaption(0000014) + ' ';
      2: Result:= IntToStr(Coord div 1000000) + ' ' + Language.GetCaption(0000001) + ' ' +
                  IntToStr(Round(1000 * Frac(Coord div 1000 / 1000))) + ' ' + Language.GetCaption(0000003) + ' ';
    end;
  end
  else
  begin
    case Index of
      0: Result:= IntToStr(Round(1000 * Frac(Coord div 1000 / 1000))) + ' ' + Language.GetCaption(0000003) + ' ' +
                  IntToStr(Round(1000 * Frac(Coord / 1000))) + ' ' + Language.GetCaption(0000013) + ' ';
      1: Result:= IntToStr(Round(1000 * Frac(Coord div 1000 / 1000))) + ' ' + Language.GetCaption(0000003) + ' ' +
                  IntToStr(Round(1000 * Frac(Coord / 1000)) div 10) + ' ' + Language.GetCaption(0000014) + '  ';
      2: Result:= IntToStr(Round(1000 * Frac(Coord div 1000 / 1000))) + ' ' + Language.GetCaption(0000003) + ' ';
    end;
  end;
end;

function RealCoordToStr(Data: TRealCoord; Index: Integer) : string;
var
  M, MM: Integer;

begin
  M:= Round(1000 * Frac(Data.MM div 1000 / 1000));
  MM:= Round(1000 * Frac(Data.MM / 1000));

  case Index of
    0: Result:= Format('%D ' + Language.GetCaption(0000001) + ' %D ' + Language.GetCaption(0000002) + ' %D ' + Language.GetCaption(0000003) + ' %D ' + Language.GetCaption(0000013), [Data.Km, Data.Pk, M, MM]);
    1: Result:= Format('%D ' + Language.GetCaption(0000001) + ' %D ' + Language.GetCaption(0000002) + ' %D ' + Language.GetCaption(0000003) + ' %D ' + Language.GetCaption(0000014), [Data.Km, Data.Pk, M, MM div 10]);
    2: Result:= Format('%D ' + Language.GetCaption(0000001) + ' %D ' + Language.GetCaption(0000002) + ' %D ' + Language.GetCaption(0000003) + '', [Data.Km, Data.Pk, M]);
    3: Result:= Format('%D / %D / %D', [Data.Km, Data.Pk, M]);
  end;
end;

function GetRealCoordKm(Data: TRealCoord) : string;
begin
  Result:= Format('%D ' + Language.GetCaption(0000001), [Data.Km]);
end;

function GetRealCoordPk(Data: TRealCoord) : string;
begin
  Result:= Format('%D ' + Language.GetCaption(0000002), [Data.Pk]);
end;

function GetRealCoordMetre(Data: TRealCoord) : string;
begin
  Result:= Format('%D ' + Language.GetCaption(0000003), [Round(1000 * Frac(Data.MM div 1000 / 1000))]);
end;

function GetRealCoordMM(Data: TRealCoord) : string;
begin
  Result:= Format(' %D ' + Language.GetCaption(0000013), [Round(1000 * Frac(Data.MM / 1000))]);
end;

// -----[ Sound ]---------------------------------------------------------------

var
  Wave: record  { format of WAV file header }
    rId             : longint; { 'RIFF'  4 characters }
    rLen            : longint; { length of DATA + FORMAT chunk }
    { FORMAT CHUNK }
    wId             : longint; { 'WAVE' }
    fId             : longint; { 'fmt ' }
    fLen            : longint; { length of FORMAT DATA = 16 }
    { format data }
    wFormatTag      : word;    { $01 = PCM }
    nChannels       : word;    { 1 = mono, 2 = stereo }
    nSamplesPerSec  : longint; { Sample frequency ie 11025}
    nAvgBytesPerSec : longint; { = nChannels * nSamplesPerSec * (nBitsPerSample/8) }
    nBlockAlign     : word;    { = nChannels * (nBitsPerSAmple / 8 }
    wBitsPerSample  : word;    { 8 or 16 }
    { DATA CHUNK }
    dId             : longint; { 'data' }
    wSampleLength   : longint; { length of SAMPLE DATA }
    { sample data : offset 44 }
    Data: array [1..10000, 1..2] of Byte;
    { for 8 bit mono = s[0],s[1]... :byte}
    { for 8 bit stereo = sleft[0],sright[0],sleft[1],sright[1]... :byte}
    { for 16 bit mono = s[0],s[1]... :word}
    { for 16 bit stereo = sleft[0],sright[0],sleft[1],sright[1]... :word}
  end;

  oHz500Flash, oHz500, oHz1000, oHz2000: Boolean;

procedure SetTone(Hz500Flash, Hz500, Hz1000, Hz2000: Boolean);
var
  I: Integer;
  fLeftVolume: Smallint;
  fRightVolume: Smallint;
  newvol: DWORD;
  DeviceID: Integer;

begin
  if not (Hz500Flash or Hz500 or Hz1000 or Hz2000) then
  begin
    oHz500Flash:= Hz500Flash;
    oHz500     := Hz500     ;
    oHz1000    := Hz1000    ;
    oHz2000    := Hz2000    ;
    PlaySound(nil, 0, 0);
    Exit;
  end;

  if (oHz500Flash = Hz500Flash) and
     (oHz500      = Hz500     ) and
     (oHz1000     = Hz1000    ) and
     (oHz2000     = Hz2000    ) then Exit;

  oHz500Flash:= Hz500Flash;
  oHz500     := Hz500     ;
  oHz1000    := Hz1000    ;
  oHz2000    := Hz2000    ;

  Wave.rId:= $46464952;       { 'RIFF' }
  Wave.rLen:= 20000 + 36;     { length of sample + format }
  Wave.wId:= $45564157;       { 'WAVE' }
  Wave.fId:= $20746d66;       { 'fmt ' }
  Wave.fLen:= 16;             { length of format chunk }
  Wave.wFormatTag:= 1;        { PCM data }
  Wave.nChannels:= 2;         { mono/stereo }
  Wave.nSamplesPerSec:= 40000; { sample rate }
  Wave.nAvgBytesPerSec:= 2 * 40000 * 1;
  Wave.nBlockAlign:= 2; // channels * (resolution div 8);
  Wave.wBitsPerSample:= 8;    { resolution 8/16 }
  Wave.dId:= $61746164;       { 'data' }
  Wave.wSampleLength:= 20000; { sample size }

  if not (Hz500Flash or Hz500 or Hz1000 or Hz2000) then for I:= 1 to 10000 do Wave.Data[I, 1]:= 127 else
  for I:= 1 to 10000 do
  begin
    Wave.Data[I, 1]:= Round(127 * (1 + ( Ord(Hz500Flash) * Sin(125 * 2 * Pi * (I - 1) / 10000 * Ord((I < 1000) or ((I > 2000) and (I < 3000)) or ((I > 4000) and (I < 5000)) or ((I > 6000) and (I < 7000)) or ((I > 8000) and (I < 9000)) )  ) +
                                              Ord(Hz500) * Sin(125 * 2 * Pi * (I - 1) / 10000) +
                                             Ord(Hz1000) * Sin(250 * 2 * Pi * (I - 1) / 10000) +
                                             Ord(Hz2000) * Sin(500 * 2 * Pi * (I - 1) / 10000)) / (Ord(Hz500Flash) + Ord(Hz500) + Ord(Hz1000) + Ord(Hz2000))));

    Wave.Data[I, 2]:= Wave.Data[I, 1];
  end;
{
 }
//  Move(Wave.Data[1, 1], Wave.Data[1, 2], 1000);

{  if not (Right500Hz or Right1000Hz or Right2000Hz) then for I:= 1 to 40 do Wave.Data[I, 2]:= 127 else
  for I:= 1 to 40 do
    Wave.Data[I, 2]:= Round(127 * (1 + (Ord(Right500Hz) * Sin(2 * Pi * (I - 1) / 40) +
                                        Ord(Right1000Hz) * Sin(2 * Pi * (I - 1) / 20) +
                                        Ord(Right2000Hz) * Sin(2 * Pi * (I - 1) / 10)) / (Ord(Right500Hz) + Ord(Right1000Hz) + Ord(Right2000Hz))));
 }
  fLeftVolume:= 10;
  fRightVolume:= 10;
{$R-}
  newvol:=(($ffff * fLeftVolume) div 100)+((($ffff * fRightVolume) div 100) shl $10);
{$R+}
  DeviceID:= 0;
  waveOutSetVolume(DeviceID, newvol);
  PlaySound(nil, 0, 0);
  PlaySound(@Wave, 0, SND_ASYNC + SND_MEMORY + SND_LOOP);
end;

procedure StopTone;
begin
  PlaySound(nil, 0, 0);
end;

////////////////////////////////////////////////////////////////////////////////
// TMySet
////////////////////////////////////////////////////////////////////////////////

constructor TMySet.Create;
begin
  FCap:= 0;
  FMaxIdx:= - 1;
end;

destructor TMySet.Destroy;
begin
  SetLength(FItems, 0);
end;

function TMySet.GetItem(Index: Integer): Integer;
begin
  Result:= FItems[Index];
end;

function TMySet.GetCount: Integer;
begin
  Result:= FMaxIdx + 1;
end;

function TMySet.AddVal(Val: Integer): Boolean;
begin
  if not InSet(Val) then
  begin
    Inc(FMaxIdx);
    if FMaxIdx = FCap then
    begin
      if FCap = 0 then FCap:= 10
                  else FCap:= 2 * FCap;
      SetLength(FItems, FCap);
    end;
    FItems[FMaxIdx]:= Val;
    Result:= True;
  end else Result:= False;
end;

procedure TMySet.AddSet(MySet: TMySet);
var
  I: Integer;

begin
  for I:= 0 to MySet.Count - 1 do Self.AddVal(MySet.Item[I]);
end;

function TMySet.InSet(Val: Integer): Boolean;
var
  I: Integer;

begin
  Result:= False;
  for I:= 0 to FMaxIdx do
    if FItems[I] = Val then
    begin
      Result:= True;
      Exit;
    end;
end;

////////////////////////////////////////////////////////////////////////////////
// TMySetList
////////////////////////////////////////////////////////////////////////////////

constructor TMySetList.Create;
begin
  FCap:= 0;
  FMaxIdx:= - 1;
end;

destructor TMySetList.Destroy;
var
  I: Integer;
begin
  for I:= 0 to GetCount - 1 do FItems[I].Free;
  SetLength(FItems, 0);
end;

function TMySetList.GetItem(Index: Integer): TMySet;
begin
  Result:= FItems[Index];
end;

function TMySetList.GetCount: Integer;
begin
  Result:= FMaxIdx + 1;
end;

function TMySetList.AddSet(NewSet: TMySet = nil): Integer;
begin
  Inc(FMaxIdx);
  if FMaxIdx = FCap then
  begin
    if FCap = 0 then FCap:= 10
                else FCap:= 2 * FCap;
    SetLength(FItems, FCap);
  end;

  if Assigned(NewSet) then FItems[FMaxIdx]:= NewSet
                      else FItems[FMaxIdx]:= TMySet.Create;
  Result:= FMaxIdx;
end;

function TMySetList.InSets(Val: Integer; var Idx: Integer): Boolean;
var
  I: Integer;

begin
  for I:= 0 to FMaxIdx do
    if FItems[I].InSet(Val) then
    begin
      Idx:= I;
      Result:= True;
      Exit;
    end;
  Result:= False;
end;

function TMySetList.TestCross(SetList: TMySetList): Boolean;
var
  J, L: Integer;

begin        {
  Result:= False;
  for J:= 0 to GetCount - 1 do
    for L:= 0 to SetList.Count - 1 do
      if Self[J] = SetList[L] then
      begin
        Result:= True;
        Exit;
      end;     }
end;

procedure TMySetList.Delete(Idx: Integer);
var
  I: Integer;

begin
  FItems[Idx].Free;
  for I:= Idx to FMaxIdx - 1 do FItems[I]:= FItems[I + 1];
  Dec(FMaxIdx);
end;

end.

