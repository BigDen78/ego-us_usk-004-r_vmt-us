unit Avk11Engine;

interface

uses
  Types, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Gauges, ExtCtrls, Math, StdCtrls, IniFiles, MyTypes, ComCtrls, ActiveX, ShlObj,
  LanguageUnit, WaitUnit;

const

//------------------------< �������������� ������� ����� >---------------------------------------

  EID_ACLeft =       $80; // ��������� ������������� �������� ����� ���� - ���� ��� ���� �������
  EID_ACRight =      $81; // ��������� ������������� �������� ������ ���� - ���� ��� ���� �������
  EID_HandScan =     $82; // ������
  EID_NewHeader =    $83; // �������������� ��������� ��� �������-14 � �������-15
  EID_Sens =         $90; // ��������� �������� ����������������
  EID_Att =          $91; // ��������� ��������� ���� ����������� (0 �� �������� ����������������)
  EID_VRU =          $92; // ��������� ���
  EID_StStr =        $93; // ��������� ��������� ������ ������
  EID_EndStr =       $94; // ��������� ��������� ����� ������
  EID_HeadPh =       $95; // ������ ���������� ���������
  EID_Mode =         $96; // ��������� ������
 //  EID_ASDLeft =      $97; // ��� ����� ����
 //  EID_ASDRight =     $98; // ��� ������ ����
  EID_2Tp =          $99; // ��������� 2��
  EID_ZondImp =      $9A; // ��������� ������ ������������ ��������
  EID_SetRailType =  $9B; // ��������� �� ��� ������
  EID_2Tp_Word =     $9C; // ��������� 2�� (word)
  EID_Stolb =        $A0; // ������� ����������
  EID_Strelka =      $A1; // ����� ����������� �������� + �����
  EID_DefLabel =     $A2; // ����� ������� + �����
  EID_TextLabel =    $A3; // ������ ������� (���������) + �����
  EID_StBoltStyk =   $A4; // ������� ������ �� + �����
  EID_EndBoltStyk =  $A5; // ���������� ������ �� + �����
  EID_Time =         $A6; // ������� �������
  EID_StLineLabel =  $A7; // ������ ����������� ����� + �����
  EID_EndLineLabel = $A8; // ����� ����������� ����� + �����
  EID_StSwarStyk =   $A9; // ������� ������ ������� ���� + �����
  EID_EndSwarStyk =  $AA; // ���������� ������ ������� ���� + �����
  EID_OpChange =     $AB; // ����� ���������
  EID_PathChange =   $AC; // ����� ������ ����
  EID_MovDirChange = $AD; // ����� ����������� ��������
  EID_SezdNo =       $AE; // ����� ������
  EID_LongLabel =    $AF; // ����������� ����� 2
  EID_GPSCoord =     $C0; // �������������� ����������
  EID_GPSState =     $C1; // ��������� ��������� GPS
  EID_GPSError =     $C2; // ��� ������ �� ���������� ��������� GPS
  EID_BSAmpl =       $C3; // ���� ��������� ��

  EID_Sys =          $E0;
  EID_Sys1 =         $E0;
  EID_Sys2 =         $E1;
  EID_Sys3 =         $E2;
  EID_Sys4 =         $E3;
  EID_Sys5 =         $E4;
  EID_Sys6 =         $E5;
  EID_Sys7 =         $E6;
  EID_Sys8 =         $E7;
  EID_Sys9 =         $E8;
  EID_Sys10 =        $E9;
  EID_Sys11 =        $EA;
  EID_Sys12 =        $EB;
  EID_Sys13 =        $EC;
  EID_Sys14 =        $ED;
  EID_Sys15 =        $EE;
  EID_Sys16 =        $EF;
  EID_SysCrd_SS =    $F1; // ��������� ���������� ��������
  EID_SysCrd_SF =    $F4; // ��������� ���������� ������
  EID_SysCrd_FS =    $F9; // ��������� ���������� ��������
  EID_SysCrd_FF =    $FC; // ��������� ���������� ������
  EID_EndFile =      $FF; // ����� �����

//------------------------< �������������� �������������� ������� >---------------------------------------

  EID_FwdDir =     1; // ������ ������� ������
  EID_LinkPt =     2; // ����� ��� ����� ��������� � ������� � �����
  EID_BwdDir =     3; // ������ ������� �����
  EID_EndBM =      4; //
//  EID_FwdDirEnd =  5; //
//  EID_BwdDirEnd =  6; //

  EID_BadBody  =  1; // �������� ID
  EID_BadEnd   =  2; // �������� ����� �����
  EID_BadLinc  =  3; // �������� ������ � ����������
  EID_BadCoord =  4; // ������ ����������

  ErrorList: array [1..4] of string = ('�������� ID', '�������� ����� �����', '�������� ������ � ����������', '������ ����������');


//------------------------< ��������� ������ >---------------------------------------

var

  Avk11TextEncode:  array [0..2, 0..255] of Char = (('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', //  9
                                                     ' ', '�', '�', '�', '�', '�', '�', '�', '�', '�', // 19
                                                     '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', // 29
                                                     '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', // 39
                                                     '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', // 49
                                                     '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', // 59
                                                     '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', // 69
                                                     '�', '�', '�', '�', '�', '.', '<', '>', '-', 'a', // 79
                                                     ':', '^', '?', 'f', '(', ')', ' ', ' ', ' ', ' ', // 89
                                                     ' ', ' ', ' ', '+', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' '),

                                                    ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                                                     ' ', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
                                                     'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
                                                     'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', 'a', 'b', 'c', 'd', 'e', 'f', 'g',
                                                     'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
                                                     'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', '.', '<', '>', '-', 'a',
                                                     ':', '^', '?', 'f', '(', ')', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', '+', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                     ' ', ' ', ' ', ' ', ' ', ' '),

                                                     ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                                                      ' ', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
                                                      'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
                                                      'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ' ', ' ', ' ',
                                                      ' ', ' ', ' ', 'a', 'b', 'c', 'd', 'e', 'f', 'g',
                                                      'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
                                                      'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', ' ',
                                                      ' ', ' ', ' ', ' ', ' ', '.', '<', '>', '-', 'a',
                                                      ':', '^', '?', 'f', '(', ')', ' ', ' ', ' ', ' ',
                                                      ' ', ' ', ' ', '+', ' ', ' ', ' ', ' ', ' ', ' ',
                                                      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                      ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                                                      ' ', ' ', ' ', ' ', ' ', ' '));

//------------------------< �������� ������� ����������� >---------------------------------------

const

  DBName:  array [0..15] of string =  ('-12', '-10', '-8', '-6', '-4', '-2', '0', '2', '4', '6', '8', '10', '12', '14', '16', '18');
  DBValue: array [0..15] of Integer = ( -12,   -10,   -8,   -6,   -4,   -2,   0,   2,   4,   6,   8,   10,   12,   14,   16,   18);

//------------------------< ��������� ��� ������� ����������� ����� >----------------------------

  CheckSummStartValue = 1539450456;

type

// ----[ ������ ������� ������ ]------------------------------------------------

  mChParam = packed record             //  ������: 3 �����
    ChannelRail: Byte;                 //  EID_Sens    ��������� �������� ����������������
    Value: Byte;                       //  EID_Att     ��������� ��������� ���� ����������� (0 �� �������� ����������������)
    Value2: Byte;
  end;                                 //  EID_VRU     ��������� ���
  pChParam = ^mChParam;                //  EID_StStr   ��������� ��������� ������ ������
                                       //  EID_EndStr  ��������� ��������� ����� ������
                                       //  EID_2Tp     ��������� 2��
                                       //  EID_ZondImp ��������� ������ ������������ ��������

                                       // ������: 24 �����
                                       // EID_HeadPh - ������ ���������� ���������
  mHeadPhones = packed array [r_Left..r_Right, 1..2] of Byte;
  pHeadPhones = ^mHeadPhones;

  mChgMode = Byte;                     // ������: 4 �����
  pChgMode = ^mChgMode;                // EID_Mode - ��������� ������

  mCoord = packed record               // ������: 14 �����
    Km: array [0..1] of Smallint;      // EID_Stolb - ������� ����������
    Pk: array [0..1] of Byte;
    Km_: array [0..1] of Smallint;
    Pk_: array [0..1] of Byte;
    Houer: Byte;
    Minute: Byte;
  end;
  pCoord = ^mCoord;

  mStrelka = packed record             //  ������: 6 �����
    Number: array [0..5] of Byte;      //  EID_Strelka - ����� ����������� �������� + �����
    Number_: array [0..5] of Byte;
    Houer: Byte;
    Minute: Byte;
  end;
  pStrelka = ^mStrelka;

  mDef = packed record                 //  ������: 8 �����
    Rail: Byte;                        //  EID_DefLabel  - ����� ������� + �����
    Name: array [0..4] of Byte;
    Houer: Byte;
    Minute: Byte;
  end;
  pDef = ^mDef;

  mText = packed record                //  ������: 22 �����
    Name: array [0..19] of Byte;       //  EID_SpecLabel - ������ ������� (���������) + �����
    Houer: Byte;
    Minute: Byte;
  end;
  pText = ^mText;

  mTime = packed record                //  ������: 2 �����
    Houer: Byte;                       //  EID_StBoltStyk   - ������� ������ �� + �����
    Minute: Byte;                      //  EID_EndBoltStyk  - ���������� ������ �� + �����
  end;                                 //  EID_Time =       - ������� �������
  pTime = ^mTime;                      //  EID_StLineLabel  - ������ ����������� ����� + �����
                                       //  EID_EndLineLabel - ����� ����������� ����� + �����
                                       //  EID_StSwarStyk   - ������� ������ ������� ���� + �����
                                       //  EID_EndSwarStyk  - ���������� ������ ������� ���� + �����

  mSezdNo = packed record              //  ������: 8 �����
    Name: array [0..9] of Byte;        //  EID_SezdNo - ����� ������
    Name_: array [0..9] of Byte;
    Houer: Byte;
    Minute: Byte;
  end;
  pSezdNo = ^mSezdNo;

  mChgDir = Boolean;
  pChgDir = ^mChgDir;

  mGPSCoord = packed record              //  ������: 8 ����� - �������������� ����������
    Lat: Single;
    Lon: Single;
  end;
  pGPSCoord = ^mGPSCoord;

  mGPSState = packed array [1..2] of Byte;              //  ������: 8 ����� - ��������� ��������� GPS
  pGPSState = ^mGPSState;

  mLongL2 = packed record
    ID: Byte;
    Houer: Byte;
    Minute: Byte;
  end;
  pLongL2 = ^mLongL2;

  mBSAmplHead = packed record             // ������ ��������� ��
    EndSysCrd: LongInt;
    Len: Word;
  end;
  pBSAmplHead = ^mBSAmplHead;

//------------------------< GPS >-----------------------------------------------------------

  TGPSPoint = record
    Lat: Single;
    Lon: Single;
    KM: Integer;
    Metre: Single;
    LabelExists: Boolean;
    LabelText: string;
    StolbExists: Boolean;
    LeftKm: Smallint;
    LeftPk: Byte;
    RightKm: Smallint;
    RightPk: Byte;
  end;

//------------------------< ������ ������� �������� >---------------------------------------

  THSHead = packed record
    Rail: RRail;      // 1-� ���� ���� 0 - ����� ����; 1 ������ ����.
    ScanSurf: Byte;   // 2-� ���� ����� ����������� ������������ ������ (����� �� 0 �� 9)
    Channel: Byte;    // 3-� ���� ����� ������
    Attenuator: ShortInt; // 4-� ���� ����������
    Sens: Byte;       // 5-� ���� �������� �������� ����������������
    VRU: Byte;        // 6-� ���� �������� ���
  end;

  THSItem = packed record
    Sample: Word; // ����� ����� ������������
    Count: Byte; // ���������� ���-�������� (0�3)
    Data: array [0..6] of Byte;
  end;

  THSEcho = packed record
    Delay: array [1..8] of Byte;
    Ampl: array [1..8] of Byte;
    Count: Integer;
    Idx: Integer;
  end;

  THSListItem = record
    HSCh: Integer;
    HSMode: Integer;
    EventIdx: Integer;
    Header: THSHead;
    DisCoord: Integer;
    Samples: array of THSEcho;
  end;

  THSList = array of THSListItem;

//------------------------< ������ ������������ � ����� ����� >---------------------------------------

  TPackDataItem = packed record
    Delay: Byte;
    Ampl: Byte;
  end;

  TPackData = array [r_Left..r_Right] of TPackDataItem;

  TFileEvent = packed record
    ID: Byte;
    OffSet: Integer;
    SysCoord: Integer;
    DisCoord: Integer;
    Pack: TPackData;
  end;

  TFileError = packed record
    Code: Byte;
    Offset: Integer;
    Size: Integer;
    BegSysCoord: Integer;
    EndSysCoord: Integer;
    BegDisCoord: Integer;
    EndDisCoord: Integer;
  end;

  TBScanRect = packed record
    Rail: Byte; // ����: 0 - ����� / 1 - ������
    Ch1: Byte; //       ������: 0 - ��� / 1 - ���������� / 2 - �����������
    Ch2: Byte; //       ������: 0 - ��� / 1 - ���������� / 2 - �����������
    Delay1: Byte;
    Delay2: Byte;
    DisCoord1: Integer;
    DisCoord2: Integer;
  end;

  TBScanRectList = array [1..10] of TBScanRect;

  TFileNotebook_Ver3 = packed record
    ID: Byte;                  // ���: 1 - ��������� �������� / 2 - ������ ��������.
    Rail: Byte;                // ����: 0 - ����� / 1 - ������
    DisCoord: Integer;         // ���������� ����������
    StDisCoord: Integer;       // ���������� ����������
    EdDisCoord: Integer;       // ���������� ����������
    ManName: string [30];      // ������������ ��� (��������� Win)
    Privazka: string [100];    // ��������
    BlokNotText: string [250]; // ����� �������� (��������� win)
    Defect: string [50];       // ������ - ��� / �������� ���
    Prim: string [100];        // ������ - ����������
    DT: TDateTime;
    LeftKm1: Integer;          // ��������
    LeftKm2: Integer;          // ��������
    LeftPk1: Integer;          // �����
    LeftPk2: Integer;          // �����
    RightKm1: Integer;         // ��������
    RightKm2: Integer;         // ��������
    RightPk1: Integer;         // �����
    RightPk2: Integer;         // �����
    Zveno1: Integer;           // �����1
    Zveno2: Integer;           // �����2
    LeftMM: Integer;           // ��������� (�� ������)
    RightMM: Integer;          // ��������� (�� ������)
    Chema: Integer;            // ����� ������: 1 - ������; 2 - ��������; 3 - ������.
    Zoom: Integer;             // �������
    Reduction: Byte;           // ��������: 0 - ��������� / 1 - ��� �1 / 1 - ��� �2
    ViewMode: Byte;            // ����� �����������: 1 - � ��������� / 2 - � ���� ������
    DrawRail: Byte;{TDrawData} // ������������ ���� 0 - ����� 1 - ������ 2 - ���
    AmplTh: Byte;              // ����� �����������
    AmplDon: Byte;             // ��������� ������� ������� 0 - ����
    ViewChannel: Byte;         // ������: 0 - ��� / 1 - ���������� / 2 - �����������
    SelRect: TBScanRectList;
  end;

  TFileNotebook_Ver4 = packed record
    ID: Byte;                  // ���: 1 - ��������� �������� / 2 - ������ ��������.
    Rail: Byte;                // ����: 0 - ����� / 1 - ������
    DisCoord: Integer;         // ���������� ����������
    StDisCoord: Integer;       // ���������� ����������
    EdDisCoord: Integer;       // ���������� ����������
    ManName: string [30];      // ������������ ��� (��������� Win)
    Privazka: string [100];    // ��������
    BlokNotText: string [250]; // ����� �������� (��������� win)
    Defect: string [50];       // ������ - ��� / �������� ���
    Prim: string [100];        // ������ - ����������
    DT: TDateTime;
    LeftKm1: Integer;          // ��������
    LeftKm2: Integer;          // ��������
    LeftPk1: Integer;          // �����
    LeftPk2: Integer;          // �����
    RightKm1: Integer;         // ��������
    RightKm2: Integer;         // ��������
    RightPk1: Integer;         // �����
    RightPk2: Integer;         // �����
    Zveno1: Integer;           // �����1
    SysCoord: Integer;         // �����2
    LeftMM: Integer;           // ��������� (�� ������)
    RightMM: Integer;          // ��������� (�� ������)
    Chema: Integer;            // ����� ������: 1 - ������; 2 - ��������; 3 - ������.
    Zoom: Integer;             // �������
    Reduction: Byte;           // ��������: 0 - ��������� / 1 - ��� �1 / 1 - ��� �2
    ViewMode: Byte;            // ����� �����������: 1 - � ��������� / 2 - � ���� ������
    DrawRail: Byte;{TDrawData} // ������������ ���� 0 - ����� 1 - ������ 2 - ���
    AmplTh: Byte;              // ����� �����������
    AmplDon: Byte;             // ��������� ������� ������� 0 - ����
    ViewChannel: Byte;         // ������: 0 - ��� / 1 - ���������� / 2 - �����������
    SelRect: TBScanRectList;
    PathNum: Integer;          // ����
    Peregon: string [20];      
    FindAt: Integer;           // ��������� ��� ..   0 - ������; 1 - �������; 2 - � ������� �����; 3 - 3-5 ����� ; 4 - ��������
    ViewType: Integer;         // ������ ..          0 - ��� �����������; 1 - ���������� � ����
  end;

  pFileNotebook = ^TFileNotebook_Ver4;

  TFileDecoding = packed record
    Name: array [1..30] of Char; // ������������ ��� (��������� Win)
    DT: TDateTime;
    StartDisCoord: Integer; // ���������� ���������� ������ ��������������� �������
    EndDisCoord: Integer; // ���������� ���������� ����� ��������������� �������
    StartSysCoord: Integer; // ��������� ���������� ������ ��������������� �������
    EndSysCoord: Integer; // ��������� ���������� ����� ��������������� �������
  end;

//  TARItem = TViewZone;

  TRSPInfo = packed record
    ID: Integer;
    Text: string [255];
  end;

  TExHeaderHead = packed record
    FFConst: array [0..13] of Byte;     // ��������� - 14 x 0xFFH
    DataSize: Integer;                  // ������ ���������
    DataVer: Integer;                   // ������ ������ = 5
  end;

  TExHeader_3 = packed record
    FFConst: array [0..13] of Byte;     // ��������� - 14 x 0xFFH
    DataSize: Integer;                  // ������ ���������
    DataVer: Integer;                   // ������ ������ = 3
    Flag: Integer;                      // ����: 0 - ������� ����; 1 - ���� ���� ��������
    EndKM: Integer;                     // �������� ��������
    EndPK: Integer;                     // �������� �����
    EndMM: Integer;                     // ��������� �� ���������� ������
    EndSysCoord: Integer;               // �������� ��������� ����������
    EndDisCoord: Integer;               // �������� ���������� ����������
    CheckSumm: Byte;                    // ����������� �����
    Reserv1: array [1..64] of Byte;     // ������
    ErrorDataVer: Integer;              // ������ ������� ������
    ErrorItemSize: Integer;             // ������ 1 �������� ������� ������ ��������� �����
    ErrorOffset: Integer;               // ������ �� ������� ������ ��������� �����
    ErrorCount: Integer;                // ���������� ��������� ������� ������ ��������� �����
    EventDataVer: Integer;              // ������ ������� ������ ������� ������ ��������� �����
    EventItemSize: Integer;             // ������ 1 �������� ������� ������ ��������� �����
    EventOffset: Integer;               // ������ �� ������� �������� ������� �����
    EventCount: Integer;                // ���������� ��������� ������� �������� ������� �����
    NotebookDataVer: Integer;           // ������ ������� ������ ������� ���������� �������� � ������� ��������
    NotebookItemSize: Integer;          // ������ 1 �������� ������� ���������� �������� � ������� ��������
    NotebookOffset: Integer;            // ������ �� ������� ���������� �������� � ������� ��������
    NotebookCount: Integer;             // ���������� ��������� ������� ���������� �������� � ������� ��������
    DecodingDataVer: Integer;           // ������ ������� ������ ������� ������ �����������
    DecodingItemSize: Integer;          // ������ 1 �������� ������� ������ �����������
    DecodingOffset: Integer;            // ������ �� ������� ������ �����������
    DecodingCount: Integer;             // ���������� ��������� ������� ������ �����������
    Reserv2: array [1..128] of Byte;    // ������
  end;
(*
  TExHeader = packed record
    FFConst: array [0..13] of Byte;     // ��������� - 14 x 0xFFH
    DataSize: Integer;                  // ������ ���������
    DataVer: Integer;                   // ������ ������
    Flag: Integer;                      // ����: 0 - ������� ����; 1 - ���� ���� ��������
    EndKM: Integer;                     // �������� ��������
    EndPK: Integer;                     // �������� �����
    EndMM: Integer;                     // ��������� �� ���������� ������
    EndSysCoord: Integer;               // �������� ��������� ����������
    EndDisCoord: Integer;               // �������� ���������� ����������
    CheckSumm: Byte;                    // ����������� �����
    Reserv1: array [1..64] of Byte;     // ������
    ErrorDataVer: Integer;              // ������ ������� ������
    ErrorItemSize: Integer;             // ������ 1 �������� ������� ������ ��������� �����
    ErrorOffset: Integer;               // ������ �� ������� ������ ��������� �����
    ErrorCount: Integer;                // ���������� ��������� ������� ������ ��������� �����
    EventDataVer: Integer;              // ������ ������� ������ ������� ������ ��������� �����
    EventItemSize: Integer;             // ������ 1 �������� ������� ������ ��������� �����
    EventOffset: Integer;               // ������ �� ������� �������� ������� �����
    EventCount: Integer;                // ���������� ��������� ������� �������� ������� �����
    NotebookDataVer: Integer;           // ������ ������� ������ ������� ���������� �������� � ������� ��������
    NotebookItemSize: Integer;          // ������ 1 �������� ������� ���������� �������� � ������� ��������
    NotebookOffset: Integer;            // ������ �� ������� ���������� �������� � ������� ��������
    NotebookCount: Integer;             // ���������� ��������� ������� ���������� �������� � ������� ��������
    DecodingDataVer: Integer;           // ������ ������� ������ ������� ������ �����������
    DecodingItemSize: Integer;          // ������ 1 �������� ������� ������ �����������
    DecodingOffset: Integer;            // ������ �� ������� ������ �����������
    DecodingCount: Integer;             // ���������� ��������� ������� ������ �����������
    RSPDataVer: Integer;                // ������ ������� ������ ������� ������ ���
    RSPItemSize: Integer;               // ������ 1 �������� ������� ������ ���
    RSPOffset: Integer;                 // ������ �� ������� ������ ���
    RSPCount: Integer;                  // ���������� ��������� ������� ������ ���
    Reserv2: array [1..112] of Byte;    // ������
  end;
 *)

  TExHeader_4 = packed record
    FFConst: array [0..13] of Byte;     // ��������� - 14 x 0xFFH
    DataSize: Integer;                  // ������ ���������
    DataVer: Integer;                   // ������ ������ = 4
    Flag: Integer;                      // ����: 0 - ������� ����; 1 - ���� ���� ��������
    EndKM: Integer;                     // �������� ��������
    EndPK: Integer;                     // �������� �����
    EndMM: Integer;                     // ��������� �� ���������� ������
    EndSysCoord: Integer;               // �������� ��������� ����������
    EndDisCoord: Integer;               // �������� ���������� ����������
    CheckSumm: Byte;                    // ����������� �����
    Reserv1: array [1..64] of Byte;     // ������
    ErrorDataVer: Integer;              // ������ ������� ������
    ErrorItemSize: Integer;             // ������ 1 �������� ������� ������ ��������� �����
    ErrorOffset: Integer;               // ������ �� ������� ������ ��������� �����
    ErrorCount: Integer;                // ���������� ��������� ������� ������ ��������� �����
    EventDataVer: Integer;              // ������ ������� ������ ������� ������ ��������� �����
    EventItemSize: Integer;             // ������ 1 �������� ������� ������ ��������� �����
    EventOffset: Integer;               // ������ �� ������� �������� ������� �����
    EventCount: Integer;                // ���������� ��������� ������� �������� ������� �����
    NotebookDataVer: Integer;           // ������ ������� ������ ������� ���������� �������� � ������� ��������
    NotebookItemSize: Integer;          // ������ 1 �������� ������� ���������� �������� � ������� ��������
    NotebookOffset: Integer;            // ������ �� ������� ���������� �������� � ������� ��������
    NotebookCount: Integer;             // ���������� ��������� ������� ���������� �������� � ������� ��������
    DecodingDataVer: Integer;           // ������ ������� ������ ������� ������ �����������
    DecodingItemSize: Integer;          // ������ 1 �������� ������� ������ �����������
    DecodingOffset: Integer;            // ������ �� ������� ������ �����������
    DecodingCount: Integer;             // ���������� ��������� ������� ������ �����������
    RSPDataVer: Integer;                // ������ ������� ������ ������� ������ ���
    RSPItemSize: Integer;               // ������ 1 �������� ������� ������ ���
    RSPOffset: Integer;                 // ������ �� ������� ������ ���
    RSPCount: Integer;                  // ���������� ��������� ������� ������ ���
    Reserv2: array [1..112] of Byte;    // ������
  end;

  TExHeader_5 = packed record
    FFConst: array [0..13] of Byte;     // ��������� - 14 x 0xFFH
    DataSize: Integer;                  // ������ ���������
    DataVer: Integer;                   // ������ ������ = 5
    Flag: Integer;                      // ����: 0 - ������� ����; 1 - ���� ���� ��������
    EndKM: Integer;                     // �������� ��������
    EndPK: Integer;                     // �������� �����
    EndMM: Integer;                     // ��������� �� ���������� ������
    EndSysCoord: Integer;               // �������� ��������� ����������
    EndDisCoord: Integer;               // �������� ���������� ����������
    CheckSumm: DWord;                   // ����������� �����
    DataRail: RRail;                    // ���� � ������� ���� �������: 0- �����; 1- ������; 2- � �����
    Modify: Byte;                       // = 0 ����������� ������������� ����� ���������� = $FF -������������ �� �������������
    Reserv1: array [1..59] of Byte;     // ������
    ErrorDataVer: Integer;              // ������ ������� ������
    ErrorItemSize: Integer;             // ������ 1 �������� ������� ������ ��������� �����
    ErrorOffset: Integer;               // ������ �� ������� ������ ��������� �����
    ErrorCount: Integer;                // ���������� ��������� ������� ������ ��������� �����
    EventDataVer: Integer;              // ������ ������� ������ ������� ������ ��������� �����
    EventItemSize: Integer;             // ������ 1 �������� ������� ������ ��������� �����
    EventOffset: Integer;               // ������ �� ������� �������� ������� �����
    EventCount: Integer;                // ���������� ��������� ������� �������� ������� �����
    NotebookDataVer: Integer;           // ������ ������� ������ ������� ���������� �������� � ������� ��������
    NotebookItemSize: Integer;          // ������ 1 �������� ������� ���������� �������� � ������� ��������
    NotebookOffset: Integer;            // ������ �� ������� ���������� �������� � ������� ��������
    NotebookCount: Integer;             // ���������� ��������� ������� ���������� �������� � ������� ��������
    DecodingDataVer: Integer;           // ������ ������� ������ ������� ������ �����������
    DecodingItemSize: Integer;          // ������ 1 �������� ������� ������ �����������
    DecodingOffset: Integer;            // ������ �� ������� ������ �����������
    DecodingCount: Integer;             // ���������� ��������� ������� ������ �����������
    RSPDataVer: Integer;                // ������ ������� ������ ������� ������ ���
    RSPItemSize: Integer;               // ������ 1 �������� ������� ������ ���
    RSPOffset: Integer;                 // ������ �� ������� ������ ���
    RSPCount: Integer;                  // ���������� ��������� ������� ������ ���
    UCSDataVer: Integer;                // ������ ������� ������ ������� ��������������������� ��������
    UCSItemSize: Integer;               // ������ 1 �������� ������� ��������������������� ��������
    UCSOffset: Integer;                 // ������ �� ������� ��������������������� ��������
    UCSCount: Integer;                  // ���������� ��������� ������� ��������������������� ��������
    LabelEditDataVer: Integer;          // ������ ������� ������ ������� ��������������������� ��������
    LabelEditItemSize: Integer;         // ������ 1 �������� ������� ��������������������� ��������
    LabelEditOffset: Integer;           // ������ �� ������� ��������������������� ��������
    LabelEditCount: Integer;            // ���������� ��������� ������� ��������������������� ��������
   {
    ValueListDataVer: Integer;          // ������ ������� ������ ������� ��������������������� ��������
    ValueListItemSize: Integer;         // ������ 1 �������� ������� ��������������������� ��������
    ValueListOffset: Integer;           // ������ �� ������� ��������������������� ��������
    ValueListCount: Integer;            // ���������� ��������� ������� ��������������������� ��������
   }
    Reserv2: array [1..80] of Byte;     // ������
  end;

//------------------------< ��������� ������ >---------------------------------------

  TAvk11Header1 = packed record
    ID: Word;        // 0xFF11
    DeviceID: DWord; // ����� ������� 4 �����
    Day: Byte;
    Month: Byte;
    Year: Byte;
    Hour: Byte;
    Minute: Byte;
    RRName: array [1..20] of Byte;
    PChNumber: Byte;
    Operator: array [1..20] of Byte;
    DirCode: DWord;
    SecName: array [1..20] of Byte;
    StartKM: Smallint;
    StartPk: Byte;
    StartMetre: Word;
    EditKM: Smallint;
    EditPk: Byte;
    EditM: Word;
    Path: Byte;
    MoveDir: Byte; //  ����������� �������� 1 ���� 1 - ���� �������� � ������� ���������� ��������� ����������, � - 0, ���� � ������� ����������.
    Zveno: Byte;
    ScanStep: Word;
    Reserv: array [1..33] of Byte;
    TableLink: Cardinal;
  end;

const
  cfEcho = 1;     // 0 ��� - ����������; (1 - ����; 0 - ���)
  cfBSAEcho = 2;  // 1 ��� - ��������� �� (���������); (1-����; 0-���)
  cfChAK = 4;     // 2 ��� - �������� ������������� ��������
  cfBSABlock = 8; // 3 ��� - ��������� �� (�������); (1-����; 0-���)

type

  TAvk11Header2 = packed record
    ID: Word;           // 0xFF12
    UpDeviceID: DWord;  // ����� ������� 4 �����
    DwnDeviceID: DWord; // ����� ������� 4 �����
    Day: Byte;
    Month: Byte;
    Year: Byte;
    Hour: Byte;
    Minute: Byte;
    Name: array [1..38] of Byte;
    Operator: array [1..20] of Byte;
    DirCode: DWord;
    SecName: array [1..20] of Byte;
    StartKM: Smallint;
    StartPk: Byte;
    StartMetre: Smallint;
    BUIVer1: Byte;
    BUIVer2: Byte;
    BUMVer1: Byte;
    BUMVer2: Byte;
    Reserved: Byte; // ����� ������������� 0 -
    Path: Byte;
    MoveDir: Byte; //  ����������� �������� 1 ���� 1 - ���� �������� � ������� ���������� ��������� ����������, � - 0, ���� � ������� ����������.
    Zveno: Byte;
    ScanStep: Word;
    ContentFlg: Byte;
    AmplTH: Byte;
    RailPos: Byte;
    CharSet: Byte;
    CheckSumm: Cardinal;
    Reserv: array [1..4] of Byte;
    TableLink: Cardinal;
  end;

  PAvk11Header2 = ^TAvk11Header2;

//------------------------< Exceptions >----------------------------------------

  EBadID = class(Exception);
  EBadLink = class(Exception);
  EBadEnd = class(Exception);
  EBadCoord = class(Exception);

//------------------------< ������ ������� TAvk11DatSrc >------------------------

  TBMItem = packed record
    StSkipOff: Integer;
    OkOff: Integer;
    Pack: TPackData;
//    StDisCrd: Integer;
//    EnDisCrd: Integer;
  end;

  TDataNotifyEvent = function(StartDisCoord: Integer): Boolean of object;

  TMiasParams = record
    Sens: array [r_Left..r_Right, 0..11] of Integer; // +
    Att: array [r_Left..r_Right, 0..11] of Integer;  // +
    VRU: array [r_Left..r_Right, 0..11] of Integer;  // +
    StStr: array [r_Left..r_Right, 0..9] of Integer;
    EdStr: array [r_Left..r_Right, 0..9] of Integer;
    DwaTp: array [r_Left..r_Right, 0..9] of Integer;
    ZondImp: array [r_Left..r_Right, 0..9] of Integer;
    HeadPh: array [r_Left..r_Right, 0..9] of Boolean;
    Mode: Integer;
  end;

{
  TMiasParams = record
    Sens: array [r_Left..r_Right, 0..9] of Integer;
    Att: array [r_Left..r_Right, 0..9] of Integer;
    VRU: array [r_Left..r_Right, 0..9] of Integer;
    StStr: array [r_Left..r_Right, 0..9] of Integer;
    EdStr: array [r_Left..r_Right, 0..9] of Integer;
    DwaTp: array [r_Left..r_Right, 0..9] of Integer;
    ZondImp: array [r_Left..r_Right, 0..9] of Integer;
    HeadPh: array [r_Left..r_Right, 0..9] of Boolean;
    Mode: Integer;
  end;
}
  TEventData = packed array [1..22] of Byte;

  PEventData = ^TEventData;

  TFileEventData = packed record
    Event: TFileEvent;
    Data: TEventData;
    Params: TMiasParams;
  end;

  TDestDat = packed record
    Delay: array [1..16] of Byte;
    Ampl: array [1..16] of Byte;
    Count: Integer;
    ZerroFlag: Boolean;
  end;

  TimeListItem = record
   H: Integer;
   M: Integer;
   DC: Integer;
  end;


//------------------------------------------------------------------------------
//------------------------[ TStreamTest ]---------------------------------------
//------------------------------------------------------------------------------

  TFileItemGroup = (fi_Signal, fi_Coord, fi_Mode, fi_UnknownID);

  TFileItem = record
    ID: Byte;
    Offset: Integer;
    Group: TFileItemGroup;
    Error: Boolean;
    Data: array of Byte;
  end;

  TStreamTest = class
  private
    FItems: array of TFileItem;
    FCapacity: Integer;
    FCount: Integer;
    FEOF: Boolean;
  protected
    function GetError: Boolean;
    function GetCount: Integer;
    function GetItem(Index: Integer): TFileItem;
  public
    EOFOffset: Integer;

    constructor Create;
    destructor Destroy; override;
    function Test(Dat: TMemoryStream; EndOffset: Integer; SkipFirstCrd: Boolean = False): Boolean;
    procedure Clear;
    procedure MakeLog(var Log: TStringList; SkipPageData: Boolean = False);
    property Items[Index: Integer]: TFileItem read GetItem; default;
    property Count: Integer read GetCount;
    property Error: Boolean read GetError;
    property EOF: Boolean read FEOF;
  end;

//------------------------------------------------------------------------------
//------------------------[ TAvk11DatSrc ]---------------------------------------
//------------------------------------------------------------------------------

  TCurEcho = array [RRail, 0..7] of TDestDat;
  TCurAmpl = array [RRail] of Integer;

  TKMKontentItem = record
    Pk: Integer;
    Len: Integer;
    Idx: Integer;
  end;

  TCoordListItem = record
    KM: Integer;
    Content: array of TKMKontentItem;
  end;

  TCoordList = array of TCoordListItem;

  TBSAmplLinkItem = record
    StDisCrd: Integer;
    EdDisCrd: Integer;
    Len: Integer;
    Offset1: Integer;
    Offset2: Integer;
  end;

  TAvk11DatSrc = class
  private
    FLog: TStringList;                      // ��� ������
    FFileName: string;                      // ��� �����
    FFileBody: TMemoryStream;
    FHeader: TAvk11Header2;
    FExHeader: TExHeader_5;
    FExHdrLoadOk: Boolean;
    FErrors: array of TFileError;
    FEvents: array of TFileEvent;
    FEventsData: array of TFileEventData;
    FSaveEventsData: array of TFileEventData;

    FNotebook: array of TFileNotebook_Ver4;
    FDecoding: array of TFileDecoding;
    FRSPInfo: array of TRSPInfo;
    FTimeList: array of TimeListItem;

    FCoordList: TCoordList;

    FModifyData: Boolean;
    FUnknownFileVersion: Boolean;
    FLeftFlag: Boolean;
    FRightFlag: Boolean;
    FDataSize: Integer;
    FLastErrorIdx: Integer;
    FLastBMIdx: Integer;
    FMaxSysCoord: Integer;
    FCurSysCoord: LongInt;
    FCurDisCoord: Int64;
    FSaveSysCoord: LongInt;
    FSaveDisCoord: Int64;
    FCurParams: TMiasParams;
    FCurOffset: Integer;
    FUnPackFlag: array [RRail] of Boolean;
    FGetBMStateIdx: Integer;
    FGetBMStateState: Boolean;
    FGetParamIdx: Integer;
    FRSPSkipSideRail: Boolean;
    FRSPFile: Boolean;
    FSkipBM: Boolean;
    FCanSkipBM: Boolean;
    FBMItems: array of TBMItem;
                                                   // ����� ��� ������ � ���-���������
    FSrcDat: array [0..7] of Byte;
    FDestDat: array [r_Left..r_Right] of TDestDat;
    FPack: TPackData;                              // ������� ������ ������������ 1 ������
    FCurEcho: TCurEcho;

    FSaveFileDate: TDateTime;
    FSaveFileSize: Integer;
    FReadOnlyFlag: Boolean;

    FObmen2: TPackData;
    FObmen1: Integer;

    FParDat: TMemoryStream;

  protected
    procedure PackReset;
    procedure DataToEcho;
    procedure UnPackEcho;

    procedure LoadExData;
    procedure SaveExData(CutOldExHeader: Boolean);
    procedure AnalyzeFileBody;
    procedure FillEventsData;
    procedure FillTimeList;

    function FindBSState(StartDisCoord: Integer): Boolean;
    procedure TestBMOffsetFirst;
    function TestBMOffsetNext: Boolean;
    procedure SetSkipBM(NewState: Boolean);
    function GetEventCount: Integer;
    function GetEvent(Index: Integer): TFileEventData;
    function GetErrorCount: Integer;
    function GetError(Index: Integer): TFileError;
    function GetNotebookCount: Integer;
    function GetNotebookP(Index: Integer): pFileNotebook;
    function GetFileDecodingCount: Integer;
    function GetFileDecoding(Index: Integer): TFileDecoding;
    function GetRSPInfoCount: Integer;
    function GetRSPInfoByIndex(Index: Integer): TRSPInfo;
    function GetRSPInfoByID(ID: Integer): TRSPInfo;
    function GetMaxDisCoord: Integer;
    function GetFUnPackFlag(Index: RRail): Boolean;
    function NormalizeDisCoord(Src: Integer): Integer;
    function GetBodyModify: Boolean;
    procedure SetBodyModify(NewState: Boolean);

  public
    UnpackBottom: Boolean;
    FCDList: array of Integer;
    FBSAmplLink: array of TBSAmplLinkItem;
    LoadProg: TGauge;
    HSList: THSList;
    DelayKoeff: Integer;
    BackMotion: Boolean;
    CurAK: array [RRail, 1..8] of Boolean;
    GPSPoints: array of TGPSPoint;
    StartShift: Integer;
    LabelList: TIntegerDynArray;
    SkipTestDateTime: Boolean;
//    Test: Integer;
    CurCrdLongLinc: Boolean;
    CurAmpl: TCurAmpl;

    function TestOffsetFirst: Boolean; // <--- �������� ���������� �� protected
    function TestOffsetNext: Boolean;  // <--- �������� ���������� �� protected

    constructor Create;
    destructor Destroy; override;
    function  LoadFromFile(FileName: string): Boolean;
    procedure CloseFile;
    procedure ReAnalyze;
    function  TestCheckSumm: Boolean;

    procedure LoadData(StartDisCoord, EndDisCoord, LoadShift: LongInt; BlockOk: TDataNotifyEvent);
    function SideToRail(Side: TSide): RRail;
    function SysToDisCoord(Coord: Integer): Integer;
    function DisToSysCoord(Coord: Integer): Integer;
    procedure DisToDisCoords(Coord: Integer; var Res: TIntegerDynArray);
    function DisToRealCoord(Coord: LongInt): TRealCoord;
    function GetSysCoordByRealIdx(KmIdx, PkIdx: Integer; Metr: Extended): Integer;
    function GetSysCoordByReal(Km, Pk: Integer; Metr: Extended): Integer;
    procedure DisToFileOffset(NeedDisCoord: Integer; var DisCoord: Int64; var SysCoord: Integer; var Offset: Integer);
    function  GetBMStateFirst(DisCoord: Integer): Boolean;
    function  GetBMStateNext(var DisCoord: Integer; var State: Boolean): Boolean;
    function  GetParamFirst(DisCoord: Integer; var Params: TMiasParams): Boolean;
    function  GetEventsData(Index: Integer): TFileEventData;
    function  GetParamNext(DisCoord: Integer; var Params: TMiasParams): Boolean;
    function  GetCoord(DisCoord: Integer; var CoordParams: TCoordParams): Boolean;
    function  GetTime(DisCoord: Integer): string;
    function  GetSpeed(DisCoord: Integer; var Speed: Single): string;
    function  NotebookAdd(NewDat: pFileNotebook = nil): Integer;
    procedure NotebookDelete(Index: Integer);
    procedure CutExHeader;
    procedure SetScanStep(New: Word);
    function GetEventIdx(StartDisCoord, EndDisCoord: Integer; var StartIdx, EndIdx: Integer; EmptyPar: Integer): Boolean;
    procedure GetNearestEventIdx(DisCoord: Integer; var LeftIdx, RightIdx: Integer; var SameCoord: TIntegerDynArray);
    function GetLeftEventIdx(DisCoord: Integer): Integer;
    function GetRightEventIdx(DisCoord: Integer): Integer;
    procedure GetEventData(Idx: Integer; var ID: Byte; var pData: PEventData);
    procedure AddRSPInfo(ID: Integer; Text: string);
    function  GetRSPTextByID(ID: Integer; var Res: Boolean): string;
    procedure GenerateGPSPoints;
    procedure SaveNB;
    procedure TestCanSkipBM;
    function ValueCount(Group: Byte): Integer;
    procedure GetValue(Group: Byte; Index: Integer; var ValueId, Value: Variant);
    procedure SavePiece(StDisCood, EdDisCood: Integer; SaveNB: Boolean; FileName: string);
    function GetTimeList(Index: Integer): TimeListItem;
    function GetTimeListCount: Integer;
    procedure FixShift(Body: TMemoryStream; EndOffset: Integer = MaxInt - 1);
    procedure SetBit(Body: TMemoryStream; Offset: Integer; State: Boolean);
    function FixBags: Boolean;

    property EventsData[Index: Integer]: TFileEventData read GetEventsData;
    property TimeList[Index: Integer]: TimeListItem read GetTimeList;
    property TimeListCount: Integer read GetTimeListCount;

    property RSPSkipSideRail: Boolean read FRSPSkipSideRail write FRSPSkipSideRail;
    property DataRail: RRail read FExHeader.DataRail;
    property FileName: string read FFileName;
    property FileBody: TMemoryStream read FFileBody;
    property Header: TAvk11Header2 read FHeader write FHeader;
    property ExHeader: TExHeader_5 read FExHeader;
    property ModifyExData: Boolean read FModifyData write FModifyData;
    property MaxDisCoord: Integer read GetMaxDisCoord;
    property EventCount: Integer read GetEventCount;
    property Event[Index: Integer]: TFileEventData read GetEvent;
    property ErrorCount: Integer read GetErrorCount;
    property Error[Index: Integer]: TFileError read GetError;
    property NotebookCount: Integer read GetNotebookCount;
    property Notebook[Index: Integer]: pFileNotebook read GetNotebookP;
    property FileDecodingCount: Integer read GetFileDecodingCount;
    property FileDecoding[Index: Integer]: TFileDecoding read GetFileDecoding;
    property RSPInfoCount: Integer read GetRSPInfoCount;
    property RSPInfoByIdx[Index: Integer]: TRSPInfo read GetRSPInfoByIndex;
    property RSPInfoById[Index: Integer]: TRSPInfo read GetRSPInfoByID;
    property Log: TStringList read FLog;
    property UnknownFileVersion: Boolean read FUnknownFileVersion;
    property RSPFile: Boolean read FRSPFile;
    property SkipBackMotion: Boolean read FSkipBM write SetSkipBM;
    property CurOffset: Integer read FCurOffset;
    property CurSysCoord: LongInt read FCurSysCoord;
    property CurDisCoord: Int64 read FCurDisCoord write FCurDisCoord;
    property CurParams: TMiasParams read FCurParams;
    property CurEcho: TCurEcho read FCurEcho;
    property UnPackFlag[Index: RRail]: Boolean read GetFUnPackFlag;
    property CoordList: TCoordList read FCoordList;
    property BodyModify: Boolean read GetBodyModify write SetBodyModify;
  end;

//------------------------------------------------------------------------------
//------------------------[ TAvk11DatDst ]--------------------------------------
//------------------------------------------------------------------------------

  THandScanData = record
    HSHead: THSHead;
    HSItem: array of THSItem;
  end;
{
  TLinc = record
    DisCoord: Integer;
    Offset: Integer;
  end;
}
  TAvk11DatDst = class
  private
    FID: Byte;
    FLastSaveOffset: Integer;
    FFirstCoord: Boolean;
    FData: TMemoryStream;
    FFileName: string;
    FCurDisCrd: Integer;
    FLastSysCrd: Integer;
    FPack: TPackData;
    FPackSysCoord: array [0..1] of Integer;
    FCh1Flag: array [0..1] of Boolean;
    FCurHour: Integer;
    FCurMinute: Integer;

  protected
    function DataToID(Rail, Ch: Byte): Byte;
  public
    Header: TAvk11Header2;
    CurSysCrd: Integer;

    constructor Create;
    destructor  Destroy; override;
    procedure CreateFile(FileName: string; Size: Integer);
    procedure CloseFile;
    procedure DeleteFile;

    procedure FillHeader(DateTime: TDateTime; Org, OpName, Section: string);
    procedure AddHeader;
    procedure ClearEcho;
    procedure AddSysCoord(SysCoord: Integer);
    procedure SetTime(Time: TTime);
    procedure AddEcho(Rail, Ch, Count: Byte; D1, A1, D2, A2, D3, A3, D4, A4, D5, A5, D6, A6, D7, A7, D8, A8: Byte);
    procedure AddSens(Rail, Ch: Byte; NewValue: ShortInt); // ��������� �������� ����������������
    procedure AddAtt(Rail, Ch, NewValue: Byte); // ��������� ��������� ���� ����������� (0 �� �������� ����������������)
    procedure AddVRU(Rail, Ch, NewValue: Byte); // ��������� ���
    procedure AddStStr(Rail, Ch, NewValue: Byte); // ��������� ��������� ������ ������
    procedure AddEndStr(Rail, Ch, NewValue: Byte); // ��������� ��������� ����� ������
    procedure Add2Tp(Rail, Ch, NewValue: Byte); // ��������� 2��
    procedure AddHandScan(Dat: THandScanData);
    procedure AddTextLabel(Text: string);
    procedure AddAK(Rail: Byte; Ch1, Ch2, Ch3, Ch4, Ch5, Ch6, Ch7, Ch8: Boolean); // ��������� AK
    procedure AddHeadPh(R0_Ch0, R0_Ch1, R0_Ch2, R0_Ch3, R0_Ch4, R0_Ch5, R0_Ch6, R0_Ch7, R0_Ch8, R0_Ch9,
                        R1_Ch0, R1_Ch1, R1_Ch2, R1_Ch3, R1_Ch4, R1_Ch5, R1_Ch6, R1_Ch7, R1_Ch8, R1_Ch9: Boolean);
    procedure AddMode(ModeIdx: Byte);
    procedure AddZondImp(Rail, Ch, Val: Byte);
    procedure AddRailType;
    procedure AddStolb(Km0, Km1: Smallint; Pk0, Pk1: Byte);
    procedure AddStrelka(Text: string);
    procedure AddDefLabel(Rail: Byte; Text: string);
    procedure AddStBoltStyk;
    procedure AddEdBoltStyk;
    procedure AddTime(Haur, Minute: Byte);
    procedure AddCurTime;
    procedure AddStLineLabel;
    procedure AddEdLineLabel;
    procedure AddStSwarStyk;
    procedure AddEdSwarStyk;
    procedure AddOpChange(NewOpName: string);
    procedure AddPathChange(NewPath: Byte);
    procedure AddMovDirChange(NewDir: Byte);
    procedure AddSezdNo(Text: string);
    procedure AddLongLabel(Id: Byte; Start: Boolean); // ����������� ����� 2 (Start = False - ���� �����) 
    property DataStream: TMemoryStream read FData;
  end;

//------------------------------------------------------------------------------

  EMyStreamExcept = class(Exception);

  TMyStream = class
  private
    FPtr: Pointer;
    FSize: Longint;
    FCap: Longint;
    FFlg: array [1..2] of Boolean;

  protected
  public
    Position: Longint;
    Position1: Longint;
    Position2: Longint;

    constructor Create;
    destructor Destroy; override;

//    procedure SaveToFile(FileName: string);
    procedure WriteBuffer(const Buffer; Count: Longint);
    procedure ReadBuffer(var Buffer; Count: Longint);
    procedure ReadBuffer1(var Buffer; Count: Longint);
    procedure ReadBuffer2(var Buffer; Count: Longint);

    property Size: Longint read FSize;
    property Memory: Pointer read FPtr;
  end;

//------------------------------------------------------------------------------

  TContEvent = packed record
    ID: Byte;
    OffSet: Integer;
    SysCoord: Integer;
    DisCoord: Integer;
    Pack: TPackData;
    Data: TEventData;
    Params: TMiasParams;
  end;

  TMyArray = class
  private
    FPtr: array of Pointer;
    FItemSize: Longint;
    FAddIdx: Longint;
    FCount: Integer;
    FCreateVirtItem: Boolean;

  protected
    function GetPtr(Index: Integer): TContEvent;

  public
    VirtItem: TContEvent;

    constructor Create(CreateVirtualItem: Boolean);
    destructor Destroy; override;
    function Add(const Buffer): Integer;
    property Item[Index: Integer]: TContEvent read GetPtr; default;
    property Count: LongInt read FCount;
//    property VirtualItem: TContEvent read FVirtItem write FVirtItem;
  end;

//------------------------------------------------------------------------------
//---------------------[ TAvk11DataContainer ]----------------------------------
//------------------------------------------------------------------------------

  TEchoBlock = array[0..7] of
  record
    T: Byte;
    A: Byte;
  end;

  TValueType = (vtInt, vtText, vtDate, vtDateTime, vtTime, vtFloat);

  TAvikonFileType = (aftAvikon11,   // ������������ �������
                     aftAvikon12,   // ������������ �������
                     aftAvikon14,   // ������������ ������� � ��������� ����������
                     aftAvikon15,   // ������
                     aftAvikon16);  // EGO US / IPV

  EDataContExcept = class(Exception);

  TAvk11DataContainer = class
  private
    FID: Byte;
    FLastSaveOffset: Integer;
    FFirstCoord: Boolean;

    FFileName: string;
    FCurDisCrd: Integer;
    FCurSysCrd: Integer;
    FLastSysCrd: Integer;
    FSavePack: TPackData;
    FPackSysCoord: array [0..1] of Integer;
    FCh1Flag: array [0..1] of Boolean;
    FCurHour: Integer;
    FCurMinute: Integer;
    FLastSave: Integer;
    FLeftFlag: Boolean;
    FRightFlag: Boolean;
    FCurParam: TMiasParams;
    FMaxReadOffset: Integer;
    FEventsCount: Integer;
    FLastKm: Integer;
    FLastPk: Integer;
    FLastStolb: mCoord;
    FLastStolbSysCrd: Integer;
    FExHeader: TExHeader_5;
    FParDat: TMemoryStream;

    FOffset: array [1..2] of Integer;
    FEvents: TMyArray;
    FCurParams: TMiasParams;
    FTimeList: array of TimeListItem;
    FCoordList: TCoordList;
    FBackMotionFlag: Boolean;
    FSearchBMEnd: Boolean;
    FStartBMSysCoord: Integer;
    FLastDisCrd: Integer;
    FMaxSysCoord: Integer;
    FMaxDisCoord: Integer;
    FCDList: array of Integer;
    FOldH: Integer;
    FOldM: Integer;
    FRSPSkipSideRail: Boolean;
    FCurDisCoord: Int64;
    FCurSysCoord: Integer;
    FCurOffset: Integer;
    FUnPackFlag: array [RRail] of Boolean;
    BackMotion: Boolean;
    FSaveDisCoord: Integer;
    FSaveSysCoord: Integer;
    FGetBMStateIdx: Integer;
    FGetBMStateState: Boolean;
    FGetParamIdx: Integer;
    FFileType: TAvikonFileType;
    FCh1Echo: array [0..1] of Boolean;
                                                   // ����� ��� ������ � ���-���������
    FSrcDat: array [0..7] of Byte;
    FDestDat: array [r_Left..r_Right] of TDestDat;
    FLoadPack: TPackData;                        // ������� ������ ������������ 1 ������
    FCurEcho: TCurEcho;
    FCurAK: array [RRail, 1..8] of Boolean;
    FStolbCount: Integer;
    FHSItem: array of THSItem;
    FDataRail: Byte;
    FDataCh: Byte;
    FHSSrcData: TAvk11DataContainer;

  protected
    function DataToID(Rail, Ch: Byte): Byte;
    procedure AddEvent(ID: Byte; SysCoord: Integer = - MaxInt; DisCoord: Integer = - MaxInt);

    procedure LoadPackReset;
    procedure DataToEcho;
    procedure UnPackEcho;
    function GetEventCount: Integer;
    function GetEvent(Index: Integer): TContEvent;
    function GetFUnPackFlag(Index: RRail): Boolean;
    function NormalizeDisCoord(Src: Integer): Integer;
    procedure SaveData(TestLen: Boolean); // ������ ������� �����
    function GetMaxDisCoord: Integer;
    function GetMaxSysCoord: Integer;

  public
    FData: TMyStream;
    Header: TAvk11Header2;
    CurAmpl: TCurAmpl;

    constructor Create(FileType: TAvikonFileType; FileName: string);
    destructor Destroy; override;

    procedure FillHeader(DateTime: TDateTime; CharSet: Integer; Org, OpName, Section: string);
    procedure AddHeader;
    procedure AddValueList;
    procedure ClearEcho;
    procedure AddSysCoord(SysCoord: Integer);
    procedure SetTime(Time: TTime);
    procedure AddEcho(Rail, Ch, Count: Byte; D1, A1, D2, A2, D3, A3, D4, A4, D5, A5, D6, A6, D7, A7, D8, A8: Byte); overload;
    procedure AddEcho(Rail, Ch, Count: Byte; EchoBlock: TEchoBlock ); overload;
//    procedure AddBSAmpl(Rail, Data: Byte);
    procedure AddSens(Rail, Ch: Byte; NewValue: ShortInt); // ��������� �������� ����������������
    procedure AddAtt(Rail, Ch, NewValue: Byte); // ��������� ��������� ���� ����������� (0 �� �������� ����������������)
    procedure AddVRU(Rail, Ch, NewValue: Byte); // ��������� ���
    procedure AddStStr(Rail, Ch, NewValue: Byte); // ��������� ��������� ������ ������
    procedure AddEndStr(Rail, Ch, NewValue: Byte); // ��������� ��������� ����� ������
    procedure Add2Tp(Rail, Ch, NewValue: Byte); // ��������� 2�� (byte)
    procedure Add2Tp_Word(Rail, Ch, NewValue: Word); // ��������� 2�� (word)
    procedure AddHandScan(Dat: THandScanData);
    function DataTransfer(StartDisCoord: Integer): Boolean;
    procedure AddHandScanFromDataContainer(SrcData: TAvk11DataContainer; HSHead: THSHead; DataRail, DataCh: Byte; StartCrd, EndCrd: Integer);

    procedure AddTextLabel(Text: string);
    procedure AddAK(Rail: Byte; Ch1, Ch2, Ch3, Ch4, Ch5, Ch6, Ch7, Ch8: Boolean); // ��������� AK
    procedure AddHeadPh(R0_Ch0, R0_Ch1, R0_Ch2, R0_Ch3, R0_Ch4, R0_Ch5, R0_Ch6, R0_Ch7, R0_Ch8, R0_Ch9,
                      R1_Ch0, R1_Ch1, R1_Ch2, R1_Ch3, R1_Ch4, R1_Ch5, R1_Ch6, R1_Ch7, R1_Ch8, R1_Ch9: Boolean);
    procedure AddMode(ModeIdx: Byte);
    procedure AddZondImp(Rail, Ch, Val: Byte);
    procedure AddRailType;
    procedure AddStolb(Km0, Km1: Smallint; Pk0, Pk1: Byte);
    procedure AddStrelka(Text: string);
    procedure AddDefLabel(Rail: Byte; Text: string);
    procedure AddStBoltStyk;
    procedure AddEdBoltStyk;
    procedure AddTime(Haur, Minute: Byte);
    procedure AddCurTime;
    procedure AddStLineLabel;
    procedure AddEdLineLabel;
    procedure AddStSwarStyk;
    procedure AddEdSwarStyk;
    procedure AddOpChange(NewOpName: string);
    procedure AddPathChange(NewPath: Byte);
    procedure AddMovDirChange(NewDir: Byte);
    procedure AddSezdNo(Text: string);
    procedure AddLongLabel(Id: Byte; Start: Boolean); // ����������� ����� 2
    procedure AddBSAmpl(Len: Word; var R0Data, R1Data: array of Byte); // ������ ��������� ��
    procedure AddValue(Group: Byte; ValueTyp: TValueType; ValueId: Byte; Value: Variant); overload;
    procedure AddValue(Group: Byte; ValueTyp: TValueType; ValueId: string; Value: Variant); overload;
    function ValueCount(Group: Byte): Integer;
    procedure GetValue(Group: Byte; Index: Integer; var ValueId, Value: Variant);

    procedure LoadData(StartDisCoord, EndDisCoord, LoadShift: Integer; BlockOk: TDataNotifyEvent);
    function SideToRail(Side: TSide): RRail;
    function SysToDisCoord(Coord: Integer): Integer;
    function DisToSysCoord(Coord: Integer): Integer;
    procedure DisToDisCoords(Coord: Integer; var Res: TIntegerDynArray);
    function DisToRealCoord(Coord: LongInt): TRealCoord;
    procedure DisToFileOffset(NeedDisCoord: Integer; var DisCoord: Int64; var SysCoord: Integer; var Offset: Integer);
    function  GetBMStateFirst(DisCoord: Integer): Boolean;
    function  GetBMStateNext(var DisCoord: Integer; var State: Boolean): Boolean;
    function  GetParamFirst(DisCoord: Integer; var Params: TMiasParams): Boolean;
    function  GetParamNext(DisCoord: Integer; var Params: TMiasParams): Boolean;
    function  GetCoord(DisCoord: Integer; var CoordParams: TCoordParams): Boolean;
    function  GetTime(DisCoord: Integer): string;
    function  GetSpeed(DisCoord: Integer): string;
    function GetEventIdx(StartDisCoord, EndDisCoord: Integer; var StartIdx, EndIdx: Integer; EmptyPar: Integer): Boolean;
    procedure GetNearestEventIdx(DisCoord: Integer; var LeftIdx, RightIdx: Integer; var SameCoord: TIntegerDynArray);
    function GetLeftEventIdx(DisCoord: Integer): Integer;
    function GetRightEventIdx(DisCoord: Integer): Integer;
    procedure GetEventData(Idx: Integer; var ID: Byte; var pData: PEventData);
    

    property MaxDisCoord: Integer read GetMaxDisCoord;
    property MaxSysCoord: Integer read GetMaxSysCoord;
    property EventCount: Integer read GetEventCount;
    property Event[Index: Integer]: TContEvent read GetEvent;
    property CurSysCoord: LongInt read FCurSysCrd;
    property CurDisCoord: Integer read FCurDisCrd;

    property CurLoadOffset: Integer read FCurOffset;
    property CurLoadSysCoord: LongInt read FCurSysCoord;
    property CurLoadDisCoord: Int64 read FCurDisCoord;

    property CurLoadParams: TMiasParams read FCurParams;
    property CurLoadEcho: TCurEcho read FCurEcho;
    property LoadUnPackFlag[Index: RRail]: Boolean read GetFUnPackFlag;
  end;

const

  GE: Integer = 0;

  Avk11EchoCnt: array [0..11] of Integer = (0, - 1, 1, 2, - 1, 3, 4,  - 1, 5, 6, - 1, 7);
  Avk11EchoLen: array [0..7] of Integer = (0, 2, 3, 5, 6, 8, 9, 11);
  Avk11EventLen: array [$80..$FF] of Integer = (     1,   //  $80 EID_ACLeft        ��������� ������������� �������� ����� ���� - ���� ��� ���� �������
                                                     1,   //  $81 EID_ACRight       ��������� ������������� �������� ������ ���� - ���� ��� ���� �������
                                                   - 1,   //  $82 EID_HandScan      ������ ������������
                                                   - 1,   //  $83 EID_NewHeader
                                                   - 1,   //  $84
                                                   - 1,   //  $85
                                                   - 1,   //  $86
                                                   - 1,   //  $87
                                                   - 1,   //  $88
                                                   - 1,   //  $89
                                                   - 1,   //  $8A
                                                   - 1,   //  $8B
                                                   - 1,   //  $8C
                                                   - 1,   //  $8D
                                                   - 1,   //  $8E
                                                   - 1,   //  $8F
                                                     2,   //  $90 EID_Sens          ��������� �������� ����������������
                                                     2,   //  $91 EID_Att           ��������� ��������� ���� ����������� (0 �� �������� ����������������)
                                                     2,   //  $92 EID_VRU           ��������� ���
                                                     2,   //  $93 EID_StStr         ��������� ��������� ������ ������
                                                     2,   //  $94 EID_EndStr        ��������� ��������� ����� ������
                                                     4,   //  $95 EID_HeadPh        ������ ���������� ���������
                                                     1,   //  $96 EID_Mode          ��������� ������
                                                     1,   //  $97 EID_ASDLeft       ��� ����� ����
                                                     1,   //  $98 EID_ASDRight      ��� ������ ����
                                                     2,   //  $99 EID_2Tp           ��������� 2��
                                                     2,   //  $9A EID_ZondImp       ��������� ������ ������������ ��������
                                                     0,   //  $9B EID_SetRailType
                                                     3,   //  $9C EID_2Tp_Word      ��������� 2�� (Word)
                                                   - 1,   //  $9D
                                                   - 1,   //  $9E
                                                   - 1,   //  $9F
                                                    14,   //  $A0 EID_Stolb         ������� ����������
                                                    14,   //  $A1 EID_Strelka       ����� ����������� �������� + �����
                                                     8,   //  $A2 EID_DefLabel      ����� ������� + �����
                                                    22,   //  $A3 EID_SpecLabel     ������ ������� (���������) + �����
                                                     2,   //  $A4 EID_StBoltStyk    ������� ������ �� + �����
                                                     2,   //  $A5 EID_EndBoltStyk   ���������� ������ �� + �����
                                                     2,   //  $A6 EID_Time =        ������� �������
                                                     2,   //  $A7 EID_StLineLabel   ������ ����������� ����� + �����
                                                     2,   //  $A8 EID_EndLineLabel  ����� ����������� ����� + �����
                                                     2,   //  $A9 EID_StSwarStyk    ������� ������ ������� ���� + �����
                                                     2,   //  $AA EID_EndSwarStyk   ���������� ������ ������� ���� + �����
                                                    20,   //  $AB EID_OpChange      ����� ���������
                                                     1,   //  $AC EID_PathChange    ����� ������ ����
                                                     1,   //  $AD EID_MovDirChange  ����� ����������� ��������
                                                    22,   //  $AE EID_SezdNo        ����� ������
                                                     3,   //  $AF EID_LongLabel     ����������� ����� 2
                                                   - 1,   //  $B0
                                                   - 1,   //  $B1
                                                   - 1,   //  $B2
                                                   - 1,   //  $B3
                                                   - 1,   //  $B4
                                                   - 1,   //  $B5
                                                   - 1,   //  $B6
                                                   - 1,   //  $B7
                                                   - 1,   //  $B8
                                                   - 1,   //  $B9
                                                   - 1,   //  $BA
                                                   - 1,   //  $BB
                                                   - 1,   //  $BC
                                                   - 1,   //  $BD
                                                   - 1,   //  $BE
                                                   - 1,   //  $BF
                                                     8,   //  $C0 EID_GPCoord  �������������� ����������
                                                     2,   //  $C1 EID_GPSState ��������� ��������� GPS
                                                     0,   //  $C2 EID_GPSEror
                                                   - 1,   //  $C3 EID_BSAmpl
                                                   - 1,   //  $C4
                                                   - 1,   //  $C5
                                                   - 1,   //  $C6
                                                   - 1,   //  $C7
                                                   - 1,   //  $C8
                                                   - 1,   //  $C9
                                                   - 1,   //  $CA
                                                   - 1,   //  $CB
                                                   - 1,   //  $CC
                                                   - 1,   //  $CD
                                                   - 1,   //  $CE
                                                   - 1,   //  $CF
                                                   - 1,   //  $D0
                                                   - 1,   //  $D1
                                                   - 1,   //  $D2
                                                   - 1,   //  $D3
                                                   - 1,   //  $D4
                                                   - 1,   //  $D5
                                                   - 1,   //  $D6
                                                   - 1,   //  $D7
                                                   - 1,   //  $D8
                                                   - 1,   //  $D9
                                                   - 1,   //  $DA
                                                   - 1,   //  $DB
                                                   - 1,   //  $DC
                                                   - 1,   //  $DD
                                                   - 1,   //  $DE
                                                   - 1,   //  $DF
                                                     9,   //  $E0 EID_Sys1
                                                     9,   //  $E1 EID_Sys2
                                                     9,   //  $E2 EID_Sys3
                                                     9,   //  $E3 EID_Sys4
                                                     9,   //  $E4 EID_Sys5
                                                     9,   //  $E5 EID_Sys6
                                                     9,   //  $E6 EID_Sys7
                                                     9,   //  $E7 EID_Sys8
                                                     9,   //  $E8 EID_Sys9
                                                     9,   //  $E9 EID_Sys10
                                                     9,   //  $EA EID_Sys11
                                                     9,   //  $EB EID_Sys12
                                                     9,   //  $EC EID_Sys13
                                                     9,   //  $ED EID_Sys14
                                                     9,   //  $EE EID_Sys15
                                                     9,   //  $EF EID_Sys16
                                                   - 1,   //  $F0
                                                     2,   //  $F1 ��������� ���������� ��������   EID_SysCrd_SS
                                                   - 1,   //  $F2
                                                   - 1,   //  $F3
                                                     5,   //  $F4 ��������� ���������� ������     EID_SysCrd_SF
                                                   - 1,   //  $F5
                                                   - 1,   //  $F6
                                                   - 1,   //  $F7
                                                   - 1,   //  $F8
                                                     5,   //  $F9 ��������� ���������� ��������   EID_SysCrd_FS
                                                   - 1,   //  $FA
                                                   - 1,   //  $FB
                                                     8,   //  $FC ��������� ���������� ������     EID_SysCrd_FF
                                                   - 1,   //  $FD
                                                   - 1,   //  $FE
                                                    13);  //  $FF ����� �����                     EID_EndFile

const

  idRSP_Control     = 1;                                                       //    ��������
  idRSP_ContYear    = 2;    // �� ������������ !!!                             //    ��� ��������
  idRSP_RelsNo      = 3;                                                       //    �����
  idRSP_RelsHeight  = 4;                                                       //    ������
  idRSP_RelsLength  = 5;                                                       //    �����
  idRSP_Nit         = 6;                                                       //    ����
  idRSP_Tverd       = 7;                                                       //    ���������
  idRSP_Master      = 8;                                                       //    ������
  idRSP_Oper        = 9;                                                       //    ��������
  idRSP_GrGodn      = 10;                                                      //    ������ ��������
  idRSP_DefCode     = 11;                                                      //    �������
  idRSP_PartNo      = 12;                                                      //    ������
  idRSP_TestDate    = 13;                                                      //    ����
  idRSP_TestNo      = 14;                                                      //    ����� �������
  idRSP_BUM_Serial  = 15;                                                      //    ����� ������������
  idRSP_TestPribor  = 16;                                                      //    �������� ������������
  idRSP_GrGodnV     = 17;                                                      //    ������ ����. �� ����������� ��������
  idRSP_GrGodnD     = 18;                                                      //    ������ ����. �� �������������������
  idRSP_DefCount    = 19;                                                      //    ���������� �������� � ���������
  idRSP_TestPlet    = 20;                                                      //    ������ ����������� �����
  idRSP_RelsHeight2 = 21;                                                      //    ������ ������ ����� ���������
  idRSP_Brak        = 22;                                                      //    ������� �����

  RSPParName: array [1..22] of string = ('��������',
                                         '��� ��������',
                                         '�����',
                                         '������',
                                         '�����',
                                         '����',
                                         '���������',
                                         '������',
                                         '��������',
                                         '������ ��������',
                                         '�������',
                                         '������',
                                         '����',
                                         '����� �������',
                                         '����� ������������',
                                         '�������� ������������',
                                         '������ ����. �� ����������� ��������',
                                         '������ ����. �� �������������������',
                                         '���������� �������� � ���������',
                                         '������ ����������� �����',
                                         '������ ������ ����� ���������',
                                         '������� �����');
{
var
  EchoPixel: TEchoPixel;
}
function ToCoordRec(Km, Pk, Mm: Integer): TRealCoord;
//function RealCoordToText(Data: TRealCoord; Index: Integer) : string;
procedure ScanDir(StartDir: string; Mask: string; List: TStrings);
function CodeToText(Src: array of Byte; CharSetNo: Integer): string;
procedure TextToCode(Src: string; Len: Integer;  CharSetNo: Integer; var Res: array of Byte);
function CodeToDate(Day, Month, Year: Byte) : string;
function CodeToTime(Hour, Minute: Byte) : string;
function MySelectDirectory(const Caption: string; const Root: WideString; var Directory: string): Boolean;
function Id_To_EventName(ID: Byte; Mode: Boolean; Short: Boolean = False) : string;

implementation
{
uses
  MainUnit, DisplayUnit;
}

var
  GlobCnt: Integer = 0;

procedure ToLog(S: string);
begin
//  MainForm.Memo.Lines.Add(S);
//  TestUnit4Form.Memo1.Lines.Add(S);
end;

procedure ClearLog;
begin
//  MainForm.Memo.Lines.Clear;
//  TestUnit4Form.Memo1.Clear;
end;

function ToCoordRec(Km, Pk, Mm: Integer): TRealCoord;
begin
  Result.Km:= Km;
  Result.Pk:= Pk;
  Result.Mm:= Mm;
end;
{
function RealCoordToText(Data: TRealCoord; Index: Integer) : string;
var
  M, MM: Integer;

begin
  M:= Round(1000 * Frac(Data.MM div 1000 / 1000));
  MM:= Round(1000 * Frac(Data.MM / 1000));

  case Index of
    0: Result:= Format('%D ' + Language.GetCaption(0000001) + ' %D ' + Language.GetCaption(0000002) + ' %D ' + Language.GetCaption(0000003) + ' %D ' + Language.GetCaption(0000013), [Data.Km, Data.Pk, M, MM]);
    1: Result:= Format('%D ' + Language.GetCaption(0000001) + ' %D ' + Language.GetCaption(0000002) + ' %D ' + Language.GetCaption(0000003) + ' %D ' + Language.GetCaption(0000014), [Data.Km, Data.Pk, M, MM div 10]);
    2: Result:= Format('%D ' + Language.GetCaption(0000001) + ' %D ' + Language.GetCaption(0000002) + ' %D ' + Language.GetCaption(0000003) + '', [Data.Km, Data.Pk, M]);
  end;
end;
}
function SelectDirCB(Wnd: HWND; uMsg: UINT; lParam, lpData: LPARAM): Integer stdcall;
begin
  if (uMsg = BFFM_INITIALIZED) and (lpData <> 0) then
    SendMessage(Wnd, BFFM_SETSELECTION, Integer(True), lpdata);
  result := 0;
end;

function MySelectDirectory(const Caption: string; const Root: WideString; var Directory: string): Boolean;
var
  WindowList: Pointer;
  BrowseInfo: TBrowseInfo;
  Buffer: PChar;
  OldErrorMode: Cardinal;
  RootItemIDList, ItemIDList: PItemIDList;
  ShellMalloc: IMalloc;
  IDesktopFolder: IShellFolder;
  Eaten, Flags: LongWord;

begin
  Result := False;
  if not DirectoryExists(Directory) then
    Directory := '';
  FillChar(BrowseInfo, SizeOf(BrowseInfo), 0);
  if (ShGetMalloc(ShellMalloc) = S_OK) and (ShellMalloc <> nil) then
  begin
    Buffer := ShellMalloc.Alloc(MAX_PATH);
    try
      RootItemIDList := nil;
      if Root <> '' then
      begin
        SHGetDesktopFolder(IDesktopFolder);
        IDesktopFolder.ParseDisplayName(Application.Handle, nil,
          POleStr(Root), Eaten, RootItemIDList, Flags);
      end;
      with BrowseInfo do
      begin
        hwndOwner := Application.Handle;
        pidlRoot := RootItemIDList;
        pszDisplayName := Buffer;
        lpszTitle := PChar(Caption);
        ulFlags := BIF_RETURNONLYFSDIRS or BIF_NEWDIALOGSTYLE;
        if Directory <> '' then
        begin
          lpfn := SelectDirCB;
          lParam := Integer(PChar(Directory));
        end;
      end;
      WindowList := DisableTaskWindows(0);
      OldErrorMode := SetErrorMode(SEM_FAILCRITICALERRORS);
      try
        ItemIDList := ShBrowseForFolder(BrowseInfo);
      finally
        SetErrorMode(OldErrorMode);
        EnableTaskWindows(WindowList);
      end;
      Result :=  ItemIDList <> nil;
      if Result then
      begin
        ShGetPathFromIDList(ItemIDList, Buffer);
        ShellMalloc.Free(ItemIDList);
        Directory := Buffer;
      end;
    finally
      ShellMalloc.Free(Buffer);
    end;
  end;
end;

procedure ScanDir(StartDir: string; Mask: string; List: TStrings);
var
  SearchRec: TSearchRec;

begin
  if Mask = '' then Mask := '*.*';
  if StartDir[Length(StartDir)] <> '\' then
  StartDir := StartDir + '\';
  if FindFirst(StartDir + Mask, faAnyFile, SearchRec) = 0 then
  begin
    repeat
    //  Application.ProcessMessages;
      if (SearchRec.Attr and faDirectory) <> faDirectory
        then List.AddObject(StartDir + SearchRec.Name, Pointer(SearchRec.Size))
        else if (SearchRec.Name <> '..') and (SearchRec.Name <> '.') then
        begin
          List.AddObject(StartDir + SearchRec.Name + '\', Pointer(0));
          ScanDir(StartDir + SearchRec.Name + '\', Mask, List);
        end;
    until FindNext(SearchRec) <> 0;
    FindClose(SearchRec);
  end;
end;

function CodeToText(Src: array of Byte; CharSetNo: Integer) : string;
var
  S: string;
  I: Integer;

begin
  if CharSetNo > 2 then CharSetNo:= 0;
  S:= '';
  for I:= 0 to High(Src) do
    if Src[I] <> $FF then S:= S + Avk11TextEncode[CharSetNo, Src[I]]
                     else Break;
  for I:= Length(S) downto 1 do
    if S[I] <> ' ' then
    begin
      Delete(S, I + 1, Length(S));
      Break;
    end;
  Result:= S;
end;

procedure TextToCode(Src: string; Len: Integer; CharSetNo: Integer; var Res: array of Byte);
var
  I: Integer;
  J: Integer;
  K: Integer;

begin
  if CharSetNo > 2 then CharSetNo:= 0; 
  FillChar(Res[0], Len, $FF);
  K:= 0;
  for I:= 1 to Length(Src) do
    for J:= 0 to 255 do
      if Avk11TextEncode[CharSetNo, J] = Src[I] then
      begin
        Res[K]:= J;
        Inc(K);
        if K > High(Res) then Exit;
        Break;
      end;
end;

function XXNumToCode(Value: Integer) : Byte;
var
  S: string;
  
begin
  Result:= 0;
  S:= IntToStr(Value);
  case S[Length(S)] of
   '0': Result:= 0;
   '1': Result:= 1;
   '2': Result:= 2;
   '3': Result:= 3;
   '4': Result:= 4;
   '5': Result:= 5;
   '6': Result:= 6;
   '7': Result:= 7;
   '8': Result:= 8;
   '9': Result:= 9;
  end;

  if Length(S) > 1 then
    case S[Length(S) - 1] of
     '0': Result:= Result or (0 shl 4);
     '1': Result:= Result or (1 shl 4);
     '2': Result:= Result or (2 shl 4);
     '3': Result:= Result or (3 shl 4);
     '4': Result:= Result or (4 shl 4);
     '5': Result:= Result or (5 shl 4);
     '6': Result:= Result or (6 shl 4);
     '7': Result:= Result or (7 shl 4);
     '8': Result:= Result or (8 shl 4);
     '9': Result:= Result or (9 shl 4);
    end;
end;

function CodeToDate(Day, Month, Year: Byte) : string;
var
  D, M: string;
  I: Integer;

begin
  D:= Format('%d', [(Day   shr 4 and $0F) * 10 + (Day   and $0F)]);
  M:= Format('%d', [(Month shr 4 and $0F) * 10 + (Month and $0F)]);
  if Length(D) = 1 then D:= '0' + D;
  if Length(M) = 1 then M:= '0' + M;
  Result:= D + ':' + M + Format(':%d', [(Year  shr 4 and $0F) * 10 + (Year  and $0F + 2000)]);
end;

function CodeToTime(Hour, Minute: Byte) : string;
var
  H, M: string;
  I: Integer;

begin
  H:= Format('%d', [(Hour   shr 4 and $0F) * 10 + (Hour   and $0F)]);
  M:= Format('%d', [(Minute shr 4 and $0F) * 10 + (Minute and $0F)]);
  if Length(H) = 1 then H:= '0' + H;
  if Length(M) = 1 then M:= '0' + M;
  Result:= H + ':' + M;
end;

function Id_To_EventName(ID: Byte; Mode: Boolean; Short: Boolean = False) : string;
begin
  if not Short then
  begin
    if Mode then
    begin
      if ID in [$00..$7F] then Result:= '�������'; //  - ��������������
    end
    else
    begin
      case ID of
        1: Result:= '[i] ��������� ����������� ��������';
        2: Result:= '[i] ����� �������� ����������';
      end;
    end;
    case ID of
           $80: Result:= '��������� ������������� �������� ���� ������� ����� ����';
           $81: Result:= '��������� ������������� �������� ���� ������� ������ ����';
           $82: Result:= '������ ��������';
           $83: Result:= '����������� ���������';
           $90: Result:= '��������� �������� ����������������';
           $91: Result:= '��������� ��������� ���� ����������� (0 �� �������� ����������������)';
           $92: Result:= '��������� ���';
           $93: Result:= '��������� ��������� ������ ������';
           $94: Result:= '��������� ��������� ����� ������';
           $95: Result:= '������ ���������� ���������';
           $96: Result:= '��������� ������';
           $97: Result:= '������������ ��� ����� ����';
           $98: Result:= '������������ ��� ������ ����';
           $99: Result:= '��������� 2��';
           $9A: Result:= '��������� ������ ������������ ��������';
           $9C: Result:= '��������� 2�� (�����)';
           $A0: Result:= '������� �������� ����������';
           $A1: Result:= '����� ����������� ��������';
           $A2: Result:= '����� �������';
           $A3: Result:= '������ ������� (���������)';
           $A4: Result:= '������� ������ ��';
           $A5: Result:= '���������� ������ ��';
           $A6: Result:= '������� �������';
           $A7: Result:= '������ ����������� �����';
           $A8: Result:= '����� ����������� �����';
           $A9: Result:= '������� ������ ������� ����';
           $AA: Result:= '���������� ������ ������� ����';
           $AB: Result:= '����� ���������';
           $AC: Result:= '����� ������ ����';
           $AD: Result:= '����� ����������� ��������';
           $AE: Result:= '����� ������';
           $AF: Result:= '����������� �����';
           $C0: Result:= '�������������� ����������';
           $C1: Result:= '��������� ��������� GPS';
           $C2: Result:= '��� ������ �� ������ GPS ';
           $C3: Result:= '��������� ��';
      $E0..$EF: Result:= '��������� ���������;';
           $F1: Result:= '�������� ��������� ���������� � �������� �������';
           $F4: Result:= '������ ��������� ���������� � �������� �������';
           $F9: Result:= '�������� ��������� ���������� � ������ �������';
           $FC: Result:= '������ ��������� ���������� � ������ �������';
           $FF: Result:= '����� �����';
    end;
  end
  else
  begin
    case ID of
             1: Result:= '��� ���� ������';
             2: Result:= '����� �����';
             3: Result:= '��� ���� �����';
             4: Result:= '����� ���� ���� �����';
             5: Result:= '��� ���� ������';
             6: Result:= '��� ���� �����';
           $80: Result:= '��-�';
           $81: Result:= '��-�';
           $82: Result:= '������';
           $83: Result:= '����������� ���������';
           $90: Result:= '��';
           $91: Result:= '���';
           $92: Result:= '���';
           $93: Result:= '���';
           $94: Result:= '���';
           $95: Result:= '����';
           $96: Result:= '���';
           $97: Result:= '���';
           $98: Result:= '���';
           $99: Result:= '2��';
           $9A: Result:= '�������';
           $9C: Result:= '2�� (�����)';
           $A0: Result:= '�����';
           $A1: Result:= '��';
           $A2: Result:= '���';
           $A3: Result:= '�����';
           $A4: Result:= '+��';
           $A5: Result:= '-��';
           $A6: Result:= '���';
           $A7: Result:= '+�';
           $A8: Result:= '-�';
           $A9: Result:= '+��';
           $AA: Result:= '-��';
           $AB: Result:= '�������';
           $AC: Result:= '�������';
           $AD: Result:= '�����������';
           $AE: Result:= '�����';
           $AF: Result:= '�����';
           $C0: Result:= 'GPS Coord';
           $C1: Result:= 'GPS State';
           $C2: Result:= 'GPS Error';
           $C3: Result:= '����. ��';
      $E0..$EF: Result:= 'SYS';
           $F1: Result:= '�������� ��������� ���������� � �������� �������';
           $F4: Result:= '������ ��������� ���������� � �������� �������';
           $F9: Result:= '�������� ��������� ���������� � ������ �������';
           $FC: Result:= '������ ��������� ���������� � ������ �������';
           $FF: Result:= '����� �����';
    end;
  end;
end;

//------------------------------------------------------------------------------
//------------------------[ TStreamTest ]---------------------------------------
//------------------------------------------------------------------------------

constructor TStreamTest.Create;
begin
  FEOF:= False;
  EOFOffset:= - 1;
//  FError:= False;
end;

destructor TStreamTest.Destroy;
begin
  SetLength(FItems, 0);
end;

procedure TStreamTest.Clear;
begin
  FEOF:= False;
//  FError:= False;
  SetLength(FItems, 0);
end;

function TStreamTest.GetItem(Index: Integer): TFileItem;
begin
  Result:= FItems[Index];
end;

function TStreamTest.GetCount: Integer;
begin
  Result:= Length(FItems);
end;

function TStreamTest.GetError: Boolean;
var
  I: Integer;

begin
  Result:= False;
  for I:= 0 to High(FItems) do
    if FItems[I].Error then
    begin
      Result:= True;
      Exit;
    end;
end;

procedure TStreamTest.MakeLog(var Log: TStringList; SkipPageData: Boolean = False);
const
  Names: array [TFileItemGroup] of string = ('�������', '����������', '�����', '���������� ID');

var
  I, J: Integer;

function IdToName(ID: Byte): string;
begin
  case ID of
         $80: Result:= '������������ ������� ����� ����';
         $81: Result:= '������������ ������� ������ ����';
         $82: Result:= '������ ��������';
         $83: Result:= '����������� ���������';
         $90: Result:= '�������� ����������������';
         $91: Result:= '��������� ���� �����������';
         $92: Result:= '���';
         $93: Result:= '������ ������';
         $94: Result:= '����� ������';
         $95: Result:= '���������� ��������';
         $96: Result:= '��������� ������';
         $97: Result:= '��� ����� ����';
         $98: Result:= '��� ������ ����';
         $99: Result:= '2��';
         $9A: Result:= '������� ������������ ��������';
         $9C: Result:= '2�� (word)';
         $A0: Result:= '������� �������� ����������';
         $A1: Result:= '����� ����������� ��������';
         $A2: Result:= '����� �������';
         $A3: Result:= '������ ������� (���������)';
         $A4: Result:= '������� ������ ��';
         $A5: Result:= '���������� ������ ��';
         $A6: Result:= '������� �������';
         $A7: Result:= '������ ����������� �����';
         $A8: Result:= '����� ����������� �����';
         $A9: Result:= '������� ������ ������� ����';
         $AA: Result:= '���������� ������ ������� ����';
         $AB: Result:= '����� ���������';
         $AC: Result:= '����� ������ ����';
         $AD: Result:= '����� ����������� ��������';
         $AE: Result:= '����� ������';
         $AF: Result:= '����������� �����';
         $C0: Result:= '�������������� ����������';
         $C1: Result:= '��������� ��������� GPS';
         $C2: Result:= '��� ������ �� ������ GPS ';
    $E0..$EF: Result:= '��������� ���������';
         $F1: Result:= '�������� ���������� � �������� �������';
         $F4: Result:= '������ ���������� � �������� �������';
         $F9: Result:= '�������� ���������� � ������ �������';
         $FC: Result:= '������ ���������� � ������ �������';
         $FF: Result:= '����� �����';
    else Result:= '';
  end;
end;

begin
  if Assigned(Log) then // Log.Clear
                   else Log:= TStringList.Create;
  for I:= 0 to Count - 1 do
  begin
    if FItems[I].Error then
    begin
      Log.Add('');
      Log.Add('---- ������ ----');
      Log.Add('');
    end;

    if SkipPageData then
    begin
      Log.Add(Format('%4x - %2x - %s (%s) ', [FItems[I].Offset, FItems[I].ID, Names[FItems[I].Group], IdToName(FItems[I].ID)]) );
      for J:= 0 to High(FItems[I].Data) do
        Log.Add(Format('%4x - %2x - %s', [FItems[I].Offset + J + 1, FItems[I].Data[J], '������']));
    end
    else
    begin
      Log.Add(Format('%4d (%4d) %4x - %2x - %s (%s) ', [Trunc(Items[I].Offset / 1046), Round(1046 * Frac(Items[I].Offset / 1046)), FItems[I].Offset, FItems[I].ID, Names[FItems[I].Group], IdToName(FItems[I].ID)]) );
      for J:= 0 to High(FItems[I].Data) do
        Log.Add(Format('%4d (%4d) %4x - %2x - %s', [Trunc((Items[I].Offset + J + 1) / 1046), Round(1046 * Frac((Items[I].Offset + J + 1) / 1046)), FItems[I].Offset + J + 1, FItems[I].Data[J], '������']));
    end;
  end;
end;

function TStreamTest.Test(Dat: TMemoryStream; EndOffset: Integer; SkipFirstCrd: Boolean = False): Boolean;
label
  Stop;

var
  I, J: Integer;
  LoadID: Byte;
  SkipBytes: Integer;
  New: ^TFileItem;
  Count: Byte;
  Sample: Word;
  Temp: array of Byte;
  DatSize: Integer;

function GetLongInt(P: Pointer): LongInt;
type
  PLInt = ^LongInt;

begin
  Result:= PLInt(P)^;
end;

function TestOffset(Pos: Integer): Boolean;
var
  I: Integer;
  ID: Byte;

begin
  Result:= True;
  if Pos < 0 then Exit;
  I:= Dat.Position;
  Dat.Position:= Pos;
  Dat.ReadBuffer(ID, 1);
  Dat.Position:= I;
  Result:= ID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF];
end;

begin
  FCapacity:= 1000;
  FCount:= 0;
  SetLength(FItems, FCapacity);

  if EndOffset > Dat.Size - 1 then DatSize:= Dat.Size
                              else DatSize:= EndOffset + 1;
  FEOF:= False;
  repeat
    if Dat.Position + 1 > DatSize then goto Stop;
    Dat.ReadBuffer(LoadID, 1);              // �������� �������������� �������
//    SetLength(FItems, Length(FItems) + 1);
//    New:= @FItems[High(FItems)];
    Inc(FCount);
    if FCount = FCapacity then
    begin
      FCapacity:= FCapacity * 2;
      SetLength(FItems, FCapacity);
    end;
    New:= @FItems[FCount - 1];

    New^.ID:= LoadID;
    New^.Error:= False;
    New^.Offset:= Dat.Position - 1;
    if LoadID and 128 = 0 then       // �������
    begin
      New^.Group:= fi_Signal;
      if (LoadID shr 3 and $07 <> 0) and (LoadID and $07 < 5) then  // �������� ������� ������� � ���������� ��������
      begin
        SkipBytes:= Avk11EchoLen[LoadID and $07];
        if SkipBytes <> 0 then
        begin
          SetLength(New^.Data, SkipBytes);
          if Dat.Position + SkipBytes > DatSize then goto Stop;
          Dat.ReadBuffer(New^.Data[0], SkipBytes); // ������ ��������
        end;
      end
      else        // (��� ������ � 0 ������) ��� (���������� �����������  > 4)
      begin
        New^.Error:= True;
        Break;
      end;
    end
    else                   // ��������� �������
    begin
      case LoadID of
           EID_HandScan: begin
                           New^.Group:= fi_Signal;
                           SetLength(New^.Data, 6);
                           if Dat.Position + 6 > DatSize then goto Stop;
                           Dat.ReadBuffer(New^.Data[0], 6);
                           repeat
                             SetLength(New^.Data, Length(New^.Data) + 3);
                             if Dat.Position + 3 > DatSize then goto Stop;
                             Dat.ReadBuffer(New^.Data[Length(New^.Data) - 3], 3);
                             Sample:= New^.Data[Length(New^.Data) - 3] + New^.Data[Length(New^.Data) - 2] * $FF;
                             Count:= New^.Data[Length(New^.Data) - 1];
                             if Count > 4 then
                             begin
                               New^.Error:= True;
                               Break;
                             end;
                             SkipBytes:= Avk11EchoLen[Count];
                             if SkipBytes <> 0 then
                             begin
                               SetLength(New^.Data, Length(New^.Data) + SkipBytes);
                               if Dat.Position + SkipBytes > DatSize then goto Stop;
                               Dat.ReadBuffer(New^.Data[Length(New^.Data) - SkipBytes], SkipBytes);
                             end;
                           until Sample = $FFFF;
                         end;

    EID_Sens        ,   // [$90] ��������� �������� ����������������
    EID_Att         ,   // [$91] ��������� ��������� ���� ����������� (0 �� �������� ����������������)
    EID_VRU         ,   // [$92] ��������� ���
    EID_StStr       ,   // [$93] ��������� ��������� ������ ������
    EID_EndStr      ,   // [$94] ��������� ��������� ����� ������
    EID_HeadPh      ,   // [$95] ������ ���������� ���������
    EID_Mode        ,   // [$96] ��������� ������
    EID_2Tp         ,   // [$99] ��������� 2��
    EID_ZondImp     ,   // [$9A] ��������� ������ ������������ ��������
    EID_SetRailType ,   // [$9B] ��������� �� ��� ������
    EID_2Tp_Word    ,   // [$9C] ��������� 2�� (word)
    EID_Stolb       ,   // [$A0] ������� ����������
    EID_Strelka     ,   // [$A1] ����� ����������� �������� + �����
    EID_DefLabel    ,   // [$A2] ����� ������� + �����
    EID_TextLabel   ,   // [$A3] ������ ������� (���������) + �����
    EID_StBoltStyk  ,   // [$A4] ������� ������ �� + �����
    EID_EndBoltStyk ,   // [$A5] ���������� ������ �� + �����
    EID_Time        ,   // [$A6] ������� �������
    EID_StLineLabel ,   // [$A7] ������ ����������� ����� + �����
    EID_EndLineLabel,   // [$A8] ����� ����������� ����� + �����
    EID_StSwarStyk  ,   // [$A9] ������� ������ ������� ���� + �����
    EID_EndSwarStyk ,   // [$AA] ���������� ������ ������� ���� + �����
    EID_OpChange    ,   // [$AB] ����� ���������
    EID_PathChange  ,   // [$AC] ����� ������ ����
    EID_MovDirChange,   // [$AD] ����� ����������� ��������
    EID_SezdNo      ,   // [$AE] ����� ������
    EID_Sys1..EID_Sys16:
                         begin
                           New^.Group:= fi_Mode;
                           SkipBytes:= Avk11EventLen[LoadID];
                           if SkipBytes <> 0 then
                           begin
                             SetLength(New^.Data, SkipBytes);
                             if Dat.Position + SkipBytes > DatSize then goto Stop;
                             Dat.ReadBuffer(New^.Data[0], SkipBytes);
                           end;
                         end;
          EID_SysCrd_SS: begin // ��������� �������� ������, �������� ����������
                           New^.Group:= fi_Coord;
                           SetLength(New^.Data, 2);
                           if Dat.Position + 2 > DatSize then goto Stop;
                           Dat.ReadBuffer(New^.Data[0], 2);
                           if not SkipFirstCrd then
                           begin
                             New^.Error:= not TestOffset(Dat.Position - 3 - New^.Data[0]);
                             if New^.Error then Break;
                           end;
                           SkipFirstCrd:= False;
                         end;
          EID_SysCrd_SF: begin // ��������� �������� ������, ������ ����������
                           New^.Group:= fi_Coord;
                           SetLength(New^.Data, 5);
                           if Dat.Position + 5 > DatSize then goto Stop;
                           Dat.ReadBuffer(New^.Data[0], 5);
                           if not SkipFirstCrd then
                           begin
                             New^.Error:= not TestOffset(Dat.Position - 6 - New^.Data[0]);
                             if New^.Error then Break;
                           end;
                           SkipFirstCrd:= False;
                         end;
          EID_SysCrd_FS: begin // ��������� ������ ������, �������� ����������
                           New^.Group:= fi_Coord;
                           SetLength(New^.Data, 5);
                           if Dat.Position + 5 > DatSize then goto Stop;
                           Dat.ReadBuffer(New^.Data[0], 5);
                           if not SkipFirstCrd then
                           begin
                             New^.Error:= not TestOffset(Dat.Position - 6 - GetLongInt(@New^.Data[0]));
                             if New^.Error then Break;
                           end;
                           SkipFirstCrd:= False;
                         end;
          EID_SysCrd_FF: begin // ��������� ������ ������, ������ ����������
                           New^.Group:= fi_Coord;
                           SetLength(New^.Data, 10);
                           if Dat.Position + 10 > DatSize then goto Stop;
                           Dat.ReadBuffer(New^.Data[0], 10);
                           if not SkipFirstCrd then
                           begin
                             New^.Error:= not TestOffset(Dat.Position - 11 - GetLongInt(@New^.Data[0]));
                             if New^.Error then Break;
                           end;
                           SkipFirstCrd:= False;
                         end;
            EID_EndFile: begin // ����� �����
                           New^.Group:= fi_Mode;
                           SetLength(New^.Data, 13);
                           Dat.ReadBuffer(New^.Data[0], 13);
                           EOFOffset:= Dat.Position - 1;
                           for J:= 5 to 11 do
                             if New^.Data[J] <> $FF then
                             begin
                               New^.Error:= True;
                             end;
                           Break;
                         end

        else
        begin // �������� (������) ID �������
              // �� ����������� � ������ ������ ����� - ������������� ����
              // EID_NewHeader - [$83] ����������� ��������� � ���� �����
              // EID_ACLeft    - [$80] ��������� ������������� �������� ����� ���� - ���� ��� ���� �������
              // EID_ACRight   - [$81] ��������� ������������� �������� ������ ���� - ���� ��� ���� �������
              // EID_LongLabel - [$AF] ����������� ����� 2
              // EID_GPSCoord  - [$C0] �������������� ����������
              // EID_GPSState  - [$C1] ��������� ��������� GPS
              // EID_GPSError  - [$C2] ��� ������ �� ���������� ��������� GPS


          New^.Error:= True;
          New^.Group:= fi_UnknownID;
          Break;
        end;
      end;
    end;
  until Dat.Position + 1 > DatSize;
  SetLength(FItems, FCount);
  Exit;

Stop:
  SetLength(FItems, FCount);
  FEOF:= True;
end;

//------------------------< TAvk11DatSrc >---------------------------------------

constructor TAvk11DatSrc.Create;
begin
  UnpackBottom:= True;
  SkipTestDateTime:= False;
  FSkipBM:= False;
  StartShift:= 0;
  FLog:= TStringList.Create;
  FRSPSkipSideRail:= False;
  FRSPFile:= False;
  FParDat:= TMemoryStream.Create;
end;

destructor TAvk11DatSrc.Destroy;
var
  I: Integer;

begin
  if FSkipBM then SetSkipBM(False);
  if FModifyData then SaveExData(FExHdrLoadOk);

  if Assigned(FLog) then
  begin
    FLog:= nil;
    FLog.Free;
  end;

  if Assigned(FFileBody) then
  begin
    FFileBody.Free;
    FFileBody:= nil;
  end;

  SetLength(FErrors, 0);
  SetLength(FEvents, 0);
  SetLength(FNotebook, 0);
  SetLength(FDecoding, 0);
  SetLength(FEventsData, 0);
  SetLength(FRSPInfo, 0);
  SetLength(FCDList, 0);
  SetLength(FTimeList, 0);
  SetLength(GPSPoints, 0);
  SetLength(FSaveEventsData, 0);
  SetLength(FBMItems, 0);
  for I:= 0 to High(FCoordList) do SetLength(FCoordList[I].Content, 0);
  SetLength(FCoordList, 0);
  FParDat.Free;
end;

//------------------------< ������������ �������� ������ >---------------------

function TAvk11DatSrc.GetEventCount: Integer;
begin
  Result:= Length(FEventsData);
end;

function TAvk11DatSrc.GetEvent(Index: Integer): TFileEventData;
begin
  if (Index >= 0) and (Index < Length(FEventsData)) then Result:= FEventsData[Index];
end;

function TAvk11DatSrc.GetErrorCount: Integer;
begin
  Result:= Length(FErrors);
end;

function TAvk11DatSrc.GetError(Index: Integer): TFileError;
begin
  Result:= FErrors[Index];
end;

function TAvk11DatSrc.GetNotebookCount: Integer;
begin
  Result:= Length(FNotebook);
end;

function TAvk11DatSrc.GetNotebookP(Index: Integer): pFileNotebook;
begin
  Result:= @FNotebook[Index];
end;

function TAvk11DatSrc.GetFileDecodingCount: Integer;
begin
  Result:= Length(FDecoding);
end;

function TAvk11DatSrc.GetFileDecoding(Index: Integer): TFileDecoding;
begin
  Result:= FDecoding[Index];
end;

function TAvk11DatSrc.GetRSPInfoCount: Integer;
begin
  Result:= Length(FRSPInfo);
end;

function TAvk11DatSrc.GetRSPInfoByIndex(Index: Integer): TRSPInfo;
begin
  Result:= FRSPInfo[Index];
end;

function TAvk11DatSrc.GetRSPInfoByID(ID: Integer): TRSPInfo;
var
  I: Integer;

begin
  for I:= 0 to High(FRSPInfo) do
    if FRSPInfo[I].ID = ID then
    begin
      Result:= FRSPInfo[I];
      Exit;
    end;
end;

function TAvk11DatSrc.GetRSPTextByID(ID: Integer; var Res: Boolean): string;
var
  I: Integer;

begin
  Res:= False;
  for I:= 0 to High(FRSPInfo) do
    if FRSPInfo[I].ID = ID then
    begin
      Result:= FRSPInfo[I].Text;
      Res:= True;
      Exit;
    end;
end;

procedure TAvk11DatSrc.AddRSPInfo(ID: Integer; Text: string);
begin
  FModifyData:= True;
  SetLength(FRSPInfo, Length(FRSPInfo) + 1);
  FRSPInfo[High(FRSPInfo)].ID:= ID;
  FRSPInfo[High(FRSPInfo)].Text:= Text;
end;

function TAvk11DatSrc.NotebookAdd(NewDat: pFileNotebook = nil): Integer;
var
  I: Integer;

begin
  FModifyData:= True;
  I:= Length(FNotebook);
  SetLength(FNotebook, I + 1);
  if Assigned(NewDat) then FNotebook[I]:= NewDat^;
  Result:= I;
end;

procedure TAvk11DatSrc.NotebookDelete(Index: Integer);
var
  I: Integer;

begin
  FModifyData:= True;
  for I:= Index to High(FNotebook) - 1 do FNotebook[I]:= FNotebook[I + 1];
  SetLength(FNotebook, Length(FNotebook) - 1);
end;

function TAvk11DatSrc.GetMaxDisCoord: Integer;
begin
  if FSkipBM then Result:= FMaxSysCoord
             else Result:= FExHeader.EndDisCoord;
end;

function TAvk11DatSrc.GetFUnPackFlag(Index: RRail): Boolean;
begin
  Result:= FUnPackFlag[Index];
end;

//------------------------< ������ � ����������� ���������� >---------------------

procedure TAvk11DatSrc.LoadExData;
var
  I: Integer;
  ExHeadOk: Boolean;
  NeedReAnalyze: Boolean;
  Ver4: TExHeader_4;
  Ver3: TExHeader_3;
  Head: TExHeaderHead;
  DataSize: Integer;

  TmpNbk: array of TFileNotebook_Ver3;

begin
  NeedReAnalyze:= False;
  ExHeadOk:= (FHeader.TableLink <> $FFFFFFFF);
  FUnknownFileVersion:= False;

  if ExHeadOk then
    try
      FFileBody.Position:= FHeader.TableLink;
      FFileBody.ReadBuffer(Head, SizeOf(Head));
    except
      FLog.Add(Language.GetCaption(0150011));
      ExHeadOk:= False;
    end;

  if ExHeadOk then
    for I:= 0 to 13 do
      if Head.FFConst[I] <> $FF then
      begin
        FLog.Add(Language.GetCaption(0150011));
        ExHeadOk:= False;
        Break;
      end;

  if ExHeadOk then
  begin
    FDataSize:= FHeader.TableLink;

    case Head.DataVer of

      3: begin // �� ������ �����������, � ���� �����������������
           try
             FFileBody.Position:= FHeader.TableLink;
             FFileBody.ReadBuffer(Ver3, SizeOf(TExHeader_3));
           except
             FLog.Add(Language.GetCaption(0150011));
             ExHeadOk:= False;
           end;

           FExHeader.NotebookDataVer  := Ver3.NotebookDataVer  ;
           FExHeader.NotebookItemSize := Ver3.NotebookItemSize ;
           FExHeader.NotebookOffset   := Ver3.NotebookOffset   ;
           FExHeader.NotebookCount    := Ver3.NotebookCount    ;
           FExHeader.DecodingDataVer  := Ver3.DecodingDataVer  ;
           FExHeader.DecodingItemSize := Ver3.DecodingItemSize ;
           FExHeader.DecodingOffset   := Ver3.DecodingOffset   ;
           FExHeader.DecodingCount    := Ver3.DecodingCount    ;
           FExHeader.RSPCount         := 0                     ;
           NeedReAnalyze:= True;
         end;

      4: begin // �� ������ ������������, � ���� �����������������
           try
             FFileBody.Position:= FHeader.TableLink;
             FFileBody.ReadBuffer(Ver4, SizeOf(TExHeader_4));
           except
             FLog.Add(Language.GetCaption(0150011));
             ExHeadOk:= False;
           end;

           FExHeader.NotebookDataVer  := Ver4.NotebookDataVer  ;
           FExHeader.NotebookItemSize := Ver4.NotebookItemSize ;
           FExHeader.NotebookOffset   := Ver4.NotebookOffset   ;
           FExHeader.NotebookCount    := Ver4.NotebookCount    ;
           FExHeader.DecodingDataVer  := Ver4.DecodingDataVer  ;
           FExHeader.DecodingItemSize := Ver4.DecodingItemSize ;
           FExHeader.DecodingOffset   := Ver4.DecodingOffset   ;
           FExHeader.DecodingCount    := Ver4.DecodingCount    ;
           FExHeader.RSPDataVer       := Ver4.RSPDataVer       ;
           FExHeader.RSPItemSize      := Ver4.RSPItemSize      ;
           FExHeader.RSPOffset        := Ver4.RSPOffset        ;
           FExHeader.RSPCount         := Ver4.RSPCount         ;
           NeedReAnalyze:= True;
         end;

      5: begin
           try
             FFileBody.Position:= FHeader.TableLink;
             FFileBody.ReadBuffer(FExHeader, SizeOf(FExHeader));
           except
             FLog.Add(Language.GetCaption(0150011));
             ExHeadOk:= False;
           end;
           NeedReAnalyze:= True;
         end;

      6: begin
           try
             FFileBody.Position:= FHeader.TableLink;
             FFileBody.ReadBuffer(FExHeader, SizeOf(FExHeader));
           except
             FLog.Add(Language.GetCaption(0150011));
             ExHeadOk:= False;
           end;
         end;

 7..255: begin
           FUnknownFileVersion:= True;
           Exit;

        {  try
             FFileBody.Position:= FHeader.TableLink + 14;
             FFileBody.ReadBuffer(DataSize, SizeOf(Integer));
             FFileBody.Position:= FHeader.TableLink;
             FFileBody.ReadBuffer(FExHeader, Min(SizeOf(FExHeader), DataSize));
           except
             FLog.Add(Language.GetCaption(0150011));
             ExHeadOk:= False;
           end; }

         end;
    end;

    if FExHeader.ErrorCount <> 0 then
      if FExHeader.ErrorDataVer = 3 then
      begin
        SetLength(FErrors, FExHeader.ErrorCount);
        try
          FFileBody.Position:= FExHeader.ErrorOffset;
          FFileBody.ReadBuffer(FErrors[0], FExHeader.ErrorCount * FExHeader.ErrorItemSize);
        except
          FLog.Add(Language.GetCaption(0150012));
          NeedReAnalyze:= True;
          SetLength(FErrors, 0);
        end;
      end
      else
      begin
        FUnknownFileVersion:= True;
        Exit;
      end;

    if FExHeader.EventCount <> 0 then
      if FExHeader.EventDataVer = 3 then
      begin
        SetLength(FEvents, FExHeader.EventCount);
        try
          FFileBody.Position:= FExHeader.EventOffset;
          FFileBody.ReadBuffer(FEvents[0], FExHeader.EventCount * FExHeader.EventItemSize);
        except
          FLog.Add(Language.GetCaption(0150013));
          NeedReAnalyze:= True;
          SetLength(FEvents, 0);
        end;
      end
      else
      begin
        FUnknownFileVersion:= True;
        Exit;
      end;

    if FExHeader.NotebookCount <> 0 then
    begin
      case FExHeader.NotebookDataVer of
        3: begin
             SetLength(TmpNbk, FExHeader.NotebookCount);
             try
               FFileBody.Position:= FExHeader.NotebookOffset;
               FFileBody.ReadBuffer(TmpNbk[0], FExHeader.NotebookCount * FExHeader.NotebookItemSize);
             except
               FLog.Add(Language.GetCaption(0150014));
               SetLength(FNotebook, 0);
               FModifyData:= True;
             end;

             SetLength(FNotebook, FExHeader.NotebookCount);
             for I:= 0 to FExHeader.NotebookCount - 1 do
             begin
               FNotebook[I].ID         := TmpNbk[I].ID         ;
               FNotebook[I].Rail       := TmpNbk[I].Rail       ;
               FNotebook[I].DisCoord   := TmpNbk[I].DisCoord   ;
               FNotebook[I].StDisCoord := TmpNbk[I].StDisCoord ;
               FNotebook[I].EdDisCoord := TmpNbk[I].EdDisCoord ;
               FNotebook[I].ManName    := TmpNbk[I].ManName    ;
               FNotebook[I].Privazka   := TmpNbk[I].Privazka   ;
               FNotebook[I].BlokNotText:= TmpNbk[I].BlokNotText;
               FNotebook[I].Defect     := TmpNbk[I].Defect     ;
               FNotebook[I].Prim       := TmpNbk[I].Prim       ;
               FNotebook[I].DT         := TmpNbk[I].DT         ;
               FNotebook[I].LeftKm1    := TmpNbk[I].LeftKm1    ;
               FNotebook[I].LeftKm2    := TmpNbk[I].LeftKm2    ;
               FNotebook[I].LeftPk1    := TmpNbk[I].LeftPk1    ;
               FNotebook[I].LeftPk2    := TmpNbk[I].LeftPk2    ;
               FNotebook[I].RightKm1   := TmpNbk[I].RightKm1   ;
               FNotebook[I].RightKm2   := TmpNbk[I].RightKm2   ;
               FNotebook[I].RightPk1   := TmpNbk[I].RightPk1   ;
               FNotebook[I].RightPk2   := TmpNbk[I].RightPk2   ;
               FNotebook[I].Zveno1     := TmpNbk[I].Zveno1     ;
               FNotebook[I].SysCoord   := TmpNbk[I].Zveno2     ;
               FNotebook[I].LeftMM     := TmpNbk[I].LeftMM     ;
               FNotebook[I].RightMM    := TmpNbk[I].RightMM    ;
               FNotebook[I].Chema      := TmpNbk[I].Chema      ;
               FNotebook[I].Zoom       := TmpNbk[I].Zoom       ;
               FNotebook[I].Reduction  := TmpNbk[I].Reduction  ;
               FNotebook[I].ViewMode   := TmpNbk[I].ViewMode   ;
               FNotebook[I].DrawRail   := TmpNbk[I].DrawRail   ;
               FNotebook[I].AmplTh     := TmpNbk[I].AmplTh     ;
               FNotebook[I].AmplDon    := TmpNbk[I].AmplDon    ;
               FNotebook[I].ViewChannel:= TmpNbk[I].ViewChannel;
               FNotebook[I].SelRect    := TmpNbk[I].SelRect    ;
               FNotebook[I].PathNum    := Header.Path          ;
               FNotebook[I].Peregon    := CodeToText(Header.SecName, Header.CharSet);
               FNotebook[I].FindAt     := 0;
               FNotebook[I].ViewType   := 0;
             end;
             SetLength(TmpNbk, 0);
           end;

        4: begin
             SetLength(FNotebook, FExHeader.NotebookCount);
             try
               FFileBody.Position:= FExHeader.NotebookOffset;
               FFileBody.ReadBuffer(FNotebook[0], FExHeader.NotebookCount * FExHeader.NotebookItemSize);
             except
               FLog.Add(Language.GetCaption(0150014));
               SetLength(FNotebook, 0);
               FModifyData:= True;
             end;
           end;
    5..99: begin
             FUnknownFileVersion:= True;
             Exit;
           end;
      end;
    end;

    if FExHeader.DecodingCount <> 0 then
      if FExHeader.DecodingDataVer = 3 then
      begin
        SetLength(FDecoding, FExHeader.DecodingCount);
        try
          FFileBody.Position:= FExHeader.DecodingOffset;
          FFileBody.ReadBuffer(FDecoding[0], FExHeader.DecodingCount * FExHeader.DecodingItemSize);
        except
          FLog.Add(Language.GetCaption(0150015));
          SetLength(FDecoding, 0);
          FModifyData:= True;
        end;
      end
      else
      begin
        FUnknownFileVersion:= True;
        Exit;
      end;

    if FExHeader.RSPCount <> 0 then
      if FExHeader.RSPDataVer = 3 then
      begin
        SetLength(FRSPInfo, FExHeader.RSPCount);
        try
          FFileBody.Position:= FExHeader.RSPOffset;
          FFileBody.ReadBuffer(FRSPInfo[0], FExHeader.RSPCount * FExHeader.RSPItemSize);
        except
          FLog.Add(Language.GetCaption(0150016));
          SetLength(FRSPInfo, 0);
          FModifyData:= True;
        end;
      end
      else
      begin
        FUnknownFileVersion:= True;
        Exit;
      end;
  end
  else
  begin
    NeedReAnalyze:= True;
    FExHeader.Modify:= $FF;
  end;

  if FExHeader.EventCount = 0 then NeedReAnalyze:= True;
  FExHdrLoadOk:= ExHeadOk;
  if (not ExHeadOk) or NeedReAnalyze then
  begin
    AnalyzeFileBody;
    SaveExData(FExHdrLoadOk);
    FModifyData:= False;
  end;
end;

procedure TAvk11DatSrc.CutExHeader;
var
  F: file;

begin
  try
    AssignFile(F, FFileName);                // �������� ����� ��� ������
    Reset(F, 1);
  except
    FLog.Add(Language.GetCaption(0150019));
    ShowMessage(Language.GetCaption(0150006));
    Exit;
  end;

            // ��������� ������� ������������ ���������
  try
    Seek(F, FHeader.TableLink);
    Truncate(F);
  except
    FLog.Add(Language.GetCaption(0150020));
    Exit;
  end;
end;

procedure TAvk11DatSrc.SaveExData(CutOldExHeader: Boolean);
var
  F: file;
  FileDate: TDateTime;

begin
  if FReadOnlyFlag then Exit;

  if not SkipTestDateTime then
  begin
    FileDate:= FileDateToDateTime(FileAge(FFileName));
    if (FSaveFileDate <> FileDate) or (FSaveFileSize <> FFileBody.Size) then
    begin
      ShowMessage(Language.GetCaption(0150009));
      Exit;
    end;
  end;

  try
    AssignFile(F, FFileName);                // �������� ����� ��� ������
    Reset(F, 1);
  except
    FLog.Add(Language.GetCaption(0150019));
    ShowMessage(Language.GetCaption(0150006));
    Exit;
  end;

  if CutOldExHeader and (FHeader.TableLink <> $FFFFFFFF) then   // ��������� ������� ������������ ���������
  begin
    try
      Seek(F, FHeader.TableLink);
      Truncate(F);
    except
      FLog.Add(Language.GetCaption(0150020));
      Exit;
    end;
  end;

  try
    Seek(F, FileSize(F));
    FHeader.TableLink:= FileSize(F);         // ����������� ��������� ������ ������������ ���������
    FDataSize:= FileSize(F);
  except
    FLog.Add(Language.GetCaption(0150021));
    Exit;
  end;

  try
    Seek(F, 0);
    BlockWrite(F, FHeader, Sizeof(TAvk11Header2)); // ������ ������ �������� ��������� ������������ ���������
  except
    FLog.Add(Language.GetCaption(0150022));
    Exit;
  end;

  FillChar(FExHeader.FFConst[0], Length(FExHeader.FFConst), $FF);
  FillChar(FExHeader.Reserv1[1], Length(FExHeader.Reserv1), $FF);
  FillChar(FExHeader.Reserv2[1], Length(FExHeader.Reserv2), $FF);

  FExHeader.DataVer:= 6;
  FExHeader.DataSize:= SizeOf(TExHeader_5);
  FExHeader.Flag:= 0;

  try
    Seek(F, FHeader.TableLink);                   // ������ ������������ ���������
    BlockWrite(F, FExHeader, FExHeader.DataSize);
  except
    FLog.Add(Language.GetCaption(0150028));
    Exit;
  end;

  FExHeader.ErrorDataVer:= 3;                   // ������ ������ ������ ��������� �����
  FExHeader.ErrorItemSize:= SizeOf(TFileError);
  FExHeader.ErrorCount:= Length(FErrors);
  FExHeader.ErrorOffset:= 0;
  if FExHeader.ErrorCount <> 0 then
    try
      FExHeader.ErrorOffset:= FilePos(F);
      BlockWrite(F, FErrors[0], FExHeader.ErrorCount * FExHeader.ErrorItemSize);
    except
      FLog.Add(Language.GetCaption(0150023));
      Exit;
    end;

  FExHeader.EventDataVer:= 3;                   // ������ ������ ������� �����
  FExHeader.EventItemSize:= SizeOf(TFileEvent);
  FExHeader.EventCount:= Length(FEvents);
  FExHeader.EventOffset:= 0;
  if FExHeader.EventCount <> 0 then
    try
      FExHeader.EventOffset:= FilePos(F);
      BlockWrite(F, FEvents[0], FExHeader.EventCount * FExHeader.EventItemSize);
    except
      FLog.Add(Language.GetCaption(0150024));
      Exit;
    end;

  FExHeader.NotebookDataVer:= 4;                // ������ ������ ������� �������������
  FExHeader.NotebookOffset:= 0;
  FExHeader.NotebookItemSize:= SizeOf(TFileNotebook_Ver4);
  FExHeader.NotebookCount:= Length(FNotebook);
  if FExHeader.NotebookCount <> 0 then
    try
      FExHeader.NotebookOffset:= FilePos(F);
      BlockWrite(F, FNotebook[0], FExHeader.NotebookCount * SizeOf(TFileNotebook_Ver4));
    except
      FLog.Add(Language.GetCaption(0150025));
      Exit;
    end;

  FExHeader.DecodingDataVer:= 3;                 // ������ ������ ������� � ������ �����������
  FExHeader.DecodingOffset:= 0;
  FExHeader.DecodingItemSize:= SizeOf(TFileDecoding);
  FExHeader.DecodingCount:= Length(FDecoding);
  if FExHeader.DecodingCount <> 0 then
    try
      FExHeader.DecodingOffset:= FilePos(F);
      BlockWrite(F, FDecoding[0], FExHeader.DecodingCount * SizeOf(TFileDecoding));
    except
      FLog.Add(Language.GetCaption(0150026));
      Exit;
    end;

  FExHeader.RSPDataVer:= 3;                 // ������ ������ ������ ���
  FExHeader.RSPOffset:= 0;
  FExHeader.RSPItemSize:= SizeOf(TRSPInfo);
  FExHeader.RSPCount:= Length(FRSPInfo);
  if FExHeader.RSPCount <> 0 then
    try
      FExHeader.RSPOffset:= FilePos(F);
      BlockWrite(F, FRSPInfo[0], FExHeader.RSPCount * SizeOf(TRSPInfo));
    except
      FLog.Add(Language.GetCaption(0150027));
      Exit;
    end;

  FExHeader.UCSDataVer:= 3;
  FExHeader.UCSItemSize:= 0;
  FExHeader.UCSOffset:= 0;
  FExHeader.UCSCount:= 0;

  FExHeader.LabelEditDataVer:= 3;
  FExHeader.LabelEditItemSize:= 0;
  FExHeader.LabelEditOffset:= 0;
  FExHeader.LabelEditCount:= 0;

  try                                             // ������ ������ ������������ ���������
    Seek(F, FHeader.TableLink);
    BlockWrite(F, FExHeader, FExHeader.DataSize);
    Close(F);
  except
    FLog.Add(Language.GetCaption(0150028));
    Exit;
  end;

  FFileBody.Clear;
  FFileBody.LoadFromFile(FFileName);

  FSaveFileDate:= FileDateToDateTime(FileAge(FFileName));
  FSaveFileSize:= FFileBody.Size;

  FExHdrLoadOk:= True;
end;

// ------< ������ � ��� ��������� - ���������� ������ 1 - �� ������ >-----------

procedure TAvk11DatSrc.PackReset;
begin
  FPack[r_Left].Ampl:= $FF;
  FPack[r_Right].Ampl:= $FF;
end;

procedure TAvk11DatSrc.UnPackEcho;
var
  R: RRail;

begin
  if not UnpackBottom then
  begin
    for R:= r_Left to r_Right do FPack[R].Ampl:= $FF;
    exit;
  end;

  for R:= r_Left to r_Right do
  begin
    if (FDestDat[R].Count > 1) or
//       ((FDestDat[R].Count = 1) and (FDestDat[R].Delay[1] = FPack[R].Delay) and (FDestDat[R].Ampl[1] = FPack[R].Ampl)) or
       FDestDat[R].ZerroFlag then FPack[R].Ampl:= $FF
                             else begin
                                    if FDestDat[R].Count = 1 then
                                    begin
                                      FPack[R].Delay:= FDestDat[R].Delay[1];
                                      FPack[R].Ampl:= FDestDat[R].Ampl[1];
                                    end;
                                  end;

    FDestDat[R].Count:= 0;
    FDestDat[R].ZerroFlag:= False;
  end;
end;

procedure TAvk11DatSrc.DataToEcho;
begin
  if FSrcDat[0] shr 3 and 7 = 1 then
  begin
    with FDestDat[SideToRail(TSide(FSrcDat[0] shr 6 and 1))] do
    begin
//      Count:= 0;
      case FSrcDat[0] and $07 of
        0: ZerroFlag:= True;
        1: begin
             if Count < 16 then Inc(Count);
             Delay[Count]:= FSrcDat[1];
             Ampl[Count]:= FSrcDat[2] shr 4 and $0F;
           end;
        2: begin
             if Count < 16 then Inc(Count);
             Delay[Count]:= FSrcDat[1];
             Ampl[Count]:= FSrcDat[3] shr 4 and $0F;
             if Count < 16 then Inc(Count);
             Delay[Count]:= FSrcDat[2];
             Ampl[Count]:= FSrcDat[3] and $0F;
           end;
        3: begin
             if Count < 16 then Inc(Count);
             Delay[Count]:= FSrcDat[1];
             Ampl[Count]:= FSrcDat[4] shr 4 and $0F;
             if Count < 16 then Inc(Count);
             Delay[Count]:= FSrcDat[2];
             Ampl[Count]:= FSrcDat[4] and $0F;
             if Count < 16 then Inc(Count);
             Delay[Count]:= FSrcDat[3];
             Ampl[Count]:= FSrcDat[5] shr 4 and $0F;
           end;
        4: begin
             if Count < 16 then Inc(Count);
             Delay[Count]:= FSrcDat[1];
             Ampl[Count]:= FSrcDat[5] shr 4 and $0F;
             if Count < 16 then Inc(Count);
             Delay[Count]:= FSrcDat[2];
             Ampl[Count]:= FSrcDat[5] and $0F;
             if Count < 16 then Inc(Count);
             Delay[Count]:= FSrcDat[3];
             Ampl[Count]:= FSrcDat[6] shr 4 and $0F;
             if Count < 16 then Inc(Count);
             Delay[Count]:= FSrcDat[4];
             Ampl[Count]:= FSrcDat[6] and $0F;
           end;
      end;
    end;
  end;
end;

// ------< ������ ��������� ����� � ����������� ��������� �������� >-----------

procedure TAvk11DatSrc.CloseFile;
begin
  SaveExData(FExHdrLoadOk);
end;

procedure TAvk11DatSrc.AnalyzeFileBody; // ���������� - ��������� ���� � �����. - ����� �����
label
  LoadCont,
  LoadStop;

var
  HSHead: THSHead;
  HSItem: THSItem;
  CurSysCoord: Integer;
  Load_CurSysCoord: Integer;
  LastSysCoord: Integer;
  LastDisCoord: Integer;
  CurDisCoord: Int64;
  BackMotionFlag: Boolean;
  LastCoordEvt: Integer;
//  CurCoordEvt: Integer;
  SaveOffset: Integer;
  LastSaveOffset: Integer;
  CurrCoord: mCoord;
  SkipBytes: Integer;
  I: Integer;
  LoadID: Byte;
  LoadByte: array [1..22] of Byte;
  LoadLongInt: array [1..5] of LongInt; // absolute LoadByte[1];
  NeedSync: Boolean;
  Card: DWord;
  CheckSumm: Int64;
  ErrorOffset: Integer;
  NewProgress: Integer;
  LoadStopFlag: Boolean;
  FirstCoordAfterError: Boolean;
  LastStolbSysCoord: Integer;
  AddErrID: Integer;
  FFF: Boolean;
  Last: Integer;

procedure AddEvent(ID: Byte; SysCoord: Integer = - MaxInt; DisCoord: Integer = - MaxInt);
begin
  SetLength(FEvents, Length(FEvents) + 1);

  FEvents[High(FEvents)].ID:= ID;
  FEvents[High(FEvents)].OffSet:= SaveOffset;
  if SysCoord = - MaxInt then FEvents[High(FEvents)].SysCoord:= CurSysCoord
                         else FEvents[High(FEvents)].SysCoord:= SysCoord;
  if DisCoord = - MaxInt then FEvents[High(FEvents)].DisCoord:= CurDisCoord
                         else FEvents[High(FEvents)].DisCoord:= DisCoord;
  FEvents[High(FEvents)].Pack:= FPack;

  LastSaveOffset:= SaveOffset;
end;

procedure AddError(ECode: Byte; EOffset, ESize, EBegSysCoord, EEndSysCoord, EBegDisCoord, EEndDisCoord: Integer);
begin
  if Length(FErrors) <> 0 then
    with FErrors[High(FErrors)] do
      if Offset + FErrors[High(FErrors)].Size = EOffset then
      begin
        Inc(Size, ESize);
        EndSysCoord:= EEndSysCoord;
        EndDisCoord:= EEndDisCoord;
        Exit;
      end;

  SetLength(FErrors, Length(FErrors) + 1);
  FErrors[High(FErrors)].Code:= ECode;
  FErrors[High(FErrors)].Offset:= EOffset;
  FErrors[High(FErrors)].Size:= ESize;
  FErrors[High(FErrors)].BegSysCoord:= EBegSysCoord;
  FErrors[High(FErrors)].EndSysCoord:= EEndSysCoord;
  FErrors[High(FErrors)].BegDisCoord:= EBegDisCoord;
  FErrors[High(FErrors)].EndDisCoord:= EEndDisCoord;
end;

function TestOffset(Pos: Integer): Boolean;
var
  I: Integer;
  ID: Byte;

begin
  Result:= True;
  if Pos < 0 then Exit;
  I:= FFileBody.Position;
  FFileBody.Position:= Pos;
  FFileBody.ReadBuffer(ID, 1);
  FFileBody.Position:= I;
  Result:= ID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF];
end;

var
  WaitForm: TWaitForm;
  SearchBMEnd: Boolean;
  StartBMSysCoord: Integer;
  BSHead: mBSAmplHead;

begin
  WaitForm:= TWaitForm.Create(nil);
  WaitForm.Visible:= True;
  WaitForm.BringToFront;
  WaitForm.Refresh;

                         // ����� ������
  BackMotionFlag:= False;
  SearchBMEnd:= False;
  FLeftFlag:= False;
  FRightFlag:= False;
  SetLength(FEvents, 0); // ������� ������� ���������
  SetLength(FErrors, 0); // ������� ������� ������

  LoadStopFlag:= False;
  CurSysCoord:= 0;
  Load_CurSysCoord:= 0;
  CurDisCoord:= 0;
  LastSysCoord:= MaxInt;
  LastSaveOffset:= 0;
  LastCoordEvt:= - 1;
  NeedSync:= False;
  LastStolbSysCoord:= 0;

  CurrCoord.Km[1]:= FHeader.StartKM;
  CurrCoord.Pk[1]:= FHeader.StartPk;

  PackReset;

  FFileBody.Position:= SizeOf(TAvk11Header2); // ������� ���������
  SaveOffset:= FFileBody.Position;        // ���������� offset �������
  AddEvent(EID_FwdDir, 0);
//  SelectDir[0]:= 0;

  LoadCont:

  try
    repeat

      SkipBytes:= - 1;
      SaveOffset:= FFileBody.Position;        // ���������� offset �������

      FFileBody.ReadBuffer(LoadID, 1);       // �������� �������������� �������

      if LoadID and 128 = 0 then  // �������
      begin
        if (LoadID shr 3 and $07 <> 0) and (LoadID and $07 < 5) then // (��� ������ � 0 ������) ��� (���������� �����������  > 4)
        begin
          case SideToRail(TSide(LoadID shr 6 and 1)) of    // ������� ������ � ������� ������ � ������ ����
            r_Left: FLeftFlag:= True;
           r_Right: FRightFlag:= True;
          end;
          if LoadID shr 3 and $07 = 1 then    // ���� ������ ����� �� �������������
          begin
            FSrcDat[0]:= LoadID;
            FFileBody.ReadBuffer(FSrcDat[1], Avk11EchoLen[LoadID and $07]);
            DataToEcho;
          end
          else if LoadID shr 3 and $07 = 0 then SkipBytes:= 1   // ���� ������� ����� �� ���������� 1 ����
          else SkipBytes:= Avk11EchoLen[LoadID and $07]; // ����� ����������
        end else raise EBadID.Create('');
      end
      else                   // ��������� �������
      begin
        case LoadID of

             EID_HandScan: begin           // ������
                             FFileBody.ReadBuffer(HSHead, 6);
                             repeat
                               FFileBody.ReadBuffer(HSItem, 3);
                               if HSItem.Count > 4 then raise EBadLink.Create('');
                               FFileBody.ReadBuffer(HSItem.Data[0], Avk11EchoLen[HSItem.Count]);
                             until HSItem.Sample = $FFFF;
                           end;
            EID_NewHeader: if FFileBody.Position = 129 then
                           begin
                             FFileBody.ReadBuffer(SkipBytes, 4);
                             FFileBody.Position:= FFileBody.Position + SkipBytes;
                             SkipBytes:= - 1;
                           end
                           else raise EBadID.Create('');
               EID_BSAmpl: begin
                             FFileBody.ReadBuffer(BSHead, SizeOf(mBSAmplHead));
                             FFileBody.Position:= FFileBody.Position + BSHead.Len * 2;
                             SkipBytes:= - 1;
                           end;

                  EID_ACLeft      ,   // [$80] ��������� ������������� �������� ����� ���� - ���� ��� ���� �������
                  EID_ACRight     ,   // [$81] ��������� ������������� �������� ������ ���� - ���� ��� ���� �������
                  EID_Sens        ,   // [$90] ��������� �������� ����������������
                  EID_Att         ,   // [$91] ��������� ��������� ���� ����������� (0 �� �������� ����������������)
                  EID_VRU         ,   // [$92] ��������� ���
                  EID_StStr       ,   // [$93] ��������� ��������� ������ ������
                  EID_EndStr      ,   // [$94] ��������� ��������� ����� ������
                  EID_HeadPh      ,   // [$95] ������ ���������� ���������
                  EID_Mode        ,   // [$96] ��������� ������
                  EID_2Tp         ,   // [$99] ��������� 2��
                  EID_ZondImp     ,   // [$9A] ��������� ������ ������������ ��������
                  EID_SetRailType ,   // [$9B]
                  EID_2Tp_Word    ,   // [$9C]
                  EID_Strelka     ,   // [$A1] ����� ����������� �������� + �����
                  EID_DefLabel    ,   // [$A2] ����� ������� + �����
                  EID_TextLabel   ,   // [$A3] ������ ������� (���������) + �����
                  EID_StBoltStyk  ,   // [$A4] ������� ������ �� + �����
                  EID_EndBoltStyk ,   // [$A5] ���������� ������ �� + �����
                  EID_Time        ,   // [$A6] ������� �������
                  EID_StLineLabel ,   // [$A7] ������ ����������� ����� + �����
                  EID_EndLineLabel,   // [$A8] ����� ����������� ����� + �����
                  EID_StSwarStyk  ,   // [$A9] ������� ������ ������� ���� + �����
                  EID_EndSwarStyk ,   // [$AA] ���������� ������ ������� ���� + �����
                  EID_OpChange    ,   // [$AB] ����� ���������
                  EID_PathChange  ,   // [$AC] ����� ������ ����
                  EID_MovDirChange,   // [$AD] ����� ����������� ��������
                  EID_SezdNo      ,   // [$AE] ����� ������
                  EID_LongLabel   ,   // [$AF] ����������� �����
                  EID_GPSCoord    ,
                  EID_GPSState    ,
                  EID_GPSError    :

                           SkipBytes:= Avk11EventLen[LoadID];

      EID_Sys1..EID_Sys16: SkipBytes:= 9;
                EID_Stolb: begin
                             FFileBody.ReadBuffer(CurrCoord, Avk11EventLen[LoadID]);
                             LastStolbSysCoord:= CurSysCoord;
                           end;
            EID_SysCrd_SS: begin // - �������� ������, �������� ����������
                             FFileBody.ReadBuffer(LoadByte[1], 2);
                             if not TestOffset(FFileBody.Position - 3 - LoadByte[1]) then raise EBadLink.Create('');
                             CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[2]);
                           end;
            EID_SysCrd_SF: begin // - �������� ������, ������ ����������
                             FFileBody.ReadBuffer(LoadByte[1], 1);
                             if not TestOffset(FFileBody.Position - 2 - LoadByte[1]) then raise EBadLink.Create('');
                             FFileBody.ReadBuffer(LoadLongInt, 4);
                             CurSysCoord:= LoadLongInt[1];
                           end;
            EID_SysCrd_FS: begin // - ������ ������, �������� ����������
                             FFileBody.ReadBuffer(LoadLongInt[1], 4);
                             FFileBody.ReadBuffer(LoadByte[1], 1);
                             if not TestOffset(FFileBody.Position - 6 - LoadLongInt[1]) then raise EBadLink.Create('');
                             CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[1]);
                           end;
            EID_SysCrd_FF: begin // - ������ ������, ������ ����������
                             FFileBody.ReadBuffer(LoadLongInt[1], 4);
                             FFileBody.ReadBuffer(LoadLongInt[2], 4);
                             if not TestOffset(FFileBody.Position - 9 - LoadLongInt[1]) then raise EBadLink.Create('');
                             CurSysCoord:= LoadLongInt[2];
                           end;
              EID_EndFile: begin // ����� �����
                             FFileBody.ReadBuffer(LoadByte, 9 + 4);
                             for I:= 4 + 1 to 4 + 9 do
                               if LoadByte[I] <> $FF then raise EBadEnd.Create('');
                             AddEvent(EID_EndFile);
                             Break;
                           end

          else raise EBadID.Create('');
        end;
      end;

      if LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF] then // ��������� ����������
      begin
        UnPackEcho;

        if LastSysCoord <> MaxInt then
        begin
          Last:= CurDisCoord;

          Inc(CurDisCoord, Abs(CurSysCoord - LastSysCoord)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)

          if FirstCoordAfterError then
          begin
            if GetErrorCount > 0 then
            begin
              FErrors[GetErrorCount - 1].EndSysCoord:= CurSysCoord;
              FErrors[GetErrorCount - 1].EndDisCoord:= CurDisCoord;
            end;
            FirstCoordAfterError:= False;
          end;

          if (not BackMotionFlag) and (CurSysCoord < LastSysCoord) then // ���� ���������� ����������� �������� (������ �� �����)
          begin
            AddEvent(EID_BwdDir, LastSysCoord, Last);
            BackMotionFlag:= True;
//            AddEvent(EID_BwdDir);
            if not SearchBMEnd then
            begin
              StartBMSysCoord:= LastSysCoord;
              SearchBMEnd:= True;
            end;
          end
          else
          if BackMotionFlag and (CurSysCoord > LastSysCoord) then // ���� ���������� ����������� �������� (����� �� ������)
          begin
            AddEvent(EID_FwdDir, LastSysCoord, Last);
            BackMotionFlag:= False;
//            AddEvent(EID_FwdDir);
          end;
          if SearchBMEnd and (CurSysCoord > StartBMSysCoord) then
          begin
            SearchBMEnd:= False;
            if CurSysCoord - LastSysCoord <> 0
              then AddEvent(EID_EndBM, StartBMSysCoord, Round(LastDisCoord + (StartBMSysCoord - LastSysCoord) * (CurDisCoord - LastDisCoord) / (CurSysCoord - LastSysCoord)))
              else AddEvent(EID_EndBM);
          end;
        end
        else
        begin
          for I:= 0 to High(FEvents) do
          begin
            FEvents[I].SysCoord:= CurSysCoord;
            FEvents[I].DisCoord:= 0;
          end;
        end;
        LastSysCoord:= CurSysCoord;
        LastDisCoord:= CurDisCoord;
      end;

      if (LoadID in [EID_HandScan    ,  EID_Sens       ,  EID_Att        , EID_VRU         , EID_2Tp_Word,
                     EID_StStr       ,  EID_EndStr     ,  EID_HeadPh     , EID_Mode        ,
                     EID_2Tp         ,  EID_ZondImp    ,  EID_SetRailType, EID_Stolb       ,
                     EID_Strelka     ,  EID_DefLabel   ,  EID_TextLabel  , EID_StBoltStyk  ,
                     EID_EndBoltStyk ,  EID_Time       ,  EID_StLineLabel, EID_EndLineLabel,
                     EID_StSwarStyk  ,  EID_EndSwarStyk,  EID_OpChange   , EID_PathChange  ,
                     EID_MovDirChange,  EID_SezdNo     ,  EID_LongLabel  , EID_GPSCoord   , EID_GPSState,
                     EID_GPSError    ,  EID_EndFile    ,  EID_NewHeader  , EID_BSAmpl])  then AddEvent(LoadID);

      if FFileBody.Position - LastSaveOffset >= 16384 then AddEvent(EID_LinkPt);
      if SkipBytes <> - 1 then FFileBody.Position:= FFileBody.Position + SkipBytes;

      if Assigned(LoadProg) then
      begin
        NewProgress:= LoadProg.MinValue + Round((LoadProg.MinValue - LoadProg.MaxValue) * FFileBody.Position / FDataSize);
        if LoadProg.Progress <> NewProgress then LoadProg.Progress:= NewProgress;
      end;

    until FFileBody.Position + 1 > FDataSize;

  except
    on EBadID do
    begin
      ErrorOffset:= SaveOffset;
      FLog.Add(Format('AnalyzeFileBody: %x �������� ID', [SaveOffset]));
      AddErrID:= EID_BadBody;
      NeedSync:= True;
    end;
    on EBadLink do
    begin
      ErrorOffset:= SaveOffset;
      FLog.Add(Format('AnalyzeFileBody: %x �������� ������ � ����������', [SaveOffset]));
      AddErrID:= EID_BadLinc;
      NeedSync:= True;
    end;
    on EBadEnd do
    begin
      ErrorOffset:= SaveOffset;
      FLog.Add(Format('AnalyzeFileBody: %x �������� ����� �����', [SaveOffset]));
      AddErrID:= EID_BadBody; // EID_BadEnd;
      NeedSync:= True;
    end;
    on E0: EBadCoord do
    begin
      ErrorOffset:= SaveOffset;
      FLog.Add(Format('AnalyzeFileBody: %x ������ ����������', [SaveOffset]) + E0.Message);
      CurSysCoord:= LastSysCoord;
      AddErrID:= EID_BadCoord;
      NeedSync:= True;
  //    goto LoadCont;
    end;
    on EReadError do
    begin
      FLog.Add(Format('AnalyzeFileBody: %x ������ ������', [SaveOffset]));
    end;
  end;

  if NeedSync then
  begin
    try
      while (FExHdrLoadOk and (FFileBody.Position + 1 < FHeader.TableLink + 1)) or
            ((not FExHdrLoadOk) and (FFileBody.Position + 1 < FFileBody.Size)) do
      begin
        SaveOffset:= FFileBody.Position;

        FFileBody.ReadBuffer(LoadByte[1], 1);
        case LoadByte[1] of
           EID_SysCrd_SS,
           EID_SysCrd_SF: begin
                            FFileBody.ReadBuffer(LoadID, 1);
                            if FFileBody.Position > LoadID then
                            begin
                              FFileBody.Position:= FFileBody.Position - 2 - LoadID;
                              FFileBody.ReadBuffer(LoadID, 1);

                              if LoadByte[1] = EID_SysCrd_SF then
                              begin
                                FFileBody.Position:= SaveOffset + 2;
                                FFileBody.ReadBuffer(LoadLongInt[1], 4);
                                FFF:= Abs(LoadLongInt[1] - LastSysCoord) < 10000;
                              end else FFF:= True;

                              if FFF and (LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF]) then
                              begin
                                FFileBody.Position:= SaveOffset;
                                AddError(AddErrID, ErrorOffset, SaveOffset - ErrorOffset, CurSysCoord, 0, CurDisCoord, 0);
                                NeedSync:= False;
                                Break;
                              end;
                            end;
                          end;

           EID_SysCrd_FS,
           EID_SysCrd_FF: begin
                            FFileBody.ReadBuffer(LoadLongInt[1], 4);
                            if FFileBody.Position > Abs(LoadLongInt[1]) then
                            begin
                              FFileBody.Position:= FFileBody.Position - 5 - Abs(LoadLongInt[1]);
                              FFileBody.ReadBuffer(LoadID, 1);

                              if LoadByte[1] = EID_SysCrd_FF then
                              begin
                                FFileBody.Position:= SaveOffset + 5;
                                FFileBody.ReadBuffer(LoadLongInt[1], 4);
                                FFF:= Abs(LoadLongInt[1] - LastSysCoord) < 10000;
                              end else FFF:= True;

                              if FFF and (LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF]) then
                              begin
                                FFileBody.Position:= SaveOffset;
                                AddError(AddErrID, ErrorOffset, SaveOffset - ErrorOffset, CurSysCoord, 0, CurDisCoord, 0);
                                NeedSync:= False;
                                Break;
                              end;
                            end;
                          end;
        end;
        FFileBody.Position:= SaveOffset + 1;
      end;
    except
      FLog.Add(Format('AnalyzeFileBody: %x ������� ��� ������������� ����� ����', [SaveOffset]));
      LoadStopFlag:= True;
    end;

    if LoadStopFlag then goto LoadStop;

    if not NeedSync then
    begin
      LastCoordEvt:= - 1;
      FirstCoordAfterError:= True;
      goto LoadCont;
    end;
  end;

 LoadStop:

  if FLeftFlag and
     FRightFlag then FExHeader.DataRail:= r_Both else
  if FLeftFlag  then FExHeader.DataRail:= r_Left else
  if FRightFlag then FExHeader.DataRail:= r_Right;

  if (Length(FEvents) <> 0) and (FEvents[High(FEvents)].DisCoord <> CurDisCoord) then AddEvent(EID_LinkPt);

  FExHeader.EndKM:= CurrCoord.Km[1];
  FExHeader.EndPK:= CurrCoord.Pk[1];

  if LastStolbSysCoord = 0 then // ���� ��� �� ������ ������ ����� ...
  begin
    if Header.MoveDir = 0 then FExHeader.EndMM:= FHeader.StartMetre * 1000 - (CurSysCoord - LastStolbSysCoord) * Header.ScanStep div 100
                          else FExHeader.EndMM:= FHeader.StartMetre * 1000 + (CurSysCoord - LastStolbSysCoord) * Header.ScanStep div 100
  end
  else
  begin
    if Header.MoveDir = 0 then FExHeader.EndMM:= 100000 - (CurSysCoord - LastStolbSysCoord) * Header.ScanStep div 100
                          else FExHeader.EndMM:= (CurSysCoord - LastStolbSysCoord) * Header.ScanStep div 100;
  end;


  FExHeader.EndSysCoord:= CurSysCoord;
  FExHeader.EndDisCoord:= CurDisCoord;

  {$OVERFLOWCHECKS OFF}
  CheckSumm:= CheckSummStartValue;
  FFileBody.Position:= 0;
  for I:= 1 to 29 do
  begin
    FFileBody.ReadBuffer(Card, 4);
    CheckSumm:= CheckSumm + Card;
  end;
  FFileBody.Position:= FFileBody.Position + 3 * 4;
  while FFileBody.Position + 4 < FDataSize {FFileBody.Size} do
  begin
    FFileBody.ReadBuffer(Card, 4);
    CheckSumm:= CheckSumm + Card;
  end;
  FHeader.CheckSumm:= CheckSumm and $00000000FFFFFFFF;
  {$OVERFLOWCHECKS ON}

  WaitForm.Release;
//  showmessage(IntToStr(MaxDelta));
end;

procedure TAvk11DatSrc.ReAnalyze;
begin
  AnalyzeFileBody;
  FillEventsData;
  SaveExData(FExHdrLoadOk);
end;

// -------------< �������� ����������� ����� >------------------------------------

function TAvk11DatSrc.TestCheckSumm: Boolean;
var
  I: Integer;
  Card: DWord;
  CheckSumm: Int64;

begin
  {$OVERFLOWCHECKS OFF}
  CheckSumm:= CheckSummStartValue;
  FFileBody.Position:= 0;
  for I:= 1 to 29 do
  begin
    FFileBody.ReadBuffer(Card, 4);
    CheckSumm:= CheckSumm + Card;
  end;
  FFileBody.Position:= FFileBody.Position + 3 * 4;
  while FFileBody.Position + 4 < FDataSize {FFileBody.Size} do
  begin
    FFileBody.ReadBuffer(Card, 4);
    CheckSumm:= CheckSumm + Card;
  end;
  CheckSumm:= CheckSumm and $00000000FFFFFFFF;
  {$OVERFLOWCHECKS ON}

  Result:= FHeader.CheckSumm = CheckSumm;
end;

// -------------< �������� ��� ������ >------------------------------------

function TAvk11DatSrc.TestOffsetFirst: Boolean;
var
  I: Integer;

begin
  Result:= True;
  FLastErrorIdx:= 0;
  for I:= 0 to High(FErrors) do
  begin
    if (FFileBody.Position < FErrors[I].Offset) then
    begin
      Result:= True;
      Break;
    end;
    if (FFileBody.Position >= FErrors[I].Offset) and
       (FFileBody.Position < FErrors[I].Offset + FErrors[I].Size) then
         case FErrors[I].Code of
 EID_BadBody,
 EID_BadLinc,
EID_BadCoord: begin
                FFileBody.Position:= FErrors[I].Offset + FErrors[I].Size;
                FLastErrorIdx:= I;
                Result:= True;
                Break;
              end;
  EID_BadEnd: begin
                Result:= False;
                Break;
              end;
         end;
  end;
end;

function TAvk11DatSrc.TestOffsetNext: Boolean;
var
  I: Integer;

begin
  Result:= True;
  for I:= FLastErrorIdx to High(FErrors) do
  begin
    if (FFileBody.Position < FErrors[I].Offset) then
    begin
      FLastErrorIdx:= I;
      Result:= True;
      Break;
    end;
    if (FFileBody.Position >= FErrors[I].Offset) and
       (FFileBody.Position < FErrors[I].Offset + FErrors[I].Size) then
         case FErrors[I].Code of
 EID_BadBody,
 EID_BadLinc,
EID_BadCoord: begin
                FFileBody.Position:= FErrors[I].Offset + FErrors[I].Size;
                FLastErrorIdx:= I;
                Result:= True;
                Break;
              end;
  EID_BadEnd: begin
                Result:= False;
                Break;
              end;
         end;
  end;
end;

procedure TAvk11DatSrc.TestBMOffsetFirst;
var
  I: Integer;

begin
//  Result:= False;

  FLastBMIdx:= 0;
  for I:= 0 to High(FBMItems) do
  begin
    if (FFileBody.Position < FBMItems[I].StSkipOff) then Break;
    if (FFileBody.Position >= FBMItems[I].StSkipOff) and
       (FFileBody.Position < FBMItems[I].OkOff) then
    begin
      FLastBMIdx:= I;
      Break;

  //    FFileBody.Position:= FBMItems[I].OkOff;
  //    FPack:= FBMItems[I].Pack;
  //    Result:= True;

    end;
  end;

end;

function TAvk11DatSrc.TestBMOffsetNext: Boolean;
var
  I: Integer;

begin
  Result:= False;
  for I:= FLastBMIdx to High(FBMItems) do
  begin
    if (FFileBody.Position < FBMItems[I].StSkipOff) then
    begin
      FLastBMIdx:= I;
      Break;
    end;
    if (FFileBody.Position >= FBMItems[I].StSkipOff) and
       (FFileBody.Position < FBMItems[I].OkOff) then
    begin
      FLastBMIdx:= I;
      Result:= True;
      Break;
    end;
  end;
end;

function TAvk11DatSrc.FindBSState(StartDisCoord: Integer): Boolean;
begin
  if FCurDisCoord < FObmen1 then
  begin
    FObmen2:= FPack;
    Result:= True;
  end else Result:= False;
end;

procedure TAvk11DatSrc.TestCanSkipBM;
begin
  if (not FCanSkipBM) or (FEventsData[0].Event.ID <> EID_FwdDir) then // ���� ���� �������� ������ ������ ������ - ��� ������� ����� ���� �������� ����� - ����� ���������������
  begin
    ReAnalyze;
    FillEventsData;
  end;
end;

procedure TAvk11DatSrc.SetSkipBM(NewState: Boolean);
var
  I: Integer;
  J: Integer;
  K: Integer;
  L: Integer;
  Flag: Boolean;

  GetDisCoord: Int64;
  GetSysCoord: Integer;
  GetOffset: Integer;

  FBMItems_: array of TBMItem;
  ED_: array of TFileEventData;


begin
  if NewState and (not FSkipBM) then
  begin
    TestCanSkipBM;

{   for I:= 0 to High(FNotebook) do
      FNotebook[I].SysCoord:= DisToSysCoord(FNotebook[I].DisCoord);  }

    SetLength(FSaveEventsData, Length(FEventsData)); // ��������� �������
    Move(FEventsData[0], FSaveEventsData[0], Length(FEventsData) * SizeOf(TFileEventData));

    SetLength(ED_, Length(FEventsData));
    Move(FEventsData[0], ED_[0], Length(FEventsData) * SizeOf(TFileEventData));

    Flag:= False;
    SetLength(FBMItems, 0);
    SetLength(FBMItems_, 0);

    for I:= 0 to High(FEventsData) do
    begin
      if (not Flag) and (FEventsData[I].Event.ID = EID_BwdDir) then // ���� �������� �������� ����� - ���������� ��� ��� ��������
      begin
        Flag:= True;
        J:= FEventsData[I].Event.SysCoord;
        K:= FEventsData[I].Event.OffSet;
        L:= FEventsData[I].Event.DisCoord;
      end;

      if Flag and (FEventsData[I].Event.ID = EID_EndBM) then  // ���� �������� ����� ����������� - ��������� ������ � ���
      begin
        Flag:= False;
        SetLength(FBMItems_, Length(FBMItems_) + 1);
        FBMItems_[High(FBMItems_)].StSkipOff:= K;
        FBMItems_[High(FBMItems_)].OkOff:= FEventsData[I].Event.OffSet;
        FObmen1:= FEventsData[I].Event.DisCoord;
        LoadData(L, GetMaxDisCoord, 0, FindBSState);
        FBMItems_[High(FBMItems_)].Pack:= FObmen2;
      end;

      if Flag then
      begin                                             // ���� �� � ���� �������� ����� - ����� ��� ��������� �� ���������� ��� ������
        ED_[I].Event.SysCoord:= J;
        ED_[I].Event.DisCoord:= J;
      end
      else
      begin                                             // ���� �� �� � ���� �������� ����� - ����� ��� ������� ��������� �� �� ��������� ����������
        ED_[I].Event.DisCoord:= ED_[I].Event.SysCoord;
      end
    end;

    if Flag then
    begin
      SetLength(FBMItems_, Length(FBMItems_) + 1);
      FBMItems_[High(FBMItems_)].StSkipOff:= K;
      FBMItems_[High(FBMItems_)].OkOff:= Header.TableLink;
      FObmen1:= Header.TableLink;
      LoadData(L, GetMaxDisCoord, 0, FindBSState);
      FBMItems_[High(FBMItems_)].Pack:= FObmen2;
    end;

    SetLength(FBMItems, Length(FBMItems_));
    Move(FBMItems_[0], FBMItems[0], Length(FBMItems) * SizeOf(TBMItem));
    SetLength(FBMItems_, 0);

    Move(ED_[0], FEventsData[0], Length(FEventsData) * SizeOf(TFileEventData));
    SetLength(ED_, 0);

    FillTimeList;
  end;

  if (not NewState) and FSkipBM then
  begin
    Move(FSaveEventsData[0], FEventsData[0], Length(FSaveEventsData) * SizeOf(TFileEventData));
    SetLength(FSaveEventsData, 0);
    SetLength(FBMItems, 0);
    FillTimeList;
    for I:= 0 to High(FNotebook) do
      if FNotebook[I].DisCoord = - MaxInt then
        FNotebook[I].DisCoord:= SysToDisCoord(FNotebook[I].SysCoord);
  end;

  FSkipBM:= NewState;
end;

function TAvk11DatSrc.ValueCount(Group: Byte): Integer;
var
  Shift: Word;
  Grp: Byte;

begin
  Result:= 0;
  if FParDat.Size = 0 then Exit;
  FParDat.Position:= 0;
  repeat
    FParDat.ReadBuffer(Shift, 2);
    FParDat.ReadBuffer(Grp, 1);
    FParDat.Position:= FParDat.Position + Shift - 3;
    if Grp = Group then Inc(Result);
  until FParDat.Position >= FParDat.Size;
end;

procedure TAvk11DatSrc.GetValue(Group: Byte; Index: Integer; var ValueId, Value: Variant);
var
  Shift: Word;
  Grp: Byte;
  IdId: Byte;
  IdByte: Byte;
  IdStr: string;
  Count: Integer;
  ValueTyp: TValueType;

  I: Integer;
  S: string;
  DT: Double;
  DT_: TDateTime;
  E: Extended;

begin
  if FParDat.Size = 0 then Exit;

  Count:= - 1;
  FParDat.Position:= 0;
  repeat
    FParDat.ReadBuffer(Shift, 2);
    FParDat.ReadBuffer(Grp, 1);

    if Grp = Group then Inc(Count);
    if Index = Count then
    begin
      FParDat.ReadBuffer(IdId, 1);
      if IdId = 0 then begin
                         FParDat.ReadBuffer(IdByte, 1);
                         ValueId:= IdByte;
                       end
                       else
                       begin
                         SetLength(IdStr, IdId);
                         FParDat.ReadBuffer(IdStr[1], IdId);
                         ValueId:= IdStr;
                       end;

      FParDat.ReadBuffer(ValueTyp, 1);
      case ValueTyp of
             vtInt: begin
                      FParDat.ReadBuffer(I, SizeOf(Integer));
                      Value:= I;
                    end;
            vtText: begin
                      FParDat.ReadBuffer(IdByte, 1);
                      SetLength(S, IdByte);
                      FParDat.ReadBuffer(S[1], IdByte);
                      Value:= S;
                    end;
        vtDateTime,
            vtTime,
            vtDate: begin
                      FParDat.ReadBuffer(DT, SizeOf(Double));
                      DT_:= DT;
                      Value:= DT_;
                    end;
           vtFloat: begin
                      FParDat.ReadBuffer(E, SizeOf(Extended));
                      Value:= E;
                    end;
      end;
      Exit;
    end;
    FParDat.Position:= FParDat.Position + Shift - 3;
  until FParDat.Position >= FParDat.Size;
end;

// -------------< ������ � ������� >------------------------------------

procedure TAvk11DatSrc.FixShift(Body: TMemoryStream; EndOffset: Integer = MaxInt - 1);
var
  Dat1: Word;
  Dat2: Word;
  StPage: Integer;
  LastSave: Integer;

begin
  if Body.Position + 1 > Body.Size then Exit;
  if Body.Position + 1 > EndOffset + 1 then Exit;

  Dat1:= 0;
  Dat2:= 0;
  Body.ReadBuffer(Dat1, 1);                                // ��������� 1-� ����

  // ��� �������� ����
  // ��������� ���� ���� - ��� �����
  // ��������� ���� ����                 < -----------------------------:
  // ����� ����� ����������� ���� � 1 ��� �� ������������ ������        :
  // �������� � ���������� ��������� ����� ����������� ���� �� �����    :
  // ������������� ������� �� ���� �� ������� ����� 1 ���               :
  //                                               ----------------------

  while True do
  begin                                                    // � ������ ����� ������� ���������� �� 2-� ���� (������ ���� ��������)
    Dat1:= Dat1 shl 1;                                     // �������� ����
    if not ((Body.Position = Body.Size) or (Body.Position = EndOffset + 1)) then
    begin
      Body.ReadBuffer(Dat2, 1);                              // ��������� 2-� ����
      Dat1:= Dat1 and $FE + Ord((Dat2 and $80) <> 0);        // ��������� ��������� 1-� ���� ����� ����� �� 2-�� �����
      Body.Position:= Body.Position - 2;                     // ������ ��� ����� ��� �� �������� ��������� 1-� ����
      Body.WriteBuffer(Dat1, 1);                             // ���������� 1-� ����
    end
    else
    begin
      Dat1:= Dat1 and $FE + 1;                               // ��������� ��������� 1-� ���� ����� ����� = 1
      Body.Position:= Body.Position - 1;                     // ������ ��� ����� ��� �� �������� ��������� 1-� ����
      Body.WriteBuffer(Dat1, 1);                             // ���������� 1-� ����
      Break;
    end;
    Body.Position:= Body.Position + 1;                       // ������ ��� ������ ��� �� ������� ���������� �� 2-� ����
    Dat1:= Dat2;                                             // ��������� �� 2-�� � 1-� ����
  end;
end;

  // ��������� �������� ���� � ��������� �����

procedure TAvk11DatSrc.SetBit(Body: TMemoryStream; Offset: Integer; State: Boolean);
var
  Dat1: Byte;

begin
  Body.Position:= Offset;
  Body.ReadBuffer(Dat1, 1);
  if State then Dat1:= Dat1 or 1
           else Dat1:= Dat1 and $FE;
  Body.Position:= Offset;
  Body.WriteBuffer(Dat1, 1);                             // ���������� 1-� ����
end;

function TAvk11DatSrc.FixBags: Boolean;
var
  I, J, K, L: Integer;
  Log: TStringList;
  Tmp: TMemoryStream;
  StOffset: Integer;
  EndOffset: Integer;
  Len: Integer;
  P1: Pointer;
  P2: Pointer;
  Flg: Boolean;
  Flg_: Boolean;
  Test1, Test2: TStreamTest;
  FErrorsEvt: array of Integer;

procedure AddToLog(S: string);
begin
  if Assigned(Log) then Log.Add(S);
end;

begin
  if ErrorCount > 100 then ReAnalyze;

  SetLength(FErrorsEvt, ErrorCount);
  for I:= 0 to ErrorCount - 1 do                            // ������������� ������
    for J:= EventCount - 1 downto 0 do                      // ������������� �������
      if (not (EventsData[J].Event.ID in [EID_FwdDir, EID_LinkPt, EID_BwdDir, EID_EndBM])) and
         (EventsData[J].Event.OffSet < Error[I].Offset) then
      begin
        FErrorsEvt[I]:= J;
        Break;
      end;

  Log:= nil;
//  Log:= TStringList.Create;
  for I:= 0 to High(FErrorsEvt) do                      // ������������� ������� ����� �������
  begin

    AddToLog('');
    AddToLog(Format('������ �%d - �������� %x;', [I, Error[I].Offset, FErrorsEvt[I]]));
    AddToLog('');
    AddToLog('��������� ���� �� �� ���������� ������� �� ����� ���� ������� ����������');

    Flg_:= False;
    repeat
      AddToLog('���� ...');

      Test1:= TStreamTest.Create;                                  // ������� ������ �������
      FFileBody.Position:= EventsData[FErrorsEvt[I]].Event.OffSet;
      Test1.Test(FFileBody, Self.Error[I].Offset + 10);

      Flg:= False;
      for J:= Test1.Count - 1 downto 0 do
        if Test1.Items[J].Group in [fi_Coord {, fi_Mode} ] then // ���� ��������� ������� ����������
        begin
          Flg:= True;
          Break;
        end;
      if Flg then
      begin
        AddToLog(Format('����� !!! ������� %d �������', [FErrorsEvt[I]]));
        Break;
      end;
      Test1.Free;
      if not Flg then
      begin
        AddToLog(Format('�� ����� !!! ������� %d �� �������', [FErrorsEvt[I]]));
        AddToLog(Format('������� � ����������� ������� %d', [FErrorsEvt[I] - 1]));
        if FErrorsEvt[I] > 0 then Dec(FErrorsEvt[I]) else
        begin
          AddToLog('�� ����� - ������ ������� ��� !!!');
          Flg_:= True;
          Break;
        end;
      end;
    until False;

    if Flg_ then Continue;

    AddToLog('');
    AddToLog(Format('������� ������� ����� ������� - ����� � %d; �������� %x', [FErrorsEvt[I], EventsData[FErrorsEvt[I]].Event.OffSet]));
    AddToLog('������ �� ������� ���� ����� ... ');
    AddToLog('');
    AddToLog('');

    Test1:= TStreamTest.Create;
    FFileBody.Position:= EventsData[FErrorsEvt[I]].Event.OffSet;

    Test1.Test(FFileBody, Self.Error[I].Offset + 10);
    if Assigned(Log) then Test1.MakeLog(Log);
    AddToLog('');

    for J:= Test1.Count - 1 downto 0 do
      if Test1.Items[J].Group in [fi_Coord {, fi_Mode} ] then // ���� ��������� ������� ����������
      begin
        L:= 1;
        AddToLog(Format('��������� ������� ���������� - �������� %x', [Test1.Items[J].Offset]));
        for K:= J + 1 to Test1.Count - 1 do
          if (Test1.Items[K].Group = fi_Signal) or  // ���� ������ ...
             Test1.Items[K].Error then              // ��� ������
          begin
            AddToLog('');
            AddToLog(Format('%d - � ������ ����� ���������� - �������� %x', [L, Test1.Items[K].Offset]));
            Inc(L);
            StOffset:= Test1.Items[K].Offset;

            EndOffset:= 1046 * Trunc(Test1.Items[K].Offset / 1046) + 1045;
            if EndOffset > FFileBody.Size - 1 then EndOffset:= FFileBody.Size - 1;

            Len:= EndOffset - StOffset + 1;

            Tmp:= TMemoryStream.Create;
            Tmp.SetSize(Len);

            P1:= Pointer(Integer(FFileBody.Memory) + Test1.Items[K].Offset);
            P2:= Tmp.Memory;

            Move(P1^, P2^, Len);
            Tmp.Position:= 0;
            FixShift(Tmp);

            AddToLog(Format('������������ ����� - � %x �� %x ����� %d', [StOffset, EndOffset, Len]));
            AddToLog(Format('��������� ��������� ���� - � %x �� %x ����� %d', [StOffset, EndOffset, Len]));
            AddToLog('');
            AddToLog('');

            Tmp.Position:= 0;
            Test2:= TStreamTest.Create;
            Test2.Test(Tmp, Tmp.Size, True);
            if Assigned(Log) then Test2.MakeLog(Log, True);
            Flg:= Test2.Error;
            Test2.Free;
            Tmp.Free;

            AddToLog('');
            if Flg then AddToLog('���� ������');
            if not Flg then
            begin
              AddToLog('������ ���');
              AddToLog(Format('������������ ����� - � %x �� %x ����� %d', [StOffset, EndOffset, Len]));
              FFileBody.Position:= StOffset;
              FixShift(FFileBody, EndOffset);

              AddToLog('');
              AddToLog('������ ������� ��� ���������� ����� � 0');
              SetBit(FFileBody, EndOffset, False);
              AddToLog('');
              AddToLog('��������� ��� ���� + 100 ����');
              AddToLog('');
              Test2:= TStreamTest.Create;
              FFileBody.Position:= StOffset;
              Test2.Test(FFileBody, EndOffset + 100);
              if Assigned(Log) then Test2.MakeLog(Log);
              Flg:= Test2.Error;
              Test2.Free;
              if Flg then
              begin
                AddToLog('');
                AddToLog('���� ������');
                AddToLog('');
                AddToLog('������ ��� ���������� ����� � 1');
                AddToLog('');
                SetBit(FFileBody, EndOffset, True);
                AddToLog('��������� ��� ���� + 100 ����');
                AddToLog('');
                Test2:= TStreamTest.Create;
                FFileBody.Position:= StOffset;
                Test2.Test(FFileBody, EndOffset + 100);
                if Assigned(Log) then Test2.MakeLog(Log);
                Flg:= Test2.Error;
                Test2.Free;
                AddToLog('');
                if Flg then AddToLog('���� ������ - ��� ������ �� ���� - ��������� ��� ����')
                       else AddToLog('������ ��� - ������� ��� ��������� - 1');
              end
              else AddToLog('������ ��� - ������� ��� ��������� - 0');
              Result:= True;
              Break;
            end;
          end;
        Break;
      end;

    Test1.Free;
    AddToLog('');
    AddToLog('-------------------------------------------------------------------');
    AddToLog('');
  end;
  if Assigned(Log) then
  begin
    Log.SaveToFile(ExtractFilePath(FileName) + 'LogFile.txt');
    Log.Free;
  end;
  if Result then FFileBody.SaveToFile(FFileName);
end;

function TAvk11DatSrc.NormalizeDisCoord(Src: Integer): Integer;
begin
  if Src < 0 then Result:= 0 else
    if Src > MaxDisCoord then Result:= MaxDisCoord else Result:= Src;
end;

function TAvk11DatSrc.GetBodyModify: Boolean;
begin
  Result:= FExHeader.Modify = 0;
end;

procedure TAvk11DatSrc.SetBodyModify(NewState: Boolean);
begin
  FModifyData:= True;
  case NewState of
   False: FExHeader.Modify:= $FF;
    True: FExHeader.Modify:= 0;
  end;
end;

procedure TAvk11DatSrc.FillTimeList;
var
  I: Integer;
  ID: Byte;
  M, H: Integer;
  OldH: Integer;
  OldM: Integer;
  pData: PEventData;

begin
  OldH:= (Header.Hour shr 4 and $0F) * 10 + (Header.Hour and $0F);
  OldM:= (Header.Minute shr 4 and $0F) * 10 + (Header.Minute and $0F);
  SetLength(FTimeList, 1);
  FTimeList[High(FTimeList)].H:= OldH;
  FTimeList[High(FTimeList)].M:= OldM;
  FTimeList[High(FTimeList)].DC:= 0;

  for I:= 0 to High(FEvents) do
  begin
    GetEventData(I, ID, pData);
    if ID in [    EID_Stolb, EID_StBoltStyk,  EID_EndLineLabel,
                EID_Strelka, EID_EndBoltStyk, EID_StSwarStyk,
               EID_DefLabel, EID_Time,        EID_EndSwarStyk,
              EID_TextLabel, EID_StLineLabel ] then
    begin
      H:= pData^[Avk11EventLen[ID] - 1];
      M:= pData^[Avk11EventLen[ID] - 0];

      H:= (H shr 4 and $0F) * 10 + (H and $0F);
      M:= (M shr 4 and $0F) * 10 + (M and $0F);

      if (H <> OldH) or (M <> OldM) then
      begin
        SetLength(FTimeList, Length(FTimeList) + 1);
        FTimeList[High(FTimeList)].H:= H;
        FTimeList[High(FTimeList)].M:= M;
        FTimeList[High(FTimeList)].DC:= FEvents[I].DisCoord;
        OldH:= H;
        OldM:= M;
      end;
    end;
  end;
end;

procedure TAvk11DatSrc.FillEventsData;
const
  KuRek: array [0..9] of Integer = (14, 14, 12, 12, 16, 16, 14, 14, 16, 16);

var
  I: Integer;
  ID: Byte;
  Params_: TMiasParams;
  HSItem: THSItem;
  Echo: array [1..6] of Byte;
  pData: PEventData;
  R: RRail;
  Flag: Boolean;
  LastKm: Integer;
  LastPk: Integer;
  SkipBytes: Integer;
  ReAn: Boolean;
  SkipFlag: Boolean;

begin
  FillChar(Params_, SizeOf(Params_), 0); // ������������ ������ ��������� ��������
  for R:= r_Left to r_Right do
    for I:= 0 to 9 do Params_.HeadPh[R, I]:= True;

  ReAn:= False;
  FMaxSysCoord:= - MaxInt;
  Flag:= False;
  FCanSkipBM:= False;
  SetLength(FEventsData, Length(FEvents));

  SkipFlag:= False;

  for I:= 0 to High(FEvents) do
  begin
    FMaxSysCoord:= Max(FMaxSysCoord, FEvents[I].SysCoord);

    if FEvents[I].ID = EID_BSAmpl then
    begin
      FFileBody.Position:= FEvents[I].OffSet + 1;
      FFileBody.ReadBuffer(FEventsData[I].Data[1], SizeOf(mBSAmplHead));
      SetLength(FBSAmplLink, Length(FBSAmplLink) + 1);
      FBSAmplLink[High(FBSAmplLink)].StDisCrd:= FEvents[I].DisCoord - pBSAmplHead(@FEventsData[I].Data[1])^.Len;
      FBSAmplLink[High(FBSAmplLink)].EdDisCrd:= FEvents[I].DisCoord;
      FBSAmplLink[High(FBSAmplLink)].Len:= pBSAmplHead(@FEventsData[I].Data[1])^.Len;
      FBSAmplLink[High(FBSAmplLink)].Offset1:= FEvents[I].OffSet + 1 + SizeOf(mBSAmplHead);
      FBSAmplLink[High(FBSAmplLink)].Offset2:= FEvents[I].OffSet + 1 + SizeOf(mBSAmplHead) + pBSAmplHead(@FEventsData[I].Data[1])^.Len;
    end;

    FEventsData[I].Event:= FEvents[I];
    if (FEvents[I].ID in  [EID_HandScan    ,  EID_Sens       ,  EID_Att        , EID_VRU         , EID_2Tp_Word,
                           EID_StStr       ,  EID_EndStr     ,  EID_HeadPh     , EID_Mode        ,
                           EID_2Tp         ,  EID_ZondImp    ,  EID_SetRailType, EID_Stolb       ,
                           EID_Strelka     ,  EID_DefLabel   ,  EID_TextLabel  , EID_StBoltStyk  ,
                           EID_EndBoltStyk ,  EID_Time       ,  EID_StLineLabel, EID_EndLineLabel,
                           EID_StSwarStyk  ,  EID_EndSwarStyk,  EID_OpChange   , EID_PathChange  ,
                           EID_MovDirChange,  EID_SezdNo     ,  EID_LongLabel  , EID_GPSCoord   , EID_GPSState    ,
                           EID_GPSError    ,  {EID_NewHeader  ,}  EID_EndFile   {, EID_BSAmpl}])
                           and (FEvents[I].OffSet + Avk11EventLen[FEvents[I].ID] + 1 < FFileBody.Size) then

    begin
      FFileBody.Position:= FEvents[I].OffSet + 1;
      if Avk11EventLen[FEvents[I].ID] <> - 1 then
        FFileBody.ReadBuffer(FEventsData[I].Data[1], Avk11EventLen[FEvents[I].ID]);
    end;

    if FEvents[I].ID = EID_EndBM then FCanSkipBM:= True;
    if FEvents[I].ID in [EID_FwdDir, EID_BwdDir] then Flag:= True;

    if FEvents[I].ID = EID_NewHeader then
      if FEvents[I].OffSet = 128 then
      begin
        FFileBody.Position:= FEvents[I].OffSet + 1;
        FFileBody.ReadBuffer(SkipBytes, 4);
        FParDat.Clear;
        FParDat.SetSize(SkipBytes);
        FFileBody.ReadBuffer(FParDat.Memory^, SkipBytes);
      end
      else ReAn:= True;

    if (FEvents[I].ID = EID_HandScan) or    // � ����� !!!!
       ((FEvents[I].ID = EID_Mode) and
        (FEventsData[I].Data[1] in [4, 60, 61, 62, 63, 64, 65, 66])) then
    begin
      SetLength(HSList, Length(HSList) + 1);

      FileBody.Position:= FEvents[I].OffSet + 1;
      with HSList[High(HSList)] do
      begin

        if FEvents[I].ID = EID_HandScan then HSMode:= 1;
        if (FEvents[I].ID = EID_Mode) and
           (FEventsData[I].Data[1] in [4, 60, 61, 62, 63, 64, 65, 66]) then
        begin
          case FEventsData[I].Data[1] of
             4: HSCh:= - 1;
            60: HSCh:= 0;
            61: HSCh:= 1;
            62: HSCh:= 2;
            63: HSCh:= 3;
            64: HSCh:= 4;
            65: HSCh:= 5;
            66: HSCh:= 6;
          end;
          HSMode:= 2;
        end;
        EventIdx:= I;
        DisCoord:= FEvents[I].DisCoord;

        if HSMode = 1 then // ���� ���� ������ ������� �� ��������� ������
        begin
          FileBody.ReadBuffer(Header, 6);
          repeat
            FileBody.ReadBuffer(HSItem, 3);
            if (Length(Samples) = 0) or (HSItem.Sample <> Samples[High(Samples)].Idx) then
              SetLength(Samples, Length(Samples) + 1);

            Samples[High(Samples)].Count:= 0;
            Samples[High(Samples)].Idx:= HSItem.Sample;

            with Samples[High(Samples)] do
              case HSItem.Count of
                1: begin
                     FileBody.ReadBuffer(Echo, 2);
                     if Count < 16 then Inc(Count);
                     Delay[Count]:= Echo[1];
                     Ampl[Count]:= Echo[2] shr 4 and $0F;
                   end;
                2: begin
                     FileBody.ReadBuffer(Echo, 3);
                     if Count < 16 then Inc(Count);
                     Delay[Count]:= Echo[1];
                     Ampl[Count]:= Echo[3] shr 4 and $0F;
                     if Count < 16 then Inc(Count);
                     Delay[Count]:= Echo[2];
                     Ampl[Count]:= Echo[3] and $0F;
                   end;
                3: begin
                     FileBody.ReadBuffer(Echo, 5);
                     if Count < 16 then Inc(Count);
                     Delay[Count]:= Echo[1];
                     Ampl[Count]:= Echo[4] shr 4 and $0F;
                     if Count < 16 then Inc(Count);
                     Delay[Count]:= Echo[2];
                     Ampl[Count]:= Echo[4] and $0F;
                     if Count < 16 then Inc(Count);
                     Delay[Count]:= Echo[3];
                     Ampl[Count]:= Echo[5] shr 4 and $0F;
                   end;
                4: begin
                     FileBody.ReadBuffer(Echo, 6);
                     if Count < 16 then Inc(Count);
                     Delay[Count]:= Echo[1];
                     Ampl[Count]:= Echo[5] shr 4 and $0F;
                     if Count < 16 then Inc(Count);
                     Delay[Count]:= Echo[2];
                     Ampl[Count]:= Echo[5] and $0F;
                     if Count < 16 then Inc(Count);
                     Delay[Count]:= Echo[3];
                     Ampl[Count]:= Echo[6] shr 4 and $0F;
                     if Count < 16 then Inc(Count);
                     Delay[Count]:= Echo[4];
                     Ampl[Count]:= Echo[6] and $0F;
                   end;
              end;
          until HSItem.Sample = $FFFF;
        end;

      end;
    end;

    if FEventsData[I].Event.ID = EID_Mode then // ��������� ������
    begin
      if FEventsData[I].Data[1] in [60..66, 70..76] then SkipFlag:= True;
      if FEventsData[I].Data[1] in [1, 2, 7, 12, 20..59] then SkipFlag:= False;
    end;

    if (not SkipFlag) or (SkipFlag and not (FEventsData[I].Event.ID in [EID_Sens, EID_Att, EID_VRU, EID_StStr, EID_EndStr, EID_2Tp])) then
      with FEventsData[I], FEventsData[I].Event do
        case ID of
         EID_Sens: Params_.Sens   [SideToRail(ByteToSide(ID, Data[1])), ByteToCh(ID, Data[1])]:= ShortInt(Data[2]);
          EID_Att: begin
                     Params_.Att[SideToRail(ByteToSide(ID, Data[1])), ByteToCh(ID, Data[1])]:= ShortInt(Data[2]);
                     if ((Header.ID = $FF12) or (Header.ID = $FF14)) and
                        (FEventsData[I].Event.Discoord > 0) then
                     begin
                       if Params_.Sens   [SideToRail(ByteToSide(ID, Data[1])), ByteToCh(ID, Data[1])] + KuRek[ByteToCh(ID, Data[1])] > 60
                         then Params_.Sens   [SideToRail(ByteToSide(ID, Data[1])), ByteToCh(ID, Data[1])]:= 60 - ShortInt(Data[2])
                         else Params_.Sens   [SideToRail(ByteToSide(ID, Data[1])), ByteToCh(ID, Data[1])]:= KuRek[ByteToCh(ID, Data[1])];
                     end;
                   end;
          EID_VRU: Params_.Vru    [SideToRail(ByteToSide(ID, Data[1])), ByteToCh(ID, Data[1])]:= Data[2];
        EID_StStr: Params_.StStr  [SideToRail(ByteToSide(ID, Data[1])), ByteToCh(ID, Data[1])]:= Data[2] - 1;
       EID_EndStr: Params_.EdStr  [SideToRail(ByteToSide(ID, Data[1])), ByteToCh(ID, Data[1])]:= Data[2] - 1;
          EID_2Tp: Params_.DwaTp  [SideToRail(ByteToSide(ID, Data[1])), ByteToCh(ID, Data[1])]:= Data[2];
     EID_2Tp_Word: Params_.DwaTp  [SideToRail(ByteToSide(ID, Data[1])), ByteToCh(ID, Data[1])]:= Data[2] + $100 * Data[3];
      EID_ZondImp: Params_.ZondImp[SideToRail(ByteToSide(ID, Data[1])), ByteToCh(ID, Data[1])]:= Data[2];
         EID_Mode: Params_.Mode:= Data[1];
       EID_HeadPh: begin
                     Params_.HeadPh[SideToRail(sLeft), 0]:= (Data[1] and  32) <> 0; // ����� 0 - 1 byte / 5 bit
                     Params_.HeadPh[SideToRail(sLeft), 1]:= (Data[1] and  16) <> 0; // ����� 1 - 1 byte / 4 bit
                     Params_.HeadPh[SideToRail(sLeft), 2]:= (Data[1] and   8) <> 0; // ����� 2 - 1 byte / 3 bit
                     Params_.HeadPh[SideToRail(sLeft), 3]:= (Data[1] and   4) <> 0; // ����� 3 - 1 byte / 2 bit
                     Params_.HeadPh[SideToRail(sLeft), 4]:= (Data[1] and   2) <> 0; // ����� 4 - 1 byte / 1 bit
                     Params_.HeadPh[SideToRail(sLeft), 5]:= (Data[1] and   1) <> 0; // ����� 5 - 1 byte / 0 bit
                     Params_.HeadPh[SideToRail(sLeft), 6]:= (Data[2] and 128) <> 0; // ����� 6 - 2 byte / 7 bit
                     Params_.HeadPh[SideToRail(sLeft), 7]:= (Data[2] and  64) <> 0; // ����� 7 - 2 byte / 6 bit
                     Params_.HeadPh[SideToRail(sLeft), 8]:= (Data[2] and  32) <> 0; // ����� 8 - 2 byte / 5 bit
                     Params_.HeadPh[SideToRail(sLeft), 9]:= (Data[2] and  16) <> 0; // ����� 9 - 2 byte / 4 bit

                     Params_.HeadPh[SideToRail(sRight), 0]:= (Data[3] and  32) <> 0; // ����� 0 - 1 byte / 5 bit
                     Params_.HeadPh[SideToRail(sRight), 1]:= (Data[3] and  16) <> 0; // ����� 1 - 1 byte / 4 bit
                     Params_.HeadPh[SideToRail(sRight), 2]:= (Data[3] and   8) <> 0; // ����� 2 - 1 byte / 3 bit
                     Params_.HeadPh[SideToRail(sRight), 3]:= (Data[3] and   4) <> 0; // ����� 3 - 1 byte / 2 bit
                     Params_.HeadPh[SideToRail(sRight), 4]:= (Data[3] and   2) <> 0; // ����� 4 - 1 byte / 1 bit
                     Params_.HeadPh[SideToRail(sRight), 5]:= (Data[3] and   1) <> 0; // ����� 5 - 1 byte / 0 bit
                     Params_.HeadPh[SideToRail(sRight), 6]:= (Data[4] and 128) <> 0; // ����� 6 - 2 byte / 7 bit
                     Params_.HeadPh[SideToRail(sRight), 7]:= (Data[4] and  64) <> 0; // ����� 7 - 2 byte / 6 bit
                     Params_.HeadPh[SideToRail(sRight), 8]:= (Data[4] and  32) <> 0; // ����� 8 - 2 byte / 5 bit
                     Params_.HeadPh[SideToRail(sRight), 9]:= (Data[4] and  16) <> 0; // ����� 9 - 2 byte / 4 bit
                   end;
        end;
    FEventsData[I].Params:= Params_;
  end;

  if not Flag then FCanSkipBM:= True;

  SetLength(FCDList, 0);
  for I:= 0 to High(FEventsData) do
  begin
    if (FEventsData[I].Event.ID = EID_FwdDir) or
       (FEventsData[I].Event.ID = EID_BwdDir) then
    begin
      SetLength(FCDList, Length(FCDList) + 1);
      FCDList[High(FCDList)]:= I;
    end;
  end;
  SetLength(FCDList, Length(FCDList) + 1);
  FCDList[High(FCDList)]:= High(FEventsData);

  FillTimeList;

{
  OldH:= (Header.Hour shr 4 and $0F) * 10 + (Header.Hour and $0F);
  OldM:= (Header.Minute shr 4 and $0F) * 10 + (Header.Minute and $0F);
  SetLength(FTimeList, 1);
  FTimeList[High(FTimeList)].H:= OldH;
  FTimeList[High(FTimeList)].M:= OldM;
  FTimeList[High(FTimeList)].DC:= 0;

  for I:= 0 to High(FEvents) do
  begin
    GetEventData(I, ID, pData);
    if ID in [    EID_Stolb, EID_StBoltStyk,  EID_EndLineLabel,
                EID_Strelka, EID_EndBoltStyk, EID_StSwarStyk,
               EID_DefLabel, EID_Time,        EID_EndSwarStyk,
              EID_TextLabel, EID_StLineLabel ] then
    begin
      H:= pData^[Avk11EventLen[ID] - 1];
      M:= pData^[Avk11EventLen[ID] - 0];

      H:= (H shr 4 and $0F) * 10 + (H and $0F);
      M:= (M shr 4 and $0F) * 10 + (M and $0F);

      if (H <> OldH) or (M <> OldM) then
      begin
        SetLength(FTimeList, Length(FTimeList) + 1);
        FTimeList[High(FTimeList)].H:= H;
        FTimeList[High(FTimeList)].M:= M;
        FTimeList[High(FTimeList)].DC:= FEvents[I].DisCoord;
        OldH:= H;
        OldM:= M;
      end;
    end;
  end;
  }
// ----------------------------------------------------------------------------------

  SetLength(FCoordList, 1);
  FCoordList[0].KM:= Header.StartKM;
  with FCoordList[High(FCoordList)] do
  begin
    SetLength(Content, Length(Content) + 1);
    Content[High(Content)].Pk:= Header.StartPk;
    Content[High(Content)].Idx:= 0;
    LastKm:= High(FCoordList);
    LastPk:= High(Content);
  end;

  LastKm:= 0;
  LastPk:= 0;

  for I:= 0 to EventCount - 1 do
  begin
    GetEventData(I, ID, pData);
    if ID = EID_Stolb then
    begin
      if pCoord(pData)^.Km[0] <> pCoord(pData)^.Km[1] then
      begin
        SetLength(FCoordList, Length(FCoordList) + 1);
        FCoordList[High(FCoordList)].KM:= pCoord(pData)^.Km[1];
        with FCoordList[High(FCoordList)] do
        begin
          SetLength(Content, Length(Content) + 1);
          if Header.MoveDir = 0 then Content[High(Content)].Pk:= 10
                                else Content[High(Content)].Pk:= 1;
          Content[High(Content)].Idx:= I;
          FCoordList[LastKm].Content[LastPk].Len:= Round((Event[I].Event.SysCoord - Event[FCoordList[LastKm].Content[LastPk].Idx].Event.SysCoord) * Header.ScanStep / 100);
          LastKm:= High(FCoordList);
          LastPk:= High(Content);
        end;
      end;
      if pCoord(pData)^.Km[0] = pCoord(pData)^.Km[1] then
        with FCoordList[High(FCoordList)] do
        begin
          SetLength(Content, Length(Content) + 1);
          Content[High(Content)].Pk:= pCoord(pData)^.Pk[1];
          Content[High(Content)].Idx:= I;
          FCoordList[LastKm].Content[LastPk].Len:= Round((Event[I].Event.SysCoord - Event[FCoordList[LastKm].Content[LastPk].Idx].Event.SysCoord) * Header.ScanStep / 100);
          LastKm:= High(FCoordList);
          LastPk:= High(Content);
        end;
    end;
  end;

  FCoordList[LastKm].Content[LastPk].Len:= Round((DisToSysCoord(MaxDisCoord) - Event[FCoordList[LastKm].Content[LastPk].Idx].Event.SysCoord) * Header.ScanStep / 100);

  SetLength(LabelList, 0);
  for I:= 0 to EventCount - 1 do
    if Event[I].Event.ID in [EID_Stolb, EID_Strelka, EID_DefLabel, EID_TextLabel] then
    begin
      SetLength(LabelList, Length(LabelList) + 1);
      LabelList[High(LabelList)]:= I;
    end;

  if ReAn then
  begin
    ReAnalyze;
    FillEventsData;
  end;
end;

function TAvk11DatSrc.GetSysCoordByRealIdx(KmIdx, PkIdx: Integer; Metr: Extended): Integer;
begin
  case Header.MoveDir of
    0: begin
         if PkIdx < High(CoordList[KmIdx].Content)
           then Result:= Event[CoordList[KmIdx].Content[PkIdx + 1].Idx].Event.SysCoord
           else if KmIdx < High(CoordList) then Result:= Event[CoordList[KmIdx + 1].Content[0].Idx].Event.SysCoord
                                           else Result:= DisToSysCoord(MaxDisCoord) + 100 * (100000 - ExHeader.EndMM) div Header.ScanStep;
         Result:= Result - Round(Metr * 1000 / (Header.ScanStep / 100));
       end;
    1: Result:= Event[CoordList[KmIdx].Content[PkIdx].Idx].Event.SysCoord + Round((Metr - Ord((KmIdx = 0) and (PkIdx = 0)) * (FHeader.StartMetre + StartShift)) * 1000 / (Header.ScanStep / 100));

  end;
end;

function TAvk11DatSrc.GetSysCoordByReal(Km, Pk: Integer; Metr: Extended): Integer;
var
  I: Integer;
  J: Integer;

begin
  for I:= 0 to High(FCoordList) do
    if FCoordList[I].KM = Km then
      for J:= 0 to High(FCoordList[I].Content) do
        if FCoordList[I].Content[J].Pk = Pk then
        begin
          Result:= GetSysCoordByRealIdx(I, J, Metr);
          Exit;
        end;
end;

procedure TAvk11DatSrc.DisToFileOffset(NeedDisCoord: Integer; var DisCoord: Int64; var SysCoord: Integer; var Offset: Integer);
var
  R: RRail;
  I: Integer;
  LastSysCoord: Integer;
  SaveOffset: Integer;
  SkipBytes: Integer;
  CurSysCoord: Integer;
  CurDisCoord: Int64;
  LoadID: Byte;
  LoadByte: array [1..22] of Byte;
  LoadLongInt: array [1..5] of LongInt; // absolute LoadByte[1];
  HSHead: THSHead;
  HSItem: THSItem;
  StartIdx, EndIdx: Integer;
  DisCoord_: Integer;
  SysCoord_: Integer;
  Offset_: Integer;
  CurDisCoord_: Integer;
  CurSysCoord_: Integer;
  CurPosition_: Integer;
  BSHead: mBSAmplHead;

  SameCoord: TIntegerDynArray;

  BMSkipFlag: Boolean;
  Tmp: Boolean;

begin
  NeedDisCoord:= NormalizeDisCoord(NeedDisCoord);
// GetEventIdx(NeedDisCoord, NeedDisCoord, StartIdx, EndIdx);

                                                  // ���� �� ���������� ������ � �����
                                                  // ��� ����� - ������� ��������� ������� � ������ ��� ������� �����������
  StartIdx:= GetLeftEventIdx(NeedDisCoord);

  I:= FEventsData[StartIdx].Event.DisCoord;
  while (I = FEventsData[StartIdx].Event.DisCoord) and (StartIdx <> 0) do Dec(StartIdx);

  for I:= 1 to 5 do                               // ������ ����� �� ������� � ������ ��������
  begin
    if (StartIdx <> 0) and
       ((FEventsData[StartIdx].Event.Pack[r_Left].Ampl = $FF) or
        (FEventsData[StartIdx].Event.Pack[r_Right].Ampl = $FF)) then Dec(StartIdx)
                                                                else Break;
  end;



(*
                                                  // ���� ����� ������ ������ ������ ������ ��� �������� ����������
                                                  // ��� ����� ����������� ��� ������� � ������ �����������
  while (NeedDisCoord = FEventsData[StartIdx].Event.DisCoord) and (StartIdx <> 0) do Dec(StartIdx);
  I:= FEventsData[StartIdx].Event.DisCoord;
  while (I = FEventsData[StartIdx].Event.DisCoord) and (StartIdx <> 0) do Dec(StartIdx);
*)
//  StartIdx:= GetLeftEventIdx(NeedDisCoord - 1);

//  if StartIdx > 0 then Dec(StartIdx);
//  if StartIdx > 0 then Dec(StartIdx);

//  GetNearestEventIdx(NeedDisCoord, StartIdx, EndIdx, SameCoord);
//  if Length(SameCoord) <> 0 then StartIdx:= SameCoord[0];

  if StartIdx = - 1 then
  begin
    DisCoord:= 0;
    SysCoord:= 0;
    Offset:= 0;
    Exit;
  end;

  FFileBody.Position:= FEventsData[StartIdx].Event.OffSet;
  LastSysCoord:= FEventsData[StartIdx].Event.SysCoord;
  CurSysCoord:= FEventsData[StartIdx].Event.SysCoord;
  CurDisCoord:= FEventsData[StartIdx].Event.DisCoord;
  FPack:= FEventsData[StartIdx].Event.Pack;

  ToLog(Format('Start Event: %d', [StartIdx]));                              // ----------------------------------------------------------------------------------------

  FillChar(FCurEcho, SizeOf(TCurEcho), 0);

  if CurDisCoord >= NeedDisCoord then
  begin
    DisCoord:= CurDisCoord;
    SysCoord:= CurSysCoord;
    Offset:= FFileBody.Position;

        ToLog(Format('Rail: 0, BS: %d', [Ord(FPack[r_Left].Ampl <> 255)]));  // ----------------------------------------------------------------------------------------
        ToLog(Format('Rail: 1, BS: %d', [Ord(FPack[r_Right].Ampl <> 255)])); // ----------------------------------------------------------------------------------------

    Exit;
  end;

  TestOffsetFirst;
  if FSkipBM then TestBMOffsetFirst;

  for R:= r_Left to r_Right do
  begin
    FCurEcho[R, 1].Count:= Ord(FPack[R].Ampl <> 255);
    FCurEcho[R, 1].Delay[1]:= FPack[R].Delay;
    FCurEcho[R, 1].Ampl[1]:= FPack[R].Ampl;
  end;

  BMSkipFlag:= False;
  repeat
    if not TestOffsetNext then Exit;
    if FSkipBM then
    begin
      Tmp:= BMSkipFlag;
      BMSkipFlag:= TestBMOffsetNext;
      if Tmp and (not BMSkipFlag) then
        for R:= r_Left to r_Right do
          FCurEcho[R, 1].Count:= 0;
    end;

    SaveOffset:= FFileBody.Position;

    CurDisCoord_:= CurDisCoord;
    CurSysCoord_:= CurSysCoord;
    CurPosition_:= FFileBody.Position;

    FFileBody.ReadBuffer(LoadID, SizeOf(LoadID));
    SkipBytes:= - 1;
    if LoadID and 128 = 0 then
    begin
      if LoadID shr 3 and $07 = 1 then    // ���� ������ ����� �� �������������
      begin
        FSrcDat[0]:= LoadID;
        FFileBody.ReadBuffer(FSrcDat[1], Avk11EchoLen[LoadID and $07]);
        DataToEcho;
      end
      else if LoadID shr 3 and $07 = 0 then SkipBytes:= 1   // ���� ������� �� ���������� 1 ����
      else SkipBytes:= Avk11EchoLen[LoadID and $07]; // ����� ����������
    end
    else
    begin
      if not BMSkipFlag then
      begin
        case LoadID of

       EID_HandScan: begin           // ������
                       FFileBody.ReadBuffer(HSHead, 6);
                       repeat
                         FFileBody.ReadBuffer(HSItem, 3);
                         if HSItem.Count <= 4 then FFileBody.ReadBuffer(HSItem.Data[0], Avk11EchoLen[HSItem.Count]);
                       until HSItem.Sample = $FFFF;
                     end;

      EID_NewHeader: begin
                       FFileBody.ReadBuffer(SkipBytes, 4);
                       FFileBody.Position:= FFileBody.Position + SkipBytes;
                       SkipBytes:= - 1;
                     end;

      EID_BSAmpl:    begin
                       FFileBody.ReadBuffer(BSHead, SizeOf(mBSAmplHead));
                       FFileBody.Position:= FFileBody.Position + BSHead.Len * 2;
                       SkipBytes:= - 1;
                     end;

                     EID_ACLeft      ,
                     EID_ACRight     ,  EID_Sens       ,  EID_Att         , EID_VRU         ,
                     EID_StStr       ,  EID_EndStr     ,  EID_HeadPh      , EID_Mode        , EID_2Tp_Word    ,
                     EID_2Tp         ,  EID_ZondImp    ,  EID_SetRailType , EID_Stolb       , EID_Strelka     ,
                     EID_DefLabel    ,  EID_TextLabel  ,  EID_StBoltStyk  , EID_EndBoltStyk ,
                     EID_Time        ,  EID_StLineLabel,  EID_EndLineLabel, EID_StSwarStyk  ,
                     EID_EndSwarStyk ,  EID_OpChange   ,  EID_PathChange  , EID_MovDirChange,
                     EID_SezdNo      ,  EID_LongLabel  ,  EID_GPSCoord   ,  EID_GPSState    , EID_GPSError: SkipBytes:= Avk11EventLen[LoadID];

            EID_SysCrd_SS: begin // ��������� ���������� �������� ������, �������� ����������
                             FFileBody.ReadBuffer(LoadByte[1], 2);
                             CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[2]);
                           end;
            EID_SysCrd_SF: begin // ��������� ���������� �������� ������, ������ ����������
                             FFileBody.ReadBuffer(LoadByte[1], 1);
                             FFileBody.ReadBuffer(LoadLongInt, 4);
                             CurSysCoord:= LoadLongInt[1];
                           end;
            EID_SysCrd_FS: begin // ��������� ���������� ������ ������, �������� ����������
                             FFileBody.ReadBuffer(LoadByte[1], 5);
                             CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[5]);
                           end;
            EID_SysCrd_FF: begin // ��������� ���������� ������ ������, ������ ����������
                             FFileBody.ReadBuffer(LoadByte[1], 4);
                             FFileBody.ReadBuffer(LoadLongInt, 4);
                             CurSysCoord:= LoadLongInt[1];
                           end;
              EID_EndFile: begin // ����� �����
                             DisCoord:= CurDisCoord;
                             SysCoord:= CurSysCoord;
                             Offset:= FFileBody.Position;
                             Exit;
                           end
        end;
      end
      else
      begin
        SkipBytes:= Avk11EventLen[LoadID];
        if LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF] then UnPackEcho;
      end;
    end;

    if SkipBytes <> - 1 then FFileBody.Position:= FFileBody.Position + SkipBytes;

    if (not BMSkipFlag) and (LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF]) then
    begin
      Inc(CurDisCoord, Abs(CurSysCoord - LastSysCoord)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)
      LastSysCoord:= CurSysCoord;

      {if not SkipUnPack then}
      UnPackEcho;
//      SkipUnPack:= False;

      if CurDisCoord > NeedDisCoord then
      begin
        DisCoord:= CurDisCoord_;
        SysCoord:= CurSysCoord_;
        Offset:= CurPosition_;
        FFileBody.Position:= Offset;

        ToLog(Format('Rail: 0, BS: %d', [Ord(FPack[r_Left].Ampl <> 255)]));  // ----------------------------------------------------------------------------------------
        ToLog(Format('Rail: 1, BS: %d', [Ord(FPack[r_Right].Ampl <> 255)])); // ----------------------------------------------------------------------------------------

        Break;
      end
      else
      if CurDisCoord = NeedDisCoord then
      begin
        DisCoord:= CurDisCoord;
        SysCoord:= CurSysCoord;
        Offset:= FFileBody.Position;

        ToLog(Format('Rail: 0, BS: %d', [Ord(FPack[r_Left].Ampl <> 255)]));  // ----------------------------------------------------------------------------------------
        ToLog(Format('Rail: 1, BS: %d', [Ord(FPack[r_Right].Ampl <> 255)])); // ----------------------------------------------------------------------------------------

        Break;
      end
    end;
  until False;
end;

procedure TAvk11DatSrc.LoadData(StartDisCoord, EndDisCoord, LoadShift: LongInt; BlockOk: TDataNotifyEvent);
var
  R: RRail;
  LoadID, Dat: Byte;
  I, J, SkipBytes, Offset, Ch, T: Integer;
  LastSysCoord: LongInt;
  LastDisCoord: LongInt;
  SaveLastSysCoord: LongInt;
  LoadByte: array [1..22] of Byte;
  LoadLongInt: array [1..5] of LongInt; // absolute LoadByte[1];
  HSHead: THSHead;
  HSItem: THSItem;
  CurEcho_: TCurEcho;
  NewSysCoord: LongInt;
  BMSkipFlag: Boolean;
  Tmp: Boolean;
  BSHead: mBSAmplHead;

begin
  ClearLog;
  ToLog(Format('Need DisCoord %d', [StartDisCoord])); // ----------------------------------------------------------------------------------------

  for R:= r_Left to r_Right do
  begin
    for I:= 2 to 7 do
      begin
        FCurEcho[R, I].Count:= 0;
        FCurEcho[R, I].ZerroFlag:= False;
      end;
    CurAmpl[R]:= - 1;
  end;

  DisToFileOffset(StartDisCoord - LoadShift, FCurDisCoord, FCurSysCoord, Offset);
  for R:= r_Left to r_Right do
  begin
    if not UnpackBottom then FCurEcho[R, 1].Ampl[1]:= $FF;

    FCurEcho[R, 1].Count:= Ord(FPack[R].Ampl <> 255);
    FCurEcho[R, 1].Delay[1]:= FPack[R].Delay;
    FCurEcho[R, 1].Ampl[1]:= FPack[R].Ampl;


//    ToLog(Format('Rail: %d, BS: %d', [Ord(R), Ord(FPack[R].Ampl <> 255)])); // ----------------------------------------------------------------------------------------

  end;

  ToLog(Format('Get DisCoord %d', [FCurDisCoord])); // ----------------------------------------------------------------------------------------
  ToLog(Format('Start Offset %d', [Offset])); // ----------------------------------------------------------------------------------------


  LastSysCoord:= FCurSysCoord;
  FFileBody.Position:= Offset;

  GetParamFirst(StartDisCoord - LoadShift, FCurParams);
  if not TestOffsetFirst then Exit;
  if FSkipBM then TestBMOffsetFirst;
  BMSkipFlag:= False;
  repeat
    CurCrdLongLinc:= False;
    if not TestOffsetNext then Exit;
    if FSkipBM then
    begin
      Tmp:= BMSkipFlag;
      BMSkipFlag:= TestBMOffsetNext;
      if Tmp and (not BMSkipFlag) then
        for R:= r_Left to r_Right do
          for I:= 1 to 7 do
          begin
            FCurEcho[R, I].Count:= 0;
            FCurEcho[R, I].ZerroFlag:= False;
          end;
    end;

    FCurOffset:= FFileBody.Position;
    SkipBytes:= - 1;

    if FFileBody.Position >= Header.TableLink then Exit;

    FFileBody.ReadBuffer(LoadID, 1);

    if LoadID and 128 = 0 then
    begin
      FSrcDat[0]:= LoadID;

      Ch:= LoadID shr 3 and 7;
      if Ch = 0 then
      begin
        FFileBody.ReadBuffer(Dat, 1);
        CurAmpl[SideToRail(TSide(LoadID shr 6 and 1))]:= Dat;
      end
      else
      begin
        FFileBody.ReadBuffer(FSrcDat[1], Avk11EchoLen[LoadID and $07]);
        if Ch = 1 then DataToEcho;
        with FCurEcho[SideToRail(TSide(LoadID shr 6 and 1)), Ch] do
          case LoadID and $07 of
            0: ZerroFlag:= True;
            1: begin
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[1];
                 Ampl[Count]:= FSrcDat[2] shr 4 and $0F;
               end;
            2: begin
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[1];
                 Ampl[Count]:= FSrcDat[3] shr 4 and $0F;
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[2];
                 Ampl[Count]:= FSrcDat[3] and $0F;
               end;
            3: begin
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[1];
                 Ampl[Count]:= FSrcDat[4] shr 4 and $0F;
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[2];
                 Ampl[Count]:= FSrcDat[4] and $0F;
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[3];
                 Ampl[Count]:= FSrcDat[5] shr 4 and $0F;
               end;
            4: begin
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[1];
                 Ampl[Count]:= FSrcDat[5] shr 4 and $0F;
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[2];
                 Ampl[Count]:= FSrcDat[5] and $0F;
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[3];
                 Ampl[Count]:= FSrcDat[6] shr 4 and $0F;
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[4];
                 Ampl[Count]:= FSrcDat[6] and $0F;
               end;
          end;
      end;
{
      if Ch = 1 then
        ToLog(Format('Crd: %d; R: %d; Ch: 1 Echo: %d', [FCurDisCoord, Ord(SideToRail(TSide(LoadID shr 6 and 1))), CurEcho[SideToRail(TSide(LoadID shr 6 and 1)), Ch].Count])); // ----------------------------------------------------------------------------------------
}
    end
    else
    begin
      if not BMSkipFlag then
      begin
        case LoadID of
       EID_HandScan: begin           // ������
                       FFileBody.ReadBuffer(HSHead, 6);
                       repeat
                         FFileBody.ReadBuffer(HSItem, 3);
                         if HSItem.Count <= 4 then FFileBody.ReadBuffer(HSItem.Data[0], Avk11EchoLen[HSItem.Count]);
                       until HSItem.Sample = $FFFF;
                     end;

      EID_NewHeader: if FFileBody.Position < 128 + 128 then
                     begin
                       FFileBody.ReadBuffer(SkipBytes, 4);
                       FFileBody.Position:= FFileBody.Position + SkipBytes;
                       SkipBytes:= - 1;
                     end;

      EID_BSAmpl:    begin
                       FFileBody.ReadBuffer(BSHead, SizeOf(mBSAmplHead));
                       FFileBody.Position:= FFileBody.Position + BSHead.Len * 2;
                       SkipBytes:= - 1;
                     end;

                     EID_Sens        ,  EID_Att         , EID_VRU         ,
                     EID_StStr       ,  EID_EndStr     ,  EID_HeadPh      , EID_Mode        , EID_2Tp_Word    ,
                     EID_2Tp         ,  EID_ZondImp    ,  EID_SetRailType , EID_Stolb       , EID_Strelka     ,
                     EID_DefLabel    ,  EID_TextLabel  ,  EID_StBoltStyk  , EID_EndBoltStyk ,
                     EID_Time        ,  EID_StLineLabel,  EID_EndLineLabel, EID_StSwarStyk  ,
                     EID_EndSwarStyk ,  EID_OpChange   ,  EID_PathChange  , EID_MovDirChange,
                     EID_SezdNo      ,  EID_LongLabel  ,  EID_GPSCoord   ,  EID_GPSState    , EID_GPSError: SkipBytes:= Avk11EventLen[LoadID];

               EID_ACLeft: begin
                             FFileBody.ReadBuffer(LoadByte[1], 1);
                             CurAK[SideToRail(sLeft), 1]:= LoadByte[1] and 1 <> 0;
                             CurAK[SideToRail(sLeft), 2]:= LoadByte[1] and 2 <> 0;
                             CurAK[SideToRail(sLeft), 3]:= LoadByte[1] and 4 <> 0;
                             CurAK[SideToRail(sLeft), 4]:= LoadByte[1] and 8 <> 0;
                             CurAK[SideToRail(sLeft), 5]:= LoadByte[1] and 16 <> 0;
                             CurAK[SideToRail(sLeft), 6]:= LoadByte[1] and 32 <> 0;
                             CurAK[SideToRail(sLeft), 7]:= LoadByte[1] and 64 <> 0;
                             CurAK[SideToRail(sLeft), 8]:= LoadByte[1] and 128 <> 0;
                           end;
              EID_ACRight: begin
                             FFileBody.ReadBuffer(LoadByte[1], 1);
                             CurAK[SideToRail(sRight), 1]:= LoadByte[1] and 1 <> 0;
                             CurAK[SideToRail(sRight), 2]:= LoadByte[1] and 2 <> 0;
                             CurAK[SideToRail(sRight), 3]:= LoadByte[1] and 4 <> 0;
                             CurAK[SideToRail(sRight), 4]:= LoadByte[1] and 8 <> 0;
                             CurAK[SideToRail(sRight), 5]:= LoadByte[1] and 16 <> 0;
                             CurAK[SideToRail(sRight), 6]:= LoadByte[1] and 32 <> 0;
                             CurAK[SideToRail(sRight), 7]:= LoadByte[1] and 64 <> 0;
                             CurAK[SideToRail(sRight), 8]:= LoadByte[1] and 128 <> 0;
                           end;
            EID_SysCrd_SS: begin // ��������� ���������� �������� ������, �������� ����������
                             FFileBody.ReadBuffer(LoadByte[1], 2);
                             NewSysCoord:=    Integer((Integer(FCurSysCoord) and $FFFFFF00) or LoadByte[2]);
                           end;
            EID_SysCrd_SF: begin // ��������� ���������� �������� ������, ������ ����������
                             FFileBody.ReadBuffer(LoadByte[1], 1);
                             FFileBody.ReadBuffer(LoadLongInt, 4);
                             NewSysCoord:=    LoadLongInt[1];
                           end;
            EID_SysCrd_FS: begin // ��������� ���������� ������ ������, �������� ����������
                             CurCrdLongLinc:= True;
                             FFileBody.ReadBuffer(LoadByte[1], 5);
                             NewSysCoord:=    Integer((Integer(FCurSysCoord) and $FFFFFF00) or LoadByte[5]);
                           end;
            EID_SysCrd_FF: begin // ��������� ���������� ������ ������, ������ ����������
                             CurCrdLongLinc:= True;
                             FFileBody.ReadBuffer(LoadByte[1], 4);
                             FFileBody.ReadBuffer(LoadLongInt, 4);
                             NewSysCoord:=    LoadLongInt[1];
                           end;
              EID_EndFile: begin // ����� �����
                             Exit;
                           end;
        end;

        if LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF] then
        begin
          UnPackEcho;

          FUnPackFlag[r_Left]:= False;
          FUnPackFlag[r_Right]:= False;

          if (FPack[r_Left].Ampl <> $FF) then
          begin
            FCurEcho[r_Left, 1].Delay[1]:= FPack[r_Left].Delay;
            FCurEcho[r_Left, 1].Ampl[1]:= FPack[r_Left].Ampl;
            FCurEcho[r_Left, 1].Count:= 1;
            FCurEcho[r_Left, 1].ZerroFlag:= False;
            FUnPackFlag[r_Left]:= True;
          end;
          if (FPack[r_Right].Ampl <> $FF) then
          begin
            FCurEcho[r_Right, 1].Delay[1]:= FPack[r_Right].Delay;
            FCurEcho[r_Right, 1].Ampl[1]:= FPack[r_Right].Ampl;
            FCurEcho[r_Right, 1].Count:= 1;
            FCurEcho[r_Right, 1].ZerroFlag:= False;
            FUnPackFlag[r_Right]:= True;
          end;

          BackMotion:= NewSysCoord < LastSysCoord;

          if Assigned(BlockOk) and (FCurDisCoord >= StartDisCoord - LoadShift) and (FCurDisCoord <= EndDisCoord) then
            if not BlockOk(StartDisCoord) then Break; // ��������� ������� ��� "������" ����������

//          ToLog(Format('Crd: %d; R: 1; Ch: 1 Echo: %d; UnPack %d', [FCurDisCoord, CurEcho[r_Left, 1].Count, Ord(FUnPackFlag[r_Left])])); // ----------------------------------------------------------------------------------------
//          ToLog(Format('Crd: %d; R: 2; Ch: 1 Echo: %d; UnPack %d', [FCurDisCoord, CurEcho[r_Right, 1].Count, Ord(FUnPackFlag[r_Right])])); // ----------------------------------------------------------------------------------------

          FCurSysCoord:= NewSysCoord;
                                                               // ���� ���� ������������� ������� ������� ������ �� ����� ����������
          if (Abs(FCurSysCoord - LastSysCoord) > 1) then
          if ((FPack[r_Left].Ampl <> $FF) or (FPack[r_Right].Ampl <> $FF)) then
          begin
            FSaveDisCoord:= FCurDisCoord;
            FSaveSysCoord:= FCurSysCoord;
            Move(CurEcho, CurEcho_, SizeOf(TCurEcho));
            FillChar(FCurEcho, SizeOf(TCurEcho), 0);

            if (FPack[r_Left].Ampl <> $FF) then
            begin
              FCurEcho[r_Left, 1].Delay[1]:= FPack[r_Left].Delay;
              FCurEcho[r_Left, 1].Ampl[1]:= FPack[r_Left].Ampl;
              FCurEcho[r_Left, 1].Count:= 1;
              FCurEcho[r_Left, 1].ZerroFlag:= False;
              FUnPackFlag[r_Left]:= True;
            end;
            if (FPack[r_Right].Ampl <> $FF) then
            begin
              FCurEcho[r_Right, 1].Delay[1]:= FPack[r_Right].Delay;
              FCurEcho[r_Right, 1].Ampl[1]:= FPack[r_Right].Ampl;
              FCurEcho[r_Right, 1].Count:= 1;
              FCurEcho[r_Right, 1].ZerroFlag:= False;
              FUnPackFlag[r_Right]:= True;
            end;

            if FCurSysCoord > LastSysCoord then
            begin

              T:= FCurDisCoord;

          //    if FCurSysCoord - LastSysCoord < 5000 then
                for J:= LastSysCoord + 1 to FCurSysCoord - 1 do
                begin
                  Inc(FCurDisCoord);
                  FCurSysCoord:= J;
                  if Assigned(BlockOk) and (FCurDisCoord >= StartDisCoord - LoadShift) and (FCurDisCoord <= EndDisCoord) then
                    if not BlockOk(StartDisCoord) then Break;
                  if CurDisCoord >= EndDisCoord then Exit;
                end;

//              ToLog(Format('Unpack St: %d Ed: %d; UnPack: 1', [T + 1, FCurDisCoord]));

            end
            else
            begin

              T:= FCurDisCoord;

          //    if LastSysCoord - FCurSysCoord < 5000 then
                for J:= LastSysCoord - 1 downto FCurSysCoord + 1 do
                begin
                  Inc(FCurDisCoord);
                  FCurSysCoord:= J;
                  if Assigned(BlockOk) and (FCurDisCoord >= StartDisCoord) and (FCurDisCoord <= EndDisCoord) then
                    if not BlockOk(StartDisCoord) then Break;
                  if CurDisCoord >= EndDisCoord then Exit;
                end;

//              ToLog(Format('Unpack St: %d Ed: %d; UnPack: 1', [T + 1, FCurDisCoord]));

            end;

            Move(CurEcho_, FCurEcho, SizeOf(TCurEcho));
            FCurDisCoord:= FSaveDisCoord;
            FCurSysCoord:= FSaveSysCoord;
          end;

          Inc(FCurDisCoord, Abs(FCurSysCoord - LastSysCoord)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)
          LastSysCoord:= FCurSysCoord;
          LastDisCoord:= FCurDisCoord;

          GetParamNext(FCurDisCoord, FCurParams);
          if CurDisCoord >= EndDisCoord then Exit;

          for R:= r_Left to r_Right do
          begin
            for I:= 1 to 7 do
            begin
              FCurEcho[R, I].Count:= 0;
              FCurEcho[R, I].ZerroFlag:= False;
            end;
            CurAmpl[R]:= - 1;
          end;
        end;
      end
      else
      begin
        SkipBytes:= Avk11EventLen[LoadID];
        if LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF] then UnPackEcho;
      end;
    end;
    if SkipBytes <> - 1 then FFileBody.Position:= FFileBody.Position + SkipBytes;
  until False;
end;

function TAvk11DatSrc.GetBMStateFirst(DisCoord: Integer): Boolean;
var
  I: Integer;
  EndIdx: Integer;
  StartIdx: Integer;

begin
  Result:= False;
  FGetBMStateIdx:= 0;
  if FSkipBM then
  begin
    FGetBMStateState:= False;
    Exit;
  end;

  for I:= High(FCDList) downto 0 do
  begin
    if FEventsData[FCDList[I]].Event.DisCoord <= DisCoord then
    begin
      if FEventsData[FCDList[I]].Event.ID = EID_FwdDir then
      begin
        Result:= False;
        FGetBMStateIdx:= I + 1;
        Break;
      end;
      if FEventsData[FCDList[I]].Event.ID = EID_BwdDir {EID_FwdDirEnd} then
      begin
        Result:= True;
        FGetBMStateIdx:= I + 1;
        Break;
      end;
      Break;
    end;
  end;
  FGetBMStateState:= Result;


{
  FGetBMStateIdx:= 0;
  GetEventIdx(0, DisCoord, StartIdx, EndIdx, 0);

  Result:= False;
  for I:= EndIdx downto 0 do
  begin
    if FEventsData[I].Event.ID = EID_FwdDir then
    begin
      Result:= False;
      FGetBMStateIdx:= I + 1;
      Break;
    end;
    if FEventsData[I].Event.ID = EID_BwdDir then
    begin
      Result:= True;
      FGetBMStateIdx:= I + 1;
      Break;
    end;
  end;
  FGetBMStateState:= Result;
  }
end;

function TAvk11DatSrc.GetBMStateNext(var DisCoord: Integer; var State: Boolean): Boolean;
var
  I: Integer;
  EndIdx: Integer;
  StartIdx: Integer;

begin
  Result:= False;
  State:= FGetBMStateState;
  if FSkipBM then Exit;

  for I:= FGetBMStateIdx to High(FCDList) do
  begin
    if FEventsData[FCDList[I]].Event.ID = EID_FwdDir then
    begin
      State:= False;
      FGetBMStateIdx:= I + 1;
      FGetBMStateState:= False;
      DisCoord:= FEventsData[FCDList[I]].Event.DisCoord;
      Result:= I <> GetEventCount - 1;
      Break;
    end;
    if FEventsData[FCDList[I]].Event.ID = EID_BwdDir {EID_FwdDirEnd} then
    begin
      State:= True;
      FGetBMStateIdx:= I + 1;
      FGetBMStateState:= True;
      DisCoord:= FEventsData[FCDList[I]].Event.DisCoord;
      Result:= I <> GetEventCount - 1;
      Break;
    end;
  end;

{
  Result:= False;
  State:= FGetBMStateState;
  for I:= FGetBMStateIdx to GetEventCount - 1 do
  begin
    if FEventsData[I].Event.ID = EID_FwdDir then
    begin
      State:= False;
      FGetBMStateIdx:= I + 1;
      FGetBMStateState:= False;
      DisCoord:= FEventsData[I].Event.DisCoord;
      Result:= I <> GetEventCount - 1;
      Break;
    end;
    if FEventsData[I].Event.ID = EID_BwdDir then
    begin
      State:= True;
      FGetBMStateIdx:= I + 1;
      FGetBMStateState:= True;
      DisCoord:= FEventsData[I].Event.DisCoord;
      Result:= I <> GetEventCount - 1;
      Break;
    end;
  end;
  }
end;

procedure TAvk11DatSrc.SetScanStep(New: Word);
begin
  FHeader.ScanStep:= New;
end;

function TAvk11DatSrc.GetEventIdx(StartDisCoord, EndDisCoord: Integer; var StartIdx, EndIdx: Integer; EmptyPar: Integer): Boolean;
var
  I: Integer;
  SameCoord1: TIntegerDynArray;
  SameCoord2: TIntegerDynArray;
  LeftIdx1: Integer;
  LeftIdx2: Integer;
  RightIdx1: Integer;
  RightIdx2: Integer;

begin

  GetNearestEventIdx(StartDisCoord, LeftIdx1, RightIdx1, SameCoord1);
  GetNearestEventIdx(EndDisCoord, LeftIdx2, RightIdx2, SameCoord2);

  if Length(SameCoord1) <> 0 then StartIdx:= SameCoord1[0]
                             else StartIdx:= LeftIdx1 + 1;

  if Length(SameCoord2) <> 0 then EndIdx:= SameCoord2[High(SameCoord2)]
                             else EndIdx:= RightIdx2 - 1;

{
  StartIdx:= High(FEventsData);
  EndIdx:= 0;

  for I:= 0 to High(FEventsData) do
  begin
    if (StartDisCoord <= FEventsData[I].Event.DisCoord) and
       (EndDisCoord   >= FEventsData[I].Event.DisCoord) then
    begin
      StartIdx:= Min(StartIdx, I);
      EndIdx:=   Max(EndIdx, I);
    end;
    if EndDisCoord < FEventsData[I].Event.DisCoord then Break;
  end;   }
end;

procedure TAvk11DatSrc.GetNearestEventIdx(DisCoord: Integer; var LeftIdx, RightIdx: Integer; var SameCoord: TIntegerDynArray);
var
  I: Integer;
  CLeft: Integer;
  CRight: Integer;
  CurrPos: Integer;

begin
  CLeft:= 0; // ������ � ������ � ������
  CRight:= High(FEventsData); // ����� �������������� � �����
  repeat
    CurrPos:= (CLeft + CRight) div 2; // ���������� ��������
    begin
      if FEventsData[CurrPos].Event.DisCoord < DisCoord then CLeft:= CurrPos // ������� ����� �������� ����� ��� ����������� �������
                                                        else CRight:= CurrPos;
      if (CLeft = CRight) or (Abs(CLeft - CRight) = 1) then // �������� �� ����� ������
      begin
        for I:= CLeft downto 0 do
          if DisCoord = FEventsData[I].Event.DisCoord then
          begin
            SetLength(SameCoord, Length(SameCoord) + 1);
            SameCoord[High(SameCoord)]:= I;
            LeftIdx:= I;
          end
          else
          begin
            if Length(SameCoord) <> 0 then LeftIdx:= SameCoord[0]
                                      else LeftIdx:= CLeft;
            Break;
          end;

        for I:= CLeft + 1 to High(FEventsData) do
          if DisCoord = FEventsData[I].Event.DisCoord then
          begin
            SetLength(SameCoord, Length(SameCoord) + 1);
            SameCoord[High(SameCoord)]:= I;
            RightIdx:= I;
          end
          else
          begin
            if Length(SameCoord) <> 0 then RightIdx:= SameCoord[High(SameCoord)]
                                      else RightIdx:= I;
            Break;
          end;

        Exit;
      end;
    end;
  until false;

 (*
  F:= True;
  for I:= 0 to High(FEventsData) do
  begin
    if DisCoord = FEventsData[I].Event.DisCoord then
    begin
      if F then
      begin
        LeftIdx:= I - 1;
        F:= False;
      end;
      SetLength(SameCoord, Length(SameCoord) + 1);
      SameCoord[High(SameCoord)]:= I;
    end;
    if DisCoord < FEventsData[I].Event.DisCoord then
    begin
      if F then LeftIdx:= I - 1;
      RightIdx:= I;
      Break;
    end;
  end;
 *)
end;

function TAvk11DatSrc.GetLeftEventIdx(DisCoord: Integer): Integer;
var
  LeftIdx, RightIdx: Integer;
  SameCoord: TIntegerDynArray;

begin
  GetNearestEventIdx(DisCoord, LeftIdx, RightIdx, SameCoord);
  if Length(SameCoord) <> 0 then Result:= SameCoord[0]
                            else Result:= LeftIdx;
end;

function TAvk11DatSrc.GetRightEventIdx(DisCoord: Integer): Integer;
var
  LeftIdx, RightIdx: Integer;
  SameCoord: TIntegerDynArray;

begin
  GetNearestEventIdx(DisCoord, LeftIdx, RightIdx, SameCoord);
  if Length(SameCoord) <> 0 then Result:= SameCoord[High(SameCoord)]
                            else Result:= RightIdx;
end;

procedure TAvk11DatSrc.GetEventData(Idx: Integer; var ID: Byte; var pData: PEventData);
begin
  if (Idx >= 0) and (Idx <= High(FEventsData)) then
  begin
    ID:= FEventsData[Idx].Event.ID;
    pData:= Addr(FEventsData[Idx].Data);
  end;
end;

function TAvk11DatSrc.GetTimeList(Index: Integer): TimeListItem;
begin
  Result:= FTimeList[Index];
end;

function TAvk11DatSrc.GetTimeListCount: Integer;
begin
  Result:= Length(FTimeList)
end;

function TAvk11DatSrc.GetEventsData(Index: Integer): TFileEventData;
begin
  Result:= FEventsData[Index];
end;

function TAvk11DatSrc.GetParamFirst(DisCoord: Integer; var Params: TMiasParams): Boolean;
{
var
  StartIdx, EndIdx: Integer;
 }
begin
  DisCoord:= NormalizeDisCoord(DisCoord);
  if DisCoord <= 0 then DisCoord:= 1;

  FGetParamIdx:= GetLeftEventIdx(DisCoord);
  if FGetParamIdx <> - 1 then
    Params:= FEventsData[FGetParamIdx].Params;
end;

function TAvk11DatSrc.GetParamNext(DisCoord: Integer; var Params: TMiasParams): Boolean;
var
  I: Integer;
  StartIdx, EndIdx: Integer;

begin
  if FGetParamIdx = - 1 then Exit;
//  DisCoord:= NormalizeDisCoord(DisCoord);
//  GetEventIdx(DisCoord, DisCoord, StartIdx, EndIdx);
//  FGetParamIdx:= EndIdx;

  for I:= FGetParamIdx to High(FEventsData) do
  begin
    if DisCoord = FEventsData[I].Event.DisCoord then
    begin
      Params:= FEventsData[I].Params;
      FGetParamIdx:= I;
      Break;
    end;
    if DisCoord < FEventsData[I].Event.DisCoord then
    begin
      Params:= FEventsData[I - 1].Params;
      FGetParamIdx:= I;
      Break;
    end;
  end;
end;

function TAvk11DatSrc.GetCoord(DisCoord: Integer; var CoordParams: TCoordParams): Boolean;
var
  ID: Byte;
  I, J: Integer;
  pData: PEventData;
  StartIdx: Integer;
  MMLen: Integer;
  LeftFlg: Boolean;
  RightFlg: Boolean;

begin
  DisCoord:= NormalizeDisCoord(DisCoord);
                                              // ����� ������ �������������� ������ �� DisCoord
  LeftFlg:= False;
  for I:= GetLeftEventIdx(DisCoord) downto 0 do
  begin
    GetEventData(I, ID, pData);
    if ID = EID_Stolb then
    begin
      CoordParams.LeftStolb[ssLeft].Km:= pCoord(pData)^.Km[0];
      CoordParams.LeftStolb[ssLeft].Pk:= pCoord(pData)^.Pk[0];
      CoordParams.LeftStolb[ssRight].Km:= pCoord(pData)^.Km[1];
      CoordParams.LeftStolb[ssRight].Pk:= pCoord(pData)^.Pk[1];
      CoordParams.ToLeftStolbMM:= (DisToSysCoord(DisCoord) - FEventsData[I].Event.SysCoord) * FHeader.ScanStep div 100;
      CoordParams.LeftIdx:= I;
      LeftFlg:= True;
      Break;
    end;
  end;
                                              // ����� ������ �������������� ����� �� DisCoord
  RightFlg:= False;
  for I:= GetRightEventIdx(DisCoord) to High(FEventsData) do
  begin
    GetEventData(I, ID, pData);
    if ID = EID_Stolb then
    begin
      CoordParams.RightStolb[ssLeft].Km:= pCoord(pData)^.Km[0];
      CoordParams.RightStolb[ssLeft].Pk:= pCoord(pData)^.Pk[0];
      CoordParams.RightStolb[ssRight].Km:= pCoord(pData)^.Km[1];
      CoordParams.RightStolb[ssRight].Pk:= pCoord(pData)^.Pk[1];
      CoordParams.ToRightStolbMM:= (FEventsData[I].Event.SysCoord - DisToSysCoord(DisCoord)) * FHeader.ScanStep div 100;
      CoordParams.RightIdx:= I;
      RightFlg:= True;
      Break;
    end;
  end;

  if Header.MoveDir = 1 then // � ������� ����������
  begin
    if (not LeftFlg) and (not RightFlg) then // ��� �������
    begin
      CoordParams.LeftStolb[ssLeft].Km:= FHeader.StartKM;
      CoordParams.LeftStolb[ssLeft].Pk:= FHeader.StartPk;
      CoordParams.LeftStolb[ssRight].Km:= FHeader.StartKM;
      CoordParams.LeftStolb[ssRight].Pk:= FHeader.StartPk;
      CoordParams.ToLeftStolbMM:= FHeader.StartMetre * 1000 + DisToSysCoord(DisCoord) * FHeader.ScanStep div 100;
      CoordParams.LeftIdx:= - 1;

      CoordParams.RightStolb[ssLeft].Km:= FExHeader.EndKM;
      CoordParams.RightStolb[ssLeft].Pk:= FExHeader.EndPK;
      CoordParams.RightStolb[ssRight].Km:= FExHeader.EndKM;
      CoordParams.RightStolb[ssRight].Pk:= FExHeader.EndPK;
      CoordParams.ToRightStolbMM:= (DisToSysCoord(MaxDisCoord) - DisToSysCoord(DisCoord)) * FHeader.ScanStep div 100;
      CoordParams.RightIdx:= - 1;
    end;
    if (not LeftFlg) and RightFlg then // ��� ������ �����
    begin
      CoordParams.LeftStolb[ssLeft].Km:= FHeader.StartKM;
      CoordParams.LeftStolb[ssLeft].Pk:= FHeader.StartPk;
      CoordParams.LeftStolb[ssRight].Km:= FHeader.StartKM;
      CoordParams.LeftStolb[ssRight].Pk:= FHeader.StartPk;
      CoordParams.ToLeftStolbMM:= FHeader.StartMetre * 1000 + DisToSysCoord(DisCoord) * FHeader.ScanStep div 100;
      CoordParams.LeftIdx:= - 1;
    end;
    if LeftFlg and (not RightFlg) then // ��� ������ ������
    begin
      CoordParams.RightStolb[ssLeft].Km:= FExHeader.EndKM;
      CoordParams.RightStolb[ssLeft].Pk:= FExHeader.EndPK;
      CoordParams.RightStolb[ssRight].Km:= FExHeader.EndKM;
      CoordParams.RightStolb[ssRight].Pk:= FExHeader.EndPK;
      CoordParams.ToRightStolbMM:= (DisToSysCoord(MaxDisCoord) - DisToSysCoord(DisCoord)) * FHeader.ScanStep div 100;
      CoordParams.RightIdx:= - 1;
    end;
  end
  else   // � ������� ����������
  begin
    if (not LeftFlg) and (not RightFlg) then // ��� �������
    begin
      CoordParams.RightStolb[ssLeft].Km:= FExHeader.EndKM;
      CoordParams.RightStolb[ssLeft].Pk:= FExHeader.EndPK;
      CoordParams.RightStolb[ssRight].Km:= FExHeader.EndKM;
      CoordParams.RightStolb[ssRight].Pk:= FExHeader.EndPK;
      CoordParams.ToRightStolbMM:= FHeader.StartMetre * 1000 - DisToSysCoord(DisCoord) * FHeader.ScanStep div 100;
      CoordParams.RightIdx:= - 1;

      CoordParams.LeftStolb[ssLeft].Km:= FHeader.StartKM;
      CoordParams.LeftStolb[ssLeft].Pk:= FHeader.StartPk;
      CoordParams.LeftStolb[ssRight].Km:= FHeader.StartKM;
      CoordParams.LeftStolb[ssRight].Pk:= FHeader.StartPk;
      CoordParams.ToLeftStolbMM:= 100000 - FHeader.StartMetre * 1000 - DisToSysCoord(DisCoord) * FHeader.ScanStep div 100;
      CoordParams.LeftIdx:= - 1;
    end;
    if (not LeftFlg) and RightFlg then // ��� ������ �����
    begin
      CoordParams.LeftStolb[ssLeft].Km:= FHeader.StartKM;
      CoordParams.LeftStolb[ssLeft].Pk:= FHeader.StartPk;
      CoordParams.LeftStolb[ssRight].Km:= FHeader.StartKM;
      CoordParams.LeftStolb[ssRight].Pk:= FHeader.StartPk;
      CoordParams.ToLeftStolbMM:= FHeader.StartMetre * 1000 + DisToSysCoord(DisCoord) * FHeader.ScanStep div 100;
      CoordParams.LeftIdx:= - 1;
    end;
    if LeftFlg and (not RightFlg) then // ��� ������ ������
    begin
      CoordParams.RightStolb[ssLeft].Km:= FExHeader.EndKM;
      CoordParams.RightStolb[ssLeft].Pk:= FExHeader.EndPK;
      CoordParams.RightStolb[ssRight].Km:= FExHeader.EndKM;
      CoordParams.RightStolb[ssRight].Pk:= FExHeader.EndPK;
      CoordParams.ToRightStolbMM:= 100000 - CoordParams.ToLeftStolbMM;
      CoordParams.RightIdx:= - 1;
    end;
  end;
end;

function TAvk11DatSrc.GetTime(DisCoord: Integer): string;
var
  ID: Byte;
  I: Integer;
  pData: PEventData;
  StartIdx: Integer;

begin
  DisCoord:= NormalizeDisCoord(DisCoord);
  StartIdx:= GetLeftEventIdx(DisCoord);

  Result:= CodeToTime(FHeader.Hour, FHeader.Minute);

  for I:= StartIdx downto 0 do
  begin
    GetEventData(I, ID, pData);
    if ID in [    EID_Stolb, EID_StBoltStyk,  EID_EndLineLabel,
                EID_Strelka, EID_EndBoltStyk, EID_StSwarStyk,
               EID_DefLabel, EID_Time,        EID_EndSwarStyk,
              EID_TextLabel, EID_StLineLabel ] then
    begin
      Result:= CodeToTime(pData^[Avk11EventLen[ID] - 1], pData^[Avk11EventLen[ID] - 0]);
      Exit;
    end;
  end;
end;

function TAvk11DatSrc.GetSpeed(DisCoord: Integer; var Speed: Single): string;
var
  I: Integer;
  J: Integer;
  K: Integer;
  T: Extended;
  S: Extended;

begin
  for I:= High(FTimeList) downto 0 do
    if FTimeList[I].DC <= DisCoord then
    begin
      if I <> High(FTimeList) then J:= I + 1 else J:= I - 1;
      K:= I;
      if K > 0 then Dec(K);
      if J < High(FTimeList) then Inc(J);
      if K > 0 then Dec(K);
      if J < High(FTimeList) then Inc(J);
      if K > 0 then Dec(K);
      if J < High(FTimeList) then Inc(J);
      if K > 0 then Dec(K);
      if J < High(FTimeList) then Inc(J);

      T:= Abs(FTimeList[J].H + FTimeList[J].M / 60 - (FTimeList[K].H + FTimeList[K].M / 60));
      if T <> 0 then
      begin
        S:= Abs(FTimeList[J].DC - FTimeList[K].DC) * FHeader.ScanStep / 100 / 1000000;
        Speed:= S / T;
        if T <> 0 then Result:= Format('%3.1f ' + Language.GetCaption(0000023), [S / T])
                  else Result:= '?';
      end;
      Exit;
    end;
end;

function TAvk11DatSrc.LoadFromFile(FileName: string): Boolean;
var
  ID: Word;
  I: Integer;
  OldHeader1: TAvk11Header1;
  FileAttr: Integer;

begin
  FReadOnlyFlag:= False;
  FileAttr:= FileGetAttr(FileName);
  if FileAttr and faReadOnly <> 0 then
    if FileSetAttr(FileName, FileAttr and $FE) <> 0 then FReadOnlyFlag:= True;

  Result:= False;

  if not FileExists(FileName) then
  begin
    FLog.Add(Language.GetCaption(0150029) + ' ' + FileName + ' ' + Language.GetCaption(0150030));
    Exit;
  end;

  FFileName:= FileName;
  FFileBody:= TMemoryStream.Create;

  try
    FFileBody.LoadFromFile(FileName);
  except
    ShowMessage(Language.GetCaption(0150005));
    Exit;
  end;

  FDataSize:= FFileBody.Size;
  FSaveFileDate:= FileDateToDateTime(FileAge(FFileName));
  FSaveFileSize:= FFileBody.Size;

  if FDataSize < 128 then
  begin
    FLog.Add(Language.GetCaption(0150017));
    Exit;
  end;

  FFileBody.Position:= 0;
  FFileBody.ReadBuffer(ID, 2);
  if (ID <> $FF11) and (ID <> $FF12) and (ID <> $FF14) and (ID <> $FF15) and (ID <> $FF16) and (ID <> $FF17) then
  begin
    FLog.Add(Language.GetCaption(0150017));
    Exit;
  end;

  FFileBody.Position:= 0;
  if ID = $FF11 then // �������� ��������� ������� ����
  begin
    try
      FFileBody.ReadBuffer(OldHeader1, SizeOf(OldHeader1));
    except
      FLog.Add('������ ������ ������ �� ������ ��� �������� ���������');
      Exit;
    end;

    FHeader.ID:=          $FF12;
    FHeader.UpDeviceID:=  OldHeader1.DeviceID;
    FHeader.DwnDeviceID:= OldHeader1.DeviceID;
    FHeader.Day:=         OldHeader1.Day;
    FHeader.Month:=       OldHeader1.Month;
    FHeader.Year:=        OldHeader1.Year;
    FHeader.Hour:=        OldHeader1.Hour;
    FHeader.Minute:=      OldHeader1.Minute;
    TextToCode(CodeToText(OldHeader1.RRName, 0) + ' ' + Language.GetCaption(0150031) + IntToStr(OldHeader1.PChNumber), 38, 0, FHeader.Name);
    Move(OldHeader1.Operator, FHeader.Operator, 20);
    FHeader.DirCode:=     OldHeader1.DirCode;
    Move(OldHeader1.SecName, FHeader.SecName, 20);
    FHeader.StartKM:=     OldHeader1.StartKM;
    FHeader.StartPk:=     OldHeader1.StartPk;
    FHeader.StartMetre:=  OldHeader1.StartMetre;
//    FHeader.EditKM:=      OldHeader1.EditKM;
//    FHeader.EditPk:=      OldHeader1.EditPk;
//    FHeader.EditM:=       OldHeader1.EditM;
    FHeader.Path:=        OldHeader1.Path;
    FHeader.MoveDir:=     OldHeader1.MoveDir;
    FHeader.Zveno:=       OldHeader1.Zveno;
    FHeader.ScanStep:=    OldHeader1.ScanStep;
    FHeader.ContentFlg:=  0;
    FHeader.AmplTH:=      0;
    FHeader.RailPos:=     1;
    FHeader.CharSet:=     0;
    FHeader.TableLink:=   OldHeader1.TableLink;
  end;

  if (ID = $FF12) or (ID = $FF14) or (ID = $FF15) or (ID = $FF16) or (ID = $FF17) then
    try
      FFileBody.ReadBuffer(FHeader, SizeOf(FHeader));
    except
      FLog.Add('������ ������ ������ �� ������ ��� �������� ���������');
      Exit;
    end;

  if (FHeader.ScanStep = 0) or (FHeader.ScanStep > 700) then FHeader.ScanStep:= 173;

  DelayKoeff:= 3;
  if not (FHeader.MoveDir in [0, 1]) then FHeader.MoveDir:= 1;

  LoadExData;
  if FUnknownFileVersion then
  begin
    ShowMessage(Language.GetCaption(0150008));
    Result:= False;
    Exit;
  end;

  FillEventsData;
  Result:= True;

  FRSPFile:= False;
  for I:= 0 to RSPInfoCount - 1 do
    if RSPInfoByIdx[I].ID = idRSP_Control then
    begin
      FRSPFile:= True;
      Break;
    end;

  for I:= 0 to High(FNotebook) do
    if FNotebook[I].DisCoord = - MaxInt then FNotebook[I].DisCoord:= SysToDisCoord(FNotebook[I].SysCoord)
                                        else FNotebook[I].SysCoord:= DisToSysCoord(FNotebook[I].DisCoord);
end;

function TAvk11DatSrc.DisToSysCoord(Coord: Integer): Integer;
var
  LeftIdx, RightIdx: Integer;
  Del: Integer;

begin
  Coord:= NormalizeDisCoord(Coord);
  LeftIdx:= GetLeftEventIdx(Coord);
  if FEventsData[LeftIdx].Event.DisCoord = Coord then Result:= FEventsData[LeftIdx].Event.SysCoord else
  begin
    RightIdx:= GetRightEventIdx(Coord);
    Del:= (FEventsData[RightIdx].Event.DisCoord - FEventsData[LeftIdx].Event.DisCoord);
    if Del <> 0 then Result:= Round(FEventsData[LeftIdx].Event.SysCoord + ((Coord - FEventsData[LeftIdx].Event.DisCoord) / Del) * (FEventsData[RightIdx].Event.SysCoord - FEventsData[LeftIdx].Event.SysCoord))
                else Result:= FEventsData[LeftIdx].Event.SysCoord;
  end;
end;

procedure TAvk11DatSrc.DisToDisCoords(Coord: Integer; var Res: TIntegerDynArray);
var
  I, J: Integer;

begin
  Coord:= DisToSysCoord(NormalizeDisCoord(Coord));
  for I:= 0 to EventCount - 2 do
    if (FEventsData[I].Event.SysCoord <= Coord) and (FEventsData[I + 1].Event.SysCoord >= Coord) or
       (FEventsData[I].Event.SysCoord >= Coord) and (FEventsData[I + 1].Event.SysCoord <= Coord) then
    begin
      J:= FEventsData[I + 0].Event.DisCoord + Abs(Coord - FEventsData[I + 0].Event.SysCoord);
      if (Length(Res) = 0) or (J <> Res[High(Res)]) then
      begin
        SetLength(Res, Length(Res) + 1);
        Res[High(Res)]:= J;
      end;
    end;
end;

(*
procedure TAvk11DatSrc.DisToDisCoords_(Coord: Integer; var Res: TIntegerDynArray);
var
  I: Integer;
  Del: Integer;
  Res_: Integer;

begin
//  Coord:= DisToSysCoord(NormalizeDisCoord(Coord));

  for I:= 0 to EventCount - 2 do
    if (FEventsData[I].Event.SysCoord <= Coord) and (FEventsData[I + 1].Event.SysCoord >= Coord) or
       (FEventsData[I].Event.SysCoord >= Coord) and (FEventsData[I + 1].Event.SysCoord <= Coord) then
    begin


      SetLength(Res, Length(Res) + 1);
      Res[High(Res)]:= FEventsData[I + 0].Event.DisCoord + Abs(Coord - FEventsData[I + 0].Event.SysCoord);


    end;
end;
*)
function TAvk11DatSrc.SideToRail(Side: TSide): RRail;

function InvertRail(Rail: RRail): RRail;
begin
  if Rail = r_Left then Result:= r_Right
                   else Result:= r_Left;
end;

function Side_to_Rail(S: TSide): RRail;
begin
  case S of
    sLeft: Result:= r_Left;
   sRight: Result:= r_Right;
  end;
end;

begin
  {$IFDEF AVIKON12}
  case Side of
    sLeft: Result:= r_Left;
   sRight: Result:= r_Right;
  end;
  {$ENDIF}

  {$IFNDEF AVIKON12}
  if FRSPSkipSideRail
    then Result:= Side_to_Rail(Side)
    else case FHeader.RailPos of
           1: case FHeader.MoveDir of
                0: Result:= Side_to_Rail(Side);
                1: Result:= InvertRail(Side_to_Rail(Side));
              end;
           0: case FHeader.MoveDir of
                0: Result:= InvertRail(Side_to_Rail(Side));
                1: Result:= Side_to_Rail(Side);
              end;
         end;
  {$ENDIF}
end;

function TAvk11DatSrc.SysToDisCoord(Coord: Integer): Integer;
var
  I: Integer;

begin
  if Coord > FMaxSysCoord then
  begin
    Result:= MaxDisCoord;
    Exit;
  end;
  Result:= 0;
  for I:= 0 to High(FEventsData) - 1 do
    if (Coord >= FEventsData[I + 0].Event.SysCoord) and (Coord <= FEventsData[I + 1].Event.SysCoord) then
    begin
      Result:= FEventsData[I + 0].Event.DisCoord + Abs(FEventsData[I + 0].Event.SysCoord - Coord);
      Exit;
    end;
end;

function TAvk11DatSrc.DisToRealCoord(Coord: LongInt): TRealCoord;
var
  CoordParams: TCoordParams;

begin
  GetCoord(Coord, CoordParams);
{
  Result.Km:= CoordParams.LeftStolb[ssRight].Km;
  Result.Pk:= CoordParams.LeftStolb[ssRight].Pk;
  Result.MM:= CoordParams.ToLeftStolbMM;
}
  Result:= CoordParamsToRealCoord(CoordParams, Header.MoveDir);
end;

procedure TAvk11DatSrc.SaveNB;
begin
  if FModifyData then SaveExData(FExHdrLoadOk);
end;

procedure TAvk11DatSrc.GenerateGPSPoints;
var
  ID: Byte;
  I, J: Integer;
  New: TGPSPoint;
  Coord: TRealCoord;
  pData: PEventData;

begin
  SetLength(GPSPoints, 0);
  for I:= 0 to GetEventCount - 1 do
  begin
    GetEventData(I, ID, pData);

    if ID = EID_GPSCoord then
    begin
      New.Lat:= InvertGPSSingle(pGPSCoord(pData)^.Lat);
      New.Lon:= InvertGPSSingle(pGPSCoord(pData)^.Lon);

      Coord:= DisToRealCoord(GetEvent(I).Event.DisCoord);
      New.KM:= Coord.Km;
      New.Metre:= (Coord.Pk - 1) * 100 + Coord.MM / 1000;
      New.LabelExists:= False;
      New.StolbExists:= False;

      J:= I;
      while (J >= 0) and (Abs(GetEvent(I).Event.DisCoord - GetEvent(J).Event.DisCoord) * Header.ScanStep / 100 < 100) do
      begin
        GetEventData(J, ID, pData);
        case ID of
              EID_Stolb: begin   // $A0 - ������� ����������
                           New.StolbExists:= True;
                           New.LeftKm:=   pCoord(pData)^.Km[0];
                           New.LeftPk:=   pCoord(pData)^.Pk[0];
                           New.RightKm:=  pCoord(pData)^.Km[1];
                           New.RightPk:=  pCoord(pData)^.Pk[1];
                         end;
          EID_TextLabel: begin
                           New.LabelExists:= True;
                           New.LabelText:= CodeToText(pText(pData)^.Name, Header.CharSet);
                         end;
        end;
        Dec(J);
      end;

      J:= I;
      while (J < EventCount) and (Abs(GetEvent(I).Event.DisCoord - GetEvent(J).Event.DisCoord) * Header.ScanStep / 100 < 100) do
      begin
        GetEventData(J, ID, pData);
        case ID of
              EID_Stolb: begin   // $A0 - ������� ����������
                           New.StolbExists:= True;
                           New.LeftKm:=   pCoord(pData)^.Km[0];
                           New.LeftPk:=   pCoord(pData)^.Pk[0];
                           New.RightKm:=  pCoord(pData)^.Km[1];
                           New.RightPk:=  pCoord(pData)^.Pk[1];
                         end;
          EID_TextLabel: begin
                           New.LabelExists:= True;
                           New.LabelText:= CodeToText(pText(pData)^.Name, Header.CharSet);
                         end;
        end;
        Inc(J);
      end;

      SetLength(GPSPoints, Length(GPSPoints) + 1);
      GPSPoints[High(GPSPoints)]:= New;


    end;
  end;
end;

{
function TAvk11DatSrc.SaveSample(StartDisCoord: Integer): Boolean;
begin
  ActiveViewForm.DatSrc.

end;
}

procedure CopyMS(Src, Dest: TMemoryStream; Size: Integer);
begin
  Dest.SetSize(Dest.Size + Size);
  Src.ReadBuffer(Pointer(Longint(Dest.Memory) + Dest.Position)^, Size);
  Dest.Position:= Dest.Position + Size;
end;
{
procedure TAvk11DatSrc.AddARInfo(New: TARItem);
begin
 
  FModifyData:= True;
  SetLength(FARData, Length(FARData) + 1);
  FARData[High(FARData)]:= New;

end;
}
procedure TAvk11DatSrc.SavePiece(StDisCood, EdDisCood: Integer; SaveNB: Boolean; FileName: string);
var
  I: Integer;
  DisCoord1: Int64;
  SysCoord1: Integer;
  Offset1: Integer;
  DisCoord2: Int64;
  SysCoord2: Integer;
  Offset2: Integer;
  LoadID: Byte;
  SkipBytes: Integer;
  OutDat: TAvk11DatDst;
  HSItem: THSItem;
  CurSysCoord: Integer;
  ByteOffset: Byte;
  ByteCoord: Byte;
  LongCoord: LongInt;
  LongOffset: LongInt;
  NewCoord: Integer;
  LastSaveOffset: Integer;
  LastSysCrd: Integer;
  BackLink: Integer;
  FID: Byte;
  Params: TMiasParams;
  R: RRail;
  Ch: Integer;
  Rel: TRealCoord;
  OutDat2: TAvk11DatSrc;
  Tmp: TFileNotebook_Ver4;
  CoordParams: TCoordParams;
  Buff: array[5..13] of Byte;
  BSHead: mBSAmplHead;

begin

  DisToFileOffset(StDisCood, DisCoord1, SysCoord1, Offset1);
  DisToFileOffset(EdDisCood, DisCoord2, SysCoord2, Offset2);
  Rel:= DisToRealCoord(DisCoord1);

  OutDat:= TAvk11DatDst.Create;

  FFileBody.Position:= 0;
  FFileBody.ReadBuffer(OutDat.Header, 128);

  OutDat.FData:= TMemoryStream.Create;
  OutDat.FData.Position:= 0;
  OutDat.Header.StartKM:= Rel.Km;
  OutDat.Header.StartPk:= Rel.Pk;
  OutDat.Header.StartMetre:= Round(Rel.MM / 1000);
  OutDat.Header.TableLink:= $FFFFFFFF;
  OutDat.FData.WriteBuffer(OutDat.Header, 128);

  FFileBody.Position:= Offset1;

  GetParamFirst(DisCoord1, Params);

  for R:= r_Left to r_Right do
    for Ch:= 0 to 9 do
    begin
      OutDat.AddSens(Ord(R), Ch, Params.Sens[R, Ch]);
      OutDat.AddAtt(Ord(R), Ch, Params.Att[R, Ch]);
      OutDat.AddVRU(Ord(R), Ch, Params.VRU[R, Ch]);
      OutDat.AddStStr(Ord(R), Ch, Params.StStr[R, Ch]);
      OutDat.AddEndStr(Ord(R), Ch, Params.EdStr[R, Ch]);
      OutDat.Add2Tp(Ord(R), Ch, Params.DwaTp[R, Ch]);
      OutDat.AddZondImp(Ord(R), Ch, Params.ZondImp[R, Ch]);
    end;

  OutDat.AddMode(Params.Mode);
  OutDat.AddHeadPh(Params.HeadPh[r_Left, 0], Params.HeadPh[r_Left, 1], Params.HeadPh[r_Left, 2], Params.HeadPh[r_Left, 3], Params.HeadPh[r_Left, 4], Params.HeadPh[r_Left, 5],
                   Params.HeadPh[r_Left, 6], Params.HeadPh[r_Left, 7], Params.HeadPh[r_Left, 8], Params.HeadPh[r_Left, 9], Params.HeadPh[r_Right, 0], Params.HeadPh[r_Right, 1],
                   Params.HeadPh[r_Right, 2], Params.HeadPh[r_Right, 3], Params.HeadPh[r_Right, 4], Params.HeadPh[r_Right, 5], Params.HeadPh[r_Right, 6], Params.HeadPh[r_Right, 7],
                   Params.HeadPh[r_Right, 8], Params.HeadPh[r_Right, 9]);

  CurSysCoord:= SysCoord1;
  LastSysCrd:= 0;
  TestOffsetFirst;
  repeat
    if not TestOffsetNext then Exit;
    FFileBody.ReadBuffer(LoadID, SizeOf(LoadID));

    SkipBytes:= - 1;
    if LoadID and 128 = 0
      then SkipBytes:= Avk11EchoLen[LoadID and $07]
      else
    case LoadID of

   EID_HandScan: begin           // ������
                   FFileBody.Position:= FFileBody.Position - 1;
                   CopyMS(FFileBody, OutDat.FData, 6 + 1);
                   repeat
                     FFileBody.ReadBuffer(HSItem, 3);
                     FFileBody.Position:= FFileBody.Position - 3;
                     if HSItem.Count <= 4 then CopyMS(FFileBody, OutDat.FData, 3 + Avk11EchoLen[HSItem.Count]);
                   until HSItem.Sample = $FFFF;
                 end;

  EID_NewHeader: begin
                   FFileBody.ReadBuffer(SkipBytes, 4);
                   FFileBody.Position:= FFileBody.Position + SkipBytes;
                   SkipBytes:= - 1;
                 end;

     EID_BSAmpl: begin
                   FFileBody.ReadBuffer(BSHead, SizeOf(mBSAmplHead));
                   FFileBody.Position:= FFileBody.Position + BSHead.Len * 2;
                   SkipBytes:= - 1;
                 end;

                 EID_ACLeft      ,
                 EID_ACRight     ,  EID_Sens       ,  EID_Att         , EID_VRU         ,
                 EID_StStr       ,  EID_EndStr     ,  EID_HeadPh      , EID_Mode        , EID_2Tp_Word    ,
                 EID_2Tp         ,  EID_ZondImp    ,  EID_SetRailType , EID_Stolb       , EID_Strelka     ,
                 EID_DefLabel    ,  EID_TextLabel  ,  EID_StBoltStyk  , EID_EndBoltStyk ,
                 EID_Time        ,  EID_StLineLabel,  EID_EndLineLabel, EID_StSwarStyk  ,
                 EID_EndSwarStyk ,  EID_OpChange   ,  EID_PathChange  , EID_MovDirChange,
                 EID_SezdNo      ,  EID_LongLabel  ,  EID_GPSCoord   ,  EID_GPSState    , EID_GPSError: SkipBytes:= Avk11EventLen[LoadID];

        EID_SysCrd_SS: begin // ��������� ���������� �������� ������, �������� ����������
                         FFileBody.ReadBuffer(ByteOffset, 1);
                         FFileBody.ReadBuffer(ByteCoord, 1);
                         CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or ByteCoord);
                       end;
        EID_SysCrd_SF: begin // ��������� ���������� �������� ������, ������ ����������
                         FFileBody.ReadBuffer(ByteOffset, 1);
                         FFileBody.ReadBuffer(LongCoord, 4);
                         CurSysCoord:= LongCoord;
                       end;
        EID_SysCrd_FS: begin // ��������� ���������� ������ ������, �������� ����������
                         FFileBody.ReadBuffer(LongOffset, 4);
                         FFileBody.ReadBuffer(ByteCoord, 1);
                         CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or ByteCoord);
                       end;
        EID_SysCrd_FF: begin // ��������� ���������� ������ ������, ������ ����������
                         FFileBody.ReadBuffer(LongOffset, 4);
                         FFileBody.ReadBuffer(LongCoord, 4);
                         CurSysCoord:= LongCoord;
                       end;
    end;

    if LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF] then
    begin
      NewCoord:= CurSysCoord - SysCoord1;

      BackLink:= OutDat.FData.Position - LastSaveOffset;

      LastSaveOffset:= OutDat.FData.Position;

      if LastSysCrd and $FFFFFF00 = NewCoord and $FFFFFF00 then
      begin  // �������� ����������
        if BackLink and $FFFFFF00 = 0 then // �������� ������
        begin
          FID:= EID_SysCrd_SS;
          OutDat.FData.WriteBuffer(FID, 1);
          OutDat.FData.WriteBuffer(BackLink, 1);
          OutDat.FData.WriteBuffer(NewCoord, 1);
        end
        else
        begin
          FID:= EID_SysCrd_FS;
          OutDat.FData.WriteBuffer(FID, 1);
          OutDat.FData.WriteBuffer(BackLink, 4);
          OutDat.FData.WriteBuffer(NewCoord, 1);
        end;
      end
      else
      begin  // ������ ����������
        if BackLink and $FFFFFF00 = 0 then // �������� ������
        begin
          FID:= EID_SysCrd_SF;
          OutDat.FData.WriteBuffer(FID, 1);
          OutDat.FData.WriteBuffer(BackLink, 1);
          OutDat.FData.WriteBuffer(NewCoord, 4);
        end
        else
        begin
          FID:= EID_SysCrd_FF;
          OutDat.FData.WriteBuffer(FID, 1);
          OutDat.FData.WriteBuffer(BackLink, 4);
          OutDat.FData.WriteBuffer(NewCoord, 4);
        end;
      end;
      LastSysCrd:= NewCoord;
    end;

    if SkipBytes <> - 1 then
    begin
      FFileBody.Position:= FFileBody.Position - 1;
      CopyMS(FFileBody, OutDat.FData, SkipBytes + 1);
    end;
  until FFileBody.Position >= Offset2;
 
  if Header.MoveDir = 0 then
  begin
    GetCoord(EdDisCood, CoordParams);
    if CoordParams.RightIdx <> - 1 then
    begin
      NewCoord:= FEventsData[CoordParams.RightIdx].Event.SysCoord - SysCoord1;

      while NewCoord - LastSysCrd >= 10000 do
      begin
        Inc(LastSysCrd, 10000);
        BackLink:= OutDat.FData.Position - LastSaveOffset;
        LastSaveOffset:= OutDat.FData.Position;
        FID:= EID_SysCrd_FF;
        OutDat.FData.WriteBuffer(FID, 1);
        OutDat.FData.WriteBuffer(BackLink, 4);
        OutDat.FData.WriteBuffer(LastSysCrd, 4);
      end;

      BackLink:= OutDat.FData.Position - LastSaveOffset;
      LastSaveOffset:= OutDat.FData.Position;
      FID:= EID_SysCrd_FF;
      OutDat.FData.WriteBuffer(FID, 1);
      OutDat.FData.WriteBuffer(BackLink, 4);
      OutDat.FData.WriteBuffer(NewCoord, 4);

      FID:= FEventsData[CoordParams.RightIdx].Event.ID;
      OutDat.FData.WriteBuffer(FID, 1);
      OutDat.FData.WriteBuffer(FEventsData[CoordParams.RightIdx].Data, Avk11EventLen[FID]);
    end;
  end;

  FID:= EID_EndFile;
  OutDat.FData.WriteBuffer(FID, 1);
  for I:= 5 to 13 do Buff[I]:= $FF;
  BackLink:= OutDat.FData.Position - LastSaveOffset;
  OutDat.FData.WriteBuffer(BackLink, 4);
  OutDat.FData.WriteBuffer(Buff, 9);

  OutDat.FData.SaveToFile(FileName);
  OutDat.Free;

  OutDat2:= TAvk11DatSrc.Create;
  OutDat2.LoadFromFile(FileName);

  if SaveNB then
    for I:= 0 to NotebookCount - 1 do
      if (Notebook[I].DisCoord >= StDisCood) and
         (Notebook[I].DisCoord <= EdDisCood) then
      begin
        Tmp:= Notebook[I]^;
        Tmp.DisCoord:= Tmp.DisCoord - StDisCood;
        Tmp.StDisCoord:= Tmp.StDisCoord - StDisCood;
        Tmp.EdDisCoord:= Tmp.EdDisCoord - StDisCood;
        Tmp.SysCoord:= OutDat2.DisToSysCoord(Tmp.DisCoord);
        OutDat2.NotebookAdd(@Tmp);
      end;

  OutDat2.Free;
end;

//------------------------< TAvk11DatDst >---------------------------------------

constructor TAvk11DatDst.Create;
var
  SystemTime: TSystemTime;

begin
  FFirstCoord:= True;
  FLastSaveOffset:= 0;
  FCurDisCrd:= 0;
  Header.ID:= $FF12;
  Header.ScanStep:= 180;
  DateTimeToSystemTime(Now, SystemTime);
  Header.Day:=    XXNumToCode(SystemTime.wDay);
  Header.Month:=  XXNumToCode(SystemTime.wMonth);
  Header.Year:=   XXNumToCode(SystemTime.wYear);
  Header.Hour:=   XXNumToCode(SystemTime.wHour);
  Header.Minute:= XXNumToCode(SystemTime.wMinute);
  Header.TableLink:= $FFFFFFFF;
  FPack[RRail(0)].Ampl:= $FF;
  FPack[RRail(1)].Ampl:= $FF;
end;

destructor  TAvk11DatDst.Destroy;
begin
  FData.Free;
end;

procedure TAvk11DatDst.CreateFile(FileName: string; Size: Integer);
begin
  FFileName:= FileName;
  FData:= TMemoryStream.Create;
  FData.SetSize(Size);
  FData.Position:= 0;
end;

procedure TAvk11DatDst.DeleteFile;
begin
  if FileExists(FFileName) then SysUtils.DeleteFile(FFileName);
end;

procedure TAvk11DatDst.CloseFile;
var
  TMP: Integer;
  Buff: array[5..13] of Byte;

begin
  FID:= EID_EndFile;
  FData.WriteBuffer(FID, 1);
  for Tmp:= 5 to 13 do Buff[Tmp]:= $FF;
  Tmp:= FData.Size - FLastSaveOffset;
  FData.WriteBuffer(Tmp, 4);
  FData.WriteBuffer(Buff, 9);
  FData.SetSize(FData.Position);
  FData.SaveToFile(FFileName);
end;

function TAvk11DatDst.DataToID(Rail, Ch: Byte): Byte;
begin
  case Rail of
    0: case Ch of
         0: Result:= 8 or 1;
         1: Result:=      1;
         2: Result:=      2;
         3: Result:=      3;
         4: Result:=      4;
         5: Result:=      5;
         6: Result:=      6;
         7: Result:=      7;
         8: Result:= 8 or 6;
         9: Result:= 8 or 7;
       end;
    1: case Ch of
         0: Result:= 16 or 8 or 1;
         1: Result:= 16 or      1;
         2: Result:= 16 or      2;
         3: Result:= 16 or      3;
         4: Result:= 16 or      4;
         5: Result:= 16 or      5;
         6: Result:= 16 or      6;
         7: Result:= 16 or      7;
         8: Result:= 16 or 8 or 6;
         9: Result:= 16 or 8 or 7;
       end;
  end;
end;

procedure TAvk11DatDst.AddHeader;
begin
  FData.WriteBuffer(Header, SizeOf(Header));
end;

procedure TAvk11DatDst.FillHeader(DateTime: TDateTime; Org, OpName, Section: string);
var
  SystemTime: TSystemTime;
  Res: packed array [0..19] of Byte;

begin
  Header.ID:= $FF12;

  DateTimeToSystemTime(DateTime, SystemTime);
  Header.Day:= XXNumToCode(SystemTime.wDay);
  Header.Month:= XXNumToCode(SystemTime.wMonth);
  Header.Year:= XXNumToCode(SystemTime.wYear);
  Header.Hour:= XXNumToCode(SystemTime.wHour);
  Header.Minute:= XXNumToCode(SystemTime.wMinute);

  TextToCode(Org, 38, Header.CharSet, Header.Name);
  TextToCode(OpName, 20, Header.CharSet, Header.Operator);
  TextToCode(Section, 20, Header.CharSet, Header.SecName);

  Header.ScanStep:= 178;
  Header.AmplTH:= 3;
  Header.TableLink:= $FFFFFFFF;
end;

procedure TAvk11DatDst.ClearEcho;
begin
  FCh1Flag[0]:= False;
  FCh1Flag[1]:= False;
end;

procedure TAvk11DatDst.AddEcho(Rail, Ch, Count: Byte; D1, A1, D2, A2, D3, A3, D4, A4, D5, A5, D6, A6, D7, A7, D8, A8: Byte);
var
  Cnt: Byte;

begin
  if Ch = 1 then
  begin
    FCh1Flag[Rail]:= Count <> 0;
    if Count = 1 then
    begin
      FPackSysCoord[Rail]:= CurSysCrd;
      if (FPack[RRail(Rail)].Delay = D1) and (FPack[RRail(Rail)].Ampl = A1) then Exit;
      FPack[RRail(Rail)].Delay:= D1;
      FPack[RRail(Rail)].Ampl:= A1;
    end
    else FPack[RRail(Rail)].Ampl:= $FF;
  end;

  if Count > 4 then Cnt:= 4
               else Cnt:= Count;

  FID:= Rail shl 6 + Ch shl 3 + Cnt;
  FData.WriteBuffer(FID, 1);

  case Cnt of
    0: ;
    1: begin
         FData.WriteBuffer(D1, 1);
         FID:= A1 and $0F shl 4;
         FData.WriteBuffer(FID, 1);
       end;
    2: begin
         FData.WriteBuffer(D1, 1);
         FData.WriteBuffer(D2, 1);
         FID:= A1 and $0F shl 4 + A2 and $0F;
         FData.WriteBuffer(FID, 1);
       end;
    3: begin
         FData.WriteBuffer(D1, 1);
         FData.WriteBuffer(D2, 1);
         FData.WriteBuffer(D3, 1);
         FID:= A1 and $0F shl 4 + A2 and $0F;
         FData.WriteBuffer(FID, 1);
         FID:= A3 and $0F shl 4;
         FData.WriteBuffer(FID, 1);
       end;
    4: begin
         FData.WriteBuffer(D1, 1);
         FData.WriteBuffer(D2, 1);
         FData.WriteBuffer(D3, 1);
         FData.WriteBuffer(D4, 1);
         FID:= A1 and $0F shl 4 + A2 and $0F;
         FData.WriteBuffer(FID, 1);
         FID:= A3 and $0F shl 4 + A4 and $0F;
         FData.WriteBuffer(FID, 1);
       end;
  end;

  if Count > 4 then
  begin
    Cnt:= Count - 4;
    FID:= Rail shl 6 + Ch shl 3 + Cnt;
    FData.WriteBuffer(FID, 1);

    case Cnt of
      1: begin
           FData.WriteBuffer(D5, 1);
           FID:= A5 and $0F shl 4;
           FData.WriteBuffer(FID, 1);
         end;
      2: begin
           FData.WriteBuffer(D5, 1);
           FData.WriteBuffer(D6, 1);
           FID:= A5 and $0F shl 4 + A6 and $0F;
           FData.WriteBuffer(FID, 1);
         end;
      3: begin
           FData.WriteBuffer(D5, 1);
           FData.WriteBuffer(D6, 1);
           FData.WriteBuffer(D7, 1);
           FID:= A5 and $0F shl 4 + A6 and $0F;
           FData.WriteBuffer(FID, 1);
           FID:= A7 and $0F shl 4;
           FData.WriteBuffer(FID, 1);
         end;
      4: begin
           FData.WriteBuffer(D5, 1);
           FData.WriteBuffer(D6, 1);
           FData.WriteBuffer(D7, 1);
           FData.WriteBuffer(D8, 1);
           FID:= A5 and $0F shl 4 + A6 and $0F;
           FData.WriteBuffer(FID, 1);
           FID:= A7 and $0F shl 4 + A8 and $0F;
           FData.WriteBuffer(FID, 1);
         end;
    end;
  end;
end;

procedure TAvk11DatDst.AddSens(Rail, Ch: Byte; NewValue: ShortInt); // ��������� �������� ����������������
begin
  FID:= EID_Sens;
  FData.WriteBuffer(FID, 1);
  FID:= DataToID(Rail, Ch);
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(NewValue, 1);
end;

procedure TAvk11DatDst.AddAtt(Rail, Ch, NewValue: Byte); // ��������� ��������� ���� ����������� (0 �� �������� ����������������)
begin
  FID:= EID_Att;
  FData.WriteBuffer(FID, 1);
  FID:= DataToID(Rail, Ch);
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(NewValue, 1);
end;

procedure TAvk11DatDst.AddVRU(Rail, Ch, NewValue: Byte); // ��������� ���
begin
  FID:= EID_VRU;
  FData.WriteBuffer(FID, 1);
  FID:= DataToID(Rail, Ch);
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(NewValue, 1);
end;

procedure TAvk11DatDst.AddStStr(Rail, Ch, NewValue: Byte); // ��������� ��������� ������ ������
begin
  FID:= EID_StStr;
  FData.WriteBuffer(FID, 1);
  FID:= DataToID(Rail, Ch);
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(NewValue, 1);
end;

procedure TAvk11DatDst.AddEndStr(Rail, Ch, NewValue: Byte); // ��������� ��������� ����� ������
begin
  FID:= EID_EndStr;
  FData.WriteBuffer(FID, 1);
  FID:= DataToID(Rail, Ch);
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(NewValue, 1);
end;

procedure TAvk11DatDst.Add2Tp(Rail, Ch, NewValue: Byte); // ��������� 2��
begin
  FID:= EID_2Tp;
  FData.WriteBuffer(FID, 1);
  FID:= DataToID(Rail, Ch);
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(NewValue, 1);
end;

procedure TAvk11DatDst.AddAK(Rail: Byte; Ch1, Ch2, Ch3, Ch4, Ch5, Ch6, Ch7, Ch8: Boolean); // ��������� AK
begin
  case Rail of
    0: FID:= EID_ACLeft;
    1: FID:= EID_ACRight;
  end;
  FData.WriteBuffer(FID, 1);
  FID:= Ord(Ch1) + Ord(Ch2) * 2 + Ord(Ch3) * 4 + Ord(Ch4) * 8 + Ord(Ch5) * 16 + Ord(Ch6) * 32 + Ord(Ch7) * 64 + Ord(Ch8) * 128;
  FData.WriteBuffer(FID, 1);
end;

procedure TAvk11DatDst.AddHandScan(Dat: THandScanData);
var
  I: Integer;
  Tmp: THSItem;

begin
  FID:= EID_HandScan;
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(Dat.HSHead, SizeOf(THSHead));

  for I:= 0 to High(Dat.HSItem) do
    FData.WriteBuffer(Dat.HSItem[I], 3 + Avk11EchoLen[Dat.HSItem[I].Count]);

  Tmp.Sample:= $FFFF;
  Tmp.Count:= 0;
  FData.WriteBuffer(Tmp, 3);
end;

procedure TAvk11DatDst.AddTextLabel(Text: string);
var
  Res: packed array [0..19] of Byte;

begin
  FID:= EID_TextLabel;
  FData.WriteBuffer(FID, 1);
  TextToCode(Text, 20, Header.CharSet, Res);
  FData.WriteBuffer(Res, 20);
  FID:= 0;
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(FID, 1);
end;

procedure TAvk11DatDst.AddSysCoord(SysCoord: Integer);
var
  R: RRail;
  I: Integer;
  BackLink: Integer;

begin
  Inc(FCurDisCrd, Abs(SysCoord - FLastSysCrd)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)

  CurSysCrd:= SysCoord;
  for R:= r_Left to r_Right do
    if (FPack[R].Ampl <> $FF) and (not FCh1Flag[Ord(R)]) and (Abs(SysCoord - FPackSysCoord[Ord(R)]) > 50) then
    begin
      FPack[R].Ampl:= $FF;
      AddEcho(Ord(R), 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    end;

  if FFirstCoord then
  begin
    BackLink:= 0;
    FFirstCoord:= False;
  end else BackLink:= FData.Position - FLastSaveOffset;


  FLastSaveOffset:= FData.Position;

  if FLastSysCrd and $FFFFFF00 = SysCoord and $FFFFFF00 then
  begin  // �������� ����������
    if BackLink and $FFFFFF00 = 0 then // �������� ������
    begin
      FID:= EID_SysCrd_SS;
      FData.WriteBuffer(FID, 1);
      FData.WriteBuffer(BackLink, 1);
      FData.WriteBuffer(SysCoord, 1);
    end
    else
    begin
      FID:= EID_SysCrd_FS;
      FData.WriteBuffer(FID, 1);
      FData.WriteBuffer(BackLink, 4);
      FData.WriteBuffer(SysCoord, 1);
    end;
  end
  else
  begin  // ������ ����������
    if BackLink and $FFFFFF00 = 0 then // �������� ������
    begin
      FID:= EID_SysCrd_SF;
      FData.WriteBuffer(FID, 1);
      FData.WriteBuffer(BackLink, 1);
      FData.WriteBuffer(SysCoord, 4);
    end
    else
    begin
      FID:= EID_SysCrd_FF;
      FData.WriteBuffer(FID, 1);
      FData.WriteBuffer(BackLink, 4);
      FData.WriteBuffer(SysCoord, 4);
    end;
  end;

  FLastSysCrd:= SysCoord;
end;

procedure TAvk11DatDst.AddHeadPh(R0_Ch0, R0_Ch1, R0_Ch2, R0_Ch3, R0_Ch4, R0_Ch5, R0_Ch6, R0_Ch7, R0_Ch8, R0_Ch9,
                                 R1_Ch0, R1_Ch1, R1_Ch2, R1_Ch3, R1_Ch4, R1_Ch5, R1_Ch6, R1_Ch7, R1_Ch8, R1_Ch9: Boolean);
var
  B: Byte;

begin
  FID:= EID_HeadPh;
  FData.WriteBuffer(FID, 1);
  B:= 32 * Ord(R0_Ch0) + 16 * Ord(R0_Ch1) + 8 * Ord(R0_Ch2) + 4 * Ord(R0_Ch3) + 2 * Ord(R0_Ch4) + Ord(R0_Ch5);
  FData.WriteBuffer(B, 1);
  B:= 128 * Ord(R0_Ch6) + 64 * Ord(R0_Ch7) + 32 * Ord(R0_Ch8) + 16 * Ord(R0_Ch9);
  FData.WriteBuffer(B, 1);
  B:= 32 * Ord(R1_Ch0) + 16 * Ord(R1_Ch1) + 8 * Ord(R1_Ch2) + 4 * Ord(R1_Ch3) + 2 * Ord(R1_Ch4) + Ord(R1_Ch5);
  FData.WriteBuffer(B, 1);
  B:= 128 * Ord(R1_Ch6) + 64 * Ord(R1_Ch7) + 32 * Ord(R1_Ch8) + 16 * Ord(R1_Ch9);
  FData.WriteBuffer(B, 1);
end;

procedure TAvk11DatDst.AddMode(ModeIdx: Byte);
begin
  FID:= EID_Mode;
  FData.WriteBuffer(FID, 1);
  FID:= ModeIdx;
  FData.WriteBuffer(FID, 1);
end;

procedure TAvk11DatDst.AddZondImp(Rail, Ch, Val: Byte);
begin
  FID:= EID_ZondImp; // ��������� ������ ������������ ��������
  FData.WriteBuffer(FID, 1);
  FID:= DataToID(Rail, Ch);
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(Val, 1);
end;

procedure TAvk11DatDst.AddRailType;
begin
  FID:= EID_SetRailType;
  FData.WriteBuffer(FID, 1);
end;

procedure TAvk11DatDst.AddStolb(Km0, Km1: Smallint; Pk0, Pk1: Byte);
var
  Dat: mCoord;

begin
  FID:= EID_Stolb;
  FData.WriteBuffer(FID, 1);
  FillChar(Dat, 0, SizeOf(mCoord));
  Dat.Km[0]:= Km0;
  Dat.Km[1]:= Km1;
  Dat.Pk[0]:= Pk0;
  Dat.Pk[1]:= Pk1;
  FData.WriteBuffer(Dat, SizeOf(mCoord));
end;

procedure TAvk11DatDst.AddStrelka(Text: string);
var
  Res: packed array [0..5] of Byte;

begin
  FID:= EID_Strelka; // ����� ����������� �������� + �����
  FData.WriteBuffer(FID, 1);
  TextToCode(Text, 6, Header.CharSet, Res);
  FData.WriteBuffer(Res, 6);
  FData.WriteBuffer(Res, 6);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);
end;

procedure TAvk11DatDst.AddDefLabel(Rail: Byte; Text: string);
var
  Res: packed array [0..4] of Byte;

begin
  FID:= EID_DefLabel; // ����� ������� + �����
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(Rail, 1);
  TextToCode(Text, 5, Header.CharSet, Res);
  FData.WriteBuffer(Res, 5);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);
end;

procedure TAvk11DatDst.AddStBoltStyk;
begin
  FID:= EID_StBoltStyk;
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);
end;

procedure TAvk11DatDst.AddEdBoltStyk;
begin
  FID:= EID_EndBoltStyk;
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);
end;

procedure TAvk11DatDst.AddTime(Haur, Minute: Byte);
begin
  FCurHour:= Haur;
  FCurMinute:= Minute;
  FID:= EID_Time; // ������� �������
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);
end;

procedure TAvk11DatDst.AddCurTime;
begin
  FID:= EID_Time; // ������� �������
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);
end;

procedure TAvk11DatDst.AddStLineLabel;
begin
  FID:= EID_StLineLabel; // ������ ����������� ����� + �����
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);
end;

procedure TAvk11DatDst.AddEdLineLabel;
begin
  FID:= EID_EndLineLabel; // ����� ����������� ����� + �����
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);
end;

procedure TAvk11DatDst.AddStSwarStyk;
begin
  FID:= EID_StSwarStyk; // ������� ������ ������� ���� + �����
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);
end;

procedure TAvk11DatDst.AddEdSwarStyk;
begin
  FID:= EID_EndSwarStyk; // ���������� ������ ������� ���� + �����
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);
end;

procedure TAvk11DatDst.AddOpChange(NewOpName: string);
var
  Res: packed array [0..19] of Byte;

begin
  FID:= EID_OpChange; // ����� ���������
  FData.WriteBuffer(FID, 1);
  TextToCode(NewOpName, 20, Header.CharSet, Res);
  FData.WriteBuffer(Res, 20);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);
end;

procedure TAvk11DatDst.AddPathChange(NewPath: Byte);
begin
  FID:= EID_PathChange; // ����� ������ ����
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(NewPath, 1);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);
end;

procedure TAvk11DatDst.AddMovDirChange(NewDir: Byte);
begin
  FID:= EID_MovDirChange; // ����� ����������� ��������
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(NewDir, 1);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);
end;

procedure TAvk11DatDst.AddSezdNo(Text: string);
var
  Res: packed array [0..9] of Byte;

begin
  FID:= EID_SezdNo; // ����� ������
  FData.WriteBuffer(FID, 1);
  TextToCode(Text, 10, Header.CharSet, Res);
  FData.WriteBuffer(Res, 10);
  FData.WriteBuffer(Res, 10);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);
end;

procedure TAvk11DatDst.AddLongLabel(Id: Byte; Start: Boolean); // ����������� ����� 2
var
  Tmp: Byte;

begin
  FID:= EID_LongLabel;
  FData.WriteBuffer(FID, 1);
  if Start then Tmp:= ID and $7F
           else Tmp:= ID and $7F or 80;
  FData.WriteBuffer(Tmp, 1);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);
end;

procedure TAvk11DatDst.SetTime(Time: TTime);
var
  SystemTime: TSystemTime;

begin
  DateTimeToSystemTime(Time, SystemTime);
  FCurHour:= XXNumToCode(SystemTime.wHour);
  FCurMinute:= XXNumToCode(SystemTime.wMinute);
end;

//------------------------< TAvk11DataContainer >---------------------------------------

constructor TMyStream.Create;
begin
  FPtr:= nil;
  FSize:= 0;
  FCap:= 0;
  Position:= 0;
  Position1:= 0;
  Position2:= 0;
  FFlg[1]:= False;
  FFlg[2]:= False;
end;

destructor TMyStream.Destroy;
begin
  FreeMem(FPtr);
{
  FSize: Pointer;
  FCap: Pointer;
  Position: Integer;
  Position1: Integer;
  Position2: Integer;
}
end;
(*
procedure TMyStream.SaveToFile(FileName: string);
var
  F: file;
  P: Pointer;

begin
  if (FSize > 0) then
  begin
    P:= Pointer(Integer(FData.Memory) + FLastSave);

    AssignFile(F, FFileName);
    if FileExists(FFileName) then Reset(F, 1)
                             else ReWrite(F, 1);
    Seek(F, FileSize(F));
    BlockWrite(F, P^, FData.Size - FLastSave);
    System.CloseFile(F);
    FLastSave:= FData.Size;
  end;
  FPtr: Pointer;
  FSize: Pointer;
  FCap: Pointer;

  Position: Integer;

  Position1: Integer;
  Position2: Integer;
end;
*)

procedure TMyStream.WriteBuffer(const Buffer; Count: Longint);
begin
  if Position + Count > FCap then
  begin
    FFlg[2]:= True;
    while FFlg[1] do Sleep(0);
    Inc(FCap, 1024 * 1024);
    ReallocMem(FPtr, FCap);
    FFlg[2]:= False;
  end;
  Move(Buffer, Pointer(Longint(FPtr) + Position)^, Count);
  Inc(Position, Count);
  FSize:= Position;
end;

procedure TMyStream.ReadBuffer(var Buffer; Count: Longint);
begin
  Move(Pointer(Longint(FPtr) + Position)^, Buffer, Count);
  Inc(Position, Count);
end;

procedure TMyStream.ReadBuffer1(var Buffer; Count: Longint);
begin
  if Position1 + Count > FSize then raise EMyStreamExcept.Create('Position1 out of bounds');
{
  begin
    ShowMessage('Error 1 !');
    Exit;
  end;
}
  Move(Pointer(Longint(FPtr) + Position1)^, Buffer, Count);
  Inc(Position1, Count);
end;

procedure TMyStream.ReadBuffer2(var Buffer; Count: Longint);
begin
  while FFlg[2] do Sleep(0);
  FFlg[1]:= True;
  if Position2 + Count > FSize then raise EMyStreamExcept.Create('Position2 out of bounds');
{  begin
    ShowMessage('Error 2 !');
    Exit;
  end; }
  Move(Pointer(Longint(FPtr) + Position2)^, Buffer, Count);
  Inc(Position2, Count);
  FFlg[1]:= False;
end;


//------------------------< TMyArray >------------------------------------------

constructor TMyArray.Create(CreateVirtualItem: Boolean);
begin
  FItemSize:= SizeOf(TContEvent);
  FAddIdx:= 0;
  FCreateVirtItem:= CreateVirtualItem;
  FCount:= Ord(FCreateVirtItem);
end;

destructor TMyArray.Destroy;
var
  I: Integer;

begin
  for I:= 0 to High(FPtr) do FreeMem(FPtr[I]);
  SetLength(FPtr, 0);
end;

function TMyArray.Add(const Buffer): Integer;
begin
  if (FAddIdx > 999) or (Length(FPtr) = 0) then
  begin
    SetLength(FPtr, Length(FPtr) + 1);
    GetMem(FPtr[High(FPtr)], 1000 * FItemSize);
    FAddIdx:= 0;
  end;
  Move(Buffer, Pointer(Longint(FPtr[High(FPtr)]) + FAddIdx * FItemSize)^, FItemSize);
  Result:= FAddIdx;
  Inc(FAddIdx);
  Inc(FCount);
end;

function TMyArray.GetPtr(Index: Integer): TContEvent;
var
  I: Integer;

begin
  if FCreateVirtItem and (Index = FCount - 1) then
  begin
    Result:= VirtItem;
    Exit;
  end;
  I:= 0;
  while Index >= 1000 do
  begin
    Index:= Index - 1000;
    Inc(I);
  end;
  if Index >= Count then raise EMyStreamExcept.Create('Position out of bounds');
{  begin
    ShowMessage('Error 3');
    Exit;
  end; }
  try
    Result:= TContEvent(Pointer(Longint(FPtr[I]) + Index * FItemSize)^);
  except
//    ShowMessage(Format('Index: %d; PtrArrIdx: %d', [Index, I]));
    raise EMyStreamExcept.Create('Position out of bounds' + Format(' Index: %d; PtrArrIdx: %d', [Index, I]));
  end;
end;

//------------------------< TAvk11DataContainer >---------------------------------------

constructor TAvk11DataContainer.Create;
var
  R: RRail;
  I: Integer;
  SystemTime: TSystemTime;
  Ext: string;

begin
//  FHandScanReg:= False;
  FFileType:= FileType;

  Ext:= ExtractFileExt(FileName);
  I:= Pos(Ext, FileName);
  if I <> 0 then Delete(FileName, I, Length(Ext));

  case FFileType of
    aftAvikon11: FileName:= FileName + '.a11';
    aftAvikon12: FileName:= FileName + '.a12';
    aftAvikon14: FileName:= FileName + '.a14';
    aftAvikon15: FileName:= FileName + '.a15';
    aftAvikon16: FileName:= FileName + '.a16';
  end;

  DeleteFile(FileName);
  FFileName:= FileName;
  FFirstCoord:= True;
  FLastDisCrd:= 0;
  FLastSysCrd:= 0;
  FLastSaveOffset:= 0;
  FCurDisCrd:= 0;
  FCurSysCrd:= 0;
  FLastSave:= 0;
  FEventsCount:= 0;
  FStolbCount:= 0;

  FBackMotionFlag:= False;
  FSearchBMEnd:= False;
  FLeftFlag:= False;
  FRightFlag:= False;

  LoadPackReset;

  Header.ID:= $FF12;
  Header.ScanStep:= 180;
  DateTimeToSystemTime(Now, SystemTime);
  Header.Day:=    XXNumToCode(SystemTime.wDay);
  Header.Month:=  XXNumToCode(SystemTime.wMonth);
  Header.Year:=   XXNumToCode(SystemTime.wYear);
  Header.Hour:=   XXNumToCode(SystemTime.wHour);
  Header.Minute:= XXNumToCode(SystemTime.wMinute);
  Header.TableLink:= $FFFFFFFF;
  FSavePack[RRail(0)].Ampl:= $FF;
  FSavePack[RRail(1)].Ampl:= $FF;
  FRSPSkipSideRail:= False;

//  FFileName:= FileName;
  FData:= TMyStream.Create;
//  FData:= TMemoryStream.Create;
  FData.Position:= 0;

  FMaxSysCoord:= 0;
  FillChar(FCurParam, SizeOf(FCurParam), 0); // ������������ ������ ��������� ��������
  for R:= r_Left to r_Right do
    for I:= 0 to 9 do FCurParam.HeadPh[R, I]:= True;

  FEvents:= TMyArray.Create(True);
  FEvents.VirtItem.ID:= EID_EndFile;

  FParDat:= TMemoryStream.Create;
end;

destructor  TAvk11DataContainer.Destroy;
var
  TMP, I: Integer;
  Buff: array[5..13] of Byte;
  F: file;
  CheckSumm: Int64;
  Card: DWord;
  Evt: TFileEvent;

begin

  if (FEventsCount <> 0) and (FEvents[FEventsCount - 1].DisCoord <> FCurDisCrd) then
  begin
    FOffset[1]:= FData.Position;
    AddEvent(EID_LinkPt);
  end;

                                             // ������ ������� ����� �����
  FOffset[1]:= FData.Position;

  FID:= EID_EndFile;
  FData.WriteBuffer(FID, 1);
  for Tmp:= 5 to 13 do Buff[Tmp]:= $FF;
  Tmp:= FData.Size - FLastSaveOffset;
  FData.WriteBuffer(Tmp, 4);
  FData.WriteBuffer(Buff, 9);
                                             // ������ ������� ����� �����
  AddEvent(EID_EndFile);

  SaveData(False);

  AssignFile(F, FFileName);                // �������� ����� ��� ������
  Reset(F, 1);

  {$OVERFLOWCHECKS OFF}
  CheckSumm:= CheckSummStartValue;
  FData.Position:= 0;
  for I:= 1 to 29 do
  begin
    FData.ReadBuffer(Card, 4);
    CheckSumm:= CheckSumm + Card;
  end;
  FData.Position:= FData.Position + 3 * 4;
  while FData.Position + 4 < FileSize(F) do
  begin
    FData.ReadBuffer(Card, 4);
    CheckSumm:= CheckSumm + Card;
  end;
  Header.CheckSumm:= CheckSumm and $00000000FFFFFFFF;
  {$OVERFLOWCHECKS ON}

  Seek(F, 0);
  Header.TableLink:= FileSize(F);               // ����������� ��������� ������ ������������ ���������
  BlockWrite(F, Header, Sizeof(TAvk11Header2)); // ������ ������ �������� ��������� ������������ ���������

  FillChar(FExHeader.FFConst[0], Length(FExHeader.FFConst), $FF);
  FillChar(FExHeader.Reserv1[1], Length(FExHeader.Reserv1), $FF);
  FillChar(FExHeader.Reserv2[1], Length(FExHeader.Reserv2), $FF);

  if FLeftFlag and FRightFlag then FExHeader.DataRail:= r_Both else
  if FLeftFlag  then FExHeader.DataRail:= r_Left else
  if FRightFlag then FExHeader.DataRail:= r_Right;

  FExHeader.EndKM:= FLastStolb.Km[1];
  FExHeader.EndPK:= FLastStolb.Pk[1];
  FExHeader.EndMM:= (CurSysCoord - FLastStolbSysCrd) * Header.ScanStep div 100;
  FExHeader.EndSysCoord:= FCurSysCrd;
  FExHeader.EndDisCoord:= FCurDisCrd;

  FExHeader.DataVer:= 6;
  FExHeader.DataSize:= SizeOf(TExHeader_5);
  FExHeader.Flag:= 0;

  Seek(F, Header.TableLink);                   // ������ ������ ������������ ���������
  BlockWrite(F, FExHeader, FExHeader.DataSize);

  FExHeader.ErrorDataVer:= 3;                   // ������ ������ ������ ��������� �����
  FExHeader.ErrorItemSize:= SizeOf(TFileError);
  FExHeader.ErrorCount:= 0;
  FExHeader.ErrorOffset:= 0;

  FExHeader.EventDataVer:= 3;                   // ������ ������ ������� �����
  FExHeader.EventItemSize:= SizeOf(TFileEvent);
  FExHeader.EventCount:= FEventsCount - 1;
  FExHeader.EventOffset:= 0;
  if FExHeader.EventCount <> 0 then
  begin
    FExHeader.EventOffset:= FilePos(F);

    for I:= 0 to FEvents.Count - 2 do
    begin
      Evt.ID      := FEvents[I].ID;
      Evt.OffSet  := FEvents[I].OffSet;
      Evt.SysCoord:= FEvents[I].SysCoord;
      Evt.DisCoord:= FEvents[I].DisCoord;
      Evt.Pack    := FEvents[I].Pack;

      BlockWrite(F, Evt, FExHeader.EventItemSize);
    end;
  end;

  FExHeader.NotebookDataVer:= 4;                // ������ ������ ������� �������������
  FExHeader.NotebookOffset:= 0;
  FExHeader.NotebookItemSize:= SizeOf(TFileNotebook_Ver4);
  FExHeader.NotebookCount:= 0;

  FExHeader.DecodingDataVer:= 3;                 // ������ ������ ������� � ������ �����������
  FExHeader.DecodingOffset:= 0;
  FExHeader.DecodingItemSize:= SizeOf(TFileDecoding);
  FExHeader.DecodingCount:= 0;

  FExHeader.RSPDataVer:= 3;                 // ������ ������ ������ ���
  FExHeader.RSPOffset:= 0;
  FExHeader.RSPItemSize:= SizeOf(TRSPInfo);
  FExHeader.RSPCount:= 0;

  FExHeader.UCSDataVer:= 3;
  FExHeader.UCSItemSize:= 0;
  FExHeader.UCSOffset:= 0;
  FExHeader.UCSCount:= 0;

  FExHeader.LabelEditDataVer:= 3;
  FExHeader.LabelEditItemSize:= 0;
  FExHeader.LabelEditOffset:= 0;
  FExHeader.LabelEditCount:= 0;
{
  FExHeader.ValueListDataVer:= 1;               // ������ ������ ��������
  FExHeader.ValueListItemSize:= FParDat.Size;
  FExHeader.ValueListOffset:= 0;
  FExHeader.ValueListCount:= 1;
  if FParDat.Size <> 0 then
  begin
    FExHeader.ValueListOffset:= FilePos(F);
    BlockWrite(F, FParDat.Memory^, FParDat.Size);
  end;
}                                                 // ������ ������ ������������ ���������
  Seek(F, Header.TableLink);
  BlockWrite(F, FExHeader, FExHeader.DataSize);
  Close(F);

  FData.Free;

  SetLength(FTimeList, 0);
  SetLength(FCDList, 0);

  FEvents.Free;
  FParDat.Free;
end;

function TAvk11DataContainer.DataToID(Rail, Ch: Byte): Byte;
begin
  case Rail of
    0: case Ch of
         0: Result:= 8 or 1;
         1: Result:=      1;
         2: Result:=      2;      // ����� 2 - �������� 1
         3: Result:=      3;      // ����� 3 - �������� 1
         4: Result:=      4;
         5: Result:=      5;
         6: Result:=      6;
         7: Result:=      7;
         8: Result:= 8 or 6;
         9: Result:= 8 or 7;

        10: Result:= 8 or 2;      // ����� 2 - �������� 2
        11: Result:= 8 or 3;      // ����� 3 - �������� 2

       end;
    1: case Ch of
         0: Result:= 16 or 8 or 1;
         1: Result:= 16 or      1;
         2: Result:= 16 or      2;  // ����� 2 - �������� 1
         3: Result:= 16 or      3;  // ����� 3 - �������� 1
         4: Result:= 16 or      4;
         5: Result:= 16 or      5;
         6: Result:= 16 or      6;
         7: Result:= 16 or      7;
         8: Result:= 16 or 8 or 6;
         9: Result:= 16 or 8 or 7;

        10: Result:= 16 or 8 or 2;  // ����� 2 - �������� 2
        11: Result:= 16 or 8 or 3;  // ����� 3 - �������� 2
       end;
  end;
end;

procedure TAvk11DataContainer.AddHeader;
begin
  FData.WriteBuffer(Header, SizeOf(Header));

  FOldH:= (Header.Hour shr 4 and $0F) * 10 + (Header.Hour and $0F);
  FOldM:= (Header.Minute shr 4 and $0F) * 10 + (Header.Minute and $0F);
  SetLength(FTimeList, 1);
  FTimeList[High(FTimeList)].H:= FOldH;
  FTimeList[High(FTimeList)].M:= FOldM;
  FTimeList[High(FTimeList)].DC:= 0;

  FOffset[1]:= FData.Position;
  SetLength(FCoordList, 1);
  FCoordList[0].KM:= Header.StartKM;
  with FCoordList[High(FCoordList)] do
  begin
    SetLength(Content, Length(Content) + 1);
    Content[High(Content)].Pk:= Header.StartPk;
    Content[High(Content)].Idx:= 0;
    FLastKm:= High(FCoordList);
    FLastPk:= High(Content);
  end;
  AddEvent(EID_FwdDir, 0, 0);
end;

procedure TAvk11DataContainer.AddValueList;
var
  Size: Integer;

begin
  FOffset[1]:= FData.Position;
  FID:= EID_NewHeader;
  FData.WriteBuffer(FID, 1);
  Size:= FParDat.Size;
  FData.WriteBuffer(Size, 4);
  FData.WriteBuffer(FParDat.Memory^, Size);
  AddEvent(EID_NewHeader);
end;

procedure TAvk11DataContainer.FillHeader(DateTime: TDateTime; CharSet: Integer; Org, OpName, Section: string);
var
  SystemTime: TSystemTime;
  Res: packed array [0..19] of Byte;

begin
  case FFileType of
    aftAvikon11: Header.ID:= $FF12;
    aftAvikon12: Header.ID:= $FF14;
    aftAvikon14: Header.ID:= $FF15;
    aftAvikon15: Header.ID:= $FF16;
    aftAvikon16: Header.ID:= $FF17;
  end;

  DateTimeToSystemTime(DateTime, SystemTime);
  Header.Day:= XXNumToCode(SystemTime.wDay);
  Header.Month:= XXNumToCode(SystemTime.wMonth);
  Header.Year:= XXNumToCode(SystemTime.wYear);
  Header.Hour:= XXNumToCode(SystemTime.wHour);
  Header.Minute:= XXNumToCode(SystemTime.wMinute);
  Header.ContentFlg:= 1;

  Header.ScanStep:= 178;
  Header.AmplTH:= 3;
  Header.TableLink:= $FFFFFFFF;
  Header.CharSet:= CharSet;

  TextToCode(Org, 38, Header.CharSet, Header.Name);
  TextToCode(OpName, 20, Header.CharSet, Header.Operator);
  TextToCode(Section, 20, Header.CharSet, Header.SecName);
end;

procedure TAvk11DataContainer.ClearEcho;
begin
  FCh1Flag[0]:= False;
  FCh1Flag[1]:= False;
end;

procedure TAvk11DataContainer.AddEcho(Rail, Ch, Count: Byte; D1, A1, D2, A2, D3, A3, D4, A4, D5, A5, D6, A6, D7, A7, D8, A8: Byte);
var
  Cnt: Byte;

begin
  FCh1Echo[Rail]:= FCh1Echo[Rail] or ((Ch = 1) and (Count <> 0));

  case SideToRail(TSide(Rail)) of    // ������� ������ � ������� ������ � ������ ����
    r_Left: FLeftFlag:= True;
   r_Right: FRightFlag:= True;
  end;

  if Ch = 0 then
  begin
    Cnt:= 1;
    FID:= Rail shl 6 + Ch shl 3 + Cnt;
    FData.WriteBuffer(FID, 1);
    FData.WriteBuffer(D1, 1);
    Exit;
  end;
  if Ch = 1 then
  begin
    FCh1Flag[Rail]:= Count <> 0;
    if Count = 1 then
    begin
      FPackSysCoord[Rail]:= FCurSysCrd;
      if (FSavePack[RRail(Rail)].Delay = D1) and (FSavePack[RRail(Rail)].Ampl = A1) then Exit;
      FSavePack[RRail(Rail)].Delay:= D1;
      FSavePack[RRail(Rail)].Ampl:= A1;
    end
    else FSavePack[RRail(Rail)].Ampl:= $FF;
  end;

  if Count > 4 then Cnt:= 4
               else Cnt:= Count;

  FID:= Rail shl 6 + Ch shl 3 + Cnt;
  FData.WriteBuffer(FID, 1);

  case Cnt of
    0: ;
    1: begin
         FData.WriteBuffer(D1, 1);
         FID:= A1 and $0F shl 4;
         FData.WriteBuffer(FID, 1);
       end;
    2: begin
         FData.WriteBuffer(D1, 1);
         FData.WriteBuffer(D2, 1);
         FID:= A1 and $0F shl 4 + A2 and $0F;
         FData.WriteBuffer(FID, 1);
       end;
    3: begin
         FData.WriteBuffer(D1, 1);
         FData.WriteBuffer(D2, 1);
         FData.WriteBuffer(D3, 1);
         FID:= A1 and $0F shl 4 + A2 and $0F;
         FData.WriteBuffer(FID, 1);
         FID:= A3 and $0F shl 4;
         FData.WriteBuffer(FID, 1);
       end;
    4: begin
         FData.WriteBuffer(D1, 1);
         FData.WriteBuffer(D2, 1);
         FData.WriteBuffer(D3, 1);
         FData.WriteBuffer(D4, 1);
         FID:= A1 and $0F shl 4 + A2 and $0F;
         FData.WriteBuffer(FID, 1);
         FID:= A3 and $0F shl 4 + A4 and $0F;
         FData.WriteBuffer(FID, 1);
       end;
  end;

  if Count > 4 then
  begin
    Cnt:= Count - 4;
    FID:= Rail shl 6 + Ch shl 3 + Cnt;
    FData.WriteBuffer(FID, 1);

    case Cnt of
      1: begin
           FData.WriteBuffer(D5, 1);
           FID:= A5 and $0F shl 4;
           FData.WriteBuffer(FID, 1);
         end;
      2: begin
           FData.WriteBuffer(D5, 1);
           FData.WriteBuffer(D6, 1);
           FID:= A5 and $0F shl 4 + A6 and $0F;
           FData.WriteBuffer(FID, 1);
         end;
      3: begin
           FData.WriteBuffer(D5, 1);
           FData.WriteBuffer(D6, 1);
           FData.WriteBuffer(D7, 1);
           FID:= A5 and $0F shl 4 + A6 and $0F;
           FData.WriteBuffer(FID, 1);
           FID:= A7 and $0F shl 4;
           FData.WriteBuffer(FID, 1);
         end;
      4: begin
           FData.WriteBuffer(D5, 1);
           FData.WriteBuffer(D6, 1);
           FData.WriteBuffer(D7, 1);
           FData.WriteBuffer(D8, 1);
           FID:= A5 and $0F shl 4 + A6 and $0F;
           FData.WriteBuffer(FID, 1);
           FID:= A7 and $0F shl 4 + A8 and $0F;
           FData.WriteBuffer(FID, 1);
         end;
    end;
  end;
end;

procedure TAvk11DataContainer.AddEcho(Rail, Ch, Count: Byte; EchoBlock: TEchoBlock );
begin
  AddEcho( Rail, Ch, Count,
    EchoBlock[0].T, EchoBlock[0].A,
    EchoBlock[1].T, EchoBlock[1].A,
    EchoBlock[2].T, EchoBlock[2].A,
    EchoBlock[3].T, EchoBlock[3].A,
    EchoBlock[4].T, EchoBlock[4].A,
    EchoBlock[5].T, EchoBlock[5].A,
    EchoBlock[6].T, EchoBlock[6].A,
    EchoBlock[7].T, EchoBlock[7].A );
end;
{
procedure TAvk11DataContainer.AddBSAmpl(Rail, Data: Byte);
begin
  //
end;
}
procedure TAvk11DataContainer.AddSens(Rail, Ch: Byte; NewValue: ShortInt); // ��������� �������� ����������������
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 1');
    Exit;
  end; }

  if not (Ch in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]) then raise EDataContExcept.Create('Error Channel Number!');

  FOffset[1]:= FData.Position;

  FID:= EID_Sens;
  FData.WriteBuffer(FID, 1);
  FID:= DataToID(Rail, Ch);
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(NewValue, 1);

  AddEvent(EID_Sens);
end;

procedure TAvk11DataContainer.AddAtt(Rail, Ch, NewValue: Byte); // ��������� ��������� ���� ����������� (0 �� �������� ����������������)
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 2');
    Exit;
  end; }

  if not (Ch in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]) then raise EDataContExcept.Create('Error Channel Number!');

  FOffset[1]:= FData.Position;

  FID:= EID_Att;
  FData.WriteBuffer(FID, 1);
  FID:= DataToID(Rail, Ch);
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(NewValue, 1);

  AddEvent(EID_Att);
end;

procedure TAvk11DataContainer.AddVRU(Rail, Ch, NewValue: Byte); // ��������� ���
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 3');
    Exit;
  end; }

  if not (Ch in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]) then raise EDataContExcept.Create('Error Channel Number!');

  FOffset[1]:= FData.Position;

  FID:= EID_VRU;
  FData.WriteBuffer(FID, 1);
  FID:= DataToID(Rail, Ch);
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(NewValue, 1);

  AddEvent(EID_VRU);
end;

procedure TAvk11DataContainer.AddStStr(Rail, Ch, NewValue: Byte); // ��������� ��������� ������ ������
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 4');
    Exit;
  end; }

  if not (Ch in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]) then raise EDataContExcept.Create('Error Channel Number!');

  FOffset[1]:= FData.Position;

  FID:= EID_StStr;
  FData.WriteBuffer(FID, 1);
  FID:= DataToID(Rail, Ch);
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(NewValue, 1);

  AddEvent(EID_StStr);
end;

procedure TAvk11DataContainer.AddEndStr(Rail, Ch, NewValue: Byte); // ��������� ��������� ����� ������
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 5');
    Exit;
  end; }

  if not (Ch in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]) then raise EDataContExcept.Create('Error Channel Number!');

  FOffset[1]:= FData.Position;

  FID:= EID_EndStr;
  FData.WriteBuffer(FID, 1);
  FID:= DataToID(Rail, Ch);
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(NewValue, 1);

  AddEvent(EID_EndStr);
end;

procedure TAvk11DataContainer.Add2Tp(Rail, Ch, NewValue: Byte); // ��������� 2��
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 6');
    Exit;
  end; }

  if not (Ch in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]) then raise EDataContExcept.Create('Error Channel Number!');

  FOffset[1]:= FData.Position;

  FID:= EID_2Tp;
  FData.WriteBuffer(FID, 1);
  FID:= DataToID(Rail, Ch);
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(NewValue, 1);

  AddEvent(EID_2Tp);
end;

procedure TAvk11DataContainer.Add2Tp_Word(Rail, Ch, NewValue: Word); // ��������� 2�� (Word)
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 7');
    Exit;
  end; }

  if not (Ch in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]) then raise EDataContExcept.Create('Error Channel Number!');

  FOffset[1]:= FData.Position;

  FID:= EID_2Tp_Word;
  FData.WriteBuffer(FID, 1);
  FID:= DataToID(Rail, Ch);
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(NewValue, 2);

  AddEvent(EID_2Tp_Word);
end;
procedure TAvk11DataContainer.AddAK(Rail: Byte; Ch1, Ch2, Ch3, Ch4, Ch5, Ch6, Ch7, Ch8: Boolean); // ��������� AK
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 8');
    Exit;
  end; }

  case Rail of
    0: FID:= EID_ACLeft;
    1: FID:= EID_ACRight;
  end;
  FData.WriteBuffer(FID, 1);
  FID:= Ord(Ch1) + Ord(Ch2) * 2 + Ord(Ch3) * 4 + Ord(Ch4) * 8 + Ord(Ch5) * 16 + Ord(Ch6) * 32 + Ord(Ch7) * 64 + Ord(Ch8) * 128;
  FData.WriteBuffer(FID, 1);
end;

procedure TAvk11DataContainer.AddHandScan(Dat: THandScanData);
var
  I: Integer;
  Tmp: THSItem;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 9');
    Exit;
  end; }

  FOffset[1]:= FData.Position;

  FID:= EID_HandScan;
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(Dat.HSHead, SizeOf(THSHead));

  for I:= 0 to High(Dat.HSItem) do
    FData.WriteBuffer(Dat.HSItem[I], 3 + Avk11EchoLen[Dat.HSItem[I].Count]);

  Tmp.Sample:= $FFFF;
  Tmp.Count:= 0;
  FData.WriteBuffer(Tmp, 3);

  AddEvent(EID_HandScan);
end;

function TAvk11DataContainer.DataTransfer(StartDisCoord: Integer): Boolean;
var
  R: RRail;
  Ch: Integer;
  I, Cnt, FullCount: Integer;
  Dat: TDestDat;

begin
  for I:= 1 to FHSSrcData.FCurEcho[RRail(FDataRail), FDataCh].Count do
  begin
    Dat:= FHSSrcData.FCurEcho[RRail(FDataRail), FDataCh];

    FullCount:= Dat.Count;
    if FullCount = 0 then Exit;
    if FullCount > 4 then Cnt:= 4
                     else Cnt:= FullCount;

    SetLength(FHSItem, Length(FHSItem) + 1);
    FHSItem[High(FHSItem)].Sample:= FHSSrcData.FCurDisCoord - StartDisCoord;
    FHSItem[High(FHSItem)].Count:= Cnt;
    case Cnt of
      1: begin
           FHSItem[High(FHSItem)].Data[0]:= Dat.Delay[1];
           FHSItem[High(FHSItem)].Data[1]:= Dat.Ampl[1] and $0F shl 4;
         end;
      2: begin
           FHSItem[High(FHSItem)].Data[0]:= Dat.Delay[1];
           FHSItem[High(FHSItem)].Data[1]:= Dat.Delay[2];
           FHSItem[High(FHSItem)].Data[2]:= Dat.Ampl[1] and $0F shl 4 + Dat.Ampl[2] and $0F;
         end;
      3: begin
           FHSItem[High(FHSItem)].Data[0]:= Dat.Delay[1];
           FHSItem[High(FHSItem)].Data[1]:= Dat.Delay[2];
           FHSItem[High(FHSItem)].Data[2]:= Dat.Delay[3];
           FHSItem[High(FHSItem)].Data[3]:= Dat.Ampl[1] and $0F shl 4 + Dat.Ampl[2] and $0F;
           FHSItem[High(FHSItem)].Data[4]:= Dat.Ampl[3] and $0F shl 4;
         end;
      4: begin
           FHSItem[High(FHSItem)].Data[0]:= Dat.Delay[1];
           FHSItem[High(FHSItem)].Data[1]:= Dat.Delay[2];
           FHSItem[High(FHSItem)].Data[2]:= Dat.Delay[3];
           FHSItem[High(FHSItem)].Data[3]:= Dat.Delay[4];
           FHSItem[High(FHSItem)].Data[4]:= Dat.Ampl[1] and $0F shl 4 + Dat.Ampl[2] and $0F;
           FHSItem[High(FHSItem)].Data[5]:= Dat.Ampl[3] and $0F shl 4 + Dat.Ampl[4] and $0F;
         end;
    end;

    if FullCount > 4 then
    begin
      SetLength(FHSItem, Length(FHSItem) + 1);
      FHSItem[High(FHSItem)].Sample:= FHSSrcData.FCurDisCoord - StartDisCoord;
      FHSItem[High(FHSItem)].Count:= Cnt;
      case Cnt of
        1: begin
             FHSItem[High(FHSItem)].Data[0]:= Dat.Delay[5];
             FHSItem[High(FHSItem)].Data[1]:= Dat.Ampl[5] and $0F shl 4;
           end;
        2: begin
             FHSItem[High(FHSItem)].Data[0]:= Dat.Delay[5];
             FHSItem[High(FHSItem)].Data[1]:= Dat.Delay[6];
             FHSItem[High(FHSItem)].Data[2]:= Dat.Ampl[5] and $0F shl 4 + Dat.Ampl[6] and $0F;
           end;
        3: begin
             FHSItem[High(FHSItem)].Data[0]:= Dat.Delay[5];
             FHSItem[High(FHSItem)].Data[1]:= Dat.Delay[6];
             FHSItem[High(FHSItem)].Data[2]:= Dat.Delay[7];
             FHSItem[High(FHSItem)].Data[3]:= Dat.Ampl[5] and $0F shl 4 + Dat.Ampl[6] and $0F;
             FHSItem[High(FHSItem)].Data[4]:= Dat.Ampl[7] and $0F shl 4;
           end;
        4: begin
             FHSItem[High(FHSItem)].Data[0]:= Dat.Delay[5];
             FHSItem[High(FHSItem)].Data[1]:= Dat.Delay[6];
             FHSItem[High(FHSItem)].Data[2]:= Dat.Delay[7];
             FHSItem[High(FHSItem)].Data[3]:= Dat.Delay[8];
             FHSItem[High(FHSItem)].Data[4]:= Dat.Ampl[5] and $0F shl 4 + Dat.Ampl[6] and $0F;
             FHSItem[High(FHSItem)].Data[5]:= Dat.Ampl[7] and $0F shl 4 + Dat.Ampl[8] and $0F;
           end;
      end;
    end;
  end;
  Result:= True;
end;

procedure TAvk11DataContainer.AddHandScanFromDataContainer(SrcData: TAvk11DataContainer; HSHead: THSHead; DataRail, DataCh: Byte; StartCrd, EndCrd: Integer);
var
  I: Integer;
  Tmp: THSItem;

begin
  if EndCrd - StartCrd + 1 > $FFFE then raise EDataContExcept.Create('Hand index to Big');

  FDataRail:= DataRail;
  FDataCh:= DataCh;
  FHSSrcData:= SrcData;
  FHSSrcData.LoadData(StartCrd, EndCrd, 0, Self.DataTransfer);

  FOffset[1]:= FData.Position;

  FID:= EID_HandScan;
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(HSHead, SizeOf(THSHead));

  for I:= 0 to High(FHSItem) do
    FData.WriteBuffer(FHSItem[I], 3 + Avk11EchoLen[FHSItem[I].Count]);

  Tmp.Sample:= $FFFF;
  Tmp.Count:= 0;
  FData.WriteBuffer(Tmp, 3);

  AddEvent(EID_HandScan);

end;

{
procedure TAvk11DataContainer.StartHandScanReg(Rail, Ch: Byte);
begin
  FHandScanCount:= 0;
  FHandScanReg:= True;
  FHandScanRail:= Rail;
  FHandScanCh:= Ch;

  FSaveCurSys:= FCurSysCrd;
  FSaveCurDis:= FCurDisCrd;

  FCurSysCrd:= 0;
  FCurDisCrd:= 0;
  AddSysCoord(0);
end;

procedure TAvk11DataContainer.EndHandScanReg(HSHead: THSHead);
var
  I: Integer;
  Tmp: THSItem;

begin
  FCurSysCrd:= FSaveCurSys;
  FCurDisCrd:= FSaveCurDis;
  FHandScanReg:= False;

  FOffset[1]:= FData.Position;

  FID:= EID_HandScan;
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(HSHead, SizeOf(THSHead));

  for I:= 0 to FHandScanCount - 1 do
    FData.WriteBuffer(FHandScanDat[I], 3 + Avk11EchoLen[FHandScanDat[I].Count]);

  Tmp.Sample:= $FFFF;
  Tmp.Count:= 0;
  FData.WriteBuffer(Tmp, 3);

  AddEvent(EID_HandScan);
end;
 }
procedure TAvk11DataContainer.AddTextLabel(Text: string);
var
  Res: packed array [0..19] of Byte;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 10');
    Exit;
  end;
 }
  FOffset[1]:= FData.Position;

  FID:= EID_TextLabel;
  FData.WriteBuffer(FID, 1);
  TextToCode(Text, 20, Header.CharSet, Res);
  FData.WriteBuffer(Res, 20);
  FID:= 0;

  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);
//  FData.WriteBuffer(FID, 1);
//  FData.WriteBuffer(FID, 1);

  AddEvent(EID_TextLabel);
end;

procedure TAvk11DataContainer.AddSysCoord(SysCoord: Integer);
var
  R: RRail;
  I: Integer;
  BackLink: Integer;
  Last: Integer;
  P: Pointer;
  F: file;
  EchoBlock: TEchoBlock;

begin
{  if FHandScanReg then
  begin
    if FHandScanCount < $FFFF then
    begin
      Inc(FHandScanCount);
//      SetLength(FHandScanDat, Length(FHandScanDat) + 1);
      FHandScanDat[FHandScanCount - 1].Sample:= SysCoord;
      FHandScanDat[FHandScanCount - 1].Count:= 0;
    end
    else ShowMessage('Mode Error 50');

    FCurSysCrd:= SysCoord;
    Last:= FCurDisCrd;
    Inc(FCurDisCrd, Abs(SysCoord - FLastSysCrd)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)

    Exit;
  end;
  }
  FCurSysCrd:= SysCoord;
  Last:= FCurDisCrd;
  Inc(FCurDisCrd, Abs(SysCoord - FLastSysCrd)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)

//  if Abs(SysCoord - FLastSysCrd) > 1 then
//  begin
//    AddEcho(0, 1, 0, EchoBlock);
//    AddEcho(1, 1, 0, EchoBlock);
//  end;

  if not FCh1Echo[0] then AddEcho(0, 1, 0, EchoBlock);
  if not FCh1Echo[1] then AddEcho(1, 1, 0, EchoBlock);

  FCh1Echo[0]:= False;
  FCh1Echo[1]:= False;

  FMaxSysCoord:= Max(FMaxSysCoord, SysCoord);
  FMaxDisCoord:= FCurDisCrd;

  if (not FBackMotionFlag) and (FCurSysCrd < FLastSysCrd) then // ���� ���������� ����������� �������� (������ �� �����)
  begin
    FOffset[1]:= FData.Position;
    AddEvent(EID_BwdDir, FLastSysCrd, Last);
    FBackMotionFlag:= True;
    if not FSearchBMEnd then
    begin
      FStartBMSysCoord:= FLastSysCrd;
      FSearchBMEnd:= True;
    end;
  end
  else
  if FBackMotionFlag and (FCurSysCrd > FLastSysCrd) then // ���� ���������� ����������� �������� (����� �� ������)
  begin
    FOffset[1]:= FData.Position;
    AddEvent(EID_FwdDir, FLastSysCrd, Last);
    FBackMotionFlag:= False;
  end;
  if FSearchBMEnd and (FCurSysCrd > FStartBMSysCoord) then
  begin
    FSearchBMEnd:= False;
    FOffset[1]:= FData.Position;
    if FCurSysCrd - FLastSysCrd <> 0
      then AddEvent(EID_EndBM, FStartBMSysCoord, Round(FLastDisCrd + (FStartBMSysCoord - FLastSysCrd) * (FCurDisCrd - FLastDisCrd) / (FCurSysCrd - FLastSysCrd)))
      else AddEvent(EID_EndBM);
  end;

  if FData.Position - FOffset[1] >= 16384 then
  begin
    FOffset[1]:= FData.Position;
    AddEvent(EID_LinkPt);
  end;

//  FCurSysCrd:= SysCoord;
  for R:= r_Left to r_Right do
    if (FSavePack[R].Ampl <> $FF) and (not FCh1Flag[Ord(R)]) and (Abs(SysCoord - FPackSysCoord[Ord(R)]) > 50) then
    begin
      FSavePack[R].Ampl:= $FF;
      AddEcho(Ord(R), 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    end;

  if FFirstCoord then
  begin
    BackLink:= 0;
    FFirstCoord:= False;
  end else BackLink:= FData.Position - FLastSaveOffset;


  FLastSaveOffset:= FData.Position;

  if FLastSysCrd and $FFFFFF00 = SysCoord and $FFFFFF00 then
  begin  // �������� ����������
    if BackLink and $FFFFFF00 = 0 then // �������� ������
    begin
      FID:= EID_SysCrd_SS;
      FData.WriteBuffer(FID, 1);
      FData.WriteBuffer(BackLink, 1);
      FData.WriteBuffer(SysCoord, 1);
    end
    else
    begin
      FID:= EID_SysCrd_FS;
      FData.WriteBuffer(FID, 1);
      FData.WriteBuffer(BackLink, 4);
      FData.WriteBuffer(SysCoord, 1);
    end;
  end
  else
  begin  // ������ ����������
    if BackLink and $FFFFFF00 = 0 then // �������� ������
    begin
      FID:= EID_SysCrd_SF;
      FData.WriteBuffer(FID, 1);
      FData.WriteBuffer(BackLink, 1);
      FData.WriteBuffer(SysCoord, 4);
    end
    else
    begin
      FID:= EID_SysCrd_FF;
      FData.WriteBuffer(FID, 1);
      FData.WriteBuffer(BackLink, 4);
      FData.WriteBuffer(SysCoord, 4);
    end;
  end;

  FLastSysCrd:= SysCoord;
  FLastDisCrd:= FCurDisCrd;

  SaveData(True);
  FMaxReadOffset:= FData.Position;

  FEvents.VirtItem.OffSet:= FMaxReadOffset;
  FEvents.VirtItem.SysCoord:= FCurSysCrd;
  FEvents.VirtItem.DisCoord:= FCurDisCrd;

  FExHeader.EndMM:= (CurSysCoord - FLastStolbSysCrd) * Header.ScanStep div 100;
  FExHeader.EndSysCoord:= FCurSysCrd;
  FExHeader.EndDisCoord:= FCurDisCrd;
  FCoordList[FLastKm].Content[FLastPk].Len:= Round((DisToSysCoord(MaxDisCoord) - Event[FCoordList[FLastKm].Content[FLastPk].Idx].SysCoord) * Header.ScanStep / 100);
end;

procedure TAvk11DataContainer.AddHeadPh(R0_Ch0, R0_Ch1, R0_Ch2, R0_Ch3, R0_Ch4, R0_Ch5, R0_Ch6, R0_Ch7, R0_Ch8, R0_Ch9,
                                 R1_Ch0, R1_Ch1, R1_Ch2, R1_Ch3, R1_Ch4, R1_Ch5, R1_Ch6, R1_Ch7, R1_Ch8, R1_Ch9: Boolean);
var
  B: Byte;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 11');
    Exit;
  end;
 }
  FOffset[1]:= FData.Position;

  FID:= EID_HeadPh;
  FData.WriteBuffer(FID, 1);
  B:= 32 * Ord(R0_Ch0) + 16 * Ord(R0_Ch1) + 8 * Ord(R0_Ch2) + 4 * Ord(R0_Ch3) + 2 * Ord(R0_Ch4) + Ord(R0_Ch5);
  FData.WriteBuffer(B, 1);
  B:= 128 * Ord(R0_Ch6) + 64 * Ord(R0_Ch7) + 32 * Ord(R0_Ch8) + 16 * Ord(R0_Ch9);
  FData.WriteBuffer(B, 1);
  B:= 32 * Ord(R1_Ch0) + 16 * Ord(R1_Ch1) + 8 * Ord(R1_Ch2) + 4 * Ord(R1_Ch3) + 2 * Ord(R1_Ch4) + Ord(R1_Ch5);
  FData.WriteBuffer(B, 1);
  B:= 128 * Ord(R1_Ch6) + 64 * Ord(R1_Ch7) + 32 * Ord(R1_Ch8) + 16 * Ord(R1_Ch9);
  FData.WriteBuffer(B, 1);

  AddEvent(EID_HeadPh);
end;

procedure TAvk11DataContainer.AddMode(ModeIdx: Byte);
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 12');
    Exit;
  end;
 }
  FOffset[1]:= FData.Position;

  FID:= EID_Mode;
  FData.WriteBuffer(FID, 1);
  FID:= ModeIdx;
  FData.WriteBuffer(FID, 1);

  AddEvent(EID_Mode);
end;

procedure TAvk11DataContainer.AddZondImp(Rail, Ch, Val: Byte);
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 13');
    Exit;
  end;
 }
  if not (Ch in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]) then raise EDataContExcept.Create('Error Channel Number!');

  FOffset[1]:= FData.Position;

  FID:= EID_ZondImp; // ��������� ������ ������������ ��������
  FData.WriteBuffer(FID, 1);
  FID:= DataToID(Rail, Ch);
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(Val, 1);

  AddEvent(EID_ZondImp);
end;

procedure TAvk11DataContainer.AddRailType;
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 14');
    Exit;
  end;
}
  FOffset[1]:= FData.Position;

  FID:= EID_SetRailType;
  FData.WriteBuffer(FID, 1);

  AddEvent(EID_SetRailType);
end;

procedure TAvk11DataContainer.AddStolb(Km0, Km1: Smallint; Pk0, Pk1: Byte);
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 15');
    Exit;
  end;
}
  FOffset[1]:= FData.Position;

  FID:= EID_Stolb;
  FData.WriteBuffer(FID, 1);
  FillChar(FLastStolb, 0, SizeOf(mCoord));
  FLastStolb.Km[0]:= Km0;
  FLastStolb.Km[1]:= Km1;
  FLastStolb.Pk[0]:= Pk0;
  FLastStolb.Pk[1]:= Pk1;
  FLastStolb.Houer:= FCurHour;
  FLastStolb.Minute:= FCurMinute;
  FData.WriteBuffer(FLastStolb, SizeOf(mCoord));

  FLastStolbSysCrd:= FCurSysCrd;

  AddEvent(EID_Stolb);

  FExHeader.EndKM:= FLastStolb.Km[1];
  FExHeader.EndPK:= FLastStolb.Pk[1];
  FExHeader.EndMM:= (CurSysCoord - FLastStolbSysCrd) * Header.ScanStep div 100;

  Inc(FStolbCount);
end;

procedure TAvk11DataContainer.AddStrelka(Text: string);
var
  Res: packed array [0..5] of Byte;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 16');
    Exit;
  end;
}
  FOffset[1]:= FData.Position;

  FID:= EID_Strelka; // ����� ����������� �������� + �����
  FData.WriteBuffer(FID, 1);
  TextToCode(Text, 6, Header.CharSet, Res);
  FData.WriteBuffer(Res, 6);
  FData.WriteBuffer(Res, 6);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);

  AddEvent(EID_Strelka);
end;

procedure TAvk11DataContainer.AddDefLabel(Rail: Byte; Text: string);
var
  Res: packed array [0..4] of Byte;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 17');
    Exit;
  end;
}
  FOffset[1]:= FData.Position;

  FID:= EID_DefLabel; // ����� ������� + �����
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(Rail, 1);
  TextToCode(Text, 5, Header.CharSet, Res);
  FData.WriteBuffer(Res, 5);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);

  AddEvent(EID_DefLabel);
end;

procedure TAvk11DataContainer.AddStBoltStyk;
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 18');
    Exit;
  end;
}
  FOffset[1]:= FData.Position;

  FID:= EID_StBoltStyk;
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);

  AddEvent(EID_StBoltStyk);
end;

procedure TAvk11DataContainer.AddEdBoltStyk;
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 19');
    Exit;
  end;
}
  FOffset[1]:= FData.Position;

  FID:= EID_EndBoltStyk;
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);

  AddEvent(EID_EndBoltStyk);
end;

procedure TAvk11DataContainer.AddTime(Haur, Minute: Byte);
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 20');
    Exit;
  end;
}
  FOffset[1]:= FData.Position;

  FCurHour:= Haur;
  FCurMinute:= Minute;
  FID:= EID_Time; // ������� �������
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);

  AddEvent(EID_Time);
end;

procedure TAvk11DataContainer.AddCurTime;
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 21');
    Exit;
  end;
}
  FOffset[1]:= FData.Position;

  FID:= EID_Time; // ������� �������
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);

  AddEvent(EID_Time);
end;

procedure TAvk11DataContainer.AddStLineLabel;
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 22');
    Exit;
  end; }

  FOffset[1]:= FData.Position;

  FID:= EID_StLineLabel; // ������ ����������� ����� + �����
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);

  AddEvent(EID_StLineLabel);
end;

procedure TAvk11DataContainer.AddEdLineLabel;
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 23');
    Exit;
  end; }

  FOffset[1]:= FData.Position;

  FID:= EID_EndLineLabel; // ����� ����������� ����� + �����
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);

  AddEvent(EID_EndLineLabel);
end;

procedure TAvk11DataContainer.AddStSwarStyk;
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 24');
    Exit;
  end; }

  FOffset[1]:= FData.Position;

  FID:= EID_StSwarStyk; // ������� ������ ������� ���� + �����
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);

  AddEvent(EID_StSwarStyk);
end;

procedure TAvk11DataContainer.AddEdSwarStyk;
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 25');
    Exit;
  end; }

  FOffset[1]:= FData.Position;

  FID:= EID_EndSwarStyk; // ���������� ������ ������� ���� + �����
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);

  AddEvent(EID_EndSwarStyk);
end;

procedure TAvk11DataContainer.AddOpChange(NewOpName: string);
var
  Res: packed array [0..19] of Byte;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 26');
    Exit;
  end; }

  FOffset[1]:= FData.Position;

  FID:= EID_OpChange; // ����� ���������
  FData.WriteBuffer(FID, 1);
  TextToCode(NewOpName, 20, Header.CharSet, Res);
  FData.WriteBuffer(Res, 20);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);

  AddEvent(EID_OpChange);
end;

procedure TAvk11DataContainer.AddPathChange(NewPath: Byte);
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 27');
    Exit;
  end; }

  FOffset[1]:= FData.Position;

  FID:= EID_PathChange; // ����� ������ ����
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(NewPath, 1);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);

  AddEvent(EID_PathChange);
end;

procedure TAvk11DataContainer.AddMovDirChange(NewDir: Byte);
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 28');
    Exit;
  end; }

  FOffset[1]:= FData.Position;

  FID:= EID_MovDirChange; // ����� ����������� ��������
  FData.WriteBuffer(FID, 1);
  FData.WriteBuffer(NewDir, 1);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);

  AddEvent(EID_MovDirChange);
end;

procedure TAvk11DataContainer.AddSezdNo(Text: string);
var
  Res: packed array [0..9] of Byte;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 29');
    Exit;
  end; }

  FOffset[1]:= FData.Position;

  FID:= EID_SezdNo; // ����� ������
  FData.WriteBuffer(FID, 1);
  TextToCode(Text, 10, Header.CharSet, Res);
  FData.WriteBuffer(Res, 10);
  FData.WriteBuffer(Res, 10);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);

  AddEvent(EID_SezdNo);
end;

procedure TAvk11DataContainer.AddLongLabel(Id: Byte; Start: Boolean); // ����������� ����� 2
var
  Tmp: Byte;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 30');
    Exit;
  end; }
  
  FOffset[1]:= FData.Position;

  FID:= EID_LongLabel;
  FData.WriteBuffer(FID, 1);
  if Start then Tmp:= ID and $7F
           else Tmp:= ID and $7F or $80;
  FData.WriteBuffer(Tmp, 1);
  FData.WriteBuffer(FCurHour, 1);
  FData.WriteBuffer(FCurMinute, 1);

  AddEvent(EID_LongLabel);
end;

procedure TAvk11DataContainer.AddBSAmpl(Len: Word; var R0Data, R1Data: array of Byte); // ������ ��������� ��
var
  Head: mBSAmplHead;

begin
  FOffset[1]:= FData.Position;

  FID:= EID_BSAmpl;
  FData.WriteBuffer(FID, 1);
  Head.EndSysCrd:= FCurSysCrd;
  Head.Len:= Len;
  FData.WriteBuffer(Head.EndSysCrd, SizeOf(mBSAmplHead));
  FData.WriteBuffer(R0Data[0], Len);
  FData.WriteBuffer(R1Data[0], Len);
  AddEvent(EID_BSAmpl);
end;

procedure TAvk11DataContainer.SetTime(Time: TTime);
var
  SystemTime: TSystemTime;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 31');
    Exit;
  end; }

  DateTimeToSystemTime(Time, SystemTime);
  FCurHour:= XXNumToCode(SystemTime.wHour);
  FCurMinute:= XXNumToCode(SystemTime.wMinute);
end;

procedure TAvk11DataContainer.AddEvent(ID: Byte; SysCoord: Integer = - MaxInt; DisCoord: Integer = - MaxInt);
var
//  I, J: Integer;
  HSItem: THSItem;
  Echo: array [1..6] of Byte;
  pData: PEventData;
  R: RRail;
  Flag: Boolean;
//  LastKm: Integer;
//  LastPk: Integer;
  H, M: Integer;
  NewIdx: Integer;
  New: TContEvent;

begin
  New.ID:= ID;
  New.OffSet:= FOffset[1];
  if SysCoord = - MaxInt then New.SysCoord:= FCurSysCrd
                         else New.SysCoord:= SysCoord;
  if DisCoord = - MaxInt then New.DisCoord:= FCurDisCrd
                         else New.DisCoord:= DisCoord;
  New.Pack:= FSavePack;

//  FOffset:= SaveOffset;
//               AddEvent(LoadID);
//                        AddHandScan ?
//                        AddAK       ?
// ���������� ������� FEventsData
//  SetLength(FEventsData, Length(FEvents) + 1);
//  I:= High(FEvents);

  FMaxSysCoord:= Max(FMaxSysCoord, New.SysCoord);
  FMaxDisCoord:= New.DisCoord;

//  FEventsData[I].Event:= FEvents[I];
  if (New.ID in  [EID_HandScan    ,  EID_Sens       ,  EID_Att        , EID_VRU         ,
                  EID_StStr       ,  EID_EndStr     ,  EID_HeadPh     , EID_Mode        ,
                  EID_2Tp         ,  EID_ZondImp    ,  EID_SetRailType, EID_Stolb       ,
                  EID_Strelka     ,  EID_DefLabel   ,  EID_TextLabel  , EID_StBoltStyk  ,
                  EID_EndBoltStyk ,  EID_Time       ,  EID_StLineLabel, EID_EndLineLabel,
                  EID_StSwarStyk  ,  EID_EndSwarStyk,  EID_OpChange   , EID_PathChange  ,
                  EID_MovDirChange,  EID_SezdNo     ,  EID_LongLabel  , EID_GPSCoord   , EID_GPSState    ,
                  EID_GPSError    ,  EID_EndFile    ,  EID_2Tp_Word   {, EID_BSAmpl}])


                         {and (FEvents[I].OffSet + Avk11EventLen[FEvents[I].ID] + 1 < FData.Size)} then

  begin
    FData.Position1:= New.OffSet + 1;
    if Avk11EventLen[New.ID] <> - 1 then
      FData.ReadBuffer1(New.Data[1], Avk11EventLen[New.ID]);
  end;

//  if FEvents[I].ID = EID_EndBM then FCanSkipBM:= True;
//  if FEvents[I].ID in [EID_FwdDir, EID_BwdDir] then Flag:= True;

  with New do
    case ID of
     EID_Sens: FCurParam.Sens   [SideToRail(ByteToSide(ID, Data[1])), ByteToCh(ID, Data[1])]:= ShortInt(Data[2]);
      EID_Att: FCurParam.Att    [SideToRail(ByteToSide(ID, Data[1])), ByteToCh(ID, Data[1])]:= ShortInt(Data[2]);
      EID_VRU: FCurParam.Vru    [SideToRail(ByteToSide(ID, Data[1])), ByteToCh(ID, Data[1])]:= Data[2];
    EID_StStr: FCurParam.StStr  [SideToRail(ByteToSide(ID, Data[1])), ByteToCh(ID, Data[1])]:= Data[2] - 1;
   EID_EndStr: FCurParam.EdStr  [SideToRail(ByteToSide(ID, Data[1])), ByteToCh(ID, Data[1])]:= Data[2] - 1;
      EID_2Tp: FCurParam.DwaTp  [SideToRail(ByteToSide(ID, Data[1])), ByteToCh(ID, Data[1])]:= Data[2];
 EID_2Tp_Word: FCurParam.DwaTp  [SideToRail(ByteToSide(ID, Data[1])), ByteToCh(ID, Data[1])]:= Data[2] + $100 * Data[3];
  EID_ZondImp: FCurParam.ZondImp[SideToRail(ByteToSide(ID, Data[1])), ByteToCh(ID, Data[1])]:= Data[2];
     EID_Mode: FCurParam.Mode:= Data[1];
   EID_HeadPh: begin
                 FCurParam.HeadPh[SideToRail(sLeft), 0]:= (Data[1] and  32) <> 0; // ����� 0 - 1 byte / 5 bit
                 FCurParam.HeadPh[SideToRail(sLeft), 1]:= (Data[1] and  16) <> 0; // ����� 1 - 1 byte / 4 bit
                 FCurParam.HeadPh[SideToRail(sLeft), 2]:= (Data[1] and   8) <> 0; // ����� 2 - 1 byte / 3 bit
                 FCurParam.HeadPh[SideToRail(sLeft), 3]:= (Data[1] and   4) <> 0; // ����� 3 - 1 byte / 2 bit
                 FCurParam.HeadPh[SideToRail(sLeft), 4]:= (Data[1] and   2) <> 0; // ����� 4 - 1 byte / 1 bit
                 FCurParam.HeadPh[SideToRail(sLeft), 5]:= (Data[1] and   1) <> 0; // ����� 5 - 1 byte / 0 bit
                 FCurParam.HeadPh[SideToRail(sLeft), 6]:= (Data[2] and 128) <> 0; // ����� 6 - 2 byte / 7 bit
                 FCurParam.HeadPh[SideToRail(sLeft), 7]:= (Data[2] and  64) <> 0; // ����� 7 - 2 byte / 6 bit
                 FCurParam.HeadPh[SideToRail(sLeft), 8]:= (Data[2] and  32) <> 0; // ����� 8 - 2 byte / 5 bit
                 FCurParam.HeadPh[SideToRail(sLeft), 9]:= (Data[2] and  16) <> 0; // ����� 9 - 2 byte / 4 bit

                 FCurParam.HeadPh[SideToRail(sRight), 0]:= (Data[3] and  32) <> 0; // ����� 0 - 1 byte / 5 bit
                 FCurParam.HeadPh[SideToRail(sRight), 1]:= (Data[3] and  16) <> 0; // ����� 1 - 1 byte / 4 bit
                 FCurParam.HeadPh[SideToRail(sRight), 2]:= (Data[3] and   8) <> 0; // ����� 2 - 1 byte / 3 bit
                 FCurParam.HeadPh[SideToRail(sRight), 3]:= (Data[3] and   4) <> 0; // ����� 3 - 1 byte / 2 bit
                 FCurParam.HeadPh[SideToRail(sRight), 4]:= (Data[3] and   2) <> 0; // ����� 4 - 1 byte / 1 bit
                 FCurParam.HeadPh[SideToRail(sRight), 5]:= (Data[3] and   1) <> 0; // ����� 5 - 1 byte / 0 bit
                 FCurParam.HeadPh[SideToRail(sRight), 6]:= (Data[4] and 128) <> 0; // ����� 6 - 2 byte / 7 bit
                 FCurParam.HeadPh[SideToRail(sRight), 7]:= (Data[4] and  64) <> 0; // ����� 7 - 2 byte / 6 bit
                 FCurParam.HeadPh[SideToRail(sRight), 8]:= (Data[4] and  32) <> 0; // ����� 8 - 2 byte / 5 bit
                 FCurParam.HeadPh[SideToRail(sRight), 9]:= (Data[4] and  16) <> 0; // ����� 9 - 2 byte / 4 bit
               end;
    end;
  New.Params:= FCurParam;

  NewIdx:= FEvents.Add(New);
  FEventsCount:= FEvents.Count;

  if (New.ID = EID_FwdDir) or
     (New.ID = EID_BwdDir) then
  begin
    SetLength(FCDList, Length(FCDList) + 1);
    FCDList[High(FCDList)]:= NewIdx;
  end;

                                   // ���������� ������� �������

  if ID in [    EID_Stolb, EID_StBoltStyk,  EID_EndLineLabel,
              EID_Strelka, EID_EndBoltStyk, EID_StSwarStyk,
             EID_DefLabel, EID_Time,        EID_EndSwarStyk,
            EID_TextLabel, EID_StLineLabel ] then
  begin
    pData:= Addr(New.Data);

    H:= pData^[Avk11EventLen[ID] - 1];
    M:= pData^[Avk11EventLen[ID] - 0];

    H:= (H shr 4 and $0F) * 10 + (H and $0F);
    M:= (M shr 4 and $0F) * 10 + (M and $0F);

    if (H <> FOldH) or (M <> FOldM) then
    begin
      SetLength(FTimeList, Length(FTimeList) + 1);
      FTimeList[High(FTimeList)].H:= H;
      FTimeList[High(FTimeList)].M:= M;
      FTimeList[High(FTimeList)].DC:= New.DisCoord;
      FOldH:= H;
      FOldM:= M;
    end;
  end;

// ----------------------------------------------------------------------------------

  if ID = EID_Stolb then
  begin
    pData:= Addr(New.Data);
    if pCoord(pData)^.Km[0] <> pCoord(pData)^.Km[1] then
    begin
      SetLength(FCoordList, Length(FCoordList) + 1);
      FCoordList[High(FCoordList)].KM:= pCoord(pData)^.Km[1];
      with FCoordList[High(FCoordList)] do
      begin
        SetLength(Content, Length(Content) + 1);
        if Header.MoveDir = 0 then Content[High(Content)].Pk:= 10
                              else Content[High(Content)].Pk:= 1;
        Content[High(Content)].Idx:= NewIdx;
        FCoordList[FLastKm].Content[FLastPk].Len:= Round((New.SysCoord - Event[FCoordList[FLastKm].Content[FLastPk].Idx].SysCoord) * Header.ScanStep / 100);
        FLastKm:= High(FCoordList);
        FLastPk:= High(Content);
      end;
    end;
    if pCoord(pData)^.Km[0] = pCoord(pData)^.Km[1] then
      with FCoordList[High(FCoordList)] do
      begin
        SetLength(Content, Length(Content) + 1);
        Content[High(Content)].Pk:= pCoord(pData)^.Pk[1];
        Content[High(Content)].Idx:= NewIdx;
        FCoordList[FLastKm].Content[FLastPk].Len:= Round((New.SysCoord - Event[FCoordList[FLastKm].Content[FLastPk].Idx].SysCoord) * Header.ScanStep / 100);
        FLastKm:= High(FCoordList);
        FLastPk:= High(Content);
      end;
  end;
  FCoordList[FLastKm].Content[FLastPk].Len:= Round((DisToSysCoord(MaxDisCoord) - Event[FCoordList[FLastKm].Content[FLastPk].Idx].SysCoord) * Header.ScanStep / 100);
end;

procedure TAvk11DataContainer.SaveData(TestLen: Boolean); // ������ ������� �����
var
  F: file;
  P: Pointer;

begin
  if (not TestLen) or (TestLen and (FData.Size - FLastSave > 1024)) then
  begin
    P:= Pointer(Integer(FData.Memory) + FLastSave);
    AssignFile(F, FFileName);
    if FileExists(FFileName) then Reset(F, 1)
                             else ReWrite(F, 1);
    Seek(F, FileSize(F));
    BlockWrite(F, P^, FData.Size - FLastSave);
    System.CloseFile(F);
    FLastSave:= FData.Size;
  end;
end;

// ---- ������ � �������� ------------------------------------------------------

procedure TAvk11DataContainer.AddValue(Group: Byte; ValueTyp: TValueType; ValueId: Byte; Value: Variant);
const
  Id: Byte = 0;

var
  I: Integer;
  S: string;
  DT: Double;
  E: Extended;
  Shift: Word;

begin
  FParDat.Position:= FParDat.Size;
  case ValueTyp of
         vtInt: begin
                  I:= Value;
                  Shift:= 6 + SizeOf(Integer);
                  FParDat.WriteBuffer(Shift, 2);
                  FParDat.WriteBuffer(Group, 1);
                  FParDat.WriteBuffer(Id, 1);
                  FParDat.WriteBuffer(ValueId, 1);
                  FParDat.WriteBuffer(ValueTyp, 1);
                  FParDat.WriteBuffer(I, SizeOf(Integer));
                end;
        vtText: begin
                  S:= Value;
                  Shift:= 7 + Length(S);
                  FParDat.WriteBuffer(Shift, 2);
                  FParDat.WriteBuffer(Group, 1);
                  FParDat.WriteBuffer(Id, 1);
                  FParDat.WriteBuffer(ValueId, 1);
                  FParDat.WriteBuffer(ValueTyp, 1);
                  I:= Length(S);
                  FParDat.WriteBuffer(I, 1);
                  FParDat.WriteBuffer(S[1], Length(S));
                end;
    vtDateTime,
        vtTime,
        vtDate: begin
                  DT:= Value;
                  Shift:= 6 + SizeOf(Double);
                  FParDat.WriteBuffer(Shift, 2);
                  FParDat.WriteBuffer(Group, 1);
                  FParDat.WriteBuffer(Id, 1);
                  FParDat.WriteBuffer(ValueId, 1);
                  FParDat.WriteBuffer(ValueTyp, 1);
                  FParDat.WriteBuffer(DT, SizeOf(Double));
                end;
       vtFloat: begin
                  E:= Value;
                  Shift:= 6 + SizeOf(Extended);
                  FParDat.WriteBuffer(Shift, 2);
                  FParDat.WriteBuffer(Group, 1);
                  FParDat.WriteBuffer(Id, 1);
                  FParDat.WriteBuffer(ValueId, 1);
                  FParDat.WriteBuffer(ValueTyp, 1);
                  FParDat.WriteBuffer(E, SizeOf(Extended));
                end;
  end;
end;

procedure TAvk11DataContainer.AddValue(Group: Byte; ValueTyp: TValueType; ValueId: string; Value: Variant);
var
  I: Integer;
  S: string;
  DT: Double;
  E: Extended;
  Shift: Word;

begin
  FParDat.Position:= FParDat.Size;
  case ValueTyp of
         vtInt: begin
                  Shift:= 5 + Length(ValueId) + SizeOf(Integer);
                  FParDat.WriteBuffer(Shift, 2);
                  FParDat.WriteBuffer(Group, 1);
                  I:= Length(ValueId);
                  FParDat.WriteBuffer(I, 1);
                  FParDat.WriteBuffer(ValueId[1], Length(ValueId));
                  FParDat.WriteBuffer(ValueTyp, 1);
                  I:= Value;
                  FParDat.WriteBuffer(I, SizeOf(Integer));
                end;
        vtText: begin
                  S:= Value;
                  Shift:= 6 + Length(ValueId) + Length(S);
                  FParDat.WriteBuffer(Shift, 2);
                  FParDat.WriteBuffer(Group, 1);
                  I:= Length(ValueId);
                  FParDat.WriteBuffer(I, 1);
                  FParDat.WriteBuffer(ValueId[1], Length(ValueId));
                  FParDat.WriteBuffer(ValueTyp, 1);
                  I:= Length(S);
                  FParDat.WriteBuffer(I, 1);
                  FParDat.WriteBuffer(S[1], Length(S));
                end;
    vtDateTime,
        vtTime,
        vtDate: begin
                  DT:= Value;
                  Shift:= 5 + Length(ValueId) + SizeOf(Double);
                  FParDat.WriteBuffer(Shift, 2);
                  FParDat.WriteBuffer(Group, 1);
                  I:= Length(ValueId);
                  FParDat.WriteBuffer(I, 1);
                  FParDat.WriteBuffer(ValueId[1], Length(ValueId));
                  FParDat.WriteBuffer(ValueTyp, 1);
                  FParDat.WriteBuffer(DT, SizeOf(Double));
                end;
       vtFloat: begin
                  E:= Value;
                  Shift:= 5 + Length(ValueId) + SizeOf(Extended);
                  FParDat.WriteBuffer(Shift, 2);
                  FParDat.WriteBuffer(Group, 1);
                  I:= Length(ValueId);
                  FParDat.WriteBuffer(I, 1);
                  FParDat.WriteBuffer(ValueId[1], Length(ValueId));
                  FParDat.WriteBuffer(ValueTyp, 1);
                  FParDat.WriteBuffer(E, SizeOf(Extended));
                end;
  end;
end;

function TAvk11DataContainer.ValueCount(Group: Byte): Integer;
var
  Shift: Word;
  Grp: Byte;

begin
  Result:= 0;
  if FParDat.Size = 0 then Exit;
  FParDat.Position:= 0;
  repeat
    FParDat.ReadBuffer(Shift, 2);
    FParDat.ReadBuffer(Grp, 1);
    FParDat.Position:= FParDat.Position + Shift - 3;
    if Grp = Group then Inc(Result);
  until FParDat.Position >= FParDat.Size;
end;

procedure TAvk11DataContainer.GetValue(Group: Byte; Index: Integer; var ValueId, Value: Variant);
var
  Shift: Word;
  Grp: Byte;
  IdId: Byte;
  IdByte: Byte;
  IdStr: string;
  Count: Integer;
  ValueTyp: TValueType;

  I: Integer;
  S: string;
  DT: Double;
  DT_: TDateTime;
  E: Extended;

begin
  Count:= - 1;
  if FParDat.Size = 0 then Exit;
  FParDat.Position:= 0;
  repeat
    FParDat.ReadBuffer(Shift, 2);
    FParDat.ReadBuffer(Grp, 1);

    if Grp = Group then Inc(Count);
    if Index = Count then
    begin
      FParDat.ReadBuffer(IdId, 1);
      if IdId = 0 then begin
                         FParDat.ReadBuffer(IdByte, 1);
                         ValueId:= IdByte;
                       end
                       else
                       begin
                         SetLength(IdStr, IdId);
                         FParDat.ReadBuffer(IdStr[1], IdId);
                         ValueId:= IdStr;
                       end;

      FParDat.ReadBuffer(ValueTyp, 1);
      case ValueTyp of
             vtInt: begin
                      FParDat.ReadBuffer(I, SizeOf(Integer));
                      Value:= I;
                    end;
            vtText: begin
                      FParDat.ReadBuffer(IdByte, 1);
                      SetLength(S, IdByte);
                      FParDat.ReadBuffer(S[1], IdByte);
                      Value:= S;
                    end;
        vtDateTime,
            vtTime,
            vtDate: begin
                      FParDat.ReadBuffer(DT, SizeOf(Double));
                      DT_:= DT;
                      Value:= DT_;
                    end;
           vtFloat: begin
                      FParDat.ReadBuffer(E, SizeOf(Extended));
                      Value:= E;
                    end;
      end;
      Exit;
    end;
    FParDat.Position:= FParDat.Position + Shift - 3;
  until FParDat.Position >= FParDat.Size;
end;

// ---- �������� ---------------------------------------------------------------

function TAvk11DataContainer.SideToRail(Side: TSide): RRail;
begin
  case Side of
    sLeft: Result:= r_Left;
   sRight: Result:= r_Right;
  end;
end;

procedure TAvk11DataContainer.LoadPackReset;
begin
  FLoadPack[r_Left].Ampl:= $FF;
  FLoadPack[r_Right].Ampl:= $FF;
end;

procedure TAvk11DataContainer.UnPackEcho;
var
  R: RRail;

begin
  for R:= r_Left to r_Right do
  begin
    if (FDestDat[R].Count > 1) or
       ((FDestDat[R].Count = 1) and (FDestDat[R].Delay[1] = FLoadPack[R].Delay) and (FDestDat[R].Ampl[1] = FLoadPack[R].Ampl)) or
       FDestDat[R].ZerroFlag then FLoadPack[R].Ampl:= $FF
                             else begin
                                    if FDestDat[R].Count = 1 then
                                    begin
                                      FLoadPack[R].Delay:= FDestDat[R].Delay[1];
                                      FLoadPack[R].Ampl:= FDestDat[R].Ampl[1];
                                    end;
                                  end;

    FDestDat[R].Count:= 0;
    FDestDat[R].ZerroFlag:= False;
  end;

end;

procedure TAvk11DataContainer.DataToEcho;
begin
  if FSrcDat[0] shr 3 and 7 = 1 then
  begin
    with FDestDat[SideToRail(TSide(FSrcDat[0] shr 6 and 1))] do
      case FSrcDat[0] and $07 of
        0: ZerroFlag:= True;
        1: begin
             if Count < 16 then Inc(Count);
             Delay[Count]:= FSrcDat[1];
             Ampl[Count]:= FSrcDat[2] shr 4 and $0F;
           end;
        2: begin
             if Count < 16 then Inc(Count);
             Delay[Count]:= FSrcDat[1];
             Ampl[Count]:= FSrcDat[3] shr 4 and $0F;
             if Count < 16 then Inc(Count);
             Delay[Count]:= FSrcDat[2];
             Ampl[Count]:= FSrcDat[3] and $0F;
           end;
        3: begin
             if Count < 16 then Inc(Count);
             Delay[Count]:= FSrcDat[1];
             Ampl[Count]:= FSrcDat[4] shr 4 and $0F;
             if Count < 16 then Inc(Count);
             Delay[Count]:= FSrcDat[2];
             Ampl[Count]:= FSrcDat[4] and $0F;
             if Count < 16 then Inc(Count);
             Delay[Count]:= FSrcDat[3];
             Ampl[Count]:= FSrcDat[5] shr 4 and $0F;
           end;
        4: begin
             if Count < 16 then Inc(Count);
             Delay[Count]:= FSrcDat[1];
             Ampl[Count]:= FSrcDat[5] shr 4 and $0F;
             if Count < 16 then Inc(Count);
             Delay[Count]:= FSrcDat[2];
             Ampl[Count]:= FSrcDat[5] and $0F;
             if Count < 16 then Inc(Count);
             Delay[Count]:= FSrcDat[3];
             Ampl[Count]:= FSrcDat[6] shr 4 and $0F;
             if Count < 16 then Inc(Count);
             Delay[Count]:= FSrcDat[4];
             Ampl[Count]:= FSrcDat[6] and $0F;
           end;
      end;
  end;
end;
{
function TAvk11DataContainer.FindBSState(StartDisCoord: Integer): Boolean;
begin
  if FCurDisCoord < FObmen1 then
  begin
    FObmen2:= FPack;
    Result:= True;
  end else Result:= False;
end;
}{
procedure TAvk11DataContainer.TestCanSkipBM;
begin
  if (not FCanSkipBM) or (FEventsData[0].Event.ID <> EID_FwdDir) then // ���� ���� �������� ������ ������ ������ - ��� ������� ����� ���� �������� ����� - ����� ���������������
  begin
    ReAnalyze;
    FillEventsData;
  end;
end;
}
(*
procedure TAvk11DataContainer.SetSkipBM(NewState: Boolean);
var
  I: Integer;
  J: Integer;
  K: Integer;
  L: Integer;
  Flag: Boolean;

  GetDisCoord: Int64;
  GetSysCoord: Integer;
  GetOffset: Integer;

  FBMItems_: array of TBMItem;
  ED_: array of TFileEventData;


begin
  if NewState and (not FSkipBM) then
  begin
    TestCanSkipBM;

{   for I:= 0 to High(FNotebook) do
      FNotebook[I].SysCoord:= DisToSysCoord(FNotebook[I].DisCoord);  }

    SetLength(FSaveEventsData, Length(FEventsData)); // ��������� �������
    Move(FEventsData[0], FSaveEventsData[0], Length(FEventsData) * SizeOf(TFileEventData));

    SetLength(ED_, Length(FEventsData));
    Move(FEventsData[0], ED_[0], Length(FEventsData) * SizeOf(TFileEventData));

    Flag:= False;
    SetLength(FBMItems, 0);
    SetLength(FBMItems_, 0);

    for I:= 0 to High(FEventsData) do
    begin
      if (not Flag) and (FEventsData[I].Event.ID = EID_BwdDir) then // ���� �������� �������� ����� - ���������� ��� ��� ��������
      begin
        Flag:= True;
        J:= FEventsData[I].Event.SysCoord;
        K:= FEventsData[I].Event.OffSet;
        L:= FEventsData[I].Event.DisCoord;
      end;

      if Flag and (FEventsData[I].Event.ID = EID_EndBM) then  // ���� �������� ����� ����������� - ��������� ������ � ���
      begin
        Flag:= False;
        SetLength(FBMItems_, Length(FBMItems_) + 1);
        FBMItems_[High(FBMItems_)].StSkipOff:= K;
        FBMItems_[High(FBMItems_)].OkOff:= FEventsData[I].Event.OffSet;
        FObmen1:= FEventsData[I].Event.DisCoord;
        LoadData(L, GetMaxDisCoord, 0, FindBSState);
        FBMItems_[High(FBMItems_)].Pack:= FObmen2;
      end;

      if Flag then
      begin                                             // ���� �� � ���� �������� ����� - ����� ��� ��������� �� ���������� ��� ������
        ED_[I].Event.SysCoord:= J;
        ED_[I].Event.DisCoord:= J;
      end
      else
      begin                                             // ���� �� �� � ���� �������� ����� - ����� ��� ������� ��������� �� �� ��������� ����������
        ED_[I].Event.DisCoord:= ED_[I].Event.SysCoord;
      end
    end;

    if Flag then
    begin
      SetLength(FBMItems_, Length(FBMItems_) + 1);
      FBMItems_[High(FBMItems_)].StSkipOff:= K;
      FBMItems_[High(FBMItems_)].OkOff:= Header.TableLink;
      FObmen1:= Header.TableLink;
      LoadData(L, GetMaxDisCoord, 0, FindBSState);
      FBMItems_[High(FBMItems_)].Pack:= FObmen2;
    end;

    SetLength(FBMItems, Length(FBMItems_));
    Move(FBMItems_[0], FBMItems[0], Length(FBMItems) * SizeOf(TBMItem));
    SetLength(FBMItems_, 0);

    Move(ED_[0], FEventsData[0], Length(FEventsData) * SizeOf(TFileEventData));
    SetLength(ED_, 0);

    FillTimeList;
  end;

  if (not NewState) and FSkipBM then
  begin
    Move(FSaveEventsData[0], FEventsData[0], Length(FSaveEventsData) * SizeOf(TFileEventData));
    SetLength(FSaveEventsData, 0);
    SetLength(FBMItems, 0);
    FillTimeList;
    for I:= 0 to High(FNotebook) do
      if FNotebook[I].DisCoord = - MaxInt then
        FNotebook[I].DisCoord:= SysToDisCoord(FNotebook[I].SysCoord);
  end;

  FSkipBM:= NewState;
end;
 *)
// -------------< ������ � ������� >------------------------------------

function TAvk11DataContainer.NormalizeDisCoord(Src: Integer): Integer;
begin
  if Src < 0 then Result:= 0 else
    if Src > FMaxDisCoord then Result:= FMaxDisCoord else Result:= Src;
end;

//------------------------< ������������ �������� ������ >---------------------

function TAvk11DataContainer.GetMaxDisCoord: Integer;
begin
{  if FHandScanReg then  // ����������� ������� ��������
  begin
    if FHandScanCount - 1 <> 0 then
      Result:= FHandScanDat[FHandScanCount - 1].Sample;
  end else} Result:= FMaxDisCoord;
end;

function TAvk11DataContainer.GetMaxSysCoord: Integer;
begin
{  if FHandScanReg then  // ����������� ������� ��������
  begin
    if FHandScanCount - 1 <> 0 then
      Result:= FHandScanDat[FHandScanCount - 1].Sample;
  end else} Result:= FMaxSysCoord;
end;

function TAvk11DataContainer.GetEventCount: Integer;
begin
  Result:= FEventsCount;
end;

function TAvk11DataContainer.GetEvent(Index: Integer): TContEvent;
begin
  if (Index >= 0) and (Index < FEventsCount) then Result:= FEvents[Index];
end;

function TAvk11DataContainer.GetFUnPackFlag(Index: RRail): Boolean;
begin
//  Result:= FUnPackFlag[Index];
end;

function TAvk11DataContainer.SysToDisCoord(Coord: Integer): Integer;
var
  I: Integer;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 32');
    Exit;
  end;
}
  Result:= 0;
  for I:= 0 to FEventsCount - 2 do
    if (Coord >= FEvents[I + 0].SysCoord) and (Coord <= FEvents[I + 1].SysCoord) then
    begin
      Result:= FEvents[I + 0].DisCoord + Abs(FEvents[I + 0].SysCoord - Coord);
      Exit;
    end;
end;

function TAvk11DataContainer.DisToRealCoord(Coord: LongInt): TRealCoord;
var
  CoordParams: TCoordParams;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 33');
    Exit;
  end;
}
  GetCoord(Coord, CoordParams);
{
  Result.Km:= CoordParams.LeftStolb[ssRight].Km;
  Result.Pk:= CoordParams.LeftStolb[ssRight].Pk;
  Result.MM:= CoordParams.ToLeftStolbMM;
}
  Result:= CoordParamsToRealCoord(CoordParams, Header.MoveDir);
end;

function TAvk11DataContainer.DisToSysCoord(Coord: Integer): Integer;
var
  LeftIdx, RightIdx: Integer;
  Del: Integer;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 34');
    Exit;
  end;
}
  Coord:= NormalizeDisCoord(Coord);
  LeftIdx:= GetLeftEventIdx(Coord);
  if FEvents[LeftIdx].DisCoord = Coord then Result:= FEvents[LeftIdx].SysCoord else
  begin
    RightIdx:= GetRightEventIdx(Coord);
    Del:= (FEvents[RightIdx].DisCoord - FEvents[LeftIdx].DisCoord);
    if Del <> 0 then Result:= Round(FEvents[LeftIdx].SysCoord + ((Coord - FEvents[LeftIdx].DisCoord) / Del) * (FEvents[RightIdx].SysCoord - FEvents[LeftIdx].SysCoord))
                else Result:= FEvents[LeftIdx].SysCoord;
  end;
end;

procedure TAvk11DataContainer.DisToDisCoords(Coord: Integer; var Res: TIntegerDynArray);
var
//  LeftIdx, RightIdx: Integer;
  I: Integer;
  Del: Integer;
  Res_: Integer;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 35');
    Exit;
  end;
}
  Coord:= DisToSysCoord(NormalizeDisCoord(Coord));
//  for LeftIdx:= 0 to EventCount - 2 do
  for I:= 0 to EventCount - 2 do
  begin

    if (FEvents[I].SysCoord <= Coord) and (FEvents[I + 1].SysCoord >= Coord) or
       (FEvents[I].SysCoord >= Coord) and (FEvents[I + 1].SysCoord <= Coord) then
    begin
      SetLength(Res, Length(Res) + 1);
      Res[High(Res)]:= FEvents[I + 0].DisCoord + Abs(Coord - FEvents[I + 0].SysCoord);
    end;
{
    if (FEvents[I].Event.SysCoord >= Coord) and (FEvents[I + 1].Event.SysCoord <= Coord) then
    begin
      SetLength(Res, Length(Res) + 1);
      Res[High(Res)]:= FEvents[I + 0].Event.DisCoord + Abs(Coord - FEvents[I + 1].Event.SysCoord);
    end;
}
  end;

end;
{
function TAvk11DataContainer.GetSysCoordByRealIdx(KmIdx, PkIdx: Integer; Metr: Extended): Integer;
begin
  case Header.MoveDir of
    0: begin
         if PkIdx < High(CoordList[KmIdx].Content)
           then Result:= Event[CoordList[KmIdx].Content[PkIdx + 1].Idx].Event.SysCoord
           else if KmIdx < High(CoordList) then Result:= Event[CoordList[KmIdx + 1].Content[0].Idx].Event.SysCoord
                                           else Result:= DisToSysCoord(MaxDisCoord) + 100 * (100000 - ExHeader.EndMM) div Header.ScanStep;
         Result:= Result - Round(Metr * 1000 / (Header.ScanStep / 100));
       end;
    1: Result:= Event[CoordList[KmIdx].Content[PkIdx].Idx].Event.SysCoord + Round((Metr - Ord((KmIdx = 0) and (PkIdx = 0)) * (FHeader.StartMetre + StartShift)) * 1000 / (Header.ScanStep / 100));
  end;
end;
}{
function TAvk11DataContainer.GetSysCoordByReal(Km, Pk: Integer; Metr: Extended): Integer;
var
  I: Integer;
  J: Integer;

begin
  if FHandScanReg then
  begin
    ShowMessage('Mode Error ');
    Exit;
  end;

  for I:= 0 to High(FCoordList) do
    if FCoordList[I].KM = Km then
      for J:= 0 to High(FCoordList[I].Content) do
        if FCoordList[I].Content[J].Pk = Pk then
        begin
          Result:= GetSysCoordByRealIdx(I, J, Metr);
          Exit;
        end;
end;
}
procedure TAvk11DataContainer.DisToFileOffset(NeedDisCoord: Integer; var DisCoord: Int64; var SysCoord: Integer; var Offset: Integer);
var
  R: RRail;
  LastSysCoord: Integer;
  SaveOffset: Integer;
  SkipBytes: Integer;
  CurSysCoord: Integer;
  CurDisCoord: Int64;
  LoadID: Byte;
  LoadByte: array [1..22] of Byte;
  LoadLongInt: array [1..5] of LongInt; // absolute LoadByte[1];
  HSHead: THSHead;
  HSItem: THSItem;
  StartIdx, EndIdx: Integer;
  DisCoord_: Integer;
  SysCoord_: Integer;
  Offset_: Integer;
  CurDisCoord_: Integer;
  CurSysCoord_: Integer;
  CurPosition_: Integer;
  SameCoord: TIntegerDynArray;
  BSHead: mBSAmplHead;
  Tmp: Boolean;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 36');
    Exit;
  end;
}
  NeedDisCoord:= NormalizeDisCoord(NeedDisCoord);
// GetEventIdx(NeedDisCoord, NeedDisCoord, StartIdx, EndIdx);
  StartIdx:= GetLeftEventIdx(NeedDisCoord);
//  GetNearestEventIdx(NeedDisCoord, StartIdx, EndIdx, SameCoord);
//  if Length(SameCoord) <> 0 then StartIdx:= SameCoord[0];

  if StartIdx = - 1 then
  begin
    DisCoord:= 0;
    SysCoord:= 0;
    Offset:= 0;
    Exit;
  end;

  FData.Position2:= FEvents[StartIdx].OffSet;
  LastSysCoord:= FEvents[StartIdx].SysCoord;
  CurSysCoord:= FEvents[StartIdx].SysCoord;
  CurDisCoord:= FEvents[StartIdx].DisCoord;
  FLoadPack:= FEvents[StartIdx].Pack;

  FillChar(FCurEcho, SizeOf(TCurEcho), 0);

  if CurDisCoord >= NeedDisCoord then
  begin
    DisCoord:= CurDisCoord;
    SysCoord:= CurSysCoord;
    Offset:= FData.Position2;
    Exit;
  end;

//  if FSkipBM then TestBMOffsetFirst;

  for R:= r_Left to r_Right do
  begin
    FCurEcho[R, 1].Count:= Ord(FLoadPack[R].Ampl <> 255);
    FCurEcho[R, 1].Delay[1]:= FLoadPack[R].Delay;
    FCurEcho[R, 1].Ampl[1]:= FLoadPack[R].Ampl;
  end;

//  BMSkipFlag:= False;
  repeat
//    if not TestOffsetNext then Exit;
{    if FSkipBM then
    begin
      Tmp:= BMSkipFlag;
      BMSkipFlag:= TestBMOffsetNext;
      if Tmp and (not BMSkipFlag) then
        for R:= r_Left to r_Right do
          FCurEcho[R, 1].Count:= 0;
    end; }

    SaveOffset:= FData.Position2;

    CurDisCoord_:= CurDisCoord;
    CurSysCoord_:= CurSysCoord;
    CurPosition_:= FData.Position2;

    FData.ReadBuffer2(LoadID, SizeOf(LoadID));
    SkipBytes:= - 1;
    if LoadID and 128 = 0 then
    begin
      if LoadID shr 3 and $07 = 1 then    // ���� ������ ����� �� �������������
      begin
        FSrcDat[0]:= LoadID;
        FData.ReadBuffer2(FSrcDat[1], Avk11EchoLen[LoadID and $07]);
        DataToEcho;
      end
      else if LoadID shr 3 and $07 = 0 then SkipBytes:= 1   // ���� ������� ����� �� ���������� 1 ����
      else SkipBytes:= Avk11EchoLen[LoadID and $07]; // ����� ����������
    end
    else
    begin
  {    if not BMSkipFlag then
      begin }
        case LoadID of

       EID_HandScan: begin           // ������
                       FData.ReadBuffer2(HSHead, 6);
                       repeat
                         FData.ReadBuffer2(HSItem, 3);
                         FData.ReadBuffer2(HSItem.Data[0], Avk11EchoLen[HSItem.Count]);
                       until HSItem.Sample = $FFFF;
                     end;

      EID_BSAmpl:    begin
                       FData.ReadBuffer2(BSHead, SizeOf(mBSAmplHead));
                       FData.Position2:= FData.Position2 + BSHead.Len * 2;
                       SkipBytes:= - 1;
                     end;


                     EID_ACLeft      ,
                     EID_ACRight     ,  EID_Sens       ,  EID_Att         , EID_VRU         ,
                     EID_StStr       ,  EID_EndStr     ,  EID_HeadPh      , EID_Mode        ,
                     EID_2Tp         ,  EID_ZondImp    ,  EID_SetRailType , EID_Stolb       , EID_Strelka     ,
                     EID_DefLabel    ,  EID_TextLabel  ,  EID_StBoltStyk  , EID_EndBoltStyk , EID_2Tp_Word    ,
                     EID_Time        ,  EID_StLineLabel,  EID_EndLineLabel, EID_StSwarStyk  ,
                     EID_EndSwarStyk ,  EID_OpChange   ,  EID_PathChange  , EID_MovDirChange,
                     EID_SezdNo      ,  EID_LongLabel  ,  EID_GPSCoord   ,  EID_GPSState    , EID_GPSError: SkipBytes:= Avk11EventLen[LoadID];

            EID_SysCrd_SS: begin // ��������� ���������� �������� ������, �������� ����������
                             FData.ReadBuffer2(LoadByte[1], 2);
                             CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[2]);
                           end;
            EID_SysCrd_SF: begin // ��������� ���������� �������� ������, ������ ����������
                             FData.ReadBuffer2(LoadByte[1], 1);
                             FData.ReadBuffer2(LoadLongInt, 4);
                             CurSysCoord:= LoadLongInt[1];
                           end;
            EID_SysCrd_FS: begin // ��������� ���������� ������ ������, �������� ����������
                             FData.ReadBuffer2(LoadByte[1], 5);
                             CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[5]);
                           end;
            EID_SysCrd_FF: begin // ��������� ���������� ������ ������, ������ ����������
                             FData.ReadBuffer2(LoadByte[1], 4);
                             FData.ReadBuffer2(LoadLongInt, 4);
                             CurSysCoord:= LoadLongInt[1];
                           end;
              EID_EndFile: begin // ����� �����
                             DisCoord:= CurDisCoord;
                             SysCoord:= CurSysCoord;
                             Offset:= FData.Position2;
                             Exit;
                           end
        end;
{      end
      else
      begin
        SkipBytes:= Avk11EventLen[LoadID];
        if LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF] then UnPackEcho;
      end; }
    end;

    if SkipBytes <> - 1 then FData.Position2:= FData.Position2 + SkipBytes;

    if {(not BMSkipFlag) and } (LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF]) then
    begin
      Inc(CurDisCoord, Abs(CurSysCoord - LastSysCoord)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)
      LastSysCoord:= CurSysCoord;

      {if not SkipUnPack then}
      UnPackEcho;
//      SkipUnPack:= False;

      if CurDisCoord > NeedDisCoord then
      begin
        DisCoord:= CurDisCoord_;
        SysCoord:= CurSysCoord_;
        Offset:= CurPosition_;
        FData.Position2:= Offset;
        Break;
      end
      else
      if CurDisCoord = NeedDisCoord then
      begin
        DisCoord:= CurDisCoord;
        SysCoord:= CurSysCoord;
        Offset:= FData.Position2;
        Break;
      end
    end;
  until FData.Position2 > FMaxReadOffset;
end;

procedure TAvk11DataContainer.LoadData(StartDisCoord, EndDisCoord, LoadShift: Integer; BlockOk: TDataNotifyEvent);

var
  R: RRail;
  LoadID: Byte;
  Dat: Byte;
  I, J, K, SkipBytes, Offset, Ch: Integer;
  LastSysCoord: LongInt;
  LastDisCoord: LongInt;
  SaveLastSysCoord: LongInt;
  LoadByte: array [1..22] of Byte;
  LoadLongInt: array [1..5] of LongInt; // absolute LoadByte[1];
  HSHead: THSHead;
  HSItem: THSItem;
  CurEcho_: TCurEcho;
  NewSysCoord: LongInt;
  Tmp: Boolean;
  P: Pointer;
  BSHead: mBSAmplHead;

begin
  for R:= r_Left to r_Right do
    for I:= 2 to 7 do
      begin
        FCurEcho[R, I].Count:= 0;
        FCurEcho[R, I].ZerroFlag:= False;
      end;

  DisToFileOffset(StartDisCoord - LoadShift, FCurDisCoord, FCurSysCoord, Offset);
  for R:= r_Left to r_Right do
  begin
    FCurEcho[R, 1].Count:= Ord(FLoadPack[R].Ampl <> 255);
    FCurEcho[R, 1].Delay[1]:= FLoadPack[R].Delay;
    FCurEcho[R, 1].Ampl[1]:= FLoadPack[R].Ampl;
  end;

  LastSysCoord:= FCurSysCoord;
  FData.Position2:= Offset;

  GetParamFirst(StartDisCoord - LoadShift, FCurParams);

  repeat

    FCurOffset:= FData.Position2;
    SkipBytes:= - 1;

    if (FData.Position2 >= Header.TableLink) or
       (FData.Position2 >= FMaxReadOffset) then Exit;

    FData.ReadBuffer2(LoadID, 1);

    if LoadID and 128 = 0 then
    begin
      FSrcDat[0]:= LoadID;
      Ch:= LoadID shr 3 and 7;
      if Ch = 0 then
      begin
        FData.ReadBuffer2(Dat, 1);
        CurAmpl[SideToRail(TSide(LoadID shr 6 and 1))]:= Dat;
      end
      else
      begin
        FData.ReadBuffer2(FSrcDat[1], Avk11EchoLen[LoadID and $07]);
        if Ch = 1 then DataToEcho;
        with FCurEcho[SideToRail(TSide(LoadID shr 6 and 1)), Ch] do
          case LoadID and $07 of
            0: ZerroFlag:= True;
            1: begin
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[1];
                 Ampl[Count]:= FSrcDat[2] shr 4 and $0F;
               end;
            2: begin
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[1];
                 Ampl[Count]:= FSrcDat[3] shr 4 and $0F;
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[2];
                 Ampl[Count]:= FSrcDat[3] and $0F;
               end;
            3: begin
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[1];
                 Ampl[Count]:= FSrcDat[4] shr 4 and $0F;
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[2];
                 Ampl[Count]:= FSrcDat[4] and $0F;
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[3];
                 Ampl[Count]:= FSrcDat[5] shr 4 and $0F;
               end;
            4: begin
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[1];
                 Ampl[Count]:= FSrcDat[5] shr 4 and $0F;
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[2];
                 Ampl[Count]:= FSrcDat[5] and $0F;
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[3];
                 Ampl[Count]:= FSrcDat[6] shr 4 and $0F;
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[4];
                 Ampl[Count]:= FSrcDat[6] and $0F;
               end;
          end;
      end;
    end
    else
    begin
        case LoadID of
       EID_HandScan: begin           // ������
                       FData.ReadBuffer2(HSHead, 6);
                       repeat
                         FData.ReadBuffer2(HSItem, 3);
                         if HSItem.Count <= 4 then FData.ReadBuffer2(HSItem.Data[0], Avk11EchoLen[HSItem.Count]);
                       until HSItem.Sample = $FFFF;
                     end;

      EID_BSAmpl:    begin
                       FData.ReadBuffer2(BSHead, SizeOf(mBSAmplHead));
                       FData.Position2:= FData.Position2 + BSHead.Len * 2;
                       SkipBytes:= - 1;
                     end;

                     EID_Sens        ,  EID_Att         , EID_VRU         ,
                     EID_StStr       ,  EID_EndStr     ,  EID_HeadPh      , EID_Mode        ,
                     EID_2Tp         ,  EID_ZondImp    ,  EID_SetRailType , EID_Stolb       , EID_Strelka     ,
                     EID_DefLabel    ,  EID_TextLabel  ,  EID_StBoltStyk  , EID_EndBoltStyk , EID_2Tp_Word    ,
                     EID_Time        ,  EID_StLineLabel,  EID_EndLineLabel, EID_StSwarStyk  ,
                     EID_EndSwarStyk ,  EID_OpChange   ,  EID_PathChange  , EID_MovDirChange,
                     EID_SezdNo      ,  EID_LongLabel  ,  EID_GPSCoord   ,  EID_GPSState    , EID_GPSError: SkipBytes:= Avk11EventLen[LoadID];

               EID_ACLeft: begin
                             FData.ReadBuffer2(LoadByte[1], 1);
                             FCurAK[SideToRail(sLeft), 1]:= LoadByte[1] and 1 <> 0;
                             FCurAK[SideToRail(sLeft), 2]:= LoadByte[1] and 2 <> 0;
                             FCurAK[SideToRail(sLeft), 3]:= LoadByte[1] and 4 <> 0;
                             FCurAK[SideToRail(sLeft), 4]:= LoadByte[1] and 8 <> 0;
                             FCurAK[SideToRail(sLeft), 5]:= LoadByte[1] and 16 <> 0;
                             FCurAK[SideToRail(sLeft), 6]:= LoadByte[1] and 32 <> 0;
                             FCurAK[SideToRail(sLeft), 7]:= LoadByte[1] and 64 <> 0;
                             FCurAK[SideToRail(sLeft), 8]:= LoadByte[1] and 128 <> 0;
                           end;
              EID_ACRight: begin
                             FData.ReadBuffer2(LoadByte[1], 1);
                             FCurAK[SideToRail(sRight), 1]:= LoadByte[1] and 1 <> 0;
                             FCurAK[SideToRail(sRight), 2]:= LoadByte[1] and 2 <> 0;
                             FCurAK[SideToRail(sRight), 3]:= LoadByte[1] and 4 <> 0;
                             FCurAK[SideToRail(sRight), 4]:= LoadByte[1] and 8 <> 0;
                             FCurAK[SideToRail(sRight), 5]:= LoadByte[1] and 16 <> 0;
                             FCurAK[SideToRail(sRight), 6]:= LoadByte[1] and 32 <> 0;
                             FCurAK[SideToRail(sRight), 7]:= LoadByte[1] and 64 <> 0;
                             FCurAK[SideToRail(sRight), 8]:= LoadByte[1] and 128 <> 0;
                           end;
            EID_SysCrd_SS: begin // ��������� ���������� �������� ������, �������� ����������
                             FData.ReadBuffer2(LoadByte[1], 2);
                             NewSysCoord:=    Integer((Integer(FCurSysCoord) and $FFFFFF00) or LoadByte[2]);
                           end;
            EID_SysCrd_SF: begin // ��������� ���������� �������� ������, ������ ����������
                             FData.ReadBuffer2(LoadByte[1], 1);
                             FData.ReadBuffer2(LoadLongInt, 4);
                             NewSysCoord:=    LoadLongInt[1];
                           end;
            EID_SysCrd_FS: begin // ��������� ���������� ������ ������, �������� ����������
                             FData.ReadBuffer2(LoadByte[1], 5);
                             NewSysCoord:=    Integer((Integer(FCurSysCoord) and $FFFFFF00) or LoadByte[5]);
                           end;
            EID_SysCrd_FF: begin // ��������� ���������� ������ ������, ������ ����������
                             FData.ReadBuffer2(LoadByte[1], 4);
                             FData.ReadBuffer2(LoadLongInt, 4);
                             NewSysCoord:=    LoadLongInt[1];
                           end;
              EID_EndFile: begin // ����� �����
                             Exit;
                           end;
        end;

        if LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF] then
        begin
          UnPackEcho;

          FUnPackFlag[r_Left]:= False;
          FUnPackFlag[r_Right]:= False;

          if (FLoadPack[r_Left].Ampl <> $FF) then
          begin
            FCurEcho[r_Left, 1].Delay[1]:= FLoadPack[r_Left].Delay;
            FCurEcho[r_Left, 1].Ampl[1]:= FLoadPack[r_Left].Ampl;
            FCurEcho[r_Left, 1].Count:= 1;
            FCurEcho[r_Left, 1].ZerroFlag:= False;
            FUnPackFlag[r_Left]:= True;
          end;
          if (FLoadPack[r_Right].Ampl <> $FF) then
          begin
            FCurEcho[r_Right, 1].Delay[1]:= FLoadPack[r_Right].Delay;
            FCurEcho[r_Right, 1].Ampl[1]:= FLoadPack[r_Right].Ampl;
            FCurEcho[r_Right, 1].Count:= 1;
            FCurEcho[r_Right, 1].ZerroFlag:= False;
            FUnPackFlag[r_Right]:= True;
          end;

          BackMotion:= NewSysCoord < LastSysCoord;

          if Assigned(BlockOk) and (FCurDisCoord >= StartDisCoord) and (FCurDisCoord <= EndDisCoord) then
            if not BlockOk(StartDisCoord) then Break; // ��������� ������� ��� "������" ����������

          FCurSysCoord:= NewSysCoord;
                                                      // ���� ���� ������������� ������� ������� ������ �� ����� ����������
          if (Abs(FCurSysCoord - LastSysCoord) > 1) then
          if ((FLoadPack[r_Left].Ampl <> $FF) or (FLoadPack[r_Right].Ampl <> $FF)) then
          begin
            FSaveDisCoord:= FCurDisCoord;
            FSaveSysCoord:= FCurSysCoord;
            Move(FCurEcho, CurEcho_, SizeOf(TCurEcho));
            FillChar(FCurEcho, SizeOf(TCurEcho), 0);

            if (FLoadPack[r_Left].Ampl <> $FF) then
            begin
              FCurEcho[r_Left, 1].Delay[1]:= FLoadPack[r_Left].Delay;
              FCurEcho[r_Left, 1].Ampl[1]:= FLoadPack[r_Left].Ampl;
              FCurEcho[r_Left, 1].Count:= 1;
              FCurEcho[r_Left, 1].ZerroFlag:= False;
              FUnPackFlag[r_Left]:= True;
            end;
            if (FLoadPack[r_Right].Ampl <> $FF) then
            begin
              FCurEcho[r_Right, 1].Delay[1]:= FLoadPack[r_Right].Delay;
              FCurEcho[r_Right, 1].Ampl[1]:= FLoadPack[r_Right].Ampl;
              FCurEcho[r_Right, 1].Count:= 1;
              FCurEcho[r_Right, 1].ZerroFlag:= False;
              FUnPackFlag[r_Right]:= True;
            end;

            if FCurSysCoord > LastSysCoord then
            begin
              if FCurSysCoord - LastSysCoord < 5000 then
                for J:= LastSysCoord + 1 to FCurSysCoord - 1 do
                begin
                  Inc(FCurDisCoord);
                  FCurSysCoord:= J;
                  if Assigned(BlockOk) and (FCurDisCoord >= StartDisCoord) and (FCurDisCoord <= EndDisCoord) then
                    if not BlockOk(StartDisCoord) then Break;
                  if FCurDisCoord >= EndDisCoord then Exit;
                end;
            end
            else
              if LastSysCoord - FCurSysCoord < 5000 then
                for J:= LastSysCoord - 1 downto FCurSysCoord + 1 do
                begin
                  Inc(FCurDisCoord);
                  FCurSysCoord:= J;
                  if Assigned(BlockOk) and (FCurDisCoord >= StartDisCoord) and (FCurDisCoord <= EndDisCoord) then
                    if not BlockOk(StartDisCoord) then Break;
                  if FCurDisCoord >= EndDisCoord then Exit;
                end;

            Move(CurEcho_, FCurEcho, SizeOf(TCurEcho));
            FCurDisCoord:= FSaveDisCoord;
            FCurSysCoord:= FSaveSysCoord;
          end;

          Inc(FCurDisCoord, Abs(FCurSysCoord - LastSysCoord)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)
          LastSysCoord:= FCurSysCoord;
          LastDisCoord:= FCurDisCoord;

          GetParamNext(FCurDisCoord, FCurParams);
          if FCurDisCoord >= EndDisCoord then Exit;

          for R:= r_Left to r_Right do
          begin
            for I:= 1 to 7 do
            begin
              FCurEcho[R, I].Count:= 0;
              FCurEcho[R, I].ZerroFlag:= False;
            end;
            CurAmpl[R]:= - 1;
          end;
        end;

    end;
    if SkipBytes <> - 1 then FData.Position2:= FData.Position2 + SkipBytes;
  until FData.Position2 > FMaxReadOffset;
end;

function TAvk11DataContainer.GetBMStateFirst(DisCoord: Integer): Boolean;
var
  I: Integer;
  EndIdx: Integer;
  StartIdx: Integer;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 37');
    Exit;
  end;
}
  Result:= False;
  FGetBMStateIdx:= 0;

  for I:= High(FCDList) downto 0 do
  begin
    if FEvents[FCDList[I]].DisCoord <= DisCoord then
    begin
      if FEvents[FCDList[I]].ID = EID_FwdDir then
      begin
        Result:= False;
        FGetBMStateIdx:= I + 1;
        Break;
      end;
      if FEvents[FCDList[I]].ID = EID_BwdDir {EID_FwdDirEnd} then
      begin
        Result:= True;
        FGetBMStateIdx:= I + 1;
        Break;
      end;
      Break;
    end;
  end;
  FGetBMStateState:= Result;
end;

function TAvk11DataContainer.GetBMStateNext(var DisCoord: Integer; var State: Boolean): Boolean;
var
  I: Integer;
  EndIdx: Integer;
  StartIdx: Integer;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 38');
    Exit;
  end;
}
  Result:= False;
  State:= FGetBMStateState;
//  if FSkipBM then Exit;

  for I:= FGetBMStateIdx to High(FCDList) do
  begin
    if FEvents[FCDList[I]].ID = EID_FwdDir then
    begin
      State:= False;
      FGetBMStateIdx:= I + 1;
      FGetBMStateState:= False;
      DisCoord:= FEvents[FCDList[I]].DisCoord;
      Result:= I <> GetEventCount - 1;
      Break;
    end;
    if FEvents[FCDList[I]].ID = EID_BwdDir {EID_FwdDirEnd} then
    begin
      State:= True;
      FGetBMStateIdx:= I + 1;
      FGetBMStateState:= True;
      DisCoord:= FEvents[FCDList[I]].DisCoord;
      Result:= I <> GetEventCount - 1;
      Break;
    end;
  end;
end;

function TAvk11DataContainer.GetEventIdx(StartDisCoord, EndDisCoord: Integer; var StartIdx, EndIdx: Integer; EmptyPar: Integer): Boolean;
var
  I: Integer;
  SameCoord1: TIntegerDynArray;
  SameCoord2: TIntegerDynArray;
  LeftIdx1: Integer;
  LeftIdx2: Integer;
  RightIdx1: Integer;
  RightIdx2: Integer;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 39');
    Exit;
  end;
}

  GetNearestEventIdx(StartDisCoord, LeftIdx1, RightIdx1, SameCoord1);
  GetNearestEventIdx(EndDisCoord, LeftIdx2, RightIdx2, SameCoord2);

  if Length(SameCoord1) <> 0 then StartIdx:= SameCoord1[0]
                             else StartIdx:= LeftIdx1 + 1;

  if Length(SameCoord2) <> 0 then EndIdx:= SameCoord2[High(SameCoord2)]
                             else EndIdx:= RightIdx2 - 1;
end;

procedure TAvk11DataContainer.GetNearestEventIdx(DisCoord: Integer; var LeftIdx, RightIdx: Integer; var SameCoord: TIntegerDynArray);
var
  I: Integer;
  CLeft: Integer;
  CRight: Integer;
  CurrPos: Integer;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 40');
    Exit;
  end; }

  if FEventsCount = 0 then
  begin
    LeftIdx:= - 1;
    RightIdx:= - 1;
    SetLength(SameCoord, 0);
    Exit;
  end;
  CLeft:= 0; // ������ � ������ � ������
  CRight:= FEventsCount - 1; // ����� �������������� � �����
  repeat
    CurrPos:= (CLeft + CRight) div 2; // ���������� ��������
    begin
      Inc(GlobCnt);
      if FEvents[CurrPos].DisCoord < DisCoord then CLeft:= CurrPos // ������� ����� �������� ����� ��� ����������� �������
                                                        else CRight:= CurrPos;
      if (CLeft = CRight) or (Abs(CLeft - CRight) = 1) then // �������� �� ����� ������
      begin
        for I:= CLeft downto 0 do
          if DisCoord = FEvents[I].DisCoord then
          begin
            SetLength(SameCoord, Length(SameCoord) + 1);
            SameCoord[High(SameCoord)]:= I;
            LeftIdx:= I;
          end
          else
          begin
            if Length(SameCoord) <> 0 then LeftIdx:= SameCoord[0]
                                      else LeftIdx:= CLeft;
            Break;
          end;

        for I:= CLeft + 1 to FEventsCount - 1 do
          if DisCoord = FEvents[I].DisCoord then
          begin
            SetLength(SameCoord, Length(SameCoord) + 1);
            SameCoord[High(SameCoord)]:= I;
            RightIdx:= I;
          end
          else
          begin
            if Length(SameCoord) <> 0 then RightIdx:= SameCoord[High(SameCoord)]
                                      else RightIdx:= I;
            Break;
          end;

        Exit;
      end;
    end;
  until false;
end;

function TAvk11DataContainer.GetLeftEventIdx(DisCoord: Integer): Integer;
var
  LeftIdx, RightIdx: Integer;
  SameCoord: TIntegerDynArray;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 41');
    Exit;
  end; }

  GetNearestEventIdx(DisCoord, LeftIdx, RightIdx, SameCoord);
  if Length(SameCoord) <> 0 then Result:= SameCoord[0]
                            else Result:= LeftIdx;
end;

function TAvk11DataContainer.GetRightEventIdx(DisCoord: Integer): Integer;
var
  LeftIdx, RightIdx: Integer;
  SameCoord: TIntegerDynArray;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 42');
    Exit;
  end; }

  GetNearestEventIdx(DisCoord, LeftIdx, RightIdx, SameCoord);
  if Length(SameCoord) <> 0 then Result:= SameCoord[High(SameCoord)]
                            else Result:= RightIdx;
end;

procedure TAvk11DataContainer.GetEventData(Idx: Integer; var ID: Byte; var pData: PEventData);
var
  ED: TEventData;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 43');
    Exit;
  end; }

  if (Idx >= 0) and (Idx <= FEventsCount  -1) then
  begin
    ID:= FEvents[Idx].ID;
    ED:= FEvents[Idx].Data;
    pData:= Addr(ED);
  end;
end;

function TAvk11DataContainer.GetParamFirst(DisCoord: Integer; var Params: TMiasParams): Boolean;
{
var
  StartIdx, EndIdx: Integer;
 }
begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 44');
    Exit;
  end; }

  DisCoord:= NormalizeDisCoord(DisCoord);
  if DisCoord <= 0 then DisCoord:= 1;

  FGetParamIdx:= GetLeftEventIdx(DisCoord);
  if FGetParamIdx <> - 1 then
    Params:= FEvents[FGetParamIdx].Params;
end;

function TAvk11DataContainer.GetParamNext(DisCoord: Integer; var Params: TMiasParams): Boolean;
var
  I: Integer;
  StartIdx, EndIdx: Integer;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 45');
    Exit;
  end; }

  if FGetParamIdx = - 1 then Exit;

  for I:= FGetParamIdx to FEventsCount - 1 do
  begin
    Inc(GlobCnt);
    if DisCoord = FEvents[I].DisCoord then
    begin
      Params:= FEvents[I].Params;
      FGetParamIdx:= I;
      Break;
    end;
    if DisCoord < FEvents[I].DisCoord then
    begin
      Params:= FEvents[I - 1].Params;
      FGetParamIdx:= I;
      Break;
    end;
  end;
end;

function TAvk11DataContainer.GetCoord(DisCoord: Integer; var CoordParams: TCoordParams): Boolean;
var
  ID: Byte;
  I, J: Integer;
  pData: PEventData;
  StartIdx {, EndIdx}: Integer;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 46');
    Exit;
  end; }

  // ����� ������ �������������� ������ �� DisCoord

  DisCoord:= NormalizeDisCoord(DisCoord);

  CoordParams.RightStolb[ssLeft].Km:= FExHeader.EndKM;
  CoordParams.RightStolb[ssLeft].Pk:= FExHeader.EndPK;
  CoordParams.RightStolb[ssRight].Km:= FExHeader.EndKM;
  CoordParams.RightStolb[ssRight].Pk:= FExHeader.EndPK;
  if FStolbCount = 0 then CoordParams.ToRightStolbMM:= {100000 - }(Header.StartMetre * 1000 - DisToSysCoord(DisCoord) * Header.ScanStep div 100) else
  begin
    for I:= FEvents.Count - 1 downto 0 do
    begin
      GetEventData(I, ID, pData);
      if ID = EID_Stolb then
      begin
        CoordParams.ToRightStolbMM:= 100000 - ((DisToSysCoord(DisCoord) - FEvents[I].SysCoord) * Header.ScanStep div 100);
        Break;
      end;
    end;
  end;

  StartIdx:= GetRightEventIdx(DisCoord);
  for I:= StartIdx to FEvents.Count - 1 do
  begin
    GetEventData(I, ID, pData);
    if ID = EID_Stolb then
    begin
      CoordParams.RightStolb[ssLeft].Km:= pCoord(pData)^.Km[0];
      CoordParams.RightStolb[ssLeft].Pk:= pCoord(pData)^.Pk[0];
      CoordParams.RightStolb[ssRight].Km:= pCoord(pData)^.Km[1];
      CoordParams.RightStolb[ssRight].Pk:= pCoord(pData)^.Pk[1];
      CoordParams.ToRightStolbMM:= (FEvents[I].SysCoord - DisToSysCoord(DisCoord)) * Header.ScanStep div 100;
      Break;
    end;
  end;

  // ����� ������ �������������� ����� �� DisCoord

  CoordParams.LeftStolb[ssLeft].Km:= Header.StartKM;
  CoordParams.LeftStolb[ssLeft].Pk:= Header.StartPk;
  CoordParams.LeftStolb[ssRight].Km:= Header.StartKM;
  CoordParams.LeftStolb[ssRight].Pk:= Header.StartPk;
  CoordParams.ToLeftStolbMM:= Header.StartMetre * 1000 + DisToSysCoord(DisCoord) * Header.ScanStep div 100;

  StartIdx:= GetLeftEventIdx(DisCoord);

  for I:= StartIdx downto 0 do
  begin
    GetEventData(I, ID, pData);
    if ID = EID_Stolb then
    begin
      CoordParams.LeftStolb[ssLeft].Km:= pCoord(pData)^.Km[0];
      CoordParams.LeftStolb[ssLeft].Pk:= pCoord(pData)^.Pk[0];
      CoordParams.LeftStolb[ssRight].Km:= pCoord(pData)^.Km[1];
      CoordParams.LeftStolb[ssRight].Pk:= pCoord(pData)^.Pk[1];
      CoordParams.ToLeftStolbMM:= (DisToSysCoord(DisCoord) - FEvents[I].SysCoord) * Header.ScanStep div 100;
      Break;
    end;
  end;

end;

function TAvk11DataContainer.GetTime(DisCoord: Integer): string;
var
  ID: Byte;
  I: Integer;
  pData: PEventData;
  StartIdx: Integer;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 47');
    Exit;
  end; }

  DisCoord:= NormalizeDisCoord(DisCoord);
  StartIdx:= GetLeftEventIdx(DisCoord);

  Result:= CodeToTime(Header.Hour, Header.Minute);

  for I:= StartIdx downto 0 do
  begin
    GetEventData(I, ID, pData);
    if ID in [    EID_Stolb, EID_StBoltStyk,  EID_EndLineLabel,
                EID_Strelka, EID_EndBoltStyk, EID_StSwarStyk,
               EID_DefLabel, EID_Time,        EID_EndSwarStyk,
              EID_TextLabel, EID_StLineLabel ] then
    begin
      Result:= CodeToTime(pData^[Avk11EventLen[ID] - 1], pData^[Avk11EventLen[ID] - 0]);
      Exit;
    end;
  end;
end;

function TAvk11DataContainer.GetSpeed(DisCoord: Integer): string;
var
  I: Integer;
  J: Integer;
  T: Extended;
  S: Extended;

begin
{  if FHandScanReg then
  begin
    ShowMessage('Mode Error 48');
    Exit;
  end; }

  for I:= High(FTimeList) downto 0 do
    if FTimeList[I].DC < DisCoord then
    begin
      if I <> High(FTimeList) then J:= I + 1 else J:= I - 1;

      T:= Abs(FTimeList[J].H + FTimeList[J].M / 60 - (FTimeList[I].H + FTimeList[I].M / 60));
      S:= Abs(FTimeList[J].DC - FTimeList[I].DC) * Header.ScanStep / 100 / 1000000;
      Result:= Format('%3.1f ' + Language.GetCaption(0000023), [S / T]);
      Exit;
    end;
end;

var
  I: Integer;

initialization

  for I:= 0 to 255 do Avk11TextEncode[2, I]:= Chr(I);

end.

