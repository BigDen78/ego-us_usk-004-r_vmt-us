unit Logging;

interface
uses Windows, Classes, SysUtils, Types,  Forms, Math, IniFiles, Messages, Dialogs, Graphics, MMSystem, SyncObjs,
     PublicFunc;

type
  // ����� ������ � ����� ��� ��������� (���������� ���), ����������:
  // 1. � ����� ����� � ��������� �������� ���, �������� Logger.StartLogging (��������� ������ �� ����).
  // 2. �� ������ ����� ��������� �������� Logger.Add(�����.)

  // ��������� ������ �� �����! �������������� �� ����� � ������� ������ �� ������.
  // ������ ������ ����������������!

  // ��������� ����������� ������������:
  // 1. ����� � ����.
  // 2. ���������������� ����� � UI.
  // 3. ����� ��������� N ����� � StringList.


  TLogProvider = class;
  TLocalLog = class;

  Logger = class
  private
  public
    class procedure Add( Msg: string );
    class procedure StartLogging;
    class function GetGlobalLog: TLocalLog;
  end;

  TLocalLog = class
  private
    CS: TCriticalSection;
    LogProviders: array of TLogProvider;
    function GetLogProvider(Index: Integer): TLogProvider;
  public
    constructor Create(LogProvider: TLogProvider); // ������������ ���� �����������, ������ ���� ����� ������� ��������� ��� ��� ������ ������-�� �������, � �� ��� ���� ���������.
    destructor Destroy(); override;
    procedure Add( Msg: string );
    procedure AddProvider(LogProvider: TLogProvider);
    property Provider[Index: Integer]: TLogProvider read GetLogProvider;
  end;

  TLogProvider = class
  public
    procedure Add(Msg: string); virtual; abstract;
  end;

  TFileLogProvider = class ( TLogProvider )
  private
    F: Text;
  public
    constructor Create(FileName: string);
    procedure Add(Msg: string); override;
    destructor Destroy; override;
  end;

  TOnAddProc = procedure( Msg: string ) of object;

  TListLogProvider = class ( TLogProvider )
  private
    CS: TCriticalSection;
    SL: TStringList;
    MaxSize: Integer;
    OnAddProc: TOnAddProc;
    procedure SyncAdd;

  public
    constructor Create( MaxSize: Integer ); overload;
    constructor Create( OnAddProc: TOnAddProc ); overload;
    procedure Add(Msg: string); override;
    destructor Destroy; override;
    procedure SyncList(ExtList: TStringList);
  end;


implementation
var
  GlobalLog: TLocalLog;

{ Logger }

class procedure Logger.Add(Msg: string);
begin
  if GlobalLog <> nil then
    GlobalLog.Add( Msg );
end;

class function Logger.GetGlobalLog: TLocalLog;
begin
  Result:= GlobalLog;
end;

class procedure Logger.StartLogging;
begin
  if GlobalLog = nil then
    GlobalLog:= TLocalLog.Create( TFileLogProvider.Create('syslog.txt') );
end;


procedure TLocalLog.Add(Msg: string);
var
  i: Integer;

begin
  CS.Acquire; { lock out other threads }
  try
    for i:= 0 to High(LogProviders) do
      LogProviders[i].Add(Msg);
  finally
    CS.Release;
  end;
end;

constructor TLocalLog.Create(LogProvider: TLogProvider);
begin
  inherited Create;
  CS:= TCriticalSection.Create;
  SetLength( Self.LogProviders, 1 );
  Self.LogProviders[0]:= LogProvider;
end;

destructor TLocalLog.Destroy;
var
  i: Integer;
  
begin
  CS.Free;
  for i:= 0 to High(LogProviders) do
    LogProviders[i].Free;
  inherited;
end;

procedure TLocalLog.AddProvider(LogProvider: TLogProvider);
begin
  SetLength( LogProviders, Length( LogProviders ) + 1 );
  LogProviders[ High( LogProviders ) ]:= LogProvider;
end;

function TLocalLog.GetLogProvider(Index: Integer): TLogProvider;
begin
  Result:= LogProviders[Index];
end;

{ TFileLogProvider }

procedure TFileLogProvider.Add(Msg: string);
begin
  Writeln( F, FormatDateTime( 'dd.mm.yyyy hh:nn:ss:zzz  ', Now() ) + Msg );
end;


constructor TFileLogProvider.Create(FileName: string);
begin
  AssignFile( F, AddSlash( ExtractFilePath( Application.ExeName ) ) + FileName );
  Rewrite( F );
end;

destructor TFileLogProvider.Destroy;
begin
  CloseFile( F );
  inherited;
end;

{ TStreamLogProvider }

procedure TListLogProvider.Add(Msg: string);
begin
  CS.Enter;
  SL.Add( FormatDateTime( 'nn:ss', Now()) + ' ' + Msg );
  while SL.Count > MaxSize do SL.Delete(0);
  CS.Leave;
  if Assigned( OnAddProc ) then TThread.Synchronize(nil, SyncAdd);
end;

constructor TListLogProvider.Create( MaxSize: Integer );
begin
  SL:= TStringList.Create;
  CS:= TCriticalSection.Create;
  Self.MaxSize:= MaxSize;
  Self.OnAddProc:= nil;
end;

constructor TListLogProvider.Create( OnAddProc: TOnAddProc );
begin
  SL:= TStringList.Create;
  CS:= TCriticalSection.Create;
  Self.MaxSize:= 1000;
  Self.OnAddProc:= OnAddProc;
end;



destructor TListLogProvider.Destroy;
begin
  SL.Free;
  CS.Free;
  inherited;
end;

procedure TListLogProvider.SyncAdd;
begin
  CS.Enter;
  while SL.Count > 0 do
  begin
    OnAddProc( SL[0] );
    SL.Delete(0);
  end;
  CS.Leave;
end;

procedure TListLogProvider.SyncList(ExtList: TStringList);
begin
  CS.Enter;
  ExtList.Clear;
  ExtList.AddStrings( SL );
  CS.Release;
end;

initialization
  GlobalLog:= nil;

finalization
  GlobalLog.Free;
end.