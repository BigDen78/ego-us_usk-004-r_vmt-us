unit FileTransfer;

interface
uses Windows, Forms, Classes, SysUtils, CompressUnit;

type

  TFileDescription = class
  private
    FFileSrc: string;
    FFileDst: string;
  public
    constructor Create(Src, Dst: string);
    property FileSrc: string read FFileSrc;
    property FileDst: string read FFileDst;
  end;

  TFileList = class(TList)
  protected
    function Get(Index: Integer): TFileDescription;
    procedure Put(Index: Integer; Item: TFileDescription);
  public
    destructor Destroy; override;
    procedure AddFile(Src, Dst: string);
    property Items[Index: Integer]: TFileDescription read Get write Put; default;
  end;

  TTransferProgressEvent = procedure(Progress: Integer) of object;

  TTransferEngine = class
  private
    FCompress: TCompress;
    procedure SendProgress(Progress: Integer);
    function CopyF(FileList: TFileList; DeleteSrc: Boolean): Boolean;
    procedure FileUnCompressed(FileName: string);
  public
    OnTranferProgress: TTransferProgressEvent;
    constructor Create;
    destructor Destroy; override;
    function CopyFiles(FileList: TFileList): Boolean;
    function MoveFiles(FileList: TFileList): Boolean;
    function UnPackFiles(BurnFileName, ExtractFolder: string): Boolean;
    function PackFiles(FileList: TFileList; BurnFileName, BurnPath: string): Boolean;
  end;

implementation

{ TFileList }

procedure TFileList.AddFile(Src, Dst: string);
var
  F: TFileDescription;
begin
  F:= TFileDescription.Create(Src, Dst);
  Self.Add(F);
end;

destructor TFileList.Destroy;
var
  i: Integer;
begin
  for i:= 0 to Self.Count - 1 do
     Self.Items[i].Free;
  inherited;
end;

function TFileList.Get(Index: Integer): TFileDescription;
begin
  Result:= inherited Get( Index );
end;

procedure TFileList.Put(Index: Integer; Item: TFileDescription);
begin
  inherited Put( Index, Item );
end;

{ TFileDescription }

constructor TFileDescription.Create(Src, Dst: string);
begin
  FFileSrc:= Src;
  FFileDst:= Dst;
end;

{ TTransferEngine }

function TTransferEngine.CopyF(FileList: TFileList; DeleteSrc: Boolean): Boolean;
var
  i: Integer;
  F: TFileDescription;
  MS: TMemoryStream;
  DstFolder: string;
begin
  Result:= False;
  SendProgress(0);
  if not Assigned(FileList) then Exit;
  if FileList.Count = 0 then Exit;

  MS:= TMemoryStream.Create;
  for i:= 0 to FileList.Count - 1 do
    begin
      F:= FileList.Items[i];
      if FileExists(F.FileSrc) then
        begin
          MS.LoadFromFile(F.FileSrc);
          DstFolder:= ExtractFilePath(F.FileDst);
          ForceDirectories(DstFolder);
          MS.SaveToFile(F.FileDst);
          if DeleteSrc then
            DeleteFile(F.FileSrc);
        end;
      SendProgress(Round((i/FileList.Count) * 100));
      Application.ProcessMessages;
    end;
  MS.Free;
  Result:=true;
end;

function TTransferEngine.CopyFiles(FileList: TFileList): Boolean;
begin
  Result:= CopyF(FileList, False);
end;

constructor TTransferEngine.Create;
begin
  OnTranferProgress:= nil;
  FCompress:= TCompress.Create;
  FCompress.OnFileUncompressed:= FileUnCompressed;
end;

destructor TTransferEngine.Destroy;
begin
  OnTranferProgress:= nil;
  FCompress.Free;
  inherited;
end;

procedure TTransferEngine.FileUnCompressed(FileName: string);
var
  Name, Path, Template, NumStr, NewFileName: string;
  NumInt: integer;

  sr: TSearchRec;
  Num, MaxNum: integer;
  FindRes, i: Integer;
begin
  Name:= ExtractFileName(FileName);
  Path:= ExtractFilePath(FileName);

  //Template:= Copy(Name, 1, 5);
  Template:= Copy(Name, 1, 4);
  //if (Template='Var1_') or (Template='Var2_') or (Template='Var3_') then
  if (Template='Var1') or (Template='Var2') or (Template='Var3') then
    begin
      {NumStr:= Copy(Name, 6, Length(Name)-9);
      try
        NumInt:= StrToInt(NumStr);
      except
      end;}
      // ��������� ������������ �����
      MaxNum:=0;
      FindRes := FindFirst(Path + Template+'_*.kan', faAnyFile, SR);
      while FindRes = 0 do
        begin
          if ( SR.Name <> '.' ) and ( SR.Name <> '..' ) then
            begin
              NumStr:= Copy(SR.Name, 6, Length(SR.Name)-9);
              try
                Num:= StrToInt(NumStr);
              except
              end;
            end;
          if Num > MaxNum then
            MaxNum:= Num;
          FindRes := FindNext(SR);
        end;
      FindClose(SR);
      Inc(MaxNum);
      NewFileName:= Path + Format('%s_%d.kan', [Template, MaxNum]);
      RenameFile(FileName, NewFileName);
    end;

end;

function TTransferEngine.MoveFiles(FileList: TFileList): Boolean;
begin
  Result:= CopyF(FileList, True);
end;

function TTransferEngine.PackFiles(FileList: TFileList; BurnFileName,
  BurnPath: string): Boolean;
var
 SL: TStringList;
 WorkFolder: string;
 i: integer;
begin
  result:=false;
  if BurnFileName = '' then Exit;
  if BurnPath = '' then Exit;
  if FileList.Count = 0 then Exit;

  WorkFolder:= ExtractFilePath(Application.ExeName);

  SL:= TStringList.Create;
  for i:= 0 to FileList.Count - 1 do
     SL.Add(FileList[i].FFileSrc);

  FCompress.CompessFiles( WorkFolder, SL, BurnPath+BurnFileName );
  SL.Free;

  result := true;
end;

procedure TTransferEngine.SendProgress(Progress: Integer);
begin
  if Assigned( OnTranferProgress ) then
    OnTranferProgress(Progress);
end;

function TTransferEngine.UnPackFiles(BurnFileName, ExtractFolder: string): Boolean;
begin
  if BurnFileName = '' then Exit;
  if ExtractFolder = '' then Exit;

  if FCompress.UnCompressFiles(BurnFileName, ExtractFolder, false) = 0 then
    result := true;
end;

end.
