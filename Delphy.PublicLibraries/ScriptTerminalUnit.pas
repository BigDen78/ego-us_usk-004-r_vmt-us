unit ScriptTerminalUnit;

interface
uses Windows, Classes, Forms, SysUtils, Contnrs, Math,
     DataFileUnit, DeviceInterfaceUnit;

// ������ ������ ������������ ��� ������ � �������� ����������� ������ �������� ������ (�������� �������)

const
  // ���� ������ ���������� ������� ��� �������:
  eeUnknown = -255;
  eeCantSendData = -1;
  eeBadAnswer = -2;
  eeNoScript = -3;
  eeOK = 0;

type
  TModemCommand =
  record
    Command: string;
    Answer: string;
    ErrorCode: Integer;
    ErrorDescription: string;
    WaitInterval: Integer; // ����� �������� ������� (��)
  end;
  PModemCommand = ^TModemCommand;

  TScript = class( TList )
  private
    FExtOnByteReserved: TOnByteReserved;
    ReservedAnswer: string;
    procedure OnByteReserved( B: Byte );
  public
    Name: string;
    ID: string;
    procedure Add( Command: TModemCommand );
    procedure Clear; override;
    function Get( Index: Integer ): PModemCommand;
    procedure Load( DataFile: TDataFile );
    procedure Save( DataFile: TDataFile );
    destructor Destroy; override;
  end;

  TScriptList = class( TObjectList )
  private
  public
    procedure Clear; override;
    procedure Delete(Index: Integer);
    function Get( IndexOrID: Variant ): TScript; overload;
    function Get( Index: Integer ): TScript; overload;
    function Get( ID: string ): TScript; overload;
    function LoadFromFile( Filename: string ): string;
    procedure SaveToFile( Filename: string );
    destructor Destroy; override;
  end;

  TOnEnterDataMode = procedure of object;

  TModemScriptEngine = class
  private
    WaitAnswer: Boolean;
    ReservedAnswer: string;
    FDevice: TDevice;
    FStopWait: Boolean;
    procedure OnByteReservedInternal( B: Byte );
  protected
    procedure SetDevice( NewDevice: TDevice );
  public
    ScriptList: TScriptList;
    OnByteReserved: TOnByteReserved;
    ConnectID: string;
    OnEnterDataMode: TOnEnterDataMode;
    property Device: TDevice read FDevice write SetDevice;
    property StopWait: Boolean write FStopWait;                                                // ���������� ���������� �������� ������ �� ������.
    constructor Create( Device_: TDevice );
    function LoadScriptList( ScriptFilename: string ): string;
    function ExecCommand(Command: string; var Answer: string; WaitInterval: Integer ): Integer; // ��������� �������, ��������� ������ � ������� ����� � ���������.
    function SendCommand(Command: string): Integer;                                            // ��������� �������.
    function ExecScript( IndexOrID: Variant; out ErrorDescription: string ): Integer;          // ��������� ������ (����������� ������������������ ������)
    destructor Destroy; override;
  end;

implementation

uses Variants;
const
  ScriptFileID = 'MDMScript';

//==============================================================================
//============================= TModemScriptEngine =============================

constructor TModemScriptEngine.Create( Device_: TDevice );
begin
  inherited Create;
  WaitAnswer:= False;
  OnByteReserved:= nil;
  ScriptList:= nil;
  Device:= Device_;
  ConnectID:= 'CONNECT';
  FStopWait:= False;
end;

procedure TModemScriptEngine.SetDevice( NewDevice: TDevice );
begin
  FDevice:= NewDevice;
  if Assigned( FDevice ) then
  begin
    FDevice.ProcessBlocks:= False;
    FDevice.OnByteReserved:= OnByteReservedInternal;
  end;
end;

function TModemScriptEngine.LoadScriptList( ScriptFilename: string ): string;
begin
  if ScriptFilename = '' then
  begin
    if Assigned( ScriptList ) then ScriptList.Free;
    ScriptList:= nil;
    Result:= '';
  end else
  begin
    if Assigned( ScriptList ) then ScriptList.Clear else ScriptList:= TScriptList.Create;
    Result:= ScriptList.LoadFromFile( ScriptFilename );
  end;
end;

procedure TModemScriptEngine.OnByteReservedInternal(B: Byte);
begin
  if not ( B in [10, 13] ) then
  begin
    ReservedAnswer:= ReservedAnswer + Chr( B );
    if ( ReservedAnswer = ConnectID ) and Assigned( OnEnterDataMode ) then OnEnterDataMode; 
    if not WaitAnswer and ( Length( ReservedAnswer ) > Length( ConnectID ) ) then ReservedAnswer:= '';
  end;
  if Assigned( OnByteReserved ) then OnByteReserved( B );
end;

function TModemScriptEngine.SendCommand(Command: string ): Integer;
var
  DataToSend: string;
  j: Integer;

begin
  // ��� ������� ������������� ����������� �������� CR (#13). ���� ���������� ��� ��������� (����. ��� ������� "+++"),
  // ������� ��������� ������ ������� �������� #0.
  
  Result:= eeUnknown;
  if not Assigned( Device ) then Exit;
  if Command[ Length( Command ) ] = #0 then DataToSend:= Copy( Command, 1, Length( Command ) - 1 ) else DataToSend:= Command + #13;
  for j:= 1 to Length( DataToSend ) do
  if not Device.PutByte( Ord( DataToSend[j] ) ) then
  begin
    Result:= eeCantSendData;
    Exit;
  end;
  Result:= eeOK;
end;

function TModemScriptEngine.ExecCommand(Command: string; var Answer: string; WaitInterval: Integer): Integer;
var
  AnswerOK: Boolean;
  isBadAnswer: Boolean;
  Tick: Cardinal;
  SendResult: Integer;
  j: Integer;

begin
  WaitAnswer:= True;
  ReservedAnswer:= '';
  Result:= eeUnknown;

  // �������� ������.
  SendResult:= SendCommand( Command );
  if SendResult <> eeOK then
  begin
    Result:= SendResult;
    WaitAnswer:= False;
    Exit;
  end;

  // ����� ������.
  AnswerOK:= False;
  if WaitInterval = 0 then WaitInterval:= 2000;
  Tick:= GetTickCount;
  while ( GetTickCount - Tick < WaitInterval ) and not FStopWait do
  begin
    // ����� ������?
    if Pos( Trim( Answer ), Trim( ReservedAnswer ) ) <> 0 then
    begin
      AnswerOK:= True;
      Break;
    end;

    // ����� ��� �� �������� �������?
    isBadAnswer:= False;
    for j:= 1 to Min( Length( ReservedAnswer ) - Length( Command ), Length( Answer ) ) do
    if ( ReservedAnswer[ Length( Command ) + j] <> Answer[j] ) and ( ReservedAnswer[j] <> Answer[j] ) then
      isBadAnswer:= True;
    if isBadAnswer then Break;

    Application.ProcessMessages;
    Sleep(0);
  end;
  if AnswerOK then Result:= eeOK else Result:= eeBadAnswer;
  Answer:= ReservedAnswer;
  WaitAnswer:= False;
end;

function TModemScriptEngine.ExecScript( IndexOrID: Variant; out ErrorDescription: string): Integer;
var
  i: Integer;
  Script: TScript;
  Cmd: PModemCommand;
  Answer: string;

begin
  Result:= eeNoScript;
  ErrorDescription:= '������������� ������ �� ������.';
  if not Assigned( ScriptList ) then Exit;
  Script:= ScriptList.Get( IndexOrID );
  if not Assigned( Script ) then Exit;
  for i:= 0 to Script.Count - 1 do
  begin
    Cmd:= Script.Get(i);
    Answer:= Cmd.Answer;
    Result:= ExecCommand( Cmd.Command, Answer, Cmd.WaitInterval );
    case Result of
    eeCantSendData:
      begin
        ErrorDescription:= '���������� ��������� ������ � �����!';
        Exit;
      end;
    eeOK: ;
    eeBadAnswer:
      begin
        if Cmd.ErrorCode <> 0 then Result:= Abs( Cmd.ErrorCode );
        if Cmd.ErrorDescription = '' then ErrorDescription:= '����� ���������� ��� �������� (������� "' + Cmd.Command + '")!' else ErrorDescription:= Cmd.ErrorDescription;
        Exit;
      end;
    else
      begin
        ErrorDescription:= '����������� ������';
        Exit;
      end;
    end;
  end;
end;



//==============================================================================
//================================ TScript =====================================

procedure TScript.Add(Command: TModemCommand);
var
  Cmd: PModemCommand;
begin
  New( Cmd );
  Cmd.Command:= Command.Command;
  Cmd.Answer:= Command.Answer;
  Cmd.ErrorCode:= Command.ErrorCode;
  Cmd.WaitInterval:= Command.WaitInterval;
  Cmd.ErrorDescription:= Command.ErrorDescription;
  inherited Add( Cmd );
end;

procedure TScript.Clear;
var
  i: Integer;

begin
  for i:= 0 to Count - 1 do
    Dispose( Get(i) );
  inherited;
end;

procedure TScript.OnByteReserved(B: Byte);
begin
  if not ( B in [10, 13] ) then ReservedAnswer:= ReservedAnswer + Chr( B );
  if Assigned( FExtOnByteReserved ) then FExtOnByteReserved( B );
end;

function TScript.Get( Index: Integer ): PModemCommand;
begin
  Result:= PModemCommand( Items[ Index ] );
end;

procedure TScript.Load( DataFile: TDataFile );
var
  Command: TModemCommand;
  i: Integer;
  Cou: Integer;

begin
  Clear;
  ID:= DataFile.ReadString;
  Name:= DataFile.ReadString;
  Cou:= DataFile.ReadWord;
  for i:= 0 to Cou - 1 do
  begin
    Command.Command:= DataFile.ReadString;
    Command.Answer:= DataFile.ReadString;
    Command.ErrorCode:= DataFile.ReadInteger;
    Command.WaitInterval:= DataFile.ReadWord;
    Command.ErrorDescription:= DataFile.ReadString;
    Add( Command );
  end;

end;

procedure TScript.Save( DataFile: TDataFile );
var
  i: Integer;
  Command: PModemCommand;

begin
  DataFile.WriteString( ID );
  DataFile.WriteString( Name );
  DataFile.WriteWord( Count );
  for i:= 0 to Count - 1 do
  begin
    Command:= Get(i);
    DataFile.WriteString( Command.Command );
    DataFile.WriteString( Command.Answer );
    DataFile.WriteInteger( Command.ErrorCode );
    DataFile.WriteWord( Command.WaitInterval );
    DataFile.WriteString( Command.ErrorDescription );
  end;
end;

//==============================================================================
//================================ TScriptList =================================


procedure TScriptList.Delete(Index: Integer);
begin
//  TScript( Items[ Index ] ).Free;
  inherited;
end;

function TScriptList.Get( IndexOrID: Variant ): TScript;
begin
  if VarType( IndexOrID ) = varString then
    Result:= Get( string( IndexOrID ) )
  else
    Result:= Get( Integer( IndexOrID ) );
end;

function TScriptList.Get( Index: Integer ): TScript;
begin
  Result:= TScript( Items[ Index ] );
end;

function TScriptList.Get( ID: string ): TScript;
var
  i: Integer;

begin
  for i:= 0 to Count - 1 do
  if Get( i ).ID = ID then
  begin
    Result:= Get( i );
    Exit;
  end;
  Result:= nil;
end;

procedure TScriptList.Clear;
var
  i: Integer;

begin
  inherited Clear;
end;


function TScriptList.LoadFromFile(Filename: string): string;
const
  SupportedVer = [1];

var
  DataFile: TDataFile;
  Ver: Integer;
  Scr: TScript;
  Cou: Integer;
  i: Integer;

begin
  Result:= '����������� ������';
  try
    DataFile:= TDataFile.Create( False, Filename, fmOpenRead );
  except
    Result:= '���� �� ������.';
    Exit;
  end;

  if DataFile.ReadString( Length( ScriptFileID ) ) <> ScriptFileID then
  begin
    Result:= '�������� ������ �����';
    Exit;
  end;

  Ver:= DataFile.ReadWord;
  if not ( Ver in SupportedVer ) then
  begin
    Result:= Format( '������ ����� (%d) �� ��������������', [Ver] );
    Exit;
  end;

  Cou:= DataFile.ReadWord;

  for i:= 0 to Cou - 1 do
  begin
    Scr:= TScript.Create;
    Scr.Load( DataFile );
    Add( Scr );
  end;

  DataFile.Free;

  Result:= '';
end;

procedure TScriptList.SaveToFile(Filename: string);
const
  WriteVersion = 1;

var
  DataFile: TDataFile;
  i: Integer;

begin
  DataFile:= TDataFile.Create( False, Filename, fmCreate );
  DataFile.WriteString( ScriptFileID, True );
  DataFile.WriteWord( WriteVersion );
  DataFile.WriteWord( Count );
  for i:= 0 to Count - 1 do
    TScript( Items[i] ).Save( DataFile );
  DataFile.Free;
end;





destructor TModemScriptEngine.Destroy;
begin
  ScriptList.Free;
  inherited;
end;

destructor TScriptList.Destroy;
begin
  inherited;
end;

destructor TScript.Destroy;
begin
  inherited;
end;


end.
