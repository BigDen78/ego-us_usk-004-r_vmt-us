unit DllDescriptorM;

interface

uses
  Windows;

{$DEFINE 2CAN}

{$IFDEF 2CAN}
const
    LibName = 'U2C.DLL';
{$ELSE}
const
    LibName = 'makp.dll';
{$ENDIF}

{const
    LibName = 'makp.dll';}

// ��������, ����� �������� dll. ���� ���������� 0, �� �������� ������
// �������� �� 0 �������� ������������ ������, ���� ������� �� ���������� � USB.

// �����: ��� 16 ����������, ���������  ��� No.0
//        ��� 31 ���������� - ��������� ��� No.15
//        � ��� 0 ���������� �������� ���� 16

//  function initlib: integer; cdecl; external 'makp.dll';
  function initlibM: integer; cdecl; external LibName name 'initlib';
  //function initlibM: integer; cdecl; external 'makp.dll' name 'initlib';

// ��������� ��� ���������� ������.

  procedure finishlibM; cdecl; external LibName name 'finishlib';
  //procedure finishlibM; cdecl; external 'makp.dll' name 'finishlib';

//       ptr - ����� �������� ������; amm - ����� ���� ��� ������
// ���������� 0, ���� ������ ��� �� ���������� ����� ��� ������;
// ��� ��������� ����� ���� (������� ���� ������), ���� ������ ������ �������

  function WriteToUSBM(ptr: Pointer; amm: Word; ID: Byte): Word; cdecl; external LibName name 'WriteToUSB';
  //function WriteToUSBM(ptr: Pointer; amm: Word; ID: Byte): Word; cdecl; external 'makp.dll' name 'WriteToUSB';

//       ptr - ����� ��������� ������; amm - ����� ����, ������� ����� ���������. ������ amm ����, ���
//                                           ������. ���������� ����� ����������� ����.

  function ReadFromUSBM(ptr: Pointer; amm, mode, ID: Byte): Byte; cdecl; external LibName name 'ReadFromUSB';
  //function ReadFromUSBM(ptr: Pointer; amm, mode, ID: Byte): Byte; cdecl; external 'makp.dll' name 'ReadFromUSB';

  function GUAKK: integer;

const
  pagesize = 1056; // ������ �������� flash
  max_canmsgsize = 504; // ����. ����� ����, ������� ����� �������� � � �������� ����� USB-CAN

//   �������� �������� �������, �������� ���������� �� ������� 1 - ��� / 0 - ���� / < 0 - ������

  function power_onM(ID: Byte): Integer; cdecl; external LibName name 'power_on';
  //function power_onM(ID: Byte): Integer; cdecl; external 'makp.dll' name 'power_on';

//   ��������� �������� �������.

  function power_offM(ID: Byte): Integer; cdecl; external LibName name 'power_off';
  //function power_offM(ID: Byte): Integer; cdecl; external 'makp.dll' name 'power_off';

// ������ quantity ������� ������, ������� � �������� � �������������
// ����������� (�������� � �������� �/� (pagenum), ����� �/� (chipnum)),
// � ����� ������������ ������� � ������ destbufptr.
//  1 <= quantity <= 62
//
//
  function readpagesM(destbufptr: Pointer; pagenum: Word; chipnum, quantity, ID: byte): Integer; cdecl; external LibName name 'readpages';
  //function readpagesM(destbufptr: Pointer; pagenum: Word; chipnum, quantity, ID: byte): Integer; cdecl; external 'makp.dll' name 'readpages';

// ���������� quantity ������� ������ �� ������ ������������ srcbufptr,
// ������� � �������� � ������������� ����������� (�������� � �������� �/� (pagenum), ����� �/� (chipnum)),
//  1 <= quantity <= 62
//  � ������ srcbuptr ������ 4 ����� �� ������������, �.�. ������ ���������,
// ������� � ����� +4

  function writepagesM(srcbufptr: Pointer; pagenum: Word; chipnum, quantity, ID: byte): Integer; cdecl; external LibName name 'writepages';
  //function writepagesM(srcbufptr: Pointer; pagenum: Word; chipnum, quantity, ID: byte): Integer; cdecl; external 'makp.dll' name 'writepages';

// ���������� ����� �/� ������������� �� �������. ���� ��� �� �������� �� ������ ���� ��������� 0

  function chipcntrM(ID: Byte): Integer; cdecl; external LibName name 'chipcntr';
  //function chipcntrM(ID: Byte): Integer; cdecl; external 'makp.dll' name 'chipcntr';

// ���������� ����� �/� ������������� �� �������. ���� ��� �� �������� �� ������ ���� ��������� 0

//  function getinfo(destbufptr: Pointer; mode: Word): Integer; cdecl; external 'av11.dll';

  function pwrswitchM(power, ID:byte):integer; cdecl; external LibName name 'pwrswitch';
  //function pwrswitchM(power, ID:byte):integer; cdecl; external 'makp.dll' name 'pwrswitch';
  function func0M(flag, ID:byte):integer; cdecl; external LibName name 'func0';
  //function func0M(flag, ID:byte):integer; cdecl; external 'makp.dll' name 'func0';

  function get_libverM: DWord; cdecl; external LibName name 'get_libver';   // ����������  ������ ����������
  //function get_libverM: DWord; cdecl; external 'makp.dll' name 'get_libver';   // ����������  ������ ����������

  function get_driververM( Version: PDWord; ID: byte ): Integer; cdecl; external LibName name 'get_driverver';
  //function get_driververM( Version: PDWord; ID: byte ): Integer; cdecl; external 'makp.dll' name 'get_driverver';
// ������  ������ ��������
// ptr - ��������� �� ����� ������������
// i   - ����� ��� (0..15)
// � ������ ������ ���������� 0 � � ����� ������������ ���������� 4 �����,
// ����� ���������� -1
//

  function get_versionM( PBuffer: Pointer;  ID: byte ): Integer; cdecl; external LibName name 'get_version';
  //function get_versionM( PBuffer: Pointer;  ID: byte ): Integer; cdecl; external 'makp.dll' name 'get_version';

(*
typedef struct _coords

    UCHAR chip: Byte;
    USHORT page: Word;

//-------------------------------------------------------------------
__declspec (dllexport) void  get_coord(DWORD apage, _pcoords coordptr)
{
    coordptr->page = apage/chipammount;
    coordptr->chip = (UCHAR)fmod(apage,chipammount);
}
//-----------------------------------------------------------------------------
__declspec (dllexport) DWORD get_apage(struct _coords coords)
{
  return coords.page * chipammount + coords.chip;
}
//
*)

function GetU2CSerialNum: integer;

implementation

function GUAKK: integer;
begin
  result:= 0;
end;

function GetU2CSerialNum: integer;
begin
  result:= 0;
end; 

end.
