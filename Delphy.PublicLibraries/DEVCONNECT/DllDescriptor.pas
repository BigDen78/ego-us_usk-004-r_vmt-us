unit DllDescriptor;


interface

uses
  Windows, SysUtils;

type
  DW_array = array of DWord;


const
  akpdll = 'av11.dll';

  function get_rcvbufstate: WORD; cdecl; external akpdll;
  function get_rcvstate: LongInt; cdecl; external akpdll;
  function get_trstate: LongInt; cdecl; external akpdll;

  function rcv_resume: LongInt; cdecl; external akpdll;
  function tr_resume: LongInt; cdecl; external akpdll;

  function writercnt: pointer; cdecl; external akpdll;


// ��������, ����� �������� dll. ���� ���������� 0, �� �������� ������
// �������� �� 0 �������� ������������ ������, ���� ������� �� ���������� � USB.

  function initlib: ShortInt; cdecl; external akpdll;

// ��������� ��� ���������� ������.

  procedure finishlib; cdecl; external akpdll;

// ������ ��� ���
// ���������� ���������/���������� ������ ����� ������ ON/OFF CAN-�������
// num = 0 - ���������, ��������� �������� - ��������
// ��� ������� ���������� ���������� �������� 1
// ������������� �������� � 0 ��������������� �� ������


  function pwrswitch(Mode: Byte): ShortInt; cdecl; external akpdll;

//       ptr - ����� �������� ������; amm - ����� ���� ��� ������
// ���������� 0, ���� ������ ��� �� ���������� ����� ��� ������;
// ��� ��������� ����� ���� (������� ���� ������), ���� ������ ������ �������

  function WriteToUSB(ptr: Pointer; amm: Word): Word; cdecl; external akpdll;

//       ptr - ����� ��������� ������; amm - ����� ����, ������� ����� ���������. ������ amm ����, ���
//                                           ������. ���������� ����� ����������� ����.
// mode = 0 - ������ ������� ��������� ( ReadFromUSB ������ amm ��� 0)
// mode <> 0 - ������ ������� ����, �� �� ����� ������������ ( ReadFromUSB ������ <=amm )

  function ReadFromUSB(ptr: Pointer; amm, mode: Byte): Byte; cdecl; external akpdll;

implementation





end.
