unit DllDescriptorDynamic;

interface
uses Windows, Dialogs, SysUtils, PublicFunc, Forms;

const NumOfCANs=4;//initial number of BUMs(CANs)
(*
//{$DEFINE 2CAN}
{$DEFINE 4CAN}

{$IFDEF 4CAN}
const
    LibName = 'u2x2c_bc.DLL';
{$ELSE}
{$IFDEF 2CAN}
const
    LibName = 'U2C.DLL';
{$ELSE}
const
    LibName = 'makp.dll';
{$ENDIF}
{$ENDIF}
*)
type
   // ��������� ������� ��� �����
   Tinitlib = function: DWord;{Integer;} cdecl;
   Tfinishlib = procedure; cdecl;
   TWriteToUSB = function(ptr: Pointer; amm: Word; ID: Byte): Word; cdecl;
   TReadFromUSB = function(ptr: Pointer; amm, mode, ID: Byte): Byte; cdecl;
   Tpower_on = function(ID: Byte): Integer; cdecl;
   Tpower_off = function(ID: Byte): Integer; cdecl;
   Treadpages = function(destbufptr: Pointer; pagenum: Word; chipnum, quantity, ID: byte): Integer; cdecl;
   Twritepages = function(srcbufptr: Pointer; pagenum: Word; chipnum, quantity, ID: byte): Integer; cdecl;
   Tchipcntr = function(ID: Byte): Integer; cdecl;
   Tpwrswitch = function(power, ID:byte):integer; cdecl;
   Tfunc0 = function(flag, ID:byte):integer; cdecl;
   Tget_libver = function: DWord; cdecl;
   Tget_driverver = function( Version: PDWord; ID: byte ): Integer; cdecl;
   Tget_version = function( PBuffer: Pointer;  ID: byte ): Integer; cdecl;
   TUAKK = function: integer;  cdecl;
   TGet_sn = function: integer;  cdecl;
   Tget_devicenum = function( I:byte): Integer; cdecl;


function initlibM: integer;
procedure finishlibM;
function WriteToUSBM(ptr: Pointer; amm: Word; ID: Byte): Word;
function ReadFromUSBM(ptr: Pointer; amm, mode, ID: Byte): Byte;
function pwrswitchM(power, ID:byte):integer;
function GUAKK: integer;
function GetVersion( PBuffer: Pointer;  ID: byte ): integer;//U2C(ID) version
//function GetU2CSerialNum: integer; overload;
function GetU2CSerialNum( I:byte): Integer; //overload;

procedure SetCANLib(N:integer); // set CAN dll name

{  function initlib: integer; cdecl; external LibName;
  procedure finishlib; cdecl; external LibName;
  function WriteToUSB(ptr: Pointer; amm: Word; ID: Byte): Word; cdecl; external LibName;
  function ReadFromUSB(ptr: Pointer; amm, mode, ID: Byte): Byte; cdecl; external LibName;
  function power_on: Integer; cdecl; external LibName;
  function power_off: Integer; cdecl; external LibName;
   function readpages(destbufptr: Pointer; pagenum: Word; chipnum, quantity: byte): Integer; cdecl; external LibName;
  function writepages(srcbufptr: Pointer; pagenum: Word; chipnum, quantity: byte): Integer; cdecl; external LibName;
 function chipcntr: Integer; cdecl; external LibName;
   function getinfo(destbufptr: Pointer; mode: Word): Integer; cdecl; external LibName;
  function pwrswitch(power, ID:byte):integer; cdecl; external LibName;
  function func0(flag:byte):integer; cdecl; external LibName;
   function get_version( PBuffer: Pointer;  ID: byte ): Integer; cdecl;external LibName;
}


implementation
var
  FUSBDLLHandle: THandle; // ����� �� ��� �����
  str: string;

  // ������� �� DLL �����
   initlib: Tinitlib;
   finishlib: Tfinishlib;
   WriteToUSB: TWriteToUSB;
   ReadFromUSB: TReadFromUSB;
   power_on: Tpower_on;
   power_off: Tpower_off;
   readpages: Treadpages;
   writepages: Twritepages;
   chipcntr: Tchipcntr;
   pwrswitch: Tpwrswitch;
   func0: Tfunc0;
   get_libver: Tget_libver;
   get_driverver: Tget_driverver;
   get_version: Tget_version;
   get_devicenum: Tget_devicenum;

   uakk: TUAKK;
   Get_sn: TGet_sn;
   NCAN:integer;
   LibName:string;

procedure SetCANLib(N:integer);
begin
   NCAN:=N;
   if N=4 then LibName:='u2x2c_bc.DLL'
   else if N=2 then LibName:='U2C.DLL'//'u2x2c_bc.DLL'//
        else begin LibName := 'makp.dll'; NCAN:=1 end;
end;

function initlibM: integer;
begin
  if Assigned(initlib) then
    result:= initlib;
end;

procedure finishlibM;
begin
  if Assigned(finishlib) then
    finishlib;
end;

function WriteToUSBM(ptr: Pointer; amm: Word; ID: Byte): Word;
begin
  if Assigned(WriteToUSB) then
    result:= WriteToUSB(ptr, amm, ID);
end;

function ReadFromUSBM(ptr: Pointer; amm, mode, ID: Byte): Byte;
begin
  if Assigned(ReadFromUSB) then
    result:= ReadFromUSB(ptr, amm, mode, ID);
end;

function pwrswitchM(power, ID:byte):integer;
begin
  if Assigned(pwrswitch) then
    result:= pwrswitch(power, ID);
end;

function GUAKK: integer;
begin
  if Assigned(uakk) then
    result:= uakk
  else
    result:= 0;
end;

function GetVersion( PBuffer: Pointer;  ID: byte ): integer;
begin
  if Assigned(get_version) then
    result:= get_version(PBuffer , ID)
  else
    result:= 0;
end;

{function GetU2CSerialNum: integer; overload;
begin
  if Assigned(Get_sn) then
    result:= Get_sn
  else
    result:= 0;
end;
}
function GetU2CSerialNum(I:byte): integer; overload;
begin
  if NCAN=4 then
    if Assigned(get_devicenum) then
      result:= get_devicenum(I)
    else
      result:= 0
  else
    if Assigned(Get_sn) then
      result:= Get_sn
    else
      result:= 0;
end;

initialization

  SetCANLib(NumOfCANs);

  initlib:= nil;
  finishlib:= nil;
  WriteToUSB:= nil;
  ReadFromUSB:= nil;
  power_on:= nil;
  power_off:= nil;
  readpages:= nil;
  writepages:= nil;
  chipcntr:= nil;
  pwrswitch:= nil;
  func0:= nil;
  get_libver:= nil;
  get_driverver:= nil;
  get_version:= nil;
  uakk:= nil;
  Get_sn:= nil;
  get_devicenum:=nil;

  str:=Format('%s%s',[ExtractFilePath(Application.ExeName), LibName]);

  FUSBDLLHandle:= loadLibrary ( PWideChar(str) );
  if FUSBDLLHandle <> 0 then
    begin
    if NCAN=4 then begin
      @initlib:= getProcAddress ( FUSBDLLHandle, '@initlib$qv' );
      @finishlib:= getProcAddress ( FUSBDLLHandle, '@finishlib$qv' );
      @WriteToUSB:= getProcAddress ( FUSBDLLHandle, '@WriteToUSB$qpucusuc' );
      @ReadFromUSB:= getProcAddress ( FUSBDLLHandle, '@ReadFromUSB$qpucucucuc' );
      @pwrswitch:= getProcAddress ( FUSBDLLHandle, '@pwrswitch$qucuc' );
      @func0:= getProcAddress ( FUSBDLLHandle, '@func0$qucuc' );
      @get_libver:= getProcAddress ( FUSBDLLHandle, '@get_libver$qv' );
      @get_driverver:= getProcAddress ( FUSBDLLHandle, '@get_driverver$qpuc' );
      @get_version:= getProcAddress ( FUSBDLLHandle, '@get_version$qpucuc' );
      @uakk:= getProcAddress ( FUSBDLLHandle, '@uakk$qv' );
      @get_devicenum:= getProcAddress ( FUSBDLLHandle, '@get_devicenum$quc' );
    end
    else begin
      @initlib:= getProcAddress ( FUSBDLLHandle, 'initlib' );
      @finishlib:= getProcAddress ( FUSBDLLHandle, 'finishlib' );
      @WriteToUSB:= getProcAddress ( FUSBDLLHandle, 'WriteToUSB' );
      @ReadFromUSB:= getProcAddress ( FUSBDLLHandle, 'ReadFromUSB' );
      @power_on:= getProcAddress ( FUSBDLLHandle, 'power_on' );
      @power_off:= getProcAddress ( FUSBDLLHandle, 'power_off' );
      @readpages:= getProcAddress ( FUSBDLLHandle, 'readpages' );
      @writepages:= getProcAddress ( FUSBDLLHandle, 'writepages' );
      @chipcntr:= getProcAddress ( FUSBDLLHandle, 'chipcntr' );
      @pwrswitch:= getProcAddress ( FUSBDLLHandle, 'pwrswitch' );
      @func0:= getProcAddress ( FUSBDLLHandle, 'func0' );
      @get_libver:= getProcAddress ( FUSBDLLHandle, 'get_libver' );
      @get_driverver:= getProcAddress ( FUSBDLLHandle, 'get_driverver' );
      @get_version:= getProcAddress ( FUSBDLLHandle, 'get_version' );
      @uakk:= getProcAddress ( FUSBDLLHandle, 'uakk' );
      @Get_sn:= getProcAddress ( FUSBDLLHandle, 'get_devicenum' );
    end;
  end;

finalization
  if FUSBDLLHandle <> 0 then
    begin
      {initlib:=nil;
      finishlib:=nil;
      WriteToUSB:=nil;
      ReadFromUSB:=nil;
      pwrswitch:=nil;
      uakk:= nil;}
      initlib:= nil;
      finishlib:= nil;
      WriteToUSB:= nil;
      ReadFromUSB:= nil;
      power_on:= nil;
      power_off:= nil;
      readpages:= nil;
      writepages:= nil;
      chipcntr:= nil;
      pwrswitch:= nil;
      func0:= nil;
      get_libver:= nil;
      get_driverver:= nil;
      get_version:= nil;
      uakk:= nil;
      Get_sn:= nil;
      get_devicenum:=nil;
      FreeLibrary ( FUSBDLLHandle );
    end;

end.
