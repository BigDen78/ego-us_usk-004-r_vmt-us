unit DllDescriptorEmul;

interface

uses
   Windows, Dialogs, SysUtils, PublicFunc, Forms;

{$DEFINE 2CAN}

const
{$IFDEF 2CAN}
    USBLibName = 'U2C.DLL';
{$ELSE}
    USBLibName = 'makp.dll';
{$ENDIF}
    EmulLibName = 'BUMEmul.dll';
    pagesize = 1056; // ������ �������� flash
    max_canmsgsize = 504; // ����. ����� ����, ������� ����� �������� � � �������� ����� USB-CAN
    CANBlockLength = 10; // ���������� ���� � ����� CAN-�������
    MaxDEWaiting = 100;
    TimeToWaite = 4000;

type

   // ��������� ������� ��� �����
   Tinitlib = function: Integer; cdecl;
   Tfinishlib = procedure; cdecl;
   TWriteToUSB = function(ptr: Pointer; amm: Word; ID: Byte): Word; cdecl;
   TReadFromUSB = function(ptr: Pointer; amm, mode, ID: Byte): Byte; cdecl;
   Tpower_on = function(ID: Byte): Integer; cdecl;
   Tpower_off = function(ID: Byte): Integer; cdecl;
   Treadpages = function(destbufptr: Pointer; pagenum: Word; chipnum, quantity, ID: byte): Integer; cdecl;
   Twritepages = function(srcbufptr: Pointer; pagenum: Word; chipnum, quantity, ID: byte): Integer; cdecl;
   Tchipcntr = function(ID: Byte): Integer; cdecl;
   Tpwrswitch = function(power, ID:byte):integer; cdecl;
   Tfunc0 = function(flag, ID:byte):integer; cdecl;
   Tget_libver = function: DWord; cdecl;
   Tget_driverver = function( Version: PDWord; ID: byte ): Integer; cdecl;
   Tget_version = function( PBuffer: Pointer;  ID: byte ): Integer; cdecl;

   // ��������� ������� ��� ��������� ���
   TInitModule = procedure; cdecl;
   TCreateEmulator = function: Pointer; cdecl;
   TWriteToEmulator = function(pHandle: Pointer; ptr: Pointer; count: Word): Integer; cdecl;
   TReadFromEmulator = function(pHandle: Pointer; ptr: Pointer; count: Word): Integer; cdecl;
   TCloseEmulator = procedure(pHandle: Pointer); cdecl;
   TCloseModule = procedure; cdecl;

   // � ������� ����� ����������� ���������� � ��� ����� ���� ��������
   TisRealBUM = array[0..255] of Boolean;

   // ����� �������� ������� - ����� ��� (��� ID), �������� - ����� ����� � CAN-������� (�� 1 �� CANBlockLength) ���������� �� �����
   TByteNumReceived = array[0..255] of Integer;

   // ���������, � ������� ���� ������� ���-�� ��������� ���� DE � �������� �� ��� DB
   TDEDBCount = record
    FirstDETime: Int64;
    DECount: Integer; // �������� ��� � ������� �������� DB
    DBCount: Integer; // �������� ���, ������� DE �� ���
   end;

   // ������ ������� - ����� ����� (��� ID). ������� ������� - ���������, �������� ���������� ������� DE � DB
   TDEDBMas = array[0..255] of TDEDBCount;

   TEmulHandles = array[0..255] of Pointer;

   TDLLManager = class
   private
     // ������ �� ����������
     FUSBDLLHandle: THandle; // ����� �� ��� �����
     FEmulatorDLLHandle: THandle; // ����� �� ��� ���������� ���

     // ������� �� DLL �����
     initlib: Tinitlib;
     finishlib: Tfinishlib;
     WriteToUSB: TWriteToUSB;
     ReadFromUSB: TReadFromUSB;
     power_on: Tpower_on;
     power_off: Tpower_off;
     readpages: Treadpages;
     writepages: Twritepages;
     chipcntr: Tchipcntr;
     pwrswitch: Tpwrswitch;
     func0: Tfunc0;
     get_libver: Tget_libver;
     get_driverver: Tget_driverver;
     get_version: Tget_version;

     // ������� ���������
     InitModule: TInitModule;
     CreateEmulator: TCreateEmulator;
     WriteToEmulator: TWriteToEmulator;
     ReadFromEmulator: TReadFromEmulator;
     CloseEmulator: TCloseEmulator;
     CloseModule: TCloseModule;

     // ������ ������� �� ���������
     EmulHandles: TEmulHandles;

     // �������� ��������� ������ � ������������� �������
     isRealBUM: TisRealBUM; // ������, ������������ �������� �� ����� �� �� ID'�
     BUMStatusAprove: TisRealBUM; // ������������� ������� ����� (��������� ��� ���)

     // ��� ������������� �����
     ByteNumReceived: TByteNumReceived;
     DEDBMas: TDEDBMas; // ������, �������� ���������� ������������/�������� DE/DB �� ������� �����
   protected
   public
     constructor Create;
     destructor Destroy; override;
     function MangedInitLib: Integer;
     procedure Managedfinishlib;
     function ManagedPWRswitch(power, ID:byte): integer;
     function ManagedWriteToUSB(ptr: Pointer; amm: Word; ID: Byte): Word;
     function ManagedReadFromUSB(ptr: Pointer; amm: Word; mode, ID: Byte): Byte;
   end;

// ���������� ����������� ������� ������ ����� �������� TDevice
function initlibM: integer;
procedure finishlibM;
function WriteToUSBM(ptr: Pointer; amm: Word; ID: Byte): Word;
function ReadFromUSBM(ptr: Pointer; amm, mode, ID: Byte): Byte;
function pwrswitchM(power, ID:byte):integer;
function GetU2CSerialNum: integer;
function GUAKK: integer;

var
   DLLManager: TDLLManager;

implementation

function initlibM: integer;
begin
  if Assigned(DLLManager) then
    result:=DLLManager.MangedInitLib
  else
    result:=0;
end;

procedure finishlibM;
begin
  if Assigned(DLLManager) then
    DLLManager.Managedfinishlib;
end;

function WriteToUSBM(ptr: Pointer; amm: Word; ID: Byte): Word;
begin
  if Assigned(DLLManager) then
    result:=DLLManager.ManagedWriteToUSB(ptr, amm, ID)
  else
    result:=0;
end;

function ReadFromUSBM(ptr: Pointer; amm, mode, ID: Byte): Byte;
begin
  if Assigned(DLLManager) then
    result:=DLLManager.ManagedReadFromUSB(ptr, amm, mode, ID)
  else
    result:=0;
end;

function pwrswitchM(power, ID:byte):integer;
begin
  if Assigned(DLLManager) then
    result:=DLLManager.ManagedPWRswitch(power, ID)
  else
    result:=0;
end;

function GetU2CSerialNum: integer;
begin
  result:= 0;
end;

function GUAKK: integer;
begin
  result:= 0;
end;

{ TDLLManager }

constructor TDLLManager.Create;
var
  str: string;
  S1: PAnsiChar;
  Err: Integer;
begin
  // � ������ ������� ��� ��� ����� ���������, �� �� ������ �� �����������
  FillChar(isRealBUM, 256, true);

  // ������ ���� ����� ���������� �������� �� ��������������
  FillChar(BUMStatusAprove, 256, false);
  FillChar(ByteNumReceived, SizeOf(TByteNumReceived), 0);
  FillChar(DEDBMas, SizeOf(TDEDBMas), 0);
  FillChar(EmulHandles, SizeOf(EmulHandles), 0);
  FUSBDLLHandle:=0;
  FEmulatorDLLHandle:=0;
  // ����������� DLL �����
  str:=Format('%s%s',[ExtractFilePath(Application.ExeName), USBLibName]);
  //FUSBDLLHandle:= loadLibrary ( PAnsiChar(str) );
  FUSBDLLHandle:= loadLibrary ( PWideChar(str) );
  if FUSBDLLHandle <> 0 then
    begin
      @initlib:= getProcAddress ( FUSBDLLHandle, 'initlib' );
      @finishlib:= getProcAddress ( FUSBDLLHandle, 'finishlib' );
      @WriteToUSB:= getProcAddress ( FUSBDLLHandle, 'WriteToUSB' );
      @ReadFromUSB:= getProcAddress ( FUSBDLLHandle, 'ReadFromUSB' );
      @power_on:= getProcAddress ( FUSBDLLHandle, 'power_on' );
      @power_off:= getProcAddress ( FUSBDLLHandle, 'power_off' );
      @readpages:= getProcAddress ( FUSBDLLHandle, 'readpages' );
      @writepages:= getProcAddress ( FUSBDLLHandle, 'writepages' );
      @chipcntr:= getProcAddress ( FUSBDLLHandle, 'chipcntr' );
      @pwrswitch:= getProcAddress ( FUSBDLLHandle, 'pwrswitch' );
      @func0:= getProcAddress ( FUSBDLLHandle, 'func0' );
      @get_libver:= getProcAddress ( FUSBDLLHandle, 'get_libver' );
      @get_driverver:= getProcAddress ( FUSBDLLHandle, 'get_driverver' );
      @get_version:= getProcAddress ( FUSBDLLHandle, 'get_version' );
    end
  else
    begin
      Err:=GetLastError;
      ShowMessage(Format('Error: DLL %s not found. Path: %s. H: %d. LastErr: %d', [USBLibName, Str, FUSBDLLHandle, Err]));
    end;

  // ����������� DLL ���������
  str:=Format('%s%s',[ExtractFilePath(Application.ExeName), EmulLibName]);
  //FEmulatorDLLHandle:= loadLibrary ( PAnsiChar(str) );
  FEmulatorDLLHandle:= loadLibrary ( PWideChar(str) );
  if FEmulatorDLLHandle <> 0 then
    begin
      @InitModule:= getProcAddress ( FEmulatorDLLHandle, 'InitModule' );
      @CreateEmulator:= getProcAddress ( FEmulatorDLLHandle, 'CreateEmulator' );
      @WriteToEmulator:= getProcAddress ( FEmulatorDLLHandle, 'WriteToEmulator' );
      @ReadFromEmulator:= getProcAddress ( FEmulatorDLLHandle, 'ReadFromEmulator' );
      @CloseEmulator:= getProcAddress ( FEmulatorDLLHandle, 'CloseEmulator' );
      @CloseModule:= getProcAddress ( FEmulatorDLLHandle, 'CloseModule' );
    end
  //else ShowMessage(Format('Error: DLL %s not found', [EmulLibName]));
  else
    begin
      Err:=GetLastError;
      ShowMessage(Format('Error: DLL %s not found. Path: %s. H: %d. LastErr: %d', [EmulLibName, Str, FEmulatorDLLHandle, Err]));
      //ShowMessage(Format('Error: DLL %s not found. Path: %s. Err: %d', [USBLibName, Str, FEmulatorDLLHandle]));
    end;
  if Assigned(InitModule) then InitModule;
end;

destructor TDLLManager.Destroy;
var
  i: Integer;
begin
  for i:=0 to 255 do
    if Assigned(EmulHandles[i]) then CloseEmulator(EmulHandles[i]);
  if Assigned(CloseModule) then CloseModule;
  if FUSBDLLHandle <> 0 then
    begin
      initlib:=nil;
      finishlib:=nil;
      WriteToUSB:=nil;
      ReadFromUSB:=nil;
      pwrswitch:=nil;
      FreeLibrary ( FUSBDLLHandle );
    end;
  if FEmulatorDLLHandle <> 0 then
    begin
      InitModule:=nil;
      CreateEmulator:=nil;
      WriteToEmulator:=nil;
      ReadFromEmulator:=nil;
      CloseEmulator:=nil;
      CloseModule:=nil;
      FreeLibrary ( FEmulatorDLLHandle );
    end;
  inherited;
end;

procedure TDLLManager.Managedfinishlib;
begin
  if Assigned(finishlib) then finishlib;
end;

function TDLLManager.ManagedPWRswitch(power, ID: byte): integer;
begin
  result:=pwrswitch(power, ID);
end;

function TDLLManager.ManagedReadFromUSB(ptr: Pointer; amm: Word; mode,
  ID: Byte): Byte;
var
  RB: Byte;
begin
  // ���� ���� ��������, �� �������� �����, ���� ���, �� �������� ������ �� ���������
  result:=0;
//  if ID=0 then Exit;
  if isRealBUM[ID] then
    begin
      //if (not BUMStatusAprove[ID]) then Sleep(100);
      result:=ReadFromUSB(ptr, amm, mode, ID);
      //ShowMessage(IntToStr(PBYTE(ptr)^));
    end
  else
     begin
       // ����� ������ �� ���������
       if (Assigned(EmulHandles[ID]) and Assigned(ReadFromEmulator)) then
         result:= ReadFromEmulator(EmulHandles[ID], ptr, amm)
       else
         result:=0;
     end;

  // ������������� ����� ����, ���� � ������ ��� �� ����������� ������
  if ((not BUMStatusAprove[ID]) and (Result>0)) then
    begin
      RB:=PBYTE(ptr)^;
      Inc(ByteNumReceived[ID]);
      if ByteNumReceived[ID] = CANBlockLength then ByteNumReceived[ID]:=0;
      // ����������� ������ ���� CAN-�������
      if ByteNumReceived[ID] = 1 then if RB = $DB then Inc(DEDBMas[ID].DBCount);
    end;
end;

function TDLLManager.ManagedWriteToUSB(ptr: Pointer; amm: Word;
  ID: Byte): Word;
var
  Bl: TBlock;
  B: Byte;
begin
  // ���� ���� ��������, �� �������� �����, ���� ���, �� ������ ������ ���������
  result:=0;
 // if ID=0 then Exit;
  if isRealBUM[ID] then
    begin
      //if (not BUMStatusAprove[ID]) then Sleep(200);
      result:=WriteToUSB(ptr, amm, ID);
    end
  else
     begin
       // ������ ������ � ��������
       if (Assigned(EmulHandles[ID]) and Assigned(ReadFromEmulator)) then
         result:= WriteToEmulator(EmulHandles[ID], ptr, amm)
       else
         result:=0;
       //result:= amm;
     end;

  // ������������� ����� ���, � ��� ������ ���� ������ ���� �� ����������
  //Sleep(100);
  if ((not BUMStatusAprove[ID]) and (result=amm)) then
    begin
      CopyMemory(@Bl, ptr, amm);
      // ����������� ������ ���� CAN-�������
      if Bl[1] = $DE then
        begin
          if DEDBMas[ID].DECount = 0 then
            DEDBMas[ID].FirstDETime:=GetTickCount;
          if ((GetTickCount - DEDBMas[ID].FirstDETime) > TimeToWaite ) then
            Inc(DEDBMas[ID].DECount, MaxDEWaiting)
          else
            Inc(DEDBMas[ID].DECount, 1);
        end;
      // �� ������ ����������, ��������� � DEDBMas ��������� ������� � ��� �������� ���� ��� ���
      if DEDBMas[ID].DBCount > 0 then BUMStatusAprove[ID]:=true; // ���� ������ ���� ���� DB, �� ������������ ������ ����� ��� ���������
      // ���� ���������� ��������� DE ������ ������� (MaxDEWaiting) � ��� ���� �� ������ �� ������ DB, �� ������ ���� ����������� � ������������ ���� ��� ������
      if (DEDBMas[ID].DECount > MaxDEWaiting) and (DEDBMas[ID].DBCount = 0) then
        begin
          isRealBUM[ID]:=false;
          BUMStatusAprove[ID]:=true;
          EmulHandles[ID]:=CreateEmulator;
        end;
    end;
  
end;

function TDLLManager.MangedInitLib: Integer;
var
  res: Integer;
begin
  // � ����� ������ �������� �����, �.�. �� ������ ����� �� ������� ����� ���� �������� � ���� �� �������� �����
  if Assigned(initlib) then
    begin
      res:= initlib;
      result:= res;
    end
  else result:=0;
end;

initialization
  DLLManager:=TDLLManager.Create;

finalization
  DLLManager.Free;
end.
