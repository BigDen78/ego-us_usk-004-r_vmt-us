unit InterfaceCAN;

interface
uses Windows;

type

  // ��������� ����������� ������.
  TRxTxBuffer = record                                                          // ��������� RX_TX_Buffer (��. ���. 20)
    FrameFormat: Word;                                                          // ������ = 0 - ����������� ����.
    StandartID: Word;                                                           // 11-������ �������������.
    ExtendedID: DWord;                                                          // 29-������ ������������� (�� ���-��).
    RemoteTransmitRequest: Word;                                                // RTR (?)
    DataLengthCode: Word;                                                       // ���������� ����������� ���� ������.
    DataBytes:array [0..7] of word;                                             // ����� ������.
  end;
  PRxTxBuffer=^TRxTxBuffer;

  // �������.
  TEventData = record
    RxTxBuffer: TRxTxBuffer;
    InterruptID: Byte;                                                          // ID ���������� ( 1 - ������ ����� )
  end;
  PEventData = ^TEventData;


function CANOpen(number:integer):word; cdecl; external 'wdmcan.dll';
procedure CANClose(number:integer); cdecl; external 'wdmcan.dll';
function SelectCAN(number:integer):integer; cdecl; external 'wdmcan.dll';
procedure chBaseAddress(Base:dword); cdecl; external 'wdmcan.dll';
function SetWorkMode(Mode:word):word; cdecl; external 'wdmcan.dll';
function GetWorkMode:word; cdecl; external 'wdmcan.dll';
procedure HardReset; cdecl; external 'wdmcan.dll';
procedure SetDriverMode(Mode:word); cdecl; external 'wdmcan.dll';
function SetCANSpeed(Speed:dword):word; cdecl; external 'wdmcan.dll';
function GetStatus:word; cdecl; external 'wdmcan.dll';
procedure SetInterruptSource(Source:word); cdecl; external 'wdmcan.dll';
function SetTxBuffer(Buffer: PRxTxBuffer):word; cdecl; external 'wdmcan.dll';
function GetRxBuffer(Buffer: PRxTxBuffer):word; cdecl; external 'wdmcan.dll';
procedure SetCommand(Command:word); cdecl; external 'wdmcan.dll';
function B_SetInputFilter(Filter1,Filter2:word):word; cdecl; external 'wdmcan.dll';
procedure SetCANReg(A1,A2:word); cdecl; external 'wdmcan.dll';
function GetCANReg(A1:word):word; cdecl; external 'wdmcan.dll';
procedure ResetRequest(A1:word); cdecl; external 'wdmcan.dll';
function TestResetRequest:word; cdecl; external 'wdmcan.dll';
procedure DefEvent(hEvent:THandle;fEventSet:boolean); cdecl; external 'wdmcan.dll';
function GetEventData(Buffer: PEventData):word; cdecl; external 'wdmcan.dll';
function P_SetRxErrorCounter( newRxErrorCounter: word ):word; cdecl; external 'wdmcan.dll'; 
function P_SetTxErrorCounter( newRxErrorCounter: word ):word; cdecl; external 'wdmcan.dll';
function P_GetRxMessageCounter: integer; cdecl; external 'wdmcan.dll';
//function GetConfig(Buffer:pTDeviceDescription):word; cdecl; external 'wdmcan.dll';







implementation

end.
