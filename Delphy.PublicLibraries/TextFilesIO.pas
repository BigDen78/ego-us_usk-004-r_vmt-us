unit TextFilesIO;


interface

uses PublicData, DateUtils;

      type
        TDecodeString  = function ( Snum : Integer; S : string ): Boolean of object;
        TCodeString    = function( Snum : Integer; var S: string ): Boolean of object;



      function LoadTextFile( FN: string; DecodeString: TDecodeString ): Byte;
      function SaveTextFile( FN: string; CodeString: TCodeString ): Byte;
      function DecodeLoadError(FN: String; Code: Byte) : String;

      // �������� �������� ��������� (Dt) �� ������ (S) (��������� ��������� ���������)
      procedure GetS(var S, Dt: string); overload;
      procedure GetS(var S: string; out Dt: Single); overload;
      procedure GetS(var S: string; out Dt: Integer); overload;
      procedure GetS(var S: String; out Dt: ShortInt); overload;
      procedure GetS(var S: string; out Dt: Byte); overload;
      procedure GetS(var S: string; out Dt: Word); overload;
      procedure GetS(var S: string; out Dt: TDateTime); overload;
      procedure GetS(var S: string; out Dt: Boolean); overload;

      // �������� �������� ��������� (Dt) � ������ (S)
      procedure AddS(var S : string; Dt: string); overload;
      procedure AddS(var S: string;  Dt: Single); overload;
      procedure AddS(var S: string;  Dt: Integer); overload;
      procedure AddS(var S: String; Dt: ShortInt); overload;
      procedure AddS(var S: string;  Dt: Byte); overload;
      procedure AddS(var S: string;  Dt: Word); overload;
      procedure AddS(var S: string;  Dt: TDateTime); overload;
      procedure AddS(var S: string;  Dt: Boolean); overload;


implementation

uses StrUtils, SysUtils, Math;

function LoadTextFile( FN: String; DecodeString: TDecodeString ) : Byte;
var
  F : Text;
  i : Integer;
  S : String;
begin
  {$I-}
  Result:=0;
  assignFile(F, FN);
  reset(F);
  If IOResult<>0 then
  begin
    Result:=1;
    Exit;
  end;
  i:=0;     {$I+}
  while not
  EOF(F) do
   begin
     readln(F, S);
     If S='' then continue;
     If not DecodeString( i, S ) then
     begin
       Result:=2;
       Exit;
     end;
     inc(i);
   end;
  close(F);
end;

function SaveTextFile( FN: String; CodeString: TCodeString ) : Byte;
var
  F : Text;
  i : Integer;
  S : String;
begin
  Result:=1;
  assignFile(F, FN);
  {$I-}
  rewrite(F);
  If IoResult<>0 then exit; {$I+}
  i:=0;
  while CodeString( i, S ) do
  begin
    WriteLn(F, S);
    inc( i );
  end;
  Close(F);
  Result:=0;
end;


function DecodeLoadError(FN: String; Code: Byte) : String;
begin
 case Code of
   1: Result:= '(E) Config file ' + FN + ' not found.';
   2: Result:= '(E) Config file ' + FN + ' is bad.';
   else Result:= '(E) Config file ' + FN + ': UNEXPECTED ERROR.';
 end;
end;

Procedure GetS(var S: String; var Dt: String);
var
 Ch : array[0..5] of Char;   // ����� ��������� ��������.
 P, P_: Integer;
 ChCount: Byte;
 i, j: Integer;

begin
  Dt:='';
  i:=1;
  if S='' then Exit;

  // ������ ������ �������� � ������.
  while S[i] = ' ' do
  begin
    inc(i);
    if i > length(S) then
    begin
      S:='';
      Exit;
    end;
  end;

  // ��������� �������, �������� �������.
  if S[i]='"' then
  begin
   Ch[0]:='"';
   ChCount:= 1;
   inc(i);
  end else
  begin
    Ch[0]:=' ';
    Ch[1]:=#9;
    ChCount:= 2;
  end;

  // ��������� ��������� ������ �������.
  P:= $FFFF;
  for j:= 0 to ChCount - 1 do
  begin
    P_:= PosEx(Ch[j], S, i);
    if P_ <> 0 then P:= Min(P, P_);
  end;

  // ������� ������� ������.
  if P=$FFFF then
  begin
    Dt:=Copy(S, i, 1000);
    S:='';
  end else
  begin
    Dt:=Copy(S, i, P-i);
    S:=Copy(S, P+1, length(S)-P);
  end;
end;

procedure GetS(var S: String; out Dt: Integer);
var
  DtS : String;
  E   : Integer;
begin
  GetS( S, DtS);
  Val(DtS, Dt, E);
end;

procedure GetS(var S: string; out Dt: Boolean);
var
  DtS: string;

begin
 GetS( S, DtS);
 Dt:= ( DtS = '1' ) or ( DtS = '��' ) or ( DtS = 'yes' );
end;

procedure GetS(var S: String; out Dt: ShortInt);
var
  DtS : String;
  E   : Integer;
begin
 GetS( S, DtS);
 Val(DtS, Dt, E);
end;

procedure GetS(var S: String; out Dt: Byte);
var
  DtS : String;
  E   : Integer;
begin
 GetS( S, DtS);
 Val(DtS, Dt, E);
end;

procedure GetS(var S: String; out Dt: Word);
var
  DtS : String;
  E   : Integer;
begin
 GetS( S, DtS);
 Val(DtS, Dt, E);
end;

procedure GetS(var S: String; out Dt: Single);
var
  DtS : String;
  E   : Integer;
begin
 GetS( S, DtS);
 Val(DtS, Dt, E);
end;

Procedure GetS(var S: String; out Dt: TDateTime); overload;
var
  DtS, Str : String;
  Y,
  M,
  D, H, N, Ss : Word;
begin
 try
  GetS(S, DtS);
  D:=StrToInt( Copy(DtS,1,2) );
  M:=StrToInt( Copy(DtS,4,2) );
  Y:=StrToInt( Copy(DtS,7,4) );
  H:= 0;
  N:= 0;
  Ss:= 0;
  if Length(DtS) > 10  then
    begin
      H:= StrToInt( Copy(DtS,12,2) );
      N:= StrToInt( Copy(DtS,15,2) );
      Ss:= StrToInt( Copy(DtS,18,2) );
    end;
  ////Dt:=EncodeDate(Y, M, D);
  Dt:=EncodeDateTime(Y, M, D, H, N, Ss, 0);
 except
 end;
end;

procedure AddS(var S: String; Dt: Single);
var
 SS : String;
begin
  SS:=Format('%.4f', [Dt]);
  AddS(S, SS);
end;

procedure AddS(var S: String; Dt: String);
begin
  If Pos(' ', Dt)<>0 then Dt:='"' + Dt + '"';
  While length(Dt)<15 do
    Dt:=Dt+' ';
  S:=S+Dt;
end;

procedure AddS(var S: String; Dt: Byte);
var
 SS : String;
begin
  SS:=IntToStr(Dt);
  AddS(S, SS);
end;

procedure AddS(var S: String; Dt: Word);
var
 SS : String;
begin
  SS:=IntToStr(Dt);
  AddS(S, SS);
end;

procedure AddS(var S: String; Dt: ShortInt);
var
 SS : String;
begin
  SS:=IntToStr(Dt);
  AddS(S, SS);
end;

procedure AddS(var S: String; Dt: Integer);
var
 SS : String;
begin
  SS:=IntToStr(Dt);
  AddS(S, SS);
end;

procedure AddS(var S: String;  Dt: TDateTime);
var
 SS : String;
begin
  //DateTimeToString(SS, 'dd.mm.yyyy hh:nn:ss', Dt);
  DateTimeToString(SS, 'dd.mm.yyyy_hh:nn:ss', Dt);
  AddS(S, SS);
end;

procedure AddS(var S: string;  Dt: Boolean); overload;
begin
  AddS( S, Ord( Dt ) );
end;

end.