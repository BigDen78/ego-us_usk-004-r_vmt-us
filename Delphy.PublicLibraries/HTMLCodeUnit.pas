unit HTMLCodeUnit;

interface

uses SysUtils, Classes, ConfigUnit, CfgTablesUnit, PublicFunc, Logging;

type

  TReplaceData = record
    Name: string;
    Value: array of string;
  end;
  PReplaceData = ^TReplaceData;


  THTMLcode = class
    private
      Col1 : Integer; // ����� � ������� ReplaceData ������� ������� ������� (��� ������)
      function CodeString(var S: String; TableRow: Integer): Integer;
    public
      ReplaceData: array of TReplaceData;
      constructor Create;
      function Add(Name_: string; ValueCount: Integer = 0): PReplaceData;
      function AddTable(ColID: string; ColCount: Integer): Integer;
      procedure AddCell(ColNum: Integer; CellValue: string);
      function AddString(Name_, Value_: string): PReplaceData;
      function AddNum(Name_: string; Value_: Longint): PReplaceData;
      procedure Clear;
      function Code(SourceFile, DestFile: String): Boolean;
  end;


implementation


constructor THTMLcode.Create;
begin
  inherited Create;
  Clear;
end;

function THTMLcode.AddTable(ColID: string; ColCount: Integer): Integer;
// ��������� ColCount �������� ������� � ��������������� ColID + i (��� i - ����� �������).
// ���������� ����� ������� �������.
var
 i : Integer;
begin
  Col1:= High( ReplaceData ) + 1;
  SetLength(ReplaceData, Col1 + ColCount);
  for i:=0 to ColCount-1 do
  begin
    if ColCount = 1 then
      ReplaceData[Col1 + i].Name:= ColID
    else
      ReplaceData[Col1 + i].Name:= ColID + intTostr( i + 1 );  // ����� ����� ��������.
    SetLength( ReplaceData[Col1 + i].Value, 0 );               // ����� ���� 0.
  end;
  Result:= Col1;
end;

procedure THTMLcode.AddCell(ColNum: Integer; CellValue: string);
var
 Row: Integer;
begin
  with ReplaceData[ ColNum + Col1 ] do
  begin
    Row:= High( Value ) + 1;
    SetLength( Value, Row + 1);
    Value[ Row ]:= CellValue;
  end;
end;

function THTMLcode.Add(Name_: string; ValueCount: Integer = 0): PReplaceData;
var
 i : Integer;
begin
  i:= High( ReplaceData ) + 1;
  SetLength(ReplaceData, i + 1);
  ReplaceData[i].Name:= Name_;
  SetLength( ReplaceData[i].Value, ValueCount);
  Result:= @ReplaceData[i];
end;

function THTMLcode.AddString(Name_, Value_: string): PReplaceData;
var
 R: PReplaceData;
begin
  R:= Add( Name_ );
  with R^ do
  begin
    SetLength( Value, 1);
    Value[0]:= Value_;
  end;
  Result:= R;
end;

function THTMLcode.AddNum(Name_: string; Value_: Longint): PReplaceData;
begin
 Result:= AddString(Name_, IntToStr( Value_ ));
end;

procedure THTMLcode.Clear;
begin
  SetLength(ReplaceData, 0);
end;

function THTMLcode.Code(SourceFile, DestFile: String): Boolean;
const
 TblStartID = '<!--REPEAT';
 TblEndID =   '<!--/REPEAT';

var
 InF : Text;
 OutF: Text;
 S:    string;
 i:    Integer;
 Buff: TStringList;
 Buff2: TStringList;
 Row  : Word;
 NoRow: Boolean;

begin
  Buff:= TStringList.Create;
  Buff2:= TStringList.Create;
  
  Result:= False; {$I-}
  AssignFile(InF, SourceFile);
  Reset(InF);
  If IOResult<>0 then
  begin
    Logger.Add(TLng.Caption('(E) ���� ������� HTML (') + SourceFile + TLng.Caption(') �� ������.'));
    Exit;
  end;
  AssignFile(OutF, DestFile);
  Rewrite(OutF);
  If IOResult<>0 then
  begin
    Logger.Add(TLng.Caption('(E) ���������� ������� ���� HTML (') + DestFile + ').');
    Exit;
  end;

//   WrLog(Lng.Caption['(i) ������� ���� HTML ('] + DestFile + ')...');
  while not EOF(InF) do
  begin
    ReadLn(InF, S);
    If Pos(TblStartID, S) <> 0 then
    begin
      // ���� �������.
      // ��������� ��� ���� ������� � �����.
      Buff.Clear;
      ReadLn(InF, S);
      while Pos(TblEndID, S) = 0 do
      begin
        Buff.Add(S);
        ReadLn(InF, S);
      end;
      Row:= 0;
      NoRow:= False;

      // �������� ������� � ���� X ���.
      repeat
        // �������� ������ �� ������ ����� �� �������.
        Buff2.Clear;
        for i:=0 to Buff.Count - 1 do
        begin
          S:= Buff[i];
          case CodeString(S, Row) of
            -1: begin
                  Buff.Free;
                  Exit;
                end;
            2:  NoRow:= True;
          end;
          Buff2.Add( S );
        end;

        // ���� ������ Row �� ������, ������� �� ����� (��� ������ �����������).
        if NoRow then Break;

        // ������� ������ �� ������� ������ � ����.
        for i:=0 to Buff2.Count - 1 do
          WriteLn(OutF, Buff2[i] );

        Inc( Row );
      until False;

    end else
    begin
      // �� �������.
      If CodeString(S, 0) = -1 then Exit;
      WriteLn( OutF, S);
    end;
  end;

  CloseFile(inF);
  CloseFile(outF);

  Buff.Free;
  Buff2.Free;

//  WrLog(Lng.Caption['(i) ���� HTML ������� ������.']);
end;

function THTMLCode.CodeString(var S: String; TableRow: Integer): Integer;
{ Result = 1   - �� ��������� ������ �������;
  Result = 2   - ������ � ����� ������� (TableRow) �� ���� ������ (��� ������ ��� ���� ��������);
  Result = 0   - � ������ ��� ����� (������ �� �������); }
var
  P, P_: Integer;
  i, i_: Integer;
begin
  Result:= 0;
  repeat
    i_:= -1;
    for i:=0 to High(ReplaceData) do
    with ReplaceData[i] do
    begin
      P:= Pos(Name, S);
      if ( P <> 0 ) and ( ( i_ = -1 ) or ( Length( Name ) > Length( ReplaceData[i_].Name ) ) ) then
      begin
        i_:= i;
        P_:= P;
      end;
    end;

    if i_ <> -1 then
    with ReplaceData[i_] do
    begin
      if TableRow > High(Value) then Result:= 2 else Result:= 1;
      if TableRow > High(Value) then
          S:= Copy(S, 1, P_-1) + Copy(S, P_ + Length(Name), Length(S))
        else
          S:= Copy(S, 1, P_-1) + Value[TableRow] + Copy(S, P_ + Length(Name), Length(S));
    end;
  until i_ = -1;
end;



end.
 
