unit CompressUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls,
  Dialogs, StdCtrls, Buttons, ToolWin, ComCtrls, ExtCtrls, Math;

const
  OldID: Word = $53B4; // .ego �� 1.3.8 Control Center � 1.1.45 Av14
  NewID: Word = $53B5; // .ego c 1.3.8 Control Center � 1.1.45 Av14

//  ID1: word = $0000;
//  ID2: word = $0001;

type

  THeader = record
    ID: DWord; //
    ItemCount: Integer;
  end;

  THeaderItem = packed record
    ID: Word;
    FileName: string [255];
    Offset: Integer;
    Size: Integer;
  end;

  TFileUncompressed = procedure(UncompressedFileName: string) of object;

  TCompress = class
  private
    FileStream: TMemoryStream;
    OutFileStream: TFileStream;
    temp: TMemoryStream;
  public
    rDT: string;
    rCV: string;
    OnFileUncompressed: TFileUncompressed;
    procedure CompessFiles( WorkFolder: string; FileList: TStringList; OutFile: string);
    function UnCompressFiles ( Filename, ExtractFolder: string; CopyErrors: Boolean ): Integer;
    function ReadFiles: string;

    constructor Create;
  end;

implementation

   (*
function FullRemoveDir(Dir: string; DeleteAllFilesAndFolders,
  StopIfNotAllDeleted, RemoveRoot: boolean): Boolean;
var
  i: Integer;
  SRec: TSearchRec;
  FN: string;
begin
  Result := False;
  if not DirectoryExists(Dir) then
    exit;
  Result := True;
  // ��������� ���� � ����� � ������ ����� - "��� ����� � ����������"
  Dir := IncludeTrailingBackslash(Dir);
  i := FindFirst(Dir + '*', faAnyFile, SRec);
  try
    while i = 0 do
    begin
      // �������� ������ ���� � ����� ��� ����������
      FN := Dir + SRec.Name;
      // ���� ��� ����������
      if SRec.Attr = faDirectory then
      begin
        // ����������� ����� ���� �� ������� � ������ �������� �����
        if (SRec.Name <> '') and (SRec.Name <> '.') and (SRec.Name <> '..') then
        begin
          if DeleteAllFilesAndFolders then
            FileSetAttr(FN, faArchive);
          Result := FullRemoveDir(FN, DeleteAllFilesAndFolders,
            StopIfNotAllDeleted, True);
          if not Result and StopIfNotAllDeleted then
            exit;
        end;
      end
      else // ����� ������� ����
      begin
        if DeleteAllFilesAndFolders then
          FileSetAttr(FN, faArchive);
        Result := SysUtils.DeleteFile(FN);
        if not Result and StopIfNotAllDeleted then
          exit;
      end;
      // ����� ��������� ���� ��� ����������
      i := FindNext(SRec);
    end;
  finally
    SysUtils.FindClose(SRec);
  end;
  if not Result then
    exit;
  if RemoveRoot then // ���� ���������� ������� ������ - �������
    if not RemoveDir(Dir) then
      Result := false;
end;    *)

function FullRemoveDir(DirectoryName: string; RemoveRoot: boolean): Boolean;
var
  i: Integer;
  SR: TSearchRec;
  FullFileName: string;
begin
  Result := False;

  if DirectoryExists(DirectoryName) then
  begin
    // ��������� ���� � ����� � ���� �� ����� - "��� ����� � ����������"
    DirectoryName := IncludeTrailingBackslash(DirectoryName);
    if FindFirst(DirectoryName + '*', faAnyFile, SR) = 0 then
    repeat
      if (SR.Name <> '') and (SR.Name <> '.') and (SR.Name <> '..') then
      begin
        FullFileName := DirectoryName + SR.Name;

        if (SR.Attr and faDirectory) = faDirectory then
          result := FullRemoveDir(FullFileName, True)
        else
          Result := DeleteFile(FullFileName);
      end;
    until FindNext(SR)<>0 ;
    FindClose(SR);

    if RemoveRoot = true then
      Result:= RemoveDir(DirectoryName);
  end;
end;



{ TCompress }

procedure TCompress.CompessFiles(WorkFolder: string; FileList: TStringList; OutFile: string);
var
  I: Integer;
  Head: THeader;
  Items: array of THeaderItem;
  Src: TMemoryStream;
  Dst: TMemoryStream;
  Buffer: array of Byte;
begin
  Head.ID:= NewID;
  Head.ItemCount:= FileList.Count;
  SetLength(Items, Head.ItemCount);
  Dst:= TMemoryStream.Create;

  Dst.WriteBuffer(Head, SizeOf(Head));
  Dst.WriteBuffer(Items[0], Head.ItemCount * SizeOf(THeaderItem));

  Chdir( WorkFolder );

  for I := 0 to FileList.Count - 1 do
  begin
    Src:= TMemoryStream.Create;
    Src.LoadFromFile(FileList[I]);
    Items[I].FileName:= FileList[I];
    Items[I].Offset:= Dst.Position;
    Items[I].Size:= Src.Size;
    SetLength(Buffer, Max(Src.Size, Length(Buffer)));
    Src.ReadBuffer(Buffer[0], Src.Size);
    Dst.WriteBuffer(Buffer[0], Src.Size);
    Src.Free;
  end;
  SetLength(Buffer, 0);
  Dst.Position:= SizeOf(Head);
  Dst.WriteBuffer(Items[0], Head.ItemCount * SizeOf(THeaderItem));
  Dst.SaveToFile( OutFile );

  Dst.Free;
end;


function TCompress.UnCompressFiles(Filename, ExtractFolder: String; CopyErrors: Boolean): Integer;
const
  notDone = -1;
  Done = 0;
  OldFileType = 1;
var
  I: Integer;
  Head: THeader;
  Items: array of THeaderItem;
  Src: TMemoryStream;
  Dst: TMemoryStream;
  Buffer: array of Byte;
  tmp: string;
begin
  result:= notDone;

  Src:= TMemoryStream.Create;

  Src.LoadFromFile( Filename );

  Src.ReadBuffer( Head, sizeof(Head));
  if Head.ID = NewID then
  begin
    SetLength( Items, Head.ItemCount );
    Src.ReadBuffer( Items[0], Head.ItemCount * SizeOf(THeaderItem));

    Chdir( ExtractFolder );

    for I := 0 to high(Items) do
      begin
        // ���� ��� Errors, �� ��� �� ����� ����������
        tmp:=Copy(Items[i].FileName,1,6);
//        if (Items[i].FileName[1] = 'E') and (CopyErrors = false) then
        if (tmp = 'ERRORS') and (CopyErrors = false) then
          Continue;

        Dst:= TMemoryStream.Create;
        Src.Position:= Items[i].Offset;
        SetLength(  Buffer, Max(items[i].Size, Length(Buffer)) );
        Src.ReadBuffer( Buffer[0], items[i].Size );
        Dst.WriteBuffer( Buffer[0], items[i].Size );

        // ���� ��� Nastr ��� Errors (������� ����� ����������)
//        if  (Items[i].FileName[1] = 'N') or
//            ((Items[i].FileName[1] = 'E') and (CopyErrors = true) )then
        if  (tmp[1] = 'N') or
            ((tmp = 'ERRORS') and (CopyErrors = true) )then
        begin
          tmp:= ExtractFolder + '\' + ExtractFileDir(Items[i].FileName);
          if not DirectoryExists( tmp ) then
            ForceDirectories( tmp );
          //FullRemoveDir(tmp , false);
        end;
        tmp := ExtractFolder + '\' + Items[i].FileName;
        Dst.SaveToFile( tmp );
        Dst.Free;
        if Assigned(OnFileUncompressed) then
          OnFileUncompressed(Items[i].FileName);
      end;

    SetLength( Items, 0);
    Result:= Done;
  end
  else if Head.ID = OldID then
    Result := OldFileType;

  SetLength(Buffer, 0);
  Src.Free;
end;


constructor TCompress.Create;
begin
    OnFileUncompressed:= nil;
end;

function TCompress.ReadFiles: String;
var
    str: string;
    I: integer;
begin
  result:= '';
  FileStream.ReadBuffer( str , 1 );

   if str = ':Z' then
        begin
          FileStream.ReadBuffer( I, 1);
          temp.SetSize( I );
          temp.CopyFrom( FileStream, I );
          temp.SaveToFile( '\Organization.txt' );
          temp.Free;
          str:='';
        end
    else if str = ':P' then
        begin
          FileStream.ReadBuffer( I, 1);
          temp.SetSize( I );
          temp.CopyFrom( FileStream, I );
          temp.SaveToFile( '\OperatorList.txt' );
          temp.Free;
          str:='';
        end
    else if str = ':T' then
        begin
          FileStream.ReadBuffer( I, 1);
          temp.SetSize( I );
          temp.CopyFrom( FileStream, I );;
          temp.SaveToFile( '\PeregonList.txt' );
          temp.Free;
          str:='';
        end
    else if str = ':D' then
       begin
          FileStream.ReadBuffer( I, 1);
          temp.SetSize( I );
          temp.CopyFrom( FileStream, I );
          temp.SaveToFile( '\DefCodes.txt' );
          temp.Free;
          str:='';
        end
    else if str = ':M' then
        begin
          FileStream.ReadBuffer( I, 1);
          temp.SetSize( I );
          temp.CopyFrom( FileStream, I );
          temp.SaveToFile( '\TextMarks.txt' );
          temp.Free;
          str:='';
        end
   else if str = ':C' then
        begin
          FileStream.ReadBuffer( I, 1);
          temp.SetSize( I );
          temp.CopyFrom( FileStream, I );
          temp.SaveToFile( '\default.kan' );
          temp.Free;
          str:='';
        end
   else if str = ':B' then
        begin
          FileStream.ReadBuffer( I, 1);
          temp.SetSize( I );
          temp.CopyFrom( FileStream, I );
          temp.SaveToFile( '\default.abp' );
          temp.Free;
          str:='';
        end
   else result:= '����������� �������������!!!!!' ;
end;

end.
