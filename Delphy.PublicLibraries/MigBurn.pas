unit MigBurn;

interface
uses Math, Classes, Dialogs, StrUtils, SysUtils, PublicFunc, DataFileUnit, Forms;

const
  OT_File = 0;
  OT_Folder = 1;
  OT_Data = 2;

type
  TObjects =
  record
    OType: Byte;
    Name: string;
  end;

  TBurnInfo =
  record
    Version: Word;
    BurnDate: string[8];
    BurnVer: string;
    CmpVers: string;
    Objects: array of TObjects;
  end;

  TBurnArray = array of Byte;
  
  TMemoryData =
  record
    ID: string;
    Data: TBurnArray;
  end;

var
  MemoryData: array of TMemoryData;

// �������� ��������.
function CodeBurn( BurnFile, WorkFolder, BurnDate, BurnVer, CmpVers: string; FileList: TStringList ): string;
{
  ������ ������ ������ FileList (��. ������� CodeBurn):
  ID FN [=>ID]
  ID - �������������, ���� �� ��������� (��� �������):
    "f:" - ����,
    "d:" - �����,
    "m:" - ���� � ������ (��������, ��� ������ ���� ����� ������������� �� �� ����, � � ���������� MemoryData)
  FN - ��� �����: ��� ��������� ����� � ��������� ���� ������������ WorkFolder. ��������� ���� ����� �������� ��� ����������.
       ��� �����: ��� ����� ��� �������� (� ���� ������ ������� ��������� ����)
       ��� ����� � ������: ������ ���� � ��������� �����.
  ID - ������������� ������ ������������ ������ ��������� � ��������������� "m:" � ��� ���������� �����������
       � ���� ID ��������������� ����� ������ (��. TMemoryData).
}


// ���������� ��������.
function DecodeBurn( BurnFile, OutFolder: string; OnlyTest: Boolean; BurnVersionLocationPath: string; out Info: TBurnInfo ): string;
function GetCurrentBurnVersion( BurnVersionLocationPath: string ): string;

procedure PrepareToSelfBurn;
procedure FinishSelfBurn;



implementation

uses TypInfo;


procedure PrepareToSelfBurn;
begin
  try
    RenameFile( Application.ExeName, Application.ExeName + '_' );
  except
  end;
end;

procedure FinishSelfBurn;
begin
  if not FileExists( Application.ExeName ) and FileExists( Application.ExeName + '_' ) then
  try
    RenameFile( Application.ExeName + '_', Application.ExeName );
  except
  end;
end;


function GetCurrentBurnVersion( BurnVersionLocationPath: string ): string;
var
  PrevBurnVersion: string;
  InfoFilename: string;
  F: Text;

begin
  Result:= '';
  InfoFilename:= AddSlash( BurnVersionLocationPath ) + 'burninfo.';
  if not FileExists( InfoFilename ) then Exit;
  AssignFile( F, InfoFilename );
  try
    Reset( F );
    ReadLn( F, PrevBurnVersion );
    CloseFile( F );
  except
    Exit;
  end;
  PrevBurnVersion:= Trim( PrevBurnVersion );
  if Pos( ' ', PrevBurnVersion ) <> 0 then PrevBurnVersion:= Copy( PrevBurnVersion, 1, Pos( ' ', PrevBurnVersion ) - 1 );
  Result:= PrevBurnVersion;
end;

function DecodeBurn( BurnFile, OutFolder: string; OnlyTest: Boolean; BurnVersionLocationPath: string; out Info: TBurnInfo ): string;
const
  ID = 'MIG BURN ';
  ReadVer = [2,3];

var
  InF: TDataFile;
  OutF: file;
  InfoF: Text;
  Buf: array[1..2048] of Char;
  FN: string;
  B: Byte;
  FSize: Integer;
  RealCS, WriteCS: Byte;
  NumRead, NeedRead: Integer;
  ObjectIndex: Integer;
  isVerCmp: Boolean;
  P, P_: Integer;
  S0, S: string;
  PrevBurnVersion: string;


begin
  Result:= '';
  SetLength( MemoryData, 0 );
  try
    InF:= TDataFile.Create( True, BurnFile, fmOpenRead );
  except
    Result:= '�� ������� ������� ���� ��������.';
    Exit;
  end;

  S:= InF.ReadString( 9 );
  if ID <> S then
  begin
    Result:= '�������� ������ ��������.';
    InF.Free;
    Exit;
  end;

  Info.BurnDate:= InF.ReadString( 8 );
  Info.Version:= InF.ReadWord;
  if not ( Info.Version in ReadVer ) then
  begin
    Result:= '������ �������� �� ��������������.';
    InF.Free;
    Exit;
  end;

  if Info.Version > 2 then
  begin
    Info.BurnVer:= InF.ReadString;
    Info.CmpVers:= InF.ReadString;
  end else
  begin
    Info.BurnVer:= '';
    Info.CmpVers:= '';
  end;

  PrevBurnVersion:= GetCurrentBurnVersion( BurnVersionLocationPath );
  if ( PrevBurnVersion <> '' ) and ( Info.CmpVers <> '' ) then
  begin
    // �������� ������������� ������.
    S:= Info.CmpVers;
    isVerCmp:= False;
    P_:= 0;
    repeat
      P:= PosEx( ',', S, P_ + 1 );
      if P = 0 then P:= Length( S ) + 1;
      S0:= Copy( S, P_ + 1, P - P_ - 1 );
      if Pos( S0, PrevBurnVersion ) <> 0 then
      begin
        isVerCmp:= True;
        Break;
      end;
      P_:= P;
    until P = Length( S ) + 1;
    if not isVerCmp then
    begin
      Result:= '������! �������������� ������� ���� ��������� ��������: ' + Info.CmpVers;
      InF.Free;
      Exit;
    end;
  end;                                             

  if not OnlyTest then
  begin
    // �������� � �����, ���� ����� ������������.
    try
      if not DirectoryExists( OutFolder ) then MkDir( OutFolder );
      ChDir( OutFolder );
    except
      Result:= '�� ������� ������� � ����� "' + OutFolder + '".';
      InF.Free;
      Exit;
    end;
  end;

  repeat
    B:= InF.ReadByte;
    if B = $FF then
    begin
      RealCS:= InF.GetControlSumm( InF.Position );
      WriteCS:= InF.ReadByte;
      if RealCS <> WriteCS then
      begin
        InF.Free;
        Result:= '���� �������� ���������.';
        Exit;
      end;
      InF.Free;
      // ������� ��������� ����������.
      if not OnlyTest then
      begin
        // ������� ���������� � ������ ��������.
        AssignFile(InfoF, AddSlash( BurnVersionLocationPath ) + 'burninfo.');
        {$I-}
        Rewrite(InfoF);
        if IOResult = 0 then
        begin
          if Info.BurnVer = '' then Write(InfoF, Info.BurnDate) else Write(InfoF, Info.BurnVer + ' (' + Info.BurnDate + ')' );
          CloseFile(InfoF);
        end;
        {$I+}
      end;
      Result:= '';
      Exit;
    end;

    FN:= InF.ReadString;
    SetLength( Info.Objects, Length( Info.Objects ) + 1 );
    ObjectIndex:= High( Info.Objects );
    Info.Objects[ObjectIndex].OType:= B;
    Info.Objects[ObjectIndex].Name:= FN;

    case B of
      OT_Folder:
      begin
        // �����.
        if not OnlyTest then
        begin
          try
            if not DirectoryExists( FN ) then MkDir( FN );
          except
            Result:= '�� ������� ������� ����� ' + FN + '.';
          end;
        end;
      end;
      OT_File:
      begin
        // ����.
        FSize:= InF.ReadLongWord;
        if not OnlyTest then
        begin
          AssignFile( OutF, FN );
          {$I-}
          Rewrite( OutF, 1 );
          if IOResult <> 0 then
          begin
            Result:= '�� ������� ���������� ���� ' + FN + '.';
            InF.Free;
            Exit;
          end;
          {$I+}
        end;
        repeat
          NeedRead:= Min( SizeOf(Buf), FSize );
          NumRead:= InF.Read( Buf, NeedRead );
          if not OnlyTest then BlockWrite(OutF, Buf, NumRead );
          FSize:= FSize - SizeOf(Buf);
        until (NumRead = 0) or (FSize <= 0);
        if not OnlyTest then CloseFile( OutF );
      end;
      OT_Data:
      begin
        // ������.
        FSize:= InF.ReadLongWord;
        SetLength( MemoryData, Length( MemoryData ) + 1 );
        MemoryData[ High(MemoryData) ].ID:= FN;
        SetLength( MemoryData[ High(MemoryData) ].Data, FSize );
        InF.Read( MemoryData[High(MemoryData)].Data[0], FSize );
      end;
    end;

  until InF.Position = InF.Size;

  Result:= '����������� ����� �����. ���� ���������.';
  InF.Free;
end;


 // EOutFile.Text
//  BurnDate:= EBurnDate.Text;
function CodeBurn( BurnFile, WorkFolder, BurnDate, BurnVer, CmpVers: string; FileList: TStringList ): string;
const
  ID = 'MIG BURN ';
  WriteVer: Word = 3;
var
  InF: file;
  i: Integer;
  Buf: array[1..2048] of Char;
  NumRead, NumWritten: Integer;
  OutFileName: string;
  StoreName: string;
  OutF: TDataFile;
  S: string;
  P: Integer;


begin
  Result:= '';
  OutF:= TDataFile.Create( True, BurnFile, fmCreate	);

  OutF.WriteString(ID, True);
  if Length( BurnDate ) <> 8 then
  begin
    Result:= '���� �������� ������ ���� � ������� ��.��.��!';
    Exit;
  end;
  OutF.WriteString( BurnDate, True );
  OutF.WriteWord( WriteVer );
  OutF.WriteString( BurnVer );
  OutF.WriteString( CmpVers );

  Chdir( WorkFolder );
  for i:=0 to FileList.Count - 1 do
  begin
    S:= FileList[i];
    OutFileName:= Copy( S, 4, $FF );

    P:= Pos( '=>', OutFileName );
    if P <> 0 then
    begin
      StoreName:= Copy( OutFileName, P + 2, $FF );
      OutFileName:= Copy( OutFileName, 1, P - 1 );
    end else
      StoreName:= OutFileName;
    if S[1] = 'f' then OutF.WriteByte( 0 ) else
    if S[1] = 'd' then OutF.WriteByte( 1 ) else
    if S[1] = 'm' then OutF.WriteByte( 2 ) else
    begin
      Result:= '����������� ������������� ���� ������: "' + S[1] + '" !';
      Exit;
    end;

    // ������� ��� ����� / �����.
    OutF.WriteString( StoreName );
    if S[1] in ['f', 'm'] then
    begin
      // ������� ������ � ���������� �����.
      AssignFile( InF, OutFileName );
      {$I-}
      Reset( InF, 1 );
      if IOResult <> 0 then
      begin
        Result:= '�� ������� ������� ����: ' + OutFileName + '!';
        Exit;
      end;
      {$I+}
      OutF.WriteLongWord( FileSize( InF ) );
      repeat
        BlockRead(InF, Buf, SizeOf(Buf), NumRead);
        NumWritten:= OutF.Write( Buf, NumRead );
      until (NumRead = 0) or (NumWritten <> NumRead);
      CloseFile( InF );
    end;
  end;

  // ������� ����������� �����.
  OutF.WriteByte( $FF );
  OutF.WriteByte( OutF.GetControlSumm( -1 ) );
  OutF.Flip( False );
  OutF.Free;
end;



end.
