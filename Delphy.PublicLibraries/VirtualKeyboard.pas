unit VirtualKeyboard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Math, Buttons, CfgTablesUnit;

type
  TVKTempForm = class(TForm)
    VirtualKeyboardPanel: TPanel;
    PSysButtons: TPanel;
    PMainKeys: TPanel;
    PLanguage: TPanel;
    BSetLangRus: TSpeedButton;
    BSetLangEng: TSpeedButton;
    PEdit: TPanel;
    bReset: TSpeedButton;
    bDelete: TSpeedButton;
    PFreeSpace: TPanel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TVirtualKeyboardMode = ( vkHide, vkNumbers, vkNumbersNoDelete, vkFloatNumbers, vkAll );
  TVKLanguage = ( lRus, lEng );

  TVirtualKeyboardOptions =
  record
    Mode: TVirtualKeyboardMode;
    ShowEditButtons: Boolean;
  end;
  PVirtualKeyboardOptions = ^TVirtualKeyboardOptions;

  TButtonOptions = record
    Simbol: Char;
  end;
  PButtonOptions = ^TButtonOptions;

  TVirtualKeyboard = class
  private
    MainButtons: TList;
    VKTempForm: TVKTempForm;
    Language: TVKLanguage;
    FOptions: PVirtualKeyboardOptions;
    FVirtualKeyboardMode: TVirtualKeyboardMode;
    Lng: TLanguageTable;
    procedure CreateKeys( Count: Integer );
    procedure SetLangEng( Sender: TObject );
    procedure SetLangRus( Sender: TObject );
    procedure RefreshKeys;
    procedure SetKeyCaption( StartSimbol: Char; Ofs, Count: Integer );
    procedure OnButtonClick( Sender: TObject );
    procedure OnResetText( Sender: TObject );
    procedure OnBackspace( Sender: TObject );
    procedure TranslateInterface();
  protected
    function GetMainButton( I: Integer ): TSpeedButton;
    function GetButtonOptions( I: Integer ): PButtonOptions;
  public
    VKPanel: TPanel;
    Text: string;
    onChangeText: procedure of object;
    property MainButton[I: Integer]: TSpeedButton read GetMainButton;
    property ButtonOption[I: Integer]: PButtonOptions read GetButtonOptions;
    property Options: PVirtualKeyboardOptions read FOptions;

    constructor Create( Parent: TWinControl; Lng: TLanguageTable = nil );
    destructor Destroy; override;

    procedure OnResize;
    procedure Refresh;

  end;



implementation

{$R *.dfm}

{ TVirtualKeyboard }

constructor TVirtualKeyboard.Create( Parent: TWinControl; Lng: TLanguageTable = nil );

begin
  New( FOptions );
  FOptions.Mode:= vkAll;
  FOptions.ShowEditButtons:= True;

  VKTempForm:= TVKTempForm.Create( nil );
  VKTempForm.VirtualKeyboardPanel.Parent:= Parent;
  VKTempForm.BSetLangRus.OnClick:= SetLangRus;
  VKTempForm.BSetLangEng.OnClick:= SetLangEng;
  VKTempForm.bReset.OnClick:= OnResetText;
  VKTempForm.bDelete.OnClick:= OnBackspace;
  VKPanel:= VKTempForm.VirtualKeyboardPanel;
  MainButtons:= TList.Create;

  if Assigned( Lng ) then
  begin
    Self.Lng:= Lng;
    if Lng.Caption['LngID'] = 'rus' then
    begin
      Language:= lRus;
      VKTempForm.BSetLangRus.Down:= True;
    end else
    begin
      Language:= lEng;
      VKTempForm.BSetLangRus.Enabled:= False;
      VKTempForm.BSetLangEng.Down:= True;
    end;
    TranslateInterface();
  end else
    Self.Lng:= TFakeLanguageTable.Create( nil );

end;

destructor TVirtualKeyboard.Destroy;
begin
  Dispose( FOptions );
  VKTempForm.Free;
  MainButtons.Free;
  inherited;
end;

procedure TVirtualKeyboard.OnResetText( Sender: TObject );
begin
  Text:= '';
  if Assigned( onChangeText ) then onChangeText;
end;

procedure TVirtualKeyboard.OnBackspace(Sender: TObject);
begin
  Delete( Text, Length( Text ), 1 );
  if Assigned( onChangeText ) then onChangeText;
end;

function TVirtualKeyboard.GetMainButton( I: Integer ): TSpeedButton;
begin
  Result:= TSpeedButton( MainButtons[I] );
end;

procedure TVirtualKeyboard.SetKeyCaption( StartSimbol: Char; Ofs, Count: Integer );
var
  i: Integer;

begin
  for i:= Ofs to Min( Ofs + Count - 1, MainButtons.Count - 1 ) do
  begin
    MainButton[i].Caption:= chr( ord( StartSimbol ) + i - Ofs );
    ButtonOption[i].Simbol:= chr( ord( StartSimbol ) + i - Ofs );
  end;

end;

procedure TVirtualKeyboard.RefreshKeys;
var
  CurrentButton: Integer;

  procedure AddSimbol( C: string; Simbol: Char = #0 );
  begin
    MainButton[ CurrentButton ].Caption:= C;
    if Simbol = #0 then Simbol:= C[1];
    ButtonOption[ CurrentButton ].Simbol:= Simbol;
    Inc( CurrentButton );
  end;

begin

  if Options.Mode = vkNumbers then
  begin
    CreateKeys( 10 );
    SetKeyCaption( '0', 0, 10 );
  end else
  if Options.Mode = vkFloatNumbers then
  begin
    CreateKeys( 11 );
    SetKeyCaption( '0', 0, 10 );
      CurrentButton:= 10;
    AddSimbol( ',' );
  end else
  if Options.Mode = vkAll then
  begin

    if Language = lRus then
    begin
      CreateKeys( 46 );
      SetKeyCaption( '0', 0, 10 );
      SetKeyCaption( '�', 10, 32 );
      CurrentButton:= 42;
    end else
    begin
      CreateKeys( 40 );
      SetKeyCaption( '0', 0, 10 );
      SetKeyCaption( 'A', 10, 26 );
      CurrentButton:= 36;
    end;

    AddSimbol( '.' );
    AddSimbol( ',' );
    AddSimbol( '-' );

    MainButton[ CurrentButton ].Font.Size:= 14;
    AddSimbol( Lng.Caption['������'], ' ' );

  end;

  OnResize;
end;


procedure TVirtualKeyboard.CreateKeys( Count: Integer );
var
  i: Integer;
  Button: TSpeedButton;
  ButtonOptions: PButtonOptions;

begin

  if Count > MainButtons.Count then
  for i:= 1 to Count - MainButtons.Count do
  begin
    Button:= TSpeedButton.Create( nil );
    MainButtons.Add( Button );
    with Button do
    begin
      Parent:= VKTempForm.PMainKeys;
      Width:= 100;
      Height:= 60;
      Font.Name:= 'Arial';
      Font.Style:= [fsBold];
      New( ButtonOptions );
      Tag:= Integer( ButtonOptions );
      OnClick:= OnButtonClick;
    end;
  end;

  for i:= 0 to MainButtons.Count - 1 do
  begin
    TSpeedButton( MainButtons[i] ).Visible:= i < Count;
    TSpeedButton( MainButtons[i] ).Font.Size:= 24;
  end;

end;

procedure TVirtualKeyboard.OnResize;
var
  i, X, Y, L: Integer;
  PHeight: Integer;

begin
  X:= 0;
  Y:= 0;
  for i:= 0 to MainButtons.Count - 1 do
  with TSpeedButton( MainButtons[i] ) do
  if Visible then
  begin
    L:= 5 + ( Width + 5 ) * X;
    if L + Width > VKTempForm.PMainKeys.Width then
    begin
      X:= 0;
      L:= 5;
      Inc( Y );
    end;
    Left:= L;
    Top:= 5 + Y * ( Height + 5 );
    PHeight:= Top + Height;
    Inc( X );
  end;

  VKPanel.Height:= Max( VKTempForm.PFreeSpace.Top, PHeight + 20 );

end;


procedure TVirtualKeyboard.SetLangEng(Sender: TObject);
begin
  Language:= lEng;
  RefreshKeys;
end;

procedure TVirtualKeyboard.SetLangRus(Sender: TObject);
begin
  Language:= lRus;
  RefreshKeys;
end;

procedure TVirtualKeyboard.Refresh;
begin
  VKPanel.Visible:= Options.Mode <> vkHide;
  VKTempForm.PLanguage.Visible:= Options.Mode = vkAll;
  VKTempForm.PEdit.Visible:= Options.ShowEditButtons;
  VKTempForm.PSysButtons.Visible:= VKTempForm.PLanguage.Visible or VKTempForm.PEdit.Visible;
  RefreshKeys;
end;


procedure TVirtualKeyboard.OnButtonClick(Sender: TObject);
var
  ButtonOptions: PButtonOptions;

begin
  ButtonOptions:= Pointer( TSpeedButton( Sender ).Tag );
  Text:= Text + ButtonOptions.Simbol;
  if Assigned( onChangeText ) then onChangeText;
end;

function TVirtualKeyboard.GetButtonOptions(I: Integer): PButtonOptions;
begin
  Result:= Pointer( MainButton[I].Tag );
end;

procedure TVirtualKeyboard.TranslateInterface;
begin
  VKTempForm.BSetLangRus.Caption:= Lng.Caption['���.'];
  VKTempForm.BSetLangEng.Caption:= Lng.Caption['����.'];
  VKTempForm.bDelete.Caption:= Lng.Caption['����.'];
  VKTempForm.bReset.Caption:= Lng.Caption['�����'];
end;

end.
