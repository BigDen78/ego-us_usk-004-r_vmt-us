unit BScanProcessor;

interface
uses Classes, SyncObjs, ExtCtrls, Forms, Math, Graphics, PublicFunc,
     Buffers, DeviceInterfaceUnit, DateUtils, AviconDataContainer, AviconTypes, SysUtils, Dialogs;

{
    ������ ��� ����� � ����������� �-��������� �� ���.
     TBScanProcessor.AddBlock -> ���������� ��������� �� ��� ����� (������� � �����)
     TBScanProcessor.GetAllAndDecode -> ������� ��� ����������� ������ �-����� �� ������ � ������������.

     ������ ����������������!!!

}

const
  BVBufferCount = 1000;

type

  TBVBuffEl = packed record
    Napr: ShortInt;
    DataCount: Integer;
    Data: TBVBuffer;
  end;


  PBVBuffEl = ^TBVBuffEl;

  TBVBuff = array [0..BVBufferCount-1] of TBVBuffEl;

  TBScanSysCoordChanged = procedure( Delta: Integer ) of object;

  TBScanProcessor = class
  private
    BVBuff: TBVBuff;
    BVWritePos: Integer; // ��������� �� ������� ������������ ������ ������ B-���� ( �.�. ��������� ���������� ������, ������� ��� ��������: BVWriteBuff-1 )
    BVReadPos: Integer;  // ��������� �� ������, ��� �� ���������� ������ ������ B-���� ( �.�. ���������� ���� �������� � BVReadPos )
    FCount3F: Integer;
    FSummaryWay: Integer;
    CS_BV: TCriticalSection;
    FLastBVCount: Integer;
    FCurrentBV: PBVBuffEl;

  public
    function GetBV(var BV: TBVBuff): Integer; overload;
    function GetBV(var BV: TBVBuff; Count: Integer): Integer; overload;
    function GetBVCount: Integer;

    // ������� ����������:
    function AddBlock( BL: TBlock ): Boolean;                      // ��������� �������������� ����� � �����...
    procedure GetAllAndDecode( OnSysCoordChanged: TBScanSysCoordChanged; OnBScanDecoded: TBScanDecodedProc );
                                                                   // ... � ��������� ��� ��������� ������.

    constructor Create;
    destructor Destroy; override;

    // �������� ������� � �������� ���������� GetAllAndDecode.
    property LastBVCount: Integer read FLastBVCount;
    property CurrentBV: PBVBuffEl read FCurrentBV;

  end;

  TRegistrationThread = class(TThread)
    private
      FBScanProcessor: TBScanProcessor;
      FAviconDataContainer: TAviconDataContainer;
      FBVCount: Integer;
      MS: TMemoryStream;
      CS: TCriticalSection;
      procedure BScanDecodedProc( Rail, Takt: Byte; Echos: TDecodedEchos; AdditionalData: Integer );
    protected
      procedure Execute; override;
    public
      Cikl: integer; // 1 - ������������ ������, 2 - ������������ �����
      constructor Create( BScanProcessor_: TBScanProcessor; AviconDataContainer_: TAviconDataContainer );
      destructor Destroy; override;
  end;

  TRegistrationThreadFake = class(TThread)
    private
      FAviconDataContainer: TAviconDataContainer;
    protected
      procedure Execute; override;
    public
      constructor Create(  AviconDataContainer_: TAviconDataContainer );
      destructor Destroy; override;
  end;

implementation


function TBScanProcessor.GetBV(var BV: TBVBuff): Integer;
begin
  Result:= GetBV( BV, -1 );
end;

function TBScanProcessor.AddBlock(BL: TBlock): Boolean;
var
  Delta: Integer;
  i: Integer;
  Cou: Integer;

begin
    // B-���������.
  case BL[1] of
    $3F:
      begin // ������������ ������� ����.

        CS_BV.Enter;

        inc( FCount3F );
        if BL[3] > 127 then Delta:= BL[3] - 256 else Delta:= BL[3];
        FSummaryWay:= FSummaryWay + Delta;

        for i:= 1 to Abs( Delta ) do
        begin
          inc( BVWritePos );
          if BVWritePos > BVBufferCount - 1 then BVWritePos:= 0;
          BVBuff[ BVWritePos ].Napr:= Sign( Delta );
          BVBuff[ BVWritePos ].DataCount:= 0;
        end;

        CS_BV.Release;


      end;

    $70:
    begin
      CS_BV.Enter;

      // ������ 1 ����� B-���������.
      if BL[1] = $70 then
      if BVWritePos <> - 1 then
      with BVBuff[ BVWritePos ] do
      begin
      //try
        Cou:= ( BL[2] and $F );
        if DataCount <= High(Data) then
          begin
            Data[ DataCount ]:= Cou;
            for i:= 1 to Cou do
               if (DataCount + i) <= High(Data) then
                  Data[ DataCount + i ]:= BL[ i + 2 ];
            DataCount:= DataCount + Cou + 1;
            if DataCount > 501 then DataCount:=501;
          end;
       // except // del  it!
        //  i:=3;
       // end;
      end;

      CS_BV.Release;

    end;
  end;
end;

function TBScanProcessor.GetBV(var BV: TBVBuff; Count: Integer): Integer;
var
  C: Integer;

begin
try
  CS_BV.Enter;

  if Count = -1  then Count:= GetBVCount else Count:= Min( GetBVCount, Count );

  if Count = 0 then
  begin
    CS_BV.Release;
    Result:= 0;
    Exit;
  end;

  if BVReadPos + Count > BVBufferCount then
  begin
    // � ���������.
    C:= BVBufferCount - BVReadPos;  // �����.
    Move( BVBuff[BVReadPos], BV[0], C * SizeOf( TBVBuffEl ) );  // ������� �����.
    Move( BVBuff[0], BV[C], ( Count - C ) * SizeOf( TBVBuffEl ) );    // � ������ ������.
    BVReadPos:= Count - C;
  end else
  begin
    // ��� ��������.
    Move( BVBuff[BVReadPos], BV[0], Count * SizeOf( TBVBuffEl ) );
    BVReadPos:= BVReadPos + Count;
    if BVReadPos = BVBufferCount then BVReadPos:= 0;
  end;

  Result:= Count;

  CS_BV.Release;

except
end;
end;

function TBScanProcessor.GetBVCount: Integer;
begin
  if ( BVReadPos = BVWritePos ) or ( BVWritePos = -1 ) then Result:= 0
  else if BVReadPos > BVWritePos then Result:= BVBufferCount - BVReadPos + BVWritePos
  else Result:= BVWritePos - BVReadPos;
end;

procedure TBScanProcessor.GetAllAndDecode( OnSysCoordChanged: TBScanSysCoordChanged; OnBScanDecoded: TBScanDecodedProc );
var
  BV: TBVBuff;
  Cnt: Integer;
  i: Integer;

begin
  Cnt:= GetBV( BV );
  FLastBVCount:= Cnt;
  for i:= 0 to Cnt - 1 do
  begin
    FCurrentBV:= @BV[i];
    OnSysCoordChanged( BV[i].Napr );
    DecodeBScan( BV[i].Data, BV[i].DataCount, OnBScanDecoded, 0);
  end;
  FCurrentBV:= nil;
end;

constructor TBScanProcessor.Create;
var
  i: Integer;
begin
  CS_BV:= TCriticalSection.Create;

end;

destructor TBScanProcessor.Destroy;
begin
  CS_BV.Free;
  inherited;
end;

{ TRegistrationThread }

procedure TRegistrationThread.BScanDecodedProc(Rail, Takt: Byte;
  Echos: TDecodedEchos; AdditionalData: Integer);
var
  i: integer;
  Count, R, K: Byte;
  EchoBlock: TEchoBlock;

begin
  // ��������� ������� � ���������
 { if Takt < 12 then
    begin
      K:= Takt;
      R:= 0;
    end
  else
    begin
      K:= Takt-12;
      R:= 1;
    end;}

  if Cikl = 1 then
    begin
      K:= Takt;
      R:= 0;
    end
  else
    begin
      K:= Takt;
      R:= 1;
    end;


  // ��� ������ ���� � ������ ������, ���� �������� ��� �������
  Count:= Min(Echos.Count, 8);
  for i := 0 to Count - 1 do
    begin
      EchoBlock[i].T:= Echos.Echo[i].T;
      EchoBlock[i].A:= Echos.Echo[i].A;
    end;
  if Assigned( FAviconDataContainer ) then
    if (R<2) and  (K<12) and  (Count<9) then
      FAviconDataContainer.AddEcho(TRail(R), K, Count, EchoBlock);

  // �������� ������� �������� ��� ������� � ������������ !!
{  Count:= 1;
  //EchoBlock[0].T:= Abs(Round(Random(50)));
  EchoBlock[0].T:= 30;
  EchoBlock[0].A:= 15;
  if Assigned( FAviconDataContainer ) then
    begin
      FAviconDataContainer.AddEcho(TRail(0), K, Count, EchoBlock);
      FAviconDataContainer.AddEcho(TRail(1), K, Count, EchoBlock);
    end;}

    //FAviconDataContainer.AddEcho(TRail(R), K, 1, Round(Random(50)), 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
end;

constructor TRegistrationThread.Create(BScanProcessor_: TBScanProcessor;
  AviconDataContainer_: TAviconDataContainer);
begin
  Randomize;
  FBScanProcessor:= BScanProcessor_;
  FAviconDataContainer:= AviconDataContainer_;
  MS:= TMemoryStream.Create;
  CS:= TCriticalSection.Create;
  inherited Create(True);
end;

destructor TRegistrationThread.Destroy;
begin
  MS.Free;
  FBScanProcessor:= nil;
  FAviconDataContainer:= nil;
  CS.Free;
  inherited;
end;

procedure TRegistrationThread.Execute;
var
  i: integer;
  BV: TBVBuff;
begin
  while not Terminated do
    begin
      if Assigned(FBScanProcessor) and Assigned(FAviconDataContainer) then
        begin
          CS.Enter;
          FBVCount:= FBScanProcessor.GetBV( BV );
          for i:= 0 to FBVCount - 1 do
            begin
              // ��� ������� �������� ���� ����������� ������ ���
              //MS.Clear;
              //MS.LoadFromFile('D:\���\�������� ���������\��� �� �������������\_EXE_\RESULTS\MIGS.a16');
              //FAviconDataContainer.SaveToStream(MS);
              //MS.SaveToFile('D:\���\�������� ���������\��� �� �������������\_EXE_\RESULTS\MIG'+IntToStr(FAviconDataContainer.CurSaveDisCoord)+'.a16');
              FAviconDataContainer.AddSysCoord( FAviconDataContainer.CurSaveSysCoord + BV[i].Napr );
              DecodeBScan( BV[i].Data, BV[i].DataCount, BScanDecodedProc, 0);
            end;
          CS.Leave;
        end;
      Sleep( 10 );
    end;
end;

{ TRegistrationThreadFake }

constructor TRegistrationThreadFake.Create(
  AviconDataContainer_: TAviconDataContainer);
begin
  FAviconDataContainer:= AviconDataContainer_;
  Randomize;
  Inherited Create(true);
end;

destructor TRegistrationThreadFake.Destroy;
begin
  FAviconDataContainer:= nil;
  inherited;
end;

procedure TRegistrationThreadFake.Execute;
var
  Napr: Integer;
begin
  while not Terminated do
    begin
      Sleep(10);
      if Assigned(FAviconDataContainer) then
        begin
          Napr:= Round(Random*2+1);
          if Random > 0.5 then Napr:= Napr*(-1);
          FAviconDataContainer.AddSysCoord(FAviconDataContainer.CurSaveSysCoord+Napr);
        end;
    end;
end;

end.
