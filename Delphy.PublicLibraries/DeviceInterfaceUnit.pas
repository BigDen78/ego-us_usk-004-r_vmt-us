unit DeviceInterfaceUnit;
// ������ 1.6.
{ 1.2
  Bag Fixed. ���� IsOpen �� ������������� � PCICAN � AKP, ��-�� ���� ��� �� ��������.
  1.3
  � AKP ������������� ����� PutBlock. �������� �������� �������� (���� ����� �� ������ ��� ������ ������), �������� ���� ������ ���.
  1.4.
  ��������� �������� �� ���������� ������� CurrentBlock (��������� Range Check Error ��� ���� �������)
  1.5.
  ������������� ������� ������������ ������ ��������� (�������� ����� ������������ ���� ������)
    ���������� �� ��� ���������� ����������. � ���������� ������� ������������� ��������� �� "������" �������.
  1.6
  �������� TDeviceMAKP ��� ������ � ������� ������� IPV (������-16).
}

{$I Directives.inc}
{ $DEFINE EnableUKK}

interface
uses Classes, Math, SysUtils, Windows, Forms, PublicFunc, Buffers, Controls,
     InterfaceCAN,   // ��������� ����� PCI-CAN.
     DllDescriptor,  // ��������� ���.
     //DllDescriptorM; // ��������� ������-���.
     //DllDescriptorEmul; // ��������� ������-��� � ��������.
{$IFDEF USK004R}
     DllDescriptorDynamicSim; // ������������ ����������� DLL �����
{$ELSE}
     DllDescriptorDynamic; // ������������ ����������� DLL �����
{$ENDIF}

const
{$IFDEF USK004R}
  MaxBUM = 2;//!!!
{$ELSE}
  MaxBUM = 16;//!!!
{$ENDIF}
  DeviceNetID = 0;                                                              // ID ���������� � ���� CAN (��� ����� PCI-CAN)
  MaxDevicePCICANCount = 255;

  // de == OnDeviceError.
  deNoExternalDevice = 1;
  deReadBufferOverflow = 2;


type
//  TDebugInfoProc = procedure( Msg: string ) of object; // DEBUG MIG!
  TAddLogProc = procedure ( Msg: string ) of object;
  TOnSendErrorProc = procedure of object;
  TOnByteReserved = procedure ( B: Byte ) of object;
  TOnBlockReserved = procedure ( Block: TBlock ) of object;
  TOnBlockReservedPCICAN = procedure ( Block: TBlock; DeviceNumber, ChannelNumber: Integer ) of object;
  TOnException = procedure( ThreadName: string; E: Exception ) of object;
  TOnReadyToSendBlock = procedure( DeviceNumber, ChannelNumber: Integer ) of object;
  TOnDeviceError = procedure ( ErrorID: Integer ) of object;

  {
    1. �������� ������: PutBlock.
    2. ����� ������ �� ������� OnBlockReserved.

    TDevice ������������, ��� ���������� ����� ���������� ������ �� 1 �����, ������������� ������� GetByte � PutByte.
    ���� ���������� (����. PCI-CAN) �������� ������ ������ �������, ��� ������ �������������� Execute ��� ������������� ������
    ������ ������� � ����� ������� OnBlockReserved, � ����� �������������� PutBlock ��� �������� ������ �������.
  }

  TDeviceType = ( RS, PCICAN, AKP );

  TDevice = class( TThread )
  private
    CurrentBlock: TBlock;
    CurrentBlockPosition: Integer;
    RequestDataCount: Cardinal;
    Reserved: Cardinal;
    Sended: Cardinal;
    Tick: Cardinal;
    FRequestDataPerSec: Cardinal;
    FReservedPerSec: Cardinal;
    FSendedPerSec: Cardinal;
    isOpen: Boolean;
    function GetByte( out B: Byte ): Boolean; virtual; abstract;
    procedure AddLog( Msg: string );
    procedure ByteReserved( B: Byte );
    procedure BlockReserved( Block: TBlock );
    procedure ProcessExceptionDefault( ThreadName: string; E: Exception );
    procedure ExecuteStart; virtual;
    procedure ExecuteLoop; virtual;
    function FPutByte( B: Byte ): Boolean; virtual; abstract;
  protected
    FOnByteReserved: TOnByteReserved;
    procedure Execute; override;
  public
    DeviceID: string;
    AddLogProc: TAddLogProc;
    SleepInterval: Integer;
    OnInternalThreadException: TOnException;
    OnSendError: TOnSendErrorProc;
    OnBlockReserved: TOnBlockReserved;
    OnDeviceError: TOnDeviceError;
    WaitBlockInterval: Integer;
    ProcessBlocks: Boolean;
    SendByteMinimalInterval: Integer;  // ����� (sleep) ����� ��������� ���� ���� (-1 - ����.)
    property OnByteReserved: TOnByteReserved read FOnByteReserved write FOnByteReserved;
    property RequestDataPerSec: Cardinal read FRequestDataPerSec;
    property ReservedPerSec: Cardinal read FReservedPerSec;
    property SendedPerSec: Cardinal read FSendedPerSec;
    constructor Create; virtual;
    destructor Destroy; override;
    function Open: Boolean; virtual; abstract;
    procedure Close; virtual; abstract;
    function PutBlock( B: TBlock ): Boolean; virtual;
    function PutByte( B: Byte ): Boolean;
  end;


  TDataToSend =
  record
    isData: Boolean;
    Data: TBlock;
  end;

  // ��������� ����� PCI-CAN.
  TDevicePCICAN = class( TDevice )
  private
    FDeviceCount: Integer;
    DataToSend: array[0..MaxDevicePCICANCount, 1..2] of TDataToSend;
    ReserveDataEvent: array[0..MaxDevicePCICANCount, 1..2] of THandle;
    FStopReserved: array[ 0..MaxDevicePCICANCount, 1..2 ] of Boolean;
    StopFrequency: array[ 0..MaxDevicePCICANCount, 1..2 ] of Cardinal;
    FStopsPerSecEx: array[0..MaxDevicePCICANCount, 1..2] of Cardinal;
    ReservedEx:  array[0..MaxDevicePCICANCount, 1..2] of Cardinal;
    FReservedPerSecEx: array[0..MaxDevicePCICANCount, 1..2] of Cardinal;
    SendedEx:  array[0..MaxDevicePCICANCount, 1..2] of Cardinal;
    FSendedPerSecEx: array[0..MaxDevicePCICANCount, 1..2] of Cardinal;
    SleepCount: Integer;
    FSleepPercent: Integer;
    FSendFailByErrPercent: Integer;
    FSendFailByStopPercent: Integer;
    SendFailByStopCount: Integer;
    SendFailByErrCount: Integer;
    SendDataTryCount: Integer;
    procedure ExecuteStart; override;
    procedure ExecuteLoop; override;
    procedure ConfigCurrentDevice;
    procedure Select( DeviceNumber, ChannelNumber: Integer );
    procedure OnBlockReservedExDefault( Block: TBlock; DeviceNumber, ChannelNumber: Integer );
  protected
    function GetReservedPerSecEx( DeviceNum, ChannelNum: Integer ): Cardinal;
    function GetSendedPerSecEx( DeviceNum, ChannelNum: Integer ): Cardinal;
    function GetStopReserved( DeviceNum, ChannelNum: Integer ): Boolean;
    function GetStopsPerSecEx( DeviceNum, ChannelNum: Integer ): Cardinal;
    procedure Execute; override;
  public
    OnBlockReservedEx: TOnBlockReservedPCICAN;
    OnReadyToSendBlock: TOnReadyToSendBlock;

    Status: array[0..MaxDevicePCICANCount, 1..2] of Cardinal;

    property SendFailByErrPercent: Integer read FSendFailByErrPercent;
    property SendFailByStopPercent: Integer read FSendFailByStopPercent;
    property ReservedPerSecEx[DeviceNum, ChannelNum: Integer]: Cardinal read GetReservedPerSecEx;
    property SenededPerSecEx[DeviceNum, ChannelNum: Integer]: Cardinal read GetSendedPerSecEx;
    property StopReserved[DeviceNum, ChannelNum: Integer]: Boolean read GetStopReserved;
    property StopsPerSecEx[DeviceNum, ChannelNum: Integer]: Cardinal read GetStopsPerSecEx;
    property DeviceCount: Integer read FDeviceCount;
    property SleepPercent: Integer read FSleepPercent;
    constructor Create;
    function Open: Boolean; override;
    procedure Close; override;
    function PutBlock( B: TBlock ): Boolean; overload; override;
    function PutBlock( B: TBlock; DeviceNumber, ChannelNumber: Integer; NoWait: Boolean = False ): Boolean; overload;
    procedure DropStopFlag( DeviceNum, ChannelNum: Integer );
  end;


  // ���������� ����� ���� COM (RS).
  TDeviceRS = class( TDevice )
  private
    function SetCommTiming: Boolean;
    function SetCommBuffer(InQueue, OutQueue: LongInt): Boolean;
    function SetCommStatus(Baud: Integer): Boolean;
    function GetByte( out B: Byte ): Boolean; override;
    function FPutByte( B: Byte ): Boolean; override;
  public
    InQueue: LongInt;
    OutQueue: LongInt;
    Baud: LongInt;
    ComHandle: DWord;
    constructor Create;
    function Open: Boolean; override;
    procedure Close; override;
    function GetComPortList: TStringList;
  end;

  // ���.
  TConsolButtons = (cbCoord, cbBoltJoint, cbWeldJoint);
  TButtonStateChangeEvent = procedure(Button: TConsolButtons; State: Boolean) of object;

  TDeviceAKP = class( TDevice )
  private
    Test: Integer;
    function GetByte( out B: Byte ): Boolean; override;
    procedure GetButtonsState(B: Byte);
    function FPutByte( B: Byte ): Boolean; override;
  public
    OnButtonStateChange: TButtonStateChangeEvent;
    constructor Create; override;
    function Open: Boolean; override;
    procedure Close; override;
    function PowerSwitch( needOn: Boolean ): Boolean;
    function PutBlock( B: TBlock ): Boolean; override;
    function GetUAKK: Integer;
    procedure InitPanel;
  end;

  // ������-���.
  TAKPState = array[0..15] of Boolean;
  TAKPConnectionChange = procedure (ID: Integer) of object;
  TOnBlockReservedMas = array[0..15] of TOnBlockReserved;
  TOnByteReservedMas = array[0..15] of TOnByteReserved;

  TDeviceMAKP = class( TDevice )
  private
    FSearchForDevices: Boolean;
    FAKPState :TAKPState;
    FBUMReady :TAKPState;
    FTick: Int64;
    FReservedM: array[0..15] of Cardinal;
    FCurrentBlockPositionM: array[0..15] of Integer;
    FCurrentBlockM: array[0..15] of TBlock;
    FBUMTimeOut: array[0..15] of Cardinal;
    FBUMTestBlock: TBlock;
    FErrUakkCount: Integer;
    function GetByte( out B: Byte; ID: Byte ): Boolean; overload;
    function GetDeviceCount: Integer;
    function GetAKPState(ID: Integer): Boolean;
    procedure DeviceSearch;
    procedure BlockReceived( Block: TBlock; ID: Byte );
  protected
    FOnByteReservedMas: TOnByteReservedMas;
    procedure Execute; override;
  public
    AKPConnected: TAKPConnectionChange;
    AKPDisconnected: TAKPConnectionChange;
    OnBlockReservedMas: TOnBlockReservedMas;
    constructor Create; override;
    function Open: Boolean; override;
    procedure Close; override;
    procedure ExecuteLoop; override;
    //function PowerSwitch( needOn: Boolean ): Boolean;
    //function PutByte( B: Byte ): Boolean; overload;
    function PutByte( B, ID: Byte): Boolean; overload;
    function PutBlock( B: TBlock ): Boolean; overload; override;
    function PutBlock( B: TBlock; ID: Byte ): Boolean; overload;
    procedure StopDeviceSearching;
    function GetUAKK: Integer;
    function GetU2CSerial( ID:byte ): integer;
    function GetU2CVersion( PBuffer: Pointer;  ID: byte ): integer;
    property DeviceCount: Integer read GetDeviceCount;
    property AKPState[index: Integer]: Boolean read GetAKPState;
    //property BUMSerialNumber[index: Byte]: Word read GetBUMSN;
  end;

  TRemotePanel = class
  private
    function PowerSwitch( needOn: Boolean ): Boolean;
  protected
  public
    OnButtonStateChange: TButtonStateChangeEvent;
    constructor Create;
    destructor Destroy; override;
    function Open: Boolean;
    procedure GetButtonsState(B: Byte);
    function GetUAKK: Integer;
  published 

  end;


threadvar
  CurrentThreadID: string;    // ������������� �������� ������ (��� �������������� �������)

var // DEBUG.
  DebEv0H: Integer;
  DebEv1H: Integer;
  DebObj0Ev: Integer;
  DebObj1Ev: Integer;
  LockCANThreads: Boolean=False;

implementation
var
  cDeviceAKP: TDeviceAKP;
  cRemotePanel: TRemotePanel;

procedure _GetButtonsState_(B: Byte); cdecl;
begin
  if Assigned(cDeviceAKP) then
    cDeviceAKP.GetButtonsState(B);
end;

procedure _GetButtonsState_RemotePanel(B: Byte); cdecl;
begin
  if Assigned(cRemotePanel) then
    cRemotePanel.GetButtonsState(B);
end;


//uses unit1; // debug!

// -----------------------------------------------------------------------------
// -------------------------------  DeviceInterface  ---------------------------

procedure TDevice.AddLog(Msg: string);
begin
  if Assigned( AddLogProc ) then AddLogProc( Msg );
end;



constructor TDevice.Create;
begin
  isOpen:= False;
  AddLogProc:= nil;
  OnSendError:= nil;
  OnBlockReserved:= nil;
  OnByteReserved:= nil;
  OnDeviceError:= nil;
  SleepInterval:= 0;
  ProcessBlocks:= True;
  OnInternalThreadException:= ProcessExceptionDefault;
  CurrentBlockPosition:= 1;
  DeviceID:= '';
  WaitBlockInterval:= 2000;
  SendByteMinimalInterval:= -1;
  inherited Create( False );
end;

destructor TDevice.Destroy;
begin
  Close;
  inherited Destroy;
end;

procedure TDevice.BlockReserved( Block: TBlock );
begin
  AddLog( DeviceID + ' R: ' + BlockAsString( Block ) );
  if Assigned( OnBlockReserved ) then OnBlockReserved( Block );
end;

procedure TDevice.ByteReserved( B: Byte );
begin
  if Assigned( FOnByteReserved ) then FOnByteReserved( B );
  if not ProcessBlocks then
    AddLog( DeviceID + ' r: ' + Format( '0x%.2x', [B] ) );
end;

procedure TDevice.ExecuteStart; 
begin
  RequestDataCount:= 0;
  Reserved:= 0;
  Sended:= 0;
  Tick:= GetTickCount;
end;

procedure TDevice.ExecuteLoop;
begin
  Inc( RequestDataCount );
  if GetTickCount - Tick > 1000 then
  begin
    FRequestDataPerSec:= RequestDataCount;
    FReservedPerSec:= Reserved;
    RequestDataCount:= 0;
    Reserved:= 0;
    FSendedPerSec:= Sended;
    Sended:= 0;
    Tick:= GetTickCount;
  end;
end;

procedure TDevice.Execute;
var
  B: Byte;
  BlockRes: Boolean;

begin
  try
  ExecuteStart;
  while not Terminated do
  begin
    if not isOpen then
    begin
      Sleep( 20 );
      Continue;
    end;

    ExecuteLoop;
    if GetByte( B ) then
    begin
      // ������� ����.
      Inc( Reserved );
      //if Assigned( FOnByteReserved ) then FOnByteReserved( B );
      ByteReserved( B );
      if ProcessBlocks then
      begin
        if CurrentBlockPosition <= High( CurrentBlock ) then CurrentBlock[ CurrentBlockPosition ]:= B;
        if UseExBlocks then
          BlockRes:= CurrentBlockPosition >= 2 + CurrentBlock[2]
        else
          BlockRes:= CurrentBlockPosition >= 2 + CurrentBlock[2] and $F;
        if BlockRes then
        begin
          // �������� �������.
          BlockReserved( CurrentBlock );
          CurrentBlockPosition:= 0;
        end;
        Inc( CurrentBlockPosition );
      end;
    end else Sleep( SleepInterval );
  end;
  except
    on E: Exception do if Assigned( OnInternalThreadException ) then OnInternalThreadException( DeviceID, E );
  end;
end;

procedure TDevice.ProcessExceptionDefault( ThreadName: string; E: Exception );
begin
  Application.MessageBox( PChar( '��������� ���������� "' + E.Message + '" � ������ ��������� ������ "' + ThreadName + '". ����� ������ ���������.' ), '������', 16  );
end;



function TDevice.PutBlock( B: TBlock ): Boolean;
var
  i: Integer;

begin
  Result:= FPutByte( B[1] );
  if SendByteMinimalInterval <> -1 then Sleep( SendByteMinimalInterval );
  Result:= Result and FPutByte( B[2] );
  if SendByteMinimalInterval <> -1 then Sleep( SendByteMinimalInterval );
  for i:= 3 to B[2] + 2 do
  begin
    if SendByteMinimalInterval <> -1 then Sleep( SendByteMinimalInterval );
    Result:= Result and FPutByte( B[i] );
  end;
  if Result then AddLog( DeviceID + ' S: ' + BlockAsString(B) );
  if Result then Inc( Sended );
  if not Result and Assigned( OnSendError ) then OnSendError;
end;


function TDevice.PutByte( B: Byte ): Boolean;
var
  Fail: string;

begin
  Result:= FPutByte( B );
  if Result then Fail:= '' else Fail:= '[FAILED]';
  if not ProcessBlocks then
    AddLog( DeviceID + ' s: ' + Format( '0x%.2x %s', [B, Fail] ) );
end;


// -----------------------------------------------------------------------------
// -------------------------------  PCI - CAN  ---------------------------------


constructor TDevicePCICAN.Create;
begin
  OnBlockReservedEx:= OnBlockReservedExDefault;
  OnReadyToSendBlock:= nil;
  inherited;
  Priority:= tpTimeCritical;
end;

function TDevicePCICAN.Open: Boolean;
var
  Dev, Ch: Integer;
  EventAttributes: SECURITY_ATTRIBUTES;

begin
  Result:= False;

  // ������� ������ ������.
  EventAttributes.nLength:=sizeof( EventAttributes );
  EventAttributes.lpSecurityDescriptor:=nil;
  EventAttributes.bInheritHandle:=True;

  // ����� ��������� PCI-CAN.
  FDeviceCount:= 0;
  while CANOpen( FDeviceCount )= 0 do Inc( FDeviceCount );
  for Dev:= 0 to DeviceCount - 1 do
  for Ch:= 1 to 2 do
  begin
    Select( Dev, Ch );
    ConfigCurrentDevice;
    // ��������� �������.
    ReserveDataEvent[ Dev, Ch ]:= CreateEvent( @EventAttributes, True, False, nil );
    if ReserveDataEvent[ Dev, Ch ] = 0 then Exit;
    DefEvent( ReserveDataEvent[ Dev, Ch ], True );
  end;

  Result:= DeviceCount > 0;
  isOpen:= Result;
end;

procedure TDevicePCICAN.Close;
var
  i: Integer;

begin
  for i:= 0 to DeviceCount - 1 do
    CANClose( i );
end;


procedure TDevicePCICAN.OnBlockReservedExDefault( Block: TBlock; DeviceNumber, ChannelNumber: Integer );
begin
  if Assigned( OnBlockReserved ) then OnBlockReserved( Block );
end;


procedure TDevicePCICAN.ExecuteStart;
begin
  inherited;
  FillChar( ReservedEx[ 0, 1 ], SizeOf( ReservedEx ), 0 );
  FillChar( SendedEx[ 0, 1 ], SizeOf( SendedEx ), 0 );
  FillChar( FStopReserved[ 0, 1 ], SizeOf( FStopReserved ), 0 );
  FillChar( StopFrequency[ 0, 1 ], SizeOf( StopFrequency ), 0 );
  SleepCount:= 0;
  SendDataTryCount:= 0;
  SendFailByStopCount:= 0;
  SendFailByErrCount:= 0;
end;

procedure TDevicePCICAN.ExecuteLoop;
begin
  if GetTickCount - Tick > 1000 then
  begin
    Move( ReservedEx[ 0, 1 ], FReservedPerSecEx[ 0, 1 ], SizeOf( ReservedEx ) );
    FillChar( ReservedEx[ 0, 1 ], SizeOf( ReservedEx ), 0 );
    Move( SendedEx[ 0, 1 ], FSendedPerSecEx[ 0, 1 ], SizeOf( SendedEx ) );
    FillChar( SendedEx[ 0, 1 ], SizeOf( SendedEx ), 0 );
    if RequestDataCount <> 0 then
      FSleepPercent:= (SleepCount - 1) * 100 div RequestDataCount;
    SleepCount:= 0;
    if SendDataTryCount <> 0 then
    begin
      FSendFailByErrPercent:= SendFailByErrCount * 100 div SendDataTryCount;
      FSendFailByStopPercent:= SendFailByStopCount * 100 div SendDataTryCount;
    end else
    begin
      FSendFailByErrPercent:= 0;
      FSendFailByStopPercent:= 0;
    end;
    SendFailByStopCount:= 0;
    SendFailByErrCount:= 0;
    SendDataTryCount:= 0;
    // ������� ������� �����/����.
    Move( StopFrequency[ 0, 1 ], FStopsPerSecEx[ 0, 1 ], SizeOf( StopFrequency ) );
    FillChar( StopFrequency[ 0, 1 ], SizeOf( StopFrequency ), 0 );

  end;
  inherited ExecuteLoop;
end;


procedure TDevicePCICAN.ConfigCurrentDevice;
var
  storemode: Word;
begin
  SetWorkMode( 0 );
  SetDriverMode( $1B );
  B_SetInputFilter( $FF, $FF );  // ���� ��?
  SetCANSpeed( 1000 );

  // �������� ������ � ������
  storemode := GetCANReg(0);
  SetCANREG(6,$80);   //  SJW = 3 time
  SetCANReg(7,$14);
  SetCANReg(0,storemode);
  //

  SetInterruptSource($0001);
  SetCommand( 4 );  // ������� ����� ������.
end;

procedure TDevicePCICAN.Select( DeviceNumber, ChannelNumber: Integer );
begin
  SelectCAN( DeviceNumber);
  chBaseAddress( ChannelNumber );
end;

function TDevicePCICAN.GetReservedPerSecEx( DeviceNum, ChannelNum: Integer ): Cardinal;
begin
  Result:= FReservedPerSecEx[ DeviceNum, ChannelNum ];
end;

function TDevicePCICAN.GetSendedPerSecEx( DeviceNum, ChannelNum: Integer ): Cardinal;
begin
  Result:= FSendedPerSecEx[ DeviceNum, ChannelNum ];
end;

function TDevicePCICAN.GetStopReserved( DeviceNum, ChannelNum: Integer ): Boolean;
begin
  Result:= FStopReserved[ DeviceNum, ChannelNum ];
end;

function TDevicePCICAN.GetStopsPerSecEx( DeviceNum, ChannelNum: Integer ): Cardinal;
begin
  Result:= FStopsPerSecEx[ DeviceNum, ChannelNum ];
end;

procedure TDevicePCICAN.Execute;
var
  EventData: TEventData;
  ReadBuffer: TRxTxBuffer;
  SendBuffer: TRxTxBuffer;
  Block: TBlock;
  CDevice: Integer;
  CChannel: Integer;
  CStatus: Word;
  i: Integer;
  j: Integer;
  ErrID: Integer;
  WaitResult: DWORD;
  WaitInterval: Byte;

  isDataToSend: Boolean;
  _isDataToSend: Boolean;
  NeedReserveData: Boolean;
  ErrorSendTimeout: Cardinal;

begin
  try

  ErrorSendTimeout:= GetTickCount;
  CDevice:= 0;
  CChannel:= 1;
  Select( CDevice, CChannel );
  ExecuteStart;

  while not Terminated do
  begin
    ExecuteLoop;


    // ������� ���������� � ����� ��� ���������� ��������.
    if DeviceCount = 0 then
    begin
      Sleep(100);
      Continue;
    end;

    Inc( CChannel );
    if CChannel > 2 then
    begin
      CChannel:= 1;
      Inc( CDevice );
    end;
    if CDevice > DeviceCount - 1 then
    begin
      CDevice:= 0;
      // ���� �� ������ CAN ��������.
      isDataToSend:= _isDataToSend;
      _isDataToSend:= False;
    end;
    Select( CDevice, CChannel );

//    Status[ CDevice, CChannel ]:= GetStatus;



    (*
    // �������� �������.
    CStatus:= GetStatus;
    if ( CStatus and 128 = 128 ) and Assigned( OnDeviceError ) then OnDeviceError( deNoExternalDevice );
{    if ( CStatus and 64 = 64 ) and Assigned( OnDeviceError ) then
    begin
      OnDeviceError( 0 );
      P_SetRxErrorCounter(0);
      P_SetTxErrorCounter(0);
    end;
 }   if ( CStatus and 2 = 2 ) and Assigned( OnDeviceError ) then OnDeviceError( deReadBufferOverflow );
      *)


    // �������� ������.
    if DataToSend[ CDevice, CChannel ].isData then // ���� ���� ������ ��� �������� ...
    begin
      Inc( SendDataTryCount );
        _isDataToSend:= True;
      if FStopReserved[ CDevice, CChannel ] then Inc( SendFailByStopCount ) else
      begin // ... � �� �������� ����� �� CAN.
//        CStatus:= GetStatus;
//        if CStatus and 4 = 4 then
//        begin
          SendBuffer.FrameFormat:= 0;
          SendBuffer.StandartID:= ( Word( DataToSend[ CDevice, CChannel ].Data[1] ) shl 3 ) + ( DeviceNetID and 7 );
          SendBuffer.ExtendedID:= 0;
          SendBuffer.RemoteTransmitRequest:= 0;
          SendBuffer.DataLengthCode:= DataToSend[ CDevice, CChannel ].Data[2] and $F;
          for j:= 0 to 7 do SendBuffer.DataBytes[j]:= DataToSend[ CDevice, CChannel ].Data[j + 3];
          ErrID:= SetTxBuffer( @SendBuffer );
          if ErrID = 0 then
          begin
            ErrorSendTimeout:= GetTickCount;
            SetCommand( 1 );
            DataToSend[ CDevice, CChannel ].isData:= False;
          end else Inc( SendFailByErrCount );
//        end else Inc( SendFailByErrCount );
      end;
    end;

    if not DataToSend[ CDevice, CChannel ].isData and Assigned( OnReadyToSendBlock ) then OnReadyToSendBlock( CDevice, CChannel );


    // ����� ������.
    if not isDataToSend or ( GetTickCount - ErrorSendTimeout > 2000 ) then
    begin
      WaitResult:= WaitForMultipleObjects( DeviceCount * 2, @ReserveDataEvent[0, 1], False, 1 );
      NeedReserveData:= ( WaitResult <> WAIT_TIMEOUT );
      Inc( SleepCount );
    end else
      NeedReserveData:= True;

    if NeedReserveData then
    begin
      ResetEvent( ReserveDataEvent[ CDevice, CChannel ] );

      GetEventData( @EventData );
      Block[1]:= 0;
      if EventData.InterruptID = 1 then
      begin
        Block[1]:= EventData.RxTxBuffer.StandartID shr 3;
        Block[2]:= ( EventData.RxTxBuffer.StandartID and $7 shl 5 ) + EventData.RxTxBuffer.DataLengthCode;
        for j:= 0 to 7 do Block[j + 3]:= EventData.RxTxBuffer.DataBytes[j];

        if Block[1] = 1 then
        begin
          // ��������� ������ �����/����.
          if Block[3] and $F = 1 then FStopReserved[ CDevice, CChannel ]:= True;
          if Block[3] and $F = 0 then FStopReserved[ CDevice, CChannel ]:= False;
          Inc( StopFrequency[ CDevice, CChannel ] );
        end;
        if Assigned( OnBlockReservedEx ) then OnBlockReservedEx( Block, CDevice, CChannel ); // ������� ������ �����.

        Inc( Reserved );
        Inc( ReservedEx[ CDevice, CChannel ] );
      end;
   end;

  end;
  except
    on E: Exception do if Assigned( OnInternalThreadException ) then OnInternalThreadException( DeviceID, E );
  end;
end;

function TDevicePCICAN.PutBlock( B: TBlock ): Boolean;
begin
  PutBlock( B, 0, 1 );
end;

function TDevicePCICAN.PutBlock( B: TBlock; DeviceNumber, ChannelNumber: Integer; NoWait: Boolean = False ): Boolean;
var
  Tick: Cardinal;

begin
  Result:= False;
  if not TestBlock( B ) then Exit;
  Tick:= GetTickCount;
  while DataToSend[ DeviceNumber, ChannelNumber ].isData do
  begin
    if NoWait then Exit;
    Sleep( 0 );
    if GetTickCount - Tick > 2000 then Exit;
  end;
  DataToSend[ DeviceNumber, ChannelNumber ].Data:= B;
  DataToSend[ DeviceNumber, ChannelNumber ].isData:= True;
  Inc( SendedEx[ DeviceNumber, ChannelNumber ] );
  Result:= True;
end;

procedure TDevicePCICAN.DropStopFlag( DeviceNum, ChannelNum: Integer );
begin
  FStopReserved[ DeviceNum, ChannelNum ]:= False;
end;

// -----------------------------------------------------------------------------
// ----------------------------------  RS  -------------------------------------

constructor TDeviceRS.Create;
begin
  inherited Create;
  DeviceID:='COM1';
  InQueue:= 100000;
  OutQueue:= 100000;
  Baud:= 57600;
//  Priority:= tpHigher;
end;

{
function TDeviceRS.Open: Boolean;
begin
  if ComHandle > 0 then Close;
  ComHandle:= CreateFile(PChar( DeviceID ),
                         GENERIC_READ or GENERIC_WRITE,
                         0, nil, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
  Result:= (ComHandle > 0) and SetCommTiming and SetCommBuffer(InQueue, OutQueue) and SetCommStatus(Baud);
end;
}

function TDeviceRS.Open: Boolean;
var
  B1, B2, B3, B4: Boolean;
  
begin
  if ComHandle > 0 then Close;
  ComHandle:= CreateFile(PChar( DeviceID ),
                         GENERIC_READ or GENERIC_WRITE,
                         0, nil, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
  Result:= (ComHandle > 0) and SetCommTiming and SetCommBuffer(InQueue, OutQueue) and SetCommStatus(Baud);
  isOpen:= Result;
end;

function TDeviceRS.SetCommTiming: Boolean;
var
  Timeouts: TCommTimeOuts;
begin
  with TimeOuts do
  begin
    ReadIntervalTimeout := MAXDWORD; // 1;
    ReadTotalTimeoutMultiplier := 0;
    ReadTotalTimeoutConstant := 0; // 1;
    WriteTotalTimeoutMultiplier := 2;
    WriteTotalTimeoutConstant := 2;
  end;
  Result := SetCommTimeouts(ComHandle, Timeouts);
end;

function TDeviceRS.SetCommBuffer(InQueue, OutQueue: LongInt): Boolean;
begin
  Result:= SetupComm(ComHandle, InQueue, OutQueue);
end;

function TDeviceRS.SetCommStatus(Baud: Integer): Boolean;
var
  DCB: TDCB;
begin
  with DCB do
  begin
    DCBlength:=SizeOf(Tdcb);
    BaudRate := Baud;
    Flags:=12305;
//    Flags:= $2011;
    wReserved:=0;
    XonLim:=600;
    XoffLim:=150;
    ByteSize:=8;
    Parity:=0;
    StopBits:=0;
    XonChar:=#17;
    XoffChar:=#19;
    ErrorChar:=#0;
    EofChar:=#0;
    EvtChar:=#0;
    wReserved1:=65;

{    Flags:=12305;
    wReserved:=0;
    XonLim:=600;
    XoffLim:=150;
    ByteSize:=8;
    Parity:=0;
    StopBits:=0;
    XonChar:=#17;
    XoffChar:=#19;
    ErrorChar:=#0;
    EofChar:=#0;
    EvtChar:=#0;
    wReserved1:=65; }
  end;
  Result := SetCommState(ComHandle, DCB);
end;

function TDeviceRS.FPutByte(B: Byte): Boolean;
var
  Temp: Longword;
  ECount: Integer;

begin
  ECount:= 0;
  repeat
    Inc( ECount );
    WriteFile(ComHandle, B, 1, Temp, nil);
  until ( Temp = 1 ) or ( ECount > 100 );
  Result:= Temp = 1;
end;

function TDeviceRS.GetByte(out B: Byte): Boolean;
var
  Temp: LongWord;
  
begin
  ReadFile( ComHandle, B, 1, Temp, nil );
  Result:= Temp = 1;
end;

procedure TDeviceRS.Close;
begin
  CloseHandle( ComHandle );
  ComHandle:= 0;
end;

function TDeviceRS.GetComPortList: TStringList;
var
  DCB: TDCB;
  I: integer;
  Handle: THandle;
  PortName: string;

begin
  Result:= TStringList.Create;
  for I:= 0 to 31 do
  begin
    PortName:= Format('COM%d', [I + 1]);
    Handle:= CreateFile(PChar(PortName),
                        GENERIC_READ or GENERIC_WRITE,
                        0, nil, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, 0);
    if Handle <> INVALID_HANDLE_VALUE then Result.Add( PortName );
    CloseHandle(Handle);
  end;
end;

// -----------------------------------------------------------------------------
// ----------------------------------  ���  ------------------------------------

function  TDeviceAKP.Open: Boolean;
var
  i: Integer;
  r: integer;
begin
  for i:= 1 to 20 do
  begin
    AddLog('(I) InitLib start...');
    Result:= InitLib <> 0;
    if Result then Break else Sleep( 200 );
  end;
  if Result then
  begin
    AddLog('(I) InitLib complete. Power On start...');
    Result:= PowerSwitch( True );
    AddLog('(I) Open device complete.');
    // ������������ CallBack-������� ��� ������ ������
    // �������� ��� �������
    //r:=panelenable(Addr(_GetButtonsState_));
    if r<>0 then
      AddLog(Format('(I) Error setting callback function !. ErrCode: %d',[r]));
  end else
  begin
    AddLog('(E) InitLib ERROR!');
    Result:= False;
  end;
  IsOpen:= Result;
end;

function TDeviceAKP.PowerSwitch( needOn: Boolean ): Boolean;
var
  r: ShortInt;
begin
  if needON then AddLog('(I) Power ON start...') else  AddLog('(I) Power OFF start...');
  r:= pwrswitch( ord( needOn) );
  if r = 1 then
  begin
    AddLog('(I) Power swith complete!');
    Result:= True;
  end else
  begin
    AddLog('(I) Power swith ERROR (code = ' + IntToStr(r) + ')!');
    Result:= False;
  end;
end;


function TDeviceAKP.GetByte( out B: Byte ): Boolean;
begin
  Result:= ( ReadFromUSB(@B, 1, 0) = 1 );
end;


function TDeviceAKP.FPutByte( B: Byte ): Boolean;
begin
  Result:= ( WriteToUSB(@B, 1) = 1 );
end;


                                 {
function TDeviceAKP.PutByte( B: Byte ): Boolean;
var
  ECount: Integer;

begin

    n:= Bl[2] and $F + 2;
    repeat
      ecou:= 0;
      repeat
        n_:= USB.PutBlock( @BL[1], n );
        E:= (n_ = n);
        if E then Break;
        if UseLog then
          WrLog( 'S: ' + Bl2Str(Bl) + ' <<<< ERROR (repeat = ' + IntToStr( ecou + 1 ) + ')' );
        sleep( 50 );
        inc ( ecou );
      until E or (ecou >= 10);

      if SkipErrors then Break;

      if not E then
      begin
        ErrCode:= USB.TestSend;
        if ErrCode = 0 then
        begin
          if not WinMessage('����� �������� ����� ���������� �����. ���������� ��������?', 1) then
          begin
            WinMessage('', 100);
            Result:= False;
            Exit;
          end;
        end else
        begin
          if not WinMessage('������ ��������. ��� ' + IntToStr( ErrCode ) + '. ����������?', 1 ) then
          begin
            WinMessage('', 100);
            Result:= False;
            Exit;
          end;
          ErrCode:= USB.ResumeSend;
          if ErrCode <> 0 then
          begin
            WinMessage('���������� �������� �� �������!', 0);
            WinMessage('', 100);
          end;
        end;
      end;

    until E;
end;
}

procedure TDeviceAKP.Close;
var
   P: Pointer;
begin
  PowerSwitch( False );
//  Sleep( 10000 );
  AddLog('FinishLib start...');
  //panelenable(nil);
  P:=Addr(finishlib);
  finishlib;
  AddLog('FinishLib complete...');
end;

function TDeviceAKP.PutBlock( B: TBlock ): Boolean;
var
  RepeatCount: Integer;
  ErrorCode: Integer;
  Cou: Integer;

begin
  Result:= False;
  RepeatCount:= 0;
  Cou:= B[2] and $F + 2;
  while WriteToUSB( @B, Cou ) <> Cou do
  begin
    Sleep( 50 );
    Inc ( RepeatCount );
    if RepeatCount >= 10 then
    begin
      ErrorCode:= get_trstate;
      ErrorCode:= 0;
      if ErrorCode = 0 then
        AddLog( DeviceID + ' S: ' + BlockAsString(B) + ' <-- FAULT! AKP send buffer is full (no error code reserved).' )
      else
        AddLog( DeviceID + ' S: ' + BlockAsString(B) + Format( ' <-- FAULT! AKP error #%x', [ ErrorCode ] ) );
      if Assigned( OnSendError ) then OnSendError;
      Exit;
    end;
  end;
  if RepeatCount = 0 then AddLog( DeviceID + ' S: ' + BlockAsString(B) ) else AddLog( DeviceID + ' S: ' + BlockAsString(B) + ' (successful try #' + IntToStr( RepeatCount + 1 ) + ')' );
  Result:= True;
end;


{ TDeviceMAKP }
procedure TDeviceMAKP.BlockReceived(Block: TBlock; ID: Byte);
var
  TestResult: Word;
begin
  AddLog( IntToStr( ID ) + ' R: ' + BlockAsString( Block ) );
  {if Block[1]=$DB then
    begin
      TestResult:= Block[4] shl 8 + Block[3];
      if TestResult<>0 then
        begin
          FBUMReady[ID]:=true;
          FBUM_SN[ID]:=TestResult;
        end;
    end;}
//{$IFDEF TESTKEYS}
// if not LockCANThreads then
//{$ENDIF}
  if Assigned( OnBlockReservedMas[ID] ) then
    OnBlockReservedMas[ID]( Block );
end;

procedure TDeviceMAKP.Close;
var
  i: Integer;
begin
  FSearchForDevices:=false;
  {for i:=0 to 15 do
     if FAKPState[i] then power_offM(i);}
  for i:=0 to MaxBUM-1 do //15
    if FAKPState[i] then pwrswitchM(0, i);
  AddLog('FinishLib start...');
  finishlibM;
  AddLog('FinishLib complete...');
end;

constructor TDeviceMAKP.Create;
var
  i: Integer;
begin
  FErrUakkCount:= 0;
  for i:=0 to 15 do
     begin
       FAKPState[i]:=false;
       FBUMReady[i]:=false;
       OnBlockReservedMas[i]:=nil;
       FOnByteReservedMas[i]:=nil;
       FCurrentBlockPositionM[i]:=1;
     end;

  FBUMTestBlock[1]:=$DE;
  FBUMTestBlock[2]:=0;
  FBUMTestBlock[3]:=0;
  FBUMTestBlock[4]:=0;
  FBUMTestBlock[5]:=0;
  FBUMTestBlock[6]:=0;
  FBUMTestBlock[7]:=0;
  FBUMTestBlock[8]:=0;
  FBUMTestBlock[9]:=0;
  FBUMTestBlock[10]:=0;
  FSearchForDevices:=false;
  AKPConnected:=nil;
  AKPDisconnected:=nil;
  inherited;
end;

procedure TDeviceMAKP.DeviceSearch;
var
  i: Byte;
  B: Integer;
  OldState: Boolean;
  Bl: TBlock;
  Res: Boolean;
begin
  if ((GetTickCount-FTick) > 500)then
    begin
      FTick:=GetTickCount;
      AddLog('(I) InitLib start...');
      B:=initlibM;
      for i:= 0 to MaxBUM-1 do //15
         begin
           OldState:=FAKPState[i];
           FAKPState[i]:= B and ( 1 shl (16 + i) ) <> 0;
           if (not OldState) and (FAKPState[i]) then
             begin
               //power_onM(i);
               AddLog(Format('(I) InitLib complete. Power On ID: %d start...',[i]));
               pwrswitchM(1, i);
               AddLog(Format('(I) Open device ID: %d complete.',[i]));
               // ��������� ���� ��� ��������� ���
              { Bl[1]:=$DE; Bl[2]:=0; Bl[3]:=0; Bl[4]:=0; Bl[5]:=0;
               Bl[6]:=0; Bl[7]:=0; Bl[8]:=0; Bl[9]:=0; Bl[10]:=0;
               Res:=Self.PutBlock(Bl, i);}
               if Assigned(AKPConnected) then AKPConnected(i);
             end;
           if (OldState) and (not FAKPState[i]) then
             begin
               FBUMReady[i]:=false;
               if Assigned(AKPDisconnected) then AKPDisconnected(i);
             end;
         end;
    end;
end;

procedure TDeviceMAKP.Execute;
var
  i: Integer;
  ByteRecieved: Boolean;
  B: Byte;
  BlockRes: Boolean;

begin
  //inherited;
  try
  ExecuteStart;
  while not Terminated do
    begin
      if FSearchForDevices then DeviceSearch;
      ExecuteLoop;
      // �������� ���� ����� ������
      ByteRecieved:=false;
{$IFDEF TESTKEYS}
 if not LockCANThreads then
{$ENDIF}
      for i:=0 to MaxBUM-1 do //15
         if FAKPState[i] then
           if GetByte( B, i ) then
             begin
               ByteRecieved:=true;
               // ������� ����.
               Inc( FReservedM[i] );
               if Assigned( FOnByteReservedMas[i] ) then FOnByteReservedMas[i]( B );
               if ProcessBlocks then
                 begin
                   if FCurrentBlockPositionM[i] <= {High( FCurrentBlockM[i] )} 10 then FCurrentBlockM[i][ FCurrentBlockPositionM[i] ]:= B;
                   if UseExBlocks then
                     BlockRes:= FCurrentBlockPositionM[i] >= 2 + FCurrentBlockM[i][2]
                   else
                   BlockRes:= FCurrentBlockPositionM[i] >= 2 + FCurrentBlockM[i][2] and $F;
                   if BlockRes then
                     begin
                       // �������� �������.
                       BlockReceived( FCurrentBlockM[i], i );
                       FCurrentBlockPositionM[i]:= 0;
                      end;
                   Inc( FCurrentBlockPositionM[i] );
                 end;
             end;

          // ���� �� ������ �� ������ ����� �� �� ������ ����������, �� ����
      if not ByteRecieved then Sleep( SleepInterval );
    end;
  except
    on E: Exception do if Assigned( OnInternalThreadException ) then OnInternalThreadException( DeviceID, E );
  end;
end;

procedure TDeviceMAKP.ExecuteLoop;
var
  i: Byte;
  B: Integer;
  OldState, Res: Boolean;
  Bl: TBlock;
begin
  inherited;
  {for i:=0 to 15 do
     if (FAKPState[i]) and (not FBUMReady[i]) then
       begin
         if GetTickCount-FBUMTimeOut[i] > 1000 then
           begin
             Res:=Self.PutBlock(FBUMTestBlock, i);
             FBUMTimeOut[i]:=GetTickCount;
           end;
       end;}
       
  {Bl[1]:=$DE; Bl[2]:=0; Bl[3]:=0; Bl[4]:=0; Bl[5]:=0;
  Bl[6]:=0; Bl[7]:=0; Bl[8]:=0; Bl[9]:=0; Bl[10]:=0;
  for i:= 0 to 15 do
     if FAKPState[i] then
       Res:=Self.PutBlock(Bl, i);  }
  {if ((GetTickCount-FTick) > 500) and (FSearchForDevices) then
    begin
      FTick:=GetTickCount;
      B:=initlibM;
      for i:= 0 to 15 do
        begin
          OldState:=FAKPState[i];
          FAKPState[i]:= B and ( 1 shl (16 + i) ) <> 0;
          if (not OldState) and (FAKPState[i]) then
            begin
              power_onM(i);
              if Assigned(AKPConnected) then AKPConnected(i);
            end;
          if (OldState) and (not FAKPState[i]) then
            if Assigned(AKPDisconnected) then AKPDisconnected(i);
        end;
    end;}
end;

function TDeviceMAKP.GetAKPState(ID: Integer): Boolean;
begin
  //Result:=FAKPState[ID] and FBUMReady[ID];
  Result:=FAKPState[ID];
end;

{function TDeviceMAKP.GetByte(out B: Byte): Boolean;
begin
  Result:= ( ReadFromUSBM(@B, 1, 0, 0) = 1 );
end;}

{
function TDeviceMAKP.GetBUMSN(ID: Byte): Word;
begin
  Result:=FBUM_SN[ID];
end;
}

function TDeviceMAKP.GetByte(out B: Byte; ID: Byte): Boolean;
begin
  Result:= ( ReadFromUSBM(@B, 1, 0, ID) = 1 );
end;

function TDeviceMAKP.GetDeviceCount: Integer;
var
  i,k: Integer;
begin
  k:=0;
  for i:=0 to 15 do
    //if FAKPState[i] then Inc(k);
    if FBUMReady[i] then Inc(k);
  Result:=k;
end;

function TDeviceMAKP.GetU2CSerial( ID: byte): integer;
begin
  result:= GetU2CSerialNum(ID);
end;

function TDeviceMAKP.GetUAKK: Integer;
begin
  result :=0;
  if FErrUakkCount >= 1 then Exit;
    result:= GUAKK;
  if result < 0 then
    Inc(FErrUakkCount)
  else
    FErrUakkCount:= 0;
end;

function TDeviceMAKP.GetU2CVersion( PBuffer: Pointer;  ID: byte ): integer;
begin
  result := GetVersion(PBuffer, ID);
end;

function TDeviceMAKP.Open: Boolean;
begin
  FSearchForDevices:=true;
  Result:=true;
  isOpen:=Result;
end;

{function TDeviceMAKP.PowerSwitch(needOn: Boolean): Boolean;
begin

end; }

function TDeviceMAKP.PutBlock(B: TBlock): Boolean;
var
  RepeatCount: Integer;
  ErrorCode: Integer;
  Cou: Integer;

begin
  Result:=PutBlock(B, 0);
end;

{function TDeviceMAKP.PutByte(B: Byte): Boolean;
begin
  Result:= ( WriteToUSBM(@B, 1, 0) = 1 );
end;}

function TDeviceMAKP.PutBlock(B: TBlock; ID: Byte): Boolean;
var
  RepeatCount: Integer;
  ErrorCode, ErrDlg: Integer;
  Cou: Integer;

begin
  Result:= False;
  RepeatCount:= 0;
  Cou:= (B[2] and $F) + 2;
  while WriteToUSBM( @B, Cou, ID ) <> Cou do
  begin
    Sleep( 50 );
    Inc ( RepeatCount );
    if RepeatCount >= 10 then
    begin
      AddLog( IntToStr( ID ) + ' S: ' + BlockAsString(B) + ' <-- FAULT! Unable to send block 10 times in 500 ms.' );
      if Assigned( OnSendError ) then OnSendError;
      Exit;
    end;
  end;
  if RepeatCount = 0 then AddLog( IntToStr( ID ) + ' S: ' + BlockAsString(B) ) else AddLog( DeviceID + ' S: ' + BlockAsString(B) + ' (successful try #' + IntToStr( RepeatCount + 1 ) + ')' );
  Result:= True;
end;

function TDeviceMAKP.PutByte(B, ID: Byte): Boolean;
begin
  Result:= ( WriteToUSBM(@B, 1, ID) = 1 );
end;

procedure TDeviceMAKP.StopDeviceSearching;
begin
  FSearchForDevices:=false;
end;

procedure TDeviceAKP.GetButtonsState(B: Byte);
var
  But: TConsolButtons;
  State: Boolean;
begin
  State:=(128 and B) = 128;
  if (B=128) or (B=0) then But:=cbCoord;
  if (B=129) or (B=1) then But:=cbBoltJoint;
  if (B=130) or (B=2) then But:=cbWeldJoint;
  if Assigned(OnButtonStateChange) then
    OnButtonStateChange(But, State);
end;

constructor TDeviceAKP.Create;
begin
  inherited;
  Test:=3;
  OnButtonStateChange:=nil;
  cDeviceAKP:= self;
end;

function TDeviceAKP.GetUAKK: Integer;
begin
  //result:=uakk;
  {$IFDEF EnableUKK}
  result:= getUAKK;
  {$ENDIF}
end;


procedure TDeviceAKP.InitPanel;
var
  r: Integer;
  
begin
    // ������������ CallBack-������� ��� ������ ������
{    r:=panelenable(Addr(_GetButtonsState_));
    if r<>0 then
      AddLog(Format('(I) Error setting callback function !. ErrCode: %d',[r]));}
end;

{ TRemotePanel }

constructor TRemotePanel.Create;
begin
  cRemotePanel:= self;
end;

destructor TRemotePanel.Destroy;
begin
  finishlib;
  inherited;
end;

procedure TRemotePanel.GetButtonsState(B: Byte);
var
  But: TConsolButtons;
  State: Boolean;
begin
  State:=(128 and B) = 128;
  if (B=128) or (B=0) then But:=cbCoord;
  if (B=129) or (B=1) then But:=cbBoltJoint;
  if (B=130) or (B=2) then But:=cbWeldJoint;
  if Assigned(OnButtonStateChange) then
    OnButtonStateChange(But, State);
end;

function TRemotePanel.GetUAKK: Integer;
begin
//  result:=uakk;
end;

function TRemotePanel.Open: Boolean;
var
  r, i: Integer;
begin
  for i:= 1 to 30 do
  begin
    Result:= InitLib <> 0;
    if Result then Break else Sleep( 200 );
  end;
  if Result then
  begin
    Result:= PowerSwitch( True );
    // ������������ CallBack-������� ��� ������ ������
//    r:=panelenable(Addr(_GetButtonsState_RemotePanel));
  end else
  begin
    Result:= False;
  end;
end;

function TRemotePanel.PowerSwitch(needOn: Boolean): Boolean;
var
  r: Integer;
begin
  r:= pwrswitch( ord( needOn) );
  if r = 1 then
  begin
    Result:= True;
  end else
  begin
    Result:= False;
  end;
end;

end.



