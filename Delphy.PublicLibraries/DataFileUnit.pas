unit DataFileUnit;

interface

uses Classes, SysUtils;

type

  TDataFile = class
  private
    FCashData: Boolean;
    FFilename: string;

  protected
    function GetPosition: Int64;
    procedure SetPosition( Position: Int64 );
    function GetSize: Int64;

  public
    Stream: TStream;

    property Position: Int64 read GetPosition write SetPosition;
    property Size: Int64 read GetSize;

    constructor Create( CashData: Boolean; Filename: string; Mode: Word );
    destructor Destroy; override;
    function GetControlSumm( SkipPosition: Int64 = -1 ): Word;
    function Seek(Offset: Longint; Origin: Word): Longint;

    procedure Flip( Append: Boolean; AppendPosition: Int64 = -1 );
    procedure Clear;

    // ������.
    function Read(var Buffer; Count: Longint): Longint;
    function ReadString( Ln: Byte = 0 ): string;
    function ReadByte: Byte;
    function ReadInteger: SmallInt;
    function ReadWord: Word;
    function ReadLongWord: LongWord;
    function ReadInt64: Int64;
    function ReadSingle: Single;
    function ReadDouble: Double;
    function ReadDateTime: TDateTime;
    // ������.
    function Write(const Buffer; Count: Longint): Longint;
    procedure WriteString( V: string; FixedLength: Boolean = False );
    procedure WriteByte(V: Byte);
    procedure WriteInteger(V: SmallInt);
    procedure WriteWord(V: Word);
    procedure WriteLongWord(V: LongWord);
    procedure WriteInt64(V: Int64);
    procedure WriteSingle(V: Single);
    procedure WriteDouble(V: Double);
    procedure WriteDateTime(V: TDateTime);

    property GetFileName : string read FFilename;
  end;




implementation

//                            --- �������� ���� ---

function TDataFile.GetControlSumm( SkipPosition: Int64 = -1 ): Word;
var
  B: Byte;
  StreamSize: Int64;
  P: Int64;
  OldPosition: Int64;

begin
  OldPosition:= Stream.Position;
  Result:= 0;
  if not FCashData then Exit;
  StreamSize:= Stream.Size;
  Stream.Seek( 0, soFromBeginning	);
  P:= 0;
  while P + 1 < StreamSize do
  begin
    P:= Stream.Position;
    Stream.Read( B, 1 );
    if P <> SkipPosition then Result:= ( Result + B ) and $00FF;
  end;
  Stream.Seek( OldPosition, soFromBeginning );
end;

function TDataFile.Seek(Offset: Longint; Origin: Word): Longint;
begin
  Result:= Stream.Seek( Offset, Origin );
end;

procedure TDataFile.Flip( Append: Boolean; AppendPosition: Int64 = -1 );
{ �������� ������, ���� ������ ��� ������ � ������ CashData.
  ���� Append, ���������� ������������ ������ � ���� � ������� ����� ��� ��������� ������.
  ���� not Append, ������������ ������������ (��� �������) ����.
}

var
  FileStream: TStream;

begin
  if not FCashData then Exit;
  if Append and FileExists( FFilename ) then
  begin
    FileStream:= TFileStream.Create( FFileName, fmOpenWrite );
    if AppendPosition = -1 then FileStream.Seek( 0, soFromEnd	) else FileStream.Seek( AppendPosition, soFromBeginning );
  end else
    FileStream:= TFileStream.Create( FFileName, fmCreate );
  try
    TMemoryStream( Stream ).SaveToStream( FileStream );
  finally
    FileStream.Free;
  end;
end;

procedure TDataFile.Clear;
begin
  if FCashData then TMemoryStream( Stream ).Clear;
end;


function TDataFile.Read(var Buffer; Count: Longint): Longint;
begin
  Result:= Stream.Read( Buffer, Count );
end;

function TDataFile.ReadString( Ln: Byte = 0 ): string;
var
 S: string;
begin
  If Ln = 0 then Read(Ln, 1);
  SetLength( S, Ln );
  if Ln <> 0 then Read( S[1], Ln );
  Result:= S;
end;

function TDataFile.ReadByte: Byte;
var
 V: Byte;
begin
  Read(V, 1);
  Result:= V;
end;

function TDataFile.ReadInteger: SmallInt;
var
 V: SmallInt;
begin
  Read(V, 2);
  Result:= V;
end;


function TDataFile.ReadWord: Word;
var
 V: Word;
begin
  Read(V, 2);
  Result:= V;
end;

function TDataFile.ReadLongWord: LongWord;
var
 V: LongWord;
begin
  Read(V, 4);
  Result:= V;
end;

function TDataFile.ReadInt64: Int64;
var
  V: Int64;
begin
  Read(V, 8);
  Result:= V;
end;

function TDataFile.ReadSingle: Single;
var
 V: Single;
begin
  Read(V, 4);
  Result:= V;
end;

function TDataFile.ReadDouble: Double;
var
 V: Double;
begin
  Read(V, 8);
  Result:= V;
end;


function TDataFile.ReadDateTime: TDateTime;
var
 V: TDateTime;
begin
  Read(V, 8);
  Result:= V;
end;

// -----------------------------------------------
// ---                ������                   ---

function TDataFile.Write(const Buffer; Count: Longint): Longint;
begin
  Result:= Stream.Write( Buffer, Count );
end;


procedure TDataFile.WriteString( V: string; FixedLength: Boolean = False );
var
  L: Byte;
begin
  L:= Length(V);
  if not FixedLength then Write(L, 1);
  if L <> 0 then Write(V[1], L);
end;

procedure TDataFile.WriteByte(V: Byte);
begin
  Write(V, 1);
end;

procedure TDataFile.WriteInteger(V: SmallInt);
begin
  Write(V, 2);
end;

procedure TDataFile.WriteWord(V: Word);
begin
  Write(V, 2);
end;

procedure TDataFile.WriteLongWord(V: LongWord);
begin
  Write(V, 4);
end;

procedure TDataFile.WriteInt64(V: Int64);
begin
  Write(V, 8);
end;


procedure TDataFile.WriteSingle(V: Single);
begin
  Write(V, 4);
end;

procedure TDataFile.WriteDouble(V: Double);
begin
  Write(V, 8);
end;


procedure TDataFile.WriteDateTime(V: TDateTime);
begin
  Write(V, 8);
end;



constructor TDataFile.Create( CashData: Boolean; Filename: string; Mode: Word );
begin
  FFilename:= Filename;
  FCashData:= CashData;
  if FCashData then
  begin
    Stream:= TMemoryStream.Create;
    if Mode in [fmOpenRead, fmOpenReadWrite] then TMemoryStream( Stream ).LoadFromFile( Filename );
  end else
  begin
    Stream:= TFileStream.Create( FileName, Mode )
  end;
end;

destructor TDataFile.Destroy;
begin
  Stream.Free;
end;

function TDataFile.GetPosition: Int64;
begin
  Result:= Stream.Position;
end;

procedure TDataFile.SetPosition( Position: Int64 );
begin
  Stream.Position:= Position;
end;

function TDataFile.GetSize: Int64;
begin
  Result:= Stream.Size;
end;

end.
