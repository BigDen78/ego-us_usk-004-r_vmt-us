unit MnemoUnit;

interface

uses ExtCtrls, Windows, Classes, Graphics, Controls, DefCoreUnit, SysUtils, UtilsUnit,
     CustomInterfaceUnit, MCAN, Types, PublicData, AviconDataContainer, AviconTypes, Math, Forms, Dialogs, ConfigObj, CfgTablesUnit_2010, PublicFunc;

{$I Directives.inc}

type
    TKanalASD = record
      Time: Int64;
      Flag: Boolean;
    end;

    TOneEcho = record
      A: Byte;
      T: Byte;
    end;

    TTestKanalsASD = array [0 .. 1, 0 .. 14] of TKanalASD;

    TMnemoMode = (mmDefault, mmTwoEcho);

    TSimpleMnemoSegmentsOneRail = array of TImage;
    TSimpleMnemoSegments = array of TSimpleMnemoSegmentsOneRail;

    TMnemoSegmentsContainer = class
      private
        FRailCnt: integer;
        FChCnt: integer;
        FPicsPath: string;
        FSegments: TSimpleMnemoSegments;
        FBase: TImage;
      protected
        function GetSegment(Index1, Index2: integer): TImage;
      public
        constructor Create(RailCnt, ChCnt: integer; PicsPath: string);
        destructor Destroy; override;
        procedure AddSegment(R, Ch: integer; FileName: string);
        procedure AddBase(FileName: string);
        property Base: TImage read FBase;
        property Segments[Index1, Index2: integer]: TImage read GetSegment; default;
      end;

    TCustomMnemoScheme = class(TCustomInterfaceObject)
      private
        FRailCount: Integer;
        FChCount: Integer;
        FSegments: TMnemoSegmentsContainer;
        FTestKanals: TTestKanalsASD;
        FLastDelay: Integer;
      protected
        procedure DrawBackGround; override;
        procedure DrawForeGround; override;
        procedure KanalASDOn(Rail, Ch: Integer); virtual;
      public
        constructor Create(AOwner: TComponent; RailCnt, ChCnt: integer; PicsPath: string); virtual;
        destructor Destroy; override;
        procedure TestKanal(Rail, Num, Delay, Amp: integer);
      end;

    TAV16Scheme1Mnemo = class(TCustomMnemoScheme)
      public
        constructor Create(AOwner: TComponent; RailCnt, ChCnt: integer; PicsPath: string); override;
    end;

    TAV16Scheme2Mnemo = class(TCustomMnemoScheme)
      public
        constructor Create(AOwner: TComponent; RailCnt, ChCnt: integer; PicsPath: string); override;
    end;

    TAV16Scheme3Mnemo = class(TCustomMnemoScheme)
      public
        constructor Create(AOwner: TComponent; RailCnt, ChCnt: integer; PicsPath: string); override;
    end;

    TWheelPos = array [0 .. 1, 1 .. 2] of integer; // ��������� ����� � ��������. 0-1 - ����� 1-2 - ������ ��� ������ ������
    THeadY = array [0 .. 1] of integer; // ���������� Y ����������� �������, 0-1 �����

    TAV16WheelsMnemo = class(TCustomMnemoScheme)
      private
        FKW, FKH: Single;
        FPicWidth, FPicHeight: integer;
        // ���������� � �������� ����� ��������� ����� � ������ �� ��������
        FWheelX, FWheelY: TWheelPos;
        // ����������� ����������� �������
        FHeadY: THeadY;
        // ������ ������� � ������ ������ � �������� (��� ������ 0)
        FHeadHeight, FRailHeight, FRailHeight0, FRailHeight1, FRailHeight2: integer;
        // ������ ������� � �������� (��� ������ 58)
        FHeadHeight58: integer;
        // ������ ������ � �������� (��� ������ 43)
        FRailHeight42: integer;
        // ������ ������� � �������� (��� ������ 70)
        FHeadHeight70: integer;
        FTgAlfa, FTgBetta, FTgGamma: Single;
        Alfa42_1, Alfa42_2, Alfa58_1, Alfa58_1_P1, Alfa58_1_P2, Alfa58_2, Alfa58_2_P1, Alfa58_2_P2, Alfa70_1, Alfa70_2: Single;
        Max42_1, Max42_2, Max58_1, Max58_1_P1, Max58_1_P2, Max58_2, Max58_2_P1, Max58_2_P2, Max70_1, Max70_2: integer;
        MaxZero1, MaxZero2: integer;
        PointF58_1X, PointF58_2X, PointB58_1X, PointB58_2X: THeadY;
        FBottomTKP1: Byte;
        FBottomTKP2: Byte;
        FPointR: integer;
        WlSideOffset : integer;
        SegmentSize : integer;

        // �������� ��� ���������� ��� VMT_US Kair:
        // Alfa + ����� ������ - ���� ���� �� ��������
        // MaX + ����� ������ ������ ���� �� ��������
        Alfa6, Alfa7, Alfa3, Alfa2, Alfa11, Alfa10, Alfa5, Alfa4, Alfa13, Alfa12 : Single;
        Max6, Max7, Max3, Max2, Max11, Max10, Max5, Max4, Max13, Max12 : integer;
        X45_90 : integer;

        procedure DrawBackGround; override;
        procedure DrawForeGround; override;
        function BotY(X: integer): integer;
        function TopY(X: integer): integer;
        function TopY0(X: integer): integer;
        function TopY1(X: integer): integer;
        function TopY2(X: integer): integer;
        function HBotY(X: integer): integer;
        procedure DrawWheel(Nit, WheelNum: integer; AK: Boolean);
        procedure DrawChannel(Nit, Kanal, Delay, Amp: integer);
        procedure CountLines;
      public
        constructor Create(AOwner: TComponent; RailCnt, ChCnt: integer; PicsPath: string); override;
    end;

    TUSK004RSchemeMnemo = class(TCustomMnemoScheme)
      public
        constructor Create(AOwner: TComponent; RailCnt, ChCnt: integer; PicsPath: string); override;
    end;

    TVMT_USSchemeMnemo = class(TCustomMnemoScheme)
      public
        constructor Create(AOwner: TComponent; RailCnt, ChCnt: integer; PicsPath: string); override;
    end;



implementation

{ TSimpleMnemoSegmentsContainer }

procedure TMnemoSegmentsContainer.AddBase(FileName: string);
begin
  if not Assigned(FBase) then
    FBase := TImage.Create(nil);
  FBase.Picture.LoadFromFile(FPicsPath + FileName);
end;

procedure TMnemoSegmentsContainer.AddSegment(R, Ch: integer;
  FileName: string);
begin
  if not((R <= High(FSegments)) and (Ch <= High(FSegments[0]))) then
    Exit;
  if not Assigned(FSegments[R, Ch]) then
    FSegments[R, Ch] := TImage.Create(nil);
  FSegments[R, Ch].Picture.LoadFromFile(FPicsPath + FileName);
end;

constructor TMnemoSegmentsContainer.Create(RailCnt, ChCnt: integer;
  PicsPath: string);
var
  i, j: integer;
begin
  FRailCnt := RailCnt;
  FChCnt := ChCnt;
  FPicsPath := PicsPath;
  SetLength(FSegments, FRailCnt);
  for i := 0 to High(FSegments) do
  begin
    SetLength(FSegments[i], FChCnt);
    for j := 0 to High(FSegments[i]) do
      FSegments[i, j] := nil;
  end;
end;

destructor TMnemoSegmentsContainer.Destroy;
var
  i, j: integer;
begin
  SetLength(FSegments, FRailCnt);
  for i := 0 to High(FSegments) do
  begin
    for j := 0 to High(FSegments[i]) do
    begin
      FSegments[i, j].Free;
      FSegments[i, j] := nil;
    end;
    SetLength(FSegments[i], 0);
  end;
  SetLength(FSegments, 0);
  FBase.Free;
  inherited;
end;

function TMnemoSegmentsContainer.GetSegment(Index1,
  Index2: integer): TImage;
begin
 Result := nil;
  if not((Index1 <= High(FSegments)) and (Index2 <= High(FSegments[0]))) then
    Exit;
  Result := FSegments[Index1, Index2];
end;

{ TCustomMnemoScheme }

constructor TCustomMnemoScheme.Create(AOwner: TComponent; RailCnt, ChCnt: integer; PicsPath: string);
var
  i, j: Integer;
begin
  inherited Create(AOwner);
  FLastDelay:= 0;
  FRailCount:= RailCnt;
  FChCount:= ChCnt;
  FSegments:= TMnemoSegmentsContainer.Create(RailCnt, ChCnt, PicsPath);
  for i:= 0 to RailCnt - 1 do
     for j:= 0 to FChCount - 1 do
        FTestKanals[i, j].Flag:= false;
end;

destructor TCustomMnemoScheme.Destroy;
begin
  FSegments.Free;
  inherited;
end;

procedure TCustomMnemoScheme.DrawBackGround;
begin
  inherited;
  if Assigned(FSegments) then
    if Assigned(FSegments.Base) then
    begin
      FBackGrBmp.Canvas.Brush.Color := clWhite;
      FBackGrBmp.Canvas.FillRect(Rect(0, 0, FBackGrBmp.Width, FBackGrBmp.Height));
      FBackGrBmp.Canvas.StretchDraw(Rect(0, 0, FBackGrBmp.Width, FBackGrBmp.Height),
        FSegments.Base.Picture.Metafile);
    end;
end;

procedure TCustomMnemoScheme.DrawForeGround;
var
  i,j: Integer;
begin
  for i := 0 to FRailCount - 1 do
    for j := 0 to FChCount - 1 do
      begin
        if j <= High(Kanals[i]) then
          if Assigned(Kanals[i, j]) then
            if (Kanals[i, j].ASD) or (FTestKanals[i, j].Flag) then KanalASDOn(i, j);
      end;
end;

procedure TCustomMnemoScheme.KanalASDOn(Rail, Ch: Integer);
var
  a: Integer;
begin
  a:= Ch;
{$IFnDEF USK004R}
  if Ch in [10, 11] then a := Ch - 8;
{$ENDIF}
  if Assigned(FSegments) then
    if Assigned(FSegments[Rail, a]) then
      FBmp.Canvas.StretchDraw(Rect(0, 0, FBmp.Width, FBmp.Height), FSegments[Rail, a].Picture.Graphic);
end;

procedure TCustomMnemoScheme.TestKanal(Rail, Num, Delay, Amp: integer);
begin
  //if FLastDelay=Delay then
  if Delay = 0 then
    FTestKanals[Rail, Num].Flag:= not FTestKanals[Rail, Num].Flag
  else
    FLastDelay:=Delay;
  FTestKanals[Rail, Num].Time:= Delay;
  Self.Refresh;
end;

constructor TUSK004RSchemeMnemo.Create(AOwner: TComponent; RailCnt,
  ChCnt: integer; PicsPath: string);
begin
  inherited;
//  FSegments.AddBase('1-NULL.emf');
//  FSegments.AddBase('USK004R.png');
  FSegments.AddBase('USK004R.emf');
  FSegments.AddSegment(0, 0, '0ZTM-L.wmf');
  FSegments.AddSegment(0, 1, '1-echo-L.wmf');
  FSegments.AddSegment(0, 3, '1-58O-U.wmf');
  FSegments.AddSegment(0, 11,'1-58O-W.wmf');
  FSegments.AddSegment(0, 2, '1-58N-U.wmf');
  FSegments.AddSegment(0, 10,'1-58N-W.wmf');
  FSegments.AddSegment(0, 4, '1-70N-L.wmf');
  FSegments.AddSegment(0, 5, '1-70O-L.wmf');
  FSegments.AddSegment(0, 6, '1-42NW-L.wmf');
  FSegments.AddSegment(0, 7, '1-42OW-L.wmf');
  FSegments.AddSegment(0, 8, '1-42NB-L.wmf');
  FSegments.AddSegment(0, 9, '1-42OB-L.wmf');
{
  FSegments.AddSegment(1, 0, '0ZTM-R.wmf');
  FSegments.AddSegment(1, 1, '1-echo-R.wmf');
  FSegments.AddSegment(1, 4, '1-70N-R.wmf');
  FSegments.AddSegment(1, 5, '1-70O-R.wmf');
  FSegments.AddSegment(1, 6, '1-42NW-R.wmf');
  FSegments.AddSegment(1, 7, '1-42OW-R.wmf');
  FSegments.AddSegment(1, 8, '1-42NB-R.wmf');
  FSegments.AddSegment(1, 9, '1-42OB-R.wmf');   }
end;


{ TAV16Scheme2Mnemo }

constructor TAV16Scheme1Mnemo.Create(AOwner: TComponent; RailCnt,
  ChCnt: integer; PicsPath: string);
begin
  inherited;
  //FSegments.AddBase('1-NULL.wmf');
  FSegments.AddBase('1-NULL.emf');
  FSegments.AddSegment(0, 2, '1-58N-L.wmf');
  FSegments.AddSegment(1, 2, '1-58N-R.wmf');
  FSegments.AddSegment(0, 0, '0ZTM-L.wmf');
  FSegments.AddSegment(0, 1, '1-echo-L.wmf');
  FSegments.AddSegment(0, 3, '1-58O-L.wmf');
  FSegments.AddSegment(0, 4, '1-70N-L.wmf');
  FSegments.AddSegment(0, 5, '1-70O-L.wmf');
  FSegments.AddSegment(0, 6, '1-42NW-L.wmf');
  FSegments.AddSegment(0, 7, '1-42OW-L.wmf');
  FSegments.AddSegment(0, 8, '1-42NB-L.wmf');
  FSegments.AddSegment(0, 9, '1-42OB-L.wmf');

  FSegments.AddSegment(1, 0, '0ZTM-R.wmf');
  FSegments.AddSegment(1, 1, '1-echo-R.wmf');
  FSegments.AddSegment(1, 3, '1-58O-R.wmf');
  FSegments.AddSegment(1, 4, '1-70N-R.wmf');
  FSegments.AddSegment(1, 5, '1-70O-R.wmf');
  FSegments.AddSegment(1, 6, '1-42NW-R.wmf');
  FSegments.AddSegment(1, 7, '1-42OW-R.wmf');
  FSegments.AddSegment(1, 8, '1-42NB-R.wmf');
  FSegments.AddSegment(1, 9, '1-42OB-R.wmf');
end;

{ TAV16Scheme2Mnemo }

constructor TAV16Scheme2Mnemo.Create(AOwner: TComponent; RailCnt,
  ChCnt: integer; PicsPath: string);
begin
  inherited;
  //FSegments.AddBase('2-NULL.wmf');
  FSegments.AddBase('2-NULL.emf');
  FSegments.AddSegment(0, 2, '2-58N-L.wmf');
  FSegments.AddSegment(1, 2, '2-58N-R.wmf');

  FSegments.AddSegment(0, 0, '0ZTM-L.wmf');
  FSegments.AddSegment(0, 1, '1-echo-L.wmf');
  FSegments.AddSegment(0, 3, '1-58O-L.wmf');
  FSegments.AddSegment(0, 4, '1-70N-L.wmf');
  FSegments.AddSegment(0, 5, '1-70O-L.wmf');
  FSegments.AddSegment(0, 6, '1-42NW-L.wmf');
  FSegments.AddSegment(0, 7, '1-42OW-L.wmf');
  FSegments.AddSegment(0, 8, '1-42NB-L.wmf');
  FSegments.AddSegment(0, 9, '1-42OB-L.wmf');

  FSegments.AddSegment(1, 0, '0ZTM-R.wmf');
  FSegments.AddSegment(1, 1, '1-echo-R.wmf');
  FSegments.AddSegment(1, 3, '1-58O-R.wmf');
  FSegments.AddSegment(1, 4, '1-70N-R.wmf');
  FSegments.AddSegment(1, 5, '1-70O-R.wmf');
  FSegments.AddSegment(1, 6, '1-42NW-R.wmf');
  FSegments.AddSegment(1, 7, '1-42OW-R.wmf');
  FSegments.AddSegment(1, 8, '1-42NB-R.wmf');
  FSegments.AddSegment(1, 9, '1-42OB-R.wmf');
end;

{ TAV16WheelsMnemo }

function TAV16WheelsMnemo.BotY(X: integer): integer;
begin
  if X < FBackGrBmp.Width / 2 then
    Result := Round(FBackGrBmp.Height - X * FTgAlfa)
  else
    Result := Round(FBackGrBmp.Height - (FBackGrBmp.Width - X) * FTgAlfa)
end;

{$IFDEF VMT_US}
procedure TAV16WheelsMnemo.CountLines;
var
  //R, w, H, X42, X58, X70: integer;
  //s, a, B, c, D: Single;
  X42, X65: integer;
  s, a, B, c, D: Single;

begin
  FWheelX[0, 2] := Round(FBackGrBmp.Width * 0.19);
  FWheelX[0, 1] := Round(FBackGrBmp.Width * 0.25);
  FWheelX[1, 2] := Round(FBackGrBmp.Width - Self.Width * 0.19);
  FWheelX[1, 1] := Round(FBackGrBmp.Width - Self.Width * 0.25);

  FRailHeight := Round((FBackGrBmp.Width * 411 / 755) * 15.1 / 16.5);
  FHeadHeight := Round((FBackGrBmp.Width * 411 / 755) * 11.9 / 16.5);

  FRailHeight0 := Round((FBackGrBmp.Width * 411 / 755) * 15.05 / 16.5);

  FRailHeight1 := Round((FBackGrBmp.Width * 411 / 755) * 14.95 / 16.5);

  FRailHeight2 := Round((FBackGrBmp.Width * 411 / 755) * 14.85 /16.5);

  s := sin(2.9 / 9.5);
  FTgAlfa := s / Sqrt(1 - s * s);
  s := sin(1.8 / 9.2);
  FTgBetta := s / Sqrt(1 - s * s);
  s := sin(7.47 / 0.8);
  FTgGamma := s / Sqrt(1 - s * s);


  // ���� 0
  FBackGrBmp.Canvas.Pen.Width:= 3;
  FBackGrBmp.Canvas.Pen.Color:= clMaroon;

  FBackGrBmp.Canvas.MoveTo(FWheelX[0, 1], TopY1(FWheelX[0, 1]));
  FBackGrBmp.Canvas.LineTo(FWheelX[0, 1], BotY(FWheelX[0, 1]));

  FBackGrBmp.Canvas.MoveTo(FWheelX[1, 1], TopY1(FWheelX[1, 1]));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 1], BotY(FWheelX[1, 1]));

  // ���� 45

  FBackGrBmp.Canvas.Pen.Color := clBlue;
  X42 := Round(FWheelX[0, 1] * 0.7);

  a := FWheelX[0, 1];
  B := TopY1(FWheelX[0, 1]);
  c := FWheelX[0, 1] - X42;
  D := BotY(FWheelX[0, 1] - X42);

  Alfa7 := ArcTan((c - a) / (D - B));
  Max7 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  a := FWheelX[0, 1];
  B := TopY1(FWheelX[0, 1]);
  c := FWheelX[0, 1] + X42;
  D := BotY(FWheelX[0, 1] + X42);

  Alfa6 := ArcTan((c - a) / (D - B));
  Max6 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  FBackGrBmp.Canvas.MoveTo(FWheelX[0, 1], TopY1(FWheelX[0, 1]));
  FBackGrBmp.Canvas.LineTo(FWheelX[0, 1] + X42, BotY(FWheelX[0, 1] + X42));

  FBackGrBmp.Canvas.MoveTo(FWheelX[0, 1], TopY1(FWheelX[0, 1]));
  FBackGrBmp.Canvas.LineTo(FWheelX[0, 1] - X42, BotY(FWheelX[0, 1] - X42));

  //FBackGrBmp.Canvas.MoveTo(FWheelX[1, 2], TopY(FWheelX[1, 2]));
  //FBackGrBmp.Canvas.LineTo(FWheelX[1, 2] + X42, BotY(FWheelX[1, 2] + X42));

  FBackGrBmp.Canvas.MoveTo(FWheelX[1, 1], TopY1(FWheelX[1, 1]));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 1] - X42, BotY(FWheelX[1, 1] - X42));

  FBackGrBmp.Canvas.MoveTo(FWheelX[1, 1], TopY1(FWheelX[1, 1]));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 1] + X42, BotY(FWheelX[1, 1] + X42));

  // ���� 45/90

  FBackGrBmp.Canvas.Pen.Color := clRed;
  X45_90 := Round(FWheelX[0, 1] * 0.35);

  FBackGrBmp.Canvas.MoveTo(FWheelX[0, 1], TopY1(FWheelX[0, 1]));
  FBackGrBmp.Canvas.LineTo(FWheelX[0, 1] - X45_90, HBotY(FWheelX[0, 1] - X45_90));

  FBackGrBmp.Canvas.MoveTo(FWheelX[0, 1], TopY1(FWheelX[0, 1]));
  FBackGrBmp.Canvas.LineTo(FWheelX[0, 1] + X45_90, HBotY(FWheelX[0, 1] + X45_90));

  FBackGrBmp.Canvas.MoveTo(FWheelX[1, 1], TopY1(FWheelX[1, 1]));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 1] - X45_90, HBotY(FWheelX[1, 1] - X45_90));

  FBackGrBmp.Canvas.MoveTo(FWheelX[1, 1], TopY1(FWheelX[1, 1]));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 1] + X45_90, HBotY(FWheelX[1, 1] + X45_90));

  a := FWheelX[0, 1];
  B := TopY1(FWheelX[0, 1]);
  c := FWheelX[0, 1] + X45_90;
  D := HBotY(FWheelX[0, 1] - X45_90);

  Alfa3 := ArcTan((c - a) / (D - B));
  Max3 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  a := FWheelX[0, 1];
  B := TopY1(FWheelX[0, 1]);
  c := FWheelX[0, 1] + X45_90;
  D := HBotY(FWheelX[0, 1] + X45_90);

  Alfa2 := ArcTan((c - a) / (D - B));
  Max2 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  // ���� 65 ���
  FBackGrBmp.Canvas.Pen.Color := clBlue;
  X65 := Round(FWheelX[0, 1] * 0.9);

  //FBackGrBmp.Canvas.MoveTo(FWheelX[0, 1], TopY(FWheelX[0, 1]));
  FBackGrBmp.Canvas.MoveTo(FWheelX[0, 1], TopY1(FWheelX[0, 1]));
  FBackGrBmp.Canvas.LineTo(FWheelX[0, 1] - X65, HBotY(FWheelX[0, 1] - X65));

  //FBackGrBmp.Canvas.MoveTo(FWheelX[0, 1], TopY(FWheelX[0, 1]));
  FBackGrBmp.Canvas.MoveTo(FWheelX[0, 1], TopY1(FWheelX[0, 1]));
  FBackGrBmp.Canvas.LineTo(FWheelX[0, 1] + X65, HBotY(FWheelX[0, 1] + X65));

  FBackGrBmp.Canvas.MoveTo(FWheelX[1, 1], TopY1(FWheelX[1, 1]));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 1] - X65, HBotY(FWheelX[1, 1] - X65));

  FBackGrBmp.Canvas.MoveTo(FWheelX[1, 1], TopY1(FWheelX[1, 1]));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 1] + X65, HBotY(FWheelX[1, 1] + X65));

  // 13
  FBackGrBmp.Canvas.MoveTo(FWheelX[0, 1] - WlSideOffset, TopY2(FWheelX[0, 1] - WlSideOffset));
  FBackGrBmp.Canvas.LineTo(FWheelX[0, 1] - WlSideOffset - X65, HBotY(FWheelX[0, 1] - WlSideOffset - X65));

  FBackGrBmp.Canvas.MoveTo(FWheelX[0, 1] - WlSideOffset, TopY2(FWheelX[0, 1] - WlSideOffset));
  FBackGrBmp.Canvas.LineTo(FWheelX[0, 1] - WlSideOffset + X65, HBotY(FWheelX[0, 1] - WlSideOffset + X65));

  //FBackGrBmp.Canvas.MoveTo(FWheelX[0, 1] + WlSideOffset, TopY(FWheelX[0, 1] + WlSideOffset));
  FBackGrBmp.Canvas.MoveTo(FWheelX[0, 1] + WlSideOffset, TopY0(FWheelX[0, 1] + WlSideOffset));
  FBackGrBmp.Canvas.LineTo(FWheelX[0, 1] + WlSideOffset - X65, HBotY(FWheelX[0, 1] + WlSideOffset - X65));

  //FBackGrBmp.Canvas.MoveTo(FWheelX[0, 1] + WlSideOffset, TopY(FWheelX[0, 1] + WlSideOffset));
  FBackGrBmp.Canvas.MoveTo(FWheelX[0, 1] + WlSideOffset, TopY0(FWheelX[0, 1] + WlSideOffset));

  FBackGrBmp.Canvas.LineTo(FWheelX[0, 1] + WlSideOffset + X65, HBotY(FWheelX[0, 1] + WlSideOffset + X65));


  FBackGrBmp.Canvas.MoveTo(FWheelX[1, 1] - WlSideOffset, TopY0(FWheelX[1, 1] - WlSideOffset));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 1] - WlSideOffset - X65, HBotY(FWheelX[1, 1] - WlSideOffset - X65));

  FBackGrBmp.Canvas.MoveTo(FWheelX[1, 1] - WlSideOffset, TopY0(FWheelX[1, 1] - WlSideOffset));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 1] - WlSideOffset + X65, HBotY(FWheelX[1, 1] - WlSideOffset + X65));

  FBackGrBmp.Canvas.MoveTo(FWheelX[1, 1] + WlSideOffset, TopY2(FWheelX[1, 1] + WlSideOffset));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 1] + WlSideOffset - X65, HBotY(FWheelX[1, 1] + WlSideOffset - X65));

  FBackGrBmp.Canvas.MoveTo(FWheelX[1, 1] + WlSideOffset, TopY2(FWheelX[1, 1] + WlSideOffset));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 1] + WlSideOffset + X65, HBotY(FWheelX[1, 1] + WlSideOffset + X65));

  a := FWheelX[0, 1];
  //B := TopY(FWheelX[0, 1]);
  B := TopY1(FWheelX[0, 1]);
  c := FWheelX[0, 1] + X65;
  D := HBotY(FWheelX[0, 1] + X65);

  Alfa4 := ArcTan((c - a) / (D - B));
  Max4 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  a := FWheelX[0, 1];
  B := TopY1(FWheelX[0, 1]);
  c := FWheelX[0, 1] - X65;
  D := HBotY(FWheelX[0, 1] - X65);

  Alfa5 := ArcTan((c - a) / (D - B));
  Max5 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  a := FWheelX[0, 1] + WlSideOffset;
  B := TopY(FWheelX[0, 1] + WlSideOffset);
  c := FWheelX[0, 1] + WlSideOffset + X65;
  D := HBotY(FWheelX[0, 1] + WlSideOffset + X65);

  Alfa10 := ArcTan((c - a) / (D - B));
  Max10 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  a := FWheelX[0, 1] + WlSideOffset;
  //B := TopY(FWheelX[0, 1]);
  B := TopY(FWheelX[0, 1] + WlSideOffset);
  c := FWheelX[0, 1] + WlSideOffset - X65;
  D := HBotY(FWheelX[0, 1] + WlSideOffset - X65);

  Alfa11 := ArcTan((c - a) / (D - B));
  Max11 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  a := FWheelX[0, 1] - WlSideOffset;
  B := TopY2(FWheelX[0, 1] - WlSideOffset);
  c := FWheelX[0, 1] - WlSideOffset + X65;
  D := HBotY(FWheelX[0, 1] - WlSideOffset + X65);

  Alfa12 := ArcTan((c - a) / (D - B));
  Max12 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  a := FWheelX[0, 1] - WlSideOffset;
  B := TopY2(FWheelX[0, 1] - WlSideOffset);
  c := FWheelX[0, 1] - WlSideOffset - X65;
  D := HBotY(FWheelX[0, 1] - WlSideOffset - X65);

  Alfa13 := ArcTan((c - a) / (D - B));
  Max13 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));
end;

{$ELSE}
procedure TAV16WheelsMnemo.CountLines;
var
  //R, w, H, X42, X58, X70: integer;
  X42, X58, X70: integer;
  //s, a, B, c, D: Single;
  s, a, B, c, D: Single;

begin
  FWheelX[0, 2] := Round(FBackGrBmp.Width * 0.19);
  FWheelX[0, 1] := Round(FBackGrBmp.Width * 0.26);
  FWheelX[1, 2] := Round(FBackGrBmp.Width - Self.Width * 0.19);
  FWheelX[1, 1] := Round(FBackGrBmp.Width - Self.Width * 0.26);

  FRailHeight := Round((FBackGrBmp.Width * 411 / 755) * 15.1 / 16.5);
  FHeadHeight := Round((FBackGrBmp.Width * 411 / 755) * 11.9 / 16.5);

  s := sin(2.9 / 9.5);
  FTgAlfa := s / Sqrt(1 - s * s);
  s := sin(1.8 / 9.2);
  FTgBetta := s / Sqrt(1 - s * s);
  s := sin(7.47 / 0.8);
  FTgGamma := s / Sqrt(1 - s * s);


  // ���� 0
  FBackGrBmp.Canvas.Pen.Width:= 3;
  FBackGrBmp.Canvas.Pen.Color:= clMaroon;

  // ������ ������
  FBackGrBmp.Canvas.MoveTo(FWheelX[0, 2], TopY(FWheelX[0, 2]));
  FBackGrBmp.Canvas.LineTo(FWheelX[0, 2], BotY(FWheelX[0, 2]));

  FBackGrBmp.Canvas.MoveTo(FWheelX[1, 2], TopY(FWheelX[1, 2]));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 2], BotY(FWheelX[1, 2]));

  // �������� ������
  FBackGrBmp.Canvas.MoveTo(FWheelX[0, 1], TopY(FWheelX[0, 1]));
  FBackGrBmp.Canvas.LineTo(FWheelX[0, 1], BotY(FWheelX[0, 1]));

  FBackGrBmp.Canvas.MoveTo(FWheelX[1, 1], TopY(FWheelX[1, 1]));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 1], BotY(FWheelX[1, 1]));

  // ���� 42

  FBackGrBmp.Canvas.Pen.Color := clBlue;
  X42 := Round(FWheelX[0, 2] * 0.7);

  a := X42;
  B := BotY(FWheelX[0, 2] - X42) - TopY(FWheelX[0, 2]);

  FBackGrBmp.Canvas.MoveTo(FWheelX[0, 2], TopY(FWheelX[0, 2]));
  FBackGrBmp.Canvas.LineTo(FWheelX[0, 2] - X42, BotY(FWheelX[0, 2] - X42));

  a := FWheelX[0, 2];
  B := TopY(FWheelX[0, 2]);
  c := FWheelX[0, 2] + X42;
  D := BotY(FWheelX[0, 2] - X42);

  Alfa42_2 := ArcTan((c - a) / (D - B));
  Max42_2 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  a := FWheelX[0, 1];
  B := TopY(FWheelX[0, 1]);
  c := FWheelX[0, 1] + X42;
  D := BotY(FWheelX[0, 1] + X42);

  Alfa42_1 := ArcTan((c - a) / (D - B));
  Max42_1 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  FBackGrBmp.Canvas.MoveTo(FWheelX[0, 1], TopY(FWheelX[0, 1]));
  FBackGrBmp.Canvas.LineTo(FWheelX[0, 1] + X42, BotY(FWheelX[0, 1] + X42));

  FBackGrBmp.Canvas.MoveTo(FWheelX[1, 2], TopY(FWheelX[1, 2]));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 2] + X42, BotY(FWheelX[1, 2] + X42));

  FBackGrBmp.Canvas.MoveTo(FWheelX[1, 1], TopY(FWheelX[1, 1]));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 1] - X42, BotY(FWheelX[1, 1] - X42));

  // ���� 58

  FBackGrBmp.Canvas.Pen.Color := clRed;
  X58 := Round(FWheelX[0, 2] * 0.35);

  FBackGrBmp.Canvas.MoveTo(FWheelX[0, 2], TopY(FWheelX[0, 2]));
  FBackGrBmp.Canvas.LineTo(FWheelX[0, 2] - X58, HBotY(FWheelX[0, 2] - X58));
  FBackGrBmp.Canvas.LineTo(FWheelX[0, 2] - 2 * X58, TopY(FWheelX[0, 2] - 2 * X58));
  FBackGrBmp.Canvas.LineTo(FWheelX[0, 2] - 3 * X58, HBotY(FWheelX[0, 2] - 3 * X58));

  PointB58_1X[0] := FWheelX[0, 2] - X58;
  PointB58_2X[0] := FWheelX[0, 2] - 2 * X58;

  a := FWheelX[0, 2];
  B := TopY(FWheelX[0, 2]);
  c := FWheelX[0, 2] - X58;
  D := HBotY(FWheelX[0, 2] - X58);

  Alfa58_2 := ArcTan((c - a) / (D - B));
  Max58_2 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  a := FWheelX[0, 2] - X58;
  B := HBotY(FWheelX[0, 2] - X58);
  c := FWheelX[0, 2] - 2 * X58;
  D := TopY(FWheelX[0, 2] - 2 * X58);

  Alfa58_2_P1 := ArcTan((c - a) / (D - B));
  Max58_2_P1 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  a := FWheelX[0, 2] - 2 * X58;
  B := TopY(FWheelX[0, 2] - 2 * X58);
  c := FWheelX[0, 2] - 3 * X58;
  D := HBotY(FWheelX[0, 2] - 3 * X58);

  Alfa58_2_P2 := ArcTan((c - a) / (D - B));
  Max58_2_P2 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  FBackGrBmp.Canvas.MoveTo(FWheelX[0, 1], TopY(FWheelX[0, 1]));
  FBackGrBmp.Canvas.LineTo(FWheelX[0, 1] + X58, HBotY(FWheelX[0, 1] + X58));
  FBackGrBmp.Canvas.LineTo(FWheelX[0, 1] + 2 * X58, TopY(FWheelX[0, 1] + 2 * X58));
  FBackGrBmp.Canvas.LineTo(FWheelX[0, 1] + 3 * X58, HBotY(FWheelX[0, 1] + 3 * X58));

  PointF58_1X[0] := FWheelX[0, 1] + X58;
  PointF58_2X[0] := FWheelX[0, 1] + 2 * X58;

  a := FWheelX[0, 1];
  B := TopY(FWheelX[0, 1]);
  c := FWheelX[0, 1] + X58;
  D := HBotY(FWheelX[0, 1] + X58);

  Alfa58_1 := ArcTan((c - a) / (D - B));
  Max58_1 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  a := FWheelX[0, 1] + X58;
  B := HBotY(FWheelX[0, 1] + X58);
  c := FWheelX[0, 1] + 2 * X58;
  D := TopY(FWheelX[0, 1] + 2 * X58);

  Alfa58_1_P1 := ArcTan((c - a) / (D - B));
  Max58_1_P1 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  a := FWheelX[0, 1] + 2 * X58;
  B := TopY(FWheelX[0, 1] + 2 * X58);
  c := FWheelX[0, 1] + 3 * X58;
  D := HBotY(FWheelX[0, 1] + 3 * X58);

  Alfa58_1_P2 := ArcTan((c - a) / (D - B));
  Max58_1_P2 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  FBackGrBmp.Canvas.MoveTo(FWheelX[1, 2], TopY(FWheelX[1, 2]));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 2] + X58, HBotY(FWheelX[1, 2] + X58));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 2] + 2 * X58, TopY(FWheelX[1, 2] + 2 * X58));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 2] + 3 * X58, HBotY(FWheelX[1, 2] + 3 * X58));

  PointB58_1X[1] := FWheelX[1, 2] + X58;
  PointB58_2X[1] := FWheelX[1, 2] + 2 * X58;

  FBackGrBmp.Canvas.MoveTo(FWheelX[1, 1], TopY(FWheelX[1, 1]));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 1] - X58, HBotY(FWheelX[1, 1] - X58));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 1] - 2 * X58, TopY(FWheelX[1, 1] - 2 * X58));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 1] - 3 * X58, HBotY(FWheelX[1, 1] - 3 * X58));

  PointF58_1X[1] := FWheelX[1, 1] - X58;
  PointF58_2X[1] := FWheelX[1, 1] - 2 * X58;

  // ���� 70

  FBackGrBmp.Canvas.Pen.Color := clGreen;
  X70 := Round(FWheelX[0, 2] * 0.9);

  FBackGrBmp.Canvas.MoveTo(FWheelX[0, 2], TopY(FWheelX[0, 2]));
  FBackGrBmp.Canvas.LineTo(FWheelX[0, 2] - X70, HBotY(FWheelX[0, 2] - X70));

  a := FWheelX[0, 2];
  B := TopY(FWheelX[0, 2]);
  c := FWheelX[0, 2] - X70;
  D := HBotY(FWheelX[0, 2] - X70);

  Alfa70_2 := ArcTan((c - a) / (D - B));
  Max70_2 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  a := FWheelX[0, 1];
  B := TopY(FWheelX[0, 1]);
  c := FWheelX[0, 1] + X70;
  D := HBotY(FWheelX[0, 1] + X70);

  Alfa70_1 := ArcTan((c - a) / (D - B));
  Max70_1 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  //B := TopY(FWheelX[0, 1]);
  //D := HBotY(FWheelX[0, 1]);

  MaxZero1:= HBotY(FWheelX[0, 1]);

  //B := TopY(FWheelX[0, 1]);
  //D := HBotY(FWheelX[0, 1]);

  MaxZero2:= HBotY(FWheelX[0, 2]);

  FBackGrBmp.Canvas.MoveTo(FWheelX[0, 1], TopY(FWheelX[0, 1]));
  FBackGrBmp.Canvas.LineTo(FWheelX[0, 1] + X70, HBotY(FWheelX[0, 1] + X70));

  FBackGrBmp.Canvas.MoveTo(FWheelX[1, 1], TopY(FWheelX[1, 1]));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 1] - X70, HBotY(FWheelX[1, 1] - X70));

  FBackGrBmp.Canvas.MoveTo(FWheelX[1, 2], TopY(FWheelX[1, 2]));
  FBackGrBmp.Canvas.LineTo(FWheelX[1, 2] + X70, HBotY(FWheelX[1, 2] + X70));
end;
{$ENDIF}

constructor TAV16WheelsMnemo.Create(AOwner: TComponent; RailCnt, ChCnt: integer;
  PicsPath: string);
begin
  inherited;

{$IFDEF VMT_US}
  FSegments.AddBase('\VMT_USMnemo\Rails.wmf');

  FSegments.AddSegment(0, 0, '\VMT_USMnemo\W_L_R.wmf');
  FSegments.AddSegment(0, 1, '\VMT_USMnemo\W_L.wmf');

  FSegments.AddSegment(1, 0, '\VMT_USMnemo\W_R_R.wmf');
  FSegments.AddSegment(1, 1, '\VMT_USMnemo\W_R.wmf');

  WlSideOffset := 25;

  FTestKanals[0,6].Time := 50;
  FTestKanals[1,6].Time := 50;
  FTestKanals[0,7].Time := 50;
  FTestKanals[1,7].Time := 50;

  FTestKanals[0,6].Flag := true;
  FTestKanals[1,6].Flag := true;
  FTestKanals[0,7].Flag := true;
  FTestKanals[1,7].Flag := true;

  FTestKanals[0,2].Time := 50;
  FTestKanals[1,2].Time := 50;
  FTestKanals[0,3].Time := 50;
  FTestKanals[1,3].Time := 50;

  FTestKanals[0,2].Flag := true;
  FTestKanals[1,2].Flag := true;
  FTestKanals[0,3].Flag := true;
  FTestKanals[1,3].Flag := true;


  FTestKanals[0,4].Time := 50;
  FTestKanals[1,4].Time := 50;
  FTestKanals[0,5].Time := 50;
  FTestKanals[1,5].Time := 50;

  FTestKanals[0,4].Flag := true;
  FTestKanals[1,4].Flag := true;
  FTestKanals[0,5].Flag := true;
  FTestKanals[1,5].Flag := true;


  FTestKanals[0,13].Time := 50;
  FTestKanals[1,13].Time := 50;
  FTestKanals[0,12].Time := 50;
  FTestKanals[1,12].Time := 50;

  FTestKanals[0,13].Flag := true;
  FTestKanals[1,13].Flag := true;
  FTestKanals[0,12].Flag := true;
  FTestKanals[1,12].Flag := true;

  FTestKanals[0,11].Time := 50;
  FTestKanals[1,11].Time := 50;
  FTestKanals[0,10].Time := 50;
  FTestKanals[1,10].Time := 50;

  FTestKanals[0,11].Flag := true;
  FTestKanals[1,11].Flag := true;
  FTestKanals[0,10].Flag := true;
  FTestKanals[1,10].Flag := true;

  FTestKanals[0,1].Time := 50;
  FTestKanals[1,1].Time := 50;
  FTestKanals[0,1].Flag := true;
  FTestKanals[1,1].Flag := true;

  FTestKanals[0,0].Time := 10;
  FTestKanals[1,0].Time := 10;
  FTestKanals[0,0].Flag := true;
  FTestKanals[1,0].Flag := true;

  SegmentSize := 20;

{$ELSE}
  FSegments.AddBase('M.wmf');
  //FSegments.AddBase('M.emf');

  FSegments.AddSegment(0, 0, 'W0_1_NO.wmf');
  FSegments.AddSegment(0, 1, 'W0_1_YES.wmf');
  FSegments.AddSegment(0, 2, 'W0_2_NO.wmf');
  FSegments.AddSegment(0, 3, 'W0_2_YES.wmf');

  FSegments.AddSegment(1, 0, 'W1_1_NO.wmf');
  FSegments.AddSegment(1, 1, 'W1_1_YES.wmf');
  FSegments.AddSegment(1, 2, 'W1_2_NO.wmf');
  FSegments.AddSegment(1, 3, 'W1_2_YES.wmf');

  {FSegments.AddSegment(0, 0, 'W0_1_NO.emf');
  FSegments.AddSegment(0, 1, 'W0_1_YES.emf');
  FSegments.AddSegment(0, 2, 'W0_2_NO.emf');
  FSegments.AddSegment(0, 3, 'W0_2_YES.emf');

  FSegments.AddSegment(1, 0, 'W1_1_NO.emf');
  FSegments.AddSegment(1, 1, 'W1_1_YES.emf');
  FSegments.AddSegment(1, 2, 'W1_2_NO.emf');
  FSegments.AddSegment(1, 3, 'W1_2_YES.emf');}
  FTestKanals[0,6].Time := 120;
  FTestKanals[1,6].Time := 120;
  FTestKanals[0,7].Time := 120;
  FTestKanals[1,7].Time := 120;

  FTestKanals[0,6].Flag := true;
  FTestKanals[1,6].Flag := true;
  FTestKanals[0,7].Flag := true;
  FTestKanals[1,7].Flag := true;

  FTestKanals[0,2].Time := 20;
  FTestKanals[1,2].Time := 20;
  FTestKanals[0,3].Time := 20;
  FTestKanals[1,3].Time := 20;

  FTestKanals[0,2].Flag := true;
  FTestKanals[1,2].Flag := true;
  FTestKanals[0,3].Flag := true;
  FTestKanals[1,3].Flag := true;

{$ENDIF}

  FPointR := 5;
end;

procedure TAV16WheelsMnemo.DrawBackGround;
var
  K: Single;
begin
  inherited;
  if Assigned(FSegments) then
    if Assigned(FSegments.Base) then
    begin
      FBackGrBmp.Canvas.Brush.Color := clWhite;
      FBackGrBmp.Canvas.FillRect(Rect(0, 0, FBackGrBmp.Width, FBackGrBmp.Height));

      FKW:= FBackGrBmp.Width / FSegments.Base.Picture.Metafile.Width;
      FKH:= (FBackGrBmp.Width * 411 / 755) / FSegments.Base.Picture.Metafile.Height;
      //FKH:= (FBackGrBmp.Width * 510 / 860) / FSegments.Base.Picture.Metafile.Height;
      FPicWidth:= FBackGrBmp.Width;
      //K := FPicWidth / FBackGrBmp.Width;
      FPicHeight:= Round(FBackGrBmp.Width * 411 / 755);
      //FPicHeight:= Round(FBackGrBmp.Width * 510 / 860);

      //FBackGrBmp.Canvas.StretchDraw(Rect(0, 0, FBackGrBmp.Width, FBackGrBmp.Height),
        //FSegments.Base.Picture.Metafile);
      FBackGrBmp.Canvas.StretchDraw(Rect(0, FBackGrBmp.Height-FPicHeight, FPicWidth, FBackGrBmp.Height), FSegments.Base.Picture.Metafile);
    end;
  CountLines;
end;

{$IFDEF VMT_US}
procedure TAV16WheelsMnemo.DrawChannel(Nit, Kanal, Delay, Amp: integer);
var
  NitK, X, Y: integer;
  DrawAllow: Boolean;
begin
  if Nit = 0 then
    NitK := 1
  else
    NitK := -1;
  if Delay <= 0 then
    DrawAllow := false
  else
    DrawAllow := true;
  case Kanal of
    0:
      begin
        X := FWheelX[Nit, 1];
        Y := BotY(X) - SegmentSize;

        FBackGrBmp.Canvas.MoveTo(X, Y);
        FBackGrBmp.Canvas.LineTo(FWheelX[Nit, 1], BotY(FWheelX[Nit, 1]));

        DrawAllow := false;
      end;
    1:
      begin
        X := FWheelX[Nit, 1];

        //������ ��� ����� FBottomTKP1 � EGO_USW ���� ���
        //Y := TopY1(X) - Round(((TopY1(X) - BotY(X)) / FBottomTKP1) * Delay);
        Y := TopY1(X) - Round(((TopY1(X) - BotY(X)) / 70)* Delay);
        if (Delay > 70) then
          DrawAllow := false;
        if Y<MaxZero1 then
          DrawAllow := false;
      end;

    6:
      begin
        X := Round(NitK * sin(Alfa6) * (((Max6) / 184) * Delay))
          + FWheelX[Nit, 1];
        Y := Round(cos(Alfa6) * (((Max6) / 184) * Delay)) + TopY1
          (FWheelX[Nit, 1]);
      end;
    //7,9:
    7:
      begin
        X := Round(NitK * sin(Alfa7) * (((Max7) / 184) * Delay))
          + FWheelX[Nit, 1];
        Y := Round(cos(Alfa7) * (((Max7) / 184) * Delay)) + TopY1
          (FWheelX[Nit, 1]);
      end;
    4:
      begin
        X := Round(NitK * sin(Alfa4) * (((Max4) / 91) * Delay))
          + FWheelX[Nit, 1];
        Y := Round(cos(Alfa4) * (((Max4) / 91) * Delay)) + TopY1
          (FWheelX[Nit, 1]);
      end;
    5:
      begin
        X := Round(NitK * sin(Alfa5) * (((Max5) / 91) * Delay))
          + FWheelX[Nit, 1];
        Y := Round(cos(Alfa5) * (((Max5) / 91) * Delay)) + TopY1
          (FWheelX[Nit, 1]);
      end;
    //2,12:
    2:
      begin
      FBackGrBmp.Canvas.Pen.Color := clRed;
      FBackGrBmp.Canvas.Pen.Width := 12;

      X := Round(NitK * sin(Alfa2) * (((Max2) / 40) * 34))
        + FWheelX[Nit, 1];
      Y := Round(cos(Alfa2) * (((Max2) / 40) * 34)) + TopY1
        (FWheelX[Nit, 1]);

      FBackGrBmp.Canvas.MoveTo(X, Y);
      FBackGrBmp.Canvas.LineTo(FWheelX[Nit, 1] + NitK*X45_90, HBotY(FWheelX[Nit, 1] + NitK* X45_90));

      DrawAllow := false;
      end;
    //3,13:
    3:
      begin
      FBackGrBmp.Canvas.Pen.Color := clRed;
      FBackGrBmp.Canvas.Pen.Width := 12;

        X := Round(-NitK * sin(Alfa3) * (((Max3) / 40) * 34))
          + FWheelX[Nit, 1];
        Y := Round(cos(Alfa3) * (((Max3) / 40) * 34)) + TopY1
          (FWheelX[Nit, 1]);

      FBackGrBmp.Canvas.MoveTo(X, Y);
      FBackGrBmp.Canvas.LineTo(FWheelX[Nit, 1] - NitK*X45_90, HBotY(FWheelX[Nit, 1] - NitK* X45_90));

      DrawAllow := false;
      end;

    13:
      begin
        X := Round(NitK * sin(Alfa13) * (((Max13) / 91) * Delay))
          + FWheelX[Nit, 1] - NitK*WlSideOffset;
        Y := Round(cos(Alfa13) * (((Max13) / 91) * Delay)) + TopY2
          (FWheelX[Nit, 1] - NitK*WlSideOffset);
      end;
    12:
      begin
        X := Round(NitK * sin(Alfa12) * (((Max12) / 91) * Delay))
          + FWheelX[Nit, 1] - NitK*WlSideOffset;
        Y := Round(cos(Alfa12) * (((Max12) / 91) * Delay)) + TopY2
          (FWheelX[Nit, 1] - NitK*WlSideOffset);
      end;
    11:
      begin
        X := Round(NitK * sin(Alfa11) * (((Max11) / 91) * Delay))
          + FWheelX[Nit, 1] + NitK*WlSideOffset;;
        Y := Round(cos(Alfa11) * (((Max11) / 91) * Delay)) + TopY0
          (FWheelX[Nit, 1]  + NitK*WlSideOffset);
      end;
    10:
      begin
        X := Round(NitK * sin(Alfa10) * (((Max10) / 91) * Delay))
          + FWheelX[Nit, 1] + NitK*WlSideOffset;
        Y := Round(cos(Alfa10) * (((Max10) / 91) * Delay)) + TopY0
          (FWheelX[Nit, 1] + NitK*WlSideOffset);
      end;

  end;
  if Y < (FBackGrBmp.Height-FPicHeight) then
    DrawAllow:= false;
  if DrawAllow then
  begin
    FBmp.Canvas.Brush.Color := clRed;
    FBmp.Canvas.Ellipse(X - FPointR, Y - FPointR, X + FPointR, Y + FPointR);
  end;

end;
{$ELSE}
procedure TAV16WheelsMnemo.DrawChannel(Nit, Kanal, Delay, Amp: integer);
var
  NitK, X, Y: integer;
  DrawAllow: Boolean;
begin
  if Nit = 0 then
    NitK := 1
  else
    NitK := -1;
  if Delay <= 0 then
    DrawAllow := false
  else
    DrawAllow := true;
  case Kanal of
{$IFDEF EGO_USW}
    11:
{$ELSE}
    1:
{$ENDIF}
      begin
        X := FWheelX[Nit, 2];
        Y := TopY(X) - Round(((TopY(X) - BotY(X)) / FBottomTKP2) * Delay);
        if (Delay > 70) then
          DrawAllow := false;
        if Y<MaxZero2 then
          DrawAllow := false;
      end;
{$IFDEF EGO_USW}
    1:
{$ELSE}
    11:
{$ENDIF}
      begin
        X := FWheelX[Nit, 1];
        Y := TopY(X) - Round(((TopY(X) - BotY(X)) / FBottomTKP1) * Delay);
        if (Delay > 70) then
          DrawAllow := false;
        if Y<MaxZero1 then
          DrawAllow := false;
      end;
    6,8:
      begin
        X := Round(NitK * sin(Alfa42_1) * (((Max42_1) / 184) * Delay))
          + FWheelX[Nit, 1];
        Y := Round(cos(Alfa42_1) * (((Max42_1) / 184) * Delay)) + TopY
          (FWheelX[Nit, 1]);
      end;
    7,9:
      begin
        X := Round(-NitK * sin(Alfa42_2) * (((Max42_2) / 184) * Delay))
          + FWheelX[Nit, 2];
        Y := Round(cos(Alfa42_2) * (((Max42_2) / 184) * Delay)) + TopY
          (FWheelX[Nit, 2]);
      end;
    4:
      begin
        X := Round(NitK * sin(Alfa70_1) * (((Max70_1) / 91) * Delay))
          + FWheelX[Nit, 1];
        Y := Round(cos(Alfa70_1) * (((Max70_1) / 91) * Delay)) + TopY
          (FWheelX[Nit, 1]);
      end;
    5:
      begin
        X := Round(NitK * sin(Alfa70_2) * (((Max70_2) / 91) * Delay))
          + FWheelX[Nit, 2];
        Y := Round(cos(Alfa70_2) * (((Max70_2) / 91) * Delay)) + TopY
          (FWheelX[Nit, 2]);
      end;
    2,12:
      begin
        if (Delay >= 0) and (Delay <= 40) then
        begin
          X := Round(NitK * sin(Alfa58_1) * (((Max58_1) / 40) * Delay))
            + FWheelX[Nit, 1];
          Y := Round(cos(Alfa58_1) * (((Max58_1) / 40) * Delay)) + TopY
            (FWheelX[Nit, 1]);
        end;
        if (Delay > 40) and (Delay <= 80) then
        begin
          X := Round(-NitK * sin(Alfa58_1_P1) *
              (((Max58_1_P1) / 40) * (Delay - 40))) + PointF58_1X[Nit];
          Y := Round(-cos(Alfa58_1_P1) * (((Max58_1_P1) / 40) * (Delay - 40)))
            + HBotY(FWheelX[Nit, 1]);
        end;
        if (Delay > 80) and (Delay <= 130) then
        begin
          X := Round(NitK * sin(Alfa58_1_P2) *
              (((Max58_1_P2) / 40) * (Delay - 80))) + PointF58_2X[Nit];
          Y := Round(cos(Alfa58_1_P2) * (((Max58_1_P2) / 40) * (Delay - 80)))
            + TopY(PointF58_2X[Nit]);
        end;
      end;
    3,13:
      begin
        if (Delay >= 0) and (Delay <= 40) then
        begin
          X := Round(NitK * sin(Alfa58_2) * (((Max58_2) / 40) * Delay))
            + FWheelX[Nit, 2];
          Y := Round(cos(Alfa58_2) * (((Max58_2) / 40) * Delay)) + TopY
            (FWheelX[Nit, 2]);
        end;
        if (Delay > 40) and (Delay <= 80) then
        begin
          X := Round(-NitK * sin(Alfa58_2_P1) *
              (((Max58_2_P1) / 40) * (Delay - 40))) + PointB58_1X[Nit];
          Y := Round(-cos(Alfa58_2_P1) * (((Max58_2_P1) / 40) * (Delay - 40)))
            + HBotY(FWheelX[Nit, 2]);
        end;
        if (Delay > 80) and (Delay <= 130) then
        begin
          X := Round(NitK * sin(Alfa58_2_P2) *
              (((Max58_2_P2) / 40) * (Delay - 80))) + PointB58_2X[Nit];
          Y := Round(cos(Alfa58_2_P2) * (((Max58_2_P2) / 40) * (Delay - 80)))
            + TopY(PointB58_2X[Nit]);
        end;
      end;
  end;
  if Y < (FBackGrBmp.Height-FPicHeight) then
    DrawAllow:= false;
  if DrawAllow then
  begin
    FBmp.Canvas.Brush.Color := clRed;
    FBmp.Canvas.Ellipse(X - FPointR, Y - FPointR, X + FPointR, Y + FPointR);
  end;
end;
{$ENDIF}

procedure TAV16WheelsMnemo.DrawForeGround;
var
  Scan: TBScanArray;
  i, j, c: integer;
  MaxA, MaxIdx, Ch: Byte;
  IgnoreSignal: Boolean;
  //Max: array [0 .. 1] of TEchoBlock;
  Max: array [0..1, 0..13] of TOneEcho;

            X: integer;
          Y: integer;
begin
  //inherited;
  // ���������� ������� ��������
  Scan := FCore.GetLastBScan;
  for i := 0 to 1 do
    for j := 0 to 13 do
    begin
      Ch:= j;
      if j = 8 then
        Ch:= 6;
      if j = 9 then
        Ch:= 7;
      MaxA := Scan[i, Ch].Echos[0].a;
      MaxIdx := 0;

      for c := 0 to Scan[i, Ch].EchoCount - 1 do
         begin
           IgnoreSignal:= false;
           if j in [0, 1] then
             IgnoreSignal:= Scan[i, Ch].Echos[c].t > FCore.Kanal[i, 0].Str_st;
           if j in [10, 11] then
             IgnoreSignal:= Scan[i, Ch].Echos[c].t > FCore.Kanal[i, 10].Str_st;
           if (Scan[i, Ch].Echos[c].a > MaxA) and (not IgnoreSignal) then
             begin
               MaxA := Scan[i, Ch].Echos[c].a;
               MaxIdx := c;
             end;
         end;
      Max[i, j].t := Scan[i, Ch].Echos[MaxIdx].t;
      Max[i, j].a := Scan[i, Ch].Echos[MaxIdx].a;
    end;

  //FBottomT := Round((FCore.Kanal[0, 1].Str_en - FCore.Kanal[0, 1].Str_st) / 2);
  FBottomTKP1 := Round((FCore.Kanal[0, 11].Str_en - FCore.Kanal[0, 11].Str_st));
  FBottomTKP2 := Round((FCore.Kanal[0, 1].Str_en - FCore.Kanal[0, 1].Str_st));

  {$IFDEF Avikon14}
  if (not Kanals[0, 0].ASD) or (FTestKanals[0, 0].Flag) then
    begin
      DrawWheel(0, 1, false);
      DrawWheel(0, 2, false);
    end
  else
    begin
      DrawWheel(0, 1, true);
      DrawWheel(0, 2, true);
    end;

  if (not Kanals[1, 0].ASD) or (FTestKanals[1, 0].Flag) then
    begin
      DrawWheel(1, 1, false);
      DrawWheel(1, 2, false);
    end
  else
    begin
      DrawWheel(1, 1, true);
      DrawWheel(1, 2, true);
    end;

  {$ELSE}

  {$IFDEF VMT_US}
  //if (not Kanals[0, 0].ASD) or (FTestKanals[0, 0].Flag) then DrawWheel(0, 1, false) else DrawWheel(0, 1, true);
  //if (not Kanals[1, 0].ASD) or (FTestKanals[1, 0].Flag) then DrawWheel(1, 1, false) else DrawWheel(1, 1, true);
  if ( Kanals[0, 0].ASD) or (FTestKanals[0, 0].Flag) then DrawWheel(0, 1, true) else DrawWheel(0, 1, false);
  if ( Kanals[1, 0].ASD) or (FTestKanals[1, 0].Flag) then DrawWheel(1, 1, true) else DrawWheel(1, 1, false);

  {$ELSE}

  {$IFDEF EGO_USW}
  if (not Kanals[0, 0].ASD) or (FTestKanals[0, 10].Flag) then DrawWheel(0, 1, false) else DrawWheel(0, 1, true);
  if (not Kanals[0, 10].ASD) or (FTestKanals[0, 0].Flag) then DrawWheel(0, 2, false) else DrawWheel(0, 2, true);
  if (not Kanals[1, 0].ASD) or (FTestKanals[1, 10].Flag) then DrawWheel(1, 1, false) else DrawWheel(1, 1, true);
  if (not Kanals[1, 10].ASD) or (FTestKanals[1, 0].Flag) then DrawWheel(1, 2, false) else DrawWheel(1, 2, true);
  {$ELSE}
  if (not Kanals[0, 10].ASD) or (FTestKanals[0, 10].Flag) then DrawWheel(0, 1, false) else DrawWheel(0, 1, true);
  if (not Kanals[0, 0].ASD) or (FTestKanals[0, 0].Flag) then DrawWheel(0, 2, false) else DrawWheel(0, 2, true);
  if (not Kanals[1, 10].ASD) or (FTestKanals[1, 10].Flag) then DrawWheel(1, 1, false) else DrawWheel(1, 1, true);
  if (not Kanals[1, 0].ASD) or (FTestKanals[1, 0].Flag) then DrawWheel(1, 2, false) else DrawWheel(1, 2, true);
  {$ENDIF}

  {$ENDIF}

  {$ENDIF}

{$IFDEF VMT_US}
  for i := 0 to 1 do
    for j := 0 to 13 do
       begin
         if Assigned(Kanals[i, j]) then
           begin
             if Kanals[i, j].ASD then
              DrawChannel(i, j, Max[i, j].t, Max[i, j].a);
           end;

          if FTestKanals[i, j].Flag then
            DrawChannel(i, j, FTestKanals[i, j].Time, 100);
       end;


{$ELSE}
  for i := 0 to 1 do
    for j := 1 to High(Kanals[i]) do
       begin
         if Assigned(Kanals[i, j]) then
           begin
             if Kanals[i, j].ASD then
              DrawChannel(i, j, Max[i, j].t, Max[i, j].a);
           end;

          if FTestKanals[i, j].Flag then
            DrawChannel(i, j, FTestKanals[i, j].Time, 100);
       end;
{$ENDIF}

end;

{$IFDEF VMT_US}
procedure TAV16WheelsMnemo.DrawWheel(Nit, WheelNum: integer; AK: Boolean);
var
  X, Y, w, H: integer;
  X1, Y1, X2, Y2: integer;
begin  {
  X := FWheelX[Nit, WheelNum]+1;
  Y := TopY(X)+2;
  //w := Round((FAKPic[0, 1].Yes.Width + 4) * FKW);
  //H := Round((FAKPic[0, 1].Yes.Height + 4) * FKH);

  w := Round((FSegments.Segments[0, 0].Width + 4) * (FKW/1.3));
  H := Round((FSegments.Segments[0, 0].Height + 4) * (FKH/1.3));

  if Nit = 0 then
    X1 := Round(X - w * 0.55)
  else
    X1 := Round(X - w * 0.45);
  Y1 := Round(Y - H * 0.955);
  X2 := X1 + w;
  Y2 := Y1 + H;
  if WheelNum = 1 then WheelNum:= 0;

  if AK then
    FBmp.Canvas.StretchDraw(Rect(X1, Y1, X2, Y2),
      FSegments.Segments[Nit, WheelNum].Picture.Metafile)
  else
    FBmp.Canvas.StretchDraw(Rect(X1, Y1, X2, Y2),
      FSegments.Segments[Nit, WheelNum+1].Picture.Metafile) }

  X := FWheelX[Nit, WheelNum] + 4;
  Y := TopY(X);
  //w := Round((FAKPic[0, 1].Yes.Width + 4) * FKW);
  //H := Round((FAKPic[0, 1].Yes.Height + 4) * FKH);

  w := Round((FSegments.Segments[0, 0].Width + 4) * (FKW/1.3));
  H := Round((FSegments.Segments[0, 0].Height + 4) * (FKH/1.3));


  if Nit = 0 then
    X1 := Round(X - w * 0.55)
  else
    X1 := Round(X - w * 0.45);
  //Y1 := Round(Y - H * 0.955);

  Y1 := Round(Y - H * 0.94);

  X2 := X1 + w;
  Y2 := Y1 + H;
  if WheelNum = 1 then WheelNum:= 0;

  if AK then
    FBmp.Canvas.StretchDraw(Rect(X1, Y1, X2, Y2),
      FSegments.Segments[Nit, WheelNum].Picture.Metafile)
  else
    FBmp.Canvas.StretchDraw(Rect(X1, Y1, X2, Y2),
      FSegments.Segments[Nit, WheelNum+1].Picture.Metafile)
end;
{$ELSE}
procedure TAV16WheelsMnemo.DrawWheel(Nit, WheelNum: integer; AK: Boolean);
var
  X, Y, w, H: integer;
  X1, Y1, X2, Y2: integer;
begin
  X := FWheelX[Nit, WheelNum]+1;
  Y := TopY(X)+2;
  //w := Round((FAKPic[0, 1].Yes.Width + 4) * FKW);
  //H := Round((FAKPic[0, 1].Yes.Height + 4) * FKH);

  w := Round((FSegments.Segments[0, 0].Width + 4) * (FKW/1.3));
  H := Round((FSegments.Segments[0, 0].Height + 4) * (FKH/1.3));

  if Nit = 0 then
    X1 := Round(X - w * 0.55)
  else
    X1 := Round(X - w * 0.45);
  Y1 := Round(Y - H * 0.955);
  X2 := X1 + w;
  Y2 := Y1 + H;
  if WheelNum = 1 then WheelNum:= 0;

  if AK then
    FBmp.Canvas.StretchDraw(Rect(X1, Y1, X2, Y2),
      FSegments.Segments[Nit, WheelNum].Picture.Metafile)
  else
    FBmp.Canvas.StretchDraw(Rect(X1, Y1, X2, Y2),
      FSegments.Segments[Nit, WheelNum+1].Picture.Metafile)
end;
{$ENDIF}

{$iFDEF VMT_US}
function TAV16WheelsMnemo.HBotY(X: integer): integer;
begin
  if X < FBackGrBmp.Width / 2 then
    Result := Round(FBackGrBmp.Height - FHeadHeight + (X - FBackGrBmp.Width * 0.09)
        * FTgGamma)
  else
    Result := Round(FBmp.Height - FHeadHeight +
        ((FBackGrBmp.Width - X) - FBackGrBmp.Width * 0.09) * FTgGamma)
end;
{$ELSE}
function TAV16WheelsMnemo.HBotY(X: integer): integer;
begin
  if X < FBackGrBmp.Width / 2 then
    Result := Round(FBackGrBmp.Height - FHeadHeight + (X - FBackGrBmp.Width * 0.09)
        * FTgGamma)
  else
    Result := Round(FBmp.Height - FHeadHeight +
        ((FBackGrBmp.Width - X) - FBackGrBmp.Width * 0.09) * FTgGamma)
end;
{$ENDIF}


function TAV16WheelsMnemo.TopY(X: integer): integer;
begin
  if X < FBackGrBmp.Width / 2 then
    Result := Round(FBackGrBmp.Height - FRailHeight + (X - FBackGrBmp.Width * 0.049)
        * FTgBetta)
  else
    Result := Round(FBackGrBmp.Height - FRailHeight +
        ((FBackGrBmp.Width - X) - FBackGrBmp.Width * 0.049) * FTgBetta);
end;

function TAV16WheelsMnemo.TopY0(X: integer): integer;
begin
  if X < FBackGrBmp.Width / 2 then
    Result := Round(FBackGrBmp.Height - FRailHeight0 + (X - FBackGrBmp.Width * 0.049)
        * FTgBetta)
  else
    Result := Round(FBackGrBmp.Height - FRailHeight0 +
        ((FBackGrBmp.Width - X) - FBackGrBmp.Width * 0.049) * FTgBetta);
end;

function TAV16WheelsMnemo.TopY1(X: integer): integer;
begin
  if X < FBackGrBmp.Width / 2 then
    Result := Round(FBackGrBmp.Height - FRailHeight1 + (X - FBackGrBmp.Width * 0.049)
        * FTgBetta)
  else
    Result := Round(FBackGrBmp.Height - FRailHeight1 +
        ((FBackGrBmp.Width - X) - FBackGrBmp.Width * 0.049) * FTgBetta);
end;

function TAV16WheelsMnemo.TopY2(X: integer): integer;
begin
  if X < FBackGrBmp.Width / 2 then
    Result := Round(FBackGrBmp.Height - FRailHeight2 + (X - FBackGrBmp.Width * 0.049)
        * FTgBetta)
  else
    Result := Round(FBackGrBmp.Height - FRailHeight2 +
        ((FBackGrBmp.Width - X) - FBackGrBmp.Width * 0.049) * FTgBetta);
end;

//{$ENDIF}

{ TAV16Scheme3Mnemo }

constructor TAV16Scheme3Mnemo.Create(AOwner: TComponent; RailCnt,
  ChCnt: integer; PicsPath: string);
begin
  inherited;
  //FSegments.AddBase('sh3-null.wmf');
  FSegments.AddBase('sh3-null.emf');
  //FSegments.AddSegment(0, 2, '2-58N-L.wmf');
  //FSegments.AddSegment(1, 2, '2-58N-R.wmf');
  FSegments.AddSegment(0, 2, '3-58N-L.wmf');
  FSegments.AddSegment(1, 2, '3-58N-R.wmf');



  FSegments.AddSegment(0, 0, '0ZTM-L - SH3.wmf');
  FSegments.AddSegment(0, 1, '1-echo-L - SH3.wmf');
  FSegments.AddSegment(0, 3, '1-58O-L.wmf');
  FSegments.AddSegment(0, 4, '1-70N-L - SH3.wmf');
  //FSegments.AddSegment(0, 5, '3-58N-L.wmf');
  FSegments.AddSegment(0, 5, '2-58N-L.wmf');
  FSegments.AddSegment(0, 6, '1-42NW-L.wmf');
  FSegments.AddSegment(0, 7, '1-42OW-L.wmf');
  FSegments.AddSegment(0, 8, '1-42NB-L.wmf');
  FSegments.AddSegment(0, 9, '1-42OB-L.wmf');
  //FSegments.AddSegment(0, 12, '3-58N-L.wmf');
  FSegments.AddSegment(0, 12, '2-58N-L.wmf');

  FSegments.AddSegment(1, 0, '0ZTM-R - SH3.wmf');
  FSegments.AddSegment(1, 1, '1-echo-R - SH3.wmf');
  FSegments.AddSegment(1, 3, '1-58O-R.wmf');
  FSegments.AddSegment(1, 4, '1-70N-R - SH3.wmf');
  //FSegments.AddSegment(1, 5, '3-58N-R.wmf');
  FSegments.AddSegment(1, 5, '2-58N-R.wmf');
  FSegments.AddSegment(1, 6, '1-42NW-R.wmf');
  FSegments.AddSegment(1, 7, '1-42OW-R.wmf');
  FSegments.AddSegment(1, 8, '1-42NB-R.wmf');
  FSegments.AddSegment(1, 9, '1-42OB-R.wmf');
  //FSegments.AddSegment(1, 12, '3-58N-R.wmf');
  FSegments.AddSegment(1, 12, '2-58N-R.wmf');
end;

constructor TVMT_USSchemeMnemo.Create(AOwner: TComponent; RailCnt, ChCnt: integer;
  PicsPath: string);
begin
  inherited;
  FSegments.AddBase('m-rails-.wmf');

  FSegments.AddSegment(0, 0, 'm-wheels-left.wmf');
  FSegments.AddSegment(0, 1, 'm-wheels-red-left.wmf');

  FSegments.AddSegment(1, 0, 'm-wheels-right.wmf');
  FSegments.AddSegment(1, 1, 'm-wheels-red-right.wmf');
end;

end.
