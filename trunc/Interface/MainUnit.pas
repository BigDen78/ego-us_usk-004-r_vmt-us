/// ///////////////////////////////////////////////////////////////
/// ///
/// ������� Unit ��� ������14 � ��� �����������           ///
/// ///
/// ///////////////////////////////////////////////////////////////

unit MainUnit;

interface

{ TODO 5 : ��������� Uses � ����� }
uses Windows, Forms, SysUtils, Classes, Controls, ExtCtrls, UtilsUnit, StdCtrls,
  Graphics, PublicFunc, DefCoreUnit, BScanUnit, MCAN, AviconDataContainer,
  AviconTypes,
  Dialogs, AScanUnit, ConfigObj, APM, PublicData, Grids, Math, ComCtrls,
  PExceptions, Buttons, MessageUnit, SoundControlUnit, SkinSupport,
  CfgTablesUnit_2010, BScanThreadUnit, AviconDataSource,
  sSkinManager, sLabel, DateUtils, SimpleXML, MnemoUnit,
  FileTransfer, ProgramProfileConfig,
  CustomWindowUnit, MainMenuUnit, AScanFrameUnit,
  DeviceInterfaceUnit,
  MnemoFrameUnit, { NordcoCSV, } GPSModule, Spin, Keyboard;

/// ���� �� ������� !!!!
{$I Directives.inc}

const

  isENABLED_INPUT_STR = False; // default input string in TListWindow
  XML_VirtualKeyboardFileName = 'VK_Buttons_Cfg.xml';
  XMLConfigFileName = 'Av14Cfg.xml';
{$IFDEF Avikon11}
  DeviceFileExtension = '.a11';
{$ENDIF}
{$IFDEF Avikon12}
  DeviceFileExtension = '.a12';
{$ENDIF}
{$IFDEF Avikon14}
  DeviceFileExtension = '.a14';
{$ENDIF}
{$IFDEF Avikon14_2Block}
  DeviceFileExtension = '.a14';
{$ENDIF}
{$IFDEF Avikon15}
  DeviceFileExtension = '.a15';
{$ENDIF}
{$IFDEF Avikon15RSP}
  DeviceFileExtension = '.a15';
{$ENDIF}
{$IF DEFINED(Avikon16_IPV) OR DEFINED(EGO_USW) OR DEFINED(RKS_U) OR DEFINED(USK004R) OR DEFINED(VMT_US)}
  DeviceFileExtension = '.a16';
{$IFEND}
  ProgVersStatus = '';
{$IFDEF BUMCOUNT1}
  DevCount = 1;
{$ENDIF}
{$IFDEF BUMCOUNT2}
  DevCount = 2;
{$ENDIF}
{$IFDEF BUMCOUNT4}
  DevCount = 4;
{$ENDIF}
  BScanHeadPanelHeight = 6;
  BScanFootPanelHeight = 8;
  BScanChPanelWidth = 250;
{$IFDEF Avikon15RSP}
  ChButPos: array [0 .. 5, 1 .. 2] of integer =
    ((2, -1), (-1, -1), (-1, -1), (3, -1), (0, -1), (1, -1));
{$ENDIF}
{$IFDEF Avikon14_2Block}
  ChButPos: array [0 .. 7, 1 .. 2] of integer =
    ((2, 12), (3, 13), (4, -1), (5, -1), (6, 8), (7, 9), (0, 1), (10, 11));
{$ENDIF}
{$IFDEF Avikon11}
  ChButPos: array [0 .. 7, 1 .. 2] of integer =
    ((2, 10), (3, 11), (4, -1), (5, -1), (6, 8), (7, 9), (0, -1), (1, -1));
{$ENDIF}
{$IFDEF Avikon12}
  ChButPos: array [0 .. 7, 1 .. 2] of integer =
    ((2, 10), (3, 11), (4, -1), (5, -1), (6, 8), (7, 9), (0, -1), (1, -1));
{$ENDIF}
{$IFDEF USK004R}
  // ���������� �������
  ChButPos: array [0 .. 9, 1 .. 2] of integer =
    ((10, -1), (11, -1), (2, -1), (3, -1), (4, -1), (5, -1), (6, 8), (7, 9),
    (0, -1), (1, -1));
{$ENDIF}
{$IFDEF Avikon14}
  ChButPos: array [0 .. 7, 1 .. 2] of integer =
    ((2, 10), (3, 11), (4, -1), (5, -1), (6, 8), (7, 9), (0, -1), (1, -1));
{$ENDIF}
{$IFDEF Avikon16_IPV}
  ChButPosS: array [0 .. 7, 1 .. 2] of integer =
    ((2, 10), (3, 11), (4, -1), (5, -1), (6, 8), (7, 9), (0, -1), (1, -1));

  ChButPosS3: array [0 .. 7, 1 .. 2] of integer =
    ((2, 10), (3, 11), (4, -1), (5, 12), (6, 8), (7, 9), (0, -1), (1, -1));

  ChButPosW: array [0 .. 7, 1 .. 2] of integer =
    ((2, 12), (3, 13), (4, -1), (5, -1), (6, 8), (7, 9), (10, 11), (0, 1));
{$ENDIF}
{$IF DEFINED(EGO_USW)}
  { ChButPosW: array [0 .. 7, 1 .. 2] of integer =
    ((2, 12), (3, 13), (4, -1), (5, -1), (6, 8), (7, 9), (10, 11), (0, 1));
    ChButPosW2: array [0 .. 7, 1 .. 2] of integer =
    ((2, 12), (3, 13), (4, -1), (5, -1), (6, 8), (7, 9), (10, 11), (0, 1));
    }
  ChButPosW: array [0 .. 9, 1 .. 2] of integer =
    ((2, -1), (12, -1), (13, -1), (3, -1), (4, -1), (5, -1), (6, 8), (7, 9),
    (10, 11), (0, 1));
  ChButPosWR: array [0 .. 9, 1 .. 2] of integer =
    ((12, -1), (2, -1), (3, -1), (13, -1), (4, -1), (5, -1), (6, 8), (7, 9),
    (10, 11), (0, 1));

  ChButPosW2: array [0 .. 9, 1 .. 2] of integer =
    ((12, -1), (2, -1), (3, -1), (13, -1), (4, -1), (5, -1), (6, 8), (7, 9),
    (10, 11), (0, 1));
  ChButPosW2R: array [0 .. 9, 1 .. 2] of integer =
    ((2, -1), (12, -1), (13, -1), (3, -1), (4, -1), (5, -1), (6, 8), (7, 9),
    (10, 11), (0, 1));

  ChButPosW3: array [0 .. 9, 1 .. 2] of integer =
    ((2, 12), (3, 13), (4, -1), (5, -1), (6, 8), (7, 9), (10, 11), (0, 1),
    (14, -1), (15, -1));
{$IFEND}
{$IFDEF RKS_U}
  ChButPos: array [0 .. 7] of integer = (1, 2, 3, 4, 5, 6, 7, 8);
{$ENDIF}
  //
{$IFDEF VMT_US}
  ChButPosW: array [0 .. 11, 1 .. 2] of integer =
    ((10, -1), (4, -1), (12, -1), (2, -1), (6, 8), (0, -1), (11, -1), (5, -1),
    (13, -1), (3, -1), (7, 9), (1, -1));
{$ENDIF}
  //
  //
{$IFDEF RKS_U}
  HChButPos: array [0 .. 7] of integer = (0, 1, 2, 3, 4, -1, -1, -1);
{$ELSE}    // NOT RKS_U
{$IFDEF EGO_USW}
  HChButPos1: array [1 .. 2, 0 .. 9] of integer =
    ((0, 1, -1, -1, 2, -1, 3, -1, -1, 4), (5, 6, -1, -1, -1, -1, -1, -1, -1, -1)
    );
  HChButPos3: array [1 .. 2, 0 .. 9] of integer =
    ((0, -1, 1, -1, 2, -1, -1, 3, 4, -1), (5, -1, 6, -1, -1, -1, -1, -1, -1, -1)
    );
{$ELSE}    // NOT EGO_USW
{$IFDEF VMT_US}
  HChButPos: array [1 .. 2, 0 .. 11] of integer =
    ((0, 1, 2, 3, 4, 5, -1, -1, -1, -1, -1, -1), (6, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1));
{$ELSE}    // NOT VMT_US
{$IFDEF USK004R}
  HChButPos: array [1 .. 2, 0 .. 9] of integer =
    ((0, 1, 2, 3, 4, 5, 6, -1, -1, -1), (-1, -1, -1, -1, -1, -1, -1, -1, -1, -1)
    );
{$ELSE}
  HChButPos: array [1 .. 2, 0 .. 7] of integer =
    ((0, -1, 1, -1, 2, -1, 3, -1), (4, -1, 5, -1, 6, -1, -1, -1));
  // 0   1  2  3   4   5  6  7     0   1  2   3  4   5   6   7
{$ENDIF}
  //
{$ENDIF}
{$ENDIF}
{$ENDIF}
  ResultFilePath = 'ResFiles\';

  // NormalR65_Bottom_Delay = 60; // ��������� �������� ������� ��� ������ �65
  HeadPoint1 = 40; // ������ ���� � ������� �� 0 �� 40
  HeadPoint2 = 80; // ������ ���� �� 40 �� 80, ������ ����� 80

  ButHeight = 150;
  PanTitleH = 0.12;
  TitleFontH = 0.1;
  TitleTop = 0.01;
  TitleLeft = 0.015;
  SubTitleFontH = 0.05;
  SubTitleTop = 0.02;
  SubTitleLeft = 0.6;
  VerseFontH = 0.03;
  VerseTop = 0.095;
  VerseLeft = 0.015;
  FootH = 0.03;
  FootFontH = 0.03;
  ButW = 0.3;
  ButWMark = 0.25; // 0.3;
  ButLeft = 0.015;
  ButTop = 0.15;
  ButH = 0.1;
  ButFontH = 0.05;
  LabelLeft = 0.03;

  FBScanDefaultScale = 5;

  ScreenShotPath = 'Screens\';
  ScreenShotName = 'Av16_screen';

  ButtonPressTimeOut = 500;

  VKButtonModesCount = 3;

type
  TBootWindow = class;
  TExitProgramEvent = procedure of object;

  TListWindow = class;

  TGeneral = class(TControl)
  private
    FPanel: TPanel;
    FColorTable: TColorTable;
    FLanguageTable: TLanguageTable;
    FWinList: TList;
    FCurWin: TCustomWindow;
    FMode: TBScanWinMode;
    FSNYear: integer;
    FSNNum: integer;
    FListWindow: TListWindow;
    FListWindow2: TListWindow;
    FImitDP_On: Boolean;
    FPowerManager: TAPM;
    procedure RecieveCoreMsg(Msg: string; NewLine: Boolean);
    procedure ScreenShotToFile(FileName: string);
  protected
    procedure SetPanel(Value: TPanel);
    procedure SetWinMode(Value: TBScanWinMode);
    function GetBUISerialNumber: string;
    procedure BatteryLow(Sender: TObject);
  public
    NewCoreMsg: Boolean;
    MessageList: TStringList;
    Core: TDefCore;
    PixelFormat: TPixelFormat;
    OnExitProgram: TExitProgramEvent;
    OnRebootProgram: TExitProgramEvent;
    TestMnemoDelay: integer;
    TestMnemoNit: integer;
    TestMnemoKanal: integer;
    CurWindow: TCustomWindow;
    CurLogicalMode: TBScanWinMode;
    CurLogicalKanal: TKanal;
    FlWorkOnlyFlashDisk: Boolean; // F!
    FlashDiskModeHaveSet: Boolean; // F!
{$IFDEF GPS}
    GPSM: TGPSModule;
{$ENDIF}
    procedure TestMnemo;
    constructor Create(Parent: TForm);
    destructor Destroy; override;
    procedure ExitProgram;
    procedure RebootProgram;
    procedure Boot;
    procedure Start;
    procedure USBFlashBeforeStart;
    procedure DisposeUSBFlashBeforeStart;
    procedure OpenWindow(id: string);
    function GetWin(id: string): Pointer;
    procedure MakeScreenShot;
    procedure ShowHelp;
    procedure SetBUISN(Year, Num: integer);

    function GetListWindow: TListWindow;
    function GetListWindow2: TListWindow;

    procedure LoadFile(FileName: string);
    procedure SwitchImitDP;
    function GetProgVer: string;
    procedure OnKeyPressed(CharCode: Word; var Handled: Boolean);
{$IFDEF GPS}
    procedure OnCoordChange(Latitude, Longitude, Speed: Single);
    procedure OnStateChange(State: Boolean; UsedCount: integer);
{$ENDIF}
    property Panel: TPanel read FPanel write SetPanel;
    property ProgramVersion: string read GetProgVer;
    property Mode: TBScanWinMode read FMode write SetWinMode;
    property LanguageTable: TLanguageTable read FLanguageTable;
    property BUISN: string read GetBUISerialNumber;
    property SNYear: integer read FSNYear;
    property SNNum: integer read FSNNum;
    property ImitDP: Boolean read FImitDP_On;
    property PowerManager: TAPM read FPowerManager;
  end;

  TBootWindow = class(TCustomWindow)
  private
    FMemo: TMemo;
    FLastError: integer;
    FLastDevCount: integer;
    FlAllBUMsFound: Boolean;
    procedure OnExit(Sender: TObject);
    procedure OnReTry(Sender: TObject);
    procedure OnBoot(Sender: TObject);
    procedure RecieveCoreMsg(Msg: string; NewLine: Boolean);
    procedure ReTryBoot;
  protected
    procedure OnTimer(Sender: TObject); override;
  public
    procedure Open; override;
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
      override;
  end;

  TEnterSNWindow = class(TCustomWindow)
  private
    FLabel: TLabel;
    FButPanel: TControlPanelManager;
    FTopPanel: TPanel;
    FMidPanel: TPanel;
    FFootPanel: TPanel;
    CPanel: TControlPanel;
    YesBut, NoBut: TButton;
    Year, Num: TRegNumber;
    procedure Resize; override;
    procedure OnTimer(Sender: TObject); override;
  protected
    procedure OnYes(Sender: TObject);
    procedure OnNo(Sender: TObject);
  public
    procedure Open; override;
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
      override;
  end;

  TExitConfirmWindow = class(TCustomWindow)
  private
    FLabel: TLabel;
    FButPanel: TControlPanelManager;
    FFootPanel: TPanel;
    procedure Resize; override;
  protected
    procedure OnYes(Sender: TObject);
    procedure OnNo(Sender: TObject);
  public
    procedure Open; override;
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
      override;
  end;

  TListWindow = class(TCustomWindow)
  private
    FBut: TButton;
    FSecBut: TButton;
    FButPanel: TControlPanelManager;
    FItemsScrollBar: TCustomElementList;
    FTopPanel: TPanel;
    FFootPanel: TPanel;
    SB: TScrollBar;
    OldPos: integer;
    HeightListWin: integer;
{$IFDEF INPUTSTR_IN_LISTWIN}
    // FEnableInputStr: Boolean;
    FInputBut: TButton;
    Edit1: TEdit;
    FInputPanel: TPanel;
    FKeyb: TTouchKeyboard;
    FInputControl: TControlPanel;
    procedure CreateInputPanel(Panel: TPanel);
    procedure OnInputBut(Sender: TObject);
    procedure OnEnterText(Sender: TObject);
    procedure SetEnaInputStr(Val: Boolean);
{$ENDIF}
    procedure Resize; override;
    procedure SetCaption(Value: string);
    procedure SetButtonCaption(Value: string);
    procedure SetSecondButtonCaption(Value: string);
  protected
    procedure OnExit(Sender: TObject);
    procedure OnSecondBut(Sender: TObject);
    procedure SetItemSelectEvent(Value: TListItemSelectedEvent);
    procedure SetSelectEnable(Value: Boolean);
{$IFDEF INPUTSTR_IN_LISTWIN}
    procedure SaveToFile(FileName: String);
    // procedure SetEnaInputStr(isEnable: boolean);
    // procedure DisableInputStr;
    procedure SetKeyboardType(Value: string { TKeyboardLayout } );
    procedure VerticalMoveAllControls(Delta: integer);
    procedure ScrollChange(Sender: TObject);
{$ENDIF}
  public
    FileName: string;
    WinToExit: string;
    OnSecondButtonPress: TNotifyEvent;
    procedure Open; override;
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
      override;
    procedure Clear;
    procedure AddItem(id, Caption: string);
    procedure HideSecondButton;
    procedure ShowSecondButton;
    property ListHeader: string write SetCaption;
    property ButtonHeader: string write SetButtonCaption;
    property SecondButtonHeader: string write SetSecondButtonCaption;
    property OnItemSelected: TListItemSelectedEvent write SetItemSelectEvent;
    property SelectEnable: Boolean write SetSelectEnable;
    property EnableInputStr: Boolean write SetEnaInputStr;

  end;

  { TExtListWindow = class(TListWindow)
    private
    FInputBut: TButton;
    Edit1: TEdit;
    FKeyb: TTouchKeyboard;
    //    FInputPanel: TPanel;
    FInputControl: TControlPanel;
    procedure CreateInputPanel(Panel: TPanel);
    procedure OnInputBut(Sender: TObject);
    procedure OnEnterText(Sender: TObject);
    protected
    procedure OnExit(Sender: TObject);
    procedure OnSecondBut(Sender: TObject);
    procedure SetItemSelectEvent(Value: TListItemSelectedEvent);
    procedure SetSelectEnable(Value: Boolean);
    procedure SaveToFile(FileName: String);
    public
    FileName: string;
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
    override;
    end;
    }
  TRebootConfirmWindow = class(TCustomWindow)
  private
    FLabel: TLabel;
    FLabel2: TLabel;
    FButPanel: TControlPanelManager;
    FFootPanel: TPanel;
    procedure Resize; override;
  protected
    procedure OnYes(Sender: TObject);
    procedure OnNo(Sender: TObject);
  public
    procedure Open; override;
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
      override;
  end;

  TMarksWindow = class;

  TMarkMode = class(TWindowMode)
  public
    procedure Initialize(MRFCoord: TMRFCrd); overload; virtual;
    procedure Initialize(CaCoord: TCaCrd); overload; virtual;
    procedure SetMark; virtual;
  end;

  TMarkPK = class(TMarkMode)
  private
    CurrentKML: integer;
    CurrentKMR: integer;
    CurrentPKL: integer;
    CurrentPKR: integer;

    PKL: TCustomLabel;
    PKR: TCustomLabel;
    KM: TCustomLabel;

    KML: TCustomLabel;
    KMR: TCustomLabel;

    BtnLess: TButton;
    BtnMore: TButton;

    ImgPK: TImage;
    ImgKM: TImage;

    ScrollTimer: TTimer;
    BntLessPressed: Boolean;

    D: integer;
    FChainage: TCaCrd;

    RegChainage: TRegChainage;
    BtnKmLess: TButton;
    BtnKmMore: TButton;
    BtnPkLess: TButton;
    BtnPkMore: TButton;
    BtnMetrLess: TButton;
    BtnMetrMore: TButton;

    BtnTag: integer;

    procedure IncStolb;
    procedure DecStolb;
    procedure OnPressNext(Sender: TObject);
    procedure OnPressPrev(Sender: TObject);
    procedure OnKmLess(Sender: TObject);
    procedure OnKmMore(Sender: TObject);
    procedure OnPkLess(Sender: TObject);
    procedure OnPkMore(Sender: TObject);
    procedure OnMetrLess(Sender: TObject);
    procedure OnMetrMore(Sender: TObject);

    procedure OnKeyDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure OnKeyUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure Refresh;
    procedure OnScrollTimer(Sender: TObject);

  public
    constructor Create(Parent: TPaneledWindow);
    procedure Initialize(MRFCoord_: TMRFCrd); overload; override;
    procedure Initialize(CaCoord_: TCaCrd); overload; override;
    procedure SetMark; override;
    procedure SetActive; override;
  end;

  TMarkDefect = class(TMarkMode)
  private
    DefCodeBut: TRegButton;
    Rail: TRegComboBox;
{$IFDEF USK004R}
    // CSVRec: TNordcoCSVRecordLine;
    IsFailure: TRegComboBox;
    Size: TRegNumber;
    Size2: TRegNumber;
    Size3: TRegNumber;
    Label1: TLabel;
    Source: TRegComboBox;
    Status: TRegComboBox;
    AssetType: TRegComboBox;
    Comments: TEdit; // TMemo;
    // !!    Keyb: TTouchKeyboard;
    // procedure OnKeyPressed(CharCode:word);
{$ENDIF}
    procedure OnKraskopult(Sender: TObject);
    procedure ShowDefListWin(Sender: TObject);
    procedure OnDefectSelected(id: string);
  public
    constructor Create(Parent: TPaneledWindow);
    procedure Initialize(MRFCoord_: TMRFCrd); overload; override;
    procedure Initialize(CaCoord_: TCaCrd); overload; override;
    procedure SetMark; override;
    procedure SetActive; override;
  end;

  TMarkText = class(TMarkMode)
  private
    TxtBut: TRegButton;
    procedure ShowTxtListWin(Sender: TObject);
    procedure OnTxtSelected(id: string);
  public
    constructor Create(Parent: TPaneledWindow);
    procedure SetMark; override;
    procedure SetActive; override;
  end;

  TMarkSwitchNumber = class(TMarkMode)
  private
    FNumber: TRegNumber;
  public
    constructor Create(Parent: TPaneledWindow);
    procedure SetMark; override;
    procedure SetActive; override;
  end;

  TMarksWindow = class(TPaneledWindow)
  private
    FGP: TGroupBox;
    ModePK: TMarkPK;
    ModeDefect: TMarkDefect;
    ModeOther: TMarkText;
    ModeSwitchNum: TMarkSwitchNumber;

    procedure OnGoBack(Sender: TObject);
    procedure OnPlaceMark(Sender: TObject);

    procedure OnPkMark(Sender: TObject);
    procedure OnDefMark(Sender: TObject);
    procedure OnOtherMark(Sender: TObject);
    procedure OnSwitchNumber(Sender: TObject);

  protected
  public
    procedure Open; override;
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
      override;
  end;

  TLoadingMessageWindow = class(TCustomWindow)
  public
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
      override;
  end;

  TBScanRefresher = class(TThread)
  private
  protected
    procedure Execute; override;
  public
    BScan: TBScan;
    constructor Create;
  end;

  TBScanWindow = class(TCustomWindow)
  strict private
    // ������� ������������ � ���������� BScanWindow
    function CreateStandartPanel: TPanel;
    function CreateHeadPanel: TPanel;
    function CreateFootPanel: TPanel;
    function CreateSidePanel(Align: TAlign): TPanel;
    function CreateMiddlePanel: TPanel;

    procedure CreateChannelButtons; // �������� ������ �������
    procedure CreateZoomButtons; // �������� ������ ��������

    procedure CreateChannelButtonsRKS; // �������� ������ ������� ��� RKS-U
  private
    FMode: TBScanWinMode;
    FKanButWidth: integer;

    FButList: TList;
    FChButHelpVisible: Boolean;

    FBScan: TBScan;
    FAScanFrame: TAScanFrame;
{$IFDEF SimpleMnemo}
    FMenemoFrame: TCustomSimpleMnemoFrame;
{$ELSE}
    FMenemoFrame: TMnemoFrame;
{$ENDIF}
    FFirstShow: Boolean;
    FPrevSelectedBut: TChanelButton;
    FTestLabel: TLabel;
    FScaleButInc: TButton;
    FScaleButDec: TButton;

    FLeftPan: TPanel;
    FRightPan: TPanel;
    FMiddleWorkPan: TPanel;

    // HeadPanel
    HeadPanel: TPanel;
    FTimeLabel: TPanel;
    FBUIAkkLabel: TPanel;
    FBUMAkkLabel: TPanel;
    FRegLabel: TPanel;
    FVelLabel: TPanel;
    BUIAkkIndicator: TIndicator;
    BUMAkkIndicator: TIndicator;

    // FootPan
    FootPanel: TPanel;
    ButPanelManager: TControlPanelManager;
    ThresholdBut: TButton;

    FTVel: integer;
    FS: integer;
    FCoordLineH: integer;
    WorkH: integer;
    FCurKanalNum: integer;
    FModeChange: Boolean;
    FCurRail: integer;
    FBoltJointMode: Boolean;
    FButPressedTime: Int64;
    FBS: Boolean;

    FMaxAdjustPeriod: TDateTime;

    SparkTime: Int64;
    SparkTick: Boolean;

    FLastCoord: integer;
    ABItems: TABItemsList;
    IsSoundOn: Boolean;
    IsPaintButOn: Boolean;

    procedure OnExit(Sender: TObject);
    // procedure OnShowHelp(Sender: TObject);
    procedure Resize; override;
    procedure SetChButHelpVisible(Value: Boolean);
    // procedure ConfigureBScan;
    procedure OnTimer(Sender: TObject); override;
    procedure OnChButPress(Sender: TObject);
    procedure OnSearchBButPress(Sender: TObject);
    procedure OnSearchMButPress(Sender: TObject);
    procedure OnPlaceMarkButPress(Sender: TObject);
    procedure OnThresholdButPress(Sender: TObject);
    procedure OnScalePlusPress(Sender: TObject);
    procedure OnScaleMinusPress(Sender: TObject);
    procedure OnHandScan(Sender: TObject);
    procedure OnAdjustRailTypePress(Sender: TObject);
    procedure OnTest(Sender: TObject);
    procedure OnTest2(Sender: TObject);
    procedure OnPause(Sender: TObject);
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
    procedure OnPaintSprayer(Sender: TObject);
    procedure OnSound(Sender: TObject);
{$IFEND}
    procedure OnBoltStik(Sender: TObject);
    procedure SetAScanKanal(Kanal: TKanal);
    procedure KanalAdjusted(Rail, Num: integer);
    procedure ConfigChannelButtons;
    procedure OnCancelPause;

    function IsChAdjusted(Kanal: TKanal): Boolean;

    procedure EnableHandKanals(KanalsState: Boolean);
  protected
    procedure SetWinMode(Value: TBScanWinMode);
  public
    procedure ReInitBView;
    procedure OnShowHelp(Sender: TObject);
    procedure Open; override;
    procedure Test(Nit, Kanal, Delay, Amp: integer);
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
      override;
    destructor Destroy; override;
    property Mode: TBScanWinMode read FMode write SetWinMode;
  end;

  TAdjustDPWindow = class(TCustomWindow)
  private
    // FWay: Integer;
    FScanStep: Single;
    FTopGP, FBottomGP: TGroupBox;
    FExitBut, FClearBut: TButton;
    FInc, FDec: TButton;
    FWayLabel, FStepLabel: TLabel;
    FWayInc, FWayDec: Boolean;
    AvtoWayChangeTime: Int64;
    LastWayChange: Int64;
    procedure Resize; override;
    procedure OnExit(Sender: TObject);
    procedure OnClearWay(Sender: TObject);
    procedure OnTimer(Sender: TObject); override;
    procedure OnInc(Sender: TObject);
    procedure OnIncDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure OnIncUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure OnDec(Sender: TObject);
    procedure OnDecDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure OnDecUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
  protected
  public
    procedure Open; override;
    procedure Hide; override;
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
      override;
  end;

  TVKButtonModes = (vbDigits, vbLowerCase, vbUpperCase);
  TVKButtonTypes = (vtValue = 0, vtSpace = 1, vtBakSpace = 2, vtChangeMode = 3,
    vtMoveLeft = 4, vtMoveRight = 5);
  TVKButtonText = array [0 .. VKButtonModesCount - 1] of string;

  TVKButton = class
  private
    FParent: TWinControl;
    FButton: TButton;
    FMode: TVKButtonModes;
    FText: TVKButtonText;
    FRow: integer;
    FCol: integer;
    FRowsCount: integer;
    FColsCount: integer;
    FCurLetter: integer;
    FLastClickTime: Int64;
    FCurText: string;
    FLetter: Char;
    procedure OnClickEvent(Sender: TObject);
  protected
    function GetModeText(Index: integer): string;
    procedure SetModeText(Index: integer; Value: string);
    procedure SetMode(Value: TVKButtonModes);
    procedure SetWidth(Value: integer);
    procedure SetLeft(Value: integer);
    procedure SetHeight(Value: integer);
    procedure SetTop(Value: integer);
    function GetWidth: integer;
    function GetHeight: integer;
    function GetLeft: integer;
    function GetTop: integer;
    procedure SetRowsCount(Value: integer);
    procedure SetColsCount(Value: integer);
    procedure SetRow(Value: integer);
    procedure SetCol(Value: integer);
  public
    OnClick: TNotifyEvent;
    Num: integer;
    ButtonType: TVKButtonTypes;
    constructor Create(AOwner: TWinControl);
    destructor Destroy; override;
    procedure Resize;
    // property Width: Integer read GetWidth write SetWidth;
    // property Height: Integer read GetHeight write SetHeight;
    // property Left: Integer read GetLeft write SetLeft;
    // property Top: Integer read GetTop write SetTop;
    property RowsCount: integer read FRowsCount write SetRowsCount;
    property ColsCount: integer read FColsCount write SetColsCount;
    property Row: integer read FRow write SetRow;
    property Col: integer read FCol write SetCol;
    property Mode: TVKButtonModes read FMode write SetMode;
    property ModeText[Index: integer]
      : string read GetModeText write SetModeText;
    property Letter: Char read FLetter;
  end;

  TVKButtonContainer = class(TList)
  private
    FParent: TWinControl;
  protected
    function GetButton(Index: integer): TVKButton;
  public
    constructor Create(_Parent: TWinControl);
    destructor Destroy; override;
    procedure Clear; override;
    function AddVKButton: TVKButton;
    property VKButtons[Index: integer]: TVKButton read GetButton; default;
  end;

  TVirtualKeyboard = class(TPanel)
  private
    FButPanel: TPanel;
    FEnterButton: TButton;
    FVKButtonContainer: TVKButtonContainer;
    FColsCount: integer;
    FRowsCount: integer;
    FCT: TColorTable;
    FLT: TLanguageTable;
    FEdit: TEdit;
    FCurLetter: integer;
    FInputComplete: Boolean;
    procedure ParceXML(FileName: string);
    procedure Resize; override;
    procedure ButtonOnClick(Sender: TObject);
    procedure SetEditBox(Value: TEdit);
    procedure CursorLeft;
    procedure CursorRight;
    procedure CompleteInput;
    procedure BackSpace;
  protected
  public
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
    destructor Destroy; override;
    procedure EditOnClick;
  end;

  TAdjustSoftware = class(TCustomWindow)
  private
    Pan1: TPanel;
    Pan2: TPanel;
    Pan3: TPanel;
    GB: TGroupBox;
    SB: TScrollBar;
    Pan: TControlPanel;
    FTime: TDateTime;
    FDate: TDateTime;
    FMouseDown: Boolean;
    MouseTimer: TTimer;
    OY: integer;
    OldPos: integer;
    FMinTopControl, FMaxTopControl: TCustomInputItem;
    FCurLang: string;
    FCurAdjVar: integer;
    FCurSchema: integer;
    FCurUnitSys: integer;
    FCurAirbrushState: integer;
    FCurAirbrushConfig: string;
    FCurAirbrushSprayer: integer;
    FCurMasterBUM: Boolean;
    FCurSelectChan0: TSelectChan0; // integer;
    FCurDirectionOfMotion: Boolean;
    FDPBut: TRegButton;
    // VK: TVirtualKeyboard;
    procedure OnExitPress(Sender: TObject);
    procedure OnVolumeChange(Sender: TObject);
    procedure SetSystemDateTime(Date, Time: TDateTime);
    procedure MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure OnMouseTimer(Sender: TObject);
    procedure VerticalMoveAllControls(Delta: integer);
    procedure ScrollChange(Sender: TObject);

    procedure RunControlCenter(Sender: TObject); // EGO_USW
    procedure ChangeBUISN(Sender: TObject);
    procedure ResetChannelSettings(Sender: TObject);
    procedure DP_But_Press(Sender: TObject);

    // procedure ChangeLanguageOfLoader(New);
  public
    procedure Open; override;
    procedure Hide; override;
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
      override;
  end;

  TAdjTableWindow = class(TCustomWindow)
  private
    FExitBut: TButton;
    FSG: TStringGrid;
    FPB: TPaintBox;
    FBmp: TBitMap;
    // procedure Resize; override;
    procedure DrawTable;
    procedure RePaint(Sender: TObject);
    procedure OnExit(Sender: TObject);
    // procedure OnClearWay(Sender: TObject);
    // procedure OnTimer(Sender: TObject); override;
  protected
  public
    procedure Open; override;
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
      override;
    destructor Destroy; override;
  end;

var
  General: TGeneral;
  ProcessException: TProcessException;
  (* <Rud23> *)
  GAScanFrame: TAScanFrame;
  (* </Rud23> *)

implementation

Uses ShellAPI;
{ TGeneral }

procedure TGeneral.BatteryLow(Sender: TObject);
begin
  //
end;

procedure TGeneral.Boot;
var
  NewWin: TCustomWindow;
begin
  NewWin := TBootWindow.Create(FPanel, FColorTable, FLanguageTable);
  NewWin.id := 'BOOT';
  FWinList.Add(NewWin);
  NewWin.Open;
end;

constructor TGeneral.Create(Parent: TForm);
var
  SL: TStringList;
begin
  inherited Create(nil);

  FPowerManager := TAPM.Create(nil);
  FPowerManager.OnBatteryLow := BatteryLow;
  FPowerManager.Enabled := true;

  FImitDP_On := False;
  // ��������� ��������� ������ ���
  FSNYear := 0;
  FSNNum := 0;
  if FileExists(ExtractFilePath(Application.ExeName) + 'SYSTEM\sn.dat') then
  begin
    SL := TStringList.Create;
    SL.LoadFromFile(ExtractFilePath(Application.ExeName) + 'SYSTEM\sn.dat');
    if SL.Count = 2 then
    begin
      FSNYear := StrToInt(SL.Strings[0]);
      FSNNum := StrToInt(SL.Strings[1]);
    end;
    SL.Free;
  end;

  PixelFormat := pf32bit;
  FColorTable := TColorTable.Create(nil);
  FColorTable.LoadFromFile(ExtractFilePath(Application.ExeName)
      + XMLConfigFileName);
  FLanguageTable := TLanguageTable.Create(nil);
  FLanguageTable.LoadFromFile(ExtractFilePath(Application.ExeName)
      + XMLConfigFileName);
  FLanguageTable.CurrentGroup := Config.Language;
{$IFDEF Avikon15RSP}
  ControlFactory := TBasicControlFactory.Create; // } TSkinedControlFactory.Create( 'WMP 2008' );
{$ELSE}
  (* <Rud3> *)
  // {$IFDEF USK004R}
  // ControlFactory := TSkinedControlFactory.Create('MacMetal');
  // {$ELSE}
  ControlFactory := TSkinedControlFactory.Create('Cold');
  // {$ENDIF}
  (* </Rud3> *)
{$ENDIF}
  ControlFactory.InitializeForm(Parent);
  ControlFactory.DefaultFontName := DefaultFontName;
  ControlFactory.DefaultFontColor := FColorTable.Color['BootWin_ButtonText'];

  FWinList := TList.Create;
  FWinList.Clear;
  MessageList := TStringList.Create;
  MessageList.Clear;
  NewCoreMsg := False;
  Core := TDefCore.Create;
{$IFDEF TwoThreadsDevice}
  Core.TwoThreadDevice := true;
{$ELSE}
  Core.TwoThreadDevice := False;
{$ENDIF}
  Core.OnTextMessage := RecieveCoreMsg;
  Core.SndVolume := Config.SndVolume;
  FCurWin := nil;
  FlWorkOnlyFlashDisk := False;
  FlashDiskModeHaveSet := False;

  AviconTypes.SetCrdToStrCaptions(FLanguageTable['��'], FLanguageTable['��'],
    FLanguageTable['�'], FLanguageTable['��'], FLanguageTable['��'],
    FLanguageTable['Imperial'], FLanguageTable['Metric A (0.1km)'],
    FLanguageTable['Metric B (1km)']);
end;

destructor TGeneral.Destroy;
var
  i: integer;
begin
{$IFDEF GPS}
  GPSM.Free;
{$ENDIF}
  for i := 0 to FWinList.Count - 1 do
    if Assigned(FWinList.Items[i]) then
      TCustomWindow(FWinList.Items[i]).Free;
  FWinList.Free;
  FColorTable.Free;
  FLanguageTable.Free;
  MessageList.Free;
  FPowerManager.Free;
  if Assigned(Core) then
    Core.Free;

  KConf.Free;
  inherited;
end;

procedure TGeneral.ExitProgram;
begin

  if Assigned(OnExitProgram) then
    OnExitProgram;
end;

function TGeneral.GetBUISerialNumber: string;
begin
  Result := '';
  if (FSNYear > 0) and (FSNNum > 0) then
    Result := Format('%.2d%.3d', [FSNYear, FSNNum]);
end;

function TGeneral.GetListWindow: TListWindow;
begin
  Result := FListWindow;
  FListWindow.EnableInputStr := isENABLED_INPUT_STR;
end;

function TGeneral.GetListWindow2: TListWindow;
begin
  Result := FListWindow2;
end;

function TGeneral.GetProgVer: string;
var
  Major, Minor, Release, BuildNum: integer;
begin
  GetProgramVersion(Major, Minor, Release, BuildNum);
  if Release = 0 then
    Result := Format('%d.%d', [Major, Minor])
  else
    Result := Format('%d.%d.%d', [Major, Minor, Release]);
end;

procedure TGeneral.LoadFile(FileName: string);
begin
  Core.LoadExternFile(FileName);
  // ���� ����������� ���������, �� ������ ��������� �������������
  if not Core.RegOnUserLevel then
  begin
    Self.Core.StartTempRegistration;
    Self.Core.InsertWholeExternFile;
    Self.Core.CloseExternFile;
  end
  else // ���� ����������� �������� ��� �� �����, �� "���������" ������� � �������� �������������
    Core.AdditionalFileToShow := true;

  if ProgramProfile.DeviceFamily = 'RKS-U' then
    Core.AdditionalFileToShow := False;

  Self.Mode := bmSearch;
end;

procedure TGeneral.MakeScreenShot;
var
  sr: TSearchRec;
  FileAttrs, Num, FileNum: integer;
  NumS, Path, Fn: string;

begin
  FileAttrs := faAnyFile;
  CD(ScreenShotPath, true);
  Path := ExtractFilePath(Application.ExeName) + ScreenShotPath;
  FileNum := 0;
  if FindFirst(Path + '*.bmp', FileAttrs, sr) = 0 then
  begin
    repeat
      if (sr.Attr and FileAttrs) = sr.Attr then
      begin
        if sr.Attr <> faDirectory then
        begin
          NumS := Copy(sr.Name, Length(sr.Name) - 7, 4);
          Num := StrToInt(NumS);
          if Num > FileNum then
            FileNum := Num;
        end;
      end;
    until FindNext(sr) <> 0;
    FindClose(sr);
  end;
  Inc(FileNum, 1);
  if (FileNum >= 1) and (FileNum < 10) then
    NumS := Format('000%d', [FileNum])
  else if (FileNum >= 10) and (FileNum < 100) then
    NumS := Format('00%d', [FileNum])
  else if (FileNum >= 100) and (FileNum < 1000) then
    NumS := Format('0%d', [FileNum])
  else
    NumS := Format('%d', [FileNum]);
  Fn := Format('%s%s%s.bmp', [Path, ScreenShotName, NumS]);

  ScreenShotToFile(Fn);
end;

var
  testcount: integer = 0; // Debug

procedure TGeneral.OnKeyPressed(CharCode: Word; var Handled: Boolean);
begin
  if not General.Core.HandRegistrationOn then
    if CharCode = VK_RETURN then
    begin
      if Assigned(FCurWin) then
        if (FCurWin.id = 'PLACEMARK') then
          TMarksWindow(FCurWin).OnPlaceMark(Self)
        else if (FCurWin.id = 'BSCAN') then
        // if (General.Core.RegistrationStatus = rsOn) or
        // (General.Core.RegistrationStatus = rsPause) then
        begin
          OpenWindow('PLACEMARK');
          if FCurWin.id = 'PLACEMARK' then
            TMarksWindow(General.FCurWin).CMode := TMarksWindow(FCurWin).ModePK;
        end;
      Handled := true;
    end
    else if CharCode = VK_ADD then
    begin
      if Assigned(FCurWin) then
        if (FCurWin.id = 'PLACEMARK') then
          TMarksWindow(FCurWin).OnPlaceMark(Self)
        else if (FCurWin.id = 'BSCAN') then
        begin
          OpenWindow('PLACEMARK');
          if FCurWin.id = 'PLACEMARK' then
            TMarksWindow(General.FCurWin).CMode := TMarksWindow(FCurWin)
              .ModeDefect; // ModeOther;
        end;
      Handled := true;
    end;
{$IFDEF TESTKEYS} // �������� ������� �� ������� ������
  if CharCode = 45 { PVK_NUMPAD0 } then
  begin
    DeviceInterfaceUnit.LockCANThreads :=
      not DeviceInterfaceUnit.LockCANThreads;
    Handled := true;
  end;
{$ENDIF}
  (* //Debug
    else if CharCode=35{PVK_NUMPAD1} then begin
    //     if testcount=0 then General.Core.OnAlarmEvent(rLeft,testcount,False)
    //     else
    General.Core.OnAlarmEvent(rLeft,testcount,True);

    end
    else if CharCode=40{VK_NUMPAD2} then begin
    //     if testcount=0 then General.Core.OnAlarmEvent(rLeft,testcount,True)
    //     else
    General.Core.OnAlarmEvent({rRight}rLeft,testcount,False);
    inc(testcount); if testcount>High(Kanals[0]) then testcount:=0;

    end;
    *)
  (*
    {$IFDEF USK004R}
    // Get MarkDefect Window
    // Call that OnKeyPressed()
    // OnKeyPressed(CharCode:word);
    if Assigned(FCurWin) then
    if (FCurWin.id='PLACEMARK') then
    if Assigned(TMarksWindow(FCurWin).CMode) then
    if TMarksWindow(FCurWin).CMode = TMarksWindow(FCurWin).ModeDefect then
    TMarkDefect(TMarksWindow(FCurWin).CMode).OnKeyPressed(CharCode);
    {$ENDIF}
    *)
end;

procedure TGeneral.OpenWindow(id: string);
var
  i: integer;
begin
  for i := 0 to FWinList.Count - 1 do
    if TCustomWindow(FWinList.Items[i]).id = id then
    begin
      if Assigned(FCurWin) then
        FCurWin.Hide;
      FCurWin := TCustomWindow(FWinList.Items[i]);
      if id = 'BSCAN' then
        TBScanWindow(FCurWin).Mode := FMode;
      FCurWin.Visible := true;
      FCurWin.Open;
    end;
end;

function TGeneral.GetWin(id: string): Pointer;
var
  w: TCustomWindow;
  i: integer;
begin
  for i := 0 to FWinList.Count - 1 do
    if TCustomWindow(FWinList.Items[i]).id = id then
      break;
  if i < FWinList.Count then
    Result := FWinList.Items[i]
  else
    Result := Nil;
end;

procedure TGeneral.RebootProgram;
begin
  if Assigned(OnRebootProgram) then
    OnRebootProgram;
end;

procedure TGeneral.RecieveCoreMsg(Msg: string; NewLine: Boolean);
var
  st: string;
begin
  MessageList.Add(Msg);
end;

procedure TGeneral.ScreenShotToFile(FileName: string);
var
  Bmp: TBitMap;
begin
  Bmp := TBitMap.Create;
  Bmp.Width := Screen.Width;
  Bmp.Height := Screen.Height;
  BitBlt(Bmp.Canvas.Handle, 0, 0, Screen.Width, Screen.Height, GetDC(0), 0, 0,
    SRCCOPY);
  Bmp.SaveToFile(FileName);
  Bmp.Free;
end;

procedure TGeneral.SetBUISN(Year, Num: integer);
var
  SL: TStringList;
begin
  FSNYear := Year;
  FSNNum := Num;
  SL := TStringList.Create;
  SL.Add(IntToStr(FSNYear));
  SL.Add(IntToStr(FSNNum));
  SL.SaveToFile(ExtractFilePath(Application.ExeName) + 'SYSTEM\sn.dat');
  SL.Free;
end;

procedure TGeneral.SetPanel(Value: TPanel);
begin
  FPanel := Value;
  if Assigned(FColorTable) then
    FPanel.Color := FColorTable.Color['Main_BackGr'];
end;

procedure TGeneral.SetWinMode(Value: TBScanWinMode);
begin
  FMode := Value;
  CurLogicalMode := Value;
  General.OpenWindow('BSCAN');
end;

procedure TGeneral.ShowHelp;
var
  i: integer;
begin
  for i := 0 to FWinList.Count - 1 do
    if TCustomWindow(FWinList.Items[i]).id = 'BSCAN' then
      TBScanWindow(TCustomWindow(FWinList.Items[i])).OnShowHelp(nil);
end;
{$IFDEF GPS}

procedure TGeneral.OnCoordChange(Latitude, Longitude, Speed: Single);
begin
  if Assigned(Core) then
    if Assigned(Core.Registrator) then // ������� ����������� � �� ����������� �������
      if (Core.RegistrationStatus = rsOn) and Core.FRegOnUserLevel then
        if (FMode <> bmNastr) and (FMode <> bmHand) and (FMode <> bmNastrHand)
          then
          Core.Registrator.AddGPSCoord(Latitude, Longitude, Speed);
end;

procedure TGeneral.OnStateChange(State: Boolean; UsedCount: integer);
var
  Reserv: GPSStateReserv;
  i: integer;
begin
  for i := 0 to 5 do
    Reserv[i] := 0;
  if Assigned(Core) then
    if Assigned(Core.Registrator) then // ������� ����������� � �� ����������� �������
      if (Core.RegistrationStatus = rsOn) and Core.FRegOnUserLevel then
        if (FMode <> bmNastr) and (FMode <> bmHand) and (FMode <> bmNastrHand)
          then
          Core.Registrator.AddGPSState(Ord(State), UsedCount, true, Reserv);
end;
{$ENDIF}

procedure TGeneral.USBFlashBeforeStart;
var
  NewWin: TCustomWindow;
begin

  NewWin := TExitConfirmWindow.Create(FPanel, FColorTable, FLanguageTable);
  NewWin.id := 'ExitConfirm';
  FWinList.Add(NewWin);
  NewWin.Open;

  NewWin := TRebootConfirmWindow.Create(FPanel, FColorTable, FLanguageTable);
  NewWin.id := 'RebootConfirm';
  FWinList.Add(NewWin);
  NewWin.Open;

  FListWindow := TListWindow.Create(FPanel, FColorTable, FLanguageTable);
  FListWindow.id := 'ListWin';
  FWinList.Add(FListWindow);
  FListWindow.Open;

  { NewWin := TBootFlashMenu.Create(FPanel, FColorTable, FLanguageTable);
    NewWin.id := 'BootFlashMenu';
    FWinList.Add(NewWin);
    NewWin.Open;
    }

  NewWin := TMainMenuWindow.Create(FPanel, FColorTable, FLanguageTable);
  NewWin.id := 'MAINMENU';
  FWinList.Add(NewWin);
  // TMainMenuWindow(NewWin).SetButtonsEnabled(False);
  NewWin.Open;

end;

procedure TGeneral.DisposeUSBFlashBeforeStart;
var
  i: integer;
  p: TCustomWindow;
  bootw: TBootWindow;

  function GetWindow(id: string; var p: TCustomWindow): integer;
  var
    i: integer;
  begin
    p := Nil;
    Result := -1;
    for i := 0 to FWinList.Count - 1 do
      if TCustomWindow(FWinList.Items[i]).id = id then
      begin
        // FWinList.Delete(i);
        p := FWinList.Items[i];
        Result := i;
        break;
      end;
  end;

begin

  { for i := 0 to FWinList.Count - 1 do
    if Assigned(FWinList.Items[i]) then begin
    p:=TCustomWindow(FWinList.Items[i]);
    if p.id='BOOT' then bootw:=TBootWindow(p);
    if (p.id='ExitConfirm') or (p.id='RebootConfirm') or (p.id='MAINMENU') then begin
    FWinList.Delete(i); //TCustomWindow(FWinList.Items[i]).Free;
    p.Free;
    end;
    end;
    }
  GetWindow('BOOT', TCustomWindow(bootw));
  i := GetWindow('MAINMENU', p);
  if i >= 0 then
    FWinList.Delete(i);
  if Assigned(p) then
    p.Free;
  i := GetWindow('ExitConfirm', p);
  if i >= 0 then
    FWinList.Delete(i);
  if Assigned(p) then
    p.Free;
  i := GetWindow('RebootConfirm', p);
  if i >= 0 then
    FWinList.Delete(i);
  if Assigned(p) then
    p.Free;
  // FWinList.Clear;
  // bootw.ReTryBoot;
  FlWorkOnlyFlashDisk := False;
  General.OpenWindow('BOOT');
  if Assigned(bootw) then
    bootw.FTimer.Enabled := true
  else
    ExitProgram;

  // Boot;

end;

procedure TGeneral.Start;
var
  NewWin: TCustomWindow;
begin

  NewWin := TBScanWindow.Create(FPanel, FColorTable, FLanguageTable);
  NewWin.id := 'BSCAN';
  TBScanWindow(NewWin).Mode := bmOff;
  FWinList.Add(NewWin);

  NewWin := TMarksWindow.Create(FPanel, FColorTable, FLanguageTable);
  NewWin.id := 'PLACEMARK';
  FWinList.Add(NewWin);

  NewWin := TAdjustDPWindow.Create(FPanel, FColorTable, FLanguageTable);
  NewWin.id := 'ADJDP';
  FWinList.Add(NewWin);

  NewWin := TAdjustSoftware.Create(FPanel, FColorTable, FLanguageTable);
  NewWin.id := 'ADJSOFT';
  FWinList.Add(NewWin);

  NewWin := TAdjTableWindow.Create(FPanel, FColorTable, FLanguageTable);
  NewWin.id := 'ADJTABLE';
  FWinList.Add(NewWin);

  NewWin := TLoadingMessageWindow.Create(FPanel, FColorTable, FLanguageTable);
  NewWin.id := 'LOADINGMSG';
  FWinList.Add(NewWin);
  NewWin.Open;

  NewWin := TExitConfirmWindow.Create(FPanel, FColorTable, FLanguageTable);
  NewWin.id := 'ExitConfirm';
  FWinList.Add(NewWin);
  NewWin.Open;

  NewWin := TRebootConfirmWindow.Create(FPanel, FColorTable, FLanguageTable);
  NewWin.id := 'RebootConfirm';
  FWinList.Add(NewWin);
  NewWin.Open;

  FListWindow := TListWindow.Create(FPanel, FColorTable, FLanguageTable);
  FListWindow.id := 'ListWin';
  FWinList.Add(FListWindow);
  FListWindow.Open;

  FListWindow2 := TListWindow.Create(FPanel, FColorTable, FLanguageTable);
  FListWindow2.id := 'ListWin2';
  FWinList.Add(FListWindow2);
  FListWindow2.Open;

  if Self.BUISN = '' then
  begin
    NewWin := TMainMenuWindow.Create(FPanel, FColorTable, FLanguageTable);
    NewWin.id := 'MAINMENU';
    FWinList.Add(NewWin);
    NewWin.Open;

    NewWin := TEnterSNWindow.Create(FPanel, FColorTable, FLanguageTable);
    NewWin.id := 'EnterSN';
    FWinList.Add(NewWin);
    NewWin.Open;
  end
  else
  begin
    NewWin := TEnterSNWindow.Create(FPanel, FColorTable, FLanguageTable);
    NewWin.id := 'EnterSN';
    FWinList.Add(NewWin);
    NewWin.Open;

    NewWin := TMainMenuWindow.Create(FPanel, FColorTable, FLanguageTable);
    NewWin.id := 'MAINMENU';
    FWinList.Add(NewWin);
    NewWin.Open;
  end;
{$IFDEF GPS}
  GPSM := TGPSModule.Create;
  GPSM.OnCoordChange := OnCoordChange;
  GPSM.OnStateChange := OnStateChange;
  GPSM.Start;
{$ENDIF}
{$IFDEF UseHardware}
  Core.StartTempRegistration;
{$ENDIF}
end;

procedure TGeneral.SwitchImitDP;
begin
  FImitDP_On := not FImitDP_On;
  if Assigned(Core) then
  begin
    Core.ImitDP(255, FImitDP_On);
  end;
end;

procedure TGeneral.TestMnemo;
var
  i: integer;
begin
  for i := 0 to FWinList.Count - 1 do
    if TCustomWindow(FWinList.Items[i]).id = 'BSCAN' then
      TBScanWindow(FWinList.Items[i]).Test
        (Self.TestMnemoNit, Self.TestMnemoKanal, Self.TestMnemoDelay, 100);
end;

{ TBootWindow }

constructor TBootWindow.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);
var
  But, But2: TButton;
  Title: TCustomLabel;
  SubTitle: TCustomLabel;
  Vers: TCustomLabel;
  PanBut: TPanel;
  PanMemo: TPanel;
  PanTitle: TPanel;
  s: Cardinal;
  a: integer;

const
  PanTitleH = 0.2;
  TitleFontH = 0.1;
  TitleTop = 0.01;
  TitleLeft = 0.015;
  SubTitleFontH = 0.05;
  SubTitleTop = 0.1;
  SubTitleLeft = 0.015;
  VerseFontH = 0.03;
  VerseTop = 0.095;
  VerseLeft = 0.015;

  FootH = 0.03;
  FootFontH = 0.03;
  PanButH = 0.1;
  ButLeft = 0.015;
  ButWidth = 0.23;
  ButFontH = 0.05;

  MemoLeft = 0.015;
  MemoTop = 0.02;
  MemoWidth = 0.8;
  MemoH = 0.04;
  MemoFontH = 0.035;
begin
  inherited;
  FTimer.Interval := 350; // 500; 20-10-2015
  FLastDevCount := -1;
  FLastError := 0;
  FlAllBUMsFound := False;
  // a:=Self.Width;
  PanTitle := TPanel.Create(nil);
  PanTitle.Parent := Self;
  PanTitle.ParentColor := False;
  PanTitle.ParentBackground := False;
  PanTitle.BevelOuter := bvNone;
  PanTitle.Color := Self.Color;
  PanTitle.Height := Round(Self.Height * PanTitleH);
  PanTitle.Align := alTop;
  Obj.Add(PanTitle);

  Title := ControlFactory.CreateLabel(PanTitle);
  ControlFactory.InitializeLabel(Title, Round(Self.Height * TitleFontH));
  Title.Parent := PanTitle;

  Title.Left := Round(Self.Width * TitleLeft);
  Title.Top := Round(Self.Height * TitleTop);
  Title.Caption := ProgramProfile.ProgramTitle;
  Title.Visible := true;

  SubTitle := ControlFactory.CreateLabel(PanTitle);
  SubTitle.Parent := PanTitle;
  ControlFactory.InitializeLabel(SubTitle, Round(Self.Height * TitleFontH));

  SubTitle.Top := Round(Self.Height * SubTitleTop);
  SubTitle.Left := Round(Self.Width * SubTitleLeft);
  SubTitle.Visible := true;
  s := GetFileVersion(Application.ExeName);

  Vers := ControlFactory.CreateLabel(PanTitle);
  Vers.Parent := PanTitle;
  ControlFactory.InitializeLabel(Vers, Round(Self.Height * VerseFontH));
{$IFDEF DemoVersion}
  Vers.Caption := FLT.Caption['ProgVersDemo'] + General.ProgramVersion +
    ProgVersStatus;
{$ELSE}
  Vers.Caption := FLT.Caption['ProgVers'] + General.ProgramVersion +
    ProgVersStatus;
{$ENDIF}
  Vers.Left := Round(Self.Width * VerseLeft);
  Vers.Top := Round(Self.Height * VerseTop);

  PanBut := TPanel.Create(nil);
  PanBut.Parent := Self;
  PanBut.ParentColor := False;
  PanBut.ParentBackground := False;
  PanBut.BevelOuter := bvNone;
  PanBut.Height := Round(Self.Height * PanButH);
  PanBut.Align := alBottom;
  PanBut.Color := Self.Color;
  PanBut.Visible := true;
  Obj.Add(PanBut);

  PanMemo := TPanel.Create(nil);
  PanMemo.Parent := Self;
  PanMemo.ParentColor := False;
  PanMemo.ParentBackground := False;
  PanMemo.BevelOuter := bvNone;
  PanMemo.Color := Self.Color;
  PanMemo.Align := alClient;
  PanMemo.Visible := true;
  PanMemo.DoubleBuffered := true;
  Obj.Add(PanMemo);

  FMemo := TMemo.Create(PanMemo);
  FMemo.Parent := PanMemo;
  FMemo.DoubleBuffered := true;
  FMemo.Left := Round(Self.Width * MemoLeft);
  FMemo.Top := Round(Self.Height * MemoTop);
  FMemo.Width := Round(Self.Width * MemoWidth);
  FMemo.Height := Self.Height - PanBut.Height - PanTitle.Height - Round
    (Self.Height * MemoH * 4);

  FMemo.Lines.Clear;
  FMemo.Color := Self.Color;
  FMemo.Font.Color := FCT.Color['BootWin_LogText'];
  FMemo.Font.Name := DefaultFontName;
  FMemo.Font.Height := Round(Self.Height * MemoFontH);
  FMemo.ReadOnly := true;
  FMemo.Lines.Add('������� �������������...');
  FMemo.Visible := true;

  But := ControlFactory.CreateButton(PanBut);
  But.Parent := PanMemo;
  But.Width := Round(Self.Width * ButWidth);
  But.Left := Round(Self.Width * ButLeft);
  But.Height := PanBut.Height;
  But.Top := FMemo.Top + FMemo.Height + 10;
  But.Font.Name := DefaultFontName;
  But.Font.Color := FCT.Color['BootWin_ButtonText'];
  But.Font.Height := Round(Self.Height * ButFontH);
  But.Caption := FLT.Caption['Exit'];
  But.Visible := true;
  But.OnClick := OnExit;

  But2 := ControlFactory.CreateButton(PanBut);
  But2.Parent := PanMemo;
  But2.Width := Round(Self.Width * ButWidth);
  But2.Left := But.Left + But.Width + Round(Self.Width * ButLeft) + 5;
  But2.Height := PanBut.Height;
  But2.Top := FMemo.Top + FMemo.Height + 10;
  But2.Font.Name := DefaultFontName;
  But2.Font.Color := FCT.Color['BootWin_ButtonText'];
  But2.Font.Height := Round(Self.Height * ButFontH);
  But2.Caption := FLT.Caption['Retry'];
  But2.Visible := False;
  But2.OnClick := OnReTry;
  But2.Enabled := False;

end;

procedure TBootWindow.OnBoot(Sender: TObject);
var
  RegParam: TRegParamList;
  res: integer;
begin
  General.Core.Boot_LoadConfig; // �������� �������
{$IFDEF BUMCOUNT1}
  FMemo.Lines.Add('');
{$ENDIF}
{$IFDEF BUMCOUNT2}
  FMemo.Lines.Add('');
  FMemo.Lines.Add('');
{$ENDIF}
{$IFDEF BUMCOUNT4}
  FMemo.Lines.Add('');
  FMemo.Lines.Add('');
  FMemo.Lines.Add('');
  FMemo.Lines.Add('');
{$ENDIF}
  General.Core.Boot_TryRunBUM; // ������ ������ ���������
end;

procedure TBootWindow.OnExit(Sender: TObject);
begin
  General.ExitProgram;
end;

procedure TBootWindow.OnReTry(Sender: TObject);
var
  res: integer;
begin
  Self.ReTryBoot;
  if FLastError = 0 then
    General.Start;
end;

procedure TBootWindow.OnTimer(Sender: TObject);
label l1;
var
  res, i, j, c, c1, BUM: integer;
  RegParam: TRegParamList;
  FRegFileName, DateSt: string;
  BUMcnt: integer;
  FlMasterIsSet: Boolean;
  mastercnt, StatusIsSetCnt: integer;
  Kind: TWinKind;
  m: integer;
  FlashLetter: Char;

  procedure SetTaktCANObj(c: integer);
  var
    i, j, c1, BUM: integer;
  begin
    for i := 0 to c - 1 do
    begin
      BUM := General.Core.CANS[i].id;
      c1 := Length(Takts[BUM, 0]);
      for j := 0 to c1 - 1 do
      begin
{$IFDEF BUMCOUNT1}
        Takts[BUM, 0, j].CANObj := General.Core.CANS[i];
{$IFDEF TwoThreadsDevice}
        Takts[BUM, 1, j].CANObj := General.Core.CANS[i];
{$ENDIF}
{$ENDIF}
{$IF DEFINED(BUMCOUNT2) or DEFINED(BUMCOUNT4)}
        if Assigned(Takts[BUM, 0, j]) then
          Takts[BUM, 0, j].CANObj := General.Core.CANS[i];
        if Assigned(Takts[BUM, 1, j]) then
          Takts[BUM, 1, j].CANObj := General.Core.CANS[i];
{$IFEND}
      end;
      c1 := Length(HTakts);
      for j := 0 to c1 - 1 do
        HTakts[j].CANObj := General.Core.CANS[0];
    end;
  end;

begin
  inherited;
{$IFNDEF UseHardware}
  FTimer.Enabled := False;
  General.Start;
  Exit;
{$ENDIF}
{$IF NOT DEFINED(BUMCOUNT2) AND NOT DEFINED(BUMCOUNT4)}
  if not General.Core.AV14USBOpened then
    General.Core.Av14USBOpen;
{$IFEND}
  General.Core.CAN_SendTest;
  c := General.Core.USBDevicesCount;
  if c <> FLastDevCount then
{$IFDEF BUMCOUNT1}
    case c of
      0:
        FMemo.Lines[FMemo.Lines.Count - 1] := Format
          ('%s... %s', [FLT.Caption['BUM'], FLT.Caption['BUMSearch']]);
      1:
        FMemo.Lines[FMemo.Lines.Count - 1] := Format
          ('%s... %s, %s: %d', [FLT.Caption['BUM'], FLT.Caption['BUMFound'],
          FLT['�������� �����'], General.Core.BUMSerialNum[0]]);
    end;
{$ENDIF}
{$IFDEF BUMCOUNT2}
  case c of
    0:
      FMemo.Lines[FMemo.Lines.Count - 2] := Format
        ('%s... %s', [FLT.Caption['BUM'], FLT.Caption['BUMSearch']]);
    1:
      begin
        if General.Core.CANList[0].BUMReady then
          FMemo.Lines[FMemo.Lines.Count - 2] := Format
            ('%s 1... %s, %s: %d', [FLT.Caption['BUM'], FLT.Caption['BUMFound']
              , FLT['�������� �����'], General.Core.BUMSerialNum[0]])
        else
          FMemo.Lines[FMemo.Lines.Count - 2] := Format
            ('%s 2... %s, %s: %d', [FLT.Caption['BUM'], FLT.Caption['BUMFound']
              , FLT['�������� �����'], General.Core.BUMSerialNum[1]]);
        FMemo.Lines[FMemo.Lines.Count - 1] := Format
          ('%s... %s', [FLT.Caption['BUM'], FLT.Caption['BUMSearch']]);
      end;
{$IFNDEF USK004R}
    2:
      begin
        FMemo.Lines[FMemo.Lines.Count - 2] := Format
          ('%s 1... %s, %s: %d', [FLT.Caption['BUM'], FLT.Caption['BUMFound'],
          FLT['�������� �����'], General.Core.BUMSerialNum[0]]);
        FMemo.Lines[FMemo.Lines.Count - 1] := Format
          ('%s 2... %s, %s: %d', [FLT.Caption['BUM'], FLT.Caption['BUMFound'],
          FLT['�������� �����'], General.Core.BUMSerialNum[1]]);
      end;
{$ENDIF}
  end;
{$ENDIF}
{$IFDEF BUMCOUNT4}
  begin
    if c = 0 then
      FMemo.Lines[FMemo.Lines.Count - 4] := Format
        ('%s... %s', [FLT.Caption['BUM'], FLT.Caption['BUMSearch']])
    else
    begin
      BUMcnt := 0;
      for i := 0 to c - 1 do
        if General.Core.CANList[i].BUMReady then
        begin
          FMemo.Lines[FMemo.Lines.Count - 4 + BUMcnt] := Format
            ('%s %d... %s, %s: %d', [FLT.Caption['BUM'], BUMcnt,
            FLT.Caption['BUMFound'], FLT['�������� �����'],
            General.Core.BUMSerialNum[i]]);
          Inc(BUMcnt);
        end;
      if BUMcnt < 4 then
        FMemo.Lines[FMemo.Lines.Count - 4 + BUMcnt] := Format
          ('%s... %s', [FLT.Caption['BUM'], FLT.Caption['BUMSearch']]);
    end;
  end;
{$ENDIF}
  // if FlWorkOnlyFlashDisk then begin
  if GetFlashDriveNew(Config.FlashDrive, FlashLetter) then
  begin
    if not General.FlashDiskModeHaveSet then
    begin
      FTimer.Enabled := False;
      // General.Core.StopDeviceSearching;
      General.FlashDiskModeHaveSet := true;
      General.FlWorkOnlyFlashDisk := true;
      General.USBFlashBeforeStart;
      // General.Start;
      Exit;
    end;
  end
  else
  begin
    General.FlashDiskModeHaveSet := False;
    General.FlWorkOnlyFlashDisk := False;
  end;

  FLastDevCount := c;
  if c = DevCount then
  begin
{$IFDEF BUMCOUNT4}
    if not FlAllBUMsFound then
    begin
      FlAllBUMsFound := true;
{$ELSE}
      FTimer.Enabled := False;
{$ENDIF}
      General.Core.StopDeviceSearching;
    l1 :
      SetTaktCANObj(c);
{$IFDEF BUMCOUNT4}
    end;
    mastercnt := 0;
    FlMasterIsSet := False;
    StatusIsSetCnt := 0;
    for i := 0 to c - 1 do
    begin
      if General.Core.CANS[i].BlockStatus = 0 then
        Inc(mastercnt);
      if General.Core.CANS[i].BlockStatus >= 0 then
        Inc(StatusIsSetCnt);
    end;
    if StatusIsSetCnt >= 4 then
    begin
      FTimer.Enabled := False;
      if mastercnt >= 1 then
      begin
        m := -1;
        for i := 0 to c - 1 do
          with General.Core.CANS[i] do
            if BlockStatus = 0 then
            begin
              if FlMasterIsSet then
                BlockStatus := 1 // ���� >1 �������, ���������� ������ ���� ��� ��������
              else
              begin
                FlMasterIsSet := true;
                m := i;
              end;
              SetStatus;
            end
            else
              SetStatus;
        // if (not FStatusIsSet) and (FBUMReady) then
        // SetStatus; //������� ����� � tcan
        if m >= 0 then
          if m <= 1 then
          begin
            if not Config.MasterBUMLeft then
            begin
              General.Core.ChangeLeftRightSide();
              SetTaktCANObj(c);
            end
          end
          else if Config.MasterBUMLeft then
          begin
            General.Core.ChangeLeftRightSide();
            SetTaktCANObj(c);
          end
      end
      else
      begin // error
        // if mastercnt=0 then
        ShowMessage(FLT.Caption['�� ��������� ������ ������� ����'])
        // else ShowMessage(FLT.Caption['�� �������� ������ ��']);
        // Kind := kBad;
        // MessageForm.Display('�� ��������� ������ ��', Kind);
      end;
      General.Start;
    end
    else
    begin
      for i := 0 to c - 1 do
        if General.Core.CANS[i].BlockStatus < 0 then
          General.Core.CANS[i].AskMasterSlaveStatus;
    end;
{$ELSE}
    General.Start;
{$ENDIF}
  end;
end;

procedure TBootWindow.Open;
begin
  inherited;
  FMemo.Clear;
  General.Core.OnTextMessage := RecieveCoreMsg;
  OnBoot(nil);
end;

procedure TBootWindow.RecieveCoreMsg(Msg: string; NewLine: Boolean);
var
  st: string;
begin
  if NewLine then
    FMemo.Lines.Add(FLT.Caption[Msg])
  else
    FMemo.Lines[FMemo.Lines.Count - 1] := FMemo.Lines[FMemo.Lines.Count - 1]
      + FLT.Caption[Msg];
end;

procedure TBootWindow.ReTryBoot;
var
  CurError, LastSt: integer;
begin
  CurError := 0;
  FMemo.Clear;
  FMemo.Font.Color := clBlack;
  FMemo.RePaint;
  if ((FLastError and 1) = 1) or ((FLastError and 2) = 2) or
    ((FLastError and 4) = 4) then
  begin
    CurError := CurError + General.Core.Boot_LoadConfig;
    if CurError <> 0 then
    begin
      FMemo.Font.Color := clRed;
      FLastError := CurError;
      Exit;
    end;
  end;
  if ((FLastError and 8) = 8) or ((FLastError and 16) = 16) or
    ((FLastError and 32) = 32) then
  begin
    LastSt := FMemo.Lines.Add(FLT.Caption['BUMConnect']);
    CurError := CurError + General.Core.Boot_TryRunBUM;
    if CurError <> 0 then
    begin
      FMemo.Font.Color := clRed;
      FMemo.Lines[LastSt] := FLT.Caption['BUMConnectErr'] + ': ' + IntToStr
        (CurError);
      FLastError := CurError;
      Exit;
    end
    else
      FMemo.Lines[LastSt] := FMemo.Lines[LastSt] + ' OK';
  end;
end;

procedure TListWindow.AddItem(id, Caption: string);
begin
  FItemsScrollBar.AddItem(id, Caption);
  // SB.Max:= * round(HeightListWin{FItemsScrollBar.Height}/ceITEMSONSCREEN);
  // SB.Max:=FItemsScrollBar.ListHeight;//max(FItemsScrollBar.ListHeight, 80);
  SB.Max := Max(FItemsScrollBar.ListHeight, 100); // HeightListWin);//FYOffset;
  SB.PageSize := Round(SB.Max / 10); // * 80/HeightListWin);//80;
  // repaint;
end;

procedure TListWindow.Clear;
begin
  FItemsScrollBar.Clear;
  if Assigned(SB) then
  begin
    SB.PageSize := 0;
    // SB.SetParams(0,0,100);
    SB.Min := 0; // 100;
    SB.Max := 100; // 100;
  end;
end;
{$IFDEF INPUTSTR_IN_LISTWIN}

procedure TListWindow.CreateInputPanel(Panel: TPanel);
begin
  FInputPanel := TPanel.Create(Self { Panel } );
  FInputPanel.ParentColor := False;
  FInputPanel.Parent := { Panel;// } Self;
  FInputPanel.ParentBackground := False;
  FInputPanel.Visible := False; // true;//true;
  FInputPanel.Caption := '';
  FInputPanel.Color := Self.Color; // clBlack;//
  FInputPanel.BevelOuter := bvNone;
  FInputPanel.BorderWidth := 0;
  FInputPanel.BorderStyle := bsNone;
  FInputPanel.Ctl3D := False;
  FInputPanel.Align := alClient; // alCustom;//alBottom;
  FInputPanel.DoubleBuffered := true;
  // FInputPanel.Height := 300;//Panel.Height*0.5;
  FInputPanel.Font.Color := clBlack;
  FInputPanel.Font.Name := DefaultFontName;
  FInputPanel.Font.Height := Round(0.08 * Self.Height);

  (*
    FInputControl:=TControlPanel.Create//(FInputPanel, FCT, FLT);
    ('InputKeyb', poVertical, {caBottom}caWholeSize, 11);
    FInputControl.Color := Self.Color;
    FInputControl.Parent:=Self{FInputPanel;//};

    FInputControl.Font.Color := clBlack;
    FInputControl.Font.Name := DefaultFontName;
    FInputControl.Font.Height := Round(0.07{0.08} * Self.Height);


    //  Edit1:=TEdit.Create(FInputPanel);
    //  Edit1.Parent:=FInputPanel;
    Edit1:=TEdit.Create(FInputControl);
    FInputControl.Add(Edit1);

    //  FKeyb:=TTouchKeyboard.Create(FInputPanel);
    //  FKeyb.Parent:=FInputPanel;
    FKeyb:=TTouchKeyboard.Create(FInputControl);
    FInputControl.Add(FKeyb);
    FKeyb.Height:=round(0.3*FInputControl.Height);
    //  FKeyb.ScaleBy(2,5);
    *)

  Edit1 := TEdit.Create(FInputPanel);
  Edit1.Parent := FInputPanel;
  Edit1.Top := Round(0.2 * Self.Height);
  Edit1.Left := Self.Left + 30;
  Edit1.Width := Self.Width - 60;
  Edit1.Height := Self.Font.Height + 4;

  FKeyb := TTouchKeyboard.Create(FInputPanel);
  FKeyb.Parent := FInputPanel;
  // FKeyb.Top:=round(0.5*Self.Height);
  // FKeyb.Left:=Self.Left+20;
  // FKeyb.Width:=Self.Width-40;
  FKeyb.Top := Round(0.35 * Self.Height); // 0.4
  FKeyb.Left := Self.Left + 30;
  FKeyb.Width := Self.Width - 60;
  FKeyb.Height := Round(0.33 * Self.Height); // 0.35

end;
{$ENDIF}

procedure TListWindow.ScrollChange(Sender: TObject);
var
  D: integer;
begin
  D := -(SB.Position - OldPos);
  VerticalMoveAllControls(D { *FItemsScrollBar.Height/ceITEMSONSCREEN } );
  OldPos := SB.Position;
end;

procedure TListWindow.VerticalMoveAllControls(Delta: integer);
var
  i, D: integer;
begin
  // D:= Delta;
  (* if Assigned(FMinTopControl) then
    if (FMinTopControl.Top + Delta) > 7 then
    if FMinTopControl.Top  > 7 then
    Exit
    else D:= 7 - FMinTopControl.Top;
    if Assigned(FMaxTopControl) then
    if (FMaxTopControl.Top + Delta - 1) < Pan.Height - 100 then
    if (FMaxTopControl.Top - 1) < Pan.Height - 100 then
    Exit
    else D:= Pan.Height - 100 - FMaxTopControl.Top + 1;

    if Assigned(Pan) then
    for i := 0 to Pan.ControlCount - 1 do
    TCustomInputItem(Pan.Controls[i]).MoveVertical(D{Delta});
    *)
  // if (FEndPos

  // FItemsScrollBar.ScrollBy(0, D);
  // FItemsScrollBar.Repaint;
  FItemsScrollBar.Scroll(Delta);

end;

procedure TListWindow.SetKeyboardType(Value: string { TKeyboardLayout } );
begin
  if Assigned(SB) then
    FKeyb.Layout := Value;
end;

constructor TListWindow.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);
var
  FLng: TRegComboBox;
  h: integer;
begin
  inherited;
  Self.Caption := '';

  Self.DoubleBuffered := true;
  FTimer.Enabled := False;

  // ������� �������� � ����������
  FTopPanel := TPanel.Create(Self);
  FTopPanel.ParentColor := False;
  FTopPanel.Parent := Self;
  FTopPanel.ParentBackground := False;
  FTopPanel.Visible := true;
  FTopPanel.Caption := '';
  FTopPanel.Color := Self.Color;
  FTopPanel.BevelOuter := bvNone;
  FTopPanel.BorderWidth := 0;
  FTopPanel.BorderStyle := bsNone;
  FTopPanel.Ctl3D := False;
  FTopPanel.Align := alTop;
  FTopPanel.DoubleBuffered := true;
  FTopPanel.Height := Round(0.14 * Self.Height); // 150;
  FTopPanel.Font.Color := clBlack;
  FTopPanel.Font.Name := DefaultFontName;
  FTopPanel.Font.Height := Round(0.08 * Self.Height);
{$IFDEF INPUTSTR_IN_LISTWIN}
  CreateInputPanel(Self);
  // Self.Font.Height := Round(0.06 * Self.Height);
{$ENDIF}
  // ������ �������� � �������
  FFootPanel := TPanel.Create(Self);
  FFootPanel.ParentColor := False;
  FFootPanel.Parent := Self;
  FFootPanel.ParentBackground := False;
  FFootPanel.Visible := true;
  FFootPanel.Caption := '';
  FFootPanel.Color := Self.Color;
  FFootPanel.BevelOuter := bvNone;
  FFootPanel.BorderWidth := 0;
  FFootPanel.BorderStyle := bsNone;
  FFootPanel.Ctl3D := False;
  FFootPanel.Align := alBottom;
  FFootPanel.DoubleBuffered := true;
  FFootPanel.Height := Round(0.14 * Self.Height); // 150;

  FItemsScrollBar := TCustomElementList.Create
    (Self, clBlue, CT.Color['Main_BackGr']);
  FItemsScrollBar.Align := alClient;
  FItemsScrollBar.OnItemSelected := nil;

  SB := TScrollBar.Create(FItemsScrollBar); // Self
  SB.Parent := FItemsScrollBar;
  SB.Kind := sbVertical;
  SB.Align := alRight; // alClient;
  SB.Width := Round(Parent.Width * 0.06);
  // repaint;

  HeightListWin := // FFootPanel.Top - (FTopPanel.Top+FTopPanel.Height);
    Round((1 - 0.14 * 2) * Self.Height);
  SB.Min := 0;
  SB.Max := 100; // HeightListWin;//0;//100;//FFootPanel.Top - FTopPanel.Height-2;// - 100) + 100;
  SB.OnChange := ScrollChange;
  SB.PageSize := 10; // 80;//99;//ceITEMSONSCREEN;//99;
  SB.LargeChange := HeightListWin; // {/ceITEMSONSCREEN;//}FItemsScrollBar.Height;
  SB.SmallChange :=
  { 1;// } Round(HeightListWin / ceITEMSONSCREEN) + ceItemSpace;

  FButPanel := TControlPanelManager.Create(FFootPanel, FCT, FLT);
  FButPanel.Color := Self.Color;

  FButPanel.AddPanel(TButtonPanel.Create('List', poHorizontal, caLeft, 49
      { 45 } ));
  FBut := FButPanel.ButtonPanel['List'].AddButton(FLT.Caption['�����'], OnExit);
{$IFDEF INPUTSTR_IN_LISTWIN}
  FInputBut := FButPanel.ButtonPanel['List'].AddButton
    (FLT.Caption['Input'], OnInputBut);
  FInputBut.Visible := isENABLED_INPUT_STR; // True;//False;
  // CreateInputPanel(FTopPanel);
  // EnableInputStr:= isENABLED_INPUT_STR;
{$ELSE}
  FSecBut := FButPanel.ButtonPanel['List'].AddButton
    (FLT.Caption['������ ������'], OnSecondBut);
  FSecBut.Visible := False;
{$ENDIF}
  FItemsScrollBar.SetBackGround(FCT.Color['Main_BackGr']);

end;
{$IFDEF INPUTSTR_IN_LISTWIN}

procedure TListWindow.OnInputBut(Sender: TObject);
begin
  FInputBut.OnClick := OnEnterText;
  Edit1.Text := '';
  // FInputPanel.BringToFront;
  FInputPanel.Visible := true;
  FItemsScrollBar.Visible := False;
  Edit1.SetFocus;
  // FInputPanel.Visible := true;
  // FKeyb.ScaleBy(2,4);
  // FKeyb.Height:=round(0.3*Self.Height);
end;

procedure TListWindow.OnEnterText(Sender: TObject);
var
  s: string;
begin
  FInputBut.OnClick := OnInputBut;
  FInputPanel.Visible := False;
  FItemsScrollBar.Visible := true;
  // FInputPanel.SendToBack;
  // OnItemSelected(Edit1.Text);
  s := Trim(Edit1.Text);
  if s <> '' then
    if not FItemsScrollBar.ItemInList(s) then
      AddItem(s, s);
  // SetItemSelectEvent(Edit1.Text);
  SaveToFile(FileName);
  with FItemsScrollBar do
    if Assigned(OnItemSelected) then
      OnItemSelected(Edit1.Text);

end;

procedure TListWindow.SaveToFile(FileName: String);
var
  SList: TStringList;
  i: integer;
begin
  if FileName = '' then
    Exit;

  if Assigned(FItemsScrollBar) then
    FItemsScrollBar.SaveToFile(FileName, TEncoding.Unicode);
  { for i := 0 to FItemsScrollBar.FItemList.Count - 1 do begin
    SList.Add(FItemsScrollBar.FItemList.Items[i].Caption)
    end;
    SList.SaveToFile(FileName);
    }
end;

{ procedure TListWindow.DisableInputStr;
  begin
  //  FInputBut.OnClick:= OnEnterText;
  FInputBut.Visible:=False;
  FInputPanel.Visible:=False;
  end;
}
procedure TListWindow.SetEnaInputStr(Val: Boolean);
begin
  if Val then
  begin
    FInputBut.Visible := true;
    FInputPanel.Visible := true;
  end
  else
  begin
    FInputBut.Visible := False;
    FInputPanel.Visible := False;
  end;
end;
{$ENDIF}

procedure TListWindow.HideSecondButton;
begin
  FSecBut.Visible := False;
end;

procedure TListWindow.OnExit(Sender: TObject);
begin
{$IFDEF INPUTSTR_IN_LISTWIN}
  FInputBut.OnClick := OnInputBut;
  FItemsScrollBar.Visible := true;
{$ENDIF}
  General.OpenWindow(WinToExit);
end;

procedure TListWindow.OnSecondBut(Sender: TObject);
begin
  if Assigned(OnSecondButtonPress) then
    OnSecondButtonPress(Sender);
end;

procedure TListWindow.Open;
begin
  inherited;
  //
{$IFDEF INPUTSTR_IN_LISTWIN}
  FInputBut.OnClick := OnInputBut;
  FItemsScrollBar.Visible := true;
  // FItemsScrollBar.Resize;
  if Assigned(SB) then
    SB.Position := 0;
{$ENDIF}
end;

procedure TListWindow.Resize;
begin
  inherited;
  FFootPanel.Height := Round(0.14 * Self.Height); // 0.1
  FTopPanel.Height := Round(0.14 * Self.Height); // 0.1
  FTopPanel.Font.Height := Round(0.08 * Self.Height);
end;

procedure TListWindow.SetButtonCaption(Value: string);
begin
  FBut.Caption := Value;
end;

procedure TListWindow.SetCaption(Value: string);
begin
  FTopPanel.Caption := Value;
end;

procedure TListWindow.SetItemSelectEvent(Value: TListItemSelectedEvent);
begin
  FItemsScrollBar.OnItemSelected := Value;
end;

procedure TListWindow.SetSecondButtonCaption(Value: string);
begin
  FSecBut.Caption := Value;
end;

procedure TListWindow.SetSelectEnable(Value: Boolean);
begin
  FItemsScrollBar.SelectionEnable := Value;
end;

procedure TListWindow.ShowSecondButton;
begin
  FSecBut.Visible := true;
end;

{ TBScanWindow }

procedure TBScanWindow.ConfigChannelButtons;
var
  i: integer;
begin
  for i := 0 to FButList.Count - 1 do
    if not(FMode in [bmHand, bmNastrHand]) then
    begin
      TChanelButton(FButList.Items[i]).KanalType := rkContinuous;
      TChanelButton(FButList.Items[i]).Enabled := true;
    end
    else
    begin
      TChanelButton(FButList.Items[i]).KanalType := rkHand;
      if TChanelButton(FButList.Items[i]).KanalType <> rkHand then
        TChanelButton(FButList.Items[i]).Enabled := False;
    end;
end;

constructor TBScanWindow.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);
const
  ButHeight = 70;
  ButWidth = 0.15;
var
  Align: TAlign;
begin
  inherited;
  SetLength(ABItems, 1);
  FLastCoord := 0;
  SparkTime := GetTickCount;
  SparkTick := False;
  FBS := False;
  FBoltJointMode := False;
  FTimer.Interval := 40;
  FCurKanalNum := -1;
  FCurRail := -1;
  FModeChange := False;
  Self.DoubleBuffered := true;
  Self.ControlStyle := Self.ControlStyle - [csOpaque];
  FPrevSelectedBut := nil;
  FFirstShow := true;
  FButList := TList.Create;
  FChButHelpVisible := true;
  IsSoundOn := true;
  IsPaintButOn := true;

  HeadPanel := CreateHeadPanel();
  Obj.Add(HeadPanel);

  FootPanel := CreateFootPanel();
  Obj.Add(FootPanel);

  FLeftPan := CreateSidePanel(alLeft);
  Obj.Add(FLeftPan);

  FRightPan := CreateSidePanel(alRight);
  Obj.Add(FRightPan);
{$IFDEF RKS_U}
  CreateChannelButtonsRKS;
{$ELSE}
  CreateChannelButtons;
{$ENDIF}
  FMiddleWorkPan := CreateMiddlePanel();
  Obj.Add(FMiddleWorkPan);

  SetChButHelpVisible(False);

  FMaxAdjustPeriod := EncodeDate(2010, 2, 1) - EncodeDate(2010, 1, 1);

  FTimer.Enabled := true;
end;

function TBScanWindow.CreateStandartPanel: TPanel;
var
  Panel: TPanel;
begin
  Panel := TPanel.Create(nil);
  Panel.ParentColor := False;
  Panel.Parent := Self;
  Panel.ParentBackground := False;
  Panel.ParentDoubleBuffered := False;
  Panel.DoubleBuffered := true;
  Panel.Color := Self.Color;
  Panel.DoubleBuffered := true;

  Result := Panel;
end;

function TBScanWindow.CreateHeadPanel: TPanel;
var
  HeadPan: TPanel;
begin
  HeadPan := CreateStandartPanel();

  HeadPan.Align := alTop;
  HeadPan.Height := Round((Self.Height / 100) * BScanHeadPanelHeight);

  FTimeLabel := TPanel.Create(HeadPan);
  FTimeLabel.ParentColor := False;
  FTimeLabel.Parent := HeadPan;
  FTimeLabel.ParentBackground := False;
  FTimeLabel.BevelOuter := bvNone;
  FTimeLabel.Align := alLeft;
  FTimeLabel.Width := Round(Self.Width / 6);
  FTimeLabel.Alignment := taLeftJustify;
  FTimeLabel.Font.Name := DefaultFontName;
  FTimeLabel.Font.Color := FCT.Color['BootWin_ButtonText'];
  FTimeLabel.Font.Height := Round(Self.Height * 0.05);
  FTimeLabel.Visible := true;
  FTimeLabel.Color := Self.Color;
  FTimeLabel.Caption := '����� !';

  FBUIAkkLabel := TPanel.Create(HeadPan);
  FBUIAkkLabel.ParentColor := False;
  FBUIAkkLabel.Parent := HeadPan;
  FBUIAkkLabel.ParentBackground := False;
  FBUIAkkLabel.ParentDoubleBuffered := False;
  FBUIAkkLabel.DoubleBuffered := true;
  FBUIAkkLabel.BevelOuter := bvNone;
  FBUIAkkLabel.Align := alLeft;
  FBUIAkkLabel.Width := Round(Self.Width / 6);
  FBUIAkkLabel.Alignment := taLeftJustify;
  FBUIAkkLabel.Font.Name := DefaultFontName;
  FBUIAkkLabel.Font.Color := FCT.Color['BootWin_ButtonText'];
  FBUIAkkLabel.Font.Height := Round(Self.Height * 0.05);
  FBUIAkkLabel.Visible := true;
  FBUIAkkLabel.Color := Self.Color;

  FBUMAkkLabel := TPanel.Create(HeadPan);
  FBUMAkkLabel.ParentColor := False;
  FBUMAkkLabel.Parent := HeadPan;
  FBUMAkkLabel.ParentBackground := False;
  FBUMAkkLabel.ParentDoubleBuffered := False;
  FBUMAkkLabel.DoubleBuffered := true;
  FBUMAkkLabel.BevelOuter := bvNone;
  FBUMAkkLabel.Align := alLeft;
  FBUMAkkLabel.Width := Round(Self.Width / 6);
  FBUMAkkLabel.Alignment := taRightJustify;
  FBUMAkkLabel.Font.Name := DefaultFontName;
  FBUMAkkLabel.Font.Color := FCT.Color['BootWin_ButtonText'];
  FBUMAkkLabel.Font.Height := Round(Self.Height * 0.05);
  FBUMAkkLabel.Visible := true;
  FBUMAkkLabel.Color := Self.Color;

  BUIAkkIndicator := TIndicator.Create
    ('BUIAkkInd', FBUIAkkLabel, FLT.Caption['���'] + ':');
  BUIAkkIndicator.Width := FBUIAkkLabel.Width;
  BUIAkkIndicator.Style := isBattery;
  BUIAkkIndicator.Percent := 100;

  BUMAkkIndicator := TIndicator.Create
    ('BUMAkkInd', FBUMAkkLabel, FLT.Caption['���'] + ':');
  BUMAkkIndicator.Width := FBUMAkkLabel.Width;
  BUMAkkIndicator.Style := isBattery;
  BUMAkkIndicator.Percent := 100;

  FVelLabel := TPanel.Create(HeadPan);
  FVelLabel.ParentColor := False;
  FVelLabel.Parent := HeadPan;
  FVelLabel.ParentBackground := False;
  FVelLabel.BevelOuter := bvNone;
  FVelLabel.Align := alRight;
  FVelLabel.Width := Round(Self.Width / 6);
  FVelLabel.Alignment := taRightJustify;
  FVelLabel.Font.Name := DefaultFontName;
  FVelLabel.Font.Color := FCT.Color['BootWin_ButtonText'];
  FVelLabel.Font.Height := Round(Self.Height * 0.05);
  FVelLabel.Color := Self.Color;
  FVelLabel.Visible := true;
  FVelLabel.Caption := 'V=60 ��/�';

  FRegLabel := TPanel.Create(HeadPan);
  FRegLabel.ParentColor := False;
  FRegLabel.Parent := HeadPan;
  FRegLabel.ParentBackground := False;
  FRegLabel.BevelOuter := bvNone;
  FRegLabel.Align := alClient;
  FRegLabel.Alignment := taCenter;
  FRegLabel.Font.Name := DefaultFontName;
  FRegLabel.Font.Color := FCT.Color['BootWin_ButtonText'];
  FRegLabel.Font.Height := Round(Self.Height * 0.04);
  FRegLabel.Color := Self.Color;
  FRegLabel.Visible := true;
  FRegLabel.Caption := FLT.Caption['�����������'];

  Result := HeadPan;
end;

function TBScanWindow.CreateFootPanel: TPanel;
var
  FootPan: TPanel;
begin
  FootPan := CreateStandartPanel();

  FootPan.Align := alBottom;
  FootPan.Height := Round((Self.Height / 100) * BScanFootPanelHeight);

  ButPanelManager := TControlPanelManager.Create(FootPan, FCT, FLT);
  ButPanelManager.Color := Self.Color;

  ButPanelManager.AddPanel(TButtonPanel.Create('BScan', poHorizontal,
      caWholeSize, 12));

  ButPanelManager.ButtonPanel['BScan'].AddButton(FLT.Caption['����'], OnExit);
  ButPanelManager.ButtonPanel['BScan'].AddButton(FLT.Caption['Search_B'],
    OnSearchBButPress);
  ButPanelManager.ButtonPanel['BScan'].AddButton('Hand', FLT.Caption['������'],
    OnHandScan);
  ButPanelManager.ButtonPanel['BScan'].AddButton('Marks', FLT.Caption['Marks'],
    OnPlaceMarkButPress);
  ButPanelManager.ButtonPanel['BScan'].AddButton
    ('RailType', FLT.Caption['RailType'], OnAdjustRailTypePress);
  ThresholdBut := ButPanelManager.ButtonPanel['BScan'].AddButton
    ('Threshold', '--', OnThresholdButPress);
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
  ButPanelManager.ButtonPanel['BScan'].AddButton
    ('PaintSprayerBtn', FLT.Caption['Paint Off'], OnPaintSprayer);
  ButPanelManager.ButtonPanel['BScan'].AddButton
    ('soundASD', FLT.Caption['Sound Off'], OnSound);
{$ELSE}
  ButPanelManager.ButtonPanel['BScan'].AddButton
    ('PauseBtn', FLT.Caption['�����'], OnPause);
  ButPanelManager.ButtonPanel['BScan'].AddButton('2echo', FLT.Caption['2 ���'],
    OnBoltStik);
  TButton(ButPanelManager.ButtonPanel['BScan'].Item['2echo']).Enabled := False;
{$IFEND}
  //
  (* </Rud30> *)
{$IFNDEF NoHelp}
  ButPanelManager.ButtonPanel['BScan'].AddButton
    (FLT.Caption['������'], OnShowHelp);
{$ENDIF}
  ButPanelManager.SwitchPanel('BScan');

  if ProgramProfile.DeviceFamily = 'RKS-U' then
    with ButPanelManager.ButtonPanel['BScan'] do
    begin
      Item['Hand'].Visible := False;
      Item['Marks'].Visible := False;
      Item['RailType'].Visible := False;
      // Item['Threshold'].Visible := false;
      Item['PauseBtn'].Visible := False;
      Item['2echo'].Visible := False;
    end;

  Result := FootPan;
end;

function TBScanWindow.CreateSidePanel(Align: TAlign): TPanel;
var
  SidePanel: TPanel;
begin
  WorkH := Self.Height - FootPanel.Height - HeadPanel.Height;

  SidePanel := CreateStandartPanel();
  SidePanel.Align := Align;

  SidePanel.Width := BScanChPanelWidth;
  SidePanel.Height := WorkH;
  SidePanel.BevelOuter := bvNone;

  Result := SidePanel;
end;

function TBScanWindow.CreateMiddlePanel: TPanel;
var
  MiddlePanel: TPanel;
begin
  MiddlePanel := CreateStandartPanel();
  MiddlePanel.Align := alClient;
  MiddlePanel.BevelOuter := bvNone;

  FBScan := TBScan.Create(MiddlePanel);
  //
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US) }
  FBScan.HasSecondBottom := true;
{$ELSE}
  if Config.SoundedScheme = Wheels then
    FBScan.HasSecondBottom := true
  else
    FBScan.HasSecondBottom := False;
{$IFEND}
  //
  if Assigned(General.Core) then
    FBScan.Core := General.Core;
  FBScan.PixelFormat := General.PixelFormat;
  FBScan.Parent := MiddlePanel;
  FBScan.Scale := FBScanDefaultScale;
  FBScan.Colors := FCT;
  FBScan.Language := FLT;
  FBScan.Align := alClient;
  FBScan.Height := MiddlePanel.Height;
  FBScan.Visible := False;

  FBScan.ConfigureLines(WorkH, TChanelButton(FButList.Items[0]).Height);
  FBScan.Visible := true;

  ThresholdBut.Caption := IntToStr(FBScan.Threshold) + ' ' + FLT['��'];

  FAScanFrame := TAScanFrame.Create(MiddlePanel, FCT, FLT);
  GAScanFrame := FAScanFrame;
  FAScanFrame.OnKanalAdjusted := KanalAdjusted;
  FAScanFrame.OnEnableHandKanals := EnableHandKanals;
  FAScanFrame.Visible := true;
{$IFDEF SimpleMnemo}
{$IFDEF Avikon16_IPV}
  FMenemoFrame := TCustomSimpleMnemoFrame.Create(MiddlePanel, FCT, FLT);
{$ENDIF}
  //
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
  FMenemoFrame := TCustomSimpleMnemoFrame.Create(MiddlePanel, FCT, FLT);
{$IFEND}
  //
{$IFDEF USK004R}
  FMenemoFrame := TCustomSimpleMnemoFrame.Create(MiddlePanel, FCT, FLT);
{$ENDIF}
{$IFDEF Avikon14}
  FMenemoFrame := TCustomSimpleMnemoFrame.Create(MiddlePanel, FCT, FLT);
{$ENDIF}
{$IFDEF Avikon11}
  FMenemoFrame := TAv11Mnemo.Create(MiddlePanel, FCT, FLT);
{$ENDIF}
{$ELSE}
  FMenemoFrame := TMnemoFrame.Create(MiddlePanel, FCT, FLT);
{$ENDIF}
  FMenemoFrame.Visible := true;

  Result := MiddlePanel;
end;

procedure TBScanWindow.CreateChannelButtons;
var
  KanBut: TChanelButton;
  ButPan: TPanel;
  ButKind: TChButKind;
  Kan1, Kan2, HKan: TKanal;
  i, j, j1, t, c, ButCountOnThread: integer;
{$IF DEFINED(Avikon16_IPV)}
  ChButPos: array [0 .. 7, 1 .. 2] of integer;
{$IFEND}
{$IFDEF EGO_USW}
  ChButPos: array [0 .. 9, 1 .. 2] of integer;
  HChButPos: array [1 .. 2, 0 .. 9] of integer;
{$ENDIF}
{$IFDEF VMT_US}
  ChButPos: array [0 .. 11, 1 .. 2] of integer;
{$ENDIF}
  LeftChannelsSet, RightChannelsSet: set of Byte;
begin
{$IFNDEF RKS_U}
  LeftChannelsSet := [0, 2, 4, 6];
  RightChannelsSet := [1, 3, 5, 7];
  // ������ ��������� ��������
  ButCountOnThread := 4;
  // {$Define TwoThreadsDevice}
{$IFDEF TwoThreadsDevice}
  t := 1;
  FCoordLineH := 50;
{$ELSE}
  t := 0;
  FCoordLineH := 40;
{$ENDIF}
{$IFDEF Avikon15RSP}
  c := 5;
  FCoordLineH := 100;
  ButCountOnThread := 3;
{$ELSE}
  c := 7;
{$ENDIF}
{$IFDEF Avikon12}
  FCoordLineH := 70;
{$ENDIF}
{$IFDEF USK004R}
  FCoordLineH := 70;
  c := 9;
  ButCountOnThread := 5;
{$ENDIF}
{$IFDEF Avikon16_IPV}
  if (Config.SoundedScheme = Scheme1) or (Config.SoundedScheme = Scheme2) then
  begin
    for i := 0 to 7 do
      for j := 1 to 2 do
        ChButPos[i, j] := ChButPosS[i, j];
  end
  else if Config.SoundedScheme = Scheme3 then
  begin
    for i := 0 to 7 do
      for j := 1 to 2 do
        ChButPos[i, j] := ChButPosS3[i, j];
  end
  else if Config.SoundedScheme = Wheels then
  begin
    for i := 0 to 7 do
      for j := 1 to 2 do
        ChButPos[i, j] := ChButPosW[i, j];
    LeftChannelsSet := [10, 2, 4, 6];
    RightChannelsSet := [0, 3, 5, 7];
  end;
{$ENDIF}
{$IFDEF EGO_USW}
  LeftChannelsSet := [0, 2, 12, 4, 6];
  RightChannelsSet := [10, 3, 13, 5, 7];
  c := 9;
  ButCountOnThread := 5;
  if (Config.SoundedScheme = Scheme1) then
  begin
    for i := 0 to 9 do
      for j := 1 to 2 do
      begin
        ChButPos[i, j] := ChButPosW[i, j];
        HChButPos[j, i] := HChButPos1[j, i];
      end;
  end
  else if (Config.SoundedScheme = Scheme2) then
  begin
    for i := 0 to 9 do
      for j := 1 to 2 do
      begin
        ChButPos[i, j] := ChButPosW2[i, j];
        HChButPos[j, i] := HChButPos1[j, i];
      end;
  end
  else if (Config.SoundedScheme = Scheme3) then
  begin
    for i := 0 to 9 do
      for j := 1 to 2 do
      begin
        ChButPos[i, j] := ChButPosW3[i, j];
        HChButPos[j, i] := HChButPos3[j, i];
      end;
    LeftChannelsSet := [0, 2, 4, 6, 14];
    RightChannelsSet := [10, 3, 5, 7, 15];
    // c:=9;
  end;
{$ENDIF}
  //
{$IFDEF VMT_US}
  LeftChannelsSet := [0, 6, 2, 12, 4, 10];
  RightChannelsSet := [1, 7, 3, 13, 5, 11];
  c := 11;
  ButCountOnThread := 6;
  for i := 0 to 11 do
    for j := 1 to 2 do
    begin
      ChButPos[i, j] := ChButPosW[i, j];
    end;
{$ENDIF}
  //
  for j := 0 to t do
  begin
{$IFDEF EGO_USW}
    if j = 1 then
      if (Config.SoundedScheme = Scheme1) then
      begin
        for i := 0 to 9 do
          for j1 := 1 to 2 do
            ChButPos[i, j1] := ChButPosWR[i, j1];
      end
      else if (Config.SoundedScheme = Scheme2) then
      begin
        for i := 0 to 9 do
          for j1 := 1 to 2 do
            ChButPos[i, j1] := ChButPosW2R[i, j1];
      end;
{$ENDIF}
    for i := 0 to c do
    begin
      if j = 0 then
        Align := alTop
      else
        Align := alBottom;
      if ChButPos[i, 1] in LeftChannelsSet then
      begin
        ButPan := FLeftPan;
        ButKind := bkLeft;
      end
      else if ChButPos[i, 1] in RightChannelsSet then
      begin
        ButPan := FRightPan;
        ButKind := bkRight;
      end

      else if ChButPos[i, 1] mod 2 = 0 then
      begin
        ButPan := FLeftPan;
        ButKind := bkLeft;
      end
      else
      begin
        ButPan := FRightPan;
        ButKind := bkRight;
      end;

      Kan1 := nil;
      Kan2 := nil;
      HKan := nil;
      if ((Assigned(General.Core)) and (ChButPos[i, 1] > -1)) then
        Kan1 := General.Core.Kanal[j, ChButPos[i, 1]];
      if ChButPos[i, 2] <> -1 then
        Kan2 := General.Core.Kanal[j, ChButPos[i, 2]];

      if HChButPos[j + 1, i] <> -1 then
        HKan := HKanals[HChButPos[j + 1, i]];

      KanBut := TChanelButton.Create(ButPan, Kan1, Kan2, HKan, FCT, FLT);

      KanBut.Parent := ButPan;
      KanBut.KanalType := rkContinuous;
      KanBut.OnClick := OnChButPress;
      KanBut.Align := Align;
      KanBut.Height := Round
        ((WorkH - FCoordLineH) / (ButCountOnThread * (t + 1)));
      KanBut.Top := Parent.Height - 1;
      KanBut.Ctl3D := False;
      KanBut.Kind := ButKind;
      KanBut.Visible := False;
      KanBut.ShowHelp := true;
{$IFDEF USK004R}
      // KanBut.Font.Height:=  //Size
{$ENDIF}
      if (not Assigned(Kan1)) and (not Assigned(Kan2)) then
        KanBut.Lock;

      FButList.Add(KanBut);
    end;
  end;
  FLeftPan.Width := KanBut.CountWidth(KanBut.Height);
  FRightPan.Width := KanBut.CountWidth(KanBut.Height);

  CreateZoomButtons;
{$ENDIF}
  Align := alBottom; // AK 13-08-2015!!!

end;

procedure TBScanWindow.CreateChannelButtonsRKS;
var
  KanBut: TChanelButton;
  ButPan: TPanel;
  ButKind: TChButKind;
  Kan1, Kan2, HKan: TKanal;
  i, j, t, c, ButCountOnThread: integer;
begin
  // {$IFnDEF Avikon16_IPV}
{$IF NOT DEFINED(Avikon16_IPV) AND NOT DEFINED(EGO_USW) AND NOT DEFINED(USK004R) AND NOT DEFINED(VMT_US)}
  // ������ ��������� ��������
  if Config.SoundedScheme = Scheme1 then
    ButCountOnThread := 5
  else if Config.SoundedScheme = Scheme2 then
    ButCountOnThread := 8
  else
    Exit;

  FCoordLineH := 40;
  j := 0;
  for i := 0 to ButCountOnThread - 1 do
  begin
    Align := alBottom;

    ButPan := FLeftPan;
    ButKind := bkLeft;

    Kan1 := nil;
    Kan2 := nil;
    HKan := nil;
    if ((Assigned(General.Core)) and (ChButPos[i] > -1)) then
      Kan1 := General.Core.Kanal[j, ChButPos[i]];

    if HChButPos[i] <> -1 then
      HKan := HKanals[HChButPos[i]];

    KanBut := TChanelButton.Create(ButPan, Kan1, Kan2, HKan, FCT, FLT);

    KanBut.Parent := ButPan;
    KanBut.KanalType := rkContinuous;
    KanBut.OnClick := OnChButPress;
    KanBut.Align := Align;
    KanBut.Height := Round((WorkH - FCoordLineH) / (ButCountOnThread));
    { Round(WorkH / ButCountOnThread ); }

    KanBut.Top := Parent.Height - 1;
    KanBut.Ctl3D := False;
    KanBut.Kind := ButKind;
    KanBut.Visible := False;
    KanBut.ShowHelp := true;

    if (not Assigned(Kan1)) and (not Assigned(Kan2)) then
      KanBut.Lock;

    FButList.Add(KanBut);
  end;

  FLeftPan.Width := KanBut.CountWidth(KanBut.Height);
  FRightPan.Width := 0;

  CreateZoomButtons;
{$IFEND}
end;

procedure TBScanWindow.CreateZoomButtons;
var
  t: integer;
begin
{$IFDEF TwoThreadsDevice}
  t := 1;
{$ELSE}
  t := 0;
{$ENDIF}
  FScaleButDec := TButton.Create(FLeftPan);
  FScaleButDec.Parent := FLeftPan;
  FScaleButDec.Align := alClient;
  FScaleButDec.Font.Name := DefaultFontName;
  FScaleButDec.Font.Color := FCT.Color['BootWin_ButtonText'];
  FScaleButDec.Font.Height := Round(Self.Height * (0.1 / (t + 1)));
  FScaleButDec.Caption := '-';
  FScaleButDec.Visible := true;
  FScaleButDec.OnClick := OnScaleMinusPress;

  if ProgramProfile.DeviceFamily <> 'RKS-U' then
  begin
    FScaleButInc := TButton.Create(FRightPan);
    FScaleButInc.Parent := FRightPan;
    FScaleButInc.Align := alClient;
  end
  else
  begin
    FScaleButInc := TButton.Create(FLeftPan);
    FScaleButInc.Parent := FLeftPan;
    FScaleButInc.Align := alRight;
    // FScaleButInc.Width :=  FLeftPan.Width div 2;
  end;

  FScaleButInc.Font.Name := DefaultFontName;
  FScaleButInc.Font.Color := FCT.Color['BootWin_ButtonText'];
  FScaleButInc.Font.Height := Round(Self.Height * (0.1 / (t + 1)));
  FScaleButInc.Caption := '+';
  FScaleButInc.Visible := true;
  FScaleButInc.OnClick := OnScalePlusPress;
end;

destructor TBScanWindow.Destroy;
begin
  FTimer.Enabled := False;
  ButPanelManager.Free;

  FButList.Free;
  SetLength(ABItems, 0);
  inherited;
end;

procedure TBScanWindow.EnableHandKanals(KanalsState: Boolean);
var
  i: integer;
  B: TButton;
begin
  if not KanalsState then
  begin
    for i := 0 to FButList.Count - 1 do
      TChanelButton(FButList.Items[i]).Enabled := False;
    ButPanelManager.ButtonPanel['BScan'].DisableButtons;

  end
  else
  begin
    ButPanelManager.ButtonPanel['BScan'].EnableButtons;
    B := TButton(ButPanelManager.ButtonPanel['BScan'].Item['Threshold']);
    if Assigned(B) then
      B.Enabled := not(FMode in [bmHand, bmNastrHand]);
    if General.Core.RegOnUserLevel then
    begin
{$IF NOT DEFINED(EGO_USW) AND NOT DEFINED(VMT_US) }
      ButPanelManager.ButtonPanel['BScan'].Item['PauseBtn'].Enabled := true;
{$IFEND}
      ButPanelManager.ButtonPanel['BScan'].Item['Marks'].Enabled := true;
    end
    else
    begin
{$IF NOT DEFINED(EGO_USW) AND NOT DEFINED(VMT_US) }
      ButPanelManager.ButtonPanel['BScan'].Item['PauseBtn'].Enabled := False;
{$IFEND}
      ButPanelManager.ButtonPanel['BScan'].Item['Marks'].Enabled := False;
    end;

    for i := 0 to FButList.Count - 1 do
      if FMode in [bmHand, bmNastrHand] then
      begin
        TChanelButton(FButList.Items[i]).KanalType := rkHand;
        if TChanelButton(FButList.Items[i]).KanalType <> rkHand then
          TChanelButton(FButList.Items[i]).Enabled := False
        else
          TChanelButton(FButList.Items[i]).Enabled := true
      end
      else
        TChanelButton(FButList.Items[i]).Enabled := true;
  end;
end;

function TBScanWindow.IsChAdjusted(Kanal: TKanal): Boolean;
var
  CurDateTime: TDateTime;
begin
  Result := true;
  if (Kanal.ATT < 15) or (Kanal.ATT > 65) then
    Result := False;
  if (Kanal.Ky < 10) or (Kanal.Ky > 35) then
    Result := False;
  CurDateTime := Now;
  if ((CurDateTime - Kanal.AdjDate) > FMaxAdjustPeriod) then
    Result := False;
end;

procedure TBScanWindow.KanalAdjusted(Rail, Num: integer);
var
  i: integer;
  But: TChanelButton;
begin
  for i := 0 to FButList.Count - 1 do
  begin
    But := TChanelButton(FButList.Items[i]);
    if Assigned(But) and (But.Enabled) then
      if (But.Kanal.Nit = Rail) and (But.Kanal.Num = Num) then
        TChanelButton(FButList.Items[i]).Adjusted := true;
  end;
end;

procedure TBScanWindow.OnAdjustRailTypePress(Sender: TObject);
begin
  if (GetTickCount - FButPressedTime) < ButtonPressTimeOut then
    Exit;

  FButPressedTime := GetTickCount;
  General.Core.AdjustRailType;
  FAScanFrame.UpDateKanalParams;
  (* <Rud35> *)
{$IFDEF UseHardware}
  FBScan.UpDateStrob;
  FBScan.Refresh(0, true, true);
{$ENDIF}
  (* </Rud35> *)
end;

procedure TBScanWindow.OnBoltStik(Sender: TObject);
begin
  FBS := not FBS;
  if FBS then
  begin
    FootPanel.Color := clYellow;
    FTimeLabel.Color := clYellow;
    FVelLabel.Color := clYellow;
    FRegLabel.Color := clYellow;

  end
  else
  begin
    FootPanel.Color := HeadPanel.Color;
    FTimeLabel.Color := HeadPanel.Color;
    FVelLabel.Color := HeadPanel.Color;
    FRegLabel.Color := HeadPanel.Color;
  end;
{$IFDEF UseHardware}
  if Assigned(General.Core) then
    General.Core.BoltJointMode := FBS;
  (* <Rud30> *)
  // �������� �������� � ������� ������ � ����� ������
  if Assigned(FPrevSelectedBut) then
  begin
    FAScanFrame.AdjPanWork.Panels['1'].Button['StrSt'].Value :=
      FPrevSelectedBut.Kanal.Str_st;
    FAScanFrame.AdjPanWork.Panels['1'].Button['StrEd'].Value :=
      FPrevSelectedBut.Kanal.Str_en;
  end;
  (* </Rud30> *)
{$ENDIF}
end;

procedure TBScanWindow.OnCancelPause;
begin
  EnableHandKanals(true);
  FAScanFrame.EnableParamButtons;
  General.Core.PauseRegistration(False);
end;

procedure TBScanWindow.OnChButPress(Sender: TObject);
var
  But: TChanelButton;
  i: integer;
begin
  if Assigned(FPrevSelectedBut) then
    if FPrevSelectedBut <> Sender then
      FPrevSelectedBut.Selected := False;
  But := TChanelButton(Sender);
  But.Selected := true;
  FPrevSelectedBut := But;
  General.CurLogicalKanal := But.Kanal;
  if (FMode = bmSearch) or (FMode = bmMnemo) then
  begin
    Mode := bmEstimate;
    FModeChange := true;
  end;
  if FMode in [bmNastr, bmNastrHand] then
    KConf.SaveUserConfig(0);
  SetAScanKanal(But.Kanal);

  FAScanFrame.BScanClear;
end;

procedure TBScanWindow.OnExit(Sender: TObject);
begin
  FCurKanalNum := -1;
  FCurRail := -1;
  (* <Rud36> *)
  if FBS = true then
    OnBoltStik(nil);
  (* </Rud36> *)
  if Self.Mode in [bmNastr, bmNastrHand, bmEstimate, bmSearch] then
    KConf.SaveUserConfig(0);
  if Assigned(General.Core) then
    General.Core.ChangeModeInRegFile(rmMenu, 0, 0);
  General.OpenWindow('MAINMENU');
end;

procedure TBScanWindow.OnHandScan(Sender: TObject);
var
  i: integer;
  Kan: TKanal;
  c: integer;
  CAN: TCAN;
begin
  (* <Rud36> *)
  if FBS = true then
    OnBoltStik(nil);
  (* </Rud36> *)

  if (Self.Mode = bmHand) or (Self.Mode = bmNastrHand) then
    Exit;
  General.Core.PauseRegThread;
  Sleep(300);
  if (GetTickCount - FButPressedTime) < ButtonPressTimeOut then
    Exit;
  FButPressedTime := GetTickCount;
  if General.Mode in [bmNastr, bmNastrHand] then
  begin
    General.Mode := bmNastrHand;
    General.Core.SetMode(rHandNastr);
  end
  else
  begin
    General.Mode := bmHand;
    General.Core.SetMode(rHand);
    (* <Rud16> *)
    FCurKanalNum := -1;
    (* </Rud16> *)
  end;

  Kan := nil;
  if Assigned(FPrevSelectedBut) then
    if TChanelButton(FPrevSelectedBut).KanalType = rkHand then
    begin
      Kan := TChanelButton(FPrevSelectedBut).Kanal;
      TChanelButton(FPrevSelectedBut).Selected := true;
      FPrevSelectedBut := TChanelButton(FPrevSelectedBut);
    end;
  if not Assigned(Kan) then
    for i := 0 to FButList.Count - 1 do
      if TChanelButton(FButList.Items[i]).KanalType = rkHand then
      begin
        Kan := TChanelButton(FButList.Items[i]).Kanal;
        TChanelButton(FButList.Items[i]).Selected := true;
        FPrevSelectedBut := TChanelButton(FButList.Items[i]);
        break;
      end;
  // >>>AK
  { c := Length(HTakts);
    for i := 0 to c - 1 do
    begin
    CAN := General.Core.CANS[0];
    if (Assigned(CAN)) and (Assigned(HTakts[i])) then
    HTakts[i].CANObj := General.Core.CANS[0];
    end;
    � ���������� �������� �� ������� ���� ���
    }

  // <<<

  SetAScanKanal(Kan);
  FAScanFrame.BScanClear;
end;

(* <Rud50> *)
procedure TBScanWindow.OnPause(Sender: TObject);
var
  PauseMessage: string;
begin
  if General.Core.RegistrationStatus <> rsPause then
  begin

    EnableHandKanals(False);
    FAScanFrame.DisableParamButtons;
    ButPanelManager.ButtonPanel['BScan'].Item['PauseBtn'].Enabled := true;
    General.Core.PauseRegistration(true);
    PauseMessage := Format(FLT.Caption[
        '������� ����� �����. %s%s ������� �� ������ ��������� ��� ����������� ������'], [WCHAR(10), WCHAR(13)]);
    MessageForm.Display(PauseMessage, clRed, -1, OnCancelPause);
  end

  else if General.Core.RegistrationStatus = rsPause then
    OnCancelPause;

end;
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US) }

procedure TBScanWindow.OnPaintSprayer(Sender: TObject);
begin
  if IsPaintButOn then
  begin
    IsPaintButOn := False;
    // Off PaintSprayer
    General.Core.PaintSprayerOn := False;
    TButton(ButPanelManager.ButtonPanel['BScan'].Item['PaintSprayerBtn'])
      .Caption := 'Paint On';
    // General.Core.FDataContainer.AddAirBrushTempOff(True, General.Core.Registrator.MaxDisCoord);//Off
  end
  else
  begin
    IsPaintButOn := true;
    // PaintSprayer State:=Config.PaintSprayer;
    General.Core.PaintSprayerOn := true;
    TButton(ButPanelManager.ButtonPanel['BScan'].Item['PaintSprayerBtn'])
      .Caption := 'Paint Off';
    // General.Core.FDataContainer.AddAirBrushTempOff(False, General.Core.Registrator.MaxDisCoord);//On
  end;
end;

procedure TBScanWindow.OnSound(Sender: TObject);
begin
  if IsSoundOn then
  begin
    IsSoundOn := False;
    General.Core.SndOn := False;
    TButton(ButPanelManager.ButtonPanel['BScan'].Item['soundASD']).Caption :=
      'Sound On';
  end
  else
  begin
    IsSoundOn := true;
    General.Core.SndOn := true;
    TButton(ButPanelManager.ButtonPanel['BScan'].Item['soundASD']).Caption :=
      'Sound Off';
  end;
end;
{$IFEND}

procedure TBScanWindow.OnPlaceMarkButPress(Sender: TObject);
begin
  if (GetTickCount - FButPressedTime) < ButtonPressTimeOut then
    Exit;
  FButPressedTime := GetTickCount;
  General.OpenWindow('PLACEMARK');
end;

procedure TBScanWindow.OnScaleMinusPress(Sender: TObject);
begin
  if (GetTickCount - FButPressedTime) < ButtonPressTimeOut then
    Exit;
  FButPressedTime := GetTickCount;
  FScaleButInc.Enabled := False;
  FScaleButDec.Enabled := False;
  if FMode = bmSearch then
  begin
    if Assigned(FBScan) then
    begin
      if FBScan.Scale > 1 then
      begin
        FBScan.Scale := FBScan.Scale - 1;
        Config.Scale := FBScan.Scale;
      end;
    end;
  end
  else if FMode in [bmEstimate, bmHand] then
  begin
    if FAScanFrame.BScanScale > 1 then
    begin
      FAScanFrame.BScanScale := FAScanFrame.BScanScale - 1;
      Config.Scale := FAScanFrame.BScanScale;
    end;
  end;
end;

procedure TBScanWindow.OnScalePlusPress(Sender: TObject);
begin
  if (GetTickCount - FButPressedTime) < ButtonPressTimeOut then
    Exit;
  FScaleButInc.Enabled := False;
  FScaleButDec.Enabled := False;
  FButPressedTime := GetTickCount;
  if FMode = bmSearch then
  begin
    if Assigned(FBScan) then
    begin
      if FBScan.Scale < 10 then
      begin
        FBScan.Scale := FBScan.Scale + 1;
        Config.Scale := FBScan.Scale;
      end;
    end;
  end
  else if FMode in [bmEstimate, bmHand] then
  begin
    if FAScanFrame.BScanScale < 10 then
    begin
      FAScanFrame.BScanScale := FAScanFrame.BScanScale + 1;
      Config.Scale := FAScanFrame.BScanScale;
    end;
  end;
end;

procedure TBScanWindow.OnSearchBButPress(Sender: TObject);
begin
  if (GetTickCount - FButPressedTime) < ButtonPressTimeOut then
    Exit;
  if (Self.Mode = bmHand) or (Self.Mode = bmNastrHand) then
    General.Core.PauseRegThread;
  FButPressedTime := GetTickCount;
  if Assigned(FPrevSelectedBut) then
    FPrevSelectedBut.Selected := False;
  if Assigned(General.Core) then
    General.Core.StopAScan;
  FCurKanalNum := -1;
  FCurRail := -1;
  General.Mode := bmSearch;
{$IFDEF UseHardware}
  FBScan.Refresh(0, true, true);
{$ENDIF}
end;

procedure TBScanWindow.OnSearchMButPress(Sender: TObject);
begin
  if (GetTickCount - FButPressedTime) < ButtonPressTimeOut then
    Exit;
  FButPressedTime := GetTickCount;
  if Assigned(FPrevSelectedBut) then
    FPrevSelectedBut.Selected := False;
  if Assigned(General.Core) then
    General.Core.StopAScan;
  General.Mode := bmMnemo;
end;

procedure TBScanWindow.OnShowHelp(Sender: TObject);
begin
  if (GetTickCount - FButPressedTime) < ButtonPressTimeOut then
    Exit;
  FButPressedTime := GetTickCount;
  SetChButHelpVisible(not FChButHelpVisible);
  Self.Open;
{$IFDEF UseHardware}
  if Assigned(FBScan) then
    FBScan.Refresh(0, true, true);
{$ENDIF}
end;

procedure TBScanWindow.OnTest(Sender: TObject);
begin
  ButPanelManager.SwitchPanel('Second');
end;

procedure TBScanWindow.OnTest2(Sender: TObject);
begin
  ButPanelManager.SwitchPanel('BScan');
end;

procedure TBScanWindow.OnThresholdButPress(Sender: TObject);
begin
  if Assigned(FBScan) then
  begin
    FBScan.IncThreshold;
    TButton(Sender).Caption := IntToStr(FBScan.Threshold) + ' ' + FLT['��'];
    FBScan.Refresh(0, true, true);
  end;
  if Assigned(Self.FAScanFrame) then
  begin
    Self.FAScanFrame.IncTreshold;
  end;

end;

procedure TBScanWindow.OnTimer(Sender: TObject);
var
  h, i: integer;
  T1, T2, t, s: integer;
  Hour, Min, Sec, MSec: Word;
  CColor: TColor;
  T3: integer;
  V: Single;
  RegStat: string;
  BStik, S1, S2: string;
  AK: Byte;
  BUMUakk: Single;
  a, B: integer;
  CAN: TCAN;
begin
  inherited;
  if Assigned(General.Core.Registrator) then
  begin
    if (General.Core.Registrator.MaxDisCoord - FLastCoord) >= 20 then
    begin
      // ����� AirBrush
      if Assigned(General.Core.FAirBrush) then
        General.Core.FAirBrush.Tick_;
      FLastCoord := General.Core.Registrator.MaxDisCoord;
    end;
  end;

  if (GetTickCount - SparkTime) > 50 then
  begin
    SparkTime := GetTickCount;
    SparkTick := not SparkTick;
  end;

  // ���� ������ �����
  FTimer.Enabled := False;
  if Assigned(General.Core) then
    if General.Core.CoordMark then
    begin
      General.Core.CoordMark := False;
      General.OpenWindow('PLACEMARK');
    end;
  FTimeLabel.Caption := TimeToStr(Now);

  if (General.Core.RegOnUserLevel) and (not General.Core.WaitingForBUM) then
  begin
    CColor := FCT.Color['AK_Good'];
    RegStat := FLT.Caption['RegOn'];
  end
  else
  begin
    CColor := FCT.Color['AK_BAD'];
    RegStat := FLT.Caption['RegOff'];
  end;
  if General.Core.RegistrationStatus = rsPause then
  begin
    CColor := FCT.Color['AK_Warning'];
    RegStat := FLT.Caption['RegPaused'];
  end;

  // FRegLabel.Color:=CColor;
  FRegLabel.Font.Color := CColor;
  if FBS then
  begin
    BStik := '(����� 2 ���)';
    FRegLabel.Caption := Format(FLT.Caption['�����������'] + ' %s %s',
      [RegStat, FLT.Caption[BStik]]);
  end
  else
    FRegLabel.Caption := Format(FLT.Caption['�����������'] + ' %s', [RegStat]);

  // ����� ��� ���
  AK := 100;
  if Assigned(General.PowerManager) then
  begin
    (* <Rud17> *)
    General.PowerManager.RefreshAPMStats;
    (* </Rud17> *)
    AK := General.PowerManager.BatteryLifePercent;
  end;

  if AK < 20 then
    BUIAkkIndicator.TextColor := FCT.Color['AK_BAD']
  else if ((AK >= 20) and (AK < 50)) then
    BUIAkkIndicator.TextColor := FCT.Color['AK_Warning']
  else
    BUIAkkIndicator.TextColor := clBlack;

  if AK = 255 then
    AK := 100;

  if AK < 50 then
    BUIAkkIndicator.Text := Format('%d %%', [AK]);

  BUIAkkIndicator.Percent := AK;

  // ���������� ��� ���
  BUMUakk := General.Core.GetUAKK / 10;
  // BUMUakk:= 10.3;
  if BUMUakk < 10.5 then
    BUMAkkIndicator.TextColor := FCT.Color['AK_BAD']
  else if ((BUMUakk >= 10.5) and (BUMUakk <= 11)) then
    BUMAkkIndicator.TextColor := FCT.Color['AK_Warning']
  else
    BUMAkkIndicator.TextColor := clBlack;

  // ������ 11 - �������
  // �� 11 �� 10,5 - ������. Max ����� � ������ ���� ~ 1,5 ����
  // ���� 10,5 - �������. Max ����� � ������� ���� ~ 0,5 ����
  if BUMUakk > 0.5 then
    BUMAkkIndicator.Text := Format('%3.1f %s', [BUMUakk, FLT.Caption['�']])
  else
    BUMAkkIndicator.Text := '';
  if BUMUakk < 0.5 then
  begin
    (* <Rud48> *)
    BUMAkkIndicator.Visible := False;
    (* </Rud48> *)
    BUMAkkIndicator.TextColor := clBlack;
    BUMAkkIndicator.Percent := 100;
  end
  else
  begin
    (* <Rud48> *)
    BUMAkkIndicator.Visible := true;
    (* </Rud48> *)
    if BUMUakk > 13 then
      BUMAkkIndicator.Percent := 100
    else
      BUMAkkIndicator.Percent := Round(((BUMUakk - 11) * 50 / 2) + 50);
    if (BUMUakk >= 10.5) and (BUMUakk < 11) then
      BUMAkkIndicator.Percent := Round(((BUMUakk - 10.5) * 30 / 0.5) + 20);
    if BUMUakk < 10.5 then
      BUMAkkIndicator.Percent := 10;
  end;
{$IFDEF TestSensor}
  case General.Core.SensorStateBum1 of
    - 1:
      S1 := '��� ������';
    1:
      S1 := '������';
    0:
      S1 := '�����';
  end;
  case General.Core.SensorStateBum2 of
    - 1:
      S2 := '��� ������';
    1:
      S2 := '������';
    0:
      S2 := '�����';
  end;
  FRegLabel.Caption := Format('���. ���: %s ����. ���: %s', [S1, S2]);
{$ENDIF}
  if General.Core.RegistrationStatus = rsOn then
  begin
    T3 := GetTickCount;
    if (T3 - FTVel) >= 1000 then
    begin
      s := General.Core.Registrator.CurLoadDisCoord[1] - FS;
      V := General.Core.GetVelocity;
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US) }
      FVelLabel.Font.Color := clBlack;
{$ELSE}
      if V >= 5 then
      begin
        if SparkTick then
          FVelLabel.Font.Color := clRed
        else
          FVelLabel.Font.Color := clSkyBlue;
      end
      else
      begin
        FVelLabel.Font.Color := clBlack;
        FVelLabel.Visible := true;
      end;
{$IFEND}
      FS := General.Core.Registrator.CurLoadDisCoord[1];
      FVelLabel.Caption := Format('%3.1f ' + FLT.Caption['��/�'], [V]);
      FTVel := T3;

    end;
  end;

  if FFirstShow then
  begin
    FFirstShow := False;
    if Assigned(FButList) then
      if FButList.Count > 0 then
      begin
        h := TChanelButton(FButList.Items[0]).Height;
        for i := 0 to FBScan.LinesCount - 1 do
          FBScan.Lines[i].Height := h;
      end;
  end;
{$IFDEF UseHardware}
  if FMode = bmSearch then
  begin
    if FBoltJointMode <> General.Core.BoltJointMode then
    begin
      FBoltJointMode := General.Core.BoltJointMode;
      FBScan.ForceResize;
      FBScan.Refresh(0, true, true);
    end;
    T1 := GetTickCount;
    FTestLabel.Caption := 'Test';
    if Assigned(FBScan) then
      FBScan.Refresh(0, False, False);
    T2 := GetTickCount;
    t := T2 - T1;
    FTestLabel.Caption := IntToStr(t);
  end;

  for i := 0 to FButList.Count - 1 do
    TChanelButton(FButList.Items[i]).RefreshParams;
  if (FMode <> bmSearch) and (FMode <> bmMnemo) and (FMode <> bmOff) then
  begin
    FAScanFrame.Refresh;
  end;
  if (FMode = bmMnemo) then
  begin
    FMenemoFrame.Refresh;
{$IFDEF Avikon11}
    TAv11Mnemo(FMenemoFrame).TwoEcho := General.Core.BoltJointMode;
{$ENDIF}
  end;
{$ENDIF}
  FTimer.Enabled := true;
  if (GetTickCount - FButPressedTime) >= ButtonPressTimeOut then
  begin
    FScaleButInc.Enabled := true;
    FScaleButDec.Enabled := true;
  end;

end;

procedure TBScanWindow.Open;
var
  i: integer;
  KanalType: TKanType;
  B: TButton;
begin
  inherited;
  B := TButton(ButPanelManager.ButtonPanel['BScan'].Item['Marks']);
  if Assigned(B) then
    B.Enabled := General.Core.RegOnUserLevel;

  if Assigned(FButList) then
  begin
    if not(FMode in [bmHand, bmNastrHand]) then
      KanalType := rkContinuous
    else
      KanalType := rkHand;
    for i := 0 to FButList.Count - 1 do
      if TChanelButton(FButList.Items[i]).KanalType = KanalType then
        TChanelButton(FButList.Items[i]).Visible := true;
  end;
  FBScan.ForceResize;
  FBScan.Refresh(0, true, true);
end;

procedure TBScanWindow.Test(Nit, Kanal, Delay, Amp: integer);
begin
{$IFDEF SimpleMnemo}
  FMenemoFrame.TestKanal(Nit, Kanal, Delay, Amp);
{$ELSE}
  FMenemoFrame.Test(Nit, Kanal, Delay, Amp);
{$ENDIF}
end;

procedure TBScanWindow.Resize;
var
  h, i: integer;
begin
  inherited;
  FVelLabel.Left := FVelLabel.Parent.Width;

  BUIAkkIndicator.Width := FBUIAkkLabel.Width;
  BUIAkkIndicator.Height := FBUIAkkLabel.Height;
  BUIAkkIndicator.ObjWidth := Round(FBUIAkkLabel.Width * 0.4);
  BUIAkkIndicator.Percent := 100;

  BUMAkkIndicator.Width := FBUMAkkLabel.Width;
  BUMAkkIndicator.Height := FBUMAkkLabel.Height;
  BUMAkkIndicator.ObjWidth := Round(FBUMAkkLabel.Width * 0.4);
  BUMAkkIndicator.Percent := 100;

  if Assigned(FBScan) then
    FBScan.ForceResize;
end;

procedure TBScanWindow.SetAScanKanal(Kanal: TKanal);
var
  RegMode: TRegFileModes;
begin
  Config.TempAlfa := Kanal.Takt.Alfa;

  FModeChange := False;
  if (FCurKanalNum <> Kanal.Num) or (FCurRail <> Kanal.Nit) then
  begin
    if Assigned(General.Core) then
      General.Core.SetAScanKanal(Kanal.Nit, Kanal.Num);
    FAScanFrame.BringToFront;
    FAScanFrame.Kanal := Kanal;
    FAScanFrame.Refresh;
    FCurKanalNum := Kanal.Num;
    FCurRail := Kanal.Nit;
  end;

  if (FMode in [bmNastr, bmNastrHand]) then
    SND.SoundOneChannel(Kanal.Nit, Kanal.Num)
  else
    SND.SoundAllChannels;

  case FMode of
    bmNastr:
      RegMode := rmNastr;
    bmNastrHand:
      RegMode := rmNastrHand;
    bmHand:
      RegMode := rmHand;
    bmEstimate:
      RegMode := rmEvaluation;
  end;

  if Assigned(General.Core) then
    General.Core.ChangeModeInRegFile(RegMode, FCurRail, FCurKanalNum);
end;

procedure TBScanWindow.SetChButHelpVisible(Value: Boolean);
var
  i, w: integer;
begin
  if FChButHelpVisible <> Value then
  begin
    FChButHelpVisible := Value;
    for i := 0 to FButList.Count - 1 do
      TChanelButton(FButList.Items[i]).ShowHelp := FChButHelpVisible;
    w := TChanelButton(FButList.Items[0]).CountWidth
      (TChanelButton(FButList.Items[0]).Height);
    FLeftPan.Width := w;
    FRightPan.Width := w;

    if ProgramProfile.DeviceFamily = 'RKS-U' then
    begin
      FRightPan.Width := 0;
      FScaleButInc.Width := w div 2;
    end;
  end;
end;

procedure TBScanWindow.ReInitBView;
begin
  FLastCoord := 0;
end;

procedure TBScanWindow.SetWinMode(Value: TBScanWinMode);
var
  i: integer;
  B: TButton;
  But: TChanelButton;
begin
  // FBScan.ForceResize;
  General.CurLogicalMode := Value;
  //
{$IF NOT DEFINED(EGO_USW) AND NOT DEFINED(VMT_US) }
  if General.Core.RegOnUserLevel then
    ButPanelManager.ButtonPanel['BScan'].Item['PauseBtn'].Enabled := true
  else
    ButPanelManager.ButtonPanel['BScan'].Item['PauseBtn'].Enabled := False;
{$IFEND}
  //
  FBScan.BScanDirection := Config.ZondPosition;
  FBScan.PointSize := Config.BViewPointSize;
  FBScan.AmpByColor := Config.BViewAmpByColor;
  FBScan.AmplBySize := Config.BViewAmpBySize;
  FBScan.Scale := Config.Scale;
  if FMode <> Value then
  begin
    FMode := Value;
    General.Core.ZMEnabled := False;
    if General.Core.RegistrationStatus = rsOn then
      FS := General.Core.Registrator.CurLoadDisCoord[1];
    ConfigChannelButtons;
    for i := 0 to FButList.Count - 1 do
      TChanelButton(FButList.Items[i]).Mode := FMode;
{$IF NOT DEFINED(EGO_USW) AND DEFINED(VMT_US) }
    B := TButton(ButPanelManager.ButtonPanel['BScan'].Item['2echo']);
    if Assigned(B) then
      B.Enabled := (FMode in [bmSearch, bmEstimate, bmMnemo]);
{$IFEND}
    B := nil;

    B := TButton(ButPanelManager.ButtonPanel['BScan'].Item['Threshold']);
    if Assigned(B) then
      B.Enabled := not(FMode in [bmHand, bmNastrHand]);
    General.Core.StopHandRegistration;
    case FMode of
      bmSearch:
        begin
          if Assigned(General.Core) then
          begin
            if General.Core.Mode <> rScan then
              General.Core.SetMode(rScan);
            General.Core.ChangeModeInRegFile(rmSearchB, 0, 0);
          end;

          for i := 0 to FButList.Count - 1 do
          begin
            But := TChanelButton(FButList.Items[i]);
            But.Selected := False;
          end;
          FMode := bmSearch;
          FAScanFrame.Mode := FMode;
          FBScan.Visible := true;
{$IFDEF UseHardware}
          FAScanFrame.Stop;
          FBScan.Clear;
          FBScan.UpDateStrob;
          FBScan.Refresh(0, true, true);
          Self.Resize;

          SND.SoundAllChannels;
{$ENDIF}
          FScaleButDec.Enabled := true;
          FScaleButInc.Enabled := true;
          ButPanelManager.SwitchPanel('BScan');
          Self.FBScan.BringToFront;
          General.Core.UnPauseRegThread;
        end;
      bmMnemo:
        begin
          if Assigned(General.Core) then
          begin
            if General.Core.Mode <> rScan then
              General.Core.SetMode(rScan);
            General.Core.ChangeModeInRegFile(rmSearchM, 0, 0);
          end;
          if Assigned(FPrevSelectedBut) then
            FPrevSelectedBut.Selected := False;
          FMenemoFrame.Visible := true;
          FScaleButDec.Enabled := False;
          FScaleButInc.Enabled := False;
          FMenemoFrame.BringToFront;
          SND.SoundAllChannels;
          ButPanelManager.SwitchPanel('BScan');
        end;
      bmEstimate:
        begin
          FScaleButDec.Enabled := true;
          FScaleButInc.Enabled := true;
          FMode := bmEstimate;
          FAScanFrame.BScanDirection := Config.ZondPosition;
          FAScanFrame.PointSize := Config.BViewPointSize;
          FAScanFrame.AmpByColor := Config.BViewAmpByColor;
          FAScanFrame.AmplBySize := Config.BViewAmpBySize;
          FAScanFrame.BScanScale := Round(FBScan.Scale);
          FAScanFrame.Mode := FMode;
          SND.SoundAllChannels;
          ButPanelManager.SwitchPanel('BScan');
        end;
      bmNastr:
        begin
          if Assigned(General.Core) then
            General.Core.SetMode(rNastr);
          if Assigned(FPrevSelectedBut) then
            FPrevSelectedBut.Selected := False
          else
            FPrevSelectedBut := TChanelButton(FButList.Items[0]);
          (* <Rud27> *)
          if (ProgramProfile.DeviceFamily <> 'RKS-U') then
            FPrevSelectedBut := TChanelButton(FButList.Items[8]);
          (* </Rud27> *)
          FPrevSelectedBut.Selected := true;
          FScaleButDec.Enabled := False;
          FScaleButInc.Enabled := False;
          FMode := bmNastr;
          FCurKanalNum := -1;
          FCurRail := -1;
          FAScanFrame.Visible := true;
          FAScanFrame.Mode := FMode;
          FAScanFrame.BringToFront;
          SetAScanKanal(FPrevSelectedBut.Kanal);
          FScaleButDec.Enabled := False;
          FScaleButInc.Enabled := False;
          SND.SoundOneChannel(FPrevSelectedBut.Kanal.Nit,
            FPrevSelectedBut.Kanal.Num);
          ButPanelManager.SwitchPanel('Nastr');
          General.Core.PauseRegThread;
          for i := 0 to FButList.Count - 1 do
          begin
            But := TChanelButton(FButList.Items[i]);
            (* <Rud32> *)
            if (ProgramProfile.DeviceFamily = 'Avicon16') or
              (ProgramProfile.ProgramTitle = 'Filus X27W') then
              But.Adjusted := False
            else
              But.Adjusted := true;
            (* </Rud32> *)
          end;
        end;
      bmHand:
        begin
          ButPanelManager.SwitchPanel('Nastr');
          FAScanFrame.BScanDirection := Config.ZondPosition;
          FAScanFrame.PointSize := Config.BViewPointSize;
          FAScanFrame.AmpByColor := Config.BViewAmpByColor;
          FAScanFrame.AmplBySize := Config.BViewAmpBySize;
          FAScanFrame.Mode := FMode;
        end;
      bmNastrHand:
        begin
          ButPanelManager.SwitchPanel('Nastr');
          FAScanFrame.Mode := FMode;
          FScaleButDec.Enabled := False;
          FScaleButInc.Enabled := False;
          for i := 0 to FButList.Count - 1 do
          begin
            But := TChanelButton(FButList.Items[i]);
            (* <Rud32> *)
            if (ProgramProfile.DeviceFamily = 'Avicon16') or
              (ProgramProfile.ProgramTitle = 'Filus X27W') then
              But.Adjusted := False
            else
              But.Adjusted := true;
            (* </Rud32> *)
          end;
        end;
    end;
  end;
end;

{ TBScanRefresher }

constructor TBScanRefresher.Create;
begin
  inherited Create(true);
  BScan := nil;
end;

procedure TBScanRefresher.Execute;
begin
  try
    while not Terminated do
    begin
      if Assigned(BScan) then
        BScan.Refresh(0, False, False);
      Sleep(20);
    end;
  except
    on E: Exception do
      ProcessException.RaiseException('BScanRefresher', E);
  end;
end;

{ TMarksWindow }

constructor TMarksWindow.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);

var
  PanTitle: TPanel;
  Title, Foot: TLabel;
  But, LastBut: TButton;

begin
  inherited;
  PanTitle := TPanel.Create(Self);
  PanTitle.ParentColor := False;
  PanTitle.Parent := Self;
  PanTitle.ParentBackground := False;
  PanTitle.Color := Self.Color;
  PanTitle.Height := Round(Self.Height * PanTitleH);
  PanTitle.Align := alTop;
  Obj.Add(PanTitle);

  Title := TLabel.Create(PanTitle);
  Title.Parent := PanTitle;
  Title.Font.Color := FCT.Color['BootWin_ButtonText'];
  Title.Font.Name := DefaultFontName;
  Title.Font.Height := Round(Self.Height * TitleFontH);
  Title.Left := Round(Self.Width * TitleLeft);
  Title.Top := Round(Self.Height * TitleTop);
  Title.Caption := FLT.Caption['Mark_Title'];
  Title.Visible := true;

  Foot := TLabel.Create(Self);
  Foot.Parent := Self;
  Foot.Font.Color := FCT.Color['BootWin_TitleText'];
  Foot.Font.Name := DefaultFontName;
  Foot.Font.Height := Round(Self.Height * FootFontH);
  Foot.Height := Round(Self.Height * FootH); ;
  Foot.Align := alBottom;
  Foot.Alignment := taCenter;
  (* <Rud13> *)
  // Foot.Caption := FLT.Caption['BootWinFoot'];
  Foot.Caption := ''; // Config.Organization;//FLT.Caption['��� "�������������"'];
  (* </Rud13> *)
  Foot.Visible := true;
  Obj.Add(Foot);

  But := ControlFactory.CreateButton(Self);
  But.Parent := Self;
  But.Width := Round(Self.Width * ButWMark);
  But.Left := Round(Self.Width * ButLeft);
  But.Top := Round(Self.Height * ButTop);
  But.Height := Round(Self.Height * ButH);
  But.Font.Height := Round(Self.Height * ButFontH);
  But.Font.Name := DefaultFontName;
  But.Font.Color := FCT.Color['BootWin_ButtonText'];
  But.Caption := FLT.Caption['�����'];
  But.Visible := true;
  But.OnClick := OnPkMark;
  Obj.Add(But);

  LastBut := But;

  But := ControlFactory.CreateButton(Self);
  But.Parent := Self;
  But.Width := Round(Self.Width * ButWMark);
  But.Left := Round(Self.Width * ButLeft);
  But.Top := Round(LastBut.Top + LastBut.Height + LastBut.Height * ButTop);
  But.Height := Round(Self.Height * ButH);
  But.Font.Height := Round(Self.Height * ButFontH);
  But.Font.Name := DefaultFontName;
  But.Font.Color := FCT.Color['BootWin_ButtonText'];
  But.Caption := FLT.Caption['Def_Marks'];
  But.Visible := true;
  But.OnClick := OnDefMark;
  Obj.Add(But);

  LastBut := But;

  But := ControlFactory.CreateButton(Self);
  But.Parent := Self;
  But.Width := Round(Self.Width * ButWMark);
  But.Left := Round(Self.Width * ButLeft);
  But.Top := Round(LastBut.Top + LastBut.Height + LastBut.Height * ButTop);
  But.Height := Round(Self.Height * ButH);
  But.Font.Height := Round(Self.Height * ButFontH);
  But.Font.Name := DefaultFontName;
  But.Font.Color := FCT.Color['BootWin_ButtonText'];
  But.Caption := FLT.Caption['�������'];
  But.Visible := true;
  But.OnClick := OnSwitchNumber;
  Obj.Add(But);

  LastBut := But;

  But := ControlFactory.CreateButton(Self);
  But.Parent := Self;
  But.Width := Round(Self.Width * ButWMark);
  But.Left := Round(Self.Width * ButLeft);
  But.Top := Round(LastBut.Top + LastBut.Height + LastBut.Height * ButTop);
  But.Height := Round(Self.Height * ButH);
  But.Font.Height := Round(Self.Height * ButFontH);
  But.Font.Name := DefaultFontName;
  But.Font.Color := FCT.Color['BootWin_ButtonText'];
  But.Caption := FLT.Caption['Other_Marks'];
  But.Visible := true;
  But.OnClick := OnOtherMark;
  Obj.Add(But);

  LastBut := But;

  But := ControlFactory.CreateButton(Self);
  But.Parent := Self;
  But.Width := Round(Self.Width * ButWMark);
  But.Left := Round(Self.Width * ButLeft);
  But.Top := Round(Self.Height - Foot.Height - Self.Height * ButH - 1);
  But.Height := Round(Self.Height * ButH);
  But.Font.Height := Round(Self.Height * ButFontH);
  But.Font.Name := DefaultFontName;
  But.Font.Color := FCT.Color['BootWin_ButtonText'];
  But.Caption := FLT.Caption['Go_Back'];
  But.Visible := true;
  But.OnClick := OnGoBack;
  Obj.Add(But);

  FGP := TGroupBox.Create(Self);
  FGP.Parent := Self;
  FGP.Left := Round(Self.Width * (ButWMark + 0.02));
  FGP.Top := Round(Self.Height * (ButTop - 0.02));
  FGP.Width := Round(Self.Width - Self.Width * ButWMark - Self.Width * 0.03);
  FGP.Height := Round(Self.Height - Foot.Height - FGP.Top);
  FGP.Font.Height := Round(Self.Height * ButFontH);
  FGP.Font.Name := DefaultFontName;
  FGP.Font.Color := FCT.Color['BootWin_ButtonText'];
  FGP.Visible := true;
  Obj.Add(FGP);

  PanelManager := TControlPanelManager.Create(FGP, CT, LT);
  PanelManager.Color := FGP.Color;

  RePaint;

  ModePK := TMarkPK.Create(Self);
  ModeDefect := TMarkDefect.Create(Self);
  ModeOther := TMarkText.Create(Self);
  ModeSwitchNum := TMarkSwitchNumber.Create(Self);
  CMode := ModePK;

  But := ControlFactory.CreateButton(FGP);
  But.Parent := FGP;
  But.Width := Round(FGP.Width - Self.Width * LabelLeft);
  But.Left := Round(Self.Width * LabelLeft);
  But.Align := alBottom;
  But.Height := Round(Self.Height * ButH);
  But.Font.Height := Round(Self.Height * ButFontH);
  But.Font.Name := DefaultFontName;
  But.Font.Color := FCT.Color['BootWin_ButtonText'];
  But.Caption := FLT.Caption['Put_Mark'];
  But.Visible := true;
  But.OnClick := OnPlaceMark;

end;

procedure TMarksWindow.OnDefMark(Sender: TObject);
begin
  CMode := ModeDefect;
end;

procedure TMarksWindow.OnGoBack(Sender: TObject);
begin
  General.OpenWindow('BSCAN');
{$IFDEF USK004R}
  ModeDefect.Comments.Text := '';
{$ENDIF}
end;

procedure TMarksWindow.OnOtherMark(Sender: TObject);
begin
  CMode := ModeOther;
end;

procedure TMarksWindow.OnPkMark(Sender: TObject);
begin
  CMode := ModePK;
end;

procedure TMarksWindow.OnPlaceMark(Sender: TObject);
begin
  if (General.Core.RegistrationStatus = rsOn) or
    (General.Core.RegistrationStatus = rsPause) then
  begin
    TMarkMode(CMode).SetMark;
  end;
  OnGoBack(Sender);
end;

procedure TMarksWindow.OnSwitchNumber(Sender: TObject);
begin
  CMode := ModeSwitchNum;
end;

procedure TMarksWindow.Open;
var
  MRFC: TMRFCrd;
  CaC: TCaCrd;

begin
  inherited;
  if Config.UnitsSystem = usRus then
  begin
    MRFC := General.Core.Registrator.DisToMRFCrd
      (General.Core.Registrator.MaxDisCoord);
    ModePK.Initialize(MRFC);
    ModeDefect.Initialize(MRFC);
    ModeOther.Initialize(MRFC);
    ModeSwitchNum.Initialize(MRFC);
  end
  else
  begin
    CaC := General.Core.Registrator.DisToCaCrd
      (General.Core.Registrator.MaxDisCoord);
    ModePK.Initialize( { CaC } General.Core.Registrator.LastCaPost);
    ModeDefect.Initialize(CaC);
    ModeOther.Initialize(CaC);
    ModeSwitchNum.Initialize(CaC);
  end;
end;

{ TAdjustDPWindow }

constructor TAdjustDPWindow.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);
begin
  inherited;
  Self.FTimer.Interval := 10;
  Self.DoubleBuffered := true;
  // canStep:=1.7;
  FScanStep := Config.DPStep;
  FTopGP := TGroupBox.Create(nil);
  FTopGP.Parent := Self;
  FTopGP.DoubleBuffered := true;
  FTopGP.Align := alTop;
  FTopGP.Width := Self.Width;
  FTopGP.Height := Round(Self.Height / 2);
  FTopGP.Font.Height := Round(Self.Height * ButFontH);
  FTopGP.Font.Name := DefaultFontName;
  FTopGP.Font.Color := FCT.Color['BootWin_ButtonText'];
  FTopGP.Caption := FLT['��� ������� ����'];
  FTopGP.Visible := true;
  Obj.Add(FTopGP);

  FBottomGP := TGroupBox.Create(nil);
  FBottomGP.Parent := Self;
  FBottomGP.DoubleBuffered := true;
  FBottomGP.Align := alBottom;
  FBottomGP.Width := Self.Width;
  FBottomGP.Height := Round(Self.Height / 2);
  FBottomGP.Font.Height := Round(Self.Height * ButFontH);
  FBottomGP.Font.Name := DefaultFontName;
  FBottomGP.Font.Color := FCT.Color['BootWin_ButtonText'];
  FBottomGP.Caption := FLT['���������� ����'];
  FBottomGP.Visible := true;
  Obj.Add(FBottomGP);

  FDec := ControlFactory.CreateButton(FTopGP);
  FDec.Parent := FTopGP;
  FDec.Width := Round(Self.Width / 4);
  FDec.Height := Round(Self.Height / 5);
  FDec.Top := Round(FTopGP.Height - FDec.Height - FTopGP.Height * 0.06);
  FDec.Left := Self.Width - FDec.Width - 10;
  FDec.Font.Height := Round(Self.Height * ButFontH);
  FDec.Font.Name := DefaultFontName;
  FDec.Font.Color := FCT.Color['BootWin_ButtonText'];
  FDec.Caption := FLT['������'];
  FDec.Visible := true;
  FDec.OnClick := OnDec;
  FDec.OnMouseDown := OnDecDown;
  FDec.OnMouseUp := OnDecUp;

  FInc := ControlFactory.CreateButton(FTopGP);
  FInc.Parent := FTopGP;
  FInc.Width := Round(Self.Width / 4);
  FInc.Height := Round(Self.Height / 5);
  FInc.Top := Round(FTopGP.Height * 0.1);
  FInc.Left := Self.Width - FInc.Width - 10;
  FInc.Font.Height := Round(Self.Height * ButFontH);
  FInc.Font.Name := DefaultFontName;
  FInc.Font.Color := FCT.Color['BootWin_ButtonText'];
  FInc.Caption := FLT['������'];
  FInc.Visible := true;
  FInc.OnClick := OnInc;
  FInc.OnMouseDown := OnIncDown;
  FInc.OnMouseUp := OnIncUp;

  FStepLabel := TLabel.Create(FTopGP);
  FStepLabel.Parent := FTopGP;
  FStepLabel.Font.Height := Round(FTopGP.Height / 2.5);
  FStepLabel.Top := Round(FTopGP.Height / 2 - FStepLabel.Font.Height / 2);
  FStepLabel.Font.Name := DefaultFontName;
  FStepLabel.Font.Color := FCT.Color['BootWin_ButtonText'];
  FStepLabel.Caption := '?';
  FStepLabel.Left := Round((FTopGP.Width - FInc.Width) / 3 - FStepLabel.Width);
  FStepLabel.Visible := true;

  FExitBut := ControlFactory.CreateButton(FBottomGP);
  FExitBut.Parent := FBottomGP;
  FExitBut.Width := Round(Self.Width / 4);
  FExitBut.Height := Round(Self.Height / 5);
  FExitBut.Top := Round(FBottomGP.Height - FExitBut.Height - FBottomGP.Height *
      0.06);
  FExitBut.Left := Self.Width - FExitBut.Width - 10;
  FExitBut.Font.Height := Round(Self.Height * ButFontH);
  FExitBut.Font.Name := DefaultFontName;
  FExitBut.Font.Color := FCT.Color['BootWin_ButtonText'];
  FExitBut.Caption := FLT['�����'];
  FExitBut.Visible := true;
  FExitBut.OnClick := OnExit;

  FClearBut := ControlFactory.CreateButton(FBottomGP);
  FClearBut.Parent := FBottomGP;
  FClearBut.Width := Round(Self.Width / 4);
  FClearBut.Height := Round(Self.Height / 5);
  FClearBut.Top := Round(FBottomGP.Height * 0.1);
  FClearBut.Left := Self.Width - FExitBut.Width - 10;
  FClearBut.Font.Height := Round(Self.Height * ButFontH);
  FClearBut.Font.Name := DefaultFontName;
  FClearBut.Font.Color := FCT.Color['BootWin_ButtonText'];
  FClearBut.Caption := FLT['�����'];
  FClearBut.Visible := true;
  FClearBut.OnClick := OnClearWay;

  FWayLabel := TLabel.Create(FBottomGP);
  FWayLabel.Parent := FBottomGP;
  FWayLabel.Font.Height := Round(FBottomGP.Height / 2.5);
  FWayLabel.Top := Round(FBottomGP.Height / 2 - FWayLabel.Font.Height / 2);
  FWayLabel.Font.Name := DefaultFontName;
  FWayLabel.Font.Color := FCT.Color['BootWin_ButtonText'];
  FWayLabel.Caption := '?';
  FWayLabel.Left := Round((FBottomGP.Width - FClearBut.Width)
      / 3 - FWayLabel.Width);
  FWayLabel.Visible := true;

  Self.FTimer.Enabled := False;
end;

procedure TAdjustDPWindow.Hide;
begin
  inherited;
  General.Core.StartTempRegistration;
end;

procedure TAdjustDPWindow.OnClearWay(Sender: TObject);
begin
{$IFDEF UseHardware}
  if Assigned(General.Core.CANList[0]) then
    General.Core.CANList[0].ResetWayCounters;
{$ENDIF}
end;

procedure TAdjustDPWindow.OnDec(Sender: TObject);
begin
  if FScanStep > 0.02 then
    FScanStep := FScanStep - 0.01;
  Config.DPStep := FScanStep;
end;

procedure TAdjustDPWindow.OnDecDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  FWayDec := true;
  AvtoWayChangeTime := GetTickCount;
end;

procedure TAdjustDPWindow.OnDecUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  FWayDec := False;
end;

procedure TAdjustDPWindow.OnExit(Sender: TObject);
begin
  General.OpenWindow('MAINMENU');
end;

procedure TAdjustDPWindow.OnInc(Sender: TObject);
begin
  FScanStep := FScanStep + 0.01;
  Config.DPStep := FScanStep;
end;

procedure TAdjustDPWindow.OnIncDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  FWayInc := true;
  AvtoWayChangeTime := GetTickCount;
end;

procedure TAdjustDPWindow.OnIncUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  FWayInc := False;
end;

procedure TAdjustDPWindow.OnTimer(Sender: TObject);
var
  i, c: integer;
  BV: TBVBuff;
begin
  inherited;
  if (FWayInc) or (FWayDec) then
  begin
    if (GetTickCount - AvtoWayChangeTime > 500) and
      (GetTickCount - LastWayChange > 50) then
    begin
      LastWayChange := GetTickCount;
      if FWayInc then
        FScanStep := FScanStep + 0.01
      else if FWayDec then
        if FScanStep > 0.02 then
          FScanStep := FScanStep - 0.01;
      Config.DPStep := FScanStep;
    end;
  end;

  if Config.UnitsSystem in [usRus, usMetricTurkish] then
    FStepLabel.Caption := Format('%.2f ', [FScanStep]) + FLT['��']
  else
    FStepLabel.Caption := Format
      ('%s "', [TUnitsConverter.ConvertMMToStr(FScanStep)]);
{$IFDEF UseHardware}
  if Assigned(General.Core.CANList[0]) then
    if Config.UnitsSystem in [usRus, usMetricTurkish] then
    begin
      FWayLabel.Caption := Format
        ('%.3f ', [General.Core.CANList[0].SummaryWay * FScanStep / 1000])
        + FLT['�'];
    end
    else
    begin
      FWayLabel.Caption := TUnitsConverter.ConvertMMToStrWithFoots
        (General.Core.CANList[0].SummaryWay * FScanStep);
    end;
{$ENDIF}
end;

procedure TAdjustDPWindow.Open;
begin
  inherited;
  General.Core.StopRegistration;
end;

procedure TAdjustDPWindow.Resize;
begin
  inherited;
end;

{ TAdjTableWindow }

constructor TAdjTableWindow.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);
begin
  inherited;
  FExitBut := ControlFactory.CreateButton(Self);
  FExitBut.Parent := Self;
  FExitBut.Left := Round(Self.Width * ButLeft);
  FExitBut.Width := Round(Self.Width - FExitBut.Left * 2);
  FExitBut.Height := Round(Self.Height * ButH); ;
  FExitBut.Top := Round(Self.Height - FExitBut.Height - Self.Height * 0.02);
  FExitBut.Font.Height := Round(Self.Height * ButFontH);
  FExitBut.Font.Name := DefaultFontName;
  FExitBut.Font.Color := FCT.Color['BootWin_ButtonText'];
  FExitBut.Caption := FLT['�����'];
  FExitBut.OnClick := OnExit;
  FExitBut.Visible := true;

  FPB := TPaintBox.Create(Self);
  FPB.Parent := Self;
  FPB.Left := Round(Self.Width * ButLeft);
  FPB.Width := Round(Self.Width - FExitBut.Left * 2);
  FPB.Height := Round(Self.Height - FExitBut.Height - Self.Height * 0.06);
  FPB.Top := Round(Self.Height * 0.02);
  FPB.OnPaint := RePaint;
  FPB.Visible := true;

  FBmp := TBitMap.Create;
  FBmp.Width := FPB.Width;
  FBmp.Height := FPB.Height;
  FBmp.Canvas.Brush.Color := clWhite;
  FBmp.Canvas.FillRect(Rect(0, 0, FBmp.Width, FBmp.Height));
  FPB.RePaint;
end;

destructor TAdjTableWindow.Destroy;
begin
  if Assigned(FBmp) then
    FBmp.Free;
  inherited;
end;

procedure TAdjTableWindow.DrawTable;
var
  i, j, K, SectionCount, StrCount, StrH, FixedLeft, ChCount, ChW, TW, TX, TY,
    c, c1: integer;
  st: array [1 .. 6] of String;
  Val, Ch, Ch1, Ch2: string;
begin
  FBmp.Canvas.Brush.Color := clWhite;
  FBmp.Canvas.FillRect(Rect(0, 0, FBmp.Width, FBmp.Height));
  FBmp.Canvas.Pen.Color := clBlack;
  FBmp.Canvas.Pen.Width := 4;
  FBmp.Canvas.Rectangle(Rect(2, 2, FBmp.Width - 2, FBmp.Height - 2));

  FBmp.Canvas.Brush.Style := bsClear;

  StrCount := 7;
  StrH := Round(FBmp.Height / StrCount);
  FBmp.Canvas.Font.Height := Round((StrH / 3) * 0.95);
{$IFDEF Avikon14_2Block}
  FBmp.Canvas.Font.Height := Round((StrH / 3) * 0.9);
{$ENDIF}
  FBmp.Canvas.Font.Name := DefaultFontName;
  for i := 1 to StrCount do
  begin
    FBmp.Canvas.MoveTo(0, StrH * i);
    FBmp.Canvas.LineTo(FBmp.Width - 4, StrH * i);
  end;
  FixedLeft := Round(FBmp.Width * 0.08);

  FBmp.Canvas.MoveTo(FixedLeft, 0);
  FBmp.Canvas.LineTo(FixedLeft, FBmp.Height);
  FBmp.Canvas.TextOut(Round(FixedLeft * 0.25), StrH div 3, FLT.Caption['���.']);
{$IF DEFINED(Avikon16_IPV) OR DEFINED(EGO_USW) OR DEFINED(USK004R) OR DEFINED(VMT_US) }   // DF
  ChCount := 8;
{$ELSE}
  ChCount := Length(Kanals[0]);
{$IFEND}
  ChW := Round((FBmp.Width - FixedLeft) / ChCount);
  for i := 0 to ChCount - 1 do
  begin
{$IFDEF Avikon14_2Block}
    Ch := Format('%d� ', [Kanals[0, i].Takt.Alfa]);
    FBmp.Canvas.TextOut(Round(FixedLeft + ChW * i + FixedLeft * 0.1),
      StrH div 4, Ch);
    if i mod 2 = 0 then
      Ch1 := FLT.Caption['�.']
    else
      Ch1 := FLT.Caption['�.'];
    if (Kanals[0, i].Method = '���') or (Kanals[0, i].Method = 'ECHO') then
      Ch2 := FLT.Caption['E']
    else
      Ch2 := FLT.Caption['Z'];
    if not(i in [0, 1, 10, 11]) then
      Ch := Format('%s %s', [Ch1, Ch2])
    else
      Ch := Format('%s', [Ch2]);
    FBmp.Canvas.TextOut(Round(FixedLeft + ChW * i + FixedLeft * 0.1),
      StrH div 2, Ch);
{$ELSE}
    // DF
    Ch := Format('%d�  %s', [Kanals[0, i].Takt.Alfa,
      FLT.Caption[Kanals[0, i].Method]]);
    //
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US) }
    if Config.SoundedScheme = Wheels then
    begin
      if i = 0 then
        Ch := Format('%d�  %s%d', [Kanals[0, i].Takt.Alfa, FLT.Caption['��'], 1]
          );
      if i = 1 then
        Ch := Format('%d�  %s%d', [Kanals[0, i].Takt.Alfa, FLT.Caption['��'], 2]
          );
    end;
    FBmp.Canvas.TextOut(Round(FixedLeft + ChW * i + FixedLeft * 0.1), 0, Ch);

    if i mod 2 = 0 then
      Ch := FLT.Caption['�����.']
    else
      Ch := FLT.Caption['������.'];
{$ELSE}
    if i = 0 then
      Ch := Format('%d�  %s', [Kanals[0, i].Takt.Alfa, FLT.Caption['WPA']]);
    if i = 1 then
      Ch := Format('%d�  %s', [Kanals[0, i].Takt.Alfa, FLT.Caption['WPB']]);

    FBmp.Canvas.TextOut(Round(FixedLeft + ChW * i + FixedLeft * 0.1), 0, Ch);
    if i mod 2 = 0 then
      Ch := FLT.Caption['WPA']
    else
      Ch := FLT.Caption['WPB'];
{$IFEND}
    //
    if i in [0, 1] then
      Ch := '';
    //
{$IF NOT DEFINED(EGO_USW) AND NOT DEFINED(VMT_US) }
    if (Config.SoundedScheme = Scheme3) and (i in [5, 12]) then
      Ch := '';

    if (Config.SoundedScheme = Scheme2) and (i in [2, 10]) then
      Ch := '';

    if Config.SoundedScheme = Wheels then
{$IFEND}
    begin
      if i in [0, 1] then
        Ch := Format('%s/', [FLT.Caption['���']]);
    end;

    FBmp.Canvas.TextOut(Round(FixedLeft + ChW * i + FixedLeft * 0.1),
      StrH div 3, Ch);
    case i of // DF
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US) }
      0:
        Ch := FLT.Caption['���'];
      1:
        Ch := FLT.Caption['���'];
{$ELSE}
      0:
        if Config.SoundedScheme = Wheels then
          Ch := FLT.Caption['���']
        else
          Ch := ' ';
      1:
        if Config.SoundedScheme = Wheels then
          Ch := FLT.Caption['���']
        else
          Ch := ' ';
{$IFEND}
      2:
        Ch := { 'unw./w.'; } FLT.Caption['�����/���'];
      3:
        Ch := { 'unw./w.'; } FLT.Caption['�����/���'];
      4:
        Ch := '';
      5:
        if Config.SoundedScheme = Scheme3 then
          Ch := FLT.Caption['�����/���']
        else
          Ch := '';
      6:
        Ch := { 'web/base'; } FLT.Caption['���/���'];
      7:
        Ch := { 'web/base'; } FLT.Caption['���/���'];
    end;
    FBmp.Canvas.TextOut(Round(FixedLeft + ChW * i + FixedLeft * 0.1),
      2 * StrH div 3, Ch);
{$ENDIF}
  end;

  st[1] := FLT.Caption['���.'];
  st[1] := FLT.Caption['�������� ����������������, ��'];
  st[2] := FLT.Caption['����������, ��'];
  st[3] := FLT.Caption['�������� � ������, ���'];
  st[4] := FLT.Caption['���, ���'];
  st[5] := FLT.Caption['������ ������, ���'];
  st[6] := FLT.Caption['����� ������, ���'];

  for i := 1 to 6 do
  begin
    TW := FBmp.Canvas.TextWidth(st[i]);
    TX := Round((FBmp.Width - TW) / 2);
    TY := StrH * i;
    FBmp.Canvas.TextOut(TX, TY, st[i]);
{$IFDEF TwoThreadsDevice}
    FBmp.Canvas.TextOut(Round(FixedLeft * 0.1), TY + StrH div 3,
      FLT.Caption['�����']);
    FBmp.Canvas.TextOut(Round(FixedLeft * 0.1), TY + (StrH div 3) * 2,
      FLT.Caption['������']);
    for j := 0 to 1 do
    begin
      TY := TY + (StrH div 3);
{$IFDEF Avikon14_2Block}
      c1 := 13;
{$ELSE}
      c1 := 7;
{$ENDIF}
      for K := 0 to c1 do
      begin
        TX := Round(FixedLeft + ChW * K + FixedLeft * 0.25);
        //
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US) }
        case i of

          /// //////-------�������� ����������������------////////////
          1:
            if (K = 0) then
              Val := Format('%d/%d', [General.Core.Kanal[j, 0
                { K+ 10 } ].Ky, General.Core.Kanal[j, 1 { K+ 11 } ].Ky])

            else if (K = 1) then
              Val := Format
                ('%d/%d', [General.Core.Kanal[j, 10].Ky,
                General.Core.Kanal[j, 11].Ky])

            else if K = 2 then
              if j = 0 then
                Val := Format('%d/%d', [General.Core.Kanal[j, K + 10].Ky,
                  General.Core.Kanal[j, K].Ky])
              else
                Val := Format
                  ('%d/%d', [General.Core.Kanal[j, K].Ky,
                  General.Core.Kanal[j, K + 10].Ky])
              else if K = 3 then
                if j = 0 then
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].Ky,
                    General.Core.Kanal[j, K + 10].Ky])
                else
                  Val := Format('%d/%d', [General.Core.Kanal[j, K + 10].Ky,
                    General.Core.Kanal[j, K].Ky])

                else if K in [6, 7] then
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].Ky,
                    General.Core.Kanal[j, K + 2].Ky])

                else
                  Val := IntToStr(General.Core.Kanal[j, K].Ky);

          /// //////////-------�����������------///////////////////

          2:
            if (K = 0) then
              Val := Format('%d/%d', [General.Core.Kanal[j, 0
                { K+ 10 } ].ATT, General.Core.Kanal[j, 1 { K+ 11 } ].ATT])

            else if (K = 1) then
              Val := Format
                ('%d/%d', [General.Core.Kanal[j, 10].ATT,
                General.Core.Kanal[j, 11].ATT])

            else if K = 2 then
              if j = 0 then
                Val := Format('%d/%d', [General.Core.Kanal[j, K + 10].ATT,
                  General.Core.Kanal[j, K].ATT])
              else
                Val := Format
                  ('%d/%d', [General.Core.Kanal[j, K].ATT,
                  General.Core.Kanal[j, K + 10].ATT])
              else if K = 3 then
                if j = 0 then
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].ATT,
                    General.Core.Kanal[j, K + 10].ATT])
                else
                  Val := Format('%d/%d', [General.Core.Kanal[j, K + 10].ATT,
                    General.Core.Kanal[j, K].ATT])

                else if K in [6, 7] then
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].ATT,
                    General.Core.Kanal[j, K + 2].ATT])

                else
                  Val := IntToStr(General.Core.Kanal[j, K].ATT);

          /// //////////--------�������� � ������-------///////////////////

          3:
            // 3: Val:=IntToStr(Round(General.Core.Kanal[j, k].TwoTP*10));

            if (K = 0) then
              Val := Format('%3.1f', [General.Core.Kanal[j, K { + 10 } ].TwoTp])

            else if (K = 1) then
              Val := Format('%3.1f', [General.Core.Kanal[j, 10
                { K- 1 } ].TwoTp])

            else if (K = 2) then
              if j = 0 then
                Val := Format
                  ('%3.1f/%3.1f', [General.Core.Kanal[j, K + 10].TwoTp,
                  General.Core.Kanal[j, K].TwoTp])
              else
                Val := Format('%3.1f/%3.1f', [General.Core.Kanal[j, K].TwoTp,
                  General.Core.Kanal[j, K + 10].TwoTp])
              else if (K = 3) then
                if j = 0 then
                  Val := Format('%3.1f/%3.1f', [General.Core.Kanal[j, K].TwoTp,
                    General.Core.Kanal[j, K + 10].TwoTp])
                else
                  Val := Format
                    ('%3.1f/%3.1f', [General.Core.Kanal[j, K + 10].TwoTp,
                    General.Core.Kanal[j, K].TwoTp])

                else
                  Val := Format('%3.1f', [General.Core.Kanal[j, K].TwoTp]);
          (* </Rud1> *)

          /// //////////------------���------------///////////////////
          4:
            if (K = 0) then
              Val := Format('%d', [General.Core.Kanal[j, 0].Vrch])
            else if (K = 1) then
              Val := Format('%d', [General.Core.Kanal[j, 10].Vrch])

            else if (K = 2) then
              if j = 0 then
                Val := Format('%d/%d', [General.Core.Kanal[j, K + 10].Vrch,
                  General.Core.Kanal[j, K].Vrch])
              else
                Val := Format
                  ('%d/%d', [General.Core.Kanal[j, K].Vrch,
                  General.Core.Kanal[j, K + 10].Vrch])
              else if (K = 3) then
                if j = 0 then
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].Vrch,
                    General.Core.Kanal[j, K + 10].Vrch])
                else
                  Val := Format('%d/%d', [General.Core.Kanal[j, K + 10].Vrch,
                    General.Core.Kanal[j, K].Vrch])

                else
                  Val := Format('%d', [General.Core.Kanal[j, K].Vrch]);

          /// //////////--------������ ������--------///////////////////

          5:
            if (K = 0) then
              Val := Format('%d/%d', [General.Core.Kanal[j, 0].Str_st,
                General.Core.Kanal[j, 1].Str_st])

            else if (K = 1) then
              Val := Format('%d/%d', [General.Core.Kanal[j, 10].Str_st,
                General.Core.Kanal[j, 11].Str_st])

            else if K = 2 then
              if j = 0 then
                Val := Format('%d/%d', [General.Core.Kanal[j, K + 10].Str_st,
                  General.Core.Kanal[j, K].Str_st])
              else
                Val := Format
                  ('%d/%d', [General.Core.Kanal[j, K].Str_st,
                  General.Core.Kanal[j, K + 10].Str_st])
              else if K = 3 then
                if j = 0 then
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].Str_st,
                    General.Core.Kanal[j, K + 10].Str_st])
                else
                  Val := Format('%d/%d', [General.Core.Kanal[j, K + 10].Str_st,
                    General.Core.Kanal[j, K].Str_st])

                else if K in [6, 7] then
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].Str_st,
                    General.Core.Kanal[j, K + 2].Str_st])

                else
                  Val := IntToStr(General.Core.Kanal[j, K].Str_st);

          /// //////////---------����� ������--------///////////////////

          6:
            if (K = 0) then
              Val := Format('%d/%d', [General.Core.Kanal[j, 0].Str_en,
                General.Core.Kanal[j, 1].Str_en])

            else if (K = 1) then
              Val := Format('%d/%d', [General.Core.Kanal[j, 10].Str_en,
                General.Core.Kanal[j, 11].Str_en])

            else if K = 2 then
              if j = 0 then
                Val := Format('%d/%d', [General.Core.Kanal[j, K + 10].Str_en,
                  General.Core.Kanal[j, K].Str_en])
              else
                Val := Format
                  ('%d/%d', [General.Core.Kanal[j, K].Str_en,
                  General.Core.Kanal[j, K + 10].Str_en])
              else if K = 3 then
                if j = 0 then
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].Str_en,
                    General.Core.Kanal[j, K + 10].Str_en])
                else
                  Val := Format('%d/%d', [General.Core.Kanal[j, K + 10].Str_en,
                    General.Core.Kanal[j, K].Str_en])

                else if K in [6, 7] then
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].Str_en,
                    General.Core.Kanal[j, K + 2].Str_en])

                else
                  Val := IntToStr(General.Core.Kanal[j, K].Str_en);

        end;
        (* </Rud46> *)
{$ELSE}
{$IFDEF Avikon16_IPV}
        (* <Rud46> *)
        case i of

          /// //////-------�������� ����������������------////////////
          1:
            if (K = 0) and (Config.SoundedScheme = Wheels) then
              Val := Format('%d/%d', [General.Core.Kanal[j, K + 10].Ky,
                General.Core.Kanal[j, K + 11].Ky])

            else if (K = 1) and (Config.SoundedScheme = Wheels) then
              Val := Format('%d/%d', [General.Core.Kanal[j, K - 1].Ky,
                General.Core.Kanal[j, K].Ky])

            else if (K in [2, 3]) and (Config.SoundedScheme <> Wheels) then
              if j = 0 then
                Val := Format
                  ('%d/%d', [General.Core.Kanal[j, K].Ky,
                  General.Core.Kanal[j, K + 8].Ky])
              else
                Val := Format
                  ('%d/%d', [General.Core.Kanal[j, K + 8].Ky,
                  General.Core.Kanal[j, K].Ky])

              else if (K in [2, 3]) and (Config.SoundedScheme = Wheels) then
                if j = 0 then
                  Val := Format('%d/%d', [General.Core.Kanal[j, K + 10].Ky,
                    General.Core.Kanal[j, K].Ky])
                else
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].Ky,
                    General.Core.Kanal[j, K + 10].Ky])

                else if (K = 5) and (Config.SoundedScheme = Scheme3) then
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].Ky,
                    General.Core.Kanal[j, K + 7].Ky])

                else if K in [6, 7] then
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].Ky,
                    General.Core.Kanal[j, K + 2].Ky])

                else
                  Val := IntToStr(General.Core.Kanal[j, K].Ky);

          /// //////////-------�����������------///////////////////

          2:
            if (K = 0) and (Config.SoundedScheme = Wheels) then
              Val := Format
                ('%d/%d', [General.Core.Kanal[j, K + 10].ATT,
                General.Core.Kanal[j, K + 11].ATT])

            else if (K = 1) and (Config.SoundedScheme = Wheels) then
              Val := Format('%d/%d', [General.Core.Kanal[j, K - 1].ATT,
                General.Core.Kanal[j, K].ATT])

            else if (K in [2, 3]) and (Config.SoundedScheme <> Wheels) then
              if j = 0 then
                Val := Format
                  ('%d/%d', [General.Core.Kanal[j, K].ATT,
                  General.Core.Kanal[j, K + 8].ATT])
              else
                Val := Format('%d/%d', [General.Core.Kanal[j, K + 8].ATT,
                  General.Core.Kanal[j, K].ATT])

              else if (K in [2, 3]) and (Config.SoundedScheme = Wheels) then
                if j = 0 then
                  Val := Format('%d/%d', [General.Core.Kanal[j, K + 10].ATT,
                    General.Core.Kanal[j, K].ATT])
                else
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].ATT,
                    General.Core.Kanal[j, K + 10].ATT])

                else if (K = 5) and (Config.SoundedScheme = Scheme3) then
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].ATT,
                    General.Core.Kanal[j, K + 7].ATT])

                else if K in [6, 7] then
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].ATT,
                    General.Core.Kanal[j, K + 2].ATT])

                else
                  Val := IntToStr(General.Core.Kanal[j, K].ATT);

          /// //////////--------�������� � ������-------///////////////////

          3:
            // 3: Val:=IntToStr(Round(General.Core.Kanal[j, k].TwoTP*10));

            if (K = 0) and (Config.SoundedScheme = Wheels) then
              Val := Format('%3.1f', [General.Core.Kanal[j, K + 10].TwoTp])

            else if (K = 1) and (Config.SoundedScheme = Wheels) then
              Val := Format('%3.1f', [General.Core.Kanal[j, K - 1].TwoTp])

            else if (K in [2, 3]) and (Config.SoundedScheme <> Wheels) then
              if j = 0 then
                Val := Format('%3.1f/%3.1f', [General.Core.Kanal[j, K].TwoTp,
                  General.Core.Kanal[j, K + 8].TwoTp])
              else
                Val := Format
                  ('%3.1f/%3.1f', [General.Core.Kanal[j, K + 8].TwoTp,
                  General.Core.Kanal[j, K].TwoTp])

              else if (K in [2, 3]) and (Config.SoundedScheme = Wheels) then
                if j = 0 then
                  Val := Format
                    ('%3.1f/%3.1f', [General.Core.Kanal[j, K + 10].TwoTp,
                    General.Core.Kanal[j, K].TwoTp])
                else
                  Val := Format('%3.1f/%3.1f', [General.Core.Kanal[j, K].TwoTp,
                    General.Core.Kanal[j, K + 10].TwoTp])

                else if (K = 5) and (Config.SoundedScheme = Scheme3) then
                  Val := Format('%3.1f/%3.1f', [General.Core.Kanal[j, K].TwoTp,
                    General.Core.Kanal[j, K + 7].TwoTp])

                else
                  Val := Format('%3.1f', [General.Core.Kanal[j, K].TwoTp]);
          (* </Rud1> *)

          /// //////////------------���------------///////////////////
          4:
            // 4: Val:=IntToStr(General.Core.Kanal[j, k].VRCH);
            if (K = 0) and (Config.SoundedScheme = Wheels) then
              Val := Format('%d', [General.Core.Kanal[j, K + 10].Vrch])

            else if (K = 1) and (Config.SoundedScheme = Wheels) then
              Val := Format('%d', [General.Core.Kanal[j, K - 1].Vrch])

            else if (K in [2, 3]) and (Config.SoundedScheme <> Wheels) then
              if j = 0 then
                Val := Format
                  ('%d/%d', [General.Core.Kanal[j, K].Vrch,
                  General.Core.Kanal[j, K + 8].Vrch])
              else
                Val := Format('%d/%d', [General.Core.Kanal[j, K + 8].Vrch,
                  General.Core.Kanal[j, K].Vrch])

              else if (K in [2, 3]) and (Config.SoundedScheme = Wheels) then
                if j = 0 then
                  Val := Format('%d/%d', [General.Core.Kanal[j, K + 10].Vrch,
                    General.Core.Kanal[j, K].Vrch])
                else
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].Vrch,
                    General.Core.Kanal[j, K + 10].Vrch])

                else if (K = 5) and (Config.SoundedScheme = Scheme3) then
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].Vrch,
                    General.Core.Kanal[j, K + 7].Vrch])

                else
                  Val := Format('%d', [General.Core.Kanal[j, K].Vrch]);

          /// //////////--------������ ������--------///////////////////

          5:
            if (K = 0) and (Config.SoundedScheme = Wheels) then
              Val := Format('%d/%d', [General.Core.Kanal[j, K + 10].Str_st,
                General.Core.Kanal[j, K + 11].Str_st])

            else if (K = 1) and (Config.SoundedScheme = Wheels) then
              Val := Format('%d/%d', [General.Core.Kanal[j, K - 1].Str_st,
                General.Core.Kanal[j, K].Str_st])

            else if (K in [2, 3]) and (Config.SoundedScheme <> Wheels) then
              if j = 0 then
                Val := Format
                  ('%d/%d', [General.Core.Kanal[j, K].Str_st,
                  General.Core.Kanal[j, K + 8].Str_st])
              else
                Val := Format('%d/%d', [General.Core.Kanal[j, K + 8].Str_st,
                  General.Core.Kanal[j, K].Str_st])

              else if (K in [2, 3]) and (Config.SoundedScheme = Wheels) then
                if j = 0 then
                  Val := Format('%d/%d', [General.Core.Kanal[j, K + 10].Str_st,
                    General.Core.Kanal[j, K].Str_st])
                else
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].Str_st,
                    General.Core.Kanal[j, K + 10].Str_st])

                else if (K = 5) and (Config.SoundedScheme = Scheme3) then
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].Str_st,
                    General.Core.Kanal[j, K + 7].Str_st])

                else if K in [6, 7] then
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].Str_st,
                    General.Core.Kanal[j, K + 2].Str_st])

                else
                  Val := IntToStr(General.Core.Kanal[j, K].Str_st);

          /// //////////---------����� ������--------///////////////////

          6:
            if (K = 0) and (Config.SoundedScheme = Wheels) then
              Val := Format('%d/%d', [General.Core.Kanal[j, K + 10].Str_en,
                General.Core.Kanal[j, K + 11].Str_en])

            else if (K = 1) and (Config.SoundedScheme = Wheels) then
              Val := Format('%d/%d', [General.Core.Kanal[j, K - 1].Str_en,
                General.Core.Kanal[j, K].Str_en])

            else if (K in [2, 3]) and (Config.SoundedScheme <> Wheels) then
              if j = 0 then
                Val := Format
                  ('%d/%d', [General.Core.Kanal[j, K].Str_en,
                  General.Core.Kanal[j, K + 8].Str_en])
              else
                Val := Format('%d/%d', [General.Core.Kanal[j, K + 8].Str_en,
                  General.Core.Kanal[j, K].Str_en])

              else if (K in [2, 3]) and (Config.SoundedScheme = Wheels) then
                if j = 0 then
                  Val := Format('%d/%d', [General.Core.Kanal[j, K + 10].Str_en,
                    General.Core.Kanal[j, K].Str_en])
                else
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].Str_en,
                    General.Core.Kanal[j, K + 10].Str_en])

                else if (K = 5) and (Config.SoundedScheme = Scheme3) then
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].Str_en,
                    General.Core.Kanal[j, K + 7].Str_en])

                else if K in [6, 7] then
                  Val := Format
                    ('%d/%d', [General.Core.Kanal[j, K].Str_en,
                    General.Core.Kanal[j, K + 2].Str_en])
                else
                  Val := IntToStr(General.Core.Kanal[j, K].Str_en);
        end;
        (* </Rud46> *)
{$ELSE}
{$IFDEF Avikon14_2Block}
        case i of
          1:
            Val := IntToStr(General.Core.Kanal[j, K].Ky);
          2:
            Val := IntToStr(General.Core.Kanal[j, K].ATT);
          3:
            Val := Format('%3.1f', [General.Core.Kanal[j, K].TwoTp]);
          4:
            Val := IntToStr(General.Core.Kanal[j, K].Vrch);
          5:
            Val := IntToStr(General.Core.Kanal[j, K].Str_st);
          6:
            Val := IntToStr(General.Core.Kanal[j, K].Str_en);
        end;
{$ELSE}
        case i of
          1:
            if K in [6, 7] then
              Val := Format
                ('%d/%d', [General.Core.Kanal[j, K].Ky,
                General.Core.Kanal[j, K + 2].Ky])
            else
              Val := IntToStr(General.Core.Kanal[j, K].Ky);
          2:
            Val := IntToStr(General.Core.Kanal[j, K].ATT);
          3:
            Val := Format('%3.1f', [General.Core.Kanal[j, K].TwoTp]);
          4:
            if K in [6, 7] then
              Val := Format
                ('%d/%d', [General.Core.Kanal[j, K].Vrch,
                General.Core.Kanal[j, K + 2].Vrch])
            else
              Val := IntToStr(General.Core.Kanal[j, K].Vrch);
          5:
            if K in [6, 7] then
              Val := Format('%d/%d', [General.Core.Kanal[j, K].Str_st,
                General.Core.Kanal[j, K + 2].Str_st])
            else
              Val := IntToStr(General.Core.Kanal[j, K].Str_st);
          6:
            if K in [6, 7] then
              Val := Format('%d/%d', [General.Core.Kanal[j, K].Str_en,
                General.Core.Kanal[j, K + 2].Str_en])
            else
              Val := IntToStr(General.Core.Kanal[j, K].Str_en);

        end;
{$ENDIF}
{$ENDIF}
{$IFEND}
        //
        FBmp.Canvas.TextOut(TX, TY, Val);
      end;
    end;
{$ELSE}
    TY := TY + (StrH div 3);
    j := 0;
    c := High(Kanals[0]);
    for K := 0 to c do
    begin
      TX := Round(FixedLeft + ChW * K + FixedLeft * 0.25);
{$IFDEF USK004R}
      case i of
        1:
          if K in [6, 7] then
            Val := Format('%d/%d', [General.Core.Kanal[j, K].Ky,
              General.Core.Kanal[j, K + 2].Ky])
          else if (K in [2, 3]) then
            Val := Format('%d/%d', [General.Core.Kanal[j, K].Ky,
              General.Core.Kanal[j, K + 8].Ky])
          else
            Val := IntToStr(General.Core.Kanal[j, K].Ky);

        2:
          if K in [6, 7] then
            Val := Format('%d/%d', [General.Core.Kanal[j, K].ATT,
              General.Core.Kanal[j, K + 2].ATT])
          else if (K in [2, 3]) then
            Val := Format('%d/%d', [General.Core.Kanal[j, K].ATT,
              General.Core.Kanal[j, K + 8].ATT])
          else
            Val := IntToStr(General.Core.Kanal[j, K].ATT);
        3:
          if (K in [2, 3]) then
            Val := Format('%3.1f/%3.1f', [General.Core.Kanal[j, K].TwoTp,
              General.Core.Kanal[j, K + 8].TwoTp])
          else
            Val := Format('%3.1f', [General.Core.Kanal[j, K].TwoTp]);
        4:
          if (K in [2, 3]) then
            Val := Format('%d/%d', [General.Core.Kanal[j, K].Vrch,
              General.Core.Kanal[j, K + 8].Vrch])
            { if K in [6, 7] then
              Val := Format
              ('%d/%d', [General.Core.Kanal[j, K].VRCH,
              General.Core.Kanal[j, K + 2].VRCH]) }
          else
            Val := IntToStr(General.Core.Kanal[j, K].Vrch);
        5:
          if (K in [2, 3]) then
            Val := Format('%d/%d', [General.Core.Kanal[j, K].Str_st,
              General.Core.Kanal[j, K + 8].Str_st])
          else if K in [6, 7] then
            Val := Format('%d/%d', [General.Core.Kanal[j, K].Str_st,
              General.Core.Kanal[j, K + 2].Str_st])
          else
            Val := IntToStr(General.Core.Kanal[j, K].Str_st);
        6:
          if (K in [2, 3]) then
            Val := Format('%d/%d', [General.Core.Kanal[j, K].Str_en,
              General.Core.Kanal[j, K + 8].Str_en])
          else if K in [6, 7] then
            Val := Format('%d/%d', [General.Core.Kanal[j, K].Str_en,
              General.Core.Kanal[j, K + 2].Str_en])
          else
            Val := IntToStr(General.Core.Kanal[j, K].Str_en);

      end;
{$ELSE}
      case i of
        1:
          Val := IntToStr(General.Core.Kanal[j, K].Ky);
        2:
          Val := IntToStr(General.Core.Kanal[j, K].ATT);
        3:
          Val := Format('%3.2f', [General.Core.Kanal[j, K].TwoTp]);
        4:
          Val := IntToStr(General.Core.Kanal[j, K].Vrch);
        5:
          Val := IntToStr(General.Core.Kanal[j, K].Str_st);
        6:
          Val := IntToStr(General.Core.Kanal[j, K].Str_en);
      end;
{$ENDIF}
      FBmp.Canvas.TextOut(TX, TY, Val);
    end;
{$ENDIF}
  end;
  FBmp.Canvas.Brush.Style := bsSolid;
  FPB.RePaint;
end;

procedure TAdjTableWindow.OnExit(Sender: TObject);
begin
  General.OpenWindow('MAINMENU');
end;

procedure TAdjTableWindow.Open;
begin
  DrawTable;
  inherited;
end;

procedure TAdjTableWindow.RePaint(Sender: TObject);
begin
  FPB.Canvas.Draw(0, 0, FBmp);
end;

{ TLoadingMessageWindow }

constructor TLoadingMessageWindow.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);
var
  Msg: string;
  MsgW: integer;
begin
  inherited;
  Msg := LT.Caption['Loading_MSG'];
  Self.Font.Height := Round(0.2 * Self.Width);
  Self.Font.Name := DefaultFontName;
  Self.Font.Color := FCT.Color['BootWin_ButtonText'];
  Self.Caption := Msg;
end;

{ TAdjustSoftware }

procedure TAdjustSoftware.RunControlCenter(Sender: TObject);
var
  hWnd_: hWnd;
begin
  ShellExecute(0, nil, PChar(ExtractFilePath(Application.ExeName)
        + 'ControlCenter.exe'), nil, nil, SW_SHOW);
end;

procedure TAdjustSoftware.ChangeBUISN(Sender: TObject);
begin
  General.OpenWindow('EnterSN');
end;

constructor TAdjustSoftware.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);

var
  R: TRegComboBox;

  BP: TPanel;
  TP: TPanel;

  FExitBut: TButton;

  FLng: TRegComboBox;
  FSnd: TRegTrackBar;
  FZnd: TRegComboBox;
  FDta: TRegDate;
  FTim: TRegTime;
  FBut: TRegButton;

  FZeroSnd: TRegComboBox;

  t: TRegComboBox;
  i, l, MinTop, MaxTop: integer;

  SearchFolder: string;
  sr: TSearchRec;
  FindRes: integer;

  procedure Add(Control: TCustomInputItem);
  begin
    Pan.Add(Control);
    Control.ObjWidth := Round(Self.Width * 0.30);
    Control.ObjLeft := Round(Self.Width * 0.52);
    Control.OnMouseD := MouseDown;
    Control.OnMouseU := MouseUp;
  end;

begin
  inherited;
  Self.ParentDoubleBuffered := False;
  Self.DoubleBuffered := true;
  OY := Mouse.CursorPos.Y;
  MouseTimer := TTimer.Create(nil);
  MouseTimer.Interval := 10;
  MouseTimer.Enabled := true;
  MouseTimer.OnTimer := OnMouseTimer;

  Pan1 := TPanel.Create(Self);
  Pan1.Parent := Self;
  Pan1.Align := alClient;
  Pan1.Color := Color;
  Pan1.BevelOuter := bvNone;
  Pan1.OnMouseDown := MouseDown;
  Pan1.OnMouseUp := MouseUp;
  Pan1.ParentDoubleBuffered := False;
  Pan1.DoubleBuffered := true;

  Pan2 := TPanel.Create(Self);
  Pan2.Parent := Pan1;
  Pan2.Align := alBottom;
  Pan2.Height := 50;
  Pan2.Color := Color;
  Pan2.BevelOuter := bvNone;
  Pan2.OnMouseDown := MouseDown;
  Pan2.OnMouseUp := MouseUp;
  Pan2.ParentDoubleBuffered := False;
  Pan2.DoubleBuffered := true;

  Pan3 := TPanel.Create(Self);
  Pan3.Parent := Pan1;
  Pan3.Align := alRight;
  Pan3.Width := 70;
  Pan3.Color := Color;
  Pan3.BevelOuter := bvNone;
  Pan3.OnMouseDown := MouseDown;
  Pan3.OnMouseUp := MouseUp;
  Pan3.ParentDoubleBuffered := False;
  Pan3.DoubleBuffered := true;

  SB := TScrollBar.Create(Self);
  SB.Parent := Pan3;
  SB.Kind := sbVertical;
  SB.Align := alClient;

  GB := TGroupBox.Create(Self);
  GB.Parent := Pan1;
  GB.Align := alClient;
  GB.OnMouseDown := MouseDown;
  GB.OnMouseUp := MouseUp;
  GB.ParentDoubleBuffered := False;
  GB.DoubleBuffered := true;

  Pan := TControlPanel.Create('AdjSoft', poVertical, caTop, 10);
  Pan.Parent := GB;
  Pan.Align := alClient;
  Pan.Color := Color;
  Pan.BevelOuter := bvNone;
  Pan.OnMouseDown := MouseDown;
  Pan.OnMouseUp := MouseUp;
  Pan.ParentDoubleBuffered := False;
  Pan.DoubleBuffered := true;

  // ����.
  if not ProgramProfile.OnlyRussian then
  begin
    FLng := TRegComboBox.Create('lang', Pan, LT['����']);
    Add(FLng);
    l := 0;
    for i := 0 to LT.GroupsCount - 1 do
    begin
      FLng.Add(LT.GroupName[i]);
      if LT.GroupName[i] = Config.Language then
        l := i;
    end;
    FLng.ItemIndex := l;
    FCurLang := LT.GroupName[FLng.ItemIndex];
  end;

  // ����� �������������
  if ProgramProfile.DeviceFamily = 'Avicon16' then
  begin
    FLng := TRegComboBox.Create('scheme', Pan, LT['����� �������������']);
    Add(FLng);
    FLng.Add(LT['�����-1']);
    FLng.Add(LT['�����-2']);
    FLng.Add(LT['�����-3']);
    FLng.Add(LT['���']);
    FLng.ItemIndex := Ord(Config.SoundedScheme);
  end;

  { if ProgramProfile.DeviceFamily = 'EGO-USW' then
    begin
    FLng := TRegComboBox.Create('scheme', Pan, LT['����� �������������']);
    Add(FLng);
    FLng.Add(LT['�����-1']);
    FLng.Add(LT['�����-2']);
    FLng.Add(LT['�����-3']);
    if Ord(Config.SoundedScheme) > 2 then Config.SoundedScheme := Scheme1;
    FLng.ItemIndex := Ord(Config.SoundedScheme);
    end;
    }
  if ProgramProfile.DeviceFamily = 'Avicon14' then
  begin
    FLng := TRegComboBox.Create('scheme', Pan, LT['��� ����������� �������']);
    Add(FLng);
    FLng.Add(LT['�������']);
    FLng.Add(LT['����������']);
    if Config.SoundedScheme = Scheme1 then
      FLng.ItemIndex := 1
    else if Config.SoundedScheme = Wheels then
      FLng.ItemIndex := 0
    else
    begin
      Config.SoundedScheme := Wheels;
      FLng.ItemIndex := 0;
    end;
  end;
  if ProgramProfile.DeviceFamily = 'RKS-U' then
  begin
    FLng := TRegComboBox.Create('scheme', Pan, LT['����� �������������']);
    Add(FLng);
    FLng.Add(LT['5 �������']);
    FLng.Add(LT['8 �������']);
    FLng.ItemIndex := Ord(Config.SoundedScheme);
  end;

  FCurSchema := Ord(Config.SoundedScheme);
  FCurUnitSys := Ord(Config.UnitsSystem);
  FCurAirbrushSprayer := Ord(Config.PaintSprayer);
  FCurAirbrushState := Ord(Config.PainterState);
  FCurAirbrushConfig := Config.AirBrushConfig;

  // �����������.
  if ProgramProfile.DeviceFamily <> 'RKS-U' then
  begin
{$IFDEF USK004R}
    FLng := TRegComboBox.Create('zond', Pan, LT['Rail visualization']);
    // �����������
    Add(FLng);
    FLng.Add(LT['Upside down']); // �����
    FLng.Add(LT['Normal']); // ������    //������� ������ ������
{$ELSE}
    FLng := TRegComboBox.Create('zond', Pan, LT['�����������']);
    Add(FLng);
    FLng.Add(LT['�����']);
    FLng.Add(LT['������']);
{$ENDIF}
    FLng.ItemIndex := Ord(Config.ZondPosition);
  end;

  // ������ �����.
  FLng := TRegComboBox.Create('pntsize', Pan, LT['����� �-���������']);
  Add(FLng);
  FLng.Add(LT['�����']);
  FLng.Add(LT['�������']);
  FLng.Add(LT['�������']);
  FLng.ItemIndex := Ord(Config.BViewPointSize);

  // ��������� ����� ����.
  FLng := TRegComboBox.Create('pntamplcolor', Pan, LT['�������� ����� ����']);
  Add(FLng);
  FLng.Add(LT['����']);
  FLng.Add(LT['���']);
  FLng.ItemIndex := Ord(Config.BViewAmpByColor);

  // ��������� ����� ������ �����.
  FLng := TRegComboBox.Create('pntamplsize', Pan, LT['�������� ����� ��������']
    );
  Add(FLng);
  FLng.Add(LT['����']);
  FLng.Add(LT['���']);
  FLng.ItemIndex := Ord(Config.BViewAmpBySize);

  // ���������.
  FSnd := TRegTrackBar.Create('volume', Pan, LT['���������']);
  Add(FSnd);
  FSnd.OnChange := OnVolumeChange;
  FSnd.Value := Config.SndVolume;

  // ����-����.
  FLng := TRegComboBox.Create('flashletter', Pan, LT['����-����']);
  Add(FLng);
  FLng.Add(LT['����']);
  for i := 0 to Ord('G') - Ord('C') do
    FLng.Add(Chr(Ord('C') + i) + ':');
  FLng.ItemIndex := Config.FlashDrive + 1;

  // �����
  FTim := TRegTime.Create('time', Pan, LT['�����']);
  Add(FTim);
  FTim.Value := Now;

  // ����.
  FDta := TRegDate.Create('date', Pan, LT['����']);
  Add(FDta);
  FDta.Value := Now;

  if ProgramProfile.UseAirBrush then
  begin
    // ����������� (������ ������) (�������/��������)
    FLng := TRegComboBox.Create
      ('paint_sprayer', Pan, LT['����� ������ ������']);
    Add(FLng);
    FLng.Add(LT['����. ������']);
    FLng.Add(LT['���. �������']);
    FLng.Add(LT['���������� ����������']);
    FLng.ItemIndex := Ord(Config.PaintSprayer);

    // �������� ������������ (�������/��������)
    FLng := TRegComboBox.Create('painter', Pan, LT['��������� ������������']);
    Add(FLng);
    FLng.Add(LT['����. ������']);
    FLng.Add(LT['���. ���.']);
    FLng.Add(LT['���. ������.']);
    FLng.ItemIndex := Ord(Config.PainterState);

    // ������ ������������
    FLng := TRegComboBox.Create
      ('painter_config', Pan, LT['������. ������������']);
    Add(FLng);
    // ���� ����� � ����� � ����������� (� ����������� �� ��������� � ������ ������ ����������� ��������)
    SearchFolder := ExtractFilePath(Application.ExeName);
    FindRes := FindFirst(SearchFolder + '*.abp', faAnyFile, sr);
    l := 0;
    i := 0;
    While FindRes = 0 do
    begin
      // ���� ��������� ������� ������� �
      if not(((sr.Attr and faDirectory) = faDirectory) and
          ((sr.Name = '.') or (sr.Name = '..'))) then
      begin
        FLng.Add(LT[sr.Name]);
        Inc(i);
        if sr.Name = Config.AirBrushConfig then
          l := i - 1;
      end;
      FindRes := FindNext(sr);
    end;
    FindClose(sr);
    FLng.ItemIndex := l;
  end;

  // �������� �������� �������
  FLng := TRegComboBox.Create('adjvar', Pan, LT['������� ��������']);
  Add(FLng);
  FLng.Add(LT['������� 1']);
  FLng.Add(LT['������� 2']);
  FLng.Add(LT['������� 3']);
  FLng.ItemIndex := Config.AdjustmentVariantNum;
  FCurAdjVar := Config.AdjustmentVariantNum;

  // ����� ������� ��������� � ���������
  if ProgramProfile.AllowToChooseCoordSystem then
  begin
    FLng := TRegComboBox.Create('unitssystem', Pan, LT['������� ���������']);
    Add(FLng);
    FLng.Add(LT['Metric R']);
    FLng.Add(LT['Imperial']);
    FLng.Add(LT['Metric A (0.1km)']);
    FLng.Add(LT['Metric B (1km)']);
    FLng.Add(LT['Metric Tr(1km)']);
    FLng.ItemIndex := Ord(Config.UnitsSystem);
  end;
  //
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US) }
  FLng := TRegComboBox.Create('directofmotion', Pan, LT['����������� ��������']
    ); // ����������� �������� //Direction of motion
  Add(FLng);
  FLng.Add(LT['A forward']);
  FLng.Add(LT['B forward']);
  if Config.DirectionOfMotion then
    FLng.ItemIndex := 0
  else
    FLng.ItemIndex := 1;
  FCurDirectionOfMotion := Config.DirectionOfMotion;
  //
  FLng := TRegComboBox.Create('masterbumside', Pan, LT['"TS1" of Encoder Cable']
    ); // ������� TS1 ������ ��
  Add(FLng);
  FLng.Add(LT['Left side']);
  FLng.Add(LT['Right side']);
  if Config.MasterBUMLeft then
    FLng.ItemIndex := 0
  else
    FLng.ItemIndex := 1;
  FCurMasterBUM := Config.MasterBUMLeft;

  FLng := TRegComboBox.Create('SelectChan0', Pan, LT['Select channel 0�']);
  // ����������� ������� 0 ���� �� �-����-��
  Add(FLng);
  FLng.Add(LT['WPA and WPB']);
  FLng.Add(LT['WPA']);
  FLng.Add(LT['WPB']);
  FLng.ItemIndex := Ord(Config.SelectChan0);
  FCurSelectChan0 := Config.SelectChan0;
{$IFEND}
  //
{$IFNDEF USK004R}
  // ���������� ���� ������ � ���
  if ProgramProfile.DeviceFamily <> 'RKS-U' then
  begin
    FLng := TRegComboBox.Create
      ('bottomshift', Pan, LT['����� ���� ������ � ���']);
    Add(FLng);
    FLng.Add('5, ' + LT['���']);
    FLng.Add('6, ' + LT['���']);
    FLng.Add('7, ' + LT['���']);
    FLng.Add('8, ' + LT['���']);
    FLng.Add('9, ' + LT['���']);
    FLng.Add('10, ' + LT['���']);
    FLng.ItemIndex := Config.BottomTimeShift - 5;
  end;
{$ENDIF}
  //
{$IFNDEF USK004R}
{$IFNDEF EGO_USW}
{$IFNDEF VMT_US}
  // ������������ ������ ������ �����
  if ProgramProfile.DeviceFamily <> 'RKS-U' then
  begin
    FLng := TRegComboBox.Create('secondbottomon', Pan,
      LT['���������� ������ ������']);
    Add(FLng);
    FLng.Add(LT['���']);
    FLng.Add(LT['��']);
    if Config.SecondaryBottomOn then
      FLng.ItemIndex := 1
    else
      FLng.ItemIndex := 0;
  end;
{$ENDIF}
{$ENDIF}
{$ENDIF}
  //
{$IFDEF USK004R}
  // WorkSide
  FLng := TRegComboBox.Create('WorkSide', Pan, LT['Gauge side']);
  Add(FLng);
  FLng.Add(LT['�����']);
  FLng.Add(LT['������']);
  if Config.WorkSide then
    FLng.ItemIndex := 1
  else
    FLng.ItemIndex := 0;
{$ENDIF}
  //
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US) }
  // ������ ������ Control Center
  FBut := TRegButton.Create('ControlCentr', Pan, LT['�������������� �������']);
  Add(FBut);
  FBut.OnClick := RunControlCenter;
{$IFEND}
  //
  // ������ �������� �������� ����� ���
  FBut := TRegButton.Create
    ('ChangeBUISN', Pan, LT['�������� �������� ����� ���']);
  Add(FBut);
  FBut.OnClick := ChangeBUISN;

  // ������ �������������� �������� �������
  FBut := TRegButton.Create('ResetKanalsSettings', Pan,
    LT['�����. ��������� ��������� �������']);
  Add(FBut);
  FBut.OnClick := ResetChannelSettings;

  // ���������/���������� ������� ����
  FDPBut := TRegButton.Create('ResetKanalsSettings', Pan,
    LT['�������� �������� ������� ����']);
  Add(FDPBut);
  FDPBut.OnClick := DP_But_Press;

  BP := TPanel.Create(Self);
  BP.ParentColor := False;
  BP.Parent := Self;
  BP.ParentBackground := False;
  BP.Align := alBottom;
  BP.Height := Self.Height div 7;
  BP.Color := Color;
  BP.BevelOuter := bvNone;

  TP := TPanel.Create(Self);
  TP.ParentColor := False;
  TP.Parent := Self;
  TP.ParentBackground := False;
  TP.Align := alTop;
  TP.Height := Self.Height div 10;
  TP.Color := Color;
  TP.BevelOuter := bvNone;
  TP.Caption := LT['��������� ���������'];
  TP.Font.Name := DefaultFontName;
  TP.Font.Height := Round(TP.Height / 1.5);

  FExitBut := ControlFactory.CreateButton(BP);
  FExitBut.Parent := BP;
  FExitBut.Width := Round(Self.Width / 4);
  FExitBut.Height := BP.Height - 5;
  FExitBut.Top := 0;
  FExitBut.Left := 5;
  FExitBut.Font.Height := Round(Self.Height * ButFontH);
  FExitBut.Font.Name := DefaultFontName;
  FExitBut.Font.Color := FCT.Color['BootWin_ButtonText'];
  FExitBut.Caption := LT['�����'];
  FExitBut.Visible := true;
  FExitBut.OnClick := OnExitPress;

  FMinTopControl := nil;
  FMaxTopControl := nil;
  MinTop := 10000;
  MaxTop := 0;
  for i := 0 to Pan.ControlCount - 1 do
  begin
    if TCustomInputItem(Pan.Controls[i]).Top < MinTop then
    begin
      MinTop := TCustomInputItem(Pan.Controls[i]).Top;
      FMinTopControl := TCustomInputItem(Pan.Controls[i]);
    end;
    if TCustomInputItem(Pan.Controls[i]).Top > MaxTop then
    begin
      MaxTop := TCustomInputItem(Pan.Controls[i]).Top;
      FMaxTopControl := TCustomInputItem(Pan.Controls[i]);
    end;
  end;
  OldPos := 0;
  SB.Min := 7;
  if Assigned(FMaxTopControl) then
    SB.Max := FMaxTopControl.Top - (Pan.Height - 100) + 100;
  SB.OnChange := ScrollChange;
  SB.PageSize := 80; // 99;

  SB.LargeChange := Pan.Height;
  SB.SmallChange := Round(Pan.Height * 10 / 100);

end;

procedure TAdjustSoftware.DP_But_Press(Sender: TObject);
begin
  General.SwitchImitDP;
  if General.ImitDP then
    FDPBut.Caption := FLT['��������� �������� ������� ����']
  else
    FDPBut.Caption := FLT['�������� �������� ������� ����'];
end;

procedure TAdjustSoftware.Hide;
begin
  inherited;
  Config.Save;
end;

procedure TAdjustSoftware.MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  FMouseDown := true;
end;

procedure TAdjustSoftware.MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  FMouseDown := False;
end;

procedure TAdjustSoftware.OnExitPress;

  function BoxItemIndex(name: string): integer;
  begin
    Result := TRegComboBox(Pan.Item[name]).ItemIndex;
  end;

var
  RequestReboot: Boolean;

begin
  MouseTimer.Enabled := False;
  RequestReboot := False;

  if not ProgramProfile.OnlyRussian then
  begin
    Config.Language := TRegComboBox(Pan.Item['lang']).Value;
    RequestReboot := RequestReboot or
      (FCurLang <> TRegComboBox(Pan.Item['lang']).Value);
  end;

  if Assigned(Pan.Item['scheme']) then
  begin
    if (ProgramProfile.DeviceFamily = 'Avicon14') then
    begin
      if BoxItemIndex('scheme') = 1 then
      begin
        Config.SoundedSchemeAfterReBoot := Scheme1;
        RequestReboot := RequestReboot or (FCurSchema <> Ord(Scheme1));
      end;

      if BoxItemIndex('scheme') = 0 then
      begin
        Config.SoundedSchemeAfterReBoot := Wheels;
        RequestReboot := RequestReboot or (FCurSchema <> Ord(Wheels));
      end;
    end

    else
    begin
      Config.SoundedSchemeAfterReBoot := TSoundedScheme(BoxItemIndex('scheme'));
      RequestReboot := RequestReboot or (FCurSchema <> BoxItemIndex('scheme'));
    end;
  end;

  Config.BViewPointSize := TBViewPointSize(BoxItemIndex('pntsize'));
  Config.BViewAmpByColor := Boolean(BoxItemIndex('pntamplcolor'));
  Config.BViewAmpBySize := Boolean(BoxItemIndex('pntamplsize'));
  Config.FlashDrive := BoxItemIndex('flashletter') - 1;
  Config.AdjustmentVariantNumAfterReBoot := BoxItemIndex('adjvar');
  RequestReboot := RequestReboot or (FCurAdjVar <>
      Config.AdjustmentVariantNumAfterReBoot);

  if Assigned(Pan.Item['zond']) then
    Config.ZondPosition := TZondPosition(BoxItemIndex('zond'));
  if Assigned(Pan.Item['bottomshift']) then
    Config.BottomTimeShift := BoxItemIndex('bottomshift') + 5;
  if Assigned(Pan.Item['secondbottomon']) then
    Config.SecondaryBottomOn := Boolean(BoxItemIndex('secondbottomon'));
  //
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
  if Assigned(Pan.Item['masterbumside']) then
    Config.MasterBUMLeft := not Boolean(BoxItemIndex('masterbumside'));
  RequestReboot := RequestReboot or (FCurMasterBUM <> Config.MasterBUMLeft);
  // if (FCurMasterBUM <> Config.MasterBUMLeft) then
  // if Assigned(General) and Assigned(General.Core) then General.Core.ChangeLeftRightSide()
  // else RequestReboot := RequestReboot or (FCurMasterBUM <> Config.MasterBUMLeft);

  if Assigned(Pan.Item['directofmotion']) then
    Config.DirectionOfMotion := not Boolean(BoxItemIndex('directofmotion'));
  if (FCurDirectionOfMotion <> Config.DirectionOfMotion) then
    if Assigned(General) and Assigned(General.Core) then
    begin
      General.Core.ChangeFrontAndBack();
      FCurDirectionOfMotion := Config.DirectionOfMotion;
    end
    else
      RequestReboot := RequestReboot or
        (FCurDirectionOfMotion <> Config.DirectionOfMotion);

  if Assigned(Pan.Item['SelectChan0']) then
    Config.SelectChan0 := TSelectChan0(BoxItemIndex('SelectChan0'));
  if (FCurSelectChan0 <> Config.SelectChan0) then
  begin
    FCurSelectChan0 := Config.SelectChan0;
    if Assigned(General) and Assigned(General.Core) then
      General.Core.SetChan0Regim(Config.SelectChan0);
  end;
{$IFEND}
{$IFDEF USK004R}
  Config.WorkSide := Boolean(TRegComboBox(Pan.Item['WorkSide']).ItemIndex);
  // !07.11.2014
  // 0(Left) -> False,  1(Right) -> True
{$ENDIF}
  if ProgramProfile.AllowToChooseCoordSystem then
  begin
    Config.UnitsSystem := TUnitsSystem(BoxItemIndex('unitssystem'));
    RequestReboot := RequestReboot or
      (FCurUnitSys <> BoxItemIndex('unitssystem'));
  end;

  if ProgramProfile.UseAirBrush then
  begin
    Config.PaintSprayer := TPaintRegim(BoxItemIndex('paint_sprayer'));
    RequestReboot := RequestReboot or (FCurAirbrushSprayer <> BoxItemIndex
        ('paint_sprayer'));

    Config.PainterState := TPainterState(BoxItemIndex('painter'));
    RequestReboot := RequestReboot or (FCurAirbrushState <> BoxItemIndex
        ('painter'));

    Config.AirBrushConfig := TRegComboBox(Pan.Item['painter_config']).Value;
    RequestReboot := RequestReboot or (FCurAirbrushConfig <> TRegComboBox
        (Pan.Item['painter_config']).Value);
  end;

  Config.Save;

  if FTime <> TRegTime(Pan.Item['time']).Value then
    Self.SetSystemDateTime(Now, TRegTime(Pan.Item['time']).Value);
  if FDate <> TRegDate(Pan.Item['date']).Value then
    Self.SetSystemDateTime(TRegDate(Pan.Item['date']).Value, Now);
{$IFDEF Avikon14}
  RequestReboot := False;
{$ENDIF}
  if RequestReboot then
    General.OpenWindow('RebootConfirm')
  else
    General.OpenWindow('MAINMENU');
end;

procedure TAdjustSoftware.OnMouseTimer(Sender: TObject);
var
  D: integer;
begin
  if FMouseDown then
  begin
    D := Mouse.CursorPos.Y - OY;
    VerticalMoveAllControls(D);
    if Assigned(FMinTopControl) then
      SB.Position := -(FMinTopControl.Top - 7);
  end;
  OY := Mouse.CursorPos.Y;
end;

procedure TAdjustSoftware.OnVolumeChange(Sender: TObject);
begin
  SND.Volume := TRegTrackBar(Sender).Value;
  Config.SndVolume := SND.Volume;
end;

procedure TAdjustSoftware.Open;
begin
  inherited;
  TRegTime(Pan.Item['time']).Value := Now;
  FTime := TRegTime(Pan.Item['time']).Value;

  TRegDate(Pan.Item['date']).Value := Now;
  FDate := TRegDate(Pan.Item['time']).Value;
end;

procedure TAdjustSoftware.ResetChannelSettings(Sender: TObject);
var
  DefaultFile, VarFile: string;
  MS: TMemoryStream;
  flag: Boolean;
  Kind: TWinKind;

  procedure DeleteOldNastr(VarFile: String; N: integer);
  var
    sr: TSearchRec;
    FindRes, i: integer; // ���������� ��� ������ ���������� ������
    Path, Template: string;
  begin

    Path := ExtractFilePath(VarFile);
    Template := 'Var' + IntToStr(N);
    FindRes := FindFirst(Path + Template + '_*.kan', faAnyFile, sr);
    while FindRes = 0 do
    begin
      if (sr.Name <> '.') and (sr.Name <> '..') then
      begin
        DeleteFile(Path + sr.Name);
      end;
      FindRes := FindNext(sr);
    end;
    FindClose(sr);
  end;

begin
  KConf.DoNotSaveUserConfig := true;
  DefaultFile := ExtractFilePath(Application.ExeName) + 'NASTR\';
{$IFDEF USK004R}
  DefaultFile := DefaultFile + 'USK004R\';
  VarFile := DefaultFile;
  DefaultFile := DefaultFile + 'Default.kan';
  case Config.AdjustmentVariantNum of
    0:
      VarFile := VarFile + 'Var1.kan';
    1:
      VarFile := VarFile + 'Var2.kan';
    2:
      VarFile := VarFile + 'Var3.kan';
  end;
  flag := False;
  if FileExists(DefaultFile) then
  begin
    MS := TMemoryStream.Create;
    MS.LoadFromFile(DefaultFile);
    MS.SaveToFile(VarFile);
    MS.Free;
    flag := true;
    DeleteOldNastr(VarFile, Config.AdjustmentVariantNum + 1);
  end
  else
  begin
    flag := False;
    Kind := kBad;
    MessageForm.Display('��� ��������� ��������', Kind);
  end;
  if flag then
    General.OpenWindow('RebootConfirm');
{$ENDIF}
  //
{$IF DEFINED(EGO_USW)  }
  case Config.SoundedScheme of
    Scheme1:
      DefaultFile := DefaultFile + 'EGO_USW1\';
    Scheme2:
      DefaultFile := DefaultFile + 'EGO_USW2\';
    Scheme3:
      DefaultFile := DefaultFile + 'EGO_USW3\';
  end;
  VarFile := DefaultFile;
  DefaultFile := DefaultFile + 'Default.kan';
  case Config.AdjustmentVariantNum of
    0:
      VarFile := VarFile + 'Var1.kan';
    1:
      VarFile := VarFile + 'Var2.kan';
    2:
      VarFile := VarFile + 'Var3.kan';
  end;
  flag := False;
  if FileExists(DefaultFile) then
  begin
    MS := TMemoryStream.Create;
    MS.LoadFromFile(DefaultFile);
    MS.SaveToFile(VarFile);
    MS.Free;
    flag := true;
    DeleteOldNastr(VarFile, Config.AdjustmentVariantNum + 1);
  end
  else
  begin
    flag := False;
    Kind := kBad;
    MessageForm.Display('��� ��������� ��������', Kind);
  end;
  if flag then
    General.OpenWindow('RebootConfirm');
{$IFEND}
  //
{$IFDEF Avikon16_IPV}
  case Config.SoundedScheme of
    Scheme1:
      DefaultFile := DefaultFile + 'IPV_SCH1\';
    Scheme2:
      DefaultFile := DefaultFile + 'IPV_SCH2\';
    Wheels:
      DefaultFile := DefaultFile + 'IPV_W\';
    Scheme3:
      DefaultFile := DefaultFile + 'IPV_SCH3\';
  end;
  VarFile := DefaultFile;
  DefaultFile := DefaultFile + 'Default.kan';
  case Config.AdjustmentVariantNum of
    0:
      VarFile := VarFile + 'Var1.kan';
    1:
      VarFile := VarFile + 'Var2.kan';
    2:
      VarFile := VarFile + 'Var3.kan';
  end;
  flag := False;
  if FileExists(DefaultFile) then
  begin
    MS := TMemoryStream.Create;
    MS.LoadFromFile(DefaultFile);
    MS.SaveToFile(VarFile);
    MS.Free;
    flag := true;
  end
  else
  begin
    flag := False;
    Kind := kBad;
    MessageForm.Display('��� ��������� ��������', Kind);
  end;
  if flag then
    General.OpenWindow('RebootConfirm');
{$ENDIF}
  //
{$IFDEF VMT_US}
  DefaultFile := DefaultFile + 'VMT_US\';
  VarFile := DefaultFile;
  DefaultFile := DefaultFile + 'Default.kan';
  case Config.AdjustmentVariantNum of
    0:
      VarFile := VarFile + 'Var1.kan';
    1:
      VarFile := VarFile + 'Var2.kan';
    2:
      VarFile := VarFile + 'Var3.kan';
  end;
  flag := False;
  if FileExists(DefaultFile) then
  begin
    MS := TMemoryStream.Create;
    MS.LoadFromFile(DefaultFile);
    MS.SaveToFile(VarFile);
    MS.Free;
    flag := true;
    DeleteOldNastr(VarFile, Config.AdjustmentVariantNum + 1);
  end
  else
  begin
    flag := False;
    Kind := kBad;
    MessageForm.Display('��� ��������� ��������', Kind);
  end;
  if flag then
    General.OpenWindow('RebootConfirm');
{$ENDIF}
end;

procedure TAdjustSoftware.ScrollChange(Sender: TObject);
var
  D: integer;
begin
  D := -(SB.Position - OldPos);
  VerticalMoveAllControls(D);
  OldPos := SB.Position;
end;

procedure TAdjustSoftware.SetSystemDateTime(Date, Time: TDateTime);

  function SetPCSystemTime(tDati: TDateTime): Boolean;
  var
    tSetDati: TDateTime;
    vDatiBias: Variant;
    tTZI: TTimeZoneInformation;
    tST: TSystemTime;
  begin
    GetSystemTime(tST);
    SetPCSystemTime := SetSystemTime(tST);
  end;

var
  vsys, testSys: _SYSTEMTIME;
  TestH, TestMin, TestSec, TestMM: Word;
  vYear, vMonth, vDay, vHour, vMin, vSec, vMm: Word;
  TIME_ZONE: _TIME_ZONE_INFORMATION;

begin
  DecodeDate(Trunc(Date), vYear, vMonth, vDay);
  DecodeTime(Time, vHour, vMin, vSec, vMm);

  // ���������� �������� �� ������� ���� � ������-������ �����
  DecodeTime(Now(), TestH, TestMin, TestSec, TestMM);
  GetSystemTime(testSys);

  vMm := 0;
  vsys.wYear := vYear;
  vsys.wMonth := vMonth;
  vsys.wDay := vDay;
  vsys.wHour := (vHour - (TestH - testSys.wHour));
  vsys.wMinute := (vMin - (TestMin - testSys.wMinute));

  vsys.wSecond := vSec;
  vsys.wMilliseconds := vMm;
  vsys.wDayOfWeek := DayOfWeek(Trunc(Date));
  SetSystemTime(vsys);
end;

procedure TAdjustSoftware.VerticalMoveAllControls(Delta: integer);
var
  i, D: integer;
begin
  D := Delta;
  if Assigned(FMinTopControl) then
    if (FMinTopControl.Top + Delta) > 7 then
      if FMinTopControl.Top > 7 then
        Exit
      else
        D := 7 - FMinTopControl.Top;
  if Assigned(FMaxTopControl) then
    if (FMaxTopControl.Top + Delta - 1) < Pan.Height - 100 then
      if (FMaxTopControl.Top - 1) < Pan.Height - 100 then
        Exit
      else
        D := Pan.Height - 100 - FMaxTopControl.Top + 1;

  if Assigned(Pan) then
    for i := 0 to Pan.ControlCount - 1 do
      TCustomInputItem(Pan.Controls[i]).MoveVertical(D { Delta } );
end;

{ TMarkPK }

constructor TMarkPK.Create(Parent: TPaneledWindow);

begin
  inherited;

  ScrollTimer := TTimer.Create(Parent);
  ScrollTimer.Enabled := False;
  ScrollTimer.OnTimer := OnScrollTimer;

  CPanel := FParent.PanelManager.AddPanel
    (TControlPanel.Create('MarkPK', poVertical, caNone, 0));
  CPanel.BevelOuter := bvNone;

  ImgPK := TImage.Create(Parent);
  ImgPK.Picture.LoadFromFile(AddSlash(ExtractFilePath(Application.ExeName))
      + 'Pic\PK.wmf');
  CPanel.Add(ImgPK);
  ImgPK.Width := Round(Parent.Width * 0.3);
  ImgPK.Height := Round(Parent.Width * 0.3);
  ImgPK.Left := Round(TMarksWindow(Parent).FGP.Width * 0.5 - ImgPK.Width * 0.5);
  ImgPK.Top := Round(Parent.Height * 0.12);
  ImgPK.Stretch := true;

  PKL := ControlFactory.CreateLabel(Parent);
  ControlFactory.InitializeLabel(PKL, Round(Parent.Height * 0.1));
  CPanel.Add(PKL);
  PKL.Left := Round(Parent.Width * 0.29);
  PKL.Top := Round(Parent.Height * 0.19);

  PKR := ControlFactory.CreateLabel(Parent);
  ControlFactory.InitializeLabel(PKR, Round(Parent.Height * 0.1));
  CPanel.Add(PKR);
  PKR.Left := Round(Parent.Width * 0.346);
  PKR.Top := Round(Parent.Height * 0.19);

  ImgKM := TImage.Create(Parent);
  ImgKM.Picture.LoadFromFile(AddSlash(ExtractFilePath(Application.ExeName))
      + 'Pic\KM.wmf');
  CPanel.Add(ImgKM);
  ImgKM.Width := Round(Parent.Width * 0.3);
  ImgKM.Height := Round(Parent.Width * 0.3);
  ImgKM.Left := Round(TMarksWindow(Parent).FGP.Width * 0.5 - ImgKM.Width * 0.5);
  ImgKM.Top := Round(Parent.Height * 0.12);
  ImgKM.Stretch := true;
  ImgKM.Visible := False;

  KML := ControlFactory.CreateLabel(Parent);
  ControlFactory.InitializeLabel(KML, Round(Parent.Height * 0.11));
  CPanel.Add(KML);
  KML.Top := Round(Parent.Height * 0.2);
  KML.Visible := False;

  KMR := ControlFactory.CreateLabel(Parent);
  ControlFactory.InitializeLabel(KMR, Round(Parent.Height * 0.11));
  CPanel.Add(KMR);
  KMR.Top := Round(Parent.Height * 0.2);
  KMR.Visible := False;

  KM := ControlFactory.CreateLabel(Parent);
  ControlFactory.InitializeLabel(KM, Round(Parent.Height * 0.1));
  CPanel.Add(KM);
  KM.Top := Round(Parent.Height * 0.0);

  BtnLess := ControlFactory.CreateButton(Parent);
  BtnLess.Caption := Parent.FLT['����������'];
  BtnLess.Left := Round(Parent.Width * 0.1);
  BtnLess.Top := Round(Parent.Height * 0.52);
  BtnLess.Width := Round(Parent.Width * 0.2);
  BtnLess.Height := Round(Parent.Height * 0.15);
  BtnLess.OnClick := OnPressPrev;
  BtnLess.OnMouseDown := OnKeyDown;
  BtnLess.OnMouseUp := OnKeyUp;

  CPanel.Add(BtnLess);

  BtnMore := ControlFactory.CreateButton(Parent);
  BtnMore.Caption := Parent.FLT['���������'];
  BtnMore.Width := Round(Parent.Width * 0.2);
  BtnMore.Height := Round(Parent.Height * 0.15);
  BtnMore.Left := Round(TMarksWindow(Parent).FGP.Width - Parent.Width * 0.1 -
      BtnMore.Width);
  BtnMore.Top := Round(Parent.Height * 0.52);
  BtnMore.OnClick := OnPressNext;
  BtnMore.OnMouseDown := OnKeyDown;
  BtnMore.OnMouseUp := OnKeyUp;

  CPanel.Add(BtnMore);

  BtnKmLess := ControlFactory.CreateButton(Parent);
  BtnKmLess.Caption := '-'; // Parent.FLT['����������'];
  BtnKmLess.Left := KM.Left + Round(Parent.Width * 0.22);
  BtnKmLess.Top := KM.Top + Round(Parent.Height * 0.20);
  BtnKmLess.Width := Round(Parent.Width * 0.07);
  BtnKmLess.Height := Round(Parent.Height * 0.07);
  BtnKmLess.OnClick := OnKmLess;
  BtnKmLess.OnMouseDown := OnKeyDown;
  BtnKmLess.OnMouseUp := OnKeyUp;
  BtnKmLess.Tag := 1;

  CPanel.Add(BtnKmLess);

  BtnKmMore := ControlFactory.CreateButton(Parent);
  BtnKmMore.Caption := '+'; // Parent.FLT['���������'];
  BtnKmMore.Left := KM.Left + Round(Parent.Width * 0.22);
  BtnKmMore.Top := KM.Top + Round(Parent.Height * 0.10);
  BtnKmMore.Width := Round(Parent.Width * 0.07);
  BtnKmMore.Height := Round(Parent.Height * 0.07);
  BtnKmMore.OnClick := OnKmMore;
  BtnKmMore.OnMouseDown := OnKeyDown;
  BtnKmMore.OnMouseUp := OnKeyUp;
  BtnKmMore.Tag := 2;

  CPanel.Add(BtnKmMore);

  // >>>06-2015
  BtnPkLess := ControlFactory.CreateButton(Parent);
  BtnPkLess.Caption := '-'; // Parent.FLT['����������'];
  BtnPkLess.Left := KM.Left + Round(Parent.Width * 0.32);
  BtnPkLess.Top := KM.Top + Round(Parent.Height * 0.20);
  BtnPkLess.Width := Round(Parent.Width * 0.07);
  BtnPkLess.Height := Round(Parent.Height * 0.07);
  BtnPkLess.OnClick := OnPkLess;
  BtnPkLess.OnMouseDown := OnKeyDown;
  BtnPkLess.OnMouseUp := OnKeyUp;
  BtnPkLess.Tag := 3;

  CPanel.Add(BtnPkLess);

  BtnPkMore := ControlFactory.CreateButton(Parent);
  BtnPkMore.Caption := '+'; // Parent.FLT['���������'];
  BtnPkMore.Left := KM.Left + Round(Parent.Width * 0.32);
  BtnPkMore.Top := KM.Top + Round(Parent.Height * 0.10);
  BtnPkMore.Width := Round(Parent.Width * 0.07);
  BtnPkMore.Height := Round(Parent.Height * 0.07);
  BtnPkMore.OnClick := OnPkMore;
  BtnPkMore.OnMouseDown := OnKeyDown;
  BtnPkMore.OnMouseUp := OnKeyUp;
  BtnPkMore.Tag := 4;

  CPanel.Add(BtnPkMore);
  // <<<06-2015

  BtnMetrLess := ControlFactory.CreateButton(Parent);
  BtnMetrLess.Caption := '-'; // Parent.FLT['����������'];
  BtnMetrLess.Left := KM.Left + Round(Parent.Width * 0.40 { 0.38 } );
  BtnMetrLess.Top := KM.Top + Round(Parent.Height * 0.20);
  BtnMetrLess.Width := Round(Parent.Width * 0.07);
  BtnMetrLess.Height := Round(Parent.Height * 0.07);
  BtnMetrLess.OnClick := OnMetrLess;
  BtnMetrLess.OnMouseDown := OnKeyDown;
  BtnMetrLess.OnMouseUp := OnKeyUp;
  BtnMetrLess.Tag := 5; // 3

  CPanel.Add(BtnMetrLess);

  BtnMetrMore := ControlFactory.CreateButton(Parent);
  BtnMetrMore.Caption := '+'; // Parent.FLT['���������'];
  BtnMetrMore.Left := KM.Left + Round(Parent.Width * 0.40 { 0.38 } );
  BtnMetrMore.Top := KM.Top + Round(Parent.Height * 0.10);
  BtnMetrMore.Width := Round(Parent.Width * 0.07);
  BtnMetrMore.Height := Round(Parent.Height * 0.07);
  BtnMetrMore.OnClick := OnMetrMore;
  BtnMetrMore.OnMouseDown := OnKeyDown;
  BtnMetrMore.OnMouseUp := OnKeyUp;
  BtnMetrMore.Tag := 6; // 4

  CPanel.Add(BtnMetrMore);
end;

procedure TMarkPK.Refresh;
var
  s: string;
  isStolb: Boolean;
begin
  if Config.UnitsSystem = usRus then
  begin
    isStolb := true;
    if CurrentKML = CurrentKMR then
    begin
      PKL.Visible := true;
      PKR.Visible := true;
      ImgPK.Visible := true;
      ImgKM.Visible := False;
      KMR.Visible := False;
      KML.Visible := False;
      KM.Caption := 'KM ' + IntToStr(CurrentKML);
      KM.Left := Round(TMarksWindow(FParent).FGP.Width * 0.5 - KM.Width * 0.5);
      s := IntToStr(CurrentPKL);
      PKL.Caption := s[Length(s)];
      s := IntToStr(CurrentPKR);
      PKR.Caption := s[Length(s)];
    end
    else
    begin
      PKL.Visible := False;
      PKR.Visible := False;
      ImgPK.Visible := False;
      ImgKM.Visible := true;
      KMR.Visible := true;
      KML.Visible := true;
      KM.Caption := 'KM ';
      KM.Left := Round(TMarksWindow(FParent).FGP.Width * 0.5 - KM.Width * 0.5);
      KML.Caption := IntToStr(CurrentKML);
      KMR.Caption := IntToStr(CurrentKMR);
      KML.Left := Round(FParent.Width * 0.26 - KML.Width * 0.5);
      KMR.Left := Round(FParent.Width * 0.41 - KML.Width * 0.5);
    end;
  end
  else
  begin
    PKL.Visible := False;
    PKR.Visible := False;
    ImgKM.Visible := False;
    KMR.Visible := False;
    KML.Visible := False;
    // {$IFDEF USK004R}
{$IFDEF TURKISH_COORD_WITH_DOT}
    KM.Caption := AviconTypes.CaCrdToStr(TCoordSys(Ord(usMetricB)), FChainage);
{$ELSE}
    KM.Caption := AviconTypes.CaCrdToStr(TCoordSys(Ord(Config.UnitsSystem)),
      FChainage);
{$ENDIF}
    KM.Left := Round(TMarksWindow(FParent).FGP.Width * 0.5 - KM.Width * 0.5);

    if Config.UnitsSystem = usMetricTurkish then
      isStolb := False
    else
      isStolb := true;
  end;

  ImgPK.Visible := isStolb;
  BtnLess.Visible := isStolb;
  BtnMore.Visible := isStolb;
  BtnKmLess.Visible := not isStolb;
  BtnKmMore.Visible := not isStolb;
  BtnMetrLess.Visible := not isStolb;
  BtnMetrMore.Visible := not isStolb;

end;

procedure TMarkPK.SetMark;
begin
  Inherited;
  if Config.UnitsSystem = usRus then
  begin
    /// ������ !!!! �������� ���� !!
    if CurrentKML < CurrentKMR then
      General.Core.Registrator.AddMRFPost(CurrentKML, CurrentKMR, 10, 1)
    else if CurrentKML > CurrentKMR then
      General.Core.Registrator.AddMRFPost(CurrentKML, CurrentKMR, 1, 10)
    else
      General.Core.Registrator.AddMRFPost(CurrentKML, CurrentKMR, CurrentPKL,
        CurrentPKR);
  end
  else
  begin
    General.Core.Registrator.AddCaPost(FChainage);
  end;
end;

procedure TMarkPK.DecStolb;
var
  Dir: integer;
begin
  if Config.UnitsSystem = usRus then
  begin
    if CurrentKML = CurrentKMR then
    begin
      if (Min(CurrentPKL, CurrentPKR) = 1) and
        (Min(CurrentKML, CurrentKMR) > 0) then
      begin
        // ��������� � �� ������.
        if D = 1 then
          Dec(CurrentKML)
        else
          Dec(CurrentKMR);
      end
      else
      begin
        // ���������� ������.
        Dec(CurrentPKL);
        CurrentPKR := CurrentPKL + D;
      end;
    end
    else
    begin
      // ��������� � �������� �������.
      CurrentKML := Min(CurrentKML, CurrentKMR);
      CurrentKMR := CurrentKML;
      if D = 1 then
      begin
        CurrentPKL := 9;
        CurrentPKR := 10;
      end
      else
      begin
        CurrentPKL := 10;
        CurrentPKR := 9;
      end;
    end;
  end
  else
  begin
    if General.Core.MovingForward then
    begin
      Dir := 1;
      FChainage.XXX := FChainage.XXX - 1;
    end
    else
    begin
      FChainage.XXX := FChainage.XXX + 1;
      Dir := -1;
    end;
  end;

  Refresh;
end;

procedure TMarkPK.IncStolb;
var
  Dir: integer;
begin
  if Config.UnitsSystem = usRus then
  begin
    if CurrentKML = CurrentKMR then
    begin
      if Max(CurrentPKL, CurrentPKR) = 10 then
      begin
        // ��������� � �� ������.
        if D = 1 then
          Inc(CurrentKMR)
        else
          Inc(CurrentKML);
      end
      else
      begin
        Inc(CurrentPKL); // ���������� ������.
        CurrentPKR := CurrentPKL + D;
      end;
    end
    else
    begin
      // ��������� � �������� �������.
      CurrentKML := Max(CurrentKML, CurrentKMR);
      CurrentKMR := CurrentKML;
      if D = 1 then
      begin
        CurrentPKL := 1;
        CurrentPKR := 2;
      end
      else
      begin
        CurrentPKL := 2;
        CurrentPKR := 1;
      end;
    end;
  end
  else
  begin
    if General.Core.MovingForward then
      Dir := 1
    else
      Dir := -1;
    FChainage := AviconTypes.GetNextCaPost(TCoordSys(Ord(Config.UnitsSystem)),
      FChainage, Dir);
  end;
  Refresh;
end;

procedure TMarkPK.Initialize(MRFCoord_: TMRFCrd);
begin
  FChainage.XXX := 0;
  FChainage.YYY := 0;
  if General.Core.MovingForward then
    D := 1
  else
    D := -1;
  if General.Core.MovingForward then
  begin
    if MRFCoord_.Pk = 10 then
    begin
      // 10 ��.
      CurrentKML := MRFCoord_.KM;
      CurrentKMR := MRFCoord_.KM + 1;
    end
    else
    begin
      // 1..9 ��
      CurrentPKL := MRFCoord_.Pk;
      CurrentPKR := MRFCoord_.Pk + 1;
      CurrentKML := MRFCoord_.KM;
      CurrentKMR := MRFCoord_.KM;
    end;
  end
  else
  begin
    if MRFCoord_.Pk = 1 then
    begin
      // 1 ��.
      CurrentKML := MRFCoord_.KM;
      CurrentKMR := MRFCoord_.KM - 1;
    end
    else
    begin
      // 2..10 ��
      CurrentPKL := MRFCoord_.Pk;
      CurrentPKR := MRFCoord_.Pk - 1;
      CurrentKML := MRFCoord_.KM;
      CurrentKMR := MRFCoord_.KM;
    end;
  end;
  Refresh;
end;

procedure TMarkPK.Initialize(CaCoord_: TCaCrd);
var
  c: integer;
begin
  FChainage := CaCoord_;
  IncStolb;
  Refresh;
end;

procedure TMarkPK.OnPressNext(Sender: TObject);
begin
  if Config.UnitsSystem <> usRus then
    IncStolb
  else if General.Core.MovingForward then
    IncStolb
  else
    DecStolb;
end;

procedure TMarkPK.OnPressPrev(Sender: TObject);
begin
  if Config.UnitsSystem <> usRus then
    DecStolb
  else if General.Core.MovingForward then
    DecStolb
  else
    IncStolb;
end;

procedure TMarkPK.OnKmLess(Sender: TObject);
begin
  FChainage.XXX := FChainage.XXX - 10;
  Refresh;
end;

procedure TMarkPK.OnKmMore(Sender: TObject);
begin
  FChainage.XXX := FChainage.XXX + 10;
  Refresh;
end;

procedure TMarkPK.OnPkLess(Sender: TObject);
begin
  FChainage.XXX := FChainage.XXX - 1;
  Refresh;
end;

procedure TMarkPK.OnPkMore(Sender: TObject);
begin
  FChainage.XXX := FChainage.XXX + 1;
  Refresh;
end;

procedure TMarkPK.OnMetrLess(Sender: TObject);
begin
  FChainage.YYY := FChainage.YYY - 10;
  if FChainage.YYY < 0 then
  begin
    FChainage.XXX := FChainage.XXX - 1;
    FChainage.YYY := FChainage.YYY + 1000;
  end;
  Refresh;
end;

procedure TMarkPK.OnMetrMore(Sender: TObject);
begin
  FChainage.YYY := FChainage.YYY + 10;
  if FChainage.YYY >= 1000 then
  begin
    FChainage.XXX := FChainage.XXX + 1;
    FChainage.YYY := FChainage.YYY - 1000;
  end;
  Refresh;
end;

procedure TMarkPK.OnKeyDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  BntLessPressed := Sender = BtnLess;
  ScrollTimer.Enabled := true;
  ScrollTimer.Interval := 500;
  BtnTag := TButton(Sender).Tag;
end;

procedure TMarkPK.OnKeyUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  ScrollTimer.Enabled := False;
end;

procedure TMarkPK.OnScrollTimer(Sender: TObject);
begin
  if ScrollTimer.Interval > 400 then
    ScrollTimer.Interval := 100
  else if ScrollTimer.Interval > 10 then
    ScrollTimer.Interval := ScrollTimer.Interval - 1;

  if Config.UnitsSystem = usMetricTurkish then
    case BtnTag of
      1:
        OnKmLess(Sender);
      2:
        OnKmMore(Sender);
      3:
        OnMetrLess(Sender);
      4:
        OnMetrMore(Sender);
    end
  else if BntLessPressed then
    OnPressPrev(Sender)
  else
    OnPressNext(Sender);
end;

procedure TMarkPK.SetActive;
begin
  inherited;
  TMarksWindow(FParent).FGP.Caption := FParent.FLT['������� ������'];
end;

{ TMarkDefect }

constructor TMarkDefect.Create(Parent: TPaneledWindow);
var
  But: TButton;

begin
  inherited;
{$IFDEF USK004R}
  CPanel := FParent.PanelManager.AddPanel
    (TControlPanel.Create('MarkDef', poVertical, caTop, 10 { !7! 10  12 } ));
  CPanel.BevelOuter := bvNone;
  // CPanel.Align:=alClient;
  // CPanel.AutoSize:=True;
  // CPanel.Parent.Realign;
  // CPanel.Realign;
  // CPanel.Parent.Repaint;
  // CPanel.Repaint;
  CPanel.Height := Round(CPanel.Parent.Height * 0.9);
  CPanel.Width := Round(CPanel.Parent.Width * 0.9);
  // Application.ProcessMessages;

  DefCodeBut := TRegButton.Create
    ('DefList', CPanel, FParent.FLT['��� �������']);
  CPanel.Add(DefCodeBut);
  DefCodeBut.OnClick := ShowDefListWin;
  DefCodeBut.SetTextBeforeButton(FParent.FLT.Caption['��� �������'] + ': ');
  DefCodeBut.Caption := FParent.FLT['Select code'];

  IsFailure := TRegComboBox.Create('IsFail', CPanel, Parent.FLT['Is Failure']);
  // ���� ���������� ������ ������� �����, �� ��� ���� ������ ����� �������� TRUE. ������� ����� ������� ����� ������, ������� �������� ����������� ��������
  IsFailure.Add(Parent.FLT['False']);
  IsFailure.Add(Parent.FLT['True']);
  CPanel.Add(IsFailure);
  IsFailure.ItemIndex := 0;
  // IsFailure.ObjLeft := Round(DefCodeBut.Width * 0.65);
  // IsFailure.ObjWidth := Round(TMarksWindow(FParent).FGP.Width * 0.31);
  IsFailure.ObjLeft := Round(DefCodeBut.Width * 0.60);
  IsFailure.ObjWidth := Round(TMarksWindow(FParent).FGP.Width * 0.36);

  Size := TRegNumber.Create('Size', CPanel);
  CPanel.Add(Size);
  Size.Caption := FParent.FLT.Caption['Size'] + ' (mm): ';

  Size2 := TRegNumber.Create('Size2', CPanel);
  CPanel.Add(Size2);
  Size2.Caption := FParent.FLT.Caption['Size'] + '2 (mm): ';

  Size3 := TRegNumber.Create('Size3', CPanel);
  CPanel.Add(Size3);
  Size3.Caption := FParent.FLT.Caption['Size'] + '3 (mm): ';

  Source := TRegComboBox.Create('Source', CPanel, Parent.FLT['Source']);
  Source.Add(Parent.FLT['Ultrasonic Trolley']);
  Source.Add(Parent.FLT['Visual Inspection']);
  CPanel.Add(Source);
  Source.ItemIndex := 0;
  Source.ObjLeft := Round(DefCodeBut.Width * 0.60);
  Source.ObjWidth := Round(TMarksWindow(FParent).FGP.Width * 0.36);

  Status := TRegComboBox.Create('Status', CPanel, Parent.FLT['Status']);
  Status.Add(Parent.FLT['Open']);
  Status.Add(Parent.FLT['Closed']);
  CPanel.Add(Status);
  Status.ItemIndex := 0;
  Status.ObjLeft := Round(DefCodeBut.Width * 0.60);
  Status.ObjWidth := Round(TMarksWindow(FParent).FGP.Width * 0.36);

  AssetType := TRegComboBox.Create
    ('AssetType', CPanel, Parent.FLT['Asset Type']);
  AssetType.Add(Parent.FLT['Left Rail']);
  AssetType.Add(Parent.FLT['Right Rail']);
  CPanel.Add(AssetType);
  AssetType.ItemIndex := 0;
  AssetType.ObjLeft := Round(DefCodeBut.Width * 0.60);
  AssetType.ObjWidth := Round(TMarksWindow(FParent).FGP.Width * 0.36);

  { Label1:=TLabel.Create(CPanel);
    CPanel.Add(Label1);
    Label1.Caption:=FParent.FLT.Caption['Comments'];
    }
  Comments := TEdit.Create(CPanel); // ����������
  CPanel.Add(Comments);
  // Comments.Text:='Hello world';
  // Comments.Caption := FParent.FLT.Caption['Comments'];

  // !!  Keyb := TTouchKeyboard.Create(CPanel);  � Panasinice ��� ���� ����������� ����������
  // !!  CPanel.Add(Keyb);


  // Keyb.Height:=round(CPanel.Height*0.35);
  // keyb.EnableAlign;
  // keyb.Align:=alClient;
  // keyb.Height:=CPanel.Height-keyb.Top;
  // keyb.ScaleBy(2,5);
{$ELSE}
  CPanel := FParent.PanelManager.AddPanel
    (TControlPanel.Create('MarkDef', poVertical, caTop, 10 { 12 } ));
  CPanel.BevelOuter := bvNone;

  DefCodeBut := TRegButton.Create
    ('DefList', CPanel, FParent.FLT['��� �������']);
  CPanel.Add(DefCodeBut);
  DefCodeBut.OnClick := ShowDefListWin;
  DefCodeBut.SetTextBeforeButton(FParent.FLT.Caption['��� �������'] + ': ');
  DefCodeBut.Caption := FParent.FLT['Select code'];

  Rail := TRegComboBox.Create('def', CPanel, Parent.FLT['����']);
  Rail.Add(Parent.FLT['�����']);
  Rail.Add(Parent.FLT['������']);
  CPanel.Add(Rail);
  Rail.ItemIndex := 0;
  Rail.ObjLeft := Round(DefCodeBut.Width * 0.65);
  Rail.ObjWidth := Round(TMarksWindow(FParent).FGP.Width * 0.31);
{$ENDIF}
{$IF DEFINED(Avikon16_IPV) OR DEFINED(EGO_USW) OR DEFINED(VMT_US) }
  But := ControlFactory.CreateButton(CPanel);
  But.Parent := CPanel;
  But.Width := CPanel.Width;
  But.Left := 0;
  But.Height := Round(FParent.Height * ButH);
  But.Top := CPanel.Height - But.Height * 2 - 5;
  But.Font.Height := Round(FParent.Height * ButFontH);
  But.Font.Name := DefaultFontName;
  But.Font.Color := FParent.FCT.Color['BootWin_ButtonText'];
  But.Caption := FParent.FLT.Caption['�����������'];
  But.Visible := true;
  But.OnClick := OnKraskopult;
  if not ProgramProfile.UseAirBrush then
    But.Visible := False;
{$IFEND}
end;

procedure TMarkDefect.Initialize(MRFCoord_: TMRFCrd);
begin
{$IFDEF USK004R}
  // Comments.Text:='';
{$ENDIF}
end;

procedure TMarkDefect.Initialize(CaCoord_: TCaCrd);
begin
{$IFDEF USK004R}
  // Comments.Text:='';
{$ENDIF}
end;

procedure TMarkDefect.OnDefectSelected(id: string);
begin
  DefCodeBut.Caption := id;
  DefCodeBut.RePaint;
  DefCodeBut.SetFocus;
  General.OpenWindow('PLACEMARK');
  DefCodeBut.RePaint;
end;

procedure TMarkDefect.OnKraskopult(Sender: TObject);
begin
  General.Core.RunKraskopult(Rail.ItemIndex); // 0-Left, 1-Right
end;

procedure TMarkDefect.SetActive;
begin
  inherited;
  TMarksWindow(FParent).FGP.Caption := FParent.FLT['������� �������'];
end;

procedure TMarkDefect.SetMark;
var
  DefCode: string;
  SingleSize: Single;
  SingleSize2: Single;
  SingleSize3: Single;
  AssetType1: TRail;
  SourceType: TSourceType;
  Status2: TNStatus;
  DataContainer: TAviconDataContainer;
  s: string;
begin
  Inherited;
  // ����� �������� ���� !!!!!
{$IFDEF USK004R}
  General.Core.Registrator.AddDefLabel(TRail(Config.LeftSide),
    DefCodeBut.Caption);
  { procedure AddLine(DS: TAviconDataSource;
    StDisCoord, EdDisCoord: Integer;
    CodeDefect: Integer;
    IsFailure: Boolean;
    Size, Size2, Size3: Single; UseSize, UseSize2, UseSize3: Boolean;
    CommentText: string); overload;
    }

  if (General.Core.RegistrationStatus = rsOn)
    and General.Core.FRegOnUserLevel then
  begin
    // try
    // DefCode := StrToInt(DefCodeBut.Caption); // Integer;                    // ��� ������� �� UIC 712
    // except
    // DefCode := 0;
    // end;
    DefCode := DefCodeBut.Caption;
    SingleSize := Size.Value;
    SingleSize2 := Size2.Value;
    SingleSize3 := Size3.Value;
    if Source.ItemIndex = 1 then
      SourceType := stVisualInspection
    else
      SourceType := stUltrasonicTrolley;
    if Status.ItemIndex = 1 then
      Status2 := nsClosed
    else
      Status2 := nsOpen;
    if AssetType.ItemIndex = 1 then
      AssetType1 := rRight
    else
      { if Source.ItemIndex=0 then } AssetType1 := rLeft;
    // else Source1:=rNotSet;

    /// /  TSourceType = (stUltrasonicTrolley, stVisualInspection); // ����� ����������� �������
    /// /  TNStatus = (nsOpen, nsClosed); // ��� ���� ���������, ������ �� ������ (�.�. ������ �� ����)
    /// /  TRail = (rLeft, rRight, rNotSet);

    { procedure AddLine(DC: TAviconDataContainer;
      StDisCoord, EdDisCoord: Integer;
      Lat, Lon: Single;
      CodeDefect: Integer;
      SourceType: TSourceType;
      IsFailure: Boolean;
      Size, Size2, Size3: Single; UseSize, UseSize2, UseSize3: Boolean;
      CommentText: string); overload;
      }
    { procedure TAviconDataContainer.AddNORDCODate(StDisCoord, EdDisCoord: Integer;
      Lat, Lon: Single;
      AssetType: TRail;
      CodeDefect: string;
      SourceType: TSourceType;
      Status: TNStatus;
      IsFailure: Boolean;
      Size, Size2, Size3: Single; UseSize, UseSize2, UseSize3: Boolean;
      CommentText: string);
      }
    DataContainer := General.Core.Registrator;
    DataContainer.AddNORDCODate(General.Core.Registrator.MaxDisCoord,
      General.Core.Registrator.MaxDisCoord, General.GPSM.Latitude,
      General.GPSM.Longitude, AssetType1, DefCode, SourceType, Status2, Boolean
        (Self.IsFailure.ItemIndex), SingleSize, SingleSize2, SingleSize3,
      Boolean(Size.Value > 0), Boolean(Size2.Value > 0), Boolean
        (Size3.Value > 0), Comments.Text);

    { General.Core.FNordcoCSVFile.AddLine(General.Core.Registrator,
      General.Core.Registrator.MaxDisCoord,
      General.Core.Registrator.MaxDisCoord, General.GPSM.Latitude,
      General.GPSM.Longitude, DefCode, stUltrasonicTrolley, Boolean
      (Self.IsFailure.ItemIndex), SingleSize, SingleSize2, SingleSize3,
      Boolean(Size.Value > 0), Boolean(Size2.Value > 0), Boolean
      (Size3.Value > 0), Comments.Text);
      }
    (*
      with General.Core.RecNordcoCSV do begin
      //    Line              :=RegParamList.PeregonName;                                 // �������� ��� ������������� �������������� �����
      //    Track             :=IntToStr(RegParamList.TrackNumber);                       // ������������� ����, � ������� ��������� ������
      LocationFrom      := General.Core.Registrator.MaxDisCoord; {Single;}                                                   // ������ ������������� �������
      LocationTo        := LocationFrom;                                                     // ����� ������������� �������. � ������ ��������� ������� �� �� �����, ��� ����  "Location From"
      LatitudeFrom      := GPSM.Latitude;                                             // GPS-������ ��� ���� "Location From"
      LatitudeTo        := GPSM.Latitude;                                             // GPS-������ ��� ���� "Location To"
      LongitudeFrom     := GPSM.Longitude;                                            // GPS-������� ��� ���� "Location From"
      LongitudeTo       := GPSM.Longitude;                                            // GPS-������� ��� ���� "Location To"
      try
      Type_             := StrToInt(DefCodeBut.Caption);//Integer;                    // ��� ������� �� UIC 712
      except Type_:=0;
      SurveyDate        := Now; //TDateTime;                                          // ���� ����������� �������
      //    Source            :='Ultrasonic Trolley';                                     // ����� ����������� ������� - �� ������: Ultrasonic Trolley, Visual Inspection
      //    Status            :='Open';                                                   // ��� ���� ���������, ������ �� ������ (�.�. ������ �� ����) - �� ������: Open, Closed
      IsFailure         : Boolean;                                                    // ���� ���������� ������ ������� �����, �� ��� ���� ������ ����� �������� TRUE. ������� ����� ������� ����� ������, ������� �������� ����������� ��������
      //    if RegParamList.LeftSide=0 then AssetType:='Left Rail'
      //    else AssetType:='Right Rail';                                                 // ��� �������� �������� ���� - �� ������: Left Rail, Right Rail
      //    DataEntryDate     :=Now; {TDateTime;}                                         // ���� ����������� ������ ��������
      Size              : Single;                                                     // ������ ������� ���  ����������� �������. ��������, ���� ������ ��� �������, �� �� �����.
      Size2             : Single;                                                     // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
      Size3             : Single;                                                     // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
      //    UnitofMeasurement :='mm';                                                     // ��. ��������� ���� "Size"
      //    UnitofMeasurement2:='mm';                                                     // ��. ��������� ���� "Size2"
      //    UnitofMeasurement3:='mm';                                                     // ��. ��������� ���� "Size3"
      Comment           : string;                                                     // ����������
      //    Operator_    :=RegParamList.Operator;                                         // ��������
      //    Device       :='USK-004R';                                                    // ������
      end;
      General.Core.FNordcoCSVFile.AddLine( General.Core.RecNordcoCSV );
      *)
  end;
{$ELSE}
  General.Core.Registrator.AddDefLabel
    (TRail(Rail.ItemIndex), DefCodeBut.Caption);
{$ENDIF}
end;

procedure TMarkDefect.ShowDefListWin(Sender: TObject);
var
  Win: TListWindow;
  SL: TStringList;
  i: integer;
  fname: String;
begin
  Win := General.GetListWindow;
  Win.Clear;
  Win.ListHeader := FParent.FLT.Caption['Select code'];
  Win.OnItemSelected := OnDefectSelected;
  Win.WinToExit := 'PLACEMARK';
  Win.EnableInputStr := False;
  Win.EnableInputStr := true; // ����� �������� �� ������ �����

  SL := TStringList.Create;
  fname := ExtractFilePath(Application.ExeName) + 'NASTR\DefCodes.txt';
  SL.LoadFromFile(fname);
  for i := 0 to SL.Count - 1 do
    Win.AddItem(SL.Strings[i], SL.Strings[i]);
  SL.Free;

  // {$IFDEF INPUTSTR_IN_LISTWIN}
  // Win.FileName:=fname;
  // {$ENDIF}
  General.OpenWindow('ListWin');
end;

// procedure TMarkDefect.OnKeyPressed(CharCode:word);
// begin
// end;

{ TMarkMode }

procedure TMarkMode.Initialize(MRFCoord: TMRFCrd);
begin
end;

procedure TMarkMode.Initialize(CaCoord: TCaCrd);
begin
end;

procedure TMarkMode.SetMark;
begin
  if Assigned(General.Core) then
    if Assigned(General.Core.Registrator) then
      if General.Core.RegistrationStatus = rsOn then
        General.Core.Registrator.AddTime(Frac(Now));
end;

{ TMarkText }

constructor TMarkText.Create(Parent: TPaneledWindow);
begin
  inherited;
  CPanel := FParent.PanelManager.AddPanel
    (TControlPanel.Create('MarkText', poVertical, caTop, 12));
  CPanel.BevelOuter := bvNone;
  TxtBut := TRegButton.Create('TxtList', CPanel, FParent.FLT['�������']);
  CPanel.Add(TxtBut);
  TxtBut.OnClick := ShowTxtListWin;
  TxtBut.SetTextBeforeButton(FParent.FLT.Caption['�������'] + ': ');
  TxtBut.Caption := FParent.FLT['Select mark'];
end;

procedure TMarkText.OnTxtSelected(id: string);
begin
  TxtBut.Caption := id;
  TxtBut.RePaint;
  TxtBut.SetFocus;
  General.OpenWindow('PLACEMARK');
  TxtBut.RePaint;
end;

procedure TMarkText.SetActive;
begin
  inherited;
  TMarksWindow(FParent).FGP.Caption := FParent.FLT['������ �������'];
end;

procedure TMarkText.SetMark;
begin
  Inherited;
  General.Core.Registrator.AddTextLabel(TxtBut.Caption);
end;

procedure TMarkText.ShowTxtListWin(Sender: TObject);
var
  Win: TListWindow;
  SL: TStringList;
  i: integer;
  fname: String;
begin
  Win := General.GetListWindow;
  Win.Clear;
  Win.ListHeader := FParent.FLT.Caption['Select mark'];
  Win.OnItemSelected := OnTxtSelected;
  Win.WinToExit := 'PLACEMARK';
  Win.EnableInputStr := true;

  SL := TStringList.Create;
  fname := ExtractFilePath(Application.ExeName) + 'NASTR\TextMarks.txt';
  SL.LoadFromFile(fname);
  for i := 0 to SL.Count - 1 do
    Win.AddItem(SL.Strings[i], SL.Strings[i]);
  SL.Free;
{$IFDEF INPUTSTR_IN_LISTWIN}
  Win.FileName := fname;
{$ENDIF}
  General.OpenWindow('ListWin');
end;

{ TExitConfirmWindow }

constructor TExitConfirmWindow.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);
begin
  inherited;
  Self.Caption := '';

  FTimer.Enabled := False;

  FLabel := TLabel.Create(Self);
  FLabel.Parent := Self;
  FLabel.Visible := true;
  FLabel.Caption := FLT.Caption['������������� ����� ?'];
  FLabel.Font.Color := clBlack;
  FLabel.Font.Name := DefaultFontName;
  FLabel.Width := Self.Width;
  FLabel.Alignment := taCenter;
  FLabel.Top := 110;
  FLabel.Font.Height := Round(0.08 * Self.Height);

  FFootPanel := TPanel.Create(Self);
  FFootPanel.ParentColor := False;
  FFootPanel.Parent := Self;
  FFootPanel.ParentBackground := False;
  FFootPanel.Visible := true;
  FFootPanel.Caption := '';
  FFootPanel.Color := Self.Color;
  FFootPanel.BevelOuter := bvNone;
  FFootPanel.BorderWidth := 0;
  FFootPanel.BorderStyle := bsNone;
  FFootPanel.Ctl3D := False;
  FFootPanel.Align := alBottom;
  FFootPanel.Height := Round(0.2 * Self.Height);

  FButPanel := TControlPanelManager.Create(FFootPanel, FCT, FLT);
  FButPanel.Color := Self.Color;

  FButPanel.AddPanel(TButtonPanel.Create('Confirm', poHorizontal, caWholeSize,
      45));
  FButPanel.ButtonPanel['Confirm'].AddButton(FLT.Caption['��'], OnYes);
  FButPanel.ButtonPanel['Confirm'].AddButton(FLT.Caption['���'], OnNo);

end;

procedure TExitConfirmWindow.OnNo(Sender: TObject);
begin
  General.OpenWindow('MAINMENU');
end;

procedure TExitConfirmWindow.OnYes(Sender: TObject);
begin
  if (General.Core.RegistrationStatus = rsOn) or
    (General.Core.RegistrationStatus = rsPause) then
    General.Core.StopRegistration;
  General.ExitProgram;
end;

procedure TExitConfirmWindow.Open;
begin
  inherited;
  //
end;

procedure TExitConfirmWindow.Resize;
begin
  inherited;
  FLabel.Width := Self.Width;
  FFootPanel.Height := Round(0.5 * Self.Height);
end;

{ TVirtualKeyboard }

procedure TVirtualKeyboard.BackSpace;
begin
  if Assigned(FEdit) then
  begin
    CompleteInput;
    if FCurLetter > 0 then
    begin
      FEdit.SetFocus;
      FEdit.SelStart := FCurLetter - 1;
      FEdit.SelLength := 1;
      FEdit.ClearSelection;
      Dec(FCurLetter);
      FEdit.SelStart := FCurLetter;
    end;
  end;
end;

procedure TVirtualKeyboard.ButtonOnClick(Sender: TObject);
var
  But: TVKButton;
  i: integer;
  CurMode: TVKButtonModes;
begin
  if not Assigned(Sender) then
    Exit;
  But := TVKButton(Sender);

  case But.ButtonType of
    vtValue:
      begin
      end;
    vtSpace:
      begin
      end;
    vtBakSpace:
      begin
      end;
    vtChangeMode:
      begin
        CurMode := FVKButtonContainer[0].Mode;
        if CurMode = vbDigits then
          CurMode := vbLowerCase
        else if CurMode = vbLowerCase then
          CurMode := vbUpperCase
        else if CurMode = vbUpperCase then
          CurMode := vbDigits;
        for i := 0 to FVKButtonContainer.Count - 1 do
          FVKButtonContainer[i].Mode := CurMode;
      end;
    vtMoveLeft:
      begin
      end;
    vtMoveRight:
      begin
      end;
  end;
end;

procedure TVirtualKeyboard.CompleteInput;
begin
  if (not FInputComplete) then
  begin
    if Assigned(FEdit) then
    begin
      Inc(FCurLetter);
      FEdit.SetFocus;
      FEdit.SelStart := FCurLetter;
      FEdit.SelLength := 0;
      FInputComplete := true;
    end;
  end;
end;

constructor TVirtualKeyboard.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);
begin
  inherited Create(Panel);
  FEdit := nil;
  FCurLetter := 0;
  FInputComplete := true;
  Self.Parent := Panel;
  Self.Color := Panel.Color;
  Self.BevelOuter := bvNone;
  FButPanel := TPanel.Create(nil);
  FButPanel.ParentColor := False;
  FButPanel.Parent := Self;
  FButPanel.ParentBackground := False;
  FButPanel.Color := Panel.Color;
  FButPanel.BevelOuter := bvNone;
  FButPanel.Align := alLeft;
  FEnterButton := ControlFactory.CreateButton(nil);
  FEnterButton.Parent := Self;
  FEnterButton.Align := alClient;
  FEnterButton.Caption := '����';
  FVKButtonContainer := TVKButtonContainer.Create(FButPanel);
  ParceXML(ExtractFilePath(Application.ExeName) + XML_VirtualKeyboardFileName);
end;

procedure TVirtualKeyboard.CursorLeft;
begin
  if Assigned(FEdit) then
  begin
    CompleteInput;
    if FCurLetter > 0 then
    begin
      Dec(FCurLetter);
      FEdit.SetFocus;
      FEdit.SelStart := FCurLetter;
      FEdit.SelLength := 0;
    end;
  end;
end;

procedure TVirtualKeyboard.CursorRight;
begin
  if Assigned(FEdit) then
  begin
    CompleteInput;
    if FCurLetter < Length(FEdit.Text) then
    begin
      Inc(FCurLetter);
      FEdit.SetFocus;
      FEdit.SelStart := FCurLetter;
      FEdit.SelLength := 0;
    end;
  end;
end;

destructor TVirtualKeyboard.Destroy;
begin
  FButPanel.Free;
  FEnterButton.Free;
  FVKButtonContainer.Free;
  inherited;
end;

procedure TVirtualKeyboard.EditOnClick;
begin
  if Assigned(FEdit) then
  begin
    CompleteInput;
    FCurLetter := FEdit.SelStart;
  end;
end;

procedure TVirtualKeyboard.ParceXML(FileName: string);
var
  Doc: IXmlDocument;
  Node, Node1, Node2, Node3: IXmlNode;
  Attr: Variant;
  ButCount, c, c1, i, j, t: integer;
  Default: Boolean;
  But: TVKButton;
  Text: string;
begin
  Doc := CreateXMLDocument;
  Doc.Load(FileName);
  Node := Doc.DocumentElement;
  if not Assigned(Node) then
    Exit;
  Attr := Node.GetVarAttr('KEY_COUNT', 'NULL');
  if Attr <> 'NULL' then
    ButCount := Attr;
  Attr := Node.GetVarAttr('ROWS', 'NULL');
  if Attr <> 'NULL' then
    FRowsCount := Attr;
  Attr := Node.GetVarAttr('COLS', 'NULL');
  if Attr <> 'NULL' then
    FColsCount := Attr;
  Node := Doc.DocumentElement.SelectSingleNode('LANGUAGES');
  if not Assigned(Node) then
    Exit;
  c := Node.ChildNodes.Count;
  Default := False;
  for i := 0 to c - 1 do
  begin
    Node1 := Node.ChildNodes[i];
    Attr := Node1.GetVarAttr('DEFAULT', 'NULL');
    if Attr <> 'NULL' then
      Default := Attr;
    if Default then
      break;
  end;
  if not Default then
    Exit;
  c := Node1.ChildNodes.Count;
  for i := 0 to c - 1 do
  begin
    Node2 := Node1.ChildNodes[i];
    if Assigned(Node2) then
    begin
      But := FVKButtonContainer.AddVKButton;
      But.RowsCount := Self.FRowsCount;
      But.ColsCount := Self.FColsCount;
      But.OnClick := ButtonOnClick;
      if Node2.GetVarAttr('NUM', 'NULL') <> 'NULL' then
        But.Num := Node2.GetVarAttr('NUM', 'NULL');
      if Node2.GetVarAttr('ROW', 'NULL') <> 'NULL' then
        But.Row := Node2.GetVarAttr('ROW', 'NULL');
      if Node2.GetVarAttr('COL', 'NULL') <> 'NULL' then
        But.Col := Node2.GetVarAttr('COL', 'NULL');
      if Node2.GetVarAttr('TYPE', 'NULL') <> 'NULL' then
        But.ButtonType := TVKButtonTypes(Node2.GetVarAttr('TYPE', 'NULL'));
      c1 := Node2.ChildNodes.Count;
      for j := 0 to c1 - 1 do
      begin
        Node3 := Node2.ChildNodes[j];
        if Assigned(Node3) then
        begin
          if Node3.GetVarAttr('MODE', 'NULL') <> 'NULL' then
            t := Node3.GetVarAttr('MODE', 'NULL');
          if Node3.GetVarAttr('TEXT', 'NULL') <> 'NULL' then
            Text := Node3.GetVarAttr('TEXT', 'NULL');
          But.ModeText[t - 1] := Text;
        end;
      end;
      But.Mode := vbDigits;
    end;
  end;
end;

{ TButtonContainer }

function TVKButtonContainer.AddVKButton: TVKButton;
var
  But: TVKButton;
begin
  if not Assigned(FParent) then
    Exit;
  But := TVKButton.Create(FParent);
  Self.Add(But);
  Result := But;
end;

procedure TVKButtonContainer.Clear;
var
  i: integer;
begin
  for i := 0 to Self.Count - 1 do
    if Assigned(TVKButton(Self.Items[i])) then
      TVKButton(Self.Items[i]).Free;
  inherited;
end;

constructor TVKButtonContainer.Create(_Parent: TWinControl);
begin
  FParent := _Parent;
  inherited Create;
end;

destructor TVKButtonContainer.Destroy;
var
  i: integer;
begin
  Self.Clear;
  inherited;
end;

procedure TVirtualKeyboard.Resize;
var
  i: integer;
begin
  inherited;
  FButPanel.Width := Round(0.8 * Self.Width);
  for i := 0 to FVKButtonContainer.Count - 1 do
    FVKButtonContainer[i].Resize;
end;

procedure TVirtualKeyboard.SetEditBox(Value: TEdit);
begin
  if not Assigned(Value) then
    Exit;
  FEdit := Value;
  FEdit.SetFocus;
  FEdit.SelStart := 0;
  FCurLetter := 0;
  FEdit.SelLength := 0;
end;

{ TVKButton }

constructor TVKButton.Create(AOwner: TWinControl);
begin
  FParent := AOwner;
  FButton := ControlFactory.CreateButton(FParent);
  FButton.Parent := FParent;
  FButton.OnClick := OnClickEvent;
  FButton.Font.Name := DefaultFontName;
  OnClick := nil;
  FMode := vbDigits;
  SetMode(FMode);
  FCurLetter := 0;
end;

destructor TVKButton.Destroy;
begin
  inherited;
end;

function TVKButton.GetHeight: integer;
begin
  if Assigned(FButton) then
    Result := FButton.Height;
end;

function TVKButton.GetLeft: integer;
begin
  if Assigned(FButton) then
    Result := FButton.Left;
end;

function TVKButton.GetModeText(Index: integer): string;
begin
  if Index <= High(FText) then
    Result := FText[Index];
end;

function TVKButton.GetTop: integer;
begin
  if Assigned(FButton) then
    Result := FButton.Top;
end;

function TVKButton.GetWidth: integer;
begin
  if Assigned(FButton) then
    Result := FButton.Width;
end;

procedure TVKButton.OnClickEvent(Sender: TObject);
const
  ButHold = 500;
begin
  // ��������� ������� � ������������ � ����� ������
  if ButtonType = vtValue then
  begin
    if (GetTickCount - FLastClickTime) > ButHold then
      FCurLetter := 1
    else if FCurLetter < Length(FCurText) then
      Inc(FCurLetter)
    else
      FCurLetter := 1;
    FLetter := FCurText[FCurLetter];
    FLastClickTime := GetTickCount;
  end;
  if Assigned(OnClick) then
    OnClick(Self);
end;

procedure TVKButton.Resize;
begin
  SetRowsCount(FRowsCount);
  SetColsCount(FColsCount);
  FButton.Font.Height := Round(0.9 * FButton.Height);
end;

procedure TVKButton.SetCol(Value: integer);
var
  w: integer;
begin
  FCol := Value;
  if Assigned(FParent) then
    w := FParent.Width;
  if Assigned(FButton) then
    FButton.Left := (FCol - 1) * FButton.Width + 4 * (FCol + 1);
end;

procedure TVKButton.SetColsCount(Value: integer);
var
  w: integer;
begin
  FColsCount := Value;
  if Assigned(FParent) then
    w := FParent.Width - (4 * (FColsCount + 2));
  if Assigned(FButton) then
    FButton.Width := Round(w / FColsCount);
  SetCol(FCol);
end;

procedure TVKButton.SetHeight(Value: integer);
begin
  if Assigned(FButton) then
    FButton.Height := Value;
end;

procedure TVKButton.SetLeft(Value: integer);
begin
  if Assigned(FButton) then
    FButton.Left := Value;
end;

procedure TVKButton.SetMode(Value: TVKButtonModes);
begin
  FMode := Value;
  FCurText := FText[Ord(FMode)];
  if Assigned(FButton) then
    FButton.Caption := FCurText;
end;

procedure TVKButton.SetModeText(Index: integer; Value: string);
begin
  if Index <= High(FText) then
    FText[Index] := Value;
end;

procedure TVKButton.SetRow(Value: integer);
var
  h: integer;
begin
  FRow := Value;
  if Assigned(FParent) then
    h := FParent.Height;
  if Assigned(FButton) then
    FButton.Top := (FRow - 1) * FButton.Height + 4 * (FRow) - 4;
end;

procedure TVKButton.SetRowsCount(Value: integer);
var
  h: integer;
begin
  FRowsCount := Value;
  if Assigned(FParent) then
    h := FParent.Height - (4 * (FRowsCount + 1));
  if Assigned(FButton) then
    FButton.Height := Round(h / (FRowsCount));
  SetRow(FRow);
end;

procedure TVKButton.SetTop(Value: integer);
begin
  if Assigned(FButton) then
    FButton.Top := Value;
end;

procedure TVKButton.SetWidth(Value: integer);
begin
  if Assigned(FButton) then
    FButton.Width := Value;
end;

function TVKButtonContainer.GetButton(Index: integer): TVKButton;
begin
  if Index <= Self.Count then
    Result := Self.Items[Index];
end;

{ TMarkSwitchNumber }

constructor TMarkSwitchNumber.Create(Parent: TPaneledWindow);
begin
  inherited;

  CPanel := FParent.PanelManager.AddPanel
    (TControlPanel.Create('MarkSwitchNum', poVertical, caTop, 12));
  CPanel.BevelOuter := bvNone;

  FNumber := TRegNumber.Create('switch_num', CPanel, '');
  CPanel.Add(FNumber);
  FNumber.ObjLeft := 1;
  FNumber.ObjWidth := Round(TMarksWindow(FParent).FGP.Width * 0.9);
end;

procedure TMarkSwitchNumber.SetActive;
begin
  inherited;
  // ���� ��������, ���� ����������, ����������� ���������� �������� ����� �������
  TMarksWindow(FParent).FGP.Caption := FParent.FLT['����� �������'];
end;

procedure TMarkSwitchNumber.SetMark;
begin
  inherited;
  General.Core.Registrator.AddStrelka(IntToStr(FNumber.Value));
end;

{ TExitConfirmRebootWindow }

constructor TRebootConfirmWindow.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);
begin
  inherited;
  Self.Caption := '';

  FTimer.Enabled := False;
  FLabel := TLabel.Create(Self);
  FLabel.Parent := Self;
  FLabel.Visible := true;
  FLabel.Caption := FLT.Caption['��������� ���������� ���������.'];

  FLabel.Font.Color := clBlack;
  FLabel.Font.Name := DefaultFontName;
  FLabel.Width := Self.Width;
  FLabel.Alignment := taCenter;
  FLabel.Top := 110;
  FLabel.Font.Height := Round(0.08 * Self.Height);

  FLabel2 := TLabel.Create(Self);
  FLabel2.Parent := Self;
  FLabel2.Visible := true;
  FLabel2.Caption := FLT.Caption['���������� ���?'];
  FLabel2.Font.Color := clBlack;
  FLabel2.Font.Name := DefaultFontName;
  FLabel2.Width := Self.Width;
  FLabel2.Alignment := taCenter;
  FLabel2.Top := 220;
  FLabel2.Font.Height := Round(0.08 * Self.Height);
  FLabel2.Alignment := taCenter;

  FFootPanel := TPanel.Create(Self);
  FFootPanel.ParentColor := False;
  FFootPanel.Parent := Self;
  FFootPanel.ParentBackground := False;
  FFootPanel.Visible := true;
  FFootPanel.Caption := '';
  FFootPanel.Color := Self.Color;
  FFootPanel.BevelOuter := bvNone;
  FFootPanel.BorderWidth := 0;
  FFootPanel.BorderStyle := bsNone;
  FFootPanel.Ctl3D := False;
  FFootPanel.Align := alBottom;
  FFootPanel.Height := Round(0.2 * Self.Height);

  FButPanel := TControlPanelManager.Create(FFootPanel, FCT, FLT);
  FButPanel.Color := Self.Color;

  FButPanel.AddPanel(TButtonPanel.Create('Confirm', poHorizontal, caWholeSize,
      45));
  FButPanel.ButtonPanel['Confirm'].AddButton(FLT.Caption['��'], OnYes);
  FButPanel.ButtonPanel['Confirm'].AddButton(FLT.Caption['���'], OnNo);

end;

procedure TRebootConfirmWindow.OnNo(Sender: TObject);
begin
  General.OpenWindow('MAINMENU');
end;

procedure TRebootConfirmWindow.OnYes(Sender: TObject);
begin
  if (General.Core.RegistrationStatus = rsOn) or
    (General.Core.RegistrationStatus = rsPause) then
    General.Core.StopRegistration;
  General.RebootProgram;
end;

procedure TRebootConfirmWindow.Open;
begin
  inherited;

end;

procedure TRebootConfirmWindow.Resize;
begin
  inherited;
  FLabel.Width := Self.Width;
  FLabel2.Width := Self.Width;
  FFootPanel.Height := Round(0.5 * Self.Height);
end;

{ TEnterSNWindow }

constructor TEnterSNWindow.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);
begin
  inherited;
  Self.Caption := '';

  FTimer.Enabled := False;
  FTimer.Interval := 1;

  FTopPanel := TPanel.Create(Self);
  FTopPanel.ParentColor := False;
  FTopPanel.Parent := Self;
  FTopPanel.ParentBackground := False;
  FTopPanel.Visible := true;
  FTopPanel.Caption := '';
  FTopPanel.Color := Self.Color;
  FTopPanel.BevelOuter := bvNone;
  FTopPanel.BorderWidth := 0;
  FTopPanel.BorderStyle := bsNone;
  FTopPanel.Ctl3D := False;
  FTopPanel.Align := alTop;
  FTopPanel.Height := Round(0.25 * Self.Height);

  FLabel := TLabel.Create(Self);
  FLabel.Parent := FTopPanel;
  FLabel.Visible := true;
  FLabel.Caption := FLT.Caption['������� �������� ����� ���'];
  FLabel.Font.Color := clBlack;
  FLabel.Font.Name := DefaultFontName;
  FLabel.Width := FTopPanel.Width;
  FLabel.Alignment := taCenter;

  FLabel.Top := 110;
  FLabel.Font.Height := Round(0.07 * Self.Height);

  FFootPanel := TPanel.Create(Self);
  FFootPanel.ParentColor := False;
  FFootPanel.Parent := Self;
  FFootPanel.ParentBackground := False;
  FFootPanel.Visible := true;
  FFootPanel.Caption := '';
  FFootPanel.Color := Self.Color;
  FFootPanel.BevelOuter := bvNone;
  FFootPanel.BorderWidth := 0;
  FFootPanel.BorderStyle := bsNone;
  FFootPanel.Ctl3D := False;
  FFootPanel.Align := alBottom;
  FFootPanel.Height := Round(0.2 * Self.Height);

  FMidPanel := TPanel.Create(Self);
  FMidPanel.ParentColor := False;
  FMidPanel.Parent := Self;
  FMidPanel.ParentBackground := False;
  FMidPanel.Visible := true;
  FMidPanel.Caption := '';
  FMidPanel.Color := Self.Color;
  FMidPanel.BevelOuter := bvNone;
  FMidPanel.BorderWidth := 0;
  FMidPanel.BorderStyle := bsNone;
  FMidPanel.Ctl3D := False;
  FMidPanel.Align := alClient;
  FMidPanel.Height := Round(0.5 * Self.Height);

  FButPanel := TControlPanelManager.Create(FFootPanel, FCT, FLT);
  FButPanel.Color := Self.Color;

  FButPanel.AddPanel(TButtonPanel.Create('Confirm', poHorizontal, caWholeSize,
      30));
  YesBut := FButPanel.ButtonPanel['Confirm'].AddButton
    (FLT.Caption['������'], OnYes);
  NoBut := FButPanel.ButtonPanel['Confirm'].AddButton
    (FLT.Caption['������'], OnNo);

  CPanel := TControlPanel.Create('SN', poVertical, caWholeSize, 15);
  CPanel.Parent := FMidPanel;
  CPanel.BevelOuter := bvNone;

  Year := TRegNumber.Create('Year', CPanel);
  CPanel.Add(Year);
  Year.Caption := LT.Caption['��� �������'] + ': ';

  Num := TRegNumber.Create('Num', CPanel);
  CPanel.Add(Num);
  Num.Caption := LT.Caption['����� �������'] + ': ';

end;

procedure TEnterSNWindow.OnNo(Sender: TObject);
begin
  General.OpenWindow('MAINMENU');
end;

procedure TEnterSNWindow.OnTimer(Sender: TObject);
begin
  inherited;
  if (Year.Value > 0) and (Num.Value > 0) then
    YesBut.Enabled := true
  else
    YesBut.Enabled := False;
end;

procedure TEnterSNWindow.OnYes(Sender: TObject);
begin
  FTimer.Enabled := False;
  General.SetBUISN(Year.Value, Num.Value);
  General.OpenWindow('MAINMENU');
end;

procedure TEnterSNWindow.Open;
begin
  inherited;
  Year.Value := General.SNYear;
  Num.Value := General.SNNum;
  if (Year.Value = 0) and (Num.Value = 0) then
  begin
    YesBut.Enabled := False;
    NoBut.Enabled := False;
  end;
  FTimer.Enabled := true;
end;

procedure TEnterSNWindow.Resize;
begin
  inherited;
  FLabel.Width := Self.Width;
end;

initialization

ProcessException := TProcessException.Create;
ProcessException.MonitorAnotherThreads := true;

finalization

ProcessException.Free;

end.
