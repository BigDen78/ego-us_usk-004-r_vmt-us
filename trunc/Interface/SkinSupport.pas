unit SkinSupport;

interface
uses StdCtrls, SysUtils, Forms, Classes, Graphics, ComCtrls, 
  sButton, sLabel, sComboBox, sTrackBar, sRadioButton, acProgressBar, sSkinManager, 
  PublicFunc, sScrollBar;
  
type

  TControlFactory = class
  private
    FDefaultFontName: string;
    FDefaultFontColor: TColor;

    function GetLabelUseSkinColor( Lab: TCustomLabel ): Boolean; virtual; abstract;
    procedure SetLabelUseSkinColor( Lab: TCustomLabel; Value: Boolean ); virtual; abstract;
    function GetLabelFont( Lab: TCustomLabel ): TFont; virtual; abstract;
    function GetLabelAlignment( Lab: TCustomLabel ): TAlignment; virtual; abstract;
    procedure SetLabelAlignment( Lab: TCustomLabel; Value: TAlignment ); virtual; abstract;
  public
    procedure InitializeForm( Form: TForm ); virtual; abstract;
    procedure InitializeLabel( Lab: TCustomLabel; FontHeight: Integer );

    function CreateButton( AOwner: TComponent ): TButton; virtual; abstract;
    function CreateLabel( AOwner: TComponent ): TCustomLabel; virtual; abstract;
    function CreateComboBox( AOwner: TComponent ): TCustomComboBox; virtual; abstract;
    function CreateTrackBar( AOwner: TComponent ): TTrackBar; virtual; abstract;
    function CreateRadioButton( AOwner: TComponent ): TRadioButton; virtual; abstract;
    function CreateProgressBar( AOwner: TComponent ): TProgressBar; virtual; abstract;
    function CreateScrollBar( AOwner: TComponent ): TScrollBar; virtual; abstract;



    property LabelUseSkinColor[ Lab: TCustomLabel ]: Boolean read GetLabelUseSkinColor write SetLabelUseSkinColor;
    property LabelFont[ Lab: TCustomLabel ]: TFont read GetLabelFont;
    property LabelAligment[ Lab: TCustomLabel ]: TAlignment read GetLabelAlignment write SetLabelAlignment;

    property DefaultFontName: string read FDefaultFontName write FDefaultFontName;
    property DefaultFontColor: TColor read FDefaultFontColor write FDefaultFontColor;
  end;

  TBasicControlFactory = class( TControlFactory )
  private
    function GetLabelUseSkinColor( Lab: TCustomLabel ): Boolean; override;
    procedure SetLabelUseSkinColor( Lab: TCustomLabel; Value: Boolean ); override;
    function GetLabelFont( Lab: TCustomLabel ): TFont; override;
    function GetLabelAlignment( Lab: TCustomLabel ): TAlignment; override;
    procedure SetLabelAlignment( Lab: TCustomLabel; Value: TAlignment ); override;
  public
    procedure InitializeForm( Form: TForm ); override;
    function CreateButton( AOwner: TComponent ): TButton; override;
    function CreateLabel( AOwner: TComponent ): TCustomLabel; override;
    function CreateComboBox( AOwner: TComponent ): TCustomComboBox; override;
    function CreateTrackBar( AOwner: TComponent ): TTrackBar; override;
    function CreateRadioButton( AOwner: TComponent ): TRadioButton; override;
    function CreateProgressBar( AOwner: TComponent ): TProgressBar; override;
    function CreateScrollBar( AOwner: TComponent ): TScrollBar; override;
  end;

  TSkinedControlFactory = class( TControlFactory )
  private
    FSkinName: string;
    function GetLabelUseSkinColor( Lab: TCustomLabel ): Boolean; override;
    procedure SetLabelUseSkinColor( Lab: TCustomLabel; Value: Boolean ); override;
    function GetLabelFont( Lab: TCustomLabel ): TFont; override;
    function GetLabelAlignment( Lab: TCustomLabel ): TAlignment; override;
    procedure SetLabelAlignment( Lab: TCustomLabel; Value: TAlignment ); override;
  public
    constructor Create( SkinName: string );
    procedure InitializeForm( Form: TForm ); override;
    function CreateButton( AOwner: TComponent ): TButton; override;
    function CreateLabel( AOwner: TComponent ): TCustomLabel; override;
    function CreateComboBox( AOwner: TComponent ): TCustomComboBox; override;
    function CreateTrackBar( AOwner: TComponent ): TTrackBar; override;
    function CreateRadioButton( AOwner: TComponent ): TRadioButton; override;
    function CreateProgressBar( AOwner: TComponent ): TProgressBar; override;
    function CreateScrollBar( AOwner: TComponent ): TScrollBar; override;
  end;


var
  ControlFactory: TControlFactory;

implementation

{ TBasicControlFactory }

function TBasicControlFactory.CreateButton( AOwner: TComponent ): TButton;
begin
  Result:= TButton.Create( AOwner );
end;

function TBasicControlFactory.CreateComboBox(AOwner: TComponent): TCustomComboBox;
begin
  Result:= TComboBox.Create( AOwner );
end;

function TBasicControlFactory.CreateLabel(AOwner: TComponent): TCustomLabel;
begin
  Result:= TLabel.Create( AOwner );
end;

function TBasicControlFactory.CreateProgressBar(
  AOwner: TComponent): TProgressBar;
begin
  Result:= TProgressBar.Create( AOwner );
end;

function TBasicControlFactory.CreateRadioButton(
  AOwner: TComponent): TRadioButton;
begin
  Result:= TRadioButton.Create( AOwner );
end;

function TBasicControlFactory.CreateScrollBar(AOwner: TComponent): TScrollBar;
begin
  Result:= TScrollBar.Create( AOwner );
end;

function TBasicControlFactory.CreateTrackBar(AOwner: TComponent): TTrackBar;
begin
  Result:=TTrackBar.Create( AOwner );
end;

function TBasicControlFactory.GetLabelAlignment(Lab: TCustomLabel): TAlignment;
begin
  if Lab is TLabel then Result:= TLabel( Lab ).Alignment else Result:= taLeftJustify;
end;

function TBasicControlFactory.GetLabelFont(Lab: TCustomLabel): TFont;
begin
  if Lab is TLabel then Result:= TLabel( Lab ).Font else Result:= nil;
end;

function TBasicControlFactory.GetLabelUseSkinColor( Lab: TCustomLabel): Boolean;
begin
end;

procedure TBasicControlFactory.InitializeForm(Form: TForm);
begin
end;


procedure TBasicControlFactory.SetLabelAlignment(Lab: TCustomLabel;
  Value: TAlignment);
begin
  if Lab is TLabel then TLabel( Lab ).Alignment:= Value;
end;

procedure TBasicControlFactory.SetLabelUseSkinColor(Lab: TCustomLabel;
  Value: Boolean);
begin
  inherited;

end;

{ TSkinedControlFactory }

constructor TSkinedControlFactory.Create(SkinName: string);
begin
  inherited Create;
  FSkinName:= SkinName;
end;

function TSkinedControlFactory.CreateButton( AOwner: TComponent ): TButton;
begin
  Result:= TsButton.Create( AOwner );
  TsButton( Result ).ShowFocus:= False;
end;


function TSkinedControlFactory.CreateComboBox(
  AOwner: TComponent): TCustomComboBox;
begin
  //Result:= TsComboBox.Create( AOwner );
  Result:= TComboBox.Create( AOwner );
end;

function TSkinedControlFactory.CreateLabel(AOwner: TComponent): TCustomLabel;
begin
  Result:= TsLabel.Create( AOwner );
end;

function TSkinedControlFactory.CreateProgressBar(
  AOwner: TComponent): TProgressBar;
begin
  Result:= TsProgressBar.Create( AOwner );
end;

function TSkinedControlFactory.CreateRadioButton(
  AOwner: TComponent): TRadioButton;
begin
  Result:= TsRadioButton.Create( AOwner );
end;

function TSkinedControlFactory.CreateScrollBar(AOwner: TComponent): TScrollBar;
begin
  Result:=TScrollBar.Create( AOwner );
end;

function TSkinedControlFactory.CreateTrackBar(AOwner: TComponent): TTrackBar;
begin
  Result:=TsTrackBar.Create( AOwner );
end;

function TSkinedControlFactory.GetLabelAlignment(
  Lab: TCustomLabel): TAlignment;
begin
  if Lab is TsLabel then Result:= TsLabel( Lab ).Alignment else Result:= taLeftJustify;
end;

function TSkinedControlFactory.GetLabelFont(Lab: TCustomLabel): TFont;
begin
  if Lab is TsLabel then Result:= TsLabel( Lab ).Font else Result:= nil;
end;

function TSkinedControlFactory.GetLabelUseSkinColor(Lab: TCustomLabel): Boolean;
begin
  if Lab is TsLabel then Result:= TsLabel( Lab ).UseSkinColor else Result:= False;
end;

procedure TSkinedControlFactory.InitializeForm(Form: TForm);
var
  SkinManager: TsSkinManager;

begin
  SkinManager:= TsSkinManager.Create( Form );
  SkinManager.SkinDirectory:= AddSlash( ExtractFilePath( Application.ExeName ) ) + 'Skins';
//  SkinManager.SkinName:= 'Golden';
//  SkinManager.SkinName:= 'BlueGlass';
  SkinManager.SkinName:= FSkinName;
end;

procedure TSkinedControlFactory.SetLabelAlignment(Lab: TCustomLabel;
  Value: TAlignment);
begin
  if Lab is TsLabel then TsLabel( Lab ).Alignment:= Value;
end;

procedure TSkinedControlFactory.SetLabelUseSkinColor(Lab: TCustomLabel; Value: Boolean);
begin
  if Lab is TsLabel then TsLabel( Lab ).UseSkinColor:= Value;
end;

{ TControlFactory }


{ TControlFactory }

procedure TControlFactory.InitializeLabel( Lab: TCustomLabel; FontHeight: Integer );
begin
  LabelUseSkinColor[ Lab ]:= False;
  with LabelFont[ Lab ] do
  begin
    Name:= FDefaultFontName;
    Color:= FDefaultFontColor;
    Height:= FontHeight;
  end;
end;

initialization
  ControlFactory:= nil;

finalization
  ControlFactory.Free;

end.
