/////////////////////////////////////////////////////
//// ��������������� ������� ��� ����������      ////
////                                             ////
//// ������ �.�. ���� 2009                       ////
//// ������ 1.0.0                                ////
////                                             ////
/////////////////////////////////////////////////////

unit UtilsUnit;

interface

uses Windows, Classes, ComCtrls, SimpleXML, Graphics, StdCtrls, Controls, SysUtils, ExtCtrls,
     SkinSupport, Math, sEdit, sSpinEdit, sPanel, sMonthCalendar, Forms,
  Mask, sMaskEdit, sCustomComboEdit, sTooledit, {TransparentPanel,} Buttons, Dialogs;

const
     ceITEMSONSCREEN=11;//2;  //10
     ceFONTHEIGHT=30;//28;     //30
     ceItemSpace=5;//10;
type

    // ���� ������� ������� �������������, �������� ���� � �������� �����
    TElement = record
      Id: string;
      Value: Variant;
    end;

    // � ����� ������ ��������� ��������� (�������� ������ �������� �����)
    TGroup = record
      Name: string;
      Default: Boolean;
      Elements: array of TElement;
    end;

    // ��������� ������ ������� ��������� ��������� ����� (�������� �������� ����)
    TGroupMas = array of TGroup;

    TIDController = class
    private
    public
      class procedure SetID( Element: TComponent; ID: string );
      class function GetID( Element: TComponent ): string;
    end;

    TRegParamTypes = (rpValue, rpList, rpNum);

    TCustomInputItem = class(TCustomControl)
    private
      FSText: TStaticText;
      FObj: TControl;
      FObjOfs: Single;

      FSelected: Boolean;

      FMouseDownEvent: TMouseEvent;
      FMouseUpEvent: TMouseEvent;
      FMouseMoveEvent: TMouseMoveEvent;

      procedure OnObjectChange(Sender: TObject);

    protected
      function GetCaptionLeft: Integer; virtual;
      procedure SetCaptionLeft(Value: integer);virtual;
      function GetCaption: string;virtual;
      procedure SetCaption(Value: string);virtual;
      function GetObjLeft: Integer;
      procedure SetObjLeft(Value: integer);
      procedure SetColor(Value: TColor); virtual;
      function GetObjWidth: Integer; virtual;
      procedure SetObjWidth(Value: integer); virtual;
      function GetCaptionWidth: Integer;
      procedure UpDatePos; virtual;

      procedure Resize; override;

      function GetValue: Variant; virtual; abstract;
      procedure SetValue( Value: Variant ); virtual; abstract;

      function GetID: string;
      procedure SetID( ID: string );

      procedure SetMouseDownEvent(Value: TMouseEvent); virtual;
      procedure SetMouseUpEvent(Value: TMouseEvent); virtual;
      procedure SetMouseMoveEvent(Value: TMouseMoveEvent); virtual;

      procedure SetSelect(Selected: Boolean); virtual;

      procedure RealMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
      procedure RealMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
      procedure RealMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);

    public
      ParentID: string;
      ParamType: TRegParamTypes;

      SelectColor: TColor;
      UnSelectColor: TColor;

      OnChange: TNotifyEvent;
      constructor Create(ID_: string; Parent_: TWinControl; Caption: string = '');
      destructor Destroy; override;
      procedure MoveVertical(Delta: Integer);

      property CaptionLeft: Integer read GetCaptionLeft write SetCaptionLeft;
      property ObjLeft: Integer read GetObjLeft write SetObjLeft;
      property ObjWidth: Integer read GetObjWidth write SetObjWidth;
      property CaptionWidth: Integer read GetCaptionWidth;
      property Caption: string read GetCaption write SetCaption;
      property TextColor: TColor write SetColor;
      property Value: Variant read GetValue write SetValue;
      property ID: string read GetID write SetID;
      property OnMouseD: TMouseEvent write SetMouseDownEvent;
      property OnMouseU: TMouseEvent write SetMouseUpEvent;
      property OnMouseM: TMouseMoveEvent write SetMouseMoveEvent;
      property Selected: Boolean read FSelected write SetSelect;
    end;

    TRegComboBox = class(TCustomInputItem)
    private
    protected
      procedure Resize; override;
      procedure SetColor(Value: TColor); override;
      function GetItemIndex: Integer;
      procedure SetItemIndex( Value: Integer );
      function GetComboBoxWidth: Integer;
      procedure SetComboBoxWidth( V: Integer );
      function GetValue: Variant; override;
      procedure SetValue( Value: Variant ); override;
    public
      constructor Create( ID_: string; Parent: TWinControl; Caption: string = '' );
      procedure LoadFromFile(FileName: string);
      property ComboBoxWidth: Integer read GetComboBoxWidth write SetComboBoxWidth;
      procedure Add( Caption: string );
      property ItemIndex: Integer read GetItemIndex write SetItemIndex;
    end;

    TRegValue = class(TCustomInputItem)
    protected
      procedure Resize; override;
      procedure SetColor(Value: TColor); override;
      function GetValue: Variant; override;
      procedure SetValue( Value: Variant ); override;

      procedure SetMouseDownEvent(Value: TMouseEvent); override;
      procedure SetMouseUpEvent(Value: TMouseEvent); override;

    public
      constructor Create( ID_: string; Parent: TWinControl; Caption: string = '' );
    end;

    TRegTrackBar = class(TCustomInputItem)
    private
    protected
      procedure Resize; override;
      procedure SetColor(Value: TColor); override;
      function GetValue: Variant; override;
      procedure SetValue( Value: Variant ); override;
    public
      constructor Create( ID_: string; Parent: TWinControl; Caption: string = '' );
    end;

    TRegDate = class(TCustomInputItem)
    private
    protected
      procedure Resize; override;
      function GetValue: Variant; override;
      procedure SetValue( Value: Variant ); override;
    public
      constructor Create( ID_: string; Parent: TWinControl; Caption: string = '' );
    end;

    TRegTime = class(TCustomInputItem)
    private
    protected
      procedure Resize; override;
      function GetValue: Variant; override;
      procedure SetValue( Value: Variant ); override;
    public
      constructor Create( ID_: string; Parent: TWinControl; Caption: string = '' );
    end;

    // ���� �� ��������
    TRegEditBox = class(TCustomInputItem)
    private
    protected
      procedure Resize; override;
      function GetValue: Variant; override;
      procedure SetValue( Value: Variant ); override;

      procedure SetMouseDownEvent(Value: TMouseEvent); override;
      procedure SetMouseUpEvent(Value: TMouseEvent); override;

    public
      constructor Create( ID_: string; Parent: TWinControl; Caption: string = '' );
    end;


    TRegNumber = class(TCustomInputItem)
    private
      BtnInc: TButton;
      BtnDec: TButton;
      FFValue: Integer;
      ScrollTimer: TTimer;
      Delta: Integer;

      procedure SetFValue( V: Integer );
      property FValue: Integer read FFValue write SetFValue;

      procedure OnBtnInc( Sender: TObject ); virtual;
      procedure OnBtnDec( Sender: TObject ); virtual;

      procedure OnBtnDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer); virtual;
      procedure OnBtnUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer); virtual;
      procedure OnScroll(Sender: TObject);

      procedure Resize; override;
      procedure Refresh; virtual;
      function GetValue: Variant; override;
      procedure SetValue( Value: Variant ); override;
    public
      MinValue: Integer;
      MaxValue: Integer;
      constructor Create( ID_: string; Parent: TWinControl; Caption: string = '' );

    end;

    TSafeMaskEdit = class(TCustomMaskEdit)
    public
      constructor Create(AOwner: TComponent); override;
//      procedure ValidateEdit;override;
      procedure ValidateError;override;
    end;

    TRegChainage = class(TRegNumber)
    private
      FSelIdx: integer;
      function GetSelectedNum: integer;
      procedure SetValueToEdit;
      procedure OnBtnInc( Sender: TObject ); override;
      procedure OnBtnDec( Sender: TObject ); override;
      procedure Refresh; override;
      procedure Resize; override;
      procedure OnBtnDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
      procedure OnBtnUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    public
      constructor Create( ID_: string; Parent: TWinControl; FontName: string; FontColor: TColor; Caption: string = ''; turkish: byte=0{boolean = false});
      function ToString: string;
      procedure LoadFromString(Val: string);
    end;

    TRegButton = class(TCustomInputItem)
    private
      FBut: TButton;
      procedure OnButClick(Sender: TObject);
      function GetValue: Variant; override;
      procedure SetValue( Value: Variant ); override;
      procedure Resize; override;
      function GetCaption: string; override;
      procedure SetCaption(Value: string); override;
    public
      OnClick: TNotifyEvent;
      constructor Create( ID_: string; Parent: TWinControl; Caption: string = '' );
      procedure SetTextBeforeButton(Value: string);
    end;

    TIndicatorStyle = (isRect, isBattery);

    TIndicator = class(TCustomInputItem)
    private
      FBMP: TBitMap;
      FPercent: Integer;
      FText: string;
      procedure UpDatePicture;
    protected
      procedure SetPercent(Value: integer);
      procedure PBPaint(Sender: TObject);
      procedure Resize; override;
      procedure SetText(Value: string);
    public
      Style: TIndicatorStyle;

      constructor Create( ID_: string; Parent: TWinControl; Caption: string = '' );
      destructor Destroy;override;
      property Percent: integer read FPercent write SetPercent;
      property Text: string read FText write SetText;
    end;

     TInputItemList = class( TList )
       protected
         function Get(Index: Integer): TCustomInputItem;
         procedure Put(Index: Integer; Item: TCustomInputItem);
       public
         property Items[Index: Integer]: TCustomInputItem read Get write Put; default;
     end;

     TListItemSelectedEvent = procedure(ID: string) of object;

     TCustomElementList = class(TPanel)
       private
        Self_Color: TColor;
         FStartPos: integer;
         FEndPos: integer;
         FYOffset: integer;
         FItemsSpace: integer;
         Fomy: integer;
         FItemList: TInputItemList;

         FSelectColor: TColor;
         FUnSelectColor: TColor;

         FIsMouseMoving: Boolean;

         FMaxPixCountNotMoving: Integer;
         FMouseDownY: Integer;

         procedure Resize; override;
         procedure GlobalMouseMove(Sender: TObject; Shift: TShiftState; X,
          Y: Integer);
         procedure MD(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
         procedure MU(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
       protected
       public
         OnItemSelected: TListItemSelectedEvent;
         SelectionEnable: Boolean;
         constructor Create( Parent_: TWinControl; SelColor, UnSelColor: TColor );
         destructor Destroy; override;
         procedure Clear;
         procedure AddItem(ID, Caption: string);
         procedure SetBackGround(Color : TColor);
         procedure SaveToFile(FileName: String; Encoding: TEncoding); overload;
         procedure SaveToFile(FileName: String); overload;
         function ItemInList(ID:string):boolean;
         procedure Scroll( Delta: Integer);
         property ListHeight: Integer Read FYOffset;
     end;

{     TVirtKeybInput = class(TCustomInputItem)
     private
     public
      constructor Create( ID_: string; Parent: TWinControl; Caption: string = '' );
      destructor Destroy;override;
     end;
}

implementation

uses BScanUnit;

{ TCustomInputItem }

constructor TCustomInputItem.Create( ID_: string; Parent_: TWinControl; Caption: string = '' );
begin
  inherited Create( nil );

  FMouseDownEvent:= nil;
  FMouseUpEvent:= nil;
  FMouseMoveEvent:= nil;

  Self.Parent:= Parent_;
  Self.ParentFont:=false;
  Self.ParentDoubleBuffered:= false;
  Self.DoubleBuffered:= true;
  ID:=ID_;
  FObjOfs:= 0.5;
  Self.BevelOuter:= bvNone;

  Self.OnMouseDown:= RealMouseDown;
  Self.OnMouseUp:= RealMouseUp;
  Self.OnMouseMove:= RealMouseMove;

  FSText:=TStaticText.Create(nil);
  FSText.Parent:= Self;
  FSText.Font.Name:=DefaultFontName;
  FSText.Caption:= Caption;
  FSText.Visible:=true;
  FSText.ParentDoubleBuffered:= false;
  FSText.DoubleBuffered:= true;
  FSText.Transparent:= false;

  FSText.OnMouseDown:= RealMouseDown;
  FSText.OnMouseUp:= RealMouseUp;
  FSText.OnMouseMove:= RealMouseMove;

  FSelected:= false;

  FObj:= nil;
end;

destructor TCustomInputItem.Destroy;
begin
  FSText.Free;
  if Assigned(FObj) then FObj.Free;
  inherited;
end;

procedure TCustomInputItem.OnObjectChange(Sender: TObject);
begin
  if Assigned( OnChange ) then OnChange( Self );
end;

function TCustomInputItem.GetCaption: string;
begin
  result:=FSText.Caption;
end;

function TCustomInputItem.GetCaptionLeft: Integer;
begin
  result:=FSText.Left;
end;

function TCustomInputItem.GetCaptionWidth: Integer;
begin
  result:=FSText.Width;
end;


function TCustomInputItem.GetID: string;
begin
  Result:= TIDController.GetID( Self );
end;

function TCustomInputItem.GetObjLeft: Integer;
begin
  if Assigned(FObj) then result:=FObj.Left;
end;

function TCustomInputItem.GetObjWidth: Integer;
begin
  if Assigned(FObj) then result:=FObj.Width;
end;

procedure TCustomInputItem.MoveVertical(Delta: Integer);
begin
  Self.Top:= Self.Top + Delta;
end;

procedure TCustomInputItem.RealMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Assigned(FMouseDownEvent) then
    FMouseDownEvent(Self, Button, Shift, X, Y);
end;

procedure TCustomInputItem.RealMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  if Assigned(FMouseMoveEvent) then
    FMouseMoveEvent(Self, Shift, X, Y);
end;

procedure TCustomInputItem.RealMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Assigned(FMouseUpEvent) then
    FMouseUpEvent(Self, Button, Shift, X, Y);
end;

procedure TCustomInputItem.Resize;
begin
  inherited;
  if Assigned(FSText) then
  begin
    FSText.Font.Height:= Height;
    FSText.Left:= 0;
  end;
  if Assigned(FObj) then
  if FObj.Left = 0 then FObj.Left:= Round( Width * 0.5 );
end;

procedure TCustomInputItem.SetCaption(Value: string);
begin
  FSText.Caption:=Value;
end;

procedure TCustomInputItem.SetCaptionLeft(Value: integer);
begin
  FSText.Left:=Value;
end;

procedure TCustomInputItem.SetColor(Value: TColor);
begin
  FSText.Font.Color:=Value;
  UpDatePos;
end;

procedure TCustomInputItem.SetID(ID: string);
begin
  TIDController.SetID( Self, ID );
end;

procedure TCustomInputItem.SetMouseDownEvent(Value: TMouseEvent);
begin
  FMouseDownEvent:= Value;
end;

procedure TCustomInputItem.SetMouseMoveEvent(Value: TMouseMoveEvent);
begin
  FMouseMoveEvent:= Value;
end;

procedure TCustomInputItem.SetMouseUpEvent(Value: TMouseEvent);
begin
  FMouseUpEvent:= Value;
end;

procedure TCustomInputItem.SetObjLeft(Value: integer);
begin
  if Assigned(FObj) then FObj.Left:=Value;
  UpDatePos;
end;

procedure TCustomInputItem.SetObjWidth(Value: integer);
begin
  if Assigned(FObj) then FObj.Width:=Value;
  UpDatePos;
end;

procedure TCustomInputItem.SetSelect(Selected: Boolean);
begin
  FSelected:= Selected;
  if FSelected then
     FSText.Color:= SelectColor
  else
    FSText.Color:= UnSelectColor;
end;

procedure TCustomInputItem.UpDatePos;
begin
  //
end;

{ TRegComboBox }

procedure TRegComboBox.Add(Caption: string);
begin
  TComboBox( FObj ).AddItem( Caption, nil );
end;

constructor TRegComboBox.Create(ID_: string; Parent: TWinControl; Caption: string = '' );
begin
  inherited;
  FObj:= ControlFactory.CreateComboBox(nil);
  with TComboBox(FObj) do
    begin
      Parent:= Self;
      Style:= csDropDownList;
      Font.Name:=DefaultFontName;
      OnChange:= OnObjectChange;
    end;
end;


function TRegComboBox.GetComboBoxWidth: Integer;
begin
  Result:= TComboBox(FObj).Width;
end;

function TRegComboBox.GetItemIndex: Integer;
begin
  Result:= TComboBox( FObj ).ItemIndex;
end;

function TRegComboBox.GetValue: Variant;
begin
  result:=TComboBox(FObj).Items[TComboBox(FObj).ItemIndex];
end;


procedure TRegComboBox.LoadFromFile(FileName: string);
begin
  TComboBox(FObj).Clear;
  TComboBox(FObj).Items.LoadFromFile(FileName);
  TComboBox(FObj).ItemIndex:=0;
end;

procedure TRegComboBox.Resize;
begin
  inherited;
  TComboBox(FObj).Font.Height:= Height;
end;

procedure TRegComboBox.SetColor(Value: TColor);
begin
  inherited;
  TComboBox(FObj).Font.Color:=Value;
end;

procedure TRegComboBox.SetComboBoxWidth(V: Integer);
begin
  TComboBox(FObj).Width:= V;
end;

procedure TRegComboBox.SetItemIndex(Value: Integer);
begin
  TComboBox( FObj ).ItemIndex:= Value;
end;

procedure TRegComboBox.SetValue(Value: Variant);
var
  i: Integer;
begin
  with TComboBox(FObj) do
    begin
      for i:=0 to Items.Count-1 do
        if Items[i]=Value then ItemIndex:=i;
    end;
end;


{ TRegValue }

constructor TRegValue.Create(ID_: string; Parent: TWinControl; Caption: string = '');
begin
  inherited;
  FObj:= TLabel.Create(nil);
  with TLabel(FObj) do
    begin
      Parent:= Self;
      Font.Name:=DefaultFontName;
      Transparent:= True;
      Caption:='';
      Visible:=true;
    end;
end;

function TRegValue.GetValue: Variant;
begin
  result:=TLabel(FObj).Caption;
end;

procedure TRegValue.Resize;
begin
  inherited;
  TLabel(FObj).Font.Height:= Height;
end;

procedure TRegValue.SetColor(Value: TColor);
begin
  inherited;
  TLabel(FObj).Font.Color:=Value;
end;

procedure TRegValue.SetMouseDownEvent(Value: TMouseEvent);
begin
  inherited;
  if Assigned(FObj) then TLabel(FObj).OnMouseDown:= Value;
end;

procedure TRegValue.SetMouseUpEvent(Value: TMouseEvent);
begin
  inherited;
  if Assigned(FObj) then TLabel(FObj).OnMouseUp:= Value;
end;

procedure TRegValue.SetValue(Value: Variant);
begin
  TLabel(FObj).Caption:=Value;
end;

{ TIDController }


class function TIDController.GetID(Element: TComponent): string;
var
  S: PString;
begin
  if Element.Tag = 0 then Result:= ''
  else Result:= PString( Element.Tag )^;
end;

class procedure TIDController.SetID(Element: TComponent; ID: string);
var
  S: PString;

begin
  if Element.Tag <> 0 then Dispose( PString( Element.Tag ) );
  New( S );
  S^:= ID;
  Element.Tag:= Integer( S );
end;


{ TRegTrackBar }

constructor TRegTrackBar.Create(ID_: string; Parent: TWinControl;
  Caption: string);
begin
  inherited;
  FObj:= ControlFactory.CreateScrollBar(nil);
  with TScrollBar(FObj) do
  begin
    Parent:= Self;
    //TickMarks:= tmBoth;
    PageSize:= 21;
    Min:= 0;
    Max:= 120;
    OnChange:= OnObjectChange;
  end;

end;

function TRegTrackBar.GetValue: Variant;
begin
  Result:= TScrollBar(FObj).Position;
end;

procedure TRegTrackBar.Resize;
begin
  inherited;
  FObj.Height:= Height;
end;

procedure TRegTrackBar.SetColor(Value: TColor);
begin
  inherited;
end;

procedure TRegTrackBar.SetValue(Value: Variant);
begin
  TScrollBar(FObj).Position:= Value;
end;

{ TRegNumber }

constructor TRegNumber.Create(ID_: string; Parent: TWinControl; Caption: string);
begin
  inherited;

  MinValue:= 0;
  MaxValue:= 10000;

  ScrollTimer:= TTimer.Create( Self );
  ScrollTimer.Enabled:= False;


  FObj:= ControlFactory.CreateLabel(Self);
  ControlFactory.InitializeLabel( TCustomLabel( FObj ), Height );
  with TCustomLabel(FObj) do
  begin
    Parent:= Self;
//    TCustomLabel(FObj).Parent:= Self;
    Caption:= '00000';
//    TCustomLabel(FObj).Caption:= Caption;
  end;

  FValue:= 0;

  BtnInc:= ControlFactory.CreateButton( Self );
  BtnInc.Parent:= Self;
  BtnInc.Caption:= '+';
  BtnInc.OnClick:= OnBtnInc;
  BtnInc.OnMouseDown:= OnBtnDown;
  BtnInc.OnMouseUp:= OnBtnUp;

  BtnDec:= ControlFactory.CreateButton( Self );
  BtnDec.Parent:= Self;
  BtnDec.Caption:= '-';
  BtnDec.OnClick:= OnBtnDec;
  BtnDec.OnMouseDown:= OnBtnDown;
  BtnDec.OnMouseUp:= OnBtnUp;

end;

function TRegNumber.GetValue: Variant;
begin
  Result:= FValue;
end;

procedure TRegNumber.OnBtnDec(Sender: TObject);
begin
  FValue:= FValue - 1;
  Refresh;
end;

procedure TRegNumber.OnBtnInc(Sender: TObject);
begin
  FValue:= FValue + 1;
  Refresh;
end;

procedure TRegNumber.OnScroll(Sender: TObject);
begin
  if ScrollTimer.Interval > 400 then ScrollTimer.Interval:= 100 else
  if ScrollTimer.Interval > 1 then ScrollTimer.Interval:= ScrollTimer.Interval - 1;
  FValue:= FValue + Delta;
  Refresh;
end;

procedure TRegNumber.OnBtnDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Sender = BtnInc then Delta:= 1 else Delta:= -1;
  ScrollTimer.Enabled:= False;
  ScrollTimer.OnTimer:= OnScroll;
  ScrollTimer.Interval:= 500;
  ScrollTimer.Enabled:= True;
end;

procedure TRegNumber.OnBtnUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  ScrollTimer.Enabled:= False;
end;

procedure TRegNumber.Refresh;
begin
  FValue:= FValue;
  TCustomLabel( FObj ).Caption:= IntToStr( FValue );
end;

procedure TRegNumber.Resize;
begin
  inherited;
  ControlFactory.InitializeLabel( TCustomLabel( FObj ), Height );
  FObj.Height:= Height;
  TCustomLabel( FObj ).Caption:= '00000';
  BtnDec.Height:= Height;
  BtnDec.Width:= Height;
  BtnDec.Left:= FObj.Left + FObj.Width + 10;
  BtnDec.Top:= 0;
  BtnDec.Font.Height:= Height;
  BtnInc.Height:= Height;
  BtnInc.Width:= Height;
  BtnInc.Left:= BtnDec.Left + BtnDec.Width + 3;
  BtnInc.Top:= 0;
  BtnInc.Font.Height:= Height;
  Refresh;
end;

procedure TRegNumber.SetValue(Value: Variant);
begin
  FValue:= Value;
  Refresh;
end;

procedure TRegNumber.SetFValue(V: Integer);
begin
  FFValue:= Max( MinValue, Min( MaxValue, V ) );
end;

{ TRegEditBox }

constructor TRegEditBox.Create(ID_: string; Parent: TWinControl;
  Caption: string);
begin
  inherited;
  FObj:= TEdit.Create(nil);
  with TEdit(FObj) do
    begin
      Parent:= Self;
      Font.Name:=DefaultFontName;
      Caption:='';
      Visible:=true;
    end;
end;

function TRegEditBox.GetValue: Variant;
begin

end;

procedure TRegEditBox.Resize;
begin
  inherited;

end;

procedure TRegEditBox.SetMouseDownEvent(Value: TMouseEvent);
begin
  inherited;
  if Assigned(FObj) then TEdit(FObj).OnMouseDown:= Value;
end;

procedure TRegEditBox.SetMouseUpEvent(Value: TMouseEvent);
begin
  inherited;
  if Assigned(FObj) then TEdit(FObj).OnMouseUp:= Value;
end;

procedure TRegEditBox.SetValue(Value: Variant);
begin
  inherited;

end;

{ TRegDate }

constructor TRegDate.Create(ID_: string; Parent: TWinControl;
  Caption: string);
begin
  inherited;
  FObj:= TDateTimePicker.Create(nil);
  TDateTimePicker(FObj).Kind:=dtkDate;
  with TDateTimePicker(FObj) do
    begin
      Parent:= Self;
      Font.Name:=DefaultFontName;
      Visible:=true;
    end;
end;

function TRegDate.GetValue: Variant;
begin
  result:=TDateTimePicker(FObj).Date;
end;

procedure TRegDate.Resize;
begin
  inherited;
  FObj.Height:=Height;
  TDateTimePicker(FObj).Font.Height:=Round(Height);
end;

procedure TRegDate.SetValue(Value: Variant);
begin
  inherited;
  TDateTimePicker(FObj).Date:=Value;
end;

{ TRegTime }

constructor TRegTime.Create(ID_: string; Parent: TWinControl;
  Caption: string);
begin
  inherited;
  FObj:= TDateTimePicker.Create(nil);
  TDateTimePicker(FObj).Kind:=dtkTime;
  with TDateTimePicker(FObj) do
    begin
      Parent:= Self;
      Font.Name:=DefaultFontName;
      Visible:=true;
    end;
end;

function TRegTime.GetValue: Variant;
begin
  result:=TDateTimePicker(FObj).Time;
end;

procedure TRegTime.Resize;
begin
  inherited;
  FObj.Height:=Height;
  TDateTimePicker(FObj).Font.Height:=Height;
end;

procedure TRegTime.SetValue(Value: Variant);
begin
  inherited;
  TDateTimePicker(FObj).Time:=Value;
end;

{ TRegButton }

constructor TRegButton.Create(ID_: string; Parent: TWinControl;
  Caption: string);
begin
  inherited;
  FSText.Visible:= false;

  OnClick:= nil;
  FBut:= ControlFactory.CreateButton( Self );
  FBut.Parent:= Self;
  FBut.WordWrap:= false;
  FBut.Caption:= Caption;

  FBut.Font.Height:= Height;
  FBut.Font.Name:= 'Impact';
  FBut.Font.Color:= clBlack;

  FBut.OnClick:= OnButClick;
end;

function TRegButton.GetCaption: string;
begin
  result:= FBut.Caption;
end;

function TRegButton.GetValue: Variant;
begin
  Result:= FBut.Caption;
end;

procedure TRegButton.OnButClick(Sender: TObject);
begin
  if Assigned(OnClick) then OnClick(Sender);
end;

procedure TRegButton.Resize;
begin
  inherited;

  FBut.Height:= Height-4;
  if FSText.Visible then
    begin
      FSText.Height:= Height;
      FSText.Font.Height:= Height;
      FBut.Width:= Width - FSText.Width - 10;
      FBut.Left:= FSText.Width + 10;
    end
  else
    FBut.Width:= Width;
  FBut.Font.Height:= Height;
  Refresh;
end;

procedure TRegButton.SetCaption(Value: string);
var
  FBmp: TBitMap;
begin
  FBmp:= TBitMap.Create;
  FBmp.Width:= FBut.Width;
  FBmp.Height:= FBut.Height;
  FBut.Font.Height:= Self.Height;
  FBmp.Canvas.Font.Height:= FBut.Font.Height;
  while (FBmp.Canvas.TextWidth(Value) > (FBut.Width - 20)) and (FBmp.Canvas.Font.Height > 8) do
    begin
      FBmp.Canvas.Font.Height:= FBmp.Canvas.Font.Height - 1;
    end;
  FBut.Font.Height:= FBmp.Canvas.Font.Height;
  FBmp.Free;
  FBut.Caption:= Value;
end;

procedure TRegButton.SetTextBeforeButton(Value: string);
begin
  FSText.Visible:= true;
  FSText.Caption:= Value;
  Self.Resize;
end;

procedure TRegButton.SetValue(Value: Variant);
begin
  FBut.Caption:= Value;
end;

{ TInputItemList }

function TInputItemList.Get(Index: Integer): TCustomInputItem;
begin
  Result:= inherited Get( Index );
end;

procedure TInputItemList.Put(Index: Integer; Item: TCustomInputItem);
begin
  inherited Put( Index, Item );
end;

{ TCustomElementList }

procedure TCustomElementList.AddItem(ID, Caption: string);
var
  NewItem: TCustomInputItem;
begin
  NewItem:= TCustomInputItem.Create(ID, Self, Caption);
  NewItem.Align:= alNone;
  NewItem.Top:= FYOffset;
  NewItem.Left:= 10;
  NewItem.Color:= Self_Color;
  NewItem.DoubleBuffered:= true;


  NewItem.Width:= Self.Width - 100;
  NewItem.Height:= Round(Self.Height/ceITEMSONSCREEN) - FItemsSpace;//!
  NewItem.Font.Height:= ceFONTHEIGHT;

  NewItem.OnMouseM:= GlobalMouseMove;
  NewItem.OnMouseD:= MD;
  NewItem.OnMouseU:= MU;

  NewItem.SelectColor:= FSelectColor;
  NewItem.UnSelectColor:= FUnSelectColor;
  NewItem.Selected:= false;

  FItemList.Add(NewItem);
  FYOffset:= FYOffset + NewItem.Height + FItemsSpace;
end;

procedure TCustomElementList.SaveToFile(FileName: String);
begin
   SaveToFile(FileName, Nil);
end;

procedure TCustomElementList.SaveToFile(FileName: String; Encoding: TEncoding);
var SList: TStringList;
    i:integer;
begin
  SList:=TStringList.Create();
  if Assigned(FItemList) then
   for i := 0 to FItemList.Count - 1 do begin
      SList.Add(FItemList.Items[i].Caption)
   end;
   SList.SaveToFile(FileName, Encoding);
   SList.Free;
end;

function TCustomElementList.ItemInList(ID:string):boolean;
var i:integer;
begin
   Result:=False;
   for i := 0 to FItemList.Count - 1 do
     if FItemList.Items[i].ID = ID then begin
       Result:=True; break;
     end;
end;

procedure TCustomElementList.SetBackGround(Color : TColor);
begin
  Self_Color:= Color;
end;

procedure TCustomElementList.Clear;
var
  i: Integer;
begin
  for i:= 0 to FItemList.Count - 1 do
     FItemList.Items[i].Free;
  FItemList.Clear;
  FYOffset:= 0;
end;

constructor TCustomElementList.Create;
begin
  inherited Create( nil );

  SelectionEnable:= true;

  FMaxPixCountNotMoving:= 10;

  FItemList:= TInputItemList.Create;
  FIsMouseMoving:= false;
  Self.Parent:= Parent_;

  Self.BevelOuter:= bvNone;
  Self.BorderWidth:= 0;
  Self.BorderStyle:= bsNone;

  Self.DoubleBuffered:= true;
  Self.OnMouseMove:= GlobalMouseMove;
  FYOffset:= 0;
  FItemsSpace:=ceItemSpace;//10;
  FStartPos:= 0;
  FEndPos:= 0;

  FSelectColor:= SelColor;
  FUnSelectColor:= UnSelColor;
end;

destructor TCustomElementList.Destroy;
var
  i: Integer;
begin
  for i:= 0 to FItemList.Count - 1 do
     FItemList.Items[i].Free;
  FItemList.Clear;
  FItemList.Free;
  inherited;
end;

procedure TCustomElementList.GlobalMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  SrcPoint, Point: TPoint;
  i, D: integer;
  Flag: Boolean;
begin
  SrcPoint.X:= X;
  SrcPoint.Y:= Y;
  Point:= TPanel(Sender).ClientToScreen(SrcPoint);

  if ( (Fomy-Point.Y) <> 0 ) then
    FIsMouseMoving:= true
  else
    FIsMouseMoving:= false;

  if ssLeft in Shift then
    begin
      if abs(FMouseDownY-Point.Y) > FMaxPixCountNotMoving then
        for i:= 0 to FItemList.Count - 1 do
           FItemList[i].Selected:= false;

      D:= -(Fomy-Point.Y);

      Flag:= true;

      if (D > 0) and (FItemList.Count > 0) then
        if (FItemList[0].Top+D > 0) then
          Flag:= false;

      if (D < 0) and (FItemList.Count > 0) then
        if ( ((Self.Height-FItemList[FItemList.Count-1].Top-D) >
             (FItemList[FItemList.Count-1].Height+FItemsSpace)) ) then
          Flag:= false;

      if Flag then
        begin
          Self.ScrollBy(0, D);
          Self.Repaint;
        end;
    end;

  Fomy:= Point.Y;
end;

procedure TCustomElementList.Scroll( Delta: Integer);
var
//  SrcPoint, Point: TPoint;
  i, D: integer;
  Flag: Boolean;
begin
{  SrcPoint.X:= X;
  SrcPoint.Y:= Y;
  Point:= TPanel(Sender).ClientToScreen(SrcPoint);

  if ( (Fomy-Point.Y) <> 0 ) then
    FIsMouseMoving:= true
  else
    FIsMouseMoving:= false;

  if ssLeft in Shift then
    begin

      if abs(FMouseDownY-Point.Y) > FMaxPixCountNotMoving then
        for i:= 0 to FItemList.Count - 1 do
           FItemList[i].Selected:= false;

      D:= -(Fomy-Point.Y);
}
      D:= Delta;

      Flag:= true;
      if Assigned(FItemList) and (FItemList.Count>0) then begin

//      if {(D > 0) and }(FItemList.Count > 0) then
        if (FItemList[0].Top+D > 0) then
          if (FItemList[0].Top > 0) then Flag:= false
          else D:= -FItemList[0].Top;



//      if (D < 0) and (FItemList.Count > 0) then
        if ( ((Self.Height-FItemList[FItemList.Count-1].Top-D) >
             (FItemList[FItemList.Count-1].Height+FItemsSpace)) ) then
          if ( ((Self.Height-FItemList[FItemList.Count-1].Top) >
             (FItemList[FItemList.Count-1].Height+FItemsSpace)) ) then
            Flag:= false
          else D:=(Self.Height-FItemList[FItemList.Count-1].Top) -
                  (FItemList[FItemList.Count-1].Height+FItemsSpace);

        if Flag then
        begin
          Self.ScrollBy(0, D);
          Self.Repaint;
        end;
      end;
//    end;

//  Fomy:= Point.Y;
end;

procedure TCustomElementList.MD(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  SrcPoint, Point: TPoint;

begin
  SrcPoint.X:= X;
  SrcPoint.Y:= Y;
  Point:= TPanel(Sender).ClientToScreen(SrcPoint);

  if ( (Fomy-Point.Y) <> 0 ) then
    FIsMouseMoving:= true
  else
    FIsMouseMoving:= false;

  if (not FIsMouseMoving) and (SelectionEnable)  then
    begin
      TCustomInputItem(Sender).Selected:= true;
      FMouseDownY:= Point.Y;
    end;
end;

procedure TCustomElementList.MU(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  Item: TCustomInputItem;
begin
  Item:= TCustomInputItem(Sender);
   if Item.Selected then
     if Assigned(OnItemSelected) then OnItemSelected(Item.ID);
   Item.Selected:= false;
end;

procedure TCustomElementList.Resize;
var
  i, H: integer;
begin
  inherited;

  FYOffset:= 0;
  FStartPos:= 0;
  FEndPos:= 0;

  H:= round(Self.Height/ceITEMSONSCREEN);
  for i:= 0 to FItemList.Count - 1 do
    begin
      FItemList[i].Width:= Self.Width - 100;
      FItemList[i].Height:= H;
      FItemList[i].Top:= FYOffset;
      FItemList[i].Left:= 10;
      FItemList[i].Font.Height:= ceFONTHEIGHT;
    end;
  FEndPos:= FYOffset + H;

end;
{ TSafeMaskEdit }

constructor TSafeMaskEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

(*
procedure TSafeMaskEdit.ValidateEdit;
var
  Str: string;
  Pos: Integer;
  key: Char;
begin
  Str := EditText;
  if IsMasked and Modified then
  begin

    while not Validate(Str, Pos) do
    begin
//      if not (csDesigning in ComponentState) then
//      begin
//        Include(FMaskState, msReEnter);
//        SetFocus;
//      end;
//      SetCursor(Pos);
//      Str[Pos]:='0';
//      Text[Pos]:='0';
      SetCursor(Pos);
      Key:='0';
      KeyPress(Key);
      Str := EditText;
//      ValidateError;
    end;
  end;
end;
*)

procedure TSafeMaskEdit.ValidateError;
var
  Str: string;
  Pos: Integer;
  key: Char;
begin
  Str := EditText;
//  if IsMasked and Modified then
//  begin
    while not Validate(Str, Pos) do
    begin
      SetCursor(Pos);
      Key:='0';
      KeyPress(Key);
      Str := EditText;
    end;
//  end;
end;


{ TRegChainage }

constructor TRegChainage.Create(ID_: string; Parent: TWinControl; FontName: string; FontColor: TColor;
  Caption: string; turkish: byte{boolean});
begin
  inherited Create(ID_, Parent, Caption);
  if Assigned(FObj) then
    FObj.Free;


  FObj:= TSafeMaskEdit.Create(Self);


  with TSafeMaskEdit(FObj) do
  begin
    Parent:= Self;
    AutoSize:= true;
    if turkish = 0 then
    begin
      EditMask:= '!00000\.000;_'; //'!90000\.000;_';
      Text:= '00000.000';
    end
    else
    if turkish = 1 then
    begin
      EditMask:= '!000\+000;_';
      Text:= '000+000';
    end
    else
    begin
      EditMask:= '!0000\.000;_';//'!9000\.000;_';
      Text:= '0000.000';
    end;

    Font.Name:= FontName;
    Font.Color:= FontColor;
    Font.Height:= Height;
  end;

end;

function TRegChainage.GetSelectedNum: integer;
var
  E: TSafeMaskEdit;
begin
  result:= -1;
  E:= TSafeMaskEdit(FObj);
  if E.SelLength <> 1 then  Exit;
  if (E.SelText= '.') or (E.SelText= '+') then Exit;
  FSelIdx:= E.SelStart;
  if (FSelIdx<0) then Exit;

  try
    result:= StrToInt(E.SelText);
  except
    Exit;
  end;

end;

procedure TRegChainage.LoadFromString(Val: string);
begin
  if (Assigned(FObj)) and (Val <> '') then
    TSafeMaskEdit(FObj).Text:= Val;
end;

procedure TRegChainage.OnBtnDec(Sender: TObject);
begin
  FFValue:= GetSelectedNum;
  Dec(FFValue);
  if FFValue<0 then FFValue:=9;
  SetValueToEdit;
end;

procedure TRegChainage.OnBtnDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //
end;

procedure TRegChainage.OnBtnInc(Sender: TObject);
begin
  FFValue:= GetSelectedNum;
  Inc(FFValue);
  if FFValue>=10 then FFValue:=0;
  SetValueToEdit;
end;

procedure TRegChainage.OnBtnUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //
end;

procedure TRegChainage.Refresh;
begin
  //inherited;
end;

procedure TRegChainage.Resize;
begin
  if Assigned(FSText) then
  begin
    FSText.Font.Height:= Height;
    FSText.Left:= 0;
  end;
  if Assigned(FObj) then
  if FObj.Left = 0 then FObj.Left:= Round( Width * 0.43 );

  TSafeMaskEdit(FObj).Height:= Height;
  TSafeMaskEdit(FObj).Font.Height:= Height;

  BtnDec.Height:= Height;
  BtnDec.Width:= Height;
  BtnDec.Left:= FObj.Left + FObj.Width + 10;
  BtnDec.Top:= 0;
  BtnDec.Font.Height:= Height;
  BtnInc.Height:= Height;
  BtnInc.Width:= Height;
  BtnInc.Left:= BtnDec.Left + BtnDec.Width + 3;
  BtnInc.Top:= 0;
  BtnInc.Font.Height:= Height;
end;

procedure TRegChainage.SetValueToEdit;
var
  E: TSafeMaskEdit;
  CurT, NumStr: string;
begin
  E:= TSafeMaskEdit(FObj);
  if FSelIdx<0 then Exit;
  CurT:= E.Text;
  NumStr:= IntToStr(FFValue);
  CurT[FSelIdx+1]:=NumStr[1];
  E.Text:= CurT;
  E.SetFocus;
  E.SelStart:= FSelIdx;
  E.SelLength:=1;
end;

function TRegChainage.ToString: string;
begin
  result:= '';
  if Assigned(FObj) then
    result:= TSafeMaskEdit(FObj).Text;
end;
{ TIndicator }

constructor TIndicator.Create(ID_: string; Parent: TWinControl;
  Caption: string);
begin
  inherited;
  Style:= isRect;
  Text:= '';
  Self.DoubleBuffered:= true;
  FObj:= TPaintBox.Create(Self);
  FObj.Parent:= Self;
  TPaintBox(FObj).Width:= Self.ObjWidth;
  TPaintBox(FObj).Height:= Self.Height;
  TPaintBox(FObj).Left:= Self.ObjLeft;
  TPaintBox(FObj).OnPaint:= PBPaint;
  FBMP:= TBitMap.Create;
  FBMP.Width:= TPaintBox(FObj).Width;
  FBMP.Height:= TPaintBox(FObj).Height;
  UpDatePicture;
end;

destructor TIndicator.Destroy;
begin
  FBMP.Free;
  inherited;
end;

procedure TIndicator.PBPaint(Sender: TObject);
begin
  TPaintBox(FObj).Canvas.Draw(0, 0, FBMP);
end;

procedure TIndicator.Resize;
begin
  inherited;
  TPaintBox(FObj).Width:= Self.ObjWidth;
  TPaintBox(FObj).Height:= Self.Height;
  TPaintBox(FObj).Left:= FSText.Left + FSText.Width + 10;
  FBMP.Width:= TPaintBox(FObj).Width;
  FBMP.Height:= TPaintBox(FObj).Height;
  UpDatePicture;
  TPaintBox(FObj).Repaint;
end;

procedure TIndicator.SetPercent(Value: integer);
begin
  if Value = FPercent then
    Exit;

  if Value < 0 then
    FPercent:= 0
  else if Value > 100 then
    FPercent:= 100
  else
    FPercent:= Value;

  UpDatePicture;
  TPaintBox(FObj).Repaint;
end;

procedure TIndicator.SetText(Value: string);
begin
  if FText = Value then
    Exit;
  FText:= Value;
  UpDatePicture;
  TPaintBox(FObj).Repaint;
end;

procedure TIndicator.UpDatePicture;
const
    BatteryTopWidth = 0.10;
    BatteryTopHeight = 0.2;
var
  Canv: TCanvas;
  R, G, B: Byte;
  W, OW, SH: integer;
  TX, TY: integer;
  BatteryTopWidthPix: integer;
  BatteryTopHeightPix: integer;
begin
  Canv:= FBMP.Canvas;
  // �������
  Canv.Brush.Color:= Self.Color;
  OW:= FBMP.Width;
  SH:= FBMP.Height - 10;
  Canv.FillRect(RECT(0, 0, FBMP.Width, FBMP.Height));

  W:= Round((FPercent/100)*OW);

  if Style = isRect then
    begin
      Canv.Brush.Color:= clWhite;
      Canv.FillRect(RECT(2, 11, OW-2, SH-2));

      if FPercent >= 50 then
        Canv.Brush.Color:= clGreen
      else if (FPercent < 50) and (FPercent >= 20) then
        Canv.Brush.Color:= clYellow
      else
        Canv.Brush.Color:= clRed;

      Canv.Pen.Color:= clBlack;
      Canv.Pen.Width:=2;
      Canv.Rectangle(1, 10, OW, SH);

      // ������� � ������ ���������
      Canv.FillRect(RECT(2, 11, W-2, SH-2));
    end;

  if Style = isBattery then
    begin
      BatteryTopWidthPix:= Round(BatteryTopWidth*OW);
      BatteryTopHeightPix:= Round(BatteryTopHeight*(SH-10));

      Canv.Brush.Color:= clWhite;
      Canv.FillRect(RECT(0, 10, OW-BatteryTopWidthPix, SH));
      Canv.FillRect(RECT(OW-BatteryTopWidthPix, 10+BatteryTopHeightPix, OW-1, SH-BatteryTopHeightPix));

      if FPercent >= 50 then
        Canv.Brush.Color:= clGreen
      else if (FPercent < 50) and (FPercent >= 20) then
        Canv.Brush.Color:= clYellow
      else
        Canv.Brush.Color:= clRed;

      if W < (OW-BatteryTopWidthPix) then
        Canv.FillRect(RECT(0, 10, W-1, SH))
      else
        begin
          Canv.FillRect(RECT(0, 10, OW-BatteryTopWidthPix, SH));
          Canv.FillRect(RECT(OW-BatteryTopWidthPix, 10+BatteryTopHeightPix, W-1, SH-BatteryTopHeightPix));
        end;


      Canv.Pen.Color:= clBlack;
      Canv.Pen.Width:=1;

      Canv.MoveTo(0, 10);
      Canv.LineTo(OW-BatteryTopWidthPix, 10);
      Canv.LineTo(OW-BatteryTopWidthPix, 10+BatteryTopHeightPix);
      Canv.LineTo(OW-1, 10+BatteryTopHeightPix);
      Canv.LineTo(OW-1, SH-BatteryTopHeightPix);
      Canv.LineTo(OW-BatteryTopWidthPix, SH-BatteryTopHeightPix);
      Canv.LineTo(OW-BatteryTopWidthPix, SH);
      Canv.LineTo(0, SH);
      Canv.LineTo(0, 10);

      Canv.MoveTo(1, 11);
      Canv.LineTo(OW-BatteryTopWidthPix-1, 11);
      Canv.LineTo(OW-BatteryTopWidthPix-1, 11+BatteryTopHeightPix);
      Canv.LineTo(OW-2, 11+BatteryTopHeightPix);
      Canv.LineTo(OW-2, SH-BatteryTopHeightPix-1);
      Canv.LineTo(OW-BatteryTopWidthPix-1, SH-BatteryTopHeightPix-1);
      Canv.LineTo(OW-BatteryTopWidthPix-1, SH-1);
      Canv.LineTo(1, SH-1);
      Canv.LineTo(1, 11);
    end;

  if Text <> '' then
    begin
      Canv.Font.Height:= Round(FBMP.Height*0.5);
      Canv.Font.Name:= 'Impact';
      Canv.Font.Color:= clBlack;
      TY:= Round((FBMP.Height - Canv.TextHeight(Text))/2);
      if Style = isRect then
        TX:= OW - Canv.TextWidth(Text)-5;
      if Style = isBattery then
        TX:= OW - Canv.TextWidth(Text)-5 -20;
      FBmp.Canvas.Brush.Style:=bsClear;
      Canv.TextOut(TX, TY, Text);
      FBmp.Canvas.Brush.Style:=bsSolid;
    end;
end;

end.

