unit ProgramProfileConfig;

{$I directives.inc}
interface

uses IniFiles, Forms, SysUtils;

type

    TProgramProfile = class
    private
      FINIFile: TINIFile;
      FConfigFileName: string;
      procedure Load;
    protected
    public
      ProgramTitle: string;
      UseAirBrush: Boolean;
      OnlyRussian: Boolean;
      RussianEnabled: Boolean;
      RegistrationFileExt: string;
      DefaultRailTypeTime: integer;
      AllowToChooseCoordSystem: Boolean;
      ShowOldFiles: Boolean;
      AlfaInstedKu: Boolean;
      NoSynchronization: Boolean;
      // ��������� � �������� ����������� ����������,
      // ����� �������� �������� Avicon16 ��� Avicon14
      DeviceFamily: string;
      // ����� ��� ������ �������� ������� �����. ���� = true �� ����� ��������
      WorkAndUnWordEdge: Boolean;
      constructor Create(ConfigFileName: string = '');
      destructor Destroy; override;
    end;

var
  ProgramProfile: TProgramProfile;

implementation


{ TProgramProfile }

constructor TProgramProfile.Create(ConfigFileName: string);
begin
  if ConfigFileName = '' then FConfigFileName:= ExtractFilePath(Application.ExeName) + 'Profile.ini' // ��� � ���� INI �����
                         else FConfigFileName:= ConfigFileName;

  FINIFile:= TINIFile.Create(FConfigFileName);
  NoSynchronization:= false;
  Load;
end;

destructor TProgramProfile.Destroy;
begin
  FINIFile.Free;
  inherited;
end;

procedure TProgramProfile.Load;
begin
{$IFDEF EGO_USW}
  ProgramTitle:= FINIFile.ReadString( 'Main', 'Title', 'EGO-USW');
{$ELSE}
  ProgramTitle:= FINIFile.ReadString( 'Main', 'Title', 'EGO US');
{$ENDIF}

  UseAirBrush:= FINIFile.ReadBool( 'Main', 'UseAirBrush', false);
  OnlyRussian:= FINIFile.ReadBool( 'Main', 'OnlyRussian', false);
  RussianEnabled:= FINIFile.ReadBool( 'Main', 'RussianEnabled', false);
  RegistrationFileExt:= FINIFile.ReadString( 'Main', 'RegistrationFileExt', 'a14');
{$IFDEF EGO_USW}
  DefaultRailTypeTime:= FINIFile.ReadInteger( 'Main', 'DefaultRailTypeTime', 58);
{$ELSE}
  DefaultRailTypeTime:= FINIFile.ReadInteger( 'Main', 'DefaultRailTypeTime', 64);
{$ENDIF}
  DeviceFamily:= FINIFile.ReadString( 'Main', 'DeviceFamily', 'Avicon14');
  AllowToChooseCoordSystem:= FINIFile.ReadBool( 'Main', 'AllowToChooseCoordSystem', false);
  ShowOldFiles:= FINIFile.ReadBool( 'Main', 'ShowOldFiles', false);
  AlfaInstedKu:= FINIFile.ReadBool( 'Main', 'AlfaInstedKu', true);
  WorkAndUnWordEdge:= FINIFile.ReadBool( 'Main', 'WorkAndUnWordEdge', true);
  NoSynchronization:= FINIFile.ReadBool( 'Main', 'NoSync', false );
end;

initialization
  ProgramProfile:= TProgramProfile.Create;

finalization
  ProgramProfile.Free;
  ProgramProfile:= nil;

end.
