/////////////////////////////////////////////////////
//// ����������� ������ ����������               ////
//// �������� ��������� ��� AScan, BScan � ��.   ////
////                                             ////
//// ������ �.�. ���� 2009                       ////
//// ������ 1.0.0                                ////
////                                             ////
/////////////////////////////////////////////////////

// ������� ���������

unit CustomInterfaceUnit;

interface

uses Classes, Graphics, Controls, DefCoreUnit, SysUtils, UtilsUnit, CfgTablesUnit_2010;

type

   TCustomInterfaceObject = class(TCustomControl)
   private
   protected
     FCore: TDefCore; // ������������������ ����
     FColorTable: TColorTable; // ������� ������� ���������� ���������
     FLanguageTable: TLanguageTable; // �������� �������
     FBackGrBmp: TBitMap; // �������� � �����, �.�. � ��� ������ �������� ������� �� ��������
     FBmp: TBitMap;       // �������� ��������, �� ������� �������� ������� ��� � ����� ����������� ������ ����������� ���������� (�������� �������, ���������� ��������� � �.�.)
     FUnits: TMeasureUnits; // ������� ���������
     procedure DrawBackGround; virtual;
     procedure DrawForeGround; virtual;
     procedure Resize; override;
   public
     constructor Create(AOwner: TComponent); override;
     destructor Destroy; override;
     procedure Repaint;override;
     procedure Paint;override;
     procedure ReBuild;
     procedure Refresh; virtual;

   published
     property Align;
     property Colors: TColorTable read FColorTable write FColorTable;
     property Language: TLanguageTable read FLanguageTable write FLanguageTable;
     property Core: TDefCore read FCore write FCore;
   end;

implementation

{ TCustomInterfaceObject }

constructor TCustomInterfaceObject.Create(AOwner: TComponent);
begin
  inherited;
  FColorTable:=nil;
  FLanguageTable:=nil;
  FBmp:=TBitMap.Create;
  FBackGrBmp:=TBitMap.Create;
  FBackGrBmp.Width:=Self.Width;
  FBackGrBmp.Height:=Self.Height;
  FBmp.Width:=Self.Width;
  FBmp.Height:=Self.Height;
  Self.DoubleBuffered:=true;
end;

destructor TCustomInterfaceObject.Destroy;
begin
  FBackGrBmp.Free;
  FBmp.Free;
  inherited;
end;

procedure TCustomInterfaceObject.DrawBackGround;
var
  W, H: Integer;
  St: string;
  T: TTime;
begin
  FBackGrBmp.Width:=Self.Width;
  FBackGrBmp.Height:=Self.Height;
  FBmp.Width:=Self.Width;
  FBmp.Height:=Self.Height;
  if (not Assigned(FColorTable)) or (not Assigned(FLanguageTable)) then
    begin
      FBackGrBmp.Canvas.Brush.Color:=clBlack;
      FBackGrBmp.Canvas.FillRect(Rect(0, 0, FBackGrBmp.Width, FBackGrBmp.Height));
      FBackGrBmp.Canvas.Font.Color:=clWhite;
      St:='ERROR ! Pallete and Language not found ! ������: �� ������� ������� ��� ������� ! ';
      W:=FBackGrBmp.Canvas.TextWidth(St);
      H:=FBackGrBmp.Canvas.TextHeight(St);
      FBackGrBmp.Canvas.TextOut((FBackGrBmp.Width-W) div 2, (FBackGrBmp.Height-H) div 2, St);
      Exit;
    end;
end;

procedure TCustomInterfaceObject.DrawForeGround;
begin
  //
end;

procedure TCustomInterfaceObject.Paint;
begin
  inherited;
  FBmp.Canvas.Draw(0, 0, FBackGrBmp);
  DrawForeGround; // ������������ �� FBmp ������������ ����� (�������, ������ � �.�.)
  Self.Canvas.Draw(0, 0, FBmp);
end;

procedure TCustomInterfaceObject.ReBuild;
begin
  // ������ ����������� ����
  DrawBackGround;
  RePaint;
end;

procedure TCustomInterfaceObject.Refresh;
begin
  RePaint;
end;

procedure TCustomInterfaceObject.Repaint;
begin
  inherited;
  Paint;
end;

procedure TCustomInterfaceObject.Resize;
begin
  inherited;
  FBmp.Width:=Self.Width;
  FBmp.Height:=Self.Height;
  FBackGrBmp.Width:=Self.Width;
  FBackGrBmp.Height:=Self.Height;
  ReBuild;
end;

end.





