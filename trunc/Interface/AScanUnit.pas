unit AScanUnit;

interface

uses Classes, Graphics, Controls, Forms, DefCoreUnit, SysUtils, UtilsUnit,
  Buttons,
  CustomInterfaceUnit, MCAN, Types, PublicData, BScanUnit, ExtCtrls, StdCtrls,
  Variants, Dialogs, CfgTablesUnit_2010, Windows, Math, ConfigObj,
  ProgramProfileConfig;
{$I Directives.inc}

const
  BottomAxisHeight = 35;
  MeasurePanelHeight = 20; // � ��������� �� ������ ASCanPanel
  BscanHeight = 30; // � ��������� �� ������ ASCanPanel
  OptionsPanWidth = 30; // � ��������� �� ������ AScanPanel

  AdjustButtonTopPanelHeight = 25;
  AdjustButtonWidth = 25; // � ��������� �� ������ ������ �� ������ ���������� ������
  PanButHeight = 100;

  AdjParamHeight = 150;

type
  TAScanPoly = array [0 .. AVBuffSize - 1 + 1] of TPoint;
  TMetkaPoly = array [0 .. 2] of TPoint;
  // TAlign = (alNone, alTop, alBottom, alLeft, alRight, alClient, alCustom);

  TAdjustValueType = (vtInt, vtReal, vtText);
  TAdjustPanelKind = (akOneStr, akTwoStr);
  TAdjustButtonStyle = (bsSimpleButton, bsParamButton);
  // bsSimpleButton - ������� TButton.
  // bsParamButton - �� �����������

  TAScan = class;

  TApproveNewValueEvent = function(NewValue: Variant; id: string)
    : Boolean of Object;

  TCenterClickEvent = procedure(id: string) of Object;

  TTextArr = array of String;

  TAdjustButton = class(TPanel)
  private
    FValue: Variant;
    FConstValue: Variant;
    FUnits: string;
    FLeftButton: TSpeedButton;
    FRightButton: TSpeedButton;
    // FLeftButton: TPaintBox;
    // FRightButton: TPaintBox;
    FCenterPanel: TPanel;
    FCaptionPan: TPanel;
    FValuePan: TPanel;
    FArrowColor: TColor;
    FLeftPress: Boolean;
    FRightPress: Boolean;
    FTimer: TTimer;
    FWaite: Integer;
    FKind: TAdjustPanelKind;
    FCaption: string;
    // Bmp: TBitMap;
    FisConstParam: Boolean;

    FCT: TColorTable;
    FLT: TLanguageTable;

    FStyle: TAdjustButtonStyle;
    RegBut: TSpeedButton;
    Caption2: string;

    procedure DrawArrows;
    procedure Resize; override;
    procedure UpdateValue;
    procedure IncValue;
    procedure DecValue;
    // function GetFontSize(const Index: Integer): integer;
    // procedure SetFontSize(const Index, Value: integer);
    // function GetFintSize(Index: integer): integer;
    // procedure SetFintSize(Index, Value: integer);
  protected
    function GetCaption: string;
    procedure SetUnits(Value: string);
    procedure SetCaption(Value: string);
    procedure SetValue(Value: Variant);
    procedure SetConstValue(Value: Variant);
    procedure OnCenterClick(Sender: Tobject);
    procedure OnLeftClick(Sender: Tobject);
    procedure OnRightClick(Sender: Tobject);
    procedure OnLeftDown(Sender: Tobject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure OnLeftUp(Sender: Tobject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure OnRightDown(Sender: Tobject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure OnRightUp(Sender: Tobject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure OnTimer(Sender: Tobject);
    procedure SetKind(Value: TAdjustPanelKind);
    function GetMyFontSize(Index: Integer): Integer;
    procedure SetMyFontSize(Index, Value: Integer);
    procedure SetStyle(Value: TAdjustButtonStyle);
    procedure AdjButtonDrawText(Text: string);
    procedure AdjButtonStatus(Stat: string);
  public
    ChangesEnabled: Boolean;
    id: string;
    Circled: Boolean;
    Max, Min, Delta: Variant;
    AdjustValueType: TAdjustValueType;
    TextArr: TTextArr;
    OnApprove: TApproveNewValueEvent;
    OnChange: TNotifyEvent;
    OnCenterPress: TCenterClickEvent;
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
    destructor Destroy; override;
    procedure Update;
    (*<Rud12>*)
    procedure ReDraw;
    (*</Rud12>*)
    property Value: Variant read FValue write SetValue;
    property ConstValue: Variant read FConstValue write SetConstValue;
    property Caption: string read GetCaption write SetCaption;
    property Units: string read FUnits write SetUnits;
    property Kind: TAdjustPanelKind read FKind write SetKind;
    property CaptionFontSize
      : Integer index 1 read GetMyFontSize write SetMyFontSize;
    property ValueFontSize
      : Integer index 2 read GetMyFontSize write SetMyFontSize;
    property Style: TAdjustButtonStyle read FStyle write SetStyle;
  end;

  TAdjustPanel = class(TPanel)
  private
    FButList: TList;
    FCT: TColorTable;
    FLT: TLanguageTable;
    FChangesEnabled: Boolean;
    procedure ParamChange(Sender: Tobject);
    procedure CenterPress(id: string);
    procedure Resize; override;
  protected
    function GetButCount: Integer;
    function GetButById(id: string): TAdjustButton;
    procedure SetButtonEnable(Enabled: Boolean);
  public
    id: string;
    OnApprove: TApproveNewValueEvent;
    OnParamChange: TNotifyEvent;
    OnCenterPress: TCenterClickEvent;
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
    destructor Destroy; override;
    procedure Update;
    function AddButton: TAdjustButton;
    property ButtonsCount: Integer read GetButCount;
    property Button[id: string]: TAdjustButton read GetButById;
    property ChangesEnabled: Boolean read FChangesEnabled write SetButtonEnable;
  end;

  TParamChangeEvent = procedure(id: string; Value: Variant) of object;
  TPanelChange = procedure(Num: Integer) of object;

  TAdjustFrame = class(TPanel)
  private
    FPanelSwitchButton: TAdjustButton;
    FPanList: TList;
    FBottomPanel: TPanel;
    FCT: TColorTable;
    FLT: TLanguageTable;
    FChangesEnabled: Boolean;
    procedure OnPanelSwitch(Sender: Tobject);
    procedure OnParamChange(Sender: Tobject);
    procedure CenterPress(id: string);
  protected
    function GetValue(id, ParamId: string): Variant;
    procedure SetValue(id, ParamId: string; Value: Variant);
    function GetConstValue(id: string): Variant;
    procedure SetConstValue(id: string; Value: Variant);
    function GetAdjPanelByIdx(id: string): TAdjustPanel;
    procedure SetButtonEnable(Enabled: Boolean);
    function GetCurPanelNum: integer;
  public
    onPanelChange: TPanelChange;
    OnApprove: TApproveNewValueEvent;
    ParamChangeEvent: TParamChangeEvent;
    OnCenterPress: TCenterClickEvent;
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
    destructor Destroy; override;
    procedure Update;
    function AddPanel: TAdjustPanel;
    procedure SetCurPanel(Num: Integer);
    property ParamValue[id, ParamId: string]
      : Variant read GetValue write SetValue;
    property ParamConstValue[id: string]
      : Variant read GetConstValue write SetConstValue;
    property Panels[id: string]: TAdjustPanel read GetAdjPanelByIdx;
    property ChangesEnabled: Boolean read FChangesEnabled write SetButtonEnable;
    property CurPanel: integer read GetCurPanelNum;
  end;

  TScaleType = (stH, stR);

  TMeasureFrame = class(TPanel)
  private
    FH: TPanel;
    FRight: TPanel;
    FBot: TPanel;
    FKan: TPanel;
    FKd: TPanel;
    FL: TPanel;
    FN: TPanel;
    FCT: TColorTable;
    FLT: TLanguageTable;
    FMode: TBScanWinMode;
    procedure Resize; override;
  protected
  public
    MMScaleType: TScaleType;
    AttVis: Boolean;
    AScanRef: TAScan;
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
    destructor Destroy; override;
    procedure Refresh;
    property Mode: TBScanWinMode read FMode write FMode;
  end;

  // ����� ��� �������� � ���������
  // �������� ��������� ��������� ������, ��� ���� ����� ����������� ���������� �����
  // ������ ��� ��� ���������� � �������� ���� ����������� ����������� ������� ��������
  // � ������� ������ �����
  TKanalParams = record
    Ku: Integer;
    ATT: byte;
    TwoTP: single;
    StrobSt: Integer;
    StrobEd: Integer;
    NStrobSt: Integer;
    NStrobEd: Integer;
    VRCH: Integer;
    Adj2TP: Boolean;
    KanNum: integer;
  end;

  TAScan = class(TCustomInterfaceObject)
  private
    FScanHeight: Integer;
    FAScanLength: Integer;
    FMaxT: single;
    FMaxA: single;
    FaP: TAScanPoly;
    FKanal: TKanal;
    FMX, FMY: Integer;
    FFixMaxT: single;
    FFixMaxA: single;
    FKanalOldParams: TKanalParams;
    FForceUpdatePlus: Boolean;
    FForceUpdateCount: Integer;
    // FZMOn: Boolean;
    // FZMLastPos: Integer;
    function NoiseReduction(A: Integer): Integer;
  protected
    procedure DrawBackGround; override;
    procedure DrawForeGround; override;
    procedure Resize; override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure Click; override;
    procedure SetKanal(Value: TKanal);
    function GetN: Integer;
    // procedure SetZMStat(Value: Boolean);
  public
    MMScaleType: TScaleType;
    ScaleLineClick: TNotifyEvent;
    FixMaximum: Boolean;

    procedure Stop;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure ClearPlus;
    procedure Refresh; override;
    property Kanal: TKanal read FKanal write SetKanal;
    property FixN: Integer read GetN;
    // property ZMEnabled: Boolean read FZMOn write SetZMStat;
  published
  end;

  { TAScanPanel1 = class(TPanel)
    private
    FCore: TDefCore; // ����
    FBScan: TBScan;
    FColorTable: TColorTable; // ������� ������� ���������� ���������
    FLanguageTable: TLanguageTable; // �������� �������
    FOptionsPan: TPanel;
    FMeasurePan: TPanel;
    protected
    public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Refresh; override;
    property Core: TDefCore read FCore write FCore;
    property Colors: TColorTable read FColorTable write FColorTable;
    property Language: TLanguageTable read FLanguageTable write FLanguageTable;
    published
    end; }

implementation

{ TAscan }

constructor TAScan.Create(AOwner: TComponent);
begin
  inherited;
  FixMaximum := false;
  FForceUpdatePlus := false;
  ScaleLineClick := nil;
  FKanal := nil;
  MMScaleType := stH;
end;

destructor TAScan.Destroy;
begin
  //
  inherited;
end;

procedure TAScan.DrawBackGround;
var
  UnitsSt, NumSt: string;
  N, C, W, Y, i, j, X, X1, W1, Num, dX: Integer;
begin
  inherited;
  if (not Assigned(FColorTable)) or (not Assigned(FLanguageTable)) then
    Exit;
  FBackGrBmp.Canvas.Brush.Color := FColorTable.Color['AScan_BeckGr'];
  FBackGrBmp.Canvas.FillRect(Rect(0, 0, FBackGrBmp.Width, FBackGrBmp.Height));
  FBackGrBmp.Canvas.Brush.Color := FColorTable.Color['AScan_TimeAxisBackGr'];
  // FScanHeight:=FScanHeight; // �������� �� Y �� ������� �������� ������� ����� ����� � ������ �������
  FBackGrBmp.Canvas.FillRect(Rect(0, FScanHeight, FBackGrBmp.Width,
      FBackGrBmp.Height));
  FBackGrBmp.Canvas.Pen.Color := FColorTable.Color['AScan_TimeAxisLines'];
  FBackGrBmp.Canvas.Brush.Color := FColorTable.Color['AScan_TimeAxisLines'];
  FBackGrBmp.Canvas.Pen.Width := 1;
  FBackGrBmp.Canvas.MoveTo(0, FScanHeight);
  FBackGrBmp.Canvas.LineTo(FBackGrBmp.Width, FScanHeight);

  // ��������� ����� �������
  // �������� ������ ��� ��������� ���� �� ����
  if Assigned(FCore) then
  begin
    { if Assigned(FKanal) then
      FAScanLength:=FCore.AScanLength
      else
      FAScanLength:=200; }
    FUnits := FCore.MeasureUnits;
    if FUnits = muMM then
    begin
      // if MMScaleType=stH then
      // if Assigned(FKanal) then FAScanLength:=FKanal.AScanLength.MM_H else FAScanLength:=200
      // FAScanLength:=FCore.AScanLength.MM_H
      // else
      if Assigned(FKanal) then
        FAScanLength := FKanal.AScanLength.MM_H
      else
        FAScanLength := 200;
      // if Assigned(FKanal) then FAScanLength:=FKanal.AScanLength.MM_R else FAScanLength:=200;
      // FAScanLength:=FCore.AScanLength.MM_R;
    end
    else if Assigned(FKanal) then
      FAScanLength := FKanal.AScanLength.Mks
    else
      FAScanLength := 200;
    // FAScanLength:=FCore.AScanLength.Mks;
  end
  else
  begin
    FAScanLength := 200;
    FUnits := muMKS;
  end;
  // ������� ����� �� ���������� ������� ���������

  if FUnits = muMKS then
    UnitsSt := FLanguageTable.Caption['���']
  else if FUnits = muMM then
  begin
    if Config.UnitsSystem in [usRus, usMetricTurkish] then
      UnitsSt := FLanguageTable.Caption['��']
    else
      UnitsSt := '"';
  end;

  // ������ ����� �������
  // ��������� �������� ����� ������� ����� ������ �����
  N := 1;
  repeat
    C := Round(FAScanLength / N); // ���-�� ����
    W := Round(self.Width / C); // ���������� ����� �������
    if W >= 40 then
      break;
    if N = 1 then
      N := 2
    else if N = 2 then
      N := 5
    else if N = 5 then
      N := 10
    else if N = 10 then
      N := 20
    else if N = 20 then
      N := 30
    else if N = 30 then
      N := 40
    else if N = 40 then
      N := 50
    else if N = 50 then
      N := 60
    else if N = 60 then
      N := 70
    else if N = 70 then
      N := 80
    else if N = 80 then
      N := 90
    else if N = 90 then
      N := 100
    else if N = 100 then
    begin
      N := 0;
      break;
    end;
  until false;
  // ����� ��������� ����� �����:
  // W - ������ � �������� ���������� ����� �������
  // N - �������� ��������� ���� (����� 5 ��� ����� 10 � �.�.). N = 0 ������� ���� ��� ����� �� ������ ���������
  // � - ���-�� ����
  // ������ ����� � �����

  Y := FScanHeight;
  FBackGrBmp.Canvas.Pen.Width := 1;
  FBackGrBmp.Canvas.Brush.Color := FColorTable.Color['AScan_TimeAxisBackGr'];
  FBackGrBmp.Canvas.Font.Color := FColorTable.Color['AScan_TimeAxisText'];
  // if UnitsSt = '"' then
  // FBackGrBmp.Canvas.Font.Size:=10
  // else
  FBackGrBmp.Canvas.Font.Size := 12;
  FBackGrBmp.Canvas.Font.Style := [fsBold];

  if N > 0 then
  begin
    i := 0;
    Num := 0;
    X := 0;
    repeat
      X := Round((Num * self.Width) / FAScanLength);
      X1 := Round(((Num + N) * self.Width) / FAScanLength);
      W1 := Round((X1 - X) / 5);
      for j := 1 to 4 do
      begin
        X1 := X + j * W1;
        FBackGrBmp.Canvas.MoveTo(X1, Y + 5);
        FBackGrBmp.Canvas.LineTo(X1, Y);
      end;
      if UnitsSt = '"' then
        NumSt := TUnitsConverter.ConvertMMToStr(Num, true)
      else
        NumSt := IntToStr(Num);
      if Num > 0 then
      begin
        dX := Round(FBackGrBmp.Canvas.TextWidth(NumSt) / 2);
        X1 := X - dX;
        if X + dX < self.Width - 50 then
          FBackGrBmp.Canvas.TextOut(X1, Y + 12, NumSt);
        FBackGrBmp.Canvas.MoveTo(X, Y + 10);
        FBackGrBmp.Canvas.LineTo(X, Y);
      end;
      Num := Num + N;
      if X >= self.Width then
        break;
    until Num >= FAScanLength;
  end;
  // ����� ������� ���������

  X := self.Width - FBackGrBmp.Canvas.TextWidth(UnitsSt) - 10;
  Y := self.Height - 25;
  FBackGrBmp.Canvas.TextOut(X, Y, UnitsSt);
end;

procedure TAScan.DrawForeGround;
var
  DefaultVRCH, i, NullLevel, ALength, StrobSt, StrobEd, X, Y, X1, Y1: Integer;
  ZonaM: Word;
  MP: TMetkaPoly;
begin
  inherited;
  if (not Assigned(FColorTable)) or (not Assigned(FLanguageTable)) then
    Exit;

  // FBmp.Canvas.FillRect(Rect(X, Y, X1, Y1));

  if Assigned(FKanal) then
  begin
    // ��������� ��������
    FBmp.Canvas.Pen.Width := 1;
    FBmp.Canvas.Pen.Color := clBlack;
    FBmp.Canvas.Brush.Color := FColorTable.Color['AScan_Signal'];
    FBmp.Canvas.Polygon(FaP);

    // ��������� �������
    if (Assigned(FCore)) and (Assigned(FKanal)) then
    begin
      // StrobSt:=FCore.StrobSt;
      // StrobEd:=FCore.StrobEd;
      // ALength:=FCore.AScanLengthMKS
      // ALength:=FCore.AScanLength.Mks
      { if FCore.MeasureUnits = muMM then
        ALength:=FKanal.AScanLength.MM_H
        else }
      ALength := FKanal.AScanLength.Mks;
      // StrobSt:=FCore.StrobSt;
      // StrobEd:=FCore.StrobEd;
      if FCore.Mode in [rNastr, rHandNastr] then
      begin
        if FCore.Adj2Tp then
        begin
          if (FKanal.Takt.Alfa = 0) and ((FKanal.Num mod 2) = 0) then
          begin
            StrobSt := FKanal.Str_st;
            StrobEd := FKanal.Str_en;
          end
          else
          begin
            StrobSt := FKanal.NStr2TP_st;
            StrobEd := FKanal.NStr2TP_en;
          end;
        end
        else
        begin
          StrobSt := FKanal.NStr_st;
          StrobEd := FKanal.NStr_en;
        end;
      end
      else
      begin
        StrobSt := FKanal.Str_st;
        StrobEd := FKanal.Str_en;
      end;
    end
    else
    begin
      StrobSt := 60;
      StrobEd := 120;
      ALength := 200;
    end;

    // ������������ ������ �� ��� � �������
    StrobSt := Round((StrobSt * self.Width) / ALength);
    StrobEd := Round((StrobEd * self.Width) / ALength);
    NullLevel := Round((FScanHeight * 32) / 256);
    NullLevel := FScanHeight - NullLevel;

    FBmp.Canvas.Pen.Color := FColorTable.Color['AScan_Strob'];
    FBmp.Canvas.Pen.Width := 4;
    FBmp.Canvas.MoveTo(StrobSt - 10, NullLevel - 10);
    FBmp.Canvas.LineTo(StrobSt, NullLevel);
    FBmp.Canvas.MoveTo(StrobSt - 10, NullLevel + 10);
    FBmp.Canvas.LineTo(StrobSt, NullLevel);
    FBmp.Canvas.LineTo(StrobEd, NullLevel);
    // FBmp.Canvas.MoveTo(StrobSt, NullLevel);
    FBmp.Canvas.LineTo(StrobEd + 10, NullLevel - 10);
    FBmp.Canvas.MoveTo(StrobEd, NullLevel);
    FBmp.Canvas.LineTo(StrobEd + 10, NullLevel + 10);

    // ����� ���
    FBmp.Canvas.Pen.Color := FColorTable.Color['AScan_VRCH'];
    FBmp.Canvas.Pen.Width := 3;
    if Assigned(FCore) then
    begin
      FBmp.Canvas.MoveTo(0, FScanHeight);
      for i := 0 to FCore.VRCHCurve.Count - 1 do
      begin
        X := FCore.VRCHCurve.Curve[i].X;
        { if ALength<>0 then
          if Width<2000 then }
        // if X<1000 then
        X := Round(X * Width / { 232 } ALength); // else
        // begin
        // X:=0;
        // end;
        Y := FCore.VRCHCurve.Curve[i].Y;
        // Y:=FScanHeight-Round(Y*(FScanHeight)/160);
        Y := FScanHeight - Round(Y * (FScanHeight / 255));
        FBmp.Canvas.LineTo(X, Y);
        // FCore.VRCHCurve.Curve[i].X;
      end;
    end
    else
    begin
      DefaultVRCH := 100;
      DefaultVRCH := Round((FScanHeight * DefaultVRCH) / 256);
      DefaultVRCH := FScanHeight - DefaultVRCH;
      FBmp.Canvas.MoveTo(0, DefaultVRCH);
      FBmp.Canvas.LineTo(self.Width, DefaultVRCH);
    end;

    // ���� �������� ������ � ������ ���������
    if FixMaximum then
    begin
      if (FFixMaxT > 0) and (FFixMaxA > 0) then
      begin
        X := Round((FFixMaxT * Width) / ALength);
        Y := FScanHeight - Round(FFixMaxA / 255 * (FScanHeight));
        FBmp.Canvas.Pen.Width := 6;
        FBmp.Canvas.Pen.Style := psSolid;
        FBmp.Canvas.Pen.Color := clBlack;
        { FBmp.Canvas.MoveTo(X, Y-8);
          FBmp.Canvas.LineTo(X, Y+8);
          FBmp.Canvas.MoveTo(X-8, Y);
          FBmp.Canvas.LineTo(X+8, Y); }
        FBmp.Canvas.MoveTo(X, Y - 20);
        FBmp.Canvas.LineTo(X, Y + 20);
        FBmp.Canvas.MoveTo(X - 20, Y);
        FBmp.Canvas.LineTo(X + 20, Y);
      end;
    end;

    // ��������� �����
    if not Assigned(FCore) then
      FMaxT := ALength / 2;

    if (FMaxT <> 0) and (FMaxA <> 0) then
    begin
      FBmp.Canvas.Pen.Width := 1;
      FBmp.Canvas.Pen.Style := psSolid;
      FBmp.Canvas.Pen.Color := clBlack;
      FBmp.Canvas.Brush.Color := FColorTable.Color['AScan_Metka'];
      X := Round((FMaxT * Width) / ALength);
      MP[0].X := X - 20;
      MP[0].Y := FScanHeight + 10;
      MP[1].X := X;
      MP[1].Y := FScanHeight - 10;
      MP[2].X := X + 20;
      MP[2].Y := FScanHeight + 10;
      FBmp.Canvas.Polygon(MP);
    end;

    // ���� �����
    if Assigned(FCore) then
      if FCore.ZMEnabled then
      begin
        ZonaM := FCore.ZMPosition;
        FBmp.Canvas.Pen.Width := 1;
        FBmp.Canvas.Pen.Style := psDash;
        FBmp.Canvas.Pen.Color := FColorTable.Color['AScan_ZonaM'];
        FBmp.Canvas.Brush.Color := FColorTable.Color['AScan_BeckGr'];
        X := Round(ZonaM - ZonaMSize);
        X1 := Round(ZonaM + ZonaMSize);
        X := Round((X * Width) / ALength);
        X1 := Round((X1 * Width) / ALength);
        Y := 0;
        Y1 := FScanHeight - 1;
        FBmp.Canvas.MoveTo(X, Y);
        FBmp.Canvas.LineTo(X, Y1);
        FBmp.Canvas.MoveTo(X1, Y);
        FBmp.Canvas.LineTo(X1, Y1);
      end;
    // else
    // ZonaM:=Round(ALength/2);

    { FBmp.Canvas.Pen.Width:=1;
      FBmp.Canvas.Pen.Style:=psDash;
      FBmp.Canvas.Pen.Color:=FColorTable.Color['AScan_ZonaM'];
      FBmp.Canvas.Brush.Color:=FColorTable.Color['AScan_BeckGr'];
      X:=Round(ZonaM-ZonaMSize);
      X1:=Round(ZonaM+ZonaMSize);
      X:=Round((X*Width)/ALength);
      X1:=Round((X1*Width)/ALength);
      Y:=0;
      Y1:=FScanHeight-1;
      FBmp.Canvas.MoveTo(X, Y);
      FBmp.Canvas.LineTo(X, Y1);
      FBmp.Canvas.MoveTo(X1, Y);
      FBmp.Canvas.LineTo(X1, Y1); }
  end;

end;

procedure TAScan.Click;
begin
  inherited;
  // ShowMessage('������ �� �-��������� !');
  // if FMY>FScanHeight then //ShowMessage('������ �� ������� ! !');
  if FMY > FScanHeight then
    if Assigned(ScaleLineClick) then
      ScaleLineClick(self);
end;

function TAScan.NoiseReduction(A: Integer): Integer;
begin
  if A < 16 then
    Result := 0
  else if A < 32 then
    Result := 2 * A - 32
  else
    Result := A;
end;

procedure TAScan.Refresh;
var
  AData: PAVBuff;
  i: Integer;
begin
  // �������� � ���� �-���������
  if Assigned(FCore) then
  begin
    if FCore.WaitingForBUM then
      Exit;

    AData := nil;
    FCore.GetAScan(AData);

    // ������������ ������ �-��������� � ���� ��������
    if Assigned(AData) then
    begin
      FMaxT := AData.Tmax; // ��������� �����
      FMaxA := AData.Amax;

      // ���� ������� ��������� ������ �� ����������, �� FForceUpdatePlus = false
      // � ������� �� ����� ���������� ������������� ������ ��������
      // ���� FForceUpdatePlus = true ��� ������ ��� �����-�� �������� ������ ���������
      // � ����� ��������� ������� ������� ���� ������� ��������� � ������ ����� ��������

      FForceUpdatePlus := not((FKanalOldParams.Ku = FKanal.Ky) and
          (FKanalOldParams.ATT = FKanal.ATT) and
          (FKanalOldParams.StrobSt = FKanal.Str_st) and
          (FKanalOldParams.StrobEd = FKanal.Str_en) and
          (FKanalOldParams.TwoTP = FKanal.TwoTP) and
          (FKanalOldParams.NStrobSt = FKanal.NStr_st) and
          (FKanalOldParams.NStrobEd = FKanal.NStr_en) and
          (FKanalOldParams.VRCH = FKanal.VRCH) and
          (FKanalOldParams.Adj2Tp = FCore.Adj2Tp) and
          (FKanalOldParams.KanNum = FKanal.Num));

      if FForceUpdatePlus then
        FForceUpdateCount := 10;

      if (FMaxA > FFixMaxA) or (FForceUpdateCount > 0) then
      begin
        FFixMaxT := FMaxT;
        FFixMaxA := FMaxA;
        if FForceUpdateCount > 0 then
          Dec(FForceUpdateCount);
      end;
      if FFixMaxA >= 254 then
        self.ClearPlus;

      // �������� ������� ��������� ������, � ��� ����� ��� ��������� ������ Refresh ������ ���������� �� ��� ��� ���
      // � ��� ���� ������ ����� �������� ������ ����� �������� ����������� ��� ���
      FKanalOldParams.Ku := FKanal.Ky;
      FKanalOldParams.ATT := FKanal.ATT;
      FKanalOldParams.StrobSt := FKanal.Str_st;
      FKanalOldParams.StrobEd := FKanal.Str_en;
      FKanalOldParams.TwoTP := FKanal.TwoTP;
      FKanalOldParams.NStrobSt := FKanal.NStr_st;
      FKanalOldParams.NStrobEd := FKanal.NStr_en;
      FKanalOldParams.VRCH := FKanal.VRCH;
      FKanalOldParams.Adj2TP:= FCore.Adj2Tp;
      FKanalOldParams.KanNum:= FKanal.Num;
      { FaP[0].X:= Left;
        FaP[0].Y:= Top + Height;
        for i:= 0 to High( AData.AV ) do
        begin
        FaP[i + 1].X:= Left + Round( i / Length( AData.AV ) * Width );
        FaP[i + 1].Y:= Top + Height - Round( AData.AV[i] / 255 * Height );
        end;
        FaP[ High( AData.AV ) + 1 ].X:= FaP[ High( AData.AV ) ].X;
        FaP[ High( AData.AV ) + 1 ].Y:= Top + Height;
        end; }
      FaP[0].X := 0;
      FaP[0].Y := FScanHeight;
      for i := 0 to High(AData.AV) do
      begin
        FaP[i + 1].X := Round(i / Length(AData.AV) * Width);
        FaP[i + 1].Y := FScanHeight - Round
          (NoiseReduction(AData.AV[i]) / 255 * (FScanHeight));
      end;
      FaP[ High(AData.AV) + 1].X := FaP[ High(AData.AV)].X;
      FaP[ High(AData.AV) + 1].Y := FScanHeight;
    end;
  end;
  inherited;
end;

procedure TAScan.Resize;
begin
  inherited;
  FScanHeight := Height - BottomAxisHeight;
  self.ReBuild;
end;

{ TAScanPanel }

{ constructor TAScanPanel1.Create(AOwner: TComponent);
  begin
  inherited;
  FCore:=nil;
  Self.DoubleBuffered:=true;
  Self.BevelOuter:=bvNone;
  //  Self.Colors:=

  //  FMeasurePan:=TPanel;
  FMeasurePan:=TPanel.Create(nil);
  FMeasurePan.Parent:=Self;
  FMeasurePan.Height:=Round((Self.Height/100)*MeasurePanelHeight);
  FMeasurePan.Align:=alTop;
  FMeasurePan.Color:=Self.Color;
  FMeasurePan.DoubleBuffered:=true;

  FBScan:=TBScan.Create(nil);





  //   MeasurePanelHeight = 20; // � ��������� �� ������ ASCanPanel
  //   BscanHeight = 30; // � ��������� �� ������ ASCanPanel
  //  OptionsPanWidth = 30; // � ��������� �� ������ AScanPanel
  //   FMeasurePan.A
  //FOptionsPan: TPanel;

  end;

  destructor TAScanPanel1.Destroy;
  begin
  FBScan.Free;
  FMeasurePan.Free;
  FOptionsPan.Free;
  inherited;
  end;

  procedure TAScanPanel1.Refresh;
  begin
  inherited;

  end; }

procedure TAScan.SetKanal(Value: TKanal);
begin
  FKanal := Value;
  if Assigned(FKanal) then
  begin
    FKanalOldParams.Ku := FKanal.Ky;
    FKanalOldParams.ATT := FKanal.ATT;
    FKanalOldParams.StrobSt := FKanal.Str_st;
    FKanalOldParams.StrobEd := FKanal.Str_en;
    FKanalOldParams.TwoTP := FKanal.TwoTP;
    FKanalOldParams.NStrobSt := FKanal.NStr_st;
    FKanalOldParams.NStrobEd := FKanal.NStr_en;
    FKanalOldParams.VRCH := FKanal.VRCH;
  end;

  FCore.ZMPosition := 0;
  FCore.ZMEnabled := false;

  FFixMaxT := 0;
  FFixMaxA := 0;
  DrawBackGround;
end;

{ procedure TAScan.SetZMStat(Value: Boolean);
  begin
  if FZMOn <> Value then
  begin
  FZMOn:=Value;
  if Value = false then
  FZMLastPos:=FKanal.Takt.ZonaM
  else
  FKanal.Takt.ZonaM:=FZMLastPos;
  FKanal.Takt.UseZM:=Value;
  end;
  end; }

procedure TAScan.Stop;
begin
  FCore.StopAScan;
  FKanal := nil;
end;

procedure TAScan.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  FMX := X;
  FMY := Y;
end;

function TAScan.GetN: Integer;
var
  res: Integer;
begin
  if FFixMaxA < 31 then
    res := -1
  else if FFixMaxA = 255 then
    res := 19
  else
    res := Max(0, Round(20 * log10(FFixMaxA / 32)));
  Result := res;
end;

procedure TAScan.ClearPlus;
begin
  FFixMaxT := 0;
  FFixMaxA := 0;
end;

{ TAdjustButton }

constructor TAdjustButton.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);
begin
  inherited Create(nil);
  ChangesEnabled:= true;
  FCT := CT;
  FLT := LT;
  FStyle := bsParamButton;
  SetLength(TextArr, 0);
  OnApprove := nil;
  OnCenterPress := nil;
  // FConstValue:=EmptyParam;
  FisConstParam := false;
  // Bmp:=TBitMap.Create;
  Circled := false;
  self.Parent := Panel;
  self.ParentColor := false;
  // Self.Color:=Panel.Color;
  // Self.Color:=CT.Color['AdjustBut_BackGr'];
  self.Color := clWhite;
  self.BevelOuter := bvNone;
  self.DoubleBuffered := true;
  self.Ctl3D := false;
  self.BorderStyle := bsSingle;
  self.BorderWidth := 1;

  Delta := 1;

  FLeftButton := TSpeedButton.Create(self);
  // FLeftButton:=TPaintBox.Create(Self);
  FLeftButton.Parent := self;
  FLeftButton.Align := alLeft;
  // FLeftButton.Flat:=true;
  FLeftButton.Layout := blGlyphLeft;
  FArrowColor := CT.Color['AdjustBut_Arrow'];
  FLeftButton.Width := Round((self.Width / 100) * AdjustButtonWidth);
  FLeftButton.OnClick := OnLeftClick;
  FLeftButton.OnMouseDown := OnLeftDown;
  FLeftButton.OnMouseUp := OnLeftUp;

  RegBut := TSpeedButton.Create(self);
  RegBut.Parent := self;
  RegBut.Visible := false;
  RegBut.Align := alNone;
  RegBut.OnClick := OnCenterClick;

  FRightButton := TSpeedButton.Create(self);
  // FRightButton:=TPaintBox.Create(Self);
  FRightButton.Parent := self;
  FRightButton.Align := alRight;
  // FRightButton.Flat:=true;
  FRightButton.Layout := blGlyphRight;
  FRightButton.Width := Round((self.Width / 100) * AdjustButtonWidth);
  FRightButton.OnClick := OnRightClick;
  FRightButton.OnMouseDown := OnRightDown;
  FRightButton.OnMouseUp := OnRightUp;

  FCenterPanel := TPanel.Create(self);
  FCenterPanel.Parent := self;
  FCenterPanel.ParentBackground := false;
  // FCenterPanel.Color:=Self.Color;
  FCenterPanel.ParentColor := false;
  FCenterPanel.Color := clWhite;
  FCenterPanel.Align := alClient;
  FCenterPanel.BevelOuter := bvNone;
  // FCenterPanel.OnClick:=OnCenterClick;

  FCaptionPan := TPanel.Create(FCenterPanel);
  FCaptionPan.Parent := FCenterPanel;
  FCaptionPan.ParentBackground := false;
  FCaptionPan.Color := FCenterPanel.Color;
  FCaptionPan.Align := alClient;
  // FCaptionPan.Height:=FCenterPanel.Height div 2;
  FCaptionPan.Font.Name := DefaultFontName;
  FCaptionPan.Font.Height := FCaptionPan.Height - 5;
  // FCaptionPan.Font.Height:=FCaptionPan.Height-20;
  FCaptionPan.Font.Color := CT.Color['AdjustBut_Text'];
  FCaptionPan.BevelOuter := bvNone;
  FCaptionPan.DoubleBuffered := true;
  FCaptionPan.OnClick := OnCenterClick;

  FValuePan := TPanel.Create(FCenterPanel);
  FValuePan.Parent := FCenterPanel;
  FValuePan.ParentBackground := false;
  FValuePan.Color := FCenterPanel.Color;
  FValuePan.Align := alBottom;
  FValuePan.Height := FCenterPanel.Height div 2;
  FValuePan.Font.Name := DefaultFontName;
  FValuePan.Font.Height := FValuePan.Height - 5;
  // FValuePan.Font.Height:=FValuePan.Height-20;
  FValuePan.Font.Color := CT.Color['AdjustBut_Text'];
  FValuePan.BevelOuter := bvNone;
  FValuePan.DoubleBuffered := true;
  FValuePan.OnClick := OnCenterClick;

  FLeftPress := false;
  FRightPress := false;

  FTimer := TTimer.Create(nil);
  FTimer.Interval := 50;
  FTimer.Enabled := true;
  FTimer.OnTimer := OnTimer;
  // FValuePan: TPanel;

  { FCenterPanel.Font.Name:=DefaultFontName;
    FCenterPanel.Font.Height:=FCenterButton.Height-5; }

end;

procedure TAdjustButton.DecValue;
var
  OldValue: Variant;
  NewValue: Variant;
  flag,flagOld: Boolean;
begin
  OldValue := FValue;
  NewValue := OldValue;
  if FValue > Min then
    NewValue := FValue - Delta
  else if Circled then
    NewValue := Max;
  if OldValue <> NewValue then
  begin
    flag := true;
//    if Assigned(OnApprove) then
//      flag := OnApprove(NewValue, self.id);
    if Assigned(OnApprove) then begin
      flag := OnApprove(NewValue, self.id);
      if (not flag) and (not OnApprove(OldValue, self.id)) then flag:=True;
    end;
    if flag then
    begin
      FValue := NewValue;
      UpdateValue;
      if Assigned(OnChange) then
        OnChange(self);
    end;
  end;
end;

destructor TAdjustButton.Destroy;
begin
  FTimer.Enabled := false;
  FTimer.Free;
  SetLength(TextArr, 0);
  TextArr := nil;
  // Bmp.Free;
  inherited;
end;

procedure TAdjustButton.DrawArrows;
var
  P: array [0 .. 2] of TPoint;
  P1: array [0 .. 3] of TPoint;
begin

  FLeftButton.Glyph.Width := FLeftButton.Width;
  FLeftButton.Glyph.Height := FLeftButton.Height;

  // CT.Color['AdjustBut_BackGr'];
  // FCenterPanel.Color;
  FLeftButton.Glyph.Canvas.Brush.Color := clBlack;
  FLeftButton.Glyph.Canvas.FillRect
    (Rect(0, 0, FLeftButton.Width, FLeftButton.Height));

  P[0].X := FLeftButton.Width - 10;
  P[0].Y := 10;
  P[1].X := FLeftButton.Width - 10;
  P[1].Y := FLeftButton.Height - 10;
  P[2].X := 10;
  P[2].Y := FLeftButton.Height div 2;

  P1[0].X := 2;
  P1[0].Y := 2;
  P1[1].X := FLeftButton.Width - 3;
  P1[1].Y := 2;
  P1[2].X := FLeftButton.Width - 3;
  P1[2].Y := FLeftButton.Height - 3;
  P1[3].X := 2;
  P1[3].Y := FLeftButton.Height - 3;

  FLeftButton.Glyph.Canvas.Pen.Color := FCenterPanel.Color;
  (*<Rud12>*)
  if self.Enabled = true then
    FLeftButton.Glyph.Canvas.Brush.Color := FCenterPanel.Color
  else
    FLeftButton.Glyph.Canvas.Brush.Color := clSilver;
  (*</Rud12>*)

  FLeftButton.Glyph.Canvas.Polygon(P1);

  FLeftButton.Glyph.Canvas.Pen.Color := clBlack;
  FLeftButton.Glyph.Canvas.Brush.Color := FArrowColor;
  FLeftButton.Glyph.Canvas.Polygon(P);

  FRightButton.Glyph.Width := FRightButton.Width;
  FRightButton.Glyph.Height := FRightButton.Height;

  FRightButton.Glyph.Canvas.Brush.Color := clBlack;
  FRightButton.Glyph.Canvas.FillRect
    (Rect(0, 0, FLeftButton.Width, FLeftButton.Height));

  P[0].X := 10;
  P[0].Y := 10;
  P[1].X := 10;
  P[1].Y := FRightButton.Height - 10;
  P[2].X := FRightButton.Width - 5;
  P[2].Y := FRightButton.Height div 2;

  P1[0].X := 3;
  P1[0].Y := 3;
  P1[1].X := FRightButton.Width - 3;
  P1[1].Y := 3;
  P1[2].X := FRightButton.Width - 3;
  P1[2].Y := FRightButton.Height - 3;
  P1[3].X := 3;
  P1[3].Y := FRightButton.Height - 3;

  FRightButton.Glyph.Canvas.Pen.Color := FCenterPanel.Color;
  (*<Rud12>*)
  if self.Enabled = true then
    FRightButton.Glyph.Canvas.Brush.Color := FCenterPanel.Color
  else
    FRightButton.Glyph.Canvas.Brush.Color := clSilver;
  (*</Rud12>*)

  FRightButton.Glyph.Canvas.Polygon(P1);

  FRightButton.Glyph.Canvas.Pen.Color := clBlack;
  FRightButton.Glyph.Canvas.Brush.Color := FArrowColor;

  FRightButton.Glyph.Canvas.Polygon(P);

  // FRightButton.Repaint;
  // FLeftBMP.Canvas.Brush.Color:=clGreen;
  // FLeftBMP.Canvas.FillRect(rect(0,0,20,20));
  // FLeftButton.Glyph.Assign(FLeftBMP);
  // FLeftButton.Glyph:=FLeftBMP;
end;

function TAdjustButton.GetCaption: string;
begin
  Result := FCaptionPan.Caption;
end;

function TAdjustButton.GetMyFontSize(Index: Integer): Integer;
begin
  case Index of
    1:
      Result := FCaptionPan.Font.Height;
    2:
      Result := FValuePan.Font.Height;
  end;
end;

procedure TAdjustButton.SetMyFontSize(Index, Value: Integer);
begin
  case Index of
    1:
      FCaptionPan.Font.Height := Value;
    2:
      FValuePan.Font.Height := Value;
  end;
end;

procedure TAdjustButton.IncValue;
var
  OldValue: Variant;
  NewValue: Variant;
  flag: Boolean;
begin
  OldValue := FValue;
  NewValue := OldValue;
  if FValue < Max then
    NewValue := FValue + Delta
  else if Circled then
    NewValue := Min;
  if OldValue <> NewValue then
  begin
    flag := true;
    if Assigned(OnApprove) then
      flag := OnApprove(NewValue, self.id);
    if flag then
    begin
      FValue := NewValue;
      UpdateValue;
      if Assigned(OnChange) then
        OnChange(self);
    end;
  end;
end;

procedure TAdjustButton.OnLeftClick(Sender: Tobject);
begin
  { if FValue>Min then FValue:=FValue-1;
    UpdateValue; }
  if ChangesEnabled then
    DecValue;
end;

procedure TAdjustButton.OnLeftDown(Sender: Tobject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FLeftPress := true;
  FWaite := 0;
end;

procedure TAdjustButton.OnLeftUp(Sender: Tobject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FLeftPress := false;
  FWaite := 0;
end;

procedure TAdjustButton.OnRightClick(Sender: Tobject);
begin
  if ChangesEnabled then
    IncValue;
end;

procedure TAdjustButton.OnRightDown(Sender: Tobject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FRightPress := true;
  FWaite := 0;
end;

procedure TAdjustButton.OnRightUp(Sender: Tobject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FRightPress := false;
  FWaite := 0;
end;

procedure TAdjustButton.OnTimer(Sender: Tobject);
begin
  // if FLeftPress then DecValue;
  // if FRightPress then IncValue;
  if (((FLeftPress) or (FRightPress)) and (FWaite < 5)) then
    inc(FWaite);

  if ((FLeftPress) and (FWaite >= 5)) then
    DecValue
  else if ((FRightPress) and (FWaite >= 5)) then
    IncValue;
end;

procedure TAdjustButton.Resize;
begin
  inherited;
  FLeftButton.Width := Round((self.Width / 100) * AdjustButtonWidth);
  FRightButton.Width := Round((self.Width / 100) * AdjustButtonWidth);

  if FKind = akTwoStr then
  begin
    FValuePan.Height := FCenterPanel.Height div 2;
    FCaptionPan.Height := FValuePan.Height;
  end
  else
  begin
    FValuePan.Font.Height := 0;
    FCaptionPan.Height := FCenterPanel.Height;
  end;

  // FCaptionPan.Font.Height:=FCaptionPan.Height-15;
  // FValuePan.Font.Height:=FCaptionPan.Font.Height;
  DrawArrows;
end;

procedure TAdjustButton.SetCaption(Value: string);
begin
  FCaption := Value;
  FCaptionPan.Caption := FCaption;
end;

procedure TAdjustButton.SetKind(Value: TAdjustPanelKind);
begin
  FKind := Value;
  case FKind of
    akOneStr:
      begin
        FValuePan.Height := 0;
      end;
    akTwoStr:
      begin
        FValuePan.Font.Height := FCaptionPan.Font.Height;
      end;
  end;
end;

procedure TAdjustButton.SetUnits(Value: string);
begin
  FUnits := Value;
  UpdateValue;
end;

procedure TAdjustButton.SetValue(Value: Variant);
begin
  if Value <> FValue then
  begin
    FValue := Value;
    UpdateValue;
  end;
end;

procedure TAdjustButton.Update;
begin
  self.UpdateValue;
end;

(*<Rud12>*)
procedure TAdjustButton.ReDraw;
begin
  DrawArrows;
end;
(*</Rud12>*)

procedure TAdjustButton.UpdateValue;
var
  val, val2: string;
  st: string;
  flag: Boolean;
  W, ValueMM: Integer;
begin
  val2 := '';
  case AdjustValueType of
    vtInt:
      begin
        val := IntToStr(Integer(FValue));
        ValueMM := TUnitsConverter.ConvertMKS2MM(Integer(FValue));
        if FUnits = FLT['��'] then
          val := IntToStr(Integer(ValueMM))
        else if FUnits = '"' then
          val := TUnitsConverter.ConvertMMToStr(Integer(ValueMM));
        // val:=IntToStr(Integer(FValue));

        if FisConstParam then
        begin
          val2 := IntToStr(Integer(FConstValue));
          if FUnits = FLT['��'] then
            val2 := TUnitsConverter.ConvertMMToStr(Integer(FConstValue));
        end
      end;
    vtReal:
      begin
        val := Format('%4.1f', [single(FValue)]);
        if FisConstParam then
          val2 := Format('%4.1f', [single(FConstValue)]);
      end;
    vtText:
      begin
        // val:=FValue;
        if FValue <= High(TextArr) then
          val := TextArr[Integer(FValue)];
      end;
  end;
  case FKind of
    akOneStr:
      begin
        st := Format('%s %s %s', [FCaption, val, FUnits]);
        FCaptionPan.Caption := st;
      end;
    akTwoStr:
      begin
        if val2 <> '' then
          st := Format('%s (%s) %s', [val, val2, FUnits])
        else
          st := Format('%s %s', [val, FUnits]);
        FValuePan.Caption := st;
      end;
  end;

  { Bmp.Canvas.Font.Height:=Self.Height-5;
    Bmp.Canvas.Font.Name:=DefaultFontName;
    W:=Bmp.Canvas.TextWidth(st);
    //Self.Canvas.Font.Height:=FValuePan.Font.Height;
    //Self.Canvas.Font.Name:=DefaultFontName;
    //W:=Self.Canvas.TextWidth(st);
    while((W>(FCaptionPan.Width+6)) and (Bmp.Canvas.Font.Height>1)) do
    begin
    Bmp.Canvas.Font.Height:=Bmp.Canvas.Font.Height-1;
    W:=Bmp.Canvas.TextWidth(st);
    //Self.Canvas.Font.Height:=Self.Canvas.Font.Height-1;
    //W:=Self.Canvas.TextWidth(st);

    end;
    //FValuePan.Font.Height:=Self.Canvas.Font.Height;
    FCaptionPan.Font.Height:=Bmp.Canvas.Font.Height; }

  // Bmp.Free;
  // Self.Canvas.Te
  // FValuePan.Caption:=Format('%s %s',[val, FUnits]);
end;

procedure TAdjustButton.OnCenterClick(Sender: Tobject);
begin
  if Assigned(OnCenterPress) then
    OnCenterPress(self.id);
end;

procedure TAdjustButton.SetConstValue(Value: Variant);
begin
  if Value <> FConstValue then
  begin
    FConstValue := Value;
    FisConstParam := true;
    UpdateValue;
  end;
end;

procedure TAdjustButton.SetStyle(Value: TAdjustButtonStyle);
begin
  if FStyle <> Value then
  begin
    case Value of
      bsSimpleButton:
        begin
          FLeftButton.Visible := false;
          FRightButton.Visible := false;
          FCenterPanel.Visible := false;
          FCaptionPan.Visible := false;
          FValuePan.Visible := false;
          RegBut.Align := alClient;
          RegBut.Visible := true;
          AdjButtonStatus('start');
        end;
      bsParamButton:
        begin
          RegBut.Align := alNone;
          RegBut.Visible := false;
          FLeftButton.Visible := true;
          FRightButton.Visible := true;
          FCenterPanel.Visible := true;
          FCaptionPan.Visible := true;
          FValuePan.Visible := true;
        end;
    end;
  end;
end;

procedure TAdjustButton.AdjButtonDrawText(Text: string);
var
  R: TRect;
  N: Integer;
  Buff: array [0 .. 255] of Char;

begin
  if Assigned(RegBut) then
    with RegBut do
    begin
      Glyph.Canvas.Font := RegBut.Font;
      Glyph.Width := RegBut.Width - 6;
      Glyph.Height := RegBut.Height - 6;
      Glyph.Canvas.FillRect(Rect(0, 0, Glyph.Width, Glyph.Height));
      R := Bounds(0, 0, Glyph.Width, 0);
      StrPCopy(Buff, Text);
      Caption := '';
      DrawText(Glyph.Canvas.Handle, Buff, StrLen(Buff), R,
        DT_CENTER or DT_WORDBREAK or DT_CALCRECT);
      OffsetRect(R, (Glyph.Width - R.Right) div 2, (Glyph.Height - R.Bottom)
          div 2);
      DrawText(Glyph.Canvas.Handle, Buff, StrLen(Buff), R,
        DT_CENTER or DT_WORDBREAK);
    end;
end;

procedure TAdjustButton.AdjButtonStatus(Stat: string);
begin
  if RegBut.Visible then
    if Assigned(RegBut) then
    begin
      if Stat = 'Start' then
      begin
        RegBut.Font.Color := FCT.Color['AdjButFontColorNorm'];
        AdjButtonDrawText
          (FLT.Caption['�����'] + ' ' + FLT.Caption['�����������']);
      end;
      if Stat = 'RegOn' then
      begin
        RegBut.Font.Color := FCT.Color['AdjButFontColorOK'];
        AdjButtonDrawText
          (FLT.Caption['����'] + ' ' + FLT.Caption['Registration']);
      end;
      if Stat = 'Clear' then
      begin
        RegBut.Glyph.Width := 0;
        RegBut.Glyph.Height := 0;
        RegBut.Font.Color := FCT.Color['AdjButFontColorNorm'];
        RegBut.Caption := FLT.Caption['���������'];
      end;
      if Stat = 'Adj_OK' then
      begin
        RegBut.Font.Color := FCT.Color['AdjButFontColorOK'];
        AdjButtonDrawText(FLT.Caption['���������'] + ' ' + FLT.Caption
            ['��������']);
      end;
      if Stat = 'Adj_Error' then
      begin
        RegBut.Font.Color := FCT.Color['AdjButFontColorError'];
        AdjButtonDrawText(FLT.Caption['���������'] + ' ' + FLT.Caption['������']
          );
      end;
    end;
end;

{ TAdjustPanel }

function TAdjustPanel.AddButton: TAdjustButton;
var
  But: TAdjustButton;
begin
  But := TAdjustButton.Create(self, FCT, FLT);
  FButList.Add(But);
  But.Height := Round(self.Height / FButList.Count);
  But.Align := alTop;
  But.OnChange := ParamChange;
  But.OnApprove := OnApprove;
  But.OnCenterPress := CenterPress;
  Result := But;
end;

procedure TAdjustPanel.CenterPress(id: string);
begin
  if Assigned(OnCenterPress) then
    OnCenterPress(id);
end;

constructor TAdjustPanel.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);
begin
  inherited Create(nil);
  FButList := TList.Create;
  self.Parent := Panel;
  self.Align := alClient;
  self.Color := Panel.Color;
  self.BevelOuter := bvNone;
  self.BorderStyle := bsNone;
  self.Ctl3D := false;
  self.DoubleBuffered := true;
  FCT := CT;
  FLT := LT;
end;

destructor TAdjustPanel.Destroy;
var
  i: Integer;
begin
  for i := 0 to FButList.Count - 1 do
    TAdjustButton(FButList.Items[i]).Free;
  FButList.Free;
  inherited;
end;

function TAdjustPanel.GetButById(id: string): TAdjustButton;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to FButList.Count - 1 do
    if TAdjustButton(FButList.Items[i]).id = id then
      Result := TAdjustButton(FButList.Items[i]);
end;

function TAdjustPanel.GetButCount: Integer;
begin
  Result := FButList.Count;
end;

procedure TAdjustPanel.ParamChange(Sender: Tobject);
begin
  if Assigned(OnParamChange) then
    OnParamChange(Sender);
end;

procedure TAdjustPanel.Resize;
var
  i, H: Integer;

begin
  inherited;
  H := Round(self.Height / FButList.Count);
  // H:=AdjParamHeight;
  for i := 0 to FButList.Count - 1 do
    TAdjustButton(FButList.Items[i]).Height := H;
end;

procedure TAdjustPanel.SetButtonEnable(Enabled: Boolean);
var
  i: integer;
begin
  FChangesEnabled:= Enabled;
  for i := 0 to FButList.Count - 1 do
    TAdjustButton(FButList.Items[i]).ChangesEnabled:= Enabled;
end;

procedure TAdjustPanel.Update;
var
  i: Integer;
begin
  for i := 0 to FButList.Count - 1 do
    TAdjustButton(FButList.Items[i]).Update;
end;

{ TAdjustFrame }

function TAdjustFrame.AddPanel: TAdjustPanel;
var
  Pan: TAdjustPanel;
begin
  Pan := TAdjustPanel.Create(self, FCT, FLT);
  Pan.OnParamChange := OnParamChange;
  Pan.OnApprove := OnApprove;
  Pan.OnCenterPress := CenterPress;
  Pan.ChangesEnabled:= true;
  FPanList.Add(Pan);
  FPanelSwitchButton.Max := FPanelSwitchButton.Max + 1;
  Result := Pan;
end;

procedure TAdjustFrame.CenterPress(id: string);
begin
  if Assigned(OnCenterPress) then
    OnCenterPress(id);
end;

constructor TAdjustFrame.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);
begin
  inherited Create(nil);
  onPanelChange := nil;
  FPanList := TList.Create;
  self.Parent := Panel;
  self.Align := alClient;
  self.Color := Panel.Color;
  self.Ctl3D := false;
  self.BorderStyle := bsSingle;
  self.BevelWidth := 1;
  // Self.BevelOuter:=bvNone;
  self.DoubleBuffered := true;
  FCT := CT;
  FLT := LT;

  FPanelSwitchButton := TAdjustButton.Create(self, FCT, FLT);
  FPanelSwitchButton.Align := alTop;
  FPanelSwitchButton.Height := PanButHeight;
  FPanelSwitchButton.Min := 1;
  FPanelSwitchButton.Value := 1;
  FPanelSwitchButton.Max := 0;
  FPanelSwitchButton.AdjustValueType := vtInt;
//   FPanelSwitchButton.Kind:=akTwoStr;
  FPanelSwitchButton.Kind := akOneStr;
  FPanelSwitchButton.CaptionFontSize := 38;

  FPanelSwitchButton.Caption := LT['������'];
  FPanelSwitchButton.Units := '';
  FPanelSwitchButton.OnChange := OnPanelSwitch;
  FPanelSwitchButton.Circled := true;

  FBottomPanel := TPanel.Create(self);
  FBottomPanel.Parent := self;
  FBottomPanel.ParentBackground := false;
  FBottomPanel.Color := self.Color;
  FBottomPanel.Align := alClient;
  FBottomPanel.Visible := true;
end;

destructor TAdjustFrame.Destroy;
var
  i: Integer;
begin
  for i := 0 to FPanList.Count - 1 do
    TAdjustPanel(FPanList.Items[i]).Free;
  FPanList.Free;
  inherited;
end;

function TAdjustFrame.GetAdjPanelByIdx(id: string): TAdjustPanel;
var
  i: Integer;
begin
  for i := 0 to FPanList.Count - 1 do
  begin
    if TAdjustPanel(FPanList.Items[i]).id = id then
      Result := TAdjustPanel(FPanList.Items[i]);
  end;
end;

function TAdjustFrame.GetConstValue(id: string): Variant;
var
  i: Integer;
  But: TAdjustButton;
begin
  for i := 0 to FPanList.Count - 1 do
  begin
    But := TAdjustPanel(FPanList.Items[i]).Button[id];
    if Assigned(But) then
      Result := But.ConstValue;
  end;
end;

function TAdjustFrame.GetCurPanelNum: integer;
begin
  result:= FPanelSwitchButton.Value;
end;

function TAdjustFrame.GetValue(id, ParamId: string): Variant;
var
  i: Integer;
  But: TAdjustButton;
begin
  for i := 0 to FPanList.Count - 1 do
  begin
    But := TAdjustPanel(FPanList.Items[i]).Button[id];
    if Assigned(But) then
    begin
      if (ParamId = '') or (ParamId = 'Value') then
        Result := But.Value;
      if (ParamId = 'Max') then
        Result := But.Max;
      if (ParamId = 'Min') then
        Result := But.Min;
      if (ParamId = 'Delta') then
        Result := But.Delta;
      if (ParamId = 'Circled') then
        Result := But.Circled;
      if (ParamId = 'ConstValue') then
        Result := But.ConstValue;
      if (ParamId = 'Caption') then
        Result := But.Caption;
    end;
  end;
end;

procedure TAdjustFrame.OnPanelSwitch(Sender: Tobject);
var
  Num: Integer;
begin
  if Num <> FPanelSwitchButton.Value then
  begin
    Num := FPanelSwitchButton.Value;
    SetCurPanel(Num);
  end;
  { for i:=0 to FPanList.Count-1 do
    if StrToInt(TAdjustPanel(FPanList.Items[i]).ID) = Num then
    TAdjustPanel(FPanList.Items[i]).BringToFront; }
end;

procedure TAdjustFrame.OnParamChange(Sender: Tobject);
var
  But: TAdjustButton;
begin
  But := TAdjustButton(Sender);
  if Assigned(ParamChangeEvent) then
    ParamChangeEvent(But.id, But.Value);
end;

procedure TAdjustFrame.SetButtonEnable(Enabled: Boolean);
var
  i: integer;
begin
  FChangesEnabled:= Enabled;
  for i := 0 to FPanList.Count - 1 do
    TAdjustPanel(FPanList.Items[i]).ChangesEnabled:= Enabled;
end;

procedure TAdjustFrame.SetConstValue(id: string; Value: Variant);
var
  i: Integer;
  But: TAdjustButton;
begin
  for i := 0 to FPanList.Count - 1 do
  begin
    But := TAdjustPanel(FPanList.Items[i]).Button[id];
    if Assigned(But) then
      But.ConstValue := Value;
  end;
end;

procedure TAdjustFrame.SetCurPanel(Num: Integer);
var
  i: Integer;
begin
  for i := 0 to FPanList.Count - 1 do
    if StrToInt(TAdjustPanel(FPanList.Items[i]).id) = Num then
      TAdjustPanel(FPanList.Items[i]).BringToFront;
  if Assigned(onPanelChange) then
    onPanelChange(Num);
end;

procedure TAdjustFrame.SetValue(id, ParamId: string; Value: Variant);
var
  i: Integer;
  But: TAdjustButton;
begin
  for i := 0 to FPanList.Count - 1 do
  begin
    But := TAdjustPanel(FPanList.Items[i]).Button[id];
    if Assigned(But) then
    begin
      if (ParamId = '') or (ParamId = 'Value') then
      begin
        if (Value >= But.Min) and (Value <= But.Max) then
          But.Value := Value;
      end;
      if (ParamId = 'Max') then
        But.Max := Value;
      if (ParamId = 'Min') then
        But.Min := Value;
      if (ParamId = 'Delta') then
        But.Delta := Value;
      if (ParamId = 'Circled') then
        But.Circled := Value;
      if (ParamId = 'ConstValue') then
        But.ConstValue := Value;
      if (ParamId = 'Caption') then
        But.Caption := Value;
      if (ParamId = 'Units') then
        But.Units := Value;
      { if (Value>=But.Min) and (Value<=But.Max) then
        But.Value:=Value; }
    end;
  end;
end;

procedure TAdjustFrame.Update;
var
  i: Integer;
begin
  for i := 0 to FPanList.Count - 1 do
    TAdjustPanel(FPanList.Items[i]).Update;
end;

{ TMeasurePanel }

constructor TMeasureFrame.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);
begin
  inherited Create(nil);
  FMode := bmSearch;
  MMScaleType := stH;
  self.Parent := Panel;
  self.Align := alClient;
  // Self.Color:=Panel.Color;
  self.Color := CT.Color['MeasurePan_BackGr'];
  self.Font.Color := CT.Color['MeasurePan_Text'];
  self.BevelOuter := bvNone;
  self.Ctl3D := false;
  self.BorderStyle := bsNone;

  { Self.BorderStyle:=bsSingle;
    Self.BorderWidth:=1; }
  self.DoubleBuffered := true;

  FCT := CT;
  FLT := LT;

  FH := TPanel.Create(self);
  FH.Parent := self;
  FH.ParentBackground := false;
  FH.Color := self.Color;
  FH.Font.Color := self.Font.Color;
  FH.Align := alLeft;
  FH.BevelOuter := bvNone;
  FH.Ctl3D := false;
  FH.BorderStyle := bsSingle;
  FH.BorderWidth := 1;
  FH.DoubleBuffered := true;
  FH.Font.Name := DefaultFontName;
  FH.Caption := 'H = ';

  FRight := TPanel.Create(self);
  FRight.Parent := self;
  FRight.ParentBackground := false;
  FRight.Align := alClient;
  FRight.BevelOuter := bvNone;
  FRight.Ctl3D := false;
  FRight.BorderStyle := bsNone;

  // FRight.BorderStyle:=bsSingle;
  // FRight.BorderWidth:=1;
  FRight.DoubleBuffered := true;

  FBot := TPanel.Create(FRight);
  FBot.Parent := FRight;
  FBot.ParentBackground := false;
  FBot.Align := alBottom;
  FBot.BevelOuter := bvNone;
  FBot.Ctl3D := false;
  FBot.BorderStyle := bsNone;
  // FBot.BorderStyle:=bsSingle;
  // FBot.BorderWidth:=1;
  FBot.DoubleBuffered := true;

  FL := TPanel.Create(FRight);
  FL.Parent := FRight;
  FL.ParentBackground := false;
  FL.Color := self.Color;
  FL.Font.Color := self.Font.Color;
  FL.Align := alLeft;
  FL.Font.Name := DefaultFontName;
  FL.Caption := 'L = ';
  FL.BevelOuter := bvNone;
  FL.Ctl3D := false;
  FL.BorderStyle := bsSingle;
  FL.BorderWidth := 1;
  FL.DoubleBuffered := true;

  FN := TPanel.Create(FRight);
  FN.Parent := FRight;
  FN.ParentBackground := false;
  FN.Color := self.Color;
  FN.Font.Color := self.Font.Color;
  FN.Align := alClient;
  FN.Font.Name := DefaultFontName;
  FN.Caption := 'N = ';
  FN.BevelOuter := bvNone;
  FN.Ctl3D := false;
  FN.BorderStyle := bsSingle;
  FN.BorderWidth := 1;
  FN.DoubleBuffered := true;

  FKd := TPanel.Create(FBot);
  FKd.Parent := FBot;
  FKd.ParentBackground := false;
  FKd.Color := self.Color;
  FKd.Font.Color := self.Font.Color;
  FKd.Align := alRight;
  FKd.Font.Name := DefaultFontName;
  FKd.Caption := '�� =';
  FKd.BevelOuter := bvNone;
  FKd.Ctl3D := false;
  FKd.BorderStyle := bsSingle;
  FKd.BorderWidth := 1;
  FKd.DoubleBuffered := true;

  FKan := TPanel.Create(FBot);
  FKan.Parent := FBot;
  FKan.ParentBackground := false;
  FKan.Color := self.Color;
  FKan.Font.Color := self.Font.Color;
  FKan.Align := alClient;
  FKan.Font.Name := DefaultFontName;
  FKan.Caption := '';//'               ';
  FKan.BevelOuter := bvNone;
  FKan.Ctl3D := false;
  FKan.BorderStyle := bsSingle;
  FKan.BorderWidth := 1;
  FKan.DoubleBuffered := true;
end;

destructor TMeasureFrame.Destroy;
begin

  inherited;
end;

procedure TMeasureFrame.Refresh;
var
  dir, Head, Zn, Rail, Zone, ch, M, ASDstr: string;
  N, Alfa: Integer;
  MPKind: Boolean;
begin
  if Assigned(AScanRef) then
    if Assigned(AScanRef.Core) then
      with AScanRef.Core do
      begin
        if WaitingForBUM then
          Exit;
        M := '';
        Alfa := AScanRef.Kanal.Takt.Alfa;
        if (AScanRef.Kanal.Method = '���') or (AScanRef.Kanal.Method = 'ECHO')
          then
          M := 'ECHO'
        else if (AScanRef.Kanal.Method = '���') or (AScanRef.Kanal.Method = 'MSM') then
          M := 'MSM'
        else if (AScanRef.Kanal.Method = '����') or (AScanRef.Kanal.Method = 'MIRR') then
          M := 'MIRR'
        else
          M := 'ECHO';

        (*<Rud23>*)
        if MMScaleType = stH then
          // FH.Caption:=Format('H = %d',[MeasureResult.H])
          FH.Caption := Format('H = %s', [TUnitsConverter.ConvertMMToStr
              (MeasureResult.H)])
        else if (Alfa <> 0) and (FMode in [bmNastr,bmNastrHand]) then
          FH.Caption := Format('R = %s', [TUnitsConverter.ConvertMMToStr
              (MeasureResult.R)])
        else
          FH.Caption := Format('H = %s', [TUnitsConverter.ConvertMMToStr
              (MeasureResult.H)]);
        (*</Rud23>*)

        // FH.Caption:=Format('R = %d',[MeasureResult.R]);
        //

        // FL.Caption:=Format('L = %d',[MeasureResult.L]);
        FL.Caption := Format('L = %s', [TUnitsConverter.ConvertMMToStr
            (MeasureResult.L)]);
        if MeasureResult.N < 0 then
        begin
          N := 0;
          Zn := '<';
        end
        else if MeasureResult.N > 18 then
        begin
          N := 18;
          Zn := '>';
        end
        else
        begin
          N := MeasureResult.N;
          Zn := '=';
        end;
        FN.Caption := Format('N %s %d', [Zn, N]);
        if AttVis then
         if Config.Language = 'Spanish' then
          FKd.Caption := Format
            ('%s= %d', [FLT.Caption['����.'], {MeasureResult.Att} AScanRef.Kanal.ATT])
         else
          FKd.Caption := Format
          (* <Rud1> *)
            ('%s = %d', [FLT.Caption['����.'], {MeasureResult.Att} AScanRef.Kanal.ATT])
          (* </Rud1> *)
        else
          FKd.Caption := Format('%s %s %d', [FLT.Caption['��'], Zn,
            MeasureResult.Kd]);
        if Assigned(AScanRef.Kanal) then
        begin
          if not(FMode in [bmHand, bmNastrHand]) then
          begin
            if AScanRef.Kanal.Nit = 0 then
              Rail := FLT.Caption['�.']
            else
              Rail := FLT.Caption['�.'];
          end
          else
            Rail := FLT.Caption['������'];

          case AScanRef.Kanal.WaveDirection of
{$IFDEF EGO_USW}
            dForward:
              dir := FLT.Caption['WPA'];
            dBack:
              dir := FLT.Caption['WPB'];
{$ELSE}
            dForward:
              dir := FLT.Caption['�.'];
            dBack:
              dir := FLT.Caption['�.'];
{$ENDIF}
          else
            dir := '';
          end;
{$IFNDEF TwoThreadsDevice}
          Rail := '';
{$ENDIF}
          if not(FMode in [bmHand, bmNastrHand]) then
          begin
            Zone := '';
            if (AScanRef.Kanal.Num = 6) or (AScanRef.Kanal.Num = 7) then
              Zone := FLT.Caption['(�����)'];
            if (AScanRef.Kanal.Num = 8) or (AScanRef.Kanal.Num = 9) then
              Zone := FLT.Caption['(�������)'];
          end;
          MPKind := false;
{$IFDEF Avikon16_IPV}
          MPKind := true;
{$ENDIF}
{$IFDEF Avikon14_2Block}
          MPKind := true;
{$ENDIF}
//
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
          MPKind := true;
{$IFEND}
//
{$IFDEF USK004R}
          MPKind := true;
{$ENDIF}
          if MPKind then  //���������� ������ 58 ����.
          begin
            ch := IntToStr(AScanRef.Kanal.Num);
            if not(FMode in [bmHand, bmNastrHand]) then
            begin
              Head := '';
              ch := IntToStr(AScanRef.Kanal.Num);
{$IFDEF Avikon16_IPV}
              if AScanRef.Kanal.Takt.Edge = waWorking then
                Head := FLT.Caption['���. ��.']
              else if AScanRef.Kanal.Takt.Edge = waNotWorking then
                Head := FLT.Caption['�����. ��.']
              else
                Head := '';

              if Config.SoundedScheme = Wheels then
              begin
                if ((AScanRef.Kanal.Num = 10) or (AScanRef.Kanal.Num = 11)) then
                  Head := FLT.Caption['�� 1'];
                if ((AScanRef.Kanal.Num = 1) or (AScanRef.Kanal.Num = 0)) then
                  Head := FLT.Caption['�� 2'];

              end;
{$ENDIF}
{$IFDEF EGO_USW}
              if AScanRef.Kanal.Takt.Edge = waWorking then
                Head := FLT.Caption['���. ��.']
              else if AScanRef.Kanal.Takt.Edge = waNotWorking then
                Head := FLT.Caption['�����. ��.']
              else
                Head := '';

              if ((AScanRef.Kanal.Num = 10) or (AScanRef.Kanal.Num = 11)) then
                Head := FLT.Caption['WPB'];//['�� 2'];
              if ((AScanRef.Kanal.Num = 1) or (AScanRef.Kanal.Num = 0)) then
                Head := FLT.Caption['WPA'];//['�� 1'];
{$ENDIF}
//
{$IFDEF VMT_US}
              if AScanRef.Kanal.Takt.Edge = waWorking then
                Head := FLT.Caption['���. ��.']
              else if AScanRef.Kanal.Takt.Edge = waNotWorking then
                Head := FLT.Caption['�����. ��.']
              else
                Head := '';
{$ENDIF}
//
{$IFDEF Avikon14_2Block}
              ch := IntToStr(AScanRef.Kanal.Num);

              if ((AScanRef.Kanal.Num = 2) and (AScanRef.Kanal.Nit = 1)) or
                ((AScanRef.Kanal.Num = 3) and (AScanRef.Kanal.Nit = 1)) or
                ((AScanRef.Kanal.Num = 12) and (AScanRef.Kanal.Nit = 0)) or
                ((AScanRef.Kanal.Num = 13) and (AScanRef.Kanal.Nit = 0)) then
                Head := FLT.Caption['���. ��.'];
              if ((AScanRef.Kanal.Num = 12) and (AScanRef.Kanal.Nit = 1)) or
                ((AScanRef.Kanal.Num = 13) and (AScanRef.Kanal.Nit = 1)) or
                ((AScanRef.Kanal.Num = 2) and (AScanRef.Kanal.Nit = 0)) or
                ((AScanRef.Kanal.Num = 3) and (AScanRef.Kanal.Nit = 0)) then
                Head := FLT.Caption['�����. ��.'];
              if ((AScanRef.Kanal.Num = 10) or (AScanRef.Kanal.Num = 11)) then
                Head := FLT.Caption['�� 1'];
              if ((AScanRef.Kanal.Num = 1) or (AScanRef.Kanal.Num = 0)) then
                Head := FLT.Caption['�� 2'];
{$ENDIF}
{$IFDEF USK004R}
            if Config.WorkSide then begin
              if AScanRef.Kanal.Takt.Edge = waWorking then
                Head := FLT.Caption['�����. ��.']
              else if AScanRef.Kanal.Takt.Edge = waNotWorking then
                Head := FLT.Caption['���. ��.']
              else
                Head := '';
            end
            else
              if AScanRef.Kanal.Takt.Edge = waWorking then
                Head := FLT.Caption['���. ��.']
              else if AScanRef.Kanal.Takt.Edge = waNotWorking then
                Head := FLT.Caption['�����. ��.']
              else
                Head := '';
{              if AScanRef.Kanal.Takt.Edge = waWorking then
                Head := FLT.Caption['���. ��.']
              else if AScanRef.Kanal.Takt.Edge = waNotWorking then
                Head := FLT.Caption['�����. ��.']
              else
                Head := '';
}
{$ENDIF}

            end;
            {$IFDEF SHOWASD}
            if AScanRef.Kanal.ASD then
              ASDstr:= ' On'
            else
              ASDstr:= ' Off';
            FKan.Caption := Format
              ('%s %d� %s %s (%s) %s', [Rail, AScanRef.Kanal.Takt.Alfa, dir,
              Zone, ch, ASDstr]);
            {$ELSE}
            FKan.Caption := Format
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
              ('%s %d� %s %s %s %s (%s)', [Rail, AScanRef.Kanal.Takt.Alfa,
              FLT[M], dir, Head, Zone, ch]);
{$ELSE}
              ('%s %d� %s %s %s %s (%s)', [Rail, AScanRef.Kanal.Takt.Alfa, dir,
              FLT[M], { FLT.Caption['���.'], } Head, Zone, ch]);
{$IFEND}
           {$ENDIF}
          end
          else
          begin
            FKan.Caption := Format
              ('%s %d� %s %s %s', [Rail, AScanRef.Kanal.Takt.Alfa, dir,
              FLT[AScanRef.Kanal.Method], Zone]);
          end;
        end;
      end;
end;

procedure TMeasureFrame.Resize;
begin
  inherited;
  FH.Width := self.Height * 2;
  FBot.Height := Round(self.Height / 2);
  FL.Width := FRight.Width div 2;
  FKd.Width := 4 * FBot.Height;
  FH.Font.Height := Round(FH.Height * 0.7);
  FL.Font.Height := Round(FRight.Height * 0.6);
  FN.Font.Height := Round(FRight.Height * 0.6);
  FKan.Font.Height := Round(FKan.Height * 0.8);
  FKd.Font.Height := Round(FKd.Height * 0.9);
end;

end.
