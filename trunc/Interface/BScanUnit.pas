unit BScanUnit;

{$I Directives.inc}

interface

uses ExtCtrls, Windows, Classes, Graphics, Controls, DefCoreUnit, SysUtils, UtilsUnit,
     CustomInterfaceUnit, MCAN, Types, PublicData, AviconDataContainer, AviconTypes,
     Math, Forms, Dialogs, ConfigObj, CfgTablesUnit_2010, PublicFunc, ProgramProfileConfig;

type
   TRGB = record
      R: Byte;
      G: Byte;
      B: Byte;
    end;

const
    MinAmp = 3;
    MaxAmp = 15;
    AKKWidthPerCent = 10;
    HelpWidthPerCent = 50;

    ThinButPerCent = 0.8; //USK004R

    StrobAreaWidthPercent = 0.03;
    DefaultFontName = 'Impact';
    ThresholdMax = 6;
    ThresholdMin = -6;
    //                                          -12-11-10-9-8-7-6-5-4-3-2-1 0 1 2 3 4 5 6 7 8   9 10 11 12 13 14 15 16 17 18 ��
    ThresholdTable :array[-12..18] of Integer = (0, 0, 1, 1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,11,11,12,12,13,13,14,14,15);

    PlotCount = 2;
    PlotColors :array[0..PlotCount-1] of TColor = (clGreen, clBlue);

    ADSTimeBeforeOff = 1000; // ����� ������� ��� �� ������, ����� ��������� ����������� ������������

    MaxBaseAmpl = 250;

    PlotColor: TRGB = (R: 0; G: 150; B: 0);

type
    TCoordEventList = class;
    TIconID = ( iKM, iPK, iPainter, iDefect, iJointSt, iJointEd, iSwitch, iTxt );

    TBScanDirection = (bdUp, bdDown); // ����������� �-���������.
    // bdUp - ��� ������ �������� ��� ���� ��������� �����
    // bdDown - �������� ��� ������� ������ ��� ���� �����

    PPixelFormat = ^TPixelFormat;

    TKanColors = array[0..3, 0..15] of TRGB;
    TKanSizeByAmp = array[0..15] of Integer;

    TBorders = set of (brLeft, brRight, brTop, brBottom);

    TScanLines = array of Pointer;
    TDelays = array[0..255] of Integer; // ������� ����������� �������� � ��������
    PDelays = ^TDelays;

    TPutPixelProc = procedure(X, Y: Integer; Color: TRGB) of object;

    TPlotPoint = record
      Coord: Integer; // ���������� ����� � �������� ���������� ����������
      Y: Integer; // ���������� �� ��������� � ��������
      X: Integer; // ���������� �� ����������� � ��������
    end;
    PPlotPoint = ^TPlotPoint;

    TPlot = class(TList)
    private
      procedure RemovePoint(Index: Integer); overload;
      procedure RemovePoint(Index: Pointer); overload;
      function GetPoint(Index: Integer): TPlotPoint;
    protected
    public
      constructor Create;
      destructor Destroy; override;
      procedure AddPoint(_Coord, _Y: Integer);
      procedure Clear; override;
      procedure Filter(MaxX: Integer);
      Procedure Refresh(Scale, CurCoord: Integer);
      property Points[Index: Integer]: TPlotPoint read GetPoint; default;
    end;

    PRect = ^TRect; // ��������� �� �������������. ���� ������������ ��� ������ �� ������������� �������
    TScanLineMas = array of Pointer;

    TCoordStyle = (csAbsolute, csRelative);

    TPainter = class
    private
      FCanvas: TCanvas; // �����������, �� ������� ����� �������� (������������� ����� ��������, �� ���������)
      FWorkRect: PRect; // ������� �������� Canvas'�, ������ ������� ����� ������ ���������
      FDelayMin: Integer; // ����������� �������� - ��������, ��������������� ������ ������� FWorkRect
      FDelayMax: Integer; // ������������� �������� - ��������, ��������������� ������� ������� FWorkRect
      FDelays: array[0..255] of Integer; // ������, ����������� �������� � ���������
      FPadding: Integer; // ���������� ����. �.�. �������� �� ������� ���������� ��������� �� ������� FWorkRect
      FPaddingRect: TRect; // ���������� ������������� � ������� ���������� ���������, ������ ��� FWorkRect �� FPadding �� ���� ��������
      procedure RecountDelays; // ��������� ����������� ������������� �������� �� ��������
    protected
      procedure SetMinDelay(Value: Integer);
      procedure SetMaxDelay(Value: Integer);
      procedure SetWorkRect(Value: PRect);
      Procedure SetPadding(Value: Integer);
    public
      ScanLineMas: TScanLineMas; // ��������� �� ������ ����������, ������������ ��� �������� ��������� ������ �� �������� ������� ������������� �������
      CoordStyle: TCoordStyle; // ��� ���������, ���������� ��� �������������, ��� ��������� ����� �����������
      constructor Create;
      destructor Destroy; override;
      procedure Resize;
      procedure PutPixel(X, Y: Integer; Color: TRGB);
      procedure PutSignal(X: Integer; D: Byte; Color: TRGB);
      procedure Line(X1, Y1, X2, Y2: Integer; Color: TRGB);
      property DelayMin: Integer read FDelayMin write SetMinDelay;
      property DelayMax: Integer read FDelayMax write SetMaxDelay;
      property WorkRect: PRect read FWorkRect write SetWorkRect;
    end;

    TCustomBScanElement = class
    private
      FBmp: TBitMap;        // ��������� �� "�������" BMP
      FBackBmp: TBitMap;    // ��������� �� �������� BMP
      FWorkRect: TRect;     // ������� �������� BMP, ������ ������� ����� ������ ���������
      FColorTable: TColorTable; // ������� ������� ���������� ���������
      FLanguageTable: TLanguageTable; // �������� �������
      FBackGrColor: TColor;
      FBorderColor: TColor;
    protected
      procedure SetTop(Value: Integer);
      procedure SetLeft(Value: Integer);
      procedure SetHeight(Value: Integer);
      procedure SetWidth(Value: Integer);
      function GetTop: Integer;
      function GetLeft: Integer;
      function GetHeight: Integer;
      function GetWidth: Integer;
      procedure SetColorTable(Value: TColorTable); virtual;
      procedure SetLanguageTable(Value: TLanguageTable); virtual;
    public
      Delays: PDelays;
      ScanLines: TScanLines;
      BScanMin: Integer;
      BScanMax: Integer;
      constructor Create;
      destructor Destroy; override;
      procedure DrawForeGroundContent; virtual;
      procedure DrawBackGroundContent; virtual;
      property ForeGrPic: TBitMap read FBmp write FBmp;
      property BackGrPic: TBitMap read FBackBmp write FBackBmp;
      property Left: Integer read GetLeft write SetLeft;
      property Top: Integer read GetTop write SetTop;
      property Width: Integer read GetWidth write SetWidth;
      property Height: Integer read GetHeight write SetHeight;
      property ColorTable: TColorTable read FColorTable write SetColorTable;
      property LangugeTable: TLanguageTable read FLanguageTable write SetLanguageTable;
    end;

    TStrobArea = class(TCustomBScanElement)
    private
      FKanal: TKanal;
      FForwardColor: TColor;
      FBackwardColor: TColor;
      FCurKanalColor: TColor;
    protected
      procedure SetKanal(Value: TKanal);
      procedure SetColorTable(Value: TColorTable); override;
      Procedure AssignProperKanalColor;
    public
      constructor Create;
      destructor Destroy; override;
      procedure DrawForeGroundContent; override;
      procedure DrawBackGroundContent; override;
      property Kanal: TKanal read FKanal write SetKanal;
    published

    end;


    TKanalList = class( TList )
    protected
      function Get(Index: Integer): TKanal;
      procedure Put(Index: Integer; Item: TKanal);
    public
      property Items[Index: Integer]: TKanal read Get write Put; default;
    end;

    TStrobKind = (skCenter, skInner, skOuter, skTwoEcho);

    TStrobPainter = class
      private
        FBmp: TBitMap;
        FBounds: TRect;
        FY1: integer;
        FY2: integer;
        FWidth: integer;
        FHeight: integer;
        FMiddle: integer;
        FThird: integer;
        (*<Rud34>*)
        hasBegin, hasEnd: boolean;
        (*</Rud34>*)
      public
        Kind: TStrobKind;
        Color: TColor;
        constructor Create(Bmp: TBitMap; Left, Right, Top, Bottom: Integer);
        procedure SetStrobValues(Y1, Y2: Integer);
        procedure DrawStrob;
    end;

    TBScanLine = class(TCustomInterfaceObject)
    private
      FScanLines: TScanLines;
      FKanalList: TKanalList;
      FBScanMin: Integer; // ����������� �������� � ���
      FBScanMax: Integer; // ������������ �������� � ���
      FDelays: TDelays;
      FK: Single; // ����������� ����� Min � Max � ��� � ������� ������� � ��������
      FScanDir: TBScanDirection;
      FPointSize: Integer;   // ������ �����
      FAmplByColor: Boolean; // ���������� ��������� ����� ����
      FKanColors: TKanColors; // ������� ����� ������������ ��������
      FAmplBySize: Boolean; // ���������� ��������� ������ �����
      FKanSizeByAmp: TKanSizeByAmp; // ������� �������� ������ ������������ ��������
      FBorders: TBorders;
      FBorderWidth: Integer;
      FBorderColor: TColor;
      PutPixelProc: TPutPixelProc;
      FStrobAreaWidth: Integer;
      FEventList: TCoordEventList;
      FPainterCoord: Integer;
      FPlotMas: array[0..PlotCount-1] of TPlot;
      FDrawSide: Boolean;
      procedure ClearMas;
      procedure CountColors;
      procedure PutPixel32bit(X, Y: Integer; Color: TRGB);
      procedure PutPixel16bit(X, Y: Integer; Color: TRGB);
      procedure CountDelay;
    protected
      procedure DrawBackGround; override;
      procedure DrawForeGround; override;
      procedure Resize; override;
      procedure Paint; override;
      procedure SetKanal(Index: integer; Value: TKanal);
      procedure SetScanDir(Value: TZondPosition);
      procedure SetPointSize(Value: TBViewPointSize);
      function GetKanal(Index: Integer): TKanal;
      function GetKanalsCount: Integer;
      (*<Rud38>*)
      function DelayToLineCoords(Delay :byte; DelayShift : Integer) : integer;//byte;AK
      (*</Rud38>*)
      procedure DrawStrobZone(FColor,BColor : TColor);
      procedure SetStrobKind(var StrobPainter : TStrobPainter; KanalAlfa, KanalNum : Integer; KanalNit : Byte);
    public
      // ����� � ����� ������ ����� ����� ���������� ����� ����� ����� ������� ����� ������ ��������
      Rail: Integer;
      procedure Clear;
      procedure MoveImage(Delta: Integer; BackMotion: Boolean);
      procedure AddBackZone(St, Ed: Integer);
      procedure AddSignal(Pos: Integer; Kan: Integer; Delay,  Ampl: Byte; DelayShift : Integer);
      procedure AddPlotPointAsSignal(Pos: Integer; Kan: Integer; Level: Byte);
      procedure AddPlotLine(Coord1, Level1, Coord2, Level2: Integer);
      procedure AddPlotPoint(PlotNum, _Coord, _Y: Integer);
      constructor Create(AOwner: TComponent; BPP: TPixelFormat);
      destructor Destroy; override;
      procedure RefreshPlot(Scale, CurCoord: Integer);
      procedure AddKanal(Kanal: TKanal);
      procedure ClearKanals;
      property KanalsCount: Integer read GetKanalsCount;
      property Kanals[Index: Integer]: TKanal read GetKanal;
    published
      property AmpByColor: Boolean read FAmplByColor write FAmplByColor;
      property AmplBySize: Boolean read FAmplBySize write FAmplBySize;
      property Borders: TBorders read FBorders write FBorders;
      property BorderWidth: Integer read FBorderWidth write FBorderWidth;
      property Align;
      property EventList: TCoordEventList read FEventList write FEventList;
      property PainterCoord: Integer read FPainterCoord write FPainterCoord;
      property ZondDirection: TZondPosition write SetScanDir;
      property PointSize: TBViewPointSize write SetPointSize;
      property FlDrawSide: Boolean write FDrawSide;
    end;

    TCoordIcon =
    record
      Icon: TImage;
      Y: Integer;
    end;
    PCoordIcon = ^TCoordIcon;

    TCoordLineIcons = class( TList )
    private
    public
      constructor Create(AOwner: TComponent);
      procedure Draw( Canvas: TCanvas; X: Integer; IconID: TIconID );
    end;

    TCoordEvent = record
      Coord: Integer;
      EventIcon: TIconID;
      Rail: Byte;
    end;

    PCoordEvent = ^TCoordEvent;

    TCoordEventList = class
      private
        FEventList: TList;
      protected
        function GetEventsCount: Integer;
        function GetEvent(Index: Integer): TCoordEvent;
      public
        constructor Create;
        destructor Destroy;override;
        procedure Clear;
        procedure AddEvent(_Coord: Integer; rail: byte; _IconID: TIconID);
        property Count: Integer read GetEventsCount;
        property Events[Index: Integer]: TCoordEvent read GetEvent; default;
    end;

    TCoordUnits = (cuWay, cuSec); // cuWay - ��������� ���������� (��, ��, �). cuSec - ������� (��� �������)

    TCoordChangeByMouseEvent = procedure(CurCoord: Integer) of object;

    TCoordLine = class(TCustomInterfaceObject)
    private
      FScale: Single; // ���-�� ������
      FCurCoord: Integer;
      FBorders: TBorders;
      FBorderWidth: Integer;
      FBorderColor: TColor;
      FFontHeight: Integer;
      FFontColor: TColor;
      FCenaDeleniya: Single; // ���� ������� � ������
      FKmSt: string;
      FPkSt: string;
      FMSt: string;
      FSecSt: string;
      FWidthDisCoord: Integer; // ����� ������������ ����� � �������� ���������� ����������
      FAxisSmallLineW: Integer;
      FAxisSmallLineWDis: Integer;
      FWidthDis: Integer;
      FScanStep: Integer;
      FM2: Integer;

      FIcons: TCoordLineIcons;

      FEventList: TCoordEventList;

      FMX, FMY, FDX: Integer;

      (*<Rud49>*)
      fullBoltJoin : boolean;
      (*</Rud49>*)
    protected
      function DisToPix(DisCoord: Integer): Integer;
      procedure DrawBackGround; override;
      procedure DrawForeGround; override;
      procedure Resize; override;
      function GetStartCoord: Integer;
      procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    public
      ShowIcons: Boolean;
      CoordUnits: TCoordUnits;
      OnCoordChangeByMouse: TCoordChangeByMouseEvent;
      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;
    published
      property Scale: Single read FScale write FScale;
      property CurCoord: Integer read FCurCoord write FCurCoord;
      property StartCoord: Integer read GetStartCoord;
      property Borders: TBorders read FBorders write FBorders;
      property BorderWidth: Integer read FBorderWidth write FBorderWidth;
      property EventList: TCoordEventList read FEventList write FEventList;
    end;

    TLines = array of TBScanLine;

    TBScanWinMode = (bmOff, bmSearch, bmEstimate, bmNastr, bmHand, bmNastrHand, bmMnemo);

    TSignInBaseProbe = array[0..1, 0..7] of Byte;

    TBScan = class(TPanel)
    private
      FCore: TDefCore; // ����
      FLinesCount: Integer; // ���-�� ������� B-���������
      FLineHeight: Integer; // ������ ����� ������� � ��������
      FLines: TLines;
      FCoordLine: TCoordLine; // ������ � �����������
      FCoordLineHeight: Integer;
      FDevLineWidth: Integer; // ������ �������������� ����� � ��������
      FColorTable: TColorTable; // ������� ������� ���������� ���������
      FLanguageTable: TLanguageTable; // �������� �������
      FTempCoord: Integer;
      FScale: Single;
      FM2: Integer; // ���-��
      FCurDisCoord: Integer; // ������� ���������� ����������
      FCurSysCoord: Integer; // ������� ��������� ����������
      FIsCoordLine: Boolean;
      FPosOffset: Integer;
      FDc: Integer;
      FBackZone: Boolean;
      FRefreshModeFull: Boolean;
      FFullRefreshStart: Integer;
      FThreshold: Integer;
      FThresholdAmpl: Integer;
      FEventList: TCoordEventList;
      FPainterCoord: Integer;
      SignInBaseProbeCount: array[0..1] of Byte;
      SignInBaseProbe :TSignInBaseProbe;
      FOldPos: Integer;
      FOldLevel: Integer;
      FIsChZero: array of Boolean;
      procedure Resize;override;
      function ProcessSingnal(StartDisCoord: Integer): Boolean;
      function ProcessSingnalExtern(StartDisCoord: Integer): Boolean;
      function ProcessSingnalDummy(StartDisCoord: Integer; ExternTrue : Boolean): Boolean;
      procedure Click(Sender: TObject);
      procedure EventRoutine;
      procedure SetScanDir(Value: TZondPosition);
      procedure SetPointSize(Value: TBViewPointSize);
      procedure PlotRoutine(Pos: Integer; Line: TBScanLine);
      procedure CoordChangeByMouse(NewCurCoord: Integer);
    protected
      function GetLine(Index: Integer): TBScanLine;
      procedure SetLine(Index: Integer; Value: TBScanLine);
      procedure SetScale(Value: Single);
      function GetFontName: string;
      procedure SetFontName(Value: string);
      function GetLinesCount: Integer;
      procedure SetCoordUnits(Value: TCoordUnits);
      function GetCoordUnits: TCoordUnits;

      function GetAmpByColor: Boolean;
      procedure SetAmpByColor(Value: Boolean);

      function GetAmpBySize: Boolean;
      procedure SetAmpBySize(Value: Boolean);
      function GetShowIcons: Boolean;
      procedure SetShowIcons(Value: Boolean);

    public
      PixelFormat: TPixelFormat;
      HasSecondBottom: Boolean;

      procedure ForceResize;
      procedure IncThreshold;
      procedure DecThreshold;
      procedure UpDateStrob;
      procedure Clear;
      function AddLine({Kanal1, Kanal2: TKanal}): TBScanLine;
      procedure AddCoordLine(CoordLineHeight: integer);
      procedure ConfigureLines(WorkH, ChButtonHeight: Integer);  // ���������� ����� �-���������
      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;
      procedure Refresh(Coord: Integer; Force, Full: Boolean);
      procedure AddPlotPoint(RailNum, KanalNum, PlotNum, Ampl: Integer);

      property Core: TDefCore read FCore write FCore;
      property Colors: TColorTable read FColorTable write FColorTable;
      property Language: TLanguageTable read FLanguageTable write FLanguageTable;
      property LinesCount: Integer read GetLinesCount;
      property Lines[Index: Integer]: TBScanLine read GetLine write SetLine;
      property Scale: Single read FScale write SetScale;
      property FontName: string read GetFontName write SetFontName;
      property Threshold :Integer read FThreshold;
      property BScanDirection: TZondPosition write SetScanDir;
      property PointSize: TBViewPointSize write SetPointSize;
      property CoordUnits: TCoordUnits read GetCoordUnits write SetCoordUnits;
      property AmpByColor: Boolean read GetAmpByColor write SetAmpByColor;
      property AmplBySize: Boolean read GetAmpBySize write SetAmpBySize;
      property ShowIcons: Boolean read GetShowIcons write SetShowIcons;
    published
    end;

    TChButKind = (bkLeft, bkRight);
    TKanType = (rkContinuous, rkHand); // ��� ������. ����� ��������� �������� ��� ������

    TChanelButton = class(TPanel)
    private
      FKanal1: TKanal;
      FKanal2: TKanal;
      FHKanal: TKanal;
      FCurKanal: TKanal;
      FChanHelpPan: TPanel;
      FChanWorkPan: TPanel;
      FChanAKKPan: TPanel;
      FAlfaPan: TPanel;
      FMethodPan: TPanel;
      FDirPanel: TPanel;
      FDirPB: TPaintBox;
      FChDirNumPan: TPanel;
      FChNumPan: TPanel;
      FCT: TColorTable;
      FLT: TLanguageTable;
      FBmp: TBitMap;
      FShowHelp: Boolean;
      FArrowDir: TDirection;
      FKind: TChButKind;
      FSelected: Boolean;
      FASD: Boolean;
      FAdjusted: Boolean;
      FASDLastChangeTime: Int64;
      FASDTimeToWait: Integer;
      FMode: TBScanWinMode;
      FEnabled: Boolean;
      FKanalType: TKanType;
      FLocked: Boolean;
      procedure DrawArrow;
      procedure OnPaint(Sender: TObject);
      procedure Resize; override;
      procedure SetKindness(Value: TChButKind);
      procedure PanClick(Sender: TObject);
      procedure ResizePan(Sender: TObject);
      procedure UpDate;
    protected
       procedure SetShowHelp(Value: Boolean);
       procedure SetKanal(Value: TKanal); // �� ������, � ������������ ��� ��������� ������
       function  GetKanal: TKanal;
       procedure SetASD(Value: Boolean);
       procedure SetAK(Value: Boolean);
       procedure SetSelected(Value: Boolean);
       procedure SetAdjusted(Value: Boolean);
       procedure SetEnabled(Value: Boolean);
       function GetEnable: Boolean;
       procedure SetMode(Value: TBScanWinMode);
       procedure SetKanalType(Value: TKanType);
    public
      OnClick: TNotifyEvent;
      constructor Create(AOwner: TComponent; Kanal1, Kanal2, HKanal: TKanal; CT: TColorTable; LT: TLanguageTable);
      destructor Destroy; override;
      procedure RefreshParams;
      function CountWidth(Height: Integer): Integer;
      procedure Lock;
      procedure UnLock;
      property ShowHelp: Boolean read FShowHelp write SetShowHelp;
      property Kind: TChButKind read FKind write SetKindness;
      property Kanal: TKanal read GetKanal;
      property ASD: Boolean write SetASD;
      property AKK: Boolean write SetAK;
      property Selected: Boolean read FSelected write SetSelected;
      property Adjusted: Boolean read FAdjusted write SetAdjusted;
      property Enabled: Boolean read FEnabled write SetEnabled;
      property Mode: TBScanWinMode read FMode write SetMode;
      property KanalType: TKanType read FKanalType write SetKanalType;
    published
    end;

    TKanZeroType = (ztNotZero, ztZTM, ztEcho);

    TZeroVirtualChannels = record
      ZTM: Integer;
      ECHO: Integer;
      ZTM_Strob_st: Integer;
      ZTM_Strob_ed: Integer;
    end;

    TZeroChannelProcessor = class
    private
      FZeros: array of TZeroVirtualChannels;
      { private declarations }
    protected
      { protected declarations }
    public
      constructor Create;
      destructor Destroy; override;
      procedure AddZeroChannel(ZTMNum, EchoNum, StrobSt, StrobEd: Integer);
      function GetFlag(ChNum: Integer): Boolean;
    end;

implementation

Uses MainUnit, AScanUnit;

{ TAScan }

procedure TBScan.AddCoordLine(CoordLineHeight: Integer);
begin
  FIsCoordLine:=true;
  FCoordLineHeight:=CoordLineHeight;
  FCoordLine:=TCoordLine.Create(Self);
  FCoordLine.OnCoordChangeByMouse:= nil;
  if Assigned(FCore) then
    FCoordLine.Core:=FCore;
  if Self.FScale>0 then
    FCoordLine.Scale:=Self.FScale
  else
    FCoordLine.Scale:=1;
  FCoordLine.Parent:=Self;
  FCoordLine.EventList:=FEventList;
  FCoordLine.Colors:=FColorTable;
  FCoordLine.Language:=FLanguageTable;

  FCoordLine.Top:=FLineHeight*(FLinesCount)+1;

  FCoordLine.BorderWidth:=1;
  FCoordLine.Align:=alClient;
  FCoordLine.Borders:=[brLeft, brBottom, brRight];
  FCoordLine.ReBuild;
end;

procedure TBScan.ConfigureLines(WorkH, ChButtonHeight: Integer);
var
  CoordH, ButCountOnThread, t: integer;
  Line: TBScanLine;
  i: integer;
begin
{$IFDEF TwoThreadsDevice}
  t := 1;
{$ELSE}
  t := 0;
{$ENDIF}
 //
{$IF DEFINED(EGO_USW) or DEFINED(USK004R)}
  ButCountOnThread := 5;
{$ELSE}
  ButCountOnThread := 4;
{$IFEND}
//
{$IFDEF Avikon15RSP}
  ButCountOnThread := 3;
{$ENDIF}
//
{$IFDEF VMT_US}
  ButCountOnThread := 6;
{$ENDIF}
//
  CoordH := WorkH - (ChButtonHeight * ButCountOnThread * (t + 1));
  with (General.Core) do
  begin

{$IFDEF UseHardware}
{$IFDEF Avikon15RSP}
    AddLine(Kanal[0, 2], nil);
    AddLine(nil, Kanal[0, 3]);
    AddLine(Kanal[0, 0], Kanal[0, 1]);
    AddCoordLine(CoordH);
{$ELSE}
    if ProgramProfile.DeviceFamily = 'RKS-U'  then
    begin
      CoordH := 40;
      AddCoordLine(CoordH);

      AddLine.AddKanal(Kanal[0, 1]);
      AddLine.AddKanal(Kanal[0, 2]);
      AddLine.AddKanal(Kanal[0, 3]);
      AddLine.AddKanal(Kanal[0, 4]);
      AddLine.AddKanal(Kanal[0, 5]);

      if Config.SoundedScheme = Scheme2 then
      begin
        AddLine.AddKanal(Kanal[0, 6]);
        AddLine.AddKanal(Kanal[0, 7]);
        AddLine.AddKanal(Kanal[0, 8]);
      end;
    end

    else
    begin
{$IFDEF VMT_US}
     for i := 0 to 1 do
     begin
         Line := AddLine;
         Line.FlDrawSide:=True;
         Line.AddKanal(Kanal[i, 10]);
         Line.AddKanal(Kanal[i, 11]);
//
         Line := AddLine;
         Line.AddKanal(Kanal[i, 4]);
         Line.AddKanal(Kanal[i, 5]);
//
         Line := AddLine;
         Line.AddKanal(Kanal[i, 12]);
         Line.AddKanal(Kanal[i, 13]);
//
         Line := AddLine;
         Line.AddKanal(Kanal[i, 2]);
         Line.AddKanal(Kanal[i, 3]);
//
         Line := AddLine;
         Line.AddKanal(Kanal[i, 6]);
         Line.AddKanal(Kanal[i, 7]);
//
         Line := AddLine;
         Line.AddKanal(Kanal[0, 0]);
         Line.AddKanal(Kanal[0, 1]);
//
         if i=0 then AddCoordLine(CoordH);
     end;
{$ELSE}
   {$IFDEF EGO_USW}
     for I := 0 to 1 do begin

       if Config.SoundedScheme = Scheme2 then
       begin
         if i=0 then begin //left
           Line := AddLine;
           Line.FlDrawSide:=True;
           Line.AddKanal(Kanal[i, 12]); // ���
           Line.AddKanal(Kanal[i, 3]);
           Line := AddLine;
           Line.AddKanal(Kanal[i, 2]);  //�����
           Line.AddKanal(Kanal[i, 13]);
         end
         else begin  //right
           Line := AddLine;
           Line.FlDrawSide:=True;
           Line.AddKanal(Kanal[i, 2]); // ���
           Line.AddKanal(Kanal[i, 13]);
           Line := AddLine;
           Line.AddKanal(Kanal[i, 12]);  //�����
           Line.AddKanal(Kanal[i, 3]);
         end;
       end
       else
       if Config.SoundedScheme = Scheme1 then
       begin
         if i=1 then begin //right
           Line := AddLine;
           Line.FlDrawSide:=True;
           Line.AddKanal(Kanal[i, 12]); // ���
           Line.AddKanal(Kanal[i, 3]);
           Line := AddLine;
           Line.AddKanal(Kanal[i, 2]);  //�����
           Line.AddKanal(Kanal[i, 13]);
         end
         else begin //left
           Line := AddLine;
           Line.FlDrawSide:=True;
           Line.AddKanal(Kanal[i, 2]); // ���
           Line.AddKanal(Kanal[i, 13]);
           Line := AddLine;
           Line.AddKanal(Kanal[i, 12]);  //�����
           Line.AddKanal(Kanal[i, 3]);
         end;
       end
       else
       begin
         Line := AddLine;
         Line.FlDrawSide:=True;
         Line.AddKanal(Kanal[i, 2]);
         Line.AddKanal(Kanal[i, 3]);
         Line.AddKanal(Kanal[i, 12]);
         Line.AddKanal(Kanal[i, 13]);
       end;

       Line := AddLine;
       Line.AddKanal(Kanal[i, 4]);
       Line.AddKanal(Kanal[i, 5]);

       Line := AddLine;
       Line.AddKanal(Kanal[i, 6]);
       Line.AddKanal(Kanal[i, 7]);

       Line := AddLine;
       Line.AddKanal(Kanal[i, 0]);
       Line.AddKanal(Kanal[i, 1]);
       Line.AddKanal(Kanal[i, 10]);
       Line.AddKanal(Kanal[i, 11]);

       if Config.SoundedScheme = Scheme3 then
       begin
         Line := AddLine;
         Line.AddKanal(Kanal[i, 14]);
         Line.AddKanal(Kanal[i, 15]);
       end;
       if i=0 then AddCoordLine(CoordH);

     end;
 {$ELSE}
   {$IFDEF USK004R}
      Line := AddLine;
        Line.AddKanal(Kanal[0, 10]);
        Line.AddKanal(Kanal[0, 11]);
      Line := AddLine;
        Line.AddKanal(Kanal[0, 2]);
        Line.AddKanal(Kanal[0, 3]);
   {$ELSE}
      Line := AddLine;
      if Config.SoundedScheme = Wheels then
      begin
        Line.AddKanal(Kanal[0, 2]);
        Line.AddKanal(Kanal[0, 3]);
        Line.AddKanal(Kanal[0, 12]);
        Line.AddKanal(Kanal[0, 13]);
      end
      else
      begin
        Line.AddKanal(Kanal[0, 2]);
        Line.AddKanal(Kanal[0, 3]);
        Line.AddKanal(Kanal[0, 10]);
        Line.AddKanal(Kanal[0, 11]);
      end;
    {$ENDIF}

      Line := AddLine;
      Line.AddKanal(Kanal[0, 4]);
      Line.AddKanal(Kanal[0, 5]);
      if Config.SoundedScheme = Scheme3 then
      begin
        Line.AddKanal(Kanal[0, 12]);
      end;

      Line := AddLine;
      Line.AddKanal(Kanal[0, 6]);
      Line.AddKanal(Kanal[0, 7]);

      Line := AddLine;
      Line.AddKanal(Kanal[0, 0]);
      Line.AddKanal(Kanal[0, 1]);

      if Config.SoundedScheme = Wheels then
      begin
        Line.AddKanal(Kanal[0, 10]);
        Line.AddKanal(Kanal[0, 11]);
      end;

      AddCoordLine(CoordH);

      {$IFDEF TwoThreadsDevice}
      Line := AddLine;
      if Config.SoundedScheme = Wheels then
      begin
        Line.AddKanal(Kanal[1, 2]);
        Line.AddKanal(Kanal[1, 3]);
        Line.AddKanal(Kanal[1, 12]);
        Line.AddKanal(Kanal[1, 13]);
      end
      else
      begin
        Line.AddKanal(Kanal[1, 2]);
        Line.AddKanal(Kanal[1, 3]);
        Line.AddKanal(Kanal[1, 10]);
        Line.AddKanal(Kanal[1, 11]);
      end;

      Line := AddLine;
      Line.AddKanal(Kanal[1, 4]);
      Line.AddKanal(Kanal[1, 5]);
      if Config.SoundedScheme = Scheme3 then
      begin
        Line.AddKanal(Kanal[1, 12]);
      end;

      Line := AddLine;
      Line.AddKanal(Kanal[1, 6]);
      Line.AddKanal(Kanal[1, 7]);

      Line := AddLine;
      Line.AddKanal(Kanal[1, 0]);
      Line.AddKanal(Kanal[1, 1]);

      if Config.SoundedScheme = Wheels then
      begin
        Line.AddKanal(Kanal[1, 10]);
        Line.AddKanal(Kanal[1, 11]);
      end;
  {$ENDIF}
 {$ENDIF}
 {$ENDIF}
  end;
{$ENDIF}

{$ELSE}
{$IFDEF Avikon15RSP}
    AddLine(nil, nil);
    AddLine(nil, nil);
    AddLine(nil, nil);
    AddCoordLine(CoordH);
{$ELSE}
    Line := AddLine;
    Line := AddLine;
    Line := AddLine;
    Line := AddLine;
    AddCoordLine(CoordH);
{$IFDEF TwoThreadsDevice}
    Line := AddLine;
    Line := AddLine;
    Line := AddLine;
    Line := AddLine;
{$ENDIF}
{$ENDIF}
{$ENDIF}
  end;
end;

function TBScan.AddLine(): TBScanLine;
var
  NewLine: TBScanLine;
  i: Integer;
begin
  result:= nil;
  Inc(FLinesCount);
  FLineHeight:=Round((Height-FDevLineWidth-FCoordLineHeight)/FLinesCount);
  for i:=0 to FLinesCount-2 do
     begin
       FLines[i].Height:=FLineHeight;
       FLines[i].ReBuild;
     end;
  NewLine:=TBScanLine.Create(Self, Self.PixelFormat);
  result:= NewLine;
  NewLine.Parent:=Self;
  NewLine.EventList:=FEventList;
  NewLine.Top:=FLineHeight*(FLinesCount-1)+FCoordLineHeight-1;

  if FIsCoordLine then
    NewLine.Align:=alBottom
  else
    NewLine.Align:=alTop;
  NewLine.Height:=FLineHeight;
  if FLinesCount=1 then
    NewLine.Borders:=[brLeft, brTop, brRight, brBottom]
  else
    NewLine.Borders:=[brLeft, brRight, brBottom];
  NewLine.Colors:=FColorTable;
  NewLine.Language:=FLanguageTable;
  NewLine.OnClick:=Click;
  NewLine.ReBuild;
  SetLength(FLines, FLinesCount);
  FLines[FLinesCount-1]:=NewLine;
  RePaint;
end;

procedure TBScan.AddPlotPoint(RailNum, KanalNum, PlotNum, Ampl: Integer);
var
  i, Coord, j: Integer;
  F: Boolean;
begin
  for i:=0 to Length(FLines)-1 do
     begin
       F:= false;
       for j := 0 to FLines[i].KanalsCount - 1 do
          F:= F or (FLines[i].Kanals[j].Num = KanalNum);
     if (FLines[i].Rail = RailNum) and (F) then
       begin
         if Assigned(FCoordLine) then
           Coord:=FCoordLine.CurCoord
         else Coord:=0;
         FLines[i].AddPlotPoint(PlotNum, Coord, Ampl);
       end;
     end;
end;

procedure TBScan.Clear;
var
  i: Integer;
begin
  for i:=0 to Length(FLines)-1 do
     FLines[i].Clear;
end;

procedure TBScan.Click(Sender: TObject);
begin
//
end;

procedure TBScan.CoordChangeByMouse(NewCurCoord: Integer);
begin
  FCore.UseRegistratorCoord:= false;
  Self.Refresh(NewCurCoord, true, true);
end;

constructor TBScan.Create(AOwner: TComponent);
begin
  inherited;
  FCore:=nil;
  HasSecondBottom:= false;
  FThreshold:=Config.LastThreshold;
  FThresholdAmpl:=ThresholdTable[FThreshold];
  FIsCoordLine:=false;
  Self.BevelOuter:=bvNone;
  SetLength(FLines, 0);
  FDevLineWidth:=0;
  FLinesCount:=0;
  FCoordLine:=nil;
  FTempCoord:=0;
  FCoordLineHeight:=0;
  FEventList:=TCoordEventList.Create;

end;

procedure TBScan.DecThreshold;
begin
  if FThreshold >ThresholdMin then FThreshold:=FThreshold-2
  else FThreshold:=ThresholdMax;
  FThresholdAmpl:=ThresholdTable[FThreshold];
  Config.LastThreshold:=FThreshold;
end;

destructor TBScan.Destroy;
var
  i: Integer;
begin
  for i:=0 to High(FLines) do
     FLines[i].Free;
  SetLength(FLines, 0);
  FEventList.Free;
  inherited;
end;

{ TBScanLine }

procedure TBScanLine.AddBackZone(St, Ed: Integer);
begin
  if St<(3+FStrobAreaWidth) then St:=(3+FStrobAreaWidth);
  if St>(Self.Width-3-FStrobAreaWidth) then St:=(Self.Width-3-FStrobAreaWidth);
  if Ed>(Self.Width-3-FStrobAreaWidth) then Ed:=(Self.Width-3-FStrobAreaWidth);
  if Ed<(3+FStrobAreaWidth) then Ed:=(3+FStrobAreaWidth);
  FBmp.Canvas.Brush.Color:=clYellow;
  FBmp.Canvas.FillRect(Rect(St, 1, Ed, Self.Height-1));
end;

procedure TBScanLine.AddKanal(Kanal: TKanal);
var
  i, BMin, BMax: Integer;
begin
  if not Assigned(Kanal) then Exit;

  FKanalList.Add(Kanal);
  Rail:=Kanal.Nit;
  BMin:=255;
  BMax:= 0;
  for i := 0 to FKanalList.Count - 1 do
     begin
       if Kanal.Takt.BStr_st < BMin  then BMin:= Kanal.Takt.BStr_st; //AK
       if Kanal.Takt.BStr_en > BMax  then BMax:= Kanal.Takt.BStr_en; //AK
       //����� �� �-���� �� ����� ����-�� ��� � �����
//       if Kanal.BViewDelayMin < BMin  then BMin:= Kanal.BViewDelayMin;
//       if Kanal.BViewDelayMax > BMax  then BMax:= Kanal.BViewDelayMax;
     end;
  FBScanMin:=BMin;
  FBScanMax:=BMax;
  ClearMas;
  CountDelay;
end;

procedure TBScanLine.AddPlotLine(Coord1, Level1, Coord2, Level2: Integer);
var
  X1, X2, Y1, Y2: Integer;
begin
  X1:=Self.Width-Coord1;
  X2:=Self.Width-Coord2;
  Y1:=Round(FBmp.Height*Level1/255);
  Y2:=Round(FBmp.Height*Level2/255);
  FBmp.Canvas.Pen.Width:=3;
  FBmp.Canvas.Pen.Color:=clMoneyGreen;
  FBmp.Canvas.MoveTo(X1, Y1);
  FBmp.Canvas.LineTo(X2, Y2);
end;

procedure TBScanLine.AddPlotPoint(PlotNum, _Coord, _Y: Integer);
var
  Y: Integer;
begin
  if Not Assigned( FPlotMas[PlotNum] ) then FPlotMas[PlotNum]:=TPlot.Create;
  Y:=Round((Self.Height/255)*_Y);
  FPlotMas[PlotNum].AddPoint(_Coord, Y);
end;

procedure TBScanLine.AddPlotPointAsSignal(Pos, Kan: Integer; Level: Byte);
var
  X, Y, c, i, j: Integer;

procedure PutPixel(X, Y: Integer);
var
  a: integer;
  P: PByteArray;
begin
  a:=Length(FScanLines);
  if (Y<a) and (X>=0) and (Y>=0) then
    begin
      P:=FBmp.ScanLine[Y];
      if P<>nil then
        begin
          P^[X*4]:=PlotColor.R;
          P^[X*4+1]:=PlotColor.G;
          P^[X*4+2]:=PlotColor.B;
        end;
    end;
end;

begin
  X:=Pos-FStrobAreaWidth-1;
  if X>FStrobAreaWidth then
    begin
      Y:=Round(Self.Height*Level/255);
      c:=FPointSize+4;

      for i:=1 to c do
         begin
           for j:=1 to c do
              begin
                X:=X+1;
                if X<self.Width then
                  PutPixel(X, Y);
              end;
           Y:=Y+1;
           X:=Pos-FStrobAreaWidth-1;
         end;
    end;
end;

procedure TBScanLine.AddSignal(Pos: Integer; Kan: Integer; Delay, Ampl: Byte; DelayShift : Integer);
var
  X, Y: Integer;
  P: PByteArray;
  i, j, c, KanC: Byte;

procedure PutPixel(X, Y, KanNum, Amp: Integer);
begin
  if (Y<Length(FScanLines)) and (X>=0) and (Y>=0) then
    begin
      P:=FScanLines[Y];
      if P<>nil then
        begin
          P^[X*4]:=FKanColors[KanNum, Amp].R;
          P^[X*4+1]:=FKanColors[KanNum, Amp].G;
          P^[X*4+2]:=FKanColors[KanNum, Amp].B;
        end;
    end;
end;

begin
  X:=Pos-FStrobAreaWidth-1;
  if X>FStrobAreaWidth then
    begin
    (*<Rud38>*)
    Y:=DelayToLineCoords(Delay,DelayShift);
    (*</Rud38>*)
      case Kan of
        0: KanC:= 1;
        1: KanC:= 0;
        2: KanC:= 3;
        3: KanC:= 2;
      end;
      if FAmplBySize then
        c:=FKanSizeByAmp[Ampl]
      else
        c:=FPointSize;

      for i:=1 to c do
         begin
           for j:=1 to c do
              begin
                X:=X+1;
                if X<self.Width then
                  if FAmplByColor then PutPixel(X, Y, KanC, Ampl)
                    else PutPixel(X, Y, KanC, 15);
              end;
           Y:=Y+1;
           X:=Pos-FStrobAreaWidth-1;
         end;
    end;
end;

procedure TBScanLine.Clear;
begin
  FBmp.Canvas.Draw(0, 0, FBackGrBmp);
end;

procedure TBScanLine.ClearKanals;
begin
  FKanalList.Clear;
end;

procedure TBScanLine.ClearMas;
var
  i: Integer;
begin
  SetLength(FScanLines, 0);
  for i:=0 to 255 do
     FDelays[i]:=-1;
end;

procedure TBScanLine.CountColors;

const
     MinBright = 5; // ����������� ������� �������� 25%

var
  BackColor: TColor;
  R1, G1, B1, R2, G2, B2, i, j: Byte;
  Per: Single;
begin
  BackColor:=FColorTable.Color['BScan_BeckGr'];
  R1:=GetRValue(BackColor);
  G1:=GetGValue(BackColor);
  B1:=GetBValue(BackColor);

  for i:=0 to 3 do
    begin
      R2:=FKanColors[i, 0].R;
      G2:=FKanColors[i, 0].G;
      B2:=FKanColors[i, 0].B;
      for j:=MinAmp to MaxAmp do
        begin
          Per:= MinBright / 100 + ((j ) / 15) * ((100 - MinBright) / 100);
          FKanColors[i, j].R:= Round(R1 + (R2 - R1) * Per);
          FKanColors[i, j].G:= Round(G1 + (G2 - G1) * Per);
          FKanColors[i, j].B:= Round(B1 + (B2 - B1) * Per);
        end;

    end;

end;

procedure TBScanLine.CountDelay;
var
  i: Integer;
begin
  if (Height<=0) or (FBmp.Height<=0) then Exit;
  SetLength(FScanLines, FBmp.Height);
  for i:=0 to FBmp.Height-1 do
     FScanLines[i]:=FBmp.ScanLine[i];

  FK:=Height/(FBScanMax-FBScanMin);

  if FScanDir=bdUp then
    begin
      for i:=0 to FBScanMin-1 do
         FDelays[i]:=Height-2;
      for i:=FBScanMax+1 to 255 do
         FDelays[i]:=1;
      for i:=FBScanMin to FBScanMax do
         begin
           FDelays[i]:=Round((Height-2)-(i-FBScanMin)*FK);
           if FDelays[i]<1 then FDelays[i]:=1;
           if FDelays[i]>Height-2 then FDelays[i]:=Height-2;
       end
    end
  else
    begin
      for i:=0 to FBScanMin-1 do
         FDelays[i]:=1;
      for i:=FBScanMax+1 to 255 do
         FDelays[i]:=Height-2;
      for i:=FBScanMin to FBScanMax do
         begin
           FDelays[i]:=Round((i-FBScanMin)*FK);
           if FDelays[i]<1 then FDelays[i]:=1;
           if FDelays[i]>Height-2 then FDelays[i]:=Height-2;
         end;
    end;
end;

constructor TBScanLine.Create(AOwner: TComponent;  BPP: TPixelFormat);
var
  i: Integer;
begin
  inherited Create(AOwner);
  FKanalList:= TKanalList.Create;
  FKanalList.Clear;
  for i:=0 to High(FPlotMas) do
     FPlotMas[i]:=nil;
  Self.DoubleBuffered:=true;
  FPointSize:=2;
  FAmplByColor:=false;
  FAmplBySize:=false;

  FBorders:=[brTop, brBottom];
  FBorderWidth:=1;

  FKanColors[0, 0].R:=255;
  FKanColors[0, 0].G:=0;
  FKanColors[0, 0].B:=0;

  FKanColors[1, 0].R:=0;
  FKanColors[1, 0].G:=0;
  FKanColors[1, 0].B:=255;

  FKanColors[2, 0].R:=0;
  FKanColors[2, 0].G:=190;
  FKanColors[2, 0].B:=0;

  FKanColors[3, 0].R:=160;
  FKanColors[3, 0].G:=0;
  FKanColors[3, 0].B:=160;

  FKanSizeByAmp[0]:=1;
  FKanSizeByAmp[1]:=1;
  FKanSizeByAmp[2]:=1;
  FKanSizeByAmp[3]:=1;
  FKanSizeByAmp[4]:=1;
  FKanSizeByAmp[5]:=1;
  FKanSizeByAmp[6]:=1;
  FKanSizeByAmp[7]:=2;
  FKanSizeByAmp[8]:=2;
  FKanSizeByAmp[9]:=2;
  FKanSizeByAmp[10]:=3;
  FKanSizeByAmp[11]:=3;
  FKanSizeByAmp[12]:=3;
  FKanSizeByAmp[13]:=4;
  FKanSizeByAmp[14]:=4;
  FKanSizeByAmp[15]:=4;

  FBackGrBmp.PixelFormat:=BPP;
  FBmp.PixelFormat:=BPP;

  if BPP=pf16bit then
    PutPixelProc:=PutPixel16bit;
  if BPP=pf32bit then
    PutPixelProc:=PutPixel32bit;

  ClearMas;

  FBScanMin:=0;
  FBScanMax:=1;
  FScanDir:=bdUp;

  FBmp.Canvas.Pen.Width:=4;
  FBmp.Canvas.Pen.Color:=clBlack;
  FBmp.Canvas.MoveTo(0,0);
  FBmp.Canvas.LineTo(FBmp.Width,FBmp.Height);
  FBmp.Canvas.MoveTo(0,FBmp.Height);
  FBmp.Canvas.LineTo(FBmp.Width,0);

  FStrobAreaWidth:=Round(Self.Width*StrobAreaWidthPercent);
  FDrawSide:=False;
  Self.ReBuild;
end;

destructor TBScanLine.Destroy;
var
  i: Integer;
begin
  SetLength(FScanLines, 0);
  for i:=0 to High(FPlotMas) do
    if Assigned(FPlotMas[i]) then FPlotMas[i].Free;
  inherited;
end;

procedure TBScanLine.DrawBackGround;

const
  StrDx = 10;
  dx = 15;
  dy = 5;

var
  FColor, BColor: TColor;
  D1, D2, D3: Integer;
begin
  inherited;
  if (not Assigned(FColorTable)) or (not Assigned(FLanguageTable)) then Exit;
  FBackGrBmp.Canvas.Brush.Color:=FColorTable.Color['BScan_BeckGr'];
  FBackGrBmp.Canvas.FillRect(Rect(0, 0, FBackGrBmp.Width, FBackGrBmp.Height));
  FBorderColor:=FColorTable.Color['BScan_Border'];
  FBackGrBmp.Canvas.Pen.Color:=FBorderColor;
  FBackGrBmp.Canvas.Brush.Color:=FBorderColor;
  FBackGrBmp.Canvas.Pen.Width:=FBorderWidth;
  D1:=0;
  D2:=1;
  D3:=0;
  if FBorderWidth>1 then
    begin
      D1:=FBorderWidth div 2;
      D2:=D1;
      if FBorderWidth mod 2 = 0 then D3:=0 else D3:=1;
    end;
  if brLeft in FBorders then
    begin
      FBackGrBmp.Canvas.MoveTo(D1, 0);
      FBackGrBmp.Canvas.LineTo(D1, Height);
    end;
  if brRight in FBorders then
    begin
      FBackGrBmp.Canvas.MoveTo(Width-D2-D3, 0);
      FBackGrBmp.Canvas.LineTo(Width-D2-D3, Height);
    end;
  if brTop in FBorders then
    begin
      FBackGrBmp.Canvas.MoveTo(D1, D1);
      FBackGrBmp.Canvas.LineTo(Width, D1);
    end;
  if brBottom in FBorders then
    begin
      FBackGrBmp.Canvas.MoveTo(0, Height-D2-D3);
      FBackGrBmp.Canvas.LineTo(Width, Height-D2-D3);
    end;

  FBackGrBmp.Canvas.Brush.Color:=clBlack;
  FBackGrBmp.Canvas.MoveTo(FStrobAreaWidth-1, 1);
  FBackGrBmp.Canvas.LineTo(FStrobAreaWidth-1, Height-1);

  FBackGrBmp.Canvas.MoveTo(Width-1-FStrobAreaWidth, 1);
  FBackGrBmp.Canvas.LineTo(Width-1-FStrobAreaWidth, Height-1);

  Self.Canvas.Brush.Style:=bsSolid;

  FColor:=FColorTable.Color['BScan_ForwardChannel'];
  BColor:=FColorTable.Color['BScan_BackwardChannel'];

{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US) }
  if (not Config.DirectionOfMotion) and //B-Forward
     (KanalsCount>0) and
     (Kanals[0].Takt.Alfa<>0) and
     (Kanals[0].Takt.Reg<>1) then begin //is not handle channel
      DrawStrobZone(BColor,FColor);
      FKanColors[0, 0].R:=GetRValue(BColor);
      FKanColors[0, 0].G:=GetGValue(BColor);
      FKanColors[0, 0].B:=GetBValue(BColor);
      FKanColors[1, 0].R:=GetRValue(FColor);
      FKanColors[1, 0].G:=GetGValue(FColor);
      FKanColors[1, 0].B:=GetBValue(FColor);
  end
  else begin // A-Forward or 0 kanal
    DrawStrobZone(FColor,BColor);
    FKanColors[0, 0].R:=GetRValue(FColor);
    FKanColors[0, 0].G:=GetGValue(FColor);
    FKanColors[0, 0].B:=GetBValue(FColor);
    FKanColors[1, 0].R:=GetRValue(BColor);
    FKanColors[1, 0].G:=GetGValue(BColor);
    FKanColors[1, 0].B:=GetBValue(BColor);
  end;
{$ELSE}
  (*<Rud refactor>*)
  DrawStrobZone(FColor,BColor);
  (*<Rud refactor>*)
  FKanColors[0, 0].R:=GetRValue(FColor);
  FKanColors[0, 0].G:=GetGValue(FColor);
  FKanColors[0, 0].B:=GetBValue(FColor);
  FKanColors[1, 0].R:=GetRValue(BColor);
  FKanColors[1, 0].G:=GetGValue(BColor);
  FKanColors[1, 0].B:=GetBValue(BColor);
{$IFEND}

  CountColors;
  Clear;
end;

procedure TBScanLine.DrawStrobZone(FColor,BColor : TColor);
var
  KanCount: integer;
  i : Integer;
  Kan: TKanal;
  StrobPainter: TStrobPainter;
  DelayShift : integer;
  Y1, Y2: Integer;
  flag: boolean;
  (*<Rud39>*)
  hasDelay : boolean;
  (*</Rud39>*)
begin
  FBackGrBmp.Canvas.Pen.Width:= 1;
  if General.CurLogicalMode = bmEstimate then
    KanCount:= 1
  else
    KanCount:= FKanalList.Count;

  // ��������� �������
  for i:=0 to KanCount - 1 do
     begin
       if General.CurLogicalMode = bmEstimate then
         Kan:= General.CurLogicalKanal
       else if i < FKanalList.Count  then
         Kan:= FKanalList[i]
       else Kan:= nil;

       if Assigned(Kan) then
         begin
           if ((Kan.Num mod 2) = 0) and not ((Kan.Num=12) and (Config.SoundedScheme = Scheme3)) then
             begin
               StrobPainter:= TStrobPainter.Create(FBackGrBmp, 0, FStrobAreaWidth, 1, Height-2);
               StrobPainter.Color:=FColor;
             end
           else
             begin
               if ProgramProfile.DeviceFamily = 'RKS-U' then
                 StrobPainter:= TStrobPainter.Create(FBackGrBmp, 0, FStrobAreaWidth, 1, Height-2)
               else
                StrobPainter:= TStrobPainter.Create(FBackGrBmp, Width - FStrobAreaWidth+1, Width, 1, Height-2);

              StrobPainter.Color:=BColor;
             end;

           hasDelay := false;
           if (Kan.Takt<>Nil) then
               begin
                  SetStrobKind(StrobPainter, Kan.Takt.Alfa, Kan.Num, Kan.Nit);
                  DelayShift:=0;
{$IFnDEF VMT_US}
                  if (Kan.Takt.Alfa=0) and (FKanalList.Count>2) then
                      hasDelay := true;
                  if hasDelay = true then
                      begin
                          if Kan.Num in [0,1] then
                              DelayShift := Config.BottomTimeShift;
                          if Kan.Num in [10,11] then
                              DelayShift := -Config.BottomTimeShift;
                      end;
{$ELSE}
                  if Kan.Num=2 then DelayShift := Config.BottomTimeShift
                      else if Kan.Num=3 then DelayShift := -Config.BottomTimeShift;
{$ENDIF}

               end;

             Y1:=DelayToLineCoords( {Max( Kan.BViewDelayMin,} Kan.Str_st { )}, DelayShift);
             Y2:=DelayToLineCoords( {Min(} Kan.Str_en{, Kan.BViewDelayMax )}, DelayShift);


             if (Y1<1) or (Y2<1) then
               begin
                 StrobPainter.hasBegin := false;
                 if Y1<1 then Y1:=1;
                 if Y2<1 then Y2:=1;
               end
             else
                StrobPainter.hasBegin := true;

             if (Y1>Height-3) or (Y2>Height-3) then
               begin
                  StrobPainter.hasEnd := false;
                  if Y1>Height-3 then Y1:=Height-3;
                  if Y2>Height-3 then Y2:=Height-3;
               end
             else
               StrobPainter.hasEnd := true;

           StrobPainter.SetStrobValues(Y1, Y2);
           flag:= true;
           if (Kan.Takt.Alfa=0) and (KanCount>=2) then
           begin
            if (i=0) or (i=1) then
{$IFDEF EGO_USW}
             Flag:= (Config.SelectChan0 = scWPAandWPB);
{$ELSE}
             flag:= Config.SecondaryBottomOn;
{$ENDIF}
           end;

           if Flag then
             StrobPainter.DrawStrob;
           if (Kan.Takt.Alfa=42) {and (not Kan.TwoEcho)} or (Kan.Takt.Alfa=45) then
             if (Assigned(Kan.Takt.Kanal2)) and (General.CurLogicalMode <> bmEstimate) then
               begin
                 Y1:=DelayToLineCoords( {Max( Kan.Takt.Kanal2.BViewDelayMin,} Kan.Takt.Kanal2.Str_st { )}, DelayShift);
                 Y2:=DelayToLineCoords( {Min(} Kan.Takt.Kanal2.Str_en{, Kan.Takt.Kanal2.BViewDelayMax )}, DelayShift);
                 if (Y1<1) or (Y2<1) then
                   begin
                     StrobPainter.hasEnd := false;
                     if Y1<1 then Y1:=1;
                     if Y2<1 then Y2:=1;
                   end
                 else
                    StrobPainter.hasEnd := true;

                 if (Y1>Height-3) or (Y2>Height-3) then
                   begin
                      StrobPainter.hasBegin := false;
                      if Y1>Height-3 then Y1:=Height-3;
                      if Y2>Height-3 then Y2:=Height-3;
                   end
                 else
                   StrobPainter.hasBegin := true;
                 (*</Rud47>*)
                 StrobPainter.SetStrobValues(Y1, Y2);
                 StrobPainter.DrawStrob;
               end;
            StrobPainter.Free;
         end;
     end;
end;


procedure TBScanLine.SetStrobKind(var StrobPainter : TStrobPainter; KanalAlfa, KanalNum : Integer; KanalNit : Byte);
begin
  StrobPainter.Kind:=skCenter;
  if (KanalAlfa=0) and (FKanalList.Count>2) then
  begin
     case KanalNum of
       0: StrobPainter.Kind:=  skOuter;
       10: StrobPainter.Kind:= skInner;
       1: StrobPainter.Kind:=  skOuter;
       11: StrobPainter.Kind:= skInner;
     end;
  end;

{$IFDEF EGO_USW}
  if (KanalAlfa=65) then
    if Config.SoundedScheme = Scheme3 then
       begin
         if KanalNit = 0 then
           case KanalNum of
              2: StrobPainter.Kind:= skOuter;
              3: StrobPainter.Kind:= skOuter;
              12: StrobPainter.Kind:= skInner;
              13: StrobPainter.Kind:= skInner;
           end;
         if KanalNit = 1 then
           case KanalNum of
              2: StrobPainter.Kind:= skInner;
              3: StrobPainter.Kind:= skInner;
              12: StrobPainter.Kind:= skOuter;
              13: StrobPainter.Kind:= skOuter;
           end;
       end;
{$ELSE}
  if (KanalAlfa=58) then
   begin
     if Config.SoundedScheme = Wheels then
       begin
         if KanalNit = 0 then
           case KanalNum of
              2: StrobPainter.Kind:= skInner;
              3: StrobPainter.Kind:= skInner;
              12: StrobPainter.Kind:= skOuter;
              13: StrobPainter.Kind:= skOuter;
           end;
         if KanalNit = 1 then
           case KanalNum of
              2: StrobPainter.Kind:= skOuter;
              3: StrobPainter.Kind:= skOuter;
              12: StrobPainter.Kind:= skInner;
              13: StrobPainter.Kind:= skInner;
           end;
       end
     else if Config.SoundedScheme = Scheme3 then
       begin
         if KanalNit = 0 then
           case KanalNum of
              2: StrobPainter.Kind:= skInner;
              3: StrobPainter.Kind:= skOuter;
              5: StrobPainter.Kind:= skOuter;
              10: StrobPainter.Kind:= skOuter;
              11: StrobPainter.Kind:= skInner;
              12: StrobPainter.Kind:= skInner;
           end;
         if KanalNit = 1 then
           case KanalNum of
              2: StrobPainter.Kind:= skOuter;
              3: StrobPainter.Kind:= skInner;
              5: StrobPainter.Kind:= skInner;
              10: StrobPainter.Kind:= skInner;
              11: StrobPainter.Kind:= skOuter;
              12: StrobPainter.Kind:= skOuter;
           end;
       end
     else
       begin
         if KanalNit = 0 then
           case KanalNum of
              2: StrobPainter.Kind:= skInner;
              3: StrobPainter.Kind:= skOuter;
              10: StrobPainter.Kind:= skOuter;
              11: StrobPainter.Kind:= skInner;
           end;
         if KanalNit = 1 then
           case KanalNum of
              2: StrobPainter.Kind:= skOuter;
              3: StrobPainter.Kind:= skInner;
              10: StrobPainter.Kind:= skInner;
              11: StrobPainter.Kind:= skOuter;
           end;
       end;
   end;
{$ENDIF}
  if FKanalList.Count = 1 then
   StrobPainter.Kind:=skCenter;
end;

procedure TBScanLine.DrawForeGround;
begin
  //inherited;
  //
end;

function TBScanLine.GetKanal(Index: Integer): TKanal;
begin
  result:= FKanalList[Index];
end;

function TBScanLine.GetKanalsCount: Integer;
begin
  result:= FKanalList.Count;
end;

(*<Rud38>*)
function TBScanLine.DelayToLineCoords(Delay : byte; DelayShift : Integer ) : integer;//byte;AK
const
  maxDelay = 255;
var
  newDelay : integer;
begin
  Assert( (abs(DelayShift) in [0..10] ) , 'Bad DelayShift');
  // ������� ��������-������ ���
  if DelayShift = 0 then
    if FDelays[Delay] >=0 then
      result := FDelays[Delay]
    else
      result := 0
  // �������� ��� ������� �������-���� �����
  else
    begin
      if DelayShift < 0 then
        newDelay := round(  Delay  *
                            (FBScanMax-FBScanMin) /
                            ( (FBScanMax-FBScanMin) + abs(DelayShift) ) )
      else
        newDelay := round(  ( Delay + DelayShift) *
                            (FBScanMax-FBScanMin) /
                            ( (FBScanMax-FBScanMin) + abs(DelayShift) ) );

      Assert(newDelay in [0..255], Format('newDelay is out of !BYTE! range %d', [newDelay]));
      result := {byte}(FDelays[newDelay] );//?����� �� ����� byte()?
    end;
end;
(*</Rud38>*)

procedure TBScanLine.MoveImage(Delta: Integer; BackMotion: Boolean);
begin
  if Delta>0 then
    begin
      if Delta < FBmp.Width - 2*FStrobAreaWidth then //04-12-2015/AK
        FBmp.Canvas.CopyRect(Rect(FStrobAreaWidth, 0, FBmp.Width-Delta-1-FStrobAreaWidth, FBmp.Height), FBmp.Canvas, Rect(Delta+FStrobAreaWidth, 0, FBmp.Width-1-FStrobAreaWidth, FBmp.Height));
      if BackMotion then
        FBmp.Canvas.Brush.Color:=clYellow
      else
        FBmp.Canvas.Brush.Color:=clWhite;
      if Delta < FBmp.Width - 2*FStrobAreaWidth then //04-12-2015/AK
        FBmp.Canvas.FillRect(Rect(FBmp.Width-Delta-1-FStrobAreaWidth, 1, FBmp.Width-1-FStrobAreaWidth, FBmp.Height-1))
      else FBmp.Canvas.FillRect(Rect(FStrobAreaWidth, 1, FBmp.Width-1-FStrobAreaWidth, FBmp.Height-1))
    end;
end;

procedure TBScanLine.Paint;

const
  StrDx = 10;
  dx = 15;
  dy = 5;

var
  X, i, j: Integer;
  Flag: boolean;

  S1, S2:   String;
  A: Integer;
begin
     Self.Canvas.Draw(0, 0, FBmp);

     // ��������� ����� ������������
     {$IFDEF PAINTER}
{     X:=Self.Width-FPainterCoord;
     if (X>0) and (Self.Width>0) then
       begin
         Self.Canvas.Pen.Color:=clGray;
         Self.Canvas.Pen.Width:=1;
         Self.Canvas.Pen.Style:=psDash;
         Self.Canvas.Brush.Style:=bsCross;
         Self.Canvas.MoveTo(X, 0);
         Self.Canvas.LineTo(X, Self.Height);
         Self.Canvas.Pen.Style:=psSolid;
       end;
}     {$ENDIF}

  // ��������� ������� (��, ��, ������)
  if Assigned(FEventList) then
    begin
      Self.Canvas.Pen.Color:=clBlack;
      Self.Canvas.Pen.Width:=2;
      for i:=0 to FEventList.Count - 1 do
         begin
           Flag:= true;
           if FEventList[i].EventIcon = iDefect then
             begin
               if FKanalList.Count > 0 then
                 if Assigned(FKanalList[0]) then
                   Flag:= (FEventList[i].Rail = FKanalList[0].Nit) or (FEventList[i].Rail > 1);
             end;
           if Flag then
             begin
               X:=Self.Width-FEventList[i].Coord;
               Self.Canvas.MoveTo(X, 0);
               Self.Canvas.LineTo(X, Self.Height);
             end;
         end;
    end;

  // ��������� �������
{$IFDEF HEADCHECKING}  //??? ����� �� ���� ����� ��� headchecking?
  for i:=0 to High(FPlotMas) do
    if Assigned(FPlotMas[i]) then
      begin
        Self.Canvas.Pen.Width:=3;
        Self.Canvas.Pen.Color:=PlotColors[i];
        for j:=0 to FPlotMas[i].Count-1 do
           begin
             if j>0 then
                 begin
                   Self.Canvas.MoveTo(Self.Width-FPlotMas[i][j-1].X, FPlotMas[i][j-1].Y);
                   Self.Canvas.LineTo(Self.Width-FPlotMas[i][j].X, FPlotMas[i][j].Y);
                 end;
           end;
      end;
{$ENDIF}

{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
if FDrawSide then
if General.CurLogicalMode=bmSearch then begin
  if Rail=0 then s1:=Language.Caption['Left Side']
  else s1:=Language.Caption['Right Side'];
  Self.Canvas.Brush.Style:=bsClear;
//  A:=Self.Canvas.Font.Height;
  Self.Canvas.Font.Height:=16;
  Self.Canvas.Font.Style:=[fsItalic];
  X:=Self.Width-Self.Canvas.TextWidth(s1)-FStrobAreaWidth-5;
  Self.Canvas.TextOut(X, 2, s1);
//  Self.Canvas.Brush.Style:=bsSolid;
//  Self.Canvas.Font.Height:=A;
//  Self.Canvas.Font.Style:=[fsBold];
end;
{$IFEND}


end;

procedure TBScanLine.PutPixel16bit(X, Y: Integer; Color: TRGB);
var
  P: PByteArray;
begin
  if (Y>FBmp.Height) or (Y<0) then ShowMessage(IntToStr(Y));
    P:=FBmp.ScanLine[Y];
  if P<>nil then
    begin
      P^[X*4]:=Color.R;
      P^[X*4+1]:=Color.G;
      P^[X*4+2]:=Color.B;
    end;
end;

procedure TBScanLine.PutPixel32bit(X, Y: Integer; Color: TRGB);
var
  P: PByteArray;
begin
  if (Y>FBmp.Height) or (Y<0) then ShowMessage(IntToStr(Y));
    P:=FBmp.ScanLine[Y];
  if P<>nil then
    begin
      P^[X*4]:=Color.R;
      P^[X*4+1]:=Color.G;
      P^[X*4+2]:=Color.B;
    end;
end;

procedure TBScanLine.RefreshPlot(Scale, CurCoord: Integer);
var
  i: Integer;
begin
  for i:=0 to High(FPlotMas) do
    if Assigned(FPlotMas[i]) then
      begin
        // ������� ����� �� ���������� � �����. � ����� ������� �����������
        FPlotMas[i].Refresh(Scale, CurCoord);
        // �������������� �����, ������� �� ������� (������ ������ ������)
        FPlotMas[i].Filter(Self.Width);
      end;
end;

procedure TBScanLine.Resize;
begin
  inherited;
  FStrobAreaWidth:=Round(Self.Width*StrobAreaWidthPercent);
  Self.ReBuild;
  CountDelay;
end;

procedure TBScan.EventRoutine;
var
   SIdx, Eidx, i, EventX: Integer;
   ID, Id2: Byte;
   pData: PEventData;
   //CoordPost: TCoordPost;
begin
  // �������� ������������ ������� � ������� �����������

  FEventList.Clear;
  if not Assigned(FCoordLine) then Exit;
  FCore.Registrator.GetEventIdx(FCoordLine.StartCoord, FCurDisCoord, SIdx, Eidx, 0);
  for i:=SIdx to Eidx do
     begin

       EventX:=Round((FCurDisCoord-FCore.Registrator.Event[i].DisCoord)/FScale);
       if FCore.Registrator.Event[i].ID = EID_Stolb then
         begin
           FCore.Registrator.GetEventData(i, ID, pData);
           if pedCoordPost(pData)^.Km[0] <> pedCoordPost(pData)^.Km[1] then
             FEventList.AddEvent(EventX, 2, iKm)
           else
             FEventList.AddEvent(EventX, 2, iPK);
         end;
       // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       if FCore.Registrator.Event[i].ID = EID_DefLabel then
         begin
           FCore.Registrator.GetEventData(i, ID2, pData);
       {$IFDEF PAINTER}
         FEventList.AddEvent(EventX{+FPainterCoord}, Ord(pedDefLabel(pData)^.Rail), iDefect);//30-07-2015 AK
       {$ELSE}
         FEventList.AddEvent(EventX, Ord(pedDefLabel(pData)^.Rail), iDefect);
       {$ENDIF}
         end;
       if FCore.Registrator.Event[i].ID = EID_AirBrush then
         begin
           FCore.Registrator.GetEventData(i, ID2, pData);
           FEventList.AddEvent(EventX, Ord(pedAirBrush(pData)^.Rail), iDefect);
         end;
//{!!!
       if FCore.Registrator.Event[i].ID = EID_AirBrush2 then //!30-10-2015
         begin
           FCore.Registrator.GetEventData(i, ID2, pData);
           FEventList.AddEvent(EventX, Ord(pedAirBrush(pData)^.Rail), iDefect);
         end;
//}

       if (FCore.Registrator.Event[i].ID = EID_TextLabel) or (FCore.Registrator.Event[i].ID = EID_Switch) then
         begin
           FEventList.AddEvent(EventX, 2, iTxt);
         end;

       if FCore.Registrator.Event[i].ID = EID_StBoltStyk then
         FEventList.AddEvent(EventX, 2, iJointSt);
       if FCore.Registrator.Event[i].ID = EID_EndBoltStyk then
         FEventList.AddEvent(EventX, 2, iJointEd);
     end;
end;

procedure TBScan.ForceResize;
begin
  Self.Resize;
end;

function TBScan.GetAmpByColor: Boolean;
begin
  if Length(FLines) > 0 then
     result:=FLines[0].AmpByColor;
end;

function TBScan.GetAmpBySize: Boolean;
begin
  if Length(FLines) > 0 then
    result:=FLines[0].AmplBySize;
end;

function TBScan.GetCoordUnits: TCoordUnits;
begin
  if Assigned(FCoordLine) then
    result:=FCoordLine.CoordUnits;
end;

function TBScan.GetFontName: string;
begin
  //
end;

function TBScan.GetLine(Index: Integer): TBScanLine;
begin
  Result:=FLines[Index];
end;

function TBScan.GetLinesCount: Integer;
begin
  result:=Length(FLines);
end;

function TBScan.GetShowIcons: Boolean;
begin
  if (Assigned(FCoordLine)) then
    result:= FCoordLine.ShowIcons;
end;

procedure TBScan.IncThreshold;
begin
  if FThreshold<ThresholdMax then FThreshold:=FThreshold+2
  else FThreshold:=ThresholdMin;
  FThresholdAmpl:=ThresholdTable[FThreshold];
  Config.LastThreshold:=FThreshold;
end;

procedure TBScan.PlotRoutine(Pos: Integer; Line: TBScanLine);
var
  i, j, c, A: Integer;
  MaxA: Byte;
begin
  for i:=0 to 1 do
     begin
       MaxA:=SignInBaseProbe[0, 0];
       c:=SignInBaseProbeCount[i];
       for j:=0 to c-1 do
         if SignInBaseProbe[i, j] > MaxA then
           MaxA:=SignInBaseProbe[i, j];
       A:=Round(MaxA*MaxBaseAmpl/15*0.9);
       Line.AddPlotPointAsSignal(Pos, 0, A);
     end;
end;

function TBScan.ProcessSingnal(StartDisCoord: Integer): Boolean;
begin
  Result := ProcessSingnalDummy(StartDisCoord, false);
end;

function TBScan.ProcessSingnalExtern(StartDisCoord: Integer): Boolean;
begin
  Result := ProcessSingnalDummy(StartDisCoord, true);
end;

function TBScan.ProcessSingnalDummy(StartDisCoord: Integer; ExternTrue : Boolean): Boolean;
var
  Pos, CurDisCoord, i, j, Ch, k, TrueCH: Integer;
  CurEcho: TCurEcho;
  c: Byte;
  ColorNum, Temp: Integer;
  R: TRail;
  Flag: Boolean;
  KanZeroType: TKanZeroType;
  ZTMCh, EchoCh, DelayShift: Integer;
  (*<Rud42>*)
  ChannelsParams : TEvalChannelsParams;
  (*</Rud42>*)

  procedure GetRealDelay(Ch : Integer);
  var
    j : Integer;
  begin
    c:=CurEcho[R, Ch].Count;
    for j:=1 to c do
       CurEcho[R, Ch].Delay[j]:=CurEcho[R, Ch].Delay[j] div FCore.Kanal[Ord(R), Ch].Takt.DelayFactor;
  end;

  procedure GetRealDelayHandl;
  var
    j : Integer;
  begin
    c:=CurEcho[R, 0].Count;
      for j:=1 to c do
        CurEcho[R, 0].Delay[j]:=CurEcho[R, 0].Delay[j] div HKanals[FCore.CurrentKanalNum].Takt.DelayFactor;
  end;

begin
  // �������� ���������� � �������� �������� � ����� ���
  SignInBaseProbeCount[0]:=0;
  SignInBaseProbeCount[1]:=0;
  FillChar(SignInBaseProbe[0], 8, 0);
  FillChar(SignInBaseProbe[1], 8, 0);

  if ExternTrue = false then
    begin
      CurDisCoord:=FCore.Registrator.CurLoadDisCoord[1];
      CurEcho:=FCore.Registrator.CurLoadEcho[1];

      (*<Rud42>*)
      ChannelsParams := FCore.Registrator.CurLoadParams[1];
      (*</Rud42>*)
    end
  else
    begin
    CurDisCoord:=FCore.FExternFileData.CurDisCoord;
    CurEcho:= FCore.FExternFileData.CurEcho;

    (*<Rud42>*)
    ChannelsParams := FCore.FExternFileData.CurParams.Par;
    (*</Rud42>*)
    end;

  if not FRefreshModeFull then
    begin
      Temp:=CurDisCoord-StartDisCoord-FDc;
      if FM2<>0 then
        begin
          if Temp mod FM2 = 0 then
            Temp:= Temp div FM2
          else
            Temp:= (Temp div FM2) - 1;
        end
      else Temp:=0;

      Pos:=Self.Width+Temp-3;
    end
  else
    begin
      Temp:=CurDisCoord-StartDisCoord;
      if FM2<>0 then
        begin
          if Temp mod FM2 = 0 then
            Temp:= Temp div FM2
          else
            Temp:= (Temp div FM2) - 1;
        end
      else Temp:=0;
      Pos:=Temp+3+FFullRefreshStart-6;
    end;

  if FCore.Mode = rHand then begin
    R:=TRail(0);
    GetRealDelayHandl;
  end
  else begin
  R:=TRail(0);
    GetRealDelay(0);
    GetRealDelay(1);
{$IFDEF EGO_USW}
    GetRealDelay(10);
    GetRealDelay(11);
{$ELSE}
    if Config.SoundedScheme = Wheels then
      GetRealDelay(11);
    if Config.SoundedScheme = Scheme3 then
      GetRealDelay(4);
{$ENDIF}

  R:=TRail(1);
    GetRealDelay(0);
    GetRealDelay(1);
{$IFDEF EGO_USW}
    GetRealDelay(10);
    GetRealDelay(11);
{$ELSE}
    if Config.SoundedScheme = Wheels then
        GetRealDelay(11);
    if Config.SoundedScheme = Scheme3 then
        GetRealDelay(4);
{$ENDIF}
  end;//no handl

  for i:=0 to FLinesCount-1 do
     begin
       R:=TRail(FLines[i].Rail);

       for k := 0 to FLines[i].KanalsCount - 1 do
          begin
            if Assigned(FLines[i].Kanals[k]) then
              begin
                Ch:=FLines[i].Kanals[k].Num;
                TrueCH:= Ch;
{$IFDEF EGO_USW}
                if Ch=8 then Ch:=6;
                if Ch=9 then Ch:=7;
                    if Ch=0 then KanZeroType:=ztZTM
                    else if Ch=1 then KanZeroType:=ztEcho
                    else if Ch=10 then KanZeroType:=ztZTM
                    else if Ch=11 then KanZeroType:=ztEcho
                    else KanZeroType:=ztNotZero;
                    if (Ch=0) or (Ch=1) then
                      begin
                        ZTMCh:= 0;
                        EchoCh:= 1;
                      end;
                    if (Ch=10) or (Ch=11) then
                      begin
                        ZTMCh:= 10;
                        EchoCh:= 11;
                      end;
{$ENDIF}
{$IFDEF Avikon16_IPV}
                if Ch=8 then Ch:=6;
                if Ch=9 then Ch:=7;
                if (Config.SoundedScheme = Scheme1) or (Config.SoundedScheme = Scheme2) or (Config.SoundedScheme = Scheme3) then
                  begin
                    if Ch=0 then KanZeroType:=ztZTM
                    else if Ch=1 then KanZeroType:=ztEcho
                    else KanZeroType:=ztNotZero;
                    if (Ch=0) or (Ch=1) then
                      begin
                        ZTMCh:= 0;
                        EchoCh:= 1;
                      end;
                  end;
                if Config.SoundedScheme = Wheels then
                  begin
                    if Ch=0 then KanZeroType:=ztZTM
                    else if Ch=1 then KanZeroType:=ztEcho
                    else if Ch=10 then KanZeroType:=ztZTM
                    else if Ch=11 then KanZeroType:=ztEcho
                    else KanZeroType:=ztNotZero;
                    if (Ch=0) or (Ch=1) then
                      begin
                        ZTMCh:= 0;
                        EchoCh:= 1;
                      end;
                    if (Ch=10) or (Ch=11) then
                      begin
                        ZTMCh:= 10;
                        EchoCh:= 11;
                      end;
                  end;

{$ENDIF}
{$IFDEF Avikon14_2Block}
                if Ch=8 then Ch:=6;
                if Ch=9 then Ch:=7;

                if Ch=12 then Ch:=2
                else if Ch=2 then Ch:=12;
                if Ch=3 then Ch:=13
                else if Ch=13 then Ch:=3;

                if Ch=0 then KanZeroType:=ztZTM
                else if Ch=1 then KanZeroType:=ztEcho
                else KanZeroType:=ztNotZero;
                if Config.SoundedScheme = Wheels then
                  begin
                    if Ch=10 then KanZeroType:=ztZTM
                    else if Ch=11 then KanZeroType:=ztEcho
                    else KanZeroType:=ztNotZero;
                  end;

                if (Ch=0) or (Ch=1) then
                  begin
                    ZTMCh:= 0;
                    EchoCh:= 1;
                  end;
                if Config.SoundedScheme = Wheels then
                  if (Ch=10) or (Ch=11) then
                    begin
                      ZTMCh:= 10;
                      EchoCh:= 11;
                    end;
{$ENDIF}
//
{$IFDEF VMT_US}
                if Ch=8 then Ch:=6;
                if Ch=9 then Ch:=7;
                if Ch=0 then KanZeroType:=ztZTM
                    else if Ch=1 then KanZeroType:=ztEcho
                             else  KanZeroType:=ztNotZero;

                if (Ch=0) or (Ch=1) then
                begin
                    ZTMCh:= 0;
                    EchoCh:= 1;
                end;
{$ENDIF}
//
                if FCore.Mode = rHand then ColorNum:=1
                else if Ch mod 2 = 0 then ColorNum:=0 else ColorNum:=1;

                if KanZeroType = ztZTM then ColorNum:=0;
                if KanZeroType = ztEcho then ColorNum:=1;

                if Ch=0 then Ch:=1;
{$IFnDEF EGO_USW}
                if Config.SoundedScheme = Wheels then
{$ENDIF}
{$IFnDEF VMT_US}
                if Ch=10 then Ch:=11;
{$ENDIF}
                if FCore.Mode = rHand then
                  begin
                    Ch:= 0;
                    TrueCH:= 0;
                    R:= rLeft;
                  end;
                if Ch<>-1 then
                  begin
                    c:=CurEcho[R, Ch].Count;

                    for j:=1 to c do
                      begin
                        Flag:=true;

                        DelayShift:= 0;
                        if KanZeroType <> ztNotZero then
                          begin
                            if TrueCH = ZTMCh then
                              if (CurEcho[R, EchoCh].Delay[j]<FCore.Kanal[Ord(R), ZTMCh].Str_st) and
                                 (CurEcho[R, EchoCh].Delay[j]>FCore.Kanal[Ord(R), ZTMCh].Str_en) then
                                  Flag:=false;
                            if TrueCH = EchoCh then
                              if (CurEcho[R, EchoCh].Delay[j]>=FCore.Kanal[Ord(R), ZTMCh].Str_st) and
                               (CurEcho[R, EchoCh].Delay[j]<=FCore.Kanal[Ord(R), ZTMCh].Str_en) then
                                Flag:=false;

{$IFnDEF VMT_US}
                            if (Ch = 1) or (Ch = 11) then
                              begin
                                DelayShift:= 0;
                                if HasSecondBottom then
                                begin
{$IFDEF EGO_USW}
                                  if (Ch = 1) then
                                     Flag:= Flag and (Config.SelectChan0 <> scWPB);
                                  if (Ch = 11) then
                                     Flag:= Flag and (Config.SelectChan0 <> scWPA);
{$ELSE}
                                  if (Ch = 1) then
                                    Flag:= Flag and Config.SecondaryBottomOn;
{$ENDIF}
                                  if Flag = true then
                                  begin
                                    if (ch = 1) then
                                      DelayShift:= Config.BottomTimeShift;
                                    if (ch = 11) then
                                      DelayShift:= -Config.BottomTimeShift;
                                  end;
                                end;
                              end;
{$ENDIF}   // not VMT_US

                            if FCore.Mode = rHand then Flag:= true;

{$IFnDEF VMT_US}
                          end;
{$ELSE}
                          end
                              else if (ch=2) or (ch=3) then
                                   begin
                                       if ch=2 then DelayShift:= Config.BottomTimeShift
                                          else DelayShift:= -Config.BottomTimeShift;
                                   end;
{$ENDIF}

{$IFDEF VMT_US}
                        if (Ch = 1) then
{$ELSE}
                        if (Ch = 1) or (Ch = 11) then
{$ENDIF}
                        begin
                          if  (CurEcho[R, Ch].Delay[j] > ChannelsParams[R][Ch].EndStr)  and
                            (CurEcho[R, Ch].Delay[j] < ChannelsParams[R][Ch-1].EndStr) then
                            ColorNum := 0
                          else
                              ColorNum := 1;
                        end;


{$IFDEF RKS_U}
                    ColorNum := 1;
{$ENDIF}

                    if Flag then
                      if CurEcho[R, Ch].Ampl[j]>=FThresholdAmpl then
                         FLines[i].AddSignal(Pos, ColorNum, CurEcho[R, Ch].Delay[j], CurEcho[R, Ch].Ampl[j], DelayShift );
                      end;
                  end;
              end;
          end;

     end;

  Result:=true;
end;

procedure TBScan.Refresh(Coord: Integer; Force, Full: Boolean);
var
  MaxCoord, Pos, i, o, d, WidthDis, StDis, StBM, EdBM, BM, d2: Integer;
  ZDN: Boolean;
  DisCoordInExternFile: Integer;
  CurRealMRFCoord: AviconTypes.TMRFCrd;
  CurRealCaCoord: AviconTypes.TCaCrd;
  DevID: TAviconID;
  AllowBackZone: Boolean;

  procedure DrawSample(CurCoord, MaxCoord, DC: Integer);
  var
    i, d1: Integer;
  begin
    if FCore.WaitingForBUM then
      Exit;
    FDc:=MaxCoord - FCurDisCoord;
    if (FCurDisCoord = MaxCoord) and (not Force) then Exit;
    if FLinesCount<=0 then Exit;
    if FDc>0 then
      begin
        FPainterCoord:=Round(((Config.PaintPosition*1000)/(Config.DPStep))/FScale);
        o:=0;
        d:=0;
        if FM2<>0 then
          begin
            o:=FDc mod FM2;
            d:=FDc div FM2;
          end;
        d:=d*FM2;
        if FCurSysCoord > FCore.Registrator.DisToSysCoord(FCurDisCoord+d) then
          FBackZone:=true
        else if FCurSysCoord < FCore.Registrator.DisToSysCoord(FCurDisCoord+d) then
          FBackZone:=false;
        FCurDisCoord:=FCurDisCoord+d;
        FCurSysCoord:=FCore.Registrator.DisToSysCoord(FCurDisCoord);
        if FM2<>0 then d1:= d div FM2 else d1:=0;
        for i:=0 to Length(FLines)-1 do
             FLines[i].MoveImage(d1, FBackZone);

        //// LOADDATA
        FCore.Registrator.LoadData(1, FCurDisCoord-d, FCurDisCoord, 0, ProcessSingnal);

        // ��� ��� "�������" ���� ������������
        if FCore.AdditionalFileToShow then
          begin
            if Config.UnitsSystem = usRus then
              begin
                CurRealMRFCoord:= FCore.Registrator.DisToMRFCrd(FCurDisCoord);
                FCore.FExternFileData.MRFCrdToDisCrd(CurRealMRFCoord, DisCoordInExternFile);
                FCore.FExternFileData.LoadData(DisCoordInExternFile-d, DisCoordInExternFile, 0, ProcessSingnalExtern);
              end
            else
              begin
                CurRealCaCoord:= FCore.Registrator.DisToCaCrd(FCurDisCoord);
                FCore.FExternFileData.CaCrdToDis(CurRealCaCoord, DisCoordInExternFile);
                FCore.FExternFileData.LoadData(DisCoordInExternFile-d, DisCoordInExternFile, 0, ProcessSingnalExtern);
              end;
          end;
        EventRoutine;
        if Assigned(FCoordLine) then
          begin
            FCoordLine.FCurCoord:=FCurDisCoord;
            FCoordLine.Repaint;
          end;

        for i:=0 to Length(FLines)-1 do
           begin
             FLines[i].PainterCoord:=FPainterCoord;
{$IFDEF HEADCHECKING}
             FLines[i].RefreshPlot(FM2, FCurDisCoord);
{$ENDIF}
             FLines[i].Repaint;
           end;
      end;
  end;

begin

 for i:= 0 to 1 do
 if FCore.LastDonnEnvelope[i].New then
   AddPlotPoint(i, 0, 0, FCore.LastDonnEnvelope[i].Ampl );


  FRefreshModeFull:=Full;
  if Assigned(FCore) then
    begin
      if Assigned(FCore.Registrator) then
        begin
          DevID:= FCore.Registrator.Header.DeviceID;
          AllowBackZone:= not CompareMem(@DevID, @AviconHandScan, 7);
        end;
      if (FCore.RegistrationStatus=rsOn) or (FCore.RegistrationStatus=rsPause) then
        begin
          if FCore.UseRegistratorCoord then
            MaxCoord:=FCore.Registrator.MaxDisCoord
          else
            MaxCoord:= Coord;

          FM2:=Round(FScale);
          if FM2=0 then FM2:=1;
          if (FCurDisCoord = MaxCoord) and (not Force) then Exit;
          if FLinesCount<=0 then Exit;
              if Full then
                begin
                  WidthDis:=Round(Self.Width*FScale);
                  StDis:=MaxCoord-WidthDis;
                  FFullRefreshStart:=0;
                  if StDis<0 then
                    begin
                      FFullRefreshStart:=Round(Abs(StDis)/FScale);
                      StDis:=0;
                    end;
                  for i:=0 to Length(FLines)-1 do
                     FLines[i].Clear;

                  ZDN:=FCore.Registrator.GetBMStateFirst(1, StDis);
                  if ZDN then StBM:=StDis;
                  while FCore.Registrator.GetBMStateNext(1, BM, ZDN) do
                    begin
                      if ZDN then StBM:=BM
                      else
                        begin
                          EdBM:=BM;
                          for i:=0 to Length(FLines)-1 do
                             if AllowBackZone then
                               FLines[i].AddBackZone(Round((StBM-StDis)/FScale)+FFullRefreshStart, Round((EdBM-StDis)/FScale)+FFullRefreshStart);
                        end;
                      if BM>MaxCoord then Break;
                    end;
                  if ZDN then
                    begin
                      EdBM:=MaxCoord;
                      for i:=0 to Length(FLines)-1 do
                         if AllowBackZone then
                           FLines[i].AddBackZone(Round((StBM-StDis)/FScale)+FFullRefreshStart, Round((EdBM-StDis)/FScale)+FFullRefreshStart);
                    end;

                  //// LOADDATA
                  FCore.Registrator.LoadData(1, StDis, MaxCoord, 0, ProcessSingnal);

                  // ��� ��� "�������" ���� ������������
                  if FCore.AdditionalFileToShow then
                    begin
                      // !!!!!!!!!!!!!!
                      if Config.UnitsSystem = usRus then
                        begin
                          CurRealMRFCoord:= FCore.Registrator.DisToMRFCrd(MaxCoord);
                          FCore.FExternFileData.MRFCrdToDisCrd(CurRealMRFCoord, DisCoordInExternFile);
                          FCore.FExternFileData.LoadData(DisCoordInExternFile-abs(MaxCoord-StDis), DisCoordInExternFile, 0, ProcessSingnalExtern);
                        end
                      else
                        begin
                          CurRealCaCoord:= FCore.Registrator.DisToCaCrd(MaxCoord);
                          FCore.FExternFileData.CaCrdToDis(CurRealCaCoord, DisCoordInExternFile);
                          FCore.FExternFileData.LoadData(DisCoordInExternFile-abs(MaxCoord-StDis), DisCoordInExternFile, 0, ProcessSingnalExtern);
                        end;
                    end;

                  FCurDisCoord:=MaxCoord;
                  EventRoutine;
                  for i:=0 to Length(FLines)-1 do
                     begin
                       FLines[i].PainterCoord:=Round(((Config.PaintPosition*1000)/(Config.DPStep))/FScale);
{$IFDEF HEADCHECKING}
                       FLines[i].RefreshPlot(FM2, FCurDisCoord);
{$ENDIF}
                       FLines[i].Repaint;
                     end;
                  if Assigned(FCoordLine) then
                    begin
                      FCoordLine.FCurCoord:=FCurDisCoord;



                      FCoordLine.Repaint;
                    end;
                end
              else
                DrawSample(0, MaxCoord, 0);
        end;
    end
  else
    begin
      if Assigned(FCoordLine) then
        begin
          FCoordLine.FCurCoord:=Coord + 1;
          FCoordLine.Repaint;

          if FM2<>0 then
            d2:=Coord div FM2 else d2:=0;
          Pos:=Round(d2);
          FLines[0].AddSignal(Pos, 1, 100, 8, 0);
          FLines[0].Repaint;
        end;
    end;
end;

procedure TBScan.Resize;
var
  i: integer;
begin
  inherited;
  Self.Visible:=true;
  FLineHeight:=Round((Height-FDevLineWidth-FCoordLineHeight)/FLinesCount);
  for i:=0 to FLinesCount-1 do
     begin
       FLines[i].Height:= FLineHeight;
       FLines[i].ReBuild;
       if FLines[i].Align = alTop then
         FLines[i].Top:= FLineHeight*(i)-10
       else
         begin
           FLines[i].Top:= Self.Height;
         end;
     end;
end;

procedure TBScan.SetAmpByColor(Value: Boolean);
var
  i: Integer;
begin
  if Length(FLines) > 0 then
    for i:=0 to High(FLines) do
       FLines[i].AmpByColor:=Value;
end;

procedure TBScan.SetAmpBySize(Value: Boolean);
var
  i: Integer;
begin
  if Length(FLines) > 0 then
    for i:=0 to High(FLines) do
       FLines[i].AmplBySize:=Value;
end;

procedure TBScan.SetCoordUnits(Value: TCoordUnits);
begin
  if Assigned(FCoordLine) then FCoordLine.CoordUnits:=Value;
end;

procedure TBScan.SetFontName(Value: string);
begin

end;

procedure TBScan.SetLine(Index: Integer; Value: TBScanLine);
begin
  FLines[Index]:=Value;
end;

{ TCoordLine }

constructor TCoordLine.Create(AOwner: TComponent);
begin
  inherited;
  ShowIcons:= true;
  OnCoordChangeByMouse:= nil;
  CoordUnits:=cuWay;
  FScale:=1;
  FIcons:= TCoordLineIcons.Create( AOwner );
end;

destructor TCoordLine.Destroy;
begin
  FIcons.Free;
  OnCoordChangeByMouse:= nil;
  inherited;
end;

function TCoordLine.DisToPix(DisCoord: Integer): Integer;
begin
  Result:=Round((DisCoord*Width)/FWidthDis);
end;

procedure TCoordLine.DrawBackGround;
var
  D1, D2, D3: Integer;
  Wreal: Single;
begin
  inherited;
  if (not Assigned(FColorTable)) or (not Assigned(FLanguageTable)) then Exit;
  FKmSt:=Language.Caption['��'];
  FPkSt:=Language.Caption['��'];
  FMSt:=Language.Caption['�'];
  FSecSt:=Language.Caption['�'];

  FBackGrBmp.Canvas.Brush.Color:=FColorTable.Color['CoordLine_BackGr'];
  FBmp.Canvas.Brush.Color:=FColorTable.Color['CoordLine_BackGr'];
  FBackGrBmp.Canvas.FillRect(Rect(0, 0, FBackGrBmp.Width, FBackGrBmp.Height));
  FFontColor:=FColorTable.Color['CoordLine_Text'];
  FBmp.Canvas.Font.Color:=FFontColor;
  FBmp.Canvas.Font.Style:=[fsBold];
  FBorderColor:=FColorTable.Color['CoordLine_Border'];
  FBackGrBmp.Canvas.Pen.Color:=FBorderColor;
  FBackGrBmp.Canvas.Brush.Color:=FBorderColor;
  FBackGrBmp.Canvas.Pen.Width:=FBorderWidth;
  D1:=0;
  D2:=1;
  D3:=0;
  if FBorderWidth>1 then
    begin
      D1:=FBorderWidth div 2;
      D2:=D1;
      if FBorderWidth mod 2 = 0 then D3:=0 else D3:=1;
    end;
  if brLeft in FBorders then
    begin
      FBackGrBmp.Canvas.MoveTo(D1, 0);
      FBackGrBmp.Canvas.LineTo(D1, Height);
    end;
  if brRight in FBorders then
    begin
      FBackGrBmp.Canvas.MoveTo(Width-D2-D3, 0);
      FBackGrBmp.Canvas.LineTo(Width-D2-D3, Height);
    end;
  if brTop in FBorders then
    begin
      FBackGrBmp.Canvas.MoveTo(D1, D1);
      FBackGrBmp.Canvas.LineTo(Width, D1);
    end;
  if brBottom in FBorders then
    begin
      FBackGrBmp.Canvas.MoveTo(0, Height-D2-D3);
      FBackGrBmp.Canvas.LineTo(Width, Height-D2-D3);
    end;

  //
  if (FScale>0) and (FScale<3) then Wreal:=0.1
  else if (FScale>=3) and (FScale<6) then Wreal:=0.2
  else if (FScale>=6) and (FScale<8) then Wreal:=0.5
  else Wreal:=1;
  FScanStep:=170;
  if Assigned(FCore) then
    if FCore.RegistrationStatus = rsOn then
      FScanStep:=FCore.Registrator.Header.ScanStep;

  if FScanStep <= 0 then
    FScanStep:= 200;
  FAxisSmallLineW:=Round(Width/(FScale/Wreal));
  FAxisSmallLineWDis:=Round((Wreal*100000)/FScanStep);
  FWidthDis:=Round((Scale*1000)/(FScanStep/100))
end;

procedure TCoordLine.DrawForeGround;
var
  s1, s2: string;
  RealMRFCoord: TMRFCrd;
  RealCaCoord: TCaCrd;

  Metr, X, Xdis, w, dX, step, AxisStepMM, A, B, C, DisW, i, IconX, SysC: Integer;
  BJSt, BJEd: Integer;
  BrushColor: TColor;

  procedure DrawRect(St, Ed: Integer);
  begin
    St:=Self.Width-St;
    if St<0 then St:=0;
    Ed:=Self.Width-Ed;
    if Ed<0 then Ed:=0;
    FBmp.Canvas.FillRect(Rect(St, 1, Ed, Height-1));
  end;

begin
  inherited;
  FM2:=Round(FScale);
  if FM2=0 then FM2:=1;
  FBmp.Canvas.Font.Name:=DefaultFontName;
  if Assigned(FCore) then

   if Assigned(FCore.Registrator) then
      begin
        if CoordUnits = cuWay then
          begin
            SysC:= FCore.Registrator.CurSaveSysCoord;
            Step:=FCore.Registrator.Header.ScanStep;
            if Config.UnitsSystem = usRus then
              begin
                // ���� �������� ���������
                {$IFDEF OldEngine}
                Metr:=Round(RealCoord.MM/1000);
                s1:=Format('%d %s %d %s %d %s', [RealCoord.Km, FKmSt, RealCoord.Pk, FPkSt, Metr, FMSt]);
                {$ELSE}
                RealMRFCoord:= FCore.Registrator.DisToMRFCrd(FCurCoord);
                s1:= AviconTypes.MRFCrdToStr(RealMRFCoord);
                {$ENDIF}
                s2:=Format('%4.0f %s',[FScale*Config.DPStep, FMSt]);
              end
            else
              begin
                // ��������� �������              -
                RealCaCoord:= FCore.Registrator.DisToCaCrd(FCurCoord);
//{$IFDEF USK004R}
{$IFDEF TURKISH_COORD_WITH_DOT}
                s1:= AviconTypes.CaCrdToStr(TCoordSys(Ord(usMetricB)), RealCaCoord);
{$ELSE}
                s1:= AviconTypes.CaCrdToStr(TCoordSys(Ord(Config.UnitsSystem)), RealCaCoord);
{$ENDIF}
                if Config.UnitsSystem = usMetricTurkish then
                  s2:=Format('%4.0f %s',[FScale*Config.DPStep, FMSt])
                else
                  s2:=Format('%s "',[TUnitsConverter.ConvertMMToStr(FScale*Config.DPStep*1000)]);
              end;
          end
        else if CoordUnits = cuSec then
          begin
            Step:=FCore.Registrator.Header.ScanStep;
            s1:=Format('%d %s', [FCore.Registrator.MaxDisCoord div 200, FSecSt]);
            s2:=Format('%4.0f %s',[FScale, FSecSt]);
          end;
      end
  else
    begin
      if CoordUnits = cuWay then
        begin
          Step:=Round(Config.DPStep*100);
          s1:=Format('1 %s 1 %s %d %s', [FKmSt, FPkSt, FCurCoord, FMSt]);
          if Config.UnitsSystem in [usRus, usMetricTurkish] then
              s2:=Format('%4.0f %s',[FScale*Config.DPStep, FMSt])
            else
              s2:=Format('%s "',[TUnitsConverter.ConvertMMToStr(FScale*Config.DPStep*1000)]);
        end
      else if CoordUnits = cuSec then
        begin
          s1:=Format('%d %s', [0, FSecSt]);
          s2:=Format('%4.0f %s',[FScale, FSecSt]);
        end;
      Step:=170;

    end;

  // ���� �������� ��������� ������ ��� ������ ��
  BrushColor:=FBmp.Canvas.Brush.Color;
  FBmp.Canvas.Brush.Color:=clYellow;
  BJSt:=-1;
  BJEd:=-1;
  (*<Rud49>*)
  if Assigned(FEventList) then
    for i:=0 to FEventList.Count-1 do
       begin
         if FEventList[i].EventIcon = iJointSt then
            BJSt:=FEventList[i].Coord;
         if FEventList[i].EventIcon = iJointEd then
         begin
            BJEd:=FEventList[i].Coord;
            fullBoltJoin := false;
         end;
         if (BJSt=-1) and (BJEd>-1) then
           begin
             DrawRect(Self.Width, BJEd);
             BJEd:=-1;
           end;
         if (BJSt>-1) and (BJEd>-1) then
           begin
             DrawRect(BJSt, BJEd);
             BJSt:=-1;
             BJEd:=-1;
           end;
       end;
  if (BJSt>-1) and (BJEd=-1) then
    begin
      DrawRect(BJSt, 0);
    end;
  if (General.Core.BoltJointMode = true) and (fullBoltJoin = true) then
    DrawRect(Self.Width, 0);

  if BJSt > Self.Width then
    fullBoltJoin := true;
  (*</Rud49>*)

  FBmp.Canvas.Brush.Color:=BrushColor;

  AxisStepMM:=1000;
  if Step = 0 then Step:= 200;

  FAxisSmallLineWDis:=Round(AxisStepMM/(Step/100));

  if FAxisSmallLineWDis = 0 then ShowMessage('FAxisSmallLineWDis = 0');


  Dx:=FCurCoord mod FAxisSmallLineWDis;
  DisW:=Round(Self.Width*FScale);
  X:=FAxisSmallLineWDis;
  while X < DisW do
  begin
    if FScale = 0 then ShowMessage('FScale = 0');
    FBmp.Canvas.MoveTo(Round((X-Dx)/FScale), 0);
    FBmp.Canvas.LineTo(Round((X-Dx)/FScale), Height);
    X:= X + FAxisSmallLineWDis;
  end;

  // ��������� ������
  if Assigned(FEventList) then
    for i:=0 to FEventList.Count-1 do
       begin
              // �������� �������� ��� ������ � �������������
              if ShowIcons then
                begin
                  IconX:=Self.Width - FEventList[i].Coord;
                  FIcons.Draw( FBmp.Canvas, IconX, FEventList[i].EventIcon );
                end;
       end;

  {$IFDEF PAINTER}
//  IconX:=Self.Width - Round(((Config.PaintPosition*1000)/(Step/100))/FScale);
//Ak 18-06-2015  FIcons.Draw( FBmp.Canvas, IconX, iPainter );
  {$ENDIF}



  FBmp.Canvas.Brush.Style:=bsClear;
  FBmp.Canvas.TextOut(10, 5, s1);
  X:=Width-FBmp.Canvas.TextWidth(s2)-5;
  FBmp.Canvas.TextOut(X, 5, s2);
  FBmp.Canvas.Brush.Style:=bsSolid;

(*
{$IFDEF EGO_USW}
if General.CurLogicalMode=bmSearch then begin
  s1:=Language.Caption['Left Side ArrowUp'];
  s2:=Language.Caption['Right Side ArrowDn'];
  FBmp.Canvas.Brush.Style:=bsClear;
  i:=(Self.Height-8) div 2;
  A:=FBmp.Canvas.Font.Height;
  FBmp.Canvas.Font.Height:=i;
  FBmp.Canvas.Font.Style:=[];
  X:=X-FBmp.Canvas.TextWidth(s2)-5;
  FBmp.Canvas.TextOut(X, 2, s1);
  FBmp.Canvas.TextOut(X, Self.Height-2-i, s2);
  FBmp.Canvas.Brush.Style:=bsSolid;
  FBmp.Canvas.Font.Height:=A;
  FBmp.Canvas.Font.Style:=[fsBold];
end;
{$ENDIF}
*)
end;

procedure TCoordLine.Resize;
begin
  inherited;
  FFontHeight:=Self.Height-10;
  FBmp.Canvas.Font.Height:=FFontHeight;
  ReBuild;
end;

procedure TBScan.SetPointSize(Value: TBViewPointSize);
var
  i: Integer;
begin
  for i:=0 to High(FLines) do
     FLines[i].PointSize:=Value;
end;

procedure TBScan.SetScale(Value: Single);
begin
  FScale:=Value;
  if Assigned(FCoordLine) then
    begin
      FCoordLine.Scale:=FScale;
      FCoordLine.ReBuild;
    end;
  Self.Refresh(0, true, true);
end;

function TCoordLine.GetStartCoord: Integer;
var
  W: Integer;
begin
  W:=Round(Self.Width*FScale);
  if FCurCoord > W then
    result:=FCurCoord-W
  else
    result:=0;
end;

procedure TCoordLine.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  if Classes.ssLeft in Shift then
    begin
      FDX:= FMX - X;
      Self.CurCoord:= Round(Self.CurCoord + FDX);
      if Self.CurCoord > FCore.Registrator.MaxDisCoord then
        Self.CurCoord:= FCore.Registrator.MaxDisCoord;
      if Self.CurCoord < 0 then Self.CurCoord:= 0;

      Self.Repaint;
      if Assigned(OnCoordChangeByMouse) then
        OnCoordChangeByMouse(Self.CurCoord);
    end;
  FDX:= 0;
  FMX:=X;
  FMY:=Y;
end;

{ TChanelButton }

function TChanelButton.CountWidth(Height: Integer): Integer;
begin
{$IFDEF THIN_CHANBUT}
  if FShowHelp then result:=Round(Height*ThinButPerCent + (Height*HelpWidthPerCent/100))
  else result:=Round(Height*ThinButPerCent);//round(Height*0.6);
{$ELSE}
  if FShowHelp then result:=Round(Height + (Height*HelpWidthPerCent/100))
  else result:=Height;
{$ENDIF}
end;

constructor TChanelButton.Create(AOwner: TComponent;  Kanal1, Kanal2, HKanal: TKanal; CT: TColorTable; LT: TLanguageTable);
begin
  inherited Create(AOwner);
  FLocked:=false;
  FKanalType:=rkContinuous;
  FASDTimeToWait:=ADSTimeBeforeOff;
  FAdjusted:=false;
  FKind:=bkLeft;
  FKanal1:=Kanal1;
  FKanal2:=Kanal2;
  FHKanal:=HKanal;
  FCurKanal:=nil;
  FArrowDir:=dForward;
  FEnabled:=true;
  FCT:=CT;
  FLT:=LT;
  Kanal2:=nil;

  Self.BevelOuter:=bvNone;
  Self.BorderStyle:=bsSingle;
  Self.Visible:=false;
  Self.ParentColor:=false;
  Self.Color:=clLime;
  FShowHelp:=true;
   FASD:= False;

  FChanAKKPan:=TPanel.Create(Self);
  FChanAKKPan.Parent:=Self;
  FChanAKKPan.ParentBackground:=false;
  FChanAKKPan.ParentColor:=false;
  FChanAKKPan.OnClick:=PanClick;
  FChanAKKPan.Align:=alRight;
  FChanAKKPan.Color:=FCT.Color['ChanBut_AKKYes'];
  FChanAKKPan.Width:=Round((Width/100)*AKKWidthPerCent);
  FChanAKKPan.BevelOuter:=bvNone;
  FChanAKKPan.Ctl3D:=false;
  FChanAKKPan.Font.Name:=DefaultFontName;
  FChanAKKPan.Visible:=false;


  FChanWorkPan:=TPanel.Create(Self);
  FChanWorkPan.Parent:=Self;
  FChanWorkPan.ParentBackground:=false;
  FChanWorkPan.ParentColor:=false;
  FChanWorkPan.OnClick:=PanClick;
  FChanWorkPan.Align:=alClient;
  FChanWorkPan.BevelOuter:=bvNone;
  FChanWorkPan.Font.Color:=FCT.Color['ChanBut_KUText'];
  FChanWorkPan.Ctl3D:=false;
  FChanWorkPan.Caption:='32';
  FChanWorkPan.Font.Name:=DefaultFontName;
  FChanWorkPan.Color:=clRed;

  FChanHelpPan:=TPanel.Create(Self);
  FChanHelpPan.Parent:=Self;
  FChanHelpPan.ParentBackground:=false;
  FChanHelpPan.ParentColor:=false;
  FChanHelpPan.Width:=Round((Self.Width/100)*HelpWidthPerCent);
  FChanHelpPan.Align:=alLeft;
  FChanHelpPan.Color:=FCT.Color['ChanBut_HelpPan'];
  FChanHelpPan.BevelOuter:=bvNone;
  FChanHelpPan.Ctl3D:=false;
  FChanHelpPan.Font.Name:=DefaultFontName;

  FAlfaPan:=TPanel.Create(FChanHelpPan);
  FAlfaPan.Parent:=FChanHelpPan;
  FAlfaPan.ParentBackground:=false;
  FAlfaPan.ParentColor:=false;
  FAlfaPan.OnClick:=PanClick;
  FAlfaPan.Align:=alTop;
  FAlfaPan.Height:=Round(FChanHelpPan.Height/3);
  FAlfaPan.Color:=FChanHelpPan.Color;
  FAlfaPan.BevelOuter:=bvNone;
  FAlfaPan.Font.Color:=FCT.Color['ChanBut_HelpText'];
  FAlfaPan.Ctl3D:=false;
  FAlfaPan.Font.Name:=DefaultFontName;
  FAlfaPan.Caption:='70';

  FMethodPan:=TPanel.Create(FChanHelpPan);
  FMethodPan.Parent:=FChanHelpPan;
  FMethodPan.ParentBackground:=false;
  FMethodPan.ParentColor:=false;
  FMethodPan.OnClick:=PanClick;
  FMethodPan.Align:=alTop;
  FMethodPan.Height:=Round(FChanHelpPan.Height/3);
  FMethodPan.Color:=FChanHelpPan.Color;
  FMethodPan.BevelOuter:=bvNone;
  FMethodPan.Font.Color:=FCT.Color['ChanBut_HelpText'];
  FMethodPan.Ctl3D:=false;
  FMethodPan.Font.Name:=DefaultFontName;
  if Assigned(FKanal1) then
    if FKAnal1.Num=0 then
      FMethodPan.Caption:='���'
    else
      FMethodPan.Caption:='���';

  FChDirNumPan:=TPanel.Create(FChanHelpPan);
  FChDirNumPan.Parent:=FChanHelpPan;
  FChDirNumPan.ParentBackground:=false;
  FChDirNumPan.ParentColor:=false;
  FChDirNumPan.Align:=alTop;
  FChDirNumPan.Height:=Round(FChanHelpPan.Height/3);

  FChDirNumPan.Color:=FChanHelpPan.Color;

  FChDirNumPan.BevelOuter:=bvNone;
  FChDirNumPan.Font.Color:=clYellow;
  FChDirNumPan.Ctl3D:=false;
  FChDirNumPan.Font.Name:=DefaultFontName;

  FBmp:=TBitMap.Create;

  FDirPB:=TPaintBox.Create(FChDirNumPan);
  FDirPB.Parent:=FChDirNumPan;
  FDirPB.OnClick:=PanClick;
  FDirPB.Align:=alClient;
  FDirPB.Color:=FChDirNumPan.Color;
  FDirPB.OnPaint:=OnPaint;

  SetKanal(FKanal1);
end;

destructor TChanelButton.Destroy;
begin
  //
  inherited;
end;
procedure TChanelButton.DrawArrow;
var
  X0, Y0, dx, dy, adx1, adx2, ady1, ady2: Integer;
  P: array[0..2] of TPoint;
begin
  FBmp.Width:=FDirPB.Width;
  FBmp.Height:=FDirPB.Height;
  FBmp.Canvas.Brush.Color:=FChDirNumPan.Color;
  FBmp.Canvas.FillRect(Rect(0,0, FBmp.Width, FBmp.Height));
  FBmp.Canvas.Pen.Color:=FDirPB.Font.Color;
  X0:=FBmp.Width div 2;
  Y0:=FBmp.Height div 2;
  FBmp.Canvas.Pen.Color:=clBlack;
  FBmp.Canvas.Brush.Color:=clBlack;

  FBmp.Canvas.Pen.Width:=5;
  dy:=Round(Self.Height*20/200);

  case FArrowDir of
  dForward: begin
               dx:=Round(Self.Height*15/200);
               adx1:=-9;
               ady1:=-5;
               adx2:=-2;
               ady2:=-10;
             end;
  dBack: begin
               dx:=-Round(Self.Height*15/200);
               adx1:=9;
               ady1:=-5;
               adx2:=2;
               ady2:=-10;
             end;
  dNormal: begin
               dx:=0;
               adx1:=5;
               ady1:=-10;
               adx2:=-5;
               ady2:=-10;
             end;
  end;


  if FArrowDir = dNone then Exit;
  FBmp.Canvas.MoveTo(X0-dx, Y0-dy);
  FBmp.Canvas.LineTo(X0+dx, Y0+dy);


  P[0].X:=X0+dx;
  P[0].Y:=Y0+dy;

  P[1].X:=X0+dx+adx1;
  P[1].Y:=Y0+dy+ady1;

  P[2].X:=X0+dx+adx2;
  P[2].Y:=Y0+dy+ady2;

  FBmp.Canvas.Polygon(P);

end;

function TChanelButton.GetEnable: Boolean;
begin
  Result:=FEnabled;
end;

function TChanelButton.GetKanal: TKanal;
begin
  if FKanalType =  rkHand then
    begin
      result:=FHKanal;
      Exit;
    end;
  if Assigned(FCurKanal) then result:=FCurKanal
  else if Assigned(FKanal1) then result:=FKanal1
  else if Assigned(FKanal2) then result:=FKanal2
  else result:=nil;
end;

procedure TChanelButton.Lock;
begin
  Enabled:=false;
  FLocked:=true;
end;

procedure TChanelButton.OnPaint(Sender: TObject);
begin
  DrawArrow;
  if Assigned(FDirPB) and  Assigned(FBmp) then
    FDirPB.Canvas.Draw(0,0, FBmp);
end;

procedure TChanelButton.PanClick(Sender: TObject);
var
  FScaleType: TScaleType;
begin
  if FEnabled then
    begin
      if FKanalType = rkHand then
        begin
          FCurKanal:=FHKanal;
          SetKanal(FCurKanal);
          (*<Rud23>*)
          if assigned(GAScanFrame) then
            GAScanFrame.SetMMScaleType;
          (*</Rud23>*)
          if Assigned(OnClick) then
            OnClick(Self);
          Exit;
        end;

      if Assigned(FKanal2) then
        begin
          // ����� �� ����� ������
          if Assigned(FCurKanal) then
            begin
              if (FCurKanal.Num=FKanal1.Num) then FCurKanal:=FKanal2 else FCurKanal:=FKanal1;
            end
          else // ����� �� ������� ������
            if (FKanal2.ASD) and (not FKanal1.ASD) then FCurKanal:=FKanal2 else FCurKanal:=FKanal1;
      end
      else
        FCurKanal:=FKanal1;
      SetKanal(FCurKanal);
      if Assigned(OnClick) then
        OnClick(Self);
    end;
end;

procedure TChanelButton.RefreshParams;
var
  ASD: Boolean;
  gradus: string;
begin
{$IFDEF THIN_CHANBUT}
  gradus:='�';
{$ELSE}
  gradus:='�';
{$ENDIF}
  if Assigned(FKanal1) then
    begin
      if not FSelected then
        begin
          if FMode in [bmSearch, bmEstimate, bmMnemo] then
            begin
              if Assigned(FKanal2) then ASD:=FKanal1.ASD or FKanal2.ASD
              else ASD:=FKanal1.ASD;
              SetASD(ASD);
            end;
        end;

      // ��������� ��� ������� ����� �� ������������ �� ������� ����� ������ �������
      if FMode in [bmSearch, bmEstimate, bmMnemo, bmNastr] then
        if Assigned(FCurKanal) then
          if ProgramProfile.AlfaInstedKu then
            begin
              FChanWorkPan.Caption:=IntToStr(FCurKanal.Takt.Alfa)+gradus;//'�';
              FAlfaPan.Caption:=IntToStr(FCurKanal.Ky);
            end
          else
            begin
              FChanWorkPan.Caption:=IntToStr(FCurKanal.Ky)
            end
        else
          if ProgramProfile.AlfaInstedKu then
            begin
              if assigned(FKanal1.Takt) then
                FChanWorkPan.Caption:=IntToStr(FKanal1.Takt.Alfa)+gradus;//'�';
              FAlfaPan.Caption:=IntToStr(FKanal1.Ky);
            end
          else
            begin
              FChanWorkPan.Caption:=IntToStr(FKanal1.Ky);
            end;
      if FMode in [bmHand, bmNastrHand] then
        if Assigned(FHKanal) then
        if ProgramProfile.AlfaInstedKu then
          begin
            FChanWorkPan.Caption:=IntToStr(FHKanal.Takt.Alfa)+gradus;//'�';
            FAlfaPan.Caption:=IntToStr(FHKanal.Ky);
          end
        else
          begin
            FChanWorkPan.Caption:=IntToStr(FHKanal.Ky);
          end;
    end;
end;


procedure TChanelButton.Resize;
begin

  inherited;
  if FShowHelp then
    begin
{$IFDEF THIN_CHANBUT}
      Width:=Round(Height*ThinButPerCent + (Height*HelpWidthPerCent/100));
{$ELSE}
      Width:=Round(Height + (Height*HelpWidthPerCent/100));
{$ENDIF}
      FChanHelpPan.Width:=Round((Self.Height/100)*HelpWidthPerCent);
      FChDirNumPan.Height:=Round(FChanHelpPan.Height/3);
      FChanHelpPan.Visible:=true;
    end
  else
    begin
{$IFDEF THIN_CHANBUT}
      Width:=Round(Height*ThinButPerCent);
{$ELSE}
      Width:=Height;
{$ENDIF}
      FChanHelpPan.Visible:=false;
      FChanHelpPan.Width:=0;
    end;
  FChanAKKPan.Width:=Round((Height/100)*AKKWidthPerCent);

  if ProgramProfile.AlfaInstedKu then
    FChanWorkPan.Font.Height:=FChanWorkPan.Width-20 //!!! -0
  else
    FChanWorkPan.Font.Height:=FChanWorkPan.Width-10;

  FAlfaPan.Height:=Round(FChanHelpPan.Height/3);

  if ProgramProfile.AlfaInstedKu then
    FAlfaPan.Font.Height:=FAlfaPan.Height-10
  else
    FAlfaPan.Font.Height:=FAlfaPan.Height-2;

  FMethodPan.Height:=Round(FChanHelpPan.Height/3);
  FMethodPan.Font.Height:=FAlfaPan.Height-2;

  FChDirNumPan.Height:=Round(FChanHelpPan.Height/3);
  FChDirNumPan.Font.Height:=FAlfaPan.Height-2;

  DrawArrow;
end;

procedure TChanelButton.ResizePan(Sender: TObject);
begin
  //if (Assigned(FDirPan)) and (Assigned(FDirPanel)) then
  if Assigned(FDirPB) then
    begin
      //FDirPan.Width:=FDirPanel.Width;
      //FDirPan.Height:=FDirPanel.Height;
      //FDirPan.Left:=0;
      //FDirPan.Top:=0;
    end;
end;

procedure TChanelButton.SetAdjusted(Value: Boolean);
begin
  if FAdjusted<>Value then
    begin
      FAdjusted:=Value;
      Update;
    end;
end;

procedure TChanelButton.SetAK(Value: Boolean);
begin
  if Value then FChanAKKPan.Color:=FCT.Color['ChanBut_AKKYes']
  else FChanAKKPan.Color:=FCT.Color['ChanBut_AKKNo'];
end;

procedure TChanelButton.SetASD(Value: Boolean);
begin
  if ( FASD <> Value ) and ( Value or ( (GetTickCount-FASDLastChangeTime)>=FASDTimeToWait ) ) then
  begin
    FASD:= Value;
    UpDate;
  end;
  if Value then FASDLastChangeTime:= GetTickCount;
end;

procedure TChanelButton.SetEnabled(Value: Boolean);
begin
  if FLocked then Exit;
  if Value<>FEnabled then
    begin
      FEnabled:=Value;
      if not FEnabled then
        begin
          FDirPB.Visible:=false;
          FAlfaPan.Visible:=false;
          FMethodPan.Visible:=false;
          FChDirNumPan.Visible:=false;
        end
      else
        begin
          FDirPB.Visible:=true;
          FAlfaPan.Visible:=true;
          FMethodPan.Visible:=true;
          FChDirNumPan.Visible:=true;
        end;
      UpDate;
    end;
end;

procedure TChanelButton.SetKanal(Value: TKanal);
var gradus: string;
begin
{$IFDEF THIN_CHANBUT}
  gradus:='�';
{$ELSE}
  gradus:='�';
{$ENDIF}
  if Assigned(Value) then
    begin
      if ProgramProfile.AlfaInstedKu then
      begin
        if assigned(Value.Takt) then
          FChanWorkPan.Caption:=IntToStr(Value.Takt.Alfa)+gradus;//'�'
        FAlfaPan.Caption:=IntToStr(Value.Ky)+'dB';
      end
      else
      begin
        FChanWorkPan.Caption:=IntToStr(Value.Ky);
        if assigned(Value.Takt) then
          FAlfaPan.Caption:=IntToStr(Value.Takt.Alfa)+gradus;//'�'
      end;

      FMethodPan.Caption:= FLT[Value.Method];
      FArrowDir:=Value.WaveDirection;
      DrawArrow;
    end;
end;

procedure TChanelButton.SetKanalType(Value: TKanType);
begin
  if Value = rkHand then
    begin
    if FHKanal<>nil then
      begin
        FKanalType:=Value;
        SetKanal(Kanal);
      end;
    end
  else
    begin
      FKanalType:=Value;
      SetKanal(Kanal);
    end;
end;

procedure TChanelButton.SetKindness(Value: TChButKind);
begin
  if FKind<>Value then
    begin
       FKind:=Value;
      case FKind of
         bkLeft: begin
                   FChanWorkPan.Align:=alNone;
                   FChanHelpPan.Align:=alLeft;
                   FChanAKKPan.Align:=alRight;
                   FChanWorkPan.Align:=alClient;
                 end;
         bkRight: begin
                    FChanWorkPan.Align:=alNone;
                    FChanHelpPan.Align:=alRight;
                    FChanAKKPan.Align:=alLeft;
                    FChanWorkPan.Align:=alClient;
                  end;
      end;
    end;
end;


procedure TChanelButton.SetMode(Value: TBScanWinMode);
begin
  if FMode<>Value then
    begin
      FMode:=Value;
      FAdjusted:=false;
      FASD:=false;
      SetKanal(FCurKanal);
      OnPaint(nil);
      UpDate;
    end;
end;

procedure TChanelButton.SetSelected(Value: Boolean);
begin
  if FSelected<>Value then
    begin
      FSelected:=Value;
      if not FSelected then FCurKanal:=nil;
      UpDate;
    end;
end;

procedure TChanelButton.SetShowHelp(Value: Boolean);
begin
  FShowHelp:=Value;
  if FShowHelp then
    begin
      Width:=Round(Height + (Height*HelpWidthPerCent/100));
      FChanHelpPan.Width:=Round((Self.Height/100)*HelpWidthPerCent);
      FChanHelpPan.Visible:=true;
    end
  else
    begin
      Width:=Height;
      FChanHelpPan.Visible:=false;
      FChanHelpPan.Width:=0;
    end;
end;

procedure TBScanLine.SetKanal(Index: integer; Value: TKanal);
begin

end;

procedure TBScan.SetScanDir(Value: TZondPosition);
var
  i: Integer;
begin
  for i:=0 to High(FLines) do
     FLines[i].ZondDirection:=Value;
end;

procedure TBScan.SetShowIcons(Value: Boolean);
begin
  if (Assigned(FCoordLine)) then
    FCoordLine.ShowIcons:= Value;
end;

procedure TBScan.UpDateStrob;
var
  i: Integer;
begin
  for i:=0 to Length(FLines)-1 do
    FLines[i].ReBuild;
end;

procedure TChanelButton.UnLock;
begin
  FLocked:=false;
  Enabled:=true;
end;

procedure TChanelButton.UpDate;
begin
  if not FEnabled then
    begin
      FChanWorkPan.Color:=FCT.Color['ChanBut_DisabledText'];
      FChanWorkPan.Font.Color:=FCT.Color['ChanBut_Disabled'];
      Exit;
    end;
  if FMode in [bmSearch, bmEstimate, bmHand, bmMnemo] then
    if FSelected then
      begin
        FChanWorkPan.Color:=FCT.Color['ChanBut_KUText'];
        FChanWorkPan.Font.Color:=FCT.Color['ChanBut_AsdOff'];
      end

      else if FASD then
        begin
          FChanWorkPan.Color:=FCT.Color['ChanBut_AsdOn'];
          FChanWorkPan.Font.Color:=FCT.Color['ChanBut_KUText'];
        end

      else
        begin
          FChanWorkPan.Color:=FCT.Color['ChanBut_AsdOff'];
          FChanWorkPan.Font.Color:=FCT.Color['ChanBut_KUText'];
        end
  else
    begin
      if FSelected then
      begin
        FChanWorkPan.Color:=FCT.Color['ChanBut_KUText'];
        FChanWorkPan.Font.Color:=FCT.Color['ChanBut_AsdOff'];
      end

      else if FAdjusted then
        begin
          FChanWorkPan.Color:=FCT.Color['ChanBut_AKKYes'];
          FChanWorkPan.Font.Color:=FCT.Color['ChanBut_KUText'];
        end

      else begin
        FChanWorkPan.Color:=FCT.Color['AdjButFontColorError'];
        FChanWorkPan.Font.Color:=FCT.Color['ChanBut_KUText'];
      end;

    end;
end;

{ TCoordLineIcons }

constructor TCoordLineIcons.Create(AOwner: TComponent);

  procedure Cr( fn: string; Y: Integer );
  var
    Ic: PCoordIcon;
  begin
    New( Ic );
    Ic.Icon:= TImage.Create( AOwner );
    Ic.Icon.Picture.LoadFromFile( GetFullPath('pic\marks') + fn + '.wmf' );
    Ic.Y:= Y;
    Self.Add( Ic );
  end;

begin
  inherited Create;
  // ����� ������������������.
  //  TIconID = ( iKM, iPK, iPainter, iDefect, iJointSt, iJointEd, iSwitch, iTxt );

  Cr( 'km', 2 );
  Cr( 'pk', 2 );
  Cr( 'painter', 13 );
  Cr( 'defect', 2 );
  Cr( 'joint', 1 );
  Cr( 'joint', 1 );
  Cr( 'switch', 1 );
  Cr( 'txt', 2 );
end;

procedure TCoordLineIcons.Draw(Canvas: TCanvas; X: Integer; IconID: TIconID );
begin
   with PCoordIcon( Items[ Ord( IconID) ] )^ do
    Canvas.Draw( X - Icon.Picture.Width div 2, Y, Icon.Picture.Graphic);
end;

{ TCoordEventList }

procedure TCoordEventList.AddEvent(_Coord: Integer; rail: byte; _IconID: TIconID);
var
  Event: PCoordEvent;
begin
  Event:=nil;
  GetMem(Event, SizeOf(TCoordEvent));
  if Assigned(Event) then
    begin
      Event^.Coord:=_Coord;
      Event^.Rail:= rail;
      Event^.EventIcon:=_IconID;
    end;
  FEventList.Add(Event);
end;

procedure TCoordEventList.Clear;
var
  i: Integer;
begin
  if Assigned(FEventList) then
    begin
      for i:=0 to FEventList.Count - 1  do
         FreeMem(PCoordEvent(FEventList.Items[i]));
      FEventList.Clear;
    end;
end;

constructor TCoordEventList.Create;
begin
  FEventList:=TList.Create;
  Self.Clear;
end;

destructor TCoordEventList.Destroy;
var
  i: Integer;
begin
  if Assigned(FEventList) then
    begin
      for i:=0 to FEventList.Count - 1  do
         FreeMem(PCoordEvent(FEventList.Items[i]));
      FEventList.Free;
      FEventList:=nil;
    end;
  inherited;
end;

function TCoordEventList.GetEvent(Index: Integer): TCoordEvent;
begin
  result:=PCoordEvent(FEventList.Items[Index])^;
end;

function TCoordEventList.GetEventsCount: Integer;
begin
  if Assigned(FEventList) then result:=FEventList.Count;
end;

procedure TBScanLine.SetPointSize(Value: TBViewPointSize);
begin
  case Value of
    psSmall: FPointSize:=2;
    psMedium: FPointSize:=3;
    psLarge: FPointSize:=4;
  else
     FPointSize:=2;
  end
end;

procedure TBScanLine.SetScanDir(Value: TZondPosition);
var
  Val1: TBScanDirection;
begin
  if Value = zBelow then
    Val1:=bdUp
  else
    Val1:=bdDown;
  if Val1<>FScanDir then
    begin
      FScanDir:=Val1;
      CountDelay;
      Self.DrawBackGround;
    end;
end;

{ TPlot }

procedure TPlot.AddPoint(_Coord, _Y: Integer);
var
  PPoint: PPlotPoint;
begin
  GetMem(PPoint, SizeOf(TPlotPoint));
  PPoint^.Coord:=_Coord;
  PPoint^.Y:=_Y;
  Self.Add(PPoint);
end;

procedure TPlot.Clear;
var
  PPoint: PPlotPoint;
  i: Integer;
begin
  for i:=0 to Self.Count-1 do
    begin
      PPoint:=Self.Items[i];
      FreeMem(PPoint);
      Self.Items[i]:=nil;
    end;
  inherited Clear;
end;

constructor TPlot.Create;
begin
  inherited Create;

end;

destructor TPlot.Destroy;
begin

  inherited;
end;

procedure TPlot.Filter(MaxX: Integer);
var
  i: Integer;
  PPoint: PPlotPoint;
begin
  i:=0;
  while i < Self.Count do
    begin
      PPoint:=Self.Items[i];
      if PPoint^.X > MaxX then
        Self.RemovePoint(i)
      else
        Inc(i);
    end;
end;

function TPlot.GetPoint(Index: Integer): TPlotPoint;
var
  PPoint: PPlotPoint;
begin
  PPoint:=Self.Items[Index];
  result:=PPoint^;
end;

procedure TPlot.RemovePoint(Index: Integer);
var
  PPoint: PPlotPoint;
begin
  PPoint:=Self.Items[Index];
  FreeMem(PPoint);
  Self.Delete(Index);
end;

procedure TPlot.Refresh(Scale, CurCoord: Integer);
var
  PPoint: PPlotPoint;
  i: Integer;
begin
  for i:=0 to Self.Count-1 do
    begin
      PPoint:=Self.Items[i];
      if Scale<>0 then
        PPoint.X:= (CurCoord - PPoint.Coord) div Scale;
    end;
end;

procedure TPlot.RemovePoint(Index: Pointer);
begin
  Self.Extract(Index);
  FreeMem(Index);
end;

{ TCustomBScanElement }

constructor TCustomBScanElement.Create;
begin
  FBmp:=nil;
  Delays:=nil;
  ScanLines:=nil;
  FBackBmp:=nil;
  FColorTable:=nil;
  FLanguageTable:=nil;
  FWorkRect.Left:=0;
  FWorkRect.Top:=0;
  FWorkRect.Right:=0;
  FWorkRect.Bottom:=0;
  FBackGrColor:=clWhite;
  FBorderColor:=clBlack;
end;

destructor TCustomBScanElement.Destroy;
begin
  FBmp:=nil;
  inherited;
end;

procedure TCustomBScanElement.DrawBackGroundContent;
begin
  if Assigned(FBackBmp) then
    begin
      // ������ ������� (������� ������ ����)
      FBackBmp.Canvas.Brush.Color:=FBackGrColor;
      FBackBmp.Canvas.FillRect(FWorkRect);

      // ����� �����
      FBackBmp.Canvas.Pen.Color:=FBorderColor;
      FBackBmp.Canvas.Pen.Width:=2;
      FBackBmp.Canvas.Rectangle(FWorkRect);
    end;
end;

procedure TCustomBScanElement.DrawForeGroundContent;
begin
end;

function TCustomBScanElement.GetHeight: Integer;
begin
  result:=FWorkRect.Bottom - FWorkRect.Top;
end;

function TCustomBScanElement.GetLeft: Integer;
begin
  result:=FWorkRect.Left;
end;

function TCustomBScanElement.GetTop: Integer;
begin
  result:=FWorkRect.Top;
end;

function TCustomBScanElement.GetWidth: Integer;
begin
  result:=FWorkRect.Right - FWorkRect.Left;
end;

procedure TCustomBScanElement.SetColorTable(Value: TColorTable);
begin
  FColorTable:=Value;
  if Assigned(FColorTable) then
    begin
      FBackGrColor:=FColorTable.Color['BScan_BeckGr'];
      FBorderColor:=FColorTable.Color['BScan_Border'];
    end;
end;

procedure TCustomBScanElement.SetHeight(Value: Integer);
begin
  FWorkRect.Bottom:= FWorkRect.Top + Value;
end;

procedure TCustomBScanElement.SetLanguageTable(Value: TLanguageTable);
begin
  FLanguageTable:=Value;
end;

procedure TCustomBScanElement.SetLeft(Value: Integer);
begin
  FWorkRect.Left:= Value;
end;

procedure TCustomBScanElement.SetTop(Value: Integer);
begin
  FWorkRect.Top:= Value;
end;

procedure TCustomBScanElement.SetWidth(Value: Integer);
begin
  FWorkRect.Right:= FWorkRect.Left + Value;
end;

{ TStrobArea }

procedure TStrobArea.AssignProperKanalColor;
begin
  if Assigned(FKanal) then
    if FKanal.Num mod 2 = 0 then FCurKanalColor:= FForwardColor
    else FCurKanalColor:= FBackwardColor;
end;

constructor TStrobArea.Create;
begin
  inherited Create;
  FForwardColor:=clGreen;
  FBackwardColor:=clGreen;
  FCurKanalColor:=clYellow;
end;

destructor TStrobArea.Destroy;
begin

  inherited;
end;

procedure TStrobArea.DrawBackGroundContent;
begin
  inherited;

end;

procedure TStrobArea.DrawForeGroundContent;
begin
  inherited;

end;

procedure TStrobArea.SetColorTable(Value: TColorTable);
begin
  inherited;
  if Assigned(FColorTable) then
    begin
      FForwardColor:=FColorTable.Color['BScan_ForwardChannel'];
      FBackwardColor:=FColorTable.Color['BScan_BackwardChannel'];
      AssignProperKanalColor;
    end;
end;

procedure TStrobArea.SetKanal(Value: TKanal);
begin
  FKanal:=Value;
  AssignProperKanalColor;
end;

{ TPainter }

constructor TPainter.Create;
begin
  FCanvas:=nil;
  FWorkRect:=nil;
  ScanLineMas:=nil;
  FDelayMin:=0;
  FDelayMax:=0;
  FPaddingRect.Left:=0;
  FPaddingRect.Right:=0;
  FPaddingRect.Top:=0;
  FPaddingRect.Bottom:=0;
  FPadding:=0;
  FillChar(FDelays, SizeOf(FDelays), 0);
end;

destructor TPainter.Destroy;
begin

  inherited;
end;

procedure TPainter.Line(X1, Y1, X2, Y2: Integer; Color: TRGB);
begin
  if ((not Assigned(FWorkRect)) or (not Assigned(FCanvas))) then Exit;
  if (X1 < FWorkRect^.Left) then X1:= FWorkRect^.Left;
  if (X1 > FWorkRect^.Right) then X1:= FWorkRect^.Right;

  if (X2 < FWorkRect^.Left) then X2:= FWorkRect^.Left;
  if (X2 > FWorkRect^.Right) then X2:= FWorkRect^.Right;

  if (Y1 < FWorkRect^.Top) then Y1:= FWorkRect^.Top;
  if (Y1 > FWorkRect^.Bottom) then Y1:= FWorkRect^.Bottom;

  if (Y2 < FWorkRect^.Top) then Y2:= FWorkRect^.Top;
  if (Y2 > FWorkRect^.Bottom) then Y2:= FWorkRect^.Bottom;


  FCanvas.MoveTo(X1, Y1);
  FCanvas.LineTo(X2, Y2);
end;

procedure TPainter.PutPixel(X, Y: Integer; Color: TRGB);
var
  P: PByteArray;
  a: Integer;
begin
  if ((X < FWorkRect^.Left) and (X > FWorkRect^.Right)) then Exit;
  if ((Y < FWorkRect^.Top) and (Y > FWorkRect^.Bottom)) then Exit;
  a:=Length(ScanLineMas);
  if (Y<a) and (X>=0) and (Y>=0) then
    begin
      P:=ScanLineMas[Y];
      if P<>nil then
        begin
          P^[X*4]:=Color.R;
          P^[X*4+1]:=Color.G;
          P^[X*4+2]:=Color.B;
        end;
    end;
end;

procedure TPainter.PutSignal(X: Integer; D: Byte; Color: TRGB);
begin
  PutPixel(X, FDelays[D], Color);
end;

procedure TPainter.RecountDelays;
var
  i: Integer;
  k: single;
begin
  FPaddingRect.Left:=FWorkRect^.Left+FPadding;
  FPaddingRect.Top:=FWorkRect^.Top+FPadding;
  FPaddingRect.Right:=FWorkRect^.Right-FPadding;
  FPaddingRect.Bottom:=FWorkRect^.Bottom-FPadding;
  FillChar(FDelays, SizeOf(FDelays), 0);
  if (FDelayMax-FDelayMin) > 0 then
    k:=(FPaddingRect.Bottom-FPaddingRect.Top)/(FDelayMax-FDelayMin)
  else k:=0;
  for i:=FDelayMin to FDelayMax do
     FDelays[i]:=FPaddingRect.Bottom-Round(i*k);
end;

procedure TPainter.Resize;
begin
  RecountDelays;
end;

procedure TPainter.SetMaxDelay(Value: Integer);
begin
  FDelayMax:=Value;
  Self.RecountDelays;
end;

procedure TPainter.SetMinDelay(Value: Integer);
begin
  FDelayMin:=Value;
  Self.RecountDelays;
end;

procedure TPainter.SetPadding(Value: Integer);
begin
  if Value >= 0 then
    begin
      FPadding:=Value;
      RecountDelays;
    end;
end;

procedure TPainter.SetWorkRect(Value: PRect);
begin
  FWorkRect:=Value;
  Self.RecountDelays;
end;

{ TKanalList }

function TKanalList.Get(Index: Integer): TKanal;
begin
  Result:= inherited Get( Index );
end;

procedure TKanalList.Put(Index: Integer; Item: TKanal);
begin
  inherited Put( Index, Item );
end;

{ TZeroChannelProcessor }

procedure TZeroChannelProcessor.AddZeroChannel(ZTMNum, EchoNum, StrobSt, StrobEd: Integer);
begin
  SetLength(FZeros, Length(FZeros)+1);
  FZeros[Length(FZeros)-1].ZTM:= ZTMNum;
  FZeros[Length(FZeros)-1].ECHO:= EchoNum;
  FZeros[Length(FZeros)-1].ZTM_Strob_st:= StrobSt;
  FZeros[Length(FZeros)-1].ZTM_Strob_ed:= StrobEd;
end;

constructor TZeroChannelProcessor.Create;
begin
  SetLength(FZeros, 0);
end;

destructor TZeroChannelProcessor.Destroy;
begin
  SetLength(FZeros, 0);
  inherited;
end;

function TZeroChannelProcessor.GetFlag(ChNum: Integer): Boolean;
var
  i: Integer;
begin
  result:= true;

end;

{ TStrobPainter }

constructor TStrobPainter.Create(Bmp: TBitMap; Left, Right, Top, Bottom: Integer);
begin
  FBmp:= Bmp;
  FBounds.Left:= Left;
  FBounds.Right:= Right;
  FBounds.Top:= Top;
  FBounds.Bottom:= Bottom;
  FWidth:= abs(FBounds.Right - FBounds.Left);
  FHeight:= abs(FBounds.Bottom - FBounds.Top);
  FMiddle:= FWidth div 2;
  FThird:= FWidth div 3;
  Kind:= skInner;
end;

procedure TStrobPainter.DrawStrob;
const
    HatLength = 6;
begin
  FBmp.Canvas.Pen.Color:=Color;
  case Kind of
     skCenter: begin
                if hasBegin = true then
                begin
                 FBmp.Canvas.MoveTo( FBounds.Left + FMiddle - HatLength, FY1 );
                 FBmp.Canvas.LineTo( FBounds.Left + FMiddle + HatLength, FY1 );
                 FBmp.Canvas.MoveTo( FBounds.Left + FMiddle - HatLength, FY1-1 );
                 FBmp.Canvas.LineTo( FBounds.Left + FMiddle + HatLength, FY1-1 );
                end;

                if hasEnd = true then
                begin
                 FBmp.Canvas.MoveTo( FBounds.Left + FMiddle - HatLength, FY2 );
                 FBmp.Canvas.LineTo( FBounds.Left + FMiddle + HatLength, FY2 );
                 FBmp.Canvas.MoveTo( FBounds.Left + FMiddle - HatLength, FY2+1 );
                 FBmp.Canvas.LineTo( FBounds.Left + FMiddle + HatLength, FY2+1 );
                end;

                 FBmp.Canvas.MoveTo( (FBounds.Left + FMiddle), FY1 );
                 FBmp.Canvas.LineTo( (FBounds.Left + FMiddle), FY2 );
                 FBmp.Canvas.MoveTo( (FBounds.Left + FMiddle)-1, FY1 );
                 FBmp.Canvas.LineTo( (FBounds.Left + FMiddle)-1, FY2 );
               end;
     skInner: begin
                if hasBegin = true then
                begin
                 FBmp.Canvas.MoveTo( (FBounds.Left + FThird) - HatLength, FY1 );
                 FBmp.Canvas.LineTo( (FBounds.Left + FThird) + HatLength, FY1 );
                 FBmp.Canvas.MoveTo( (FBounds.Left + FThird) - HatLength, FY1-1 );
                 FBmp.Canvas.LineTo( (FBounds.Left + FThird) + HatLength, FY1-1 );
                end;

                if hasEnd = true then
                begin
                 FBmp.Canvas.MoveTo( (FBounds.Left + FThird) - HatLength, FY2 );
                 FBmp.Canvas.LineTo( (FBounds.Left + FThird) + HatLength, FY2 );
                 FBmp.Canvas.MoveTo( (FBounds.Left + FThird) - HatLength, FY2+1 );
                 FBmp.Canvas.LineTo( (FBounds.Left + FThird) + HatLength, FY2+1 );
                end;

                 FBmp.Canvas.MoveTo( (FBounds.Left + FThird), FY1 );
                 FBmp.Canvas.LineTo( (FBounds.Left + FThird), FY2 );
                 FBmp.Canvas.MoveTo( (FBounds.Left + FThird)-1, FY1 );
                 FBmp.Canvas.LineTo( (FBounds.Left + FThird)-1, FY2 );
              end;
     skOuter: begin
                if hasBegin = true then
                begin
                 FBmp.Canvas.MoveTo( (FBounds.Left + FThird*2) - HatLength, FY1 );
                 FBmp.Canvas.LineTo( (FBounds.Left + FThird*2) + HatLength, FY1 );
                 FBmp.Canvas.MoveTo( (FBounds.Left + FThird*2) - HatLength, FY1-1 );
                 FBmp.Canvas.LineTo( (FBounds.Left + FThird*2) + HatLength, FY1-1 );
                end;

                if hasEnd = true then
                begin
                 FBmp.Canvas.MoveTo( (FBounds.Left + FThird*2) - HatLength, FY2 );
                 FBmp.Canvas.LineTo( (FBounds.Left + FThird*2) + HatLength, FY2 );
                 FBmp.Canvas.MoveTo( (FBounds.Left + FThird*2) - HatLength, FY2+1 );
                 FBmp.Canvas.LineTo( (FBounds.Left + FThird*2) + HatLength, FY2+1 );
                end;

                 FBmp.Canvas.MoveTo( (FBounds.Left + FThird*2), FY1 );
                 FBmp.Canvas.LineTo( (FBounds.Left + FThird*2), FY2 );
                 FBmp.Canvas.MoveTo( (FBounds.Left + FThird*2)-1, FY1 );
                 FBmp.Canvas.LineTo( (FBounds.Left + FThird*2)-1, FY2 );
              end;
  end;
end;

procedure TStrobPainter.SetStrobValues(Y1, Y2: Integer);
begin
    FY1:= min(Y1,Y2);
    FY2:= max(Y1,Y2);
end;

end.
