unit MessageUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, sLabel;

type
  // ���-���-���
  TWinKind = (kBad, kGood);

  TOnMsgCloseEvent = procedure of object;

  TMessageForm = class(TForm)
    Panel1: TPanel;
    Shape1: TShape;
    Label1_old: TLabel;
    WaitTimer: TTimer;
    FadeTimer: TTimer;
    Label1: TsLabel;
    PaintBox1: TPaintBox;
    procedure WaitTimerTimer(Sender: TObject);
    procedure FadeTimerTimer(Sender: TObject);
    procedure Label1_oldClick(Sender: TObject);
    procedure Shape1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Panel1Click(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure Panel1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label1_oldMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    FOnMsgClose: TOnMsgCloseEvent;
    FInterval: integer;
    FDispNow: Boolean;
    procedure OnClk;
    { Private declarations }
  public
    { Public declarations }
    procedure Display(Text: string); overload;
    procedure Display(Text: string; Color: TColor); overload;
    procedure Display(Text: string; Kind: TWinKind); overload;
    procedure Display(Text: string; Color: TColor; Time: Integer; OnCloseEvent: TOnMsgCloseEvent); overload;
  end;

var
  MessageForm: TMessageForm;

implementation

{$R *.dfm}
{ TMessageForm }

procedure TMessageForm.Display(Text: string; Kind: TWinKind);
begin
  if FDispNow then Exit;
  FOnMsgClose:= nil;
  FInterval:= 20000;
  case Kind of
    kGood:
      Display(Text, clMoneyGreen);
    kBad:
      Display(Text, clOlive);
  end;
end;

procedure TMessageForm.Display(Text: string; Color: TColor);
begin
  if FDispNow then Exit;

  FDispNow:= true;
  Label1.AutoSize:= true;
  Label1.Width:= Shape1.Width - 20;
  Label1.Caption := Text;
  Shape1.Height := Label1.Height + 20;
  Shape1.Brush.Color := Color;
  Label1.Top := Shape1.Top + 10;
  Label1.Left := Round((Shape1.Width-Label1.Width)/2);

 { Label1.AutoSize:= true;
  Label1.Caption := Text;
  Label1_old.Height:=0;
  Label1_old.Caption := Text;
  //Label1.Height := Label1_old.Height;
  Label1.Width := Label1_old.Width;
  Label1_old.Caption := '';
  Shape1.Brush.Color := Color;
  Shape1.Height := Label1.Height + 20;
  Label1.Top := Shape1.Top + 10;
  Label1.Width := Shape1.Width - 20;
  Label1.Left := 10;
  Self.Show;
  WaitTimer.Enabled := False;
  //WaitTimer.Enabled := True;
  //WaitTimer.Interval := 20000;
  FadeTimer.Enabled := False;}

  {PaintBox1.Canvas.Font.Size:= 24;
  PaintBox1.Canvas.Font.Name:= 'Impact';
  PaintBox1.Canvas.Font.Style:= [fsBold];}


  Self.Show;
  WaitTimer.Enabled := False;
  FadeTimer.Enabled := False;

  Self.AlphaBlendValue := 255;
  if FInterval > 0 then
    begin
      WaitTimer.Interval:= FInterval;
      WaitTimer.Enabled := True;
    end;
end;

procedure TMessageForm.Display(Text: string);
begin
  if FDispNow then Exit;
  FOnMsgClose:= nil;
  FInterval:= 20000;
  Display(Text, clMoneyGreen);
end;

procedure TMessageForm.WaitTimerTimer(Sender: TObject);
begin
  // FadeTimer.Enabled:= True;
  WaitTimer.Enabled := False;
  Self.Close;
end;

procedure TMessageForm.FadeTimerTimer(Sender: TObject);
const
  dAlfa = 20;

begin
  { Self.AlphaBlend:= True;

    if Self.AlphaBlendValue = 0 then
    begin
    FadeTimer.Enabled:= False;
    Self.Close;
    end;

    if Integer( Self.AlphaBlendValue ) - dAlfa > 0 then
    Self.AlphaBlendValue:= Self.AlphaBlendValue - dAlfa
    else
    Self.AlphaBlendValue:= 0; }
end;

procedure TMessageForm.Label1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  OnClk;
end;

procedure TMessageForm.Label1_oldClick(Sender: TObject);
begin
  OnClk;
end;

procedure TMessageForm.Label1_oldMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  OnClk;
end;

procedure TMessageForm.Shape1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  OnClk;
end;

procedure TMessageForm.Panel1Click(Sender: TObject);
begin
  OnClk;
end;

procedure TMessageForm.Panel1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  OnClk;
end;

procedure TMessageForm.FormClick(Sender: TObject);
begin
  OnClk;
end;

procedure TMessageForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FDispNow:= false;
  if Assigned(FOnMsgClose) then
    FOnMsgClose;
end;

procedure TMessageForm.FormCreate(Sender: TObject);
begin
  Label1.Height:= 0;
  Label1.Caption:= '';
  Shape1.Height:=10;
  FOnMsgClose:= nil;
  FInterval:= 20000;
  FDispNow:= false;
end;

procedure TMessageForm.OnClk;
begin
  WaitTimerTimer(nil);
  // Self.Close;
end;

procedure TMessageForm.Display(Text: string; Color: TColor; Time: Integer;
  OnCloseEvent: TOnMsgCloseEvent);
begin
  if FDispNow then Exit;
  FOnMsgClose:= OnCloseEvent;
  FInterval:= Time;
  Display(Text, Color);
end;

end.
