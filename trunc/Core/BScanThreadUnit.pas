unit BScanThreadUnit;

interface
uses Windows, Classes, Dialogs, Math,
     ConfigObj, MCAN, PublicData, AviconDataContainer,  DataFileConfig, AviconTypes, PublicFunc;

{$I Directives.inc}

{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
  {$DEFINE Sinh4}
{$ELSE}
  {$DEFINE Sinh2}
{$IFEND}

type

  TCurrentBUM = ( bFirst, bSecond, bLonely, bThird, bFourth);

  TAirBrushAddSysCoord = procedure (Coord: Integer) of object;
  //TAirBrushAddEcho = procedure (Rail: TRail; ScanCh, Count: Byte; EchoBlock: TEchoBlock) of object;

  TBScanStoreThread = class( TThread )
  private
    //FDataContainer: TAvk11DataContainer;
    FDataContainer: TAviconDataContainer;
    FCanList: TCanList;

//    Log: TStringList;
    D1, D2, Way1, Way2: Integer;
    Count1, Count2: Integer;

    CurrentAK: TAK;
    FCurKanalNum: Integer;
    FCurBUMSide: Byte;

    procedure LoadBV( CurrentBUM: TCurrentBUM; BV: TBVBuffEl );
    procedure OnDecodeBV( Rail, Takt: Byte; Echos: TDecodedEchos; AdditionalData: Integer );

    procedure LoadHS( BV: TBVBuffEl );
    procedure ProcessAK;
    procedure ChangeSysCoord( Delta: Integer );

    procedure AddAdjustChanges(Force: Boolean);
  protected
    procedure Execute; override;
  public
    Registration: Boolean;
    RegMode: Integer;
    Pause: Boolean;
    Hand: Boolean;
    HandBUM: Integer;
    WaiteForBUM: Boolean;

//    OnBScanReserved: procedure (ID: Byte; BV: TBVBuffEl ) of object;
    OnHandScanReceived: procedure (HSItem: THSItem) of object;
    OnAirBrushAddSysCoord: TAirBrushAddSysCoord;
    //AirBrushAddEcho: TAirBrushAddEcho;
    //OnSysCoordChanged: TNotifyEvent;
    constructor Create( DataContainer: TAviconDataContainer; CanList: TCanList );
    destructor Destroy; override;

    procedure SetContainer(Container: TAviconDataContainer);
    procedure SetKanal(KanalNum:integer);

    property SummaryWay1: Integer read Way1;
    property SummaryWay2: Integer read Way2;
    property SummaryCount1: Integer read Count1;
    property SummaryCount2: Integer read Count2;
  end;


  THandStoreThread = class( TThread )
  private
    ConfigList: TDataFileConfigList;
    FDataContainer: TAviconDataContainer;
    FCAN: TCAN;
    FRail: Byte;
    FKanal: Integer;

    procedure OnDecodeBV( Rail, Takt: Byte; Echos: TDecodedEchos; AdditionalData: Integer );

  protected
    procedure Execute; override;
    procedure SetKanalInRegistrator(Value: Integer);
  public
    Pause: Boolean;
    constructor Create( CAN: TCAN; Rail: Byte );
    destructor Destroy; override;

    property DataContainer: TAviconDataContainer read FDataContainer;
    property CAN: TCAN read FCAN write FCAN;
    property Rail: Byte read FRail write FRail;
    property KanalNum: Integer read FKanal write SetKanalInRegistrator;
  end;



  TRefreshCommand = ( cNone, cInstallVRU, cInstallStrob, cStartAView, cInstall2TP, cRunKraskopult );

  TRefreshCmd =
  record
    Kanal: TKanal;
    Command: TRefreshCommand;
  end;

  TSendToBUMThread = class( TThread )
  private
    CommandCount: Integer;
    CurrentCommand: Integer;
    Commands: array[0..500] of TRefreshCmd;
  protected
    procedure Execute; override;
  public
    TestSend: Boolean;
    constructor Create;
    procedure Add( Kanal: TKanal; Command: TRefreshCommand );
  end;

var
  SendBUM: TSendToBUMThread;

implementation
uses SysUtils, MainUnit;



{ TBScanStoreThread }

constructor TBScanStoreThread.Create( DataContainer: TAviconDataContainer; CanList: TCanList );
begin
  Hand:= false;
  WaiteForBUM:= false;
  HandBUM:= 0;
  //AirBrushAddSysCoord:= nil;
  //AirBrushAddEcho:= nil;
  FCanList:= CanList;
  FDataContainer:= DataContainer;
  //AddAdjustChanges;
//  OnBScanReserved:= nil;
  Registration:= True;
  Pause:=false;
  CANList:=nil;
  Way1:=0;
  Way2:=0;
  Count1:= 0;
  Count2:= 0;
  Self.AddAdjustChanges(true);
//  Log:=TStringList.Create;
  inherited Create( False );
end;

procedure TBScanStoreThread.SetContainer(Container: TAviconDataContainer);
begin
  FDataContainer:= Container;
end;

procedure TBScanStoreThread.SetKanal(KanalNum:integer);
begin
  FCurKanalNum:= KanalNum;
  if Hand then
    if FCurKanalNum<=1 then FCurBUMSide:=1 else FCurBUMSide:=0
  else FCurBUMSide:=0;
end;

procedure TBScanStoreThread.ChangeSysCoord( Delta: Integer );
begin
  if (not Pause) and (not Self.Suspended) then
    begin
      if Assigned(FDataContainer) then
        FDataContainer.AddSysCoord( FDataContainer.CurSaveSysCoord + Delta );
      if (Assigned(OnAirBrushAddSysCoord)) and (Assigned(FDataContainer)) then
          OnAirBrushAddSysCoord(FDataContainer.MaxDisCoord);
    end;
end;

procedure TBScanStoreThread.OnDecodeBV( Rail, Takt: Byte; Echos: TDecodedEchos; AdditionalData: Integer );
var
  BUM: Byte;
  Kan, i: Integer;
  R: Byte;
  ScanCh, Count: Integer;
  EchoBlock: TEchoBlock;

begin
  if not Assigned(FDataContainer) then
    Exit;
  case TCurrentBUM( AdditionalData ) of
  bLonely:
    begin
      BUM:= 0;
      R:= Rail;
    end;
  bFirst:
    begin
      BUM:= 0;
      R:= 0;
    end;
{$IFnDEF BUMCOUNT4}
  bSecond:
    begin
      BUM:= 1;
      R:= 1;
    end;
{$ELSE}
  bSecond:
    begin
      BUM:= 1;
      R:= 0;
    end;
  bThird:
    begin
      BUM:= 2;
      R:= 1;
    end;
  bFourth:
    begin
      BUM:= 3;
      R:= 1;
    end;
{$ENDIF}
  end;

  if not Hand then //AK
    if Takt <= High( Takts[ BUM, Rail xor Config.WorkRail ] ) then
      if Takts[ BUM, Rail xor Config.WorkRail, Takt ] <> Nil then
        Kan:= Takts[ BUM, Rail xor Config.WorkRail, Takt ].Kan1
      else Exit
    else Exit;

{$IFDEF BUMCOUNT4}
  if (Hand) then
    if (Rail<>FCurBUMSide) or (BUM<>HTakts[0].CANObj.ID) then  Exit;
{$ELSE}
  if (Hand) and ((R<>HTakts[0].CANObj.ID)) then  Exit;
{$ENDIF}

  if Kan = 8 then Kan:= -1;
  if Kan = 9 then Kan:= -1;

  if Kan = -1 then Exit;

  if Kan = 0 then Kan:= 1;

  Count:= Min(Echos.Count, 8);
  for i := 0 to Count - 1 do
    begin


      EchoBlock[i].T:= Echos.Echo[i].T;
      EchoBlock[i].A:= Echos.Echo[i].A;

    end;

  if Hand then
    begin
      R:= 0;
      Kan:= 0;
    end;

  if (not Pause) and (not WaiteForBUM) then
    FDataContainer.AddEcho(TRail(R), Kan, Count, EchoBlock);
end;

procedure TBScanStoreThread.LoadBV( CurrentBUM: TCurrentBUM; BV: TBVBuffEl );
begin
  DecodeBScan( BV.Data, BV.DataCount, OnDecodeBV, Ord( CurrentBUM ) );
end;


procedure TBScanStoreThread.ProcessAK;
var
  Rail: Byte;
  i: Byte;

begin

end;


procedure TBScanStoreThread.Execute;
var
  i: Integer;

  BVCount0: Integer;
  BVCount1: Integer;
  BV0: TBVBuff;
  BV1: TBVBuff;

  BVCount2: Integer;
  BVCount3: Integer;
  PBV2: PBVBuff;
  PBV3: PBVBuff;

  j: Integer;
  k: Integer;
  m, m1: Integer;

  Cnt: Integer;

  imax,imin: byte;
  minBV,maxBV,tmpcnt:integer;

begin

  try
  New(PBV2);
  New(PBV3);

  while not Terminated do
  begin
{$IFDEF TESTKEYS}
 if DeviceInterfaceUnit.LockCANThreads then break;
{$ENDIF}


    {$IFDEF UseHardware}

  {$IFnDEF BUMCOUNT4}
    if FCanList.Count <= 2 then
    begin
      // ������������ 2 ���.

      repeat

        Cnt:= -1;
        {$IFDEF Sinh2}
        if FCanList.Count = 2 then
        begin
          if Abs( FCANList[0].BVBuffCount - FCANList[1].BVBuffCount ) < 500 then
          begin
            Cnt:= Min( FCANList[0].BVBuffCount, FCANList[1].BVBuffCount );

            // �������������� ��������� - � ����� ������� ����������� �������� ����� �������.
            // ����������, ����� �������� ���������� ������ 100 �������� ��.
            if FCANList[0].BVBuffCount - FCANList[1].BVBuffCount > 100 then
              FCANList[0].GetBV( BV0, 1 );

            if FCANList[1].BVBuffCount - FCANList[0].BVBuffCount > 100 then
              FCANList[1].GetBV( BV0, 1 ); //��������� ��, ��� � BV0? //AK

          end
          else
            // ��� ����� ������� ��������, ���������� �������� ������������� ������������ ���.
            General.Core.BumFailed:= True;

        end;
        {$ENDIF}

        BVCount0:= FCANList[0].GetBV( BV0, Cnt );
        if FCanList.Count = 2 then BVCount1:= FCANList[1].GetBV( BV1, Cnt ) else BVCount1:= 0;


        for i:= 0 to BVCount0 - 1 do     // ���� �� ���������� ����� ��.
        begin
          ChangeSysCoord( BV0[i].Napr );

          // �������� �� ���������� �� ���������, ���� ���������� ��������� ����� �������� � ���� �������������.
          AddAdjustChanges(false);

          if FCanList.Count = 1 then LoadBV( bLonely, BV0[i] ) else LoadBV( bFirst, BV0[i] );
          if i <= BVCount1 - 1 then LoadBV( bSecond, BV1[i] );

        end;
      until BVCount0 = 0;
      Sleep( 10 );

    end;
  {$ELSE}
    if FCanList.Count <= 4 then
    begin
      // ������������ 4 ���.

      repeat

        Cnt:= -1;

       {$IFDEF Sinh4}
        if FCanList.Count = 4 then
        begin
//          GetMinMaxBV(idxMinBV, minbuffcount, idxMaxBV, maxbuffcount);
	      	imax:=0; maxBV:=0; imin:=0; minBV:=BVBufferCount;
			    for i:=0 to FCanList.Count-1 do begin
  			    tmpcnt:=FCANList[i].BVBuffCount;
	  		   //���� ��� � ������������ �������� ������
		  	    if  tmpcnt > maxBV then begin
			        maxBV:=tmpcnt;
				      imax:=i;
			      end;
  			    if  tmpcnt < minBV then begin
	  		      minBV:=tmpcnt;
		  		    imin:=i;
			      end;
			    end;

//          if Abs( FCANList[0].BVBuffCount - FCANList[1].BVBuffCount ) < 500 then
//          if maxbuffcount-minbuffcount < 500 then
          if maxBV-minBV < 500 then
          begin
//            if minBV>0 then
              Cnt:= minBV;
            if maxBV-FCANList[0].BVBuffCount>100 then
              FCANList[imax].GetBV( BV0, 1 );
            // �������������� ��������� - � ����� ������� ����������� �������� ����� �������.
            // ����������, ����� �������� ���������� ������ 100 �������� ��.
//            if maxbuffcount-minbuffcount > 100 then
//              FCANList[idxMaxBV].GetBV( GetBufByIdx(idxMaxBV), 1 );
          end
          else
            // ��� ����� ������� ��������, ���������� �������� ������������� ������������ ���.
            General.Core.BumFailed:= True;

        end;
       {$ENDIF}

    //��� ��� ��� ���������������� �� ����������?
      //������ ��� 2-� CAN-�� �������� ������� ��������� �� ���� ����, ���
      //� BV0 ����� �� ������� ������, �.�. ������� �������� � ������ ����
      //� BV ���� ������ ��� ������� ������� ��
      //������� �� �������� � ������ ���� �.�. GetBV ������ � 0-�� �������
      //� ������� ������ �������������
        BVCount0:= FCANList[0].GetBV( BV0, Cnt );
        BVCount1:= FCANList[1].GetBV( BV1, Cnt );
        BVCount2:= FCANList[2].GetBV( PBV2^, Cnt );
        BVCount3:= FCANList[3].GetBV( PBV3^, Cnt );
        //��������� ��� ������ ����� �������  1-2 � 3-4 ���



        for i:= 0 to BVCount0 - 1 do     // ���� �� ���������� ����� ��.
        begin
          ChangeSysCoord( BV0[i].Napr );
{$IFDEF TESTING_COORD}
          if BV0[i].FullCoord<>0 then FDataContainer.AddTestFullSysCoord_UMUA_Left(BV0[i].FullCoord);
          if BV1[i].FullCoord<>0 then FDataContainer.AddTestFullSysCoord_UMUB_Left(BV1[i].FullCoord);
          if PBV2^[i].FullCoord<>0 then FDataContainer.AddTestFullSysCoord_UMUA_Right(PBV2^[i].FullCoord);
          if PBV3^[i].FullCoord<>0 then FDataContainer.AddTestFullSysCoord_UMUB_Right(PBV3^[i].FullCoord);

          if BV0[i].DeltaCoord<>0 then FDataContainer.AddTestDeltaSysCoord_UMUA_Left(BV0[i].DeltaCoord);
          if BV1[i].DeltaCoord<>0 then FDataContainer.AddTestDeltaSysCoord_UMUB_Left(BV1[i].DeltaCoord);
          if PBV2^[i].DeltaCoord<>0 then FDataContainer.AddTestDeltaSysCoord_UMUA_Right(PBV2^[i].DeltaCoord);
          if PBV3^[i].DeltaCoord<>0 then FDataContainer.AddTestDeltaSysCoord_UMUB_Right(PBV3^[i].DeltaCoord);

{$ENDIF}

          // �������� �� ���������� �� ���������, ���� ���������� ��������� ����� �������� � ���� �������������.
          AddAdjustChanges(false);

          LoadBV( bFirst, BV0[i] );
          if i <= BVCount1 - 1 then LoadBV( bSecond, BV1[i] );
          if i <= BVCount2 - 1 then LoadBV( bThird,  PBV2^[i] );
          if i <= BVCount3 - 1 then LoadBV( bFourth, PBV3^[i] );

        end;
      until BVCount0 = 0;
      Sleep( 10 );

    end;
  {$ENDIF}

   {$ENDIF}
  end;
  Dispose(PBV2); //PBV2:=Nil;
  Dispose(PBV3); //PBV3:=Nil;
  except
    on E: Exception do begin
//      if Assigned(PBV2) then Dispose(PBV2);
//      if Assigned(PBV3) then Dispose(PBV3);
      ProcessException.RaiseException( 'BScanThread', E );
    end;
  end;
end;

procedure TBScanStoreThread.AddAdjustChanges(Force: Boolean);
var
  i, j, a, b, EvalCh, rm: Integer;
  k: TAdjustParams;
  snd: Boolean;
  SndOn: array[0..1, 0..9] of Boolean;
  R: TRail;
  (*<Rud29>*)
  old_Str_st,old_Str_en : Byte;
  (*</Rud29>*)

begin
  if Assigned(FDataContainer) then
  begin
    snd:= False;
    for i:=0 to {$IFDEF TwoThreadsDevice}1{$ELSE}0{$ENDIF} do
    begin
      for j:=0 to High(Kanals[i]) do
      for k:=apKy to apTwoTp do
      if Kanals[i, j].AdjustChanges[k].Flag or Force then
      begin
        R:= TRail(i);
        case k of
          apKy: FDataContainer.AddKu(R, j, Kanals[i, j].AdjustChanges[k].CurValue);
          apATT: FDataContainer.AddAtt(R, j, Kanals[i, j].AdjustChanges[k].CurValue);
          apStrobSt: FDataContainer.AddStStr(R, j, Kanals[i, j].AdjustChanges[k].CurValue);
          apStrobEd: FDataContainer.AddEndStr(R, j, Kanals[i, j].AdjustChanges[k].CurValue);
          apVRCH: FDataContainer.AddTVG(R, j, Kanals[i, j].AdjustChanges[k].CurValue);
          apTwoTp: FDataContainer.AddPrismDelay(R, j, Kanals[i, j].AdjustChanges[k].CurValue);
          apSound: snd:= True;
        end;

        Kanals[i, j].AdjustChanges[k].Flag:= False;
      end;
    end;

  if snd then
  begin
    rm:=0;
    {$IFDEF TwoThreadsDevice}
    rm:=1;
    {$ENDIF}
    for i:=0 to rm do
      for j := 0 to High(Kanals[i]) do
         begin
           FDataContainer.AddHeadPh(TRail(i), j, Kanals[i, j].SoundOn);

           (*<Rud29>*)
            // ��� ������� 0� ��� 1 (� 11 ���� �����-�����)
           if ( (j=1) or
{$IFDEF EGO_USW}
              (j=11) ) then
{$ELSE}
              ((Config.SoundedScheme = Wheels) and (j=11)) ) then
{$ENDIF}
           begin
              FDataContainer.AddStStr(TRail(i), j, Kanals[i, j].Str_st);
              FDataContainer.AddEndStr(TRail(i), j, Kanals[i, j].Str_en);
           end;
           (*</Rud29>*)
         end;
    end;

  end;
end;

procedure TBScanStoreThread.LoadHS(BV: TBVBuffEl);
var
  k, k0: Byte;
  Pos, MaxPos, i: Integer;
  Samp: Word;
  ResItem: THSItem;

procedure FillHSItemData(var Item: THSItem; Start, Count: Integer);
var
  i: Integer;
  c: Byte;
begin
  case Count of
    2: c:=1;
    3: c:=2;
    5: c:=3;
    6: c:=4;
  end;
  Item.Count:=c;
  for i:=0 to Count-1 do
    Item.Data[i]:=BV.Data[i+Start];
end;
begin
  //Pos:=0;
  // k0     M
  // 2  ->  1
  // 3  ->  2
  // 5  ->  3
  // 6  ->  4
  MaxPos:=BV.DataCount;
  if MaxPos>=3 then
    begin
      Samp:=0;
      k0:=BV.Data[0]-1;

      if k0<=6 then // ���� ���������� ���� �� ���-������� 6 � ����� �� ��������� � ������ ���� HSItem
        begin
          ResItem.Sample:=0;
          FillHSItemData(ResItem, 2, k0);
          if Assigned(OnHandScanReceived) then OnHandScanReceived(ResItem);
        end
      else if k0>6 then // ����� ���� ���� �� ������� ������ �� ��������� ������ 4 � ������, ����� ��������� � ������ ���������
        begin
          ResItem.Sample:=0;
          FillHSItemData(ResItem, 2, k0);
          if Assigned(OnHandScanReceived) then OnHandScanReceived(ResItem);
          k0:=BV.Data[8]-1;
          ResItem.Sample:=1;
          FillHSItemData(ResItem, 8, k0);
          if Assigned(OnHandScanReceived) then OnHandScanReceived(ResItem);
        end;
    end;
end;

destructor TBScanStoreThread.Destroy;
begin
//  Log.SaveToFile('C:\BScanLog.txt');
//  Log.Free;
  inherited;
end;

{ TSendToBUMThread }

constructor TSendToBUMThread.Create;
begin
  inherited Create( False );
  CommandCount:= 0;
  CurrentCommand:= 0;
  TestSend:= False;
end;

procedure TSendToBUMThread.Add( Kanal: TKanal; Command: TRefreshCommand );
var
  Cmd: TRefreshCmd;
  i: Integer;

begin
  if CurrentCommand >= CommandCount then  // ���� ��� ������� ���������, ������� �����.
  begin
    CommandCount:= 0;
    CurrentCommand:= 0;
  end;

  Cmd.Kanal:= Kanal;
  Cmd.Command:= Command;

  // �� ��������� �������, ���� ��� ��� ���� � ������.
//  for i:= 0 to CommandCount do
//  if CompareMem( @Cmd, @Commands[i], SizeOf( TRefreshCmd ) ) then Exit;

  // ������� �������.
  if CommandCount + 1 > Length( Commands ) then Exit; // ������������ ������!
  Commands[ CommandCount]:= Cmd;
  Inc( CommandCount );

end;

procedure TSendToBUMThread.Execute;
begin
  try
  while not Terminated do
  begin
    {$IFDEF TestSend}

    if Assigned( General ) and ( General.Core.CanCount = 2 ) and TestSend then
    begin
//      General.Core.CANList[0].SendBL( $DF, 8, 1, 2, 3, 4, 5, 6, 7, 8 );
      General.Core.CANList[0].SendBL( $DE, 0 );
    end;
    {$ELSE}

    if CurrentCommand < CommandCount then
    begin
      with Commands[CurrentCommand] do
      case Command of
        cInstallVRU: Kanal.Takt.InstallVRU({ CAN,} rScan );
        //cInstallStrob: Kanal.Takt.InstallStrob({ CAN,} rScan, False );
        cInstallStrob: Kanal.Takt.InstallStrob({ CAN,} rScan, General.Core.Adj2Tp );
        cStartAView: Kanal.StartAView;
        cInstall2TP: Kanal.Takt.Install2TP( {CAN });
        cRunKraskopult: if (Config.PaintSprayer<>prAlwaysOff) and General.Core.PaintSprayerOn then Kanal.Takt.CANObj.RunKraskopult;
        cNone: ;
      end;
      Inc( CurrentCommand );
    end else Sleep( 100 );
    {$ENDIF}
  end;
  except
    on E: Exception do ProcessException.RaiseException( 'SendToBUMThread', E );
  end;
end;

{ THandStoreThread }

constructor THandStoreThread.Create( CAN: TCAN; Rail: Byte);
begin
  FKanal:=1;

  Pause:= false;

  ConfigList:= TDataFileConfigList.Create('');
  {$IFDEF OldEngine}
  FDataContainer:= TAviconDataContainer.Create(ConfigList.GetItemByID(AviconHandScan,1), GetFullPath + 'HS.tmp');
  {$ELSE}
  FDataContainer:= TAviconDataContainer.Create(ConfigList.GetItemByID(AviconHandScan,1), true, true, false, false, GetFullPath + 'HS.tmp');
//  FDataContainer:= TAviconDataContainer.Create(ConfigList.GetItemByID(AviconHandScan), false, false, true, '');
  {$ENDIF}

  FCAN:= CAN;
  FRail:= Rail;

  inherited Create(False);
end;

destructor THandStoreThread.Destroy;
begin
  FDataContainer.Free;
  inherited;
end;

procedure THandStoreThread.Execute;
var
  Cnt: Integer;
  BV: TBVBuff;
  i: Integer;

begin

  try
  while not Terminated do
  begin
    Cnt:= FCAN.GetBV( BV );

    for i:= 0 to Cnt - 1 do
    begin
      if not Pause then
         FDataContainer.AddSysCoord( FDataContainer.CurSaveSysCoord + BV[i].Napr );
      DecodeBScan( BV[i].Data, BV[i].DataCount, OnDecodeBV, 0 );
    end;

    if Cnt = 0 then Sleep( 10 );

  end;
  except
    on E: Exception do ProcessException.RaiseException( 'HandStoreThread', E );
  end;

end;

procedure THandStoreThread.OnDecodeBV(Rail, Takt: Byte; Echos: TDecodedEchos; AdditionalData: Integer);
var
  Count: Byte;
  EchoBlock: TEchoBlock;
  i: Integer;
begin
  if Rail = FRail then
    begin
      Count:= Min(Echos.Count, 8);
      for i := 0 to Count - 1 do
        begin
          EchoBlock[i].T:= Echos.Echo[i].T;
          EchoBlock[i].A:= Echos.Echo[i].A;
        end;

      if not Pause then
        FDataContainer.AddEcho(rLeft, 0, Count, EchoBlock);

    end;
end;

procedure THandStoreThread.SetKanalInRegistrator(Value: Integer);
begin
  if Value in [2..6] then
    FKanal:= Value
  else
    FKanal:= 1;
end;

initialization
  SendBUM:= TSendToBUMThread.Create;

finalization
  SendBUM.Terminate;
  SendBUM.Free;

end.

