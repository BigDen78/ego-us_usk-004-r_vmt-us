unit ConfigObj;

interface

uses
  Classes, Types, IniFiles, Graphics, SysUtils, Forms, Windows,
  PublicData, Math;

{$I Directives.inc}

type
  THotKey =
  record
    HotKey: Word;
    HotButtonVisible: Boolean;
  end;

  THotKeys =
  record
    ToMenu: THotKey;
  end;

  TColorConfig =
  record
    Text: TColor;                 // ����� � �����.  (����)
    BackGround: TColor;           // ���.
    ButtonBackground: TColor;     // ��� ������.
    Border: TColor;               // ����� ���������, ����� ������ ����.  (���)
    SelectedArea: TColor;         // ��� ���������� ������, ��� ������ "������", ��� ����������� ������ ���� (���)
    Selected: TColor;             // ���� ����� ���������� ������, ������ ����, �����, ��������� �� ������ (�����)
    Title: TColor;                // ���� ������������ ��������, ���� �������. (���)
    AView: TColor;                // ���� ������� �-��������� (����)
    MainTitle_Text: TColor;       // ���� ������� "������ 11 ���" �� ������ ��������.
    MainTitle_Border: TColor;     // ���� �������� ��� ������� "������ 11 ���" �� ������ ��������.
  end;

  TYDirection = ( yUpToDown, yDownToUp );

//{$IFDEF EGO_USW}
//  TSoundedScheme = ( Scheme1, Scheme2, Scheme3 );
//{$ELSE}
  TSoundedScheme = ( Scheme1, Scheme2, Scheme3, Wheels );
//{$ENDIF}
  TZondPosition = ( zBelow, zAbove ); // �����, ������.
  TBViewPointSize = ( psSmall, psMedium, psLarge );
  TPainterState = (psAlwaysOff, psOnWithReg, psAlwaysOn);
  TPaintRegim = (prAlwaysOff, prManually, prByAlgorithm);
  TUnitsSystem = (usRus, usImperial, usMetricA, usMetricB, usMetricTurkish{, usEuro});
  TSelectChan0 = (scWPAandWPB, scWPA, scWPB);

  TAdjustmentVarArr = array[0..3] of Integer;

  TRSPConfig = class
  protected
    FConfig: TINIFile;
    FConfigFileName: string;
    function GetNastrFolder: string;
    function GetSystemFileName: string;
    function GetAdjustmentVariantNum: Integer;
    procedure SetAdjustmentVariantNum(Value: Integer);
    function GetAdjustmentVariantNumAfterBoot: Integer;
    procedure SetAdjustmentVariantNumAfterBoot(Value: Integer);
  private
    AdjustmentVarArr: TAdjustmentVarArr;
    AdjustmentVarArrAfterReboot: TAdjustmentVarArr;
  public

    // ������������ � ������-15.
    DPStep: Single;
    Organization: string;

    // �����.
    RSPName: string;
    WaitHardware: Boolean;
    ShowDebugData: Boolean;
    AViewShowMM: Boolean;
    DefNumber: string;
    ConnectPort: string;
    NetworkMode: Boolean;
    ServerAddress: string;
    BViewYDirection: TYDirection;
    FFileNum: Integer;
    FThreshold: Integer;

    // �����.
    Colors: TColorConfig;

    // �������.
    VRCHLimit: Integer;
    ShowRailLen: Boolean;
    RailLenProgFilename: string;
    ConfirmExitProgram: Boolean;
    ConfirmShiftNew: Boolean;

    HotKeys: THotKeys;

    SoundedScheme: TSoundedScheme;
    SoundedSchemeAfterReboot: TSoundedScheme;

    Language: string;
    PaintPosition: Single; // � ������.
    ZondPosition: TZondPosition;
    ZondAmpl: Byte;
    BViewPointSize: TBViewPointSize;

    BViewAmpByColor: Boolean;
    BViewAmpBySize: Boolean;

    FlashDrive: Integer;

    PaintSprayer: TPaintRegim;
    PainterState: TPainterState;
    AirBrushConfig: string;
    WorkSide: Boolean; //False = Gauge Left / True=Gauge Right  (��� ������������ ��������)

    // �������, ���������.
    HideNoise: Boolean;
    AlfaKr: Integer;
    Vl: Single;
    Vt: Single;


    // ������������ �� ������-11. �� �����������, �� �����������, ���� ������!
    KanPanelLabelsHand: string;
    KanPanelLabels: array[0..1] of string;
//    TwoTPLimit: Integer;

    WorkRail: Byte;
    { ��� ������������ �������: 0 - ��� ��� ��������, 1 - ����� � ������ ����� �������� �������.
      ��� �������������� ������� ��������� ����� (0 - ���., 1 - ����.), �����. �������� ��, ��������������� �� ������� ������ }

    UseSecondNit: Boolean;
    FixAK: Boolean;
    AViewTabNumber: Integer; // ����� �������� �� �-����.

    Scale: Single;

    // ����
    SndVolume: Integer;

    // ��������� �����������
    //RegOper: Integer;
    //RegPeregon: Integer;
    RegOper: string;
    RegPeregon: string;
    RegNapr: Integer;
    RegWayNum: Integer;
    RegKm: Integer;
    RegPk: Integer;
    RegM: Integer;

    // ��������� ����������� � �������� �������� �� ����������
    TrackDir: Integer;
    //OperEng: Integer;
    OperEng: string;
    Line: string;
    Chainage: string;
    TrackNum: Integer;
    DirEng: Integer;
    TrackID: Integer;

    LeftSide: integer;
    // ������������ ��������� �� ��� ������
    Tm: Integer;

    BuiSN: string;

    BottomTimeShift: Integer;
    SecondaryBottomOn: Boolean;
    MasterBUMLeft: Boolean;
    DirectionOfMotion: Boolean; //True:  A-Forward, Flase: B-Forward
    UnitsSystem: TUnitsSystem;
    SelectChan0: TSelectChan0;

    TempAlfa: integer;

    constructor Create(ConfigFileName: string = ''); // �������� �������
    destructor Destroy; override; // �������� �������

    procedure Load;
    procedure Save;

    procedure UpdateAfterRebootParams;

    property FileNum: Integer read FFileNum write FFileNum;
    property LastThreshold: Integer read FThreshold write FThreshold;

    property NastrFolder: string read GetNastrFolder;
    property SystemFileName: string read GetSystemFileName;
    property AdjustmentVariantNum: integer read GetAdjustmentVariantNum write SetAdjustmentVariantNum;
    property AdjustmentVariantNumAfterReBoot: integer read GetAdjustmentVariantNumAfterBoot write SetAdjustmentVariantNumAfterBoot;
  end;

  TUnitsConverter = class
    private
      FKmStr, FPKStr, FMStr: string;
    public
      constructor Create; overload;
      constructor Create(KmStr, PKStr, MStr: string); overload;
      class function MMtoInch(mm: single): single;
      class function InchToMM(inch: single): single;
      class function MMtoInchStr(mm: single): string; overload;
      class function MMtoInchStr(mm: single; ShortForm: Boolean): string; overload;
      class function InchToMMStr(inch: single): string;
      class function ConvertMM(value: single): single;
      class function ConvertMMToStr(value: single): string; overload;
      class function ConvertMMToStr(value: single; NumsAfterDot: Boolean ): string; overload;
      class function ConvertMMToStrWithFoots(value: single): string;
      class function ConvertMKS2MMToStr(value: integer): string;
      class function ConvertMKS2MM(value: integer): integer;
  end;


var
  Config: TRSPConfig;

implementation



// -----[ TConfig ]-------------------------------------------------------------


constructor TRSPConfig.Create(ConfigFileName: string = ''); // �������� �������
begin
  if ConfigFileName = '' then FConfigFileName:= ExtractFilePath(ParamStr(0)) + 'AVIKON14.ini' // ��� � ���� INI �����
                         else FConfigFileName:= ConfigFileName;


  FConfig:= TINIFile.Create(FConfigFileName);
  HideNoise:= True;
  AlfaKr:= 20;
  Vl:= 5.9;
  Vt:= 3.26;
  UnitsSystem:= usRus;
end;

destructor TRSPConfig.Destroy; // �������� �������
begin
  FConfig.Free;
end;


function TRSPConfig.GetAdjustmentVariantNum: Integer;
begin
  result:= AdjustmentVarArr[0];
  {$IFDEF Avikon16_IPV}
  case SoundedScheme of
    Scheme1: result:=AdjustmentVarArr[0];
    Scheme2: result:=AdjustmentVarArr[1];
    Scheme3: result:=AdjustmentVarArr[2];
    Wheels: result:=AdjustmentVarArr[3];
  end;
  {$ENDIF}
  {$IFDEF EGO_USW}
  case SoundedScheme of
    Scheme1: result:=AdjustmentVarArr[0];
    Scheme2: result:=AdjustmentVarArr[1];
    Scheme3: result:=AdjustmentVarArr[2];
  end;
  {$ENDIF}
end;

function TRSPConfig.GetAdjustmentVariantNumAfterBoot: Integer;
begin
  result:= AdjustmentVarArr[0];
  {$IFDEF Avikon16_IPV}
  case SoundedSchemeAfterReboot of
    Scheme1: result:=AdjustmentVarArrAfterReboot[0];
    Scheme2: result:=AdjustmentVarArrAfterReboot[1];
    Scheme3: result:=AdjustmentVarArrAfterReboot[2];
    Wheels: result:=AdjustmentVarArrAfterReboot[3];
  end;
  {$ENDIF}
  {$IFDEF EGO_USW}
  case SoundedSchemeAfterReboot of
    Scheme1: result:=AdjustmentVarArrAfterReboot[0];
    Scheme2: result:=AdjustmentVarArrAfterReboot[1];
    Scheme3: result:=AdjustmentVarArrAfterReboot[2];
  end;
  {$ENDIF}
end;

function TRSPConfig.GetNastrFolder: string;
begin
  result:='';
  {$IFDEF Avikon11}
  result:='AV11';
  {$ENDIF}
  {$IFDEF Avikon12}
  result:='AV12';
  {$ENDIF}
  {$IFDEF Avikon14}
  result:='AV14';
  {$ENDIF}
  {$IFDEF Avikon14_2Block}
  result:='AV14_2B';
  {$ENDIF}
  {$IFDEF Avikon15}
  result:='AV15';
  {$ENDIF}
  {$IFDEF Avikon15RSP}
  result:='AV15RSP';
  {$ENDIF}
  {$IFDEF Avikon16_IPV}
  case SoundedScheme of
    Scheme1: result:='IPV_SCH1';
    Scheme2: result:='IPV_SCH2';
    Scheme3: result:='IPV_SCH3';
    Wheels: result:='IPV_W';
  end;
  {$ENDIF}
  {$IFDEF EGO_USW}
  case SoundedScheme of
    Scheme1: result:='EGO_USW1';
    Scheme2: result:='EGO_USW2';
    Scheme3: result:='EGO_USW3';
  end;
  {$ENDIF}
  {$IFDEF RKS_U}
  case SoundedScheme of
    Scheme1: result:='RKS_U_5';
    Scheme2: result:='RKS_U_8';
  end;
  {$ENDIF}
  {$IFDEF USK004R}
  result:='USK004R';
  {$ENDIF}
//
  {$IFDEF VMT_US}
  result:='VMT_US';
  {$ENDIF}
end;

function TRSPConfig.GetSystemFileName: string;
begin
  result:='';
  {$IFDEF Avikon11}
  result:='av11';
  {$ENDIF}
  {$IFDEF Avikon12}
  result:='av12';
  {$ENDIF}
  {$IFDEF Avikon14}
  result:='av14';
  {$ENDIF}
  {$IFDEF Avikon14_2Block}
  result:='av14_2B';
  {$ENDIF}
  {$IFDEF Avikon15}
  result:='av15';
  {$ENDIF}
  {$IFDEF Avikon15RSP}
  result:='av15RSP';
  {$ENDIF}
  {$IFDEF Avikon16_IPV}
  case SoundedScheme of
    Scheme1: result:='ipv_sch1';
    Scheme2: result:='ipv_sch2';
    Scheme3: result:='ipv_sch3';
    Wheels: result:='ipv_w';
  end;
  {$ENDIF}
  {$IFDEF EGO_USW}
  case SoundedScheme of
    Scheme1: result:='ego_usw1';
    Scheme2: result:='ego_usw2';
    Scheme3: result:='ego_usw3';
    else result:='ego_usw1';
  end;
  {$ENDIF}
  {$IFDEF RKS_U}
  case SoundedScheme of
    Scheme1: result:='rks_u_5';
    Scheme2: result:='rks_u_8';
  end;
  {$ENDIF}
  {$IFDEF USK004R}
  result:='USK_004R';
  {$ENDIF}

  {$IFDEF VMT_US}
      result:='VMT_US';
  {$ENDIF}
end;

procedure TRSPConfig.Load;
var
  I: Integer;
  Tmp: TStringList;

begin
  DecimalSeparator:= '.';

  // ��������� �������� ����� ��� �� ����� buisn.txt
  if FileExists(ExtractFilePath(Application.ExeName)+'buisn.txt') then
    begin
    end
  else BuiSN:= '99999';



  DPStep:= FConfig.ReadFloat( '�����', '��� ��', 1.8 );
  Organization:= FConfig.ReadString( '�����', 'Organization', '�������������' );

  DefNumber:= FConfig.ReadString( '�����', 'DefNumber', '' );
  RSPName:= FConfig.ReadString ('���������', 'RSPName', '');
  ShowDebugData:= FConfig.ReadBool   ('���������', 'ShowDebugData', False);
  WaitHardware:= FConfig.ReadBool   ('���������', '�������� � �������', True);
  AViewShowMM:= FConfig.ReadBool( '���������', '����� ��������� � ��', True );
  VRCHLimit:= FConfig.ReadInteger( '���������', '����������� ��� (���)', 50 );
  NetworkMode:= FConfig.ReadBool( '���������', '������ ����� ����', False );
  ServerAddress:= FConfig.ReadString( '���������', '������� ����� �������', '' );
  ConnectPort:= FConfig.ReadString( '���������', '���� ����� � ������-02', 'COM4' );
  ShowRailLen:= FConfig.ReadBool( '�������', '���������� ����� ������', True );
  RailLenProgFilename:= FConfig.ReadString( '�������', '���� � �� RailLength', '..\RailLength\Project1.exe' );
  RSPName:= FConfig.ReadString('���������', 'RSPName', '');
  FlashDrive:= FConfig.ReadInteger( 'System', 'FlashDrive', -1 );
  BViewYDirection:= TYDirection( FConfig.ReadInteger( '���������', 'BViewYDirection', 0 ) );

  ConfirmExitProgram:= FConfig.ReadBool( '�������', 'ConfirmExitProgram', True );
  ConfirmShiftNew:= FConfig.ReadBool( '�������', 'ConfirmShiftNew', True );

  FFileNum:= FConfig.ReadInteger( '�������', 'FileNum', 1);
  FThreshold:= FConfig.ReadInteger( '�������', 'Threshold', -6);

  SoundedScheme:= TSoundedScheme( FConfig.ReadInteger( 'System', 'Scheme', 0 ) );
  {$IFDEF EGO_USW}
  if ord(SoundedScheme)>2 then SoundedScheme:=Scheme1;
  {$ENDIF}

  {$IFDEF OnlyRussian}
  Language:= FConfig.ReadString( 'System', 'Language', 'Russian (�������)' );
  {$ELSE}
  Language:= FConfig.ReadString( 'System', 'Language', '' );
  {$ENDIF}
  PaintPosition:= FConfig.ReadFloat( 'System', 'PaintPosition', 2 );
  ZondPosition:= TZondPosition( FConfig.ReadInteger( 'System', 'ZondPosition', 0 ) );
  BViewPointSize:= TBViewPointSize( FConfig.ReadInteger( 'System', 'BViewPointSize', 0 ) );
  Scale:= FConfig.ReadFloat( 'System', 'Scale', 5);

  BViewAmpByColor:= FConfig.ReadBool( 'System', 'BViewAmpByColor', FALSE );
  BViewAmpBySize:= FConfig.ReadBool( 'System', 'BViewAmpBySize', FALSE );

  PainterState:= TPainterState( FConfig.ReadInteger( 'System', 'PainterState', 0) );
  PaintSprayer:= TPaintRegim( FConfig.ReadInteger( 'System', 'PaintSprayerRegim', 0) );

  {$IFnDEF PainterInterfaceOn}
  PainterState:= psAlwaysOff;
  PaintSprayer:= prAlwaysOff;
  {$ENDIF}

  RegOper:= FConfig.ReadString( 'Registration', 'Operator', '');
  RegPeregon:= FConfig.ReadString( 'Registration', 'Peregon', '' );
  RegNapr:= FConfig.ReadInteger( 'Registration', 'Napr', 0 );
  RegWayNum:= FConfig.ReadInteger( 'Registration', 'WayNum', 0 );
  RegKm:= FConfig.ReadInteger( 'Registration', 'Km', 0 );
  RegPk:= FConfig.ReadInteger( 'Registration', 'Pk', 0 );
  RegM:= FConfig.ReadInteger( 'Registration', 'M', 0 );


  // ��������� ����������� � �������� �������� �� ����������
  TrackDir:= FConfig.ReadInteger( 'Registration', 'TrackDir', 0);
  OperEng:= FConfig.ReadString( 'Registration', 'OperEng', '');
  if Length(OperEng) < 5 then
    OperEng:= '';
  Line:= FConfig.ReadString( 'Registration', 'Line', '');
  if Length(Line) < 5 then
    Line:= '';
  Chainage:= FConfig.ReadString( 'Registration', 'Chainage', '');
  TrackNum:= FConfig.ReadInteger( 'Registration', 'TrackNum', 0);
  DirEng:= FConfig.ReadInteger( 'Registration', 'DirEng', 0);
  TrackID:= FConfig.ReadInteger( 'Registration', 'TrackID', 0);

  SndVolume:= FConfig.ReadInteger( 'Sound', 'Volume', 20 );

  Tm:= FConfig.ReadInteger( 'RailType', 'Tm', 61 );

  AdjustmentVarArr[0]:= FConfig.ReadInteger( 'Adjustments', 'Config1', 0 );
  AdjustmentVarArr[1]:= FConfig.ReadInteger( 'Adjustments', 'Config2', 0 );
  AdjustmentVarArr[2]:= FConfig.ReadInteger( 'Adjustments', 'Config3', 0 );
  AdjustmentVarArr[3]:= FConfig.ReadInteger( 'Adjustments', 'Config4', 0 );

  BottomTimeShift:= FConfig.ReadInteger( 'System', 'BottomTimeShift', 5 );
  SecondaryBottomOn:= FConfig.ReadBool( 'System', 'SecondaryBottomOn', True );
  MasterBUMLeft:= FConfig.ReadBool( 'System', 'MasterBUMLeft', True );
  DirectionOfMotion:= FConfig.ReadBool( 'System', 'DirectionOfMotion', True );
  SelectChan0:= TSelectChan0(FConfig.ReadInteger( 'System', 'SelectChan0', Ord(scWPAandWPB) ) );
  ZondAmpl := FConfig.ReadInteger( 'System', 'ZondAmpl', 7 );
  if ZondAmpl>7 then ZondAmpl:=7;
  WorkSide:=FConfig.ReadBool( 'System', 'Gauge side', False ); //false/true=Left/Right


  with Colors do
  begin
    Text:= StringToColor( FConfig.ReadString('�����', '����� � �����', 'clBlack' ) );
    BackGround:= StringToColor( FConfig.ReadString('�����', '���', 'clSilver' ) );
    Border:= StringToColor( FConfig.ReadString('�����', '�����', 'clWhite' ) );
    SelectedArea:= StringToColor( FConfig.ReadString('�����', '���������', 'clWhite' ) );
    Selected:= StringToColor( FConfig.ReadString('�����', '�����', 'clWhite' ) );
    Title:= StringToColor( FConfig.ReadString('�����', '���������', 'clBlue' ) );
    AView:= StringToColor( FConfig.ReadString('�����', '��� �-���������', 'clBlack' ) );
    MainTitle_Text:= StringToColor( FConfig.ReadString('�����', '������� ��������� - �����', 'clBlue' ) );
    MainTitle_Border:= StringToColor( FConfig.ReadString('�����', '������� ��������� - ����', 'clWhite' ) );
    ButtonBackground:= StringToColor( FConfig.ReadString('�����', '��� ������', 'clWhite' ) );
  end;

  AdjustmentVarArrAfterReboot[0]:= AdjustmentVarArr[0];
  AdjustmentVarArrAfterReboot[1]:= AdjustmentVarArr[1];
  AdjustmentVarArrAfterReboot[2]:= AdjustmentVarArr[2];
  AdjustmentVarArrAfterReboot[3]:= AdjustmentVarArr[3];

  SoundedSchemeAfterReboot:= SoundedScheme;

  AirBrushConfig:= FConfig.ReadString( 'System', 'AirBrushConfig', 'default001.abp' );

  {$IFDEF Export}
  UnitsSystem:= TUnitsSystem( FConfig.ReadInteger( 'System', 'UnitsSystem', 0) );
  {$ELSE}
  UnitsSystem:= usRus;
  {$ENDIF}

  LeftSide:= FConfig.ReadInteger( 'System', 'LeftSide', 1);
end;

procedure TRSPConfig.Save;
var
  I: Integer;

begin
  DecimalSeparator:= '.';

  FConfig.WriteFloat( '�����', '��� ��', DPStep );
  FConfig.WriteString( '�����', 'Organization', Organization );

  FConfig.WriteString( '�����', 'DefNumber', Config.DefNumber );
  FConfig.WriteString ( '���������', 'RSPName', RSPName );
  FConfig.WriteBool( '���������', 'ShowDebugData', ShowDebugData );
  FConfig.WriteBool( '���������', '�������� � �������',  WaitHardware );
  FConfig.WriteBool( '���������', '����� ��������� � ��', AViewShowMM );
  FConfig.WriteInteger( '���������', '����������� ��� (���)', VRCHLimit );
  FConfig.WriteBool( '���������', '������ ����� ����', NetWorkMode );
  FConfig.WriteString( '���������', '������� ����� �������', ServerAddress );
  FConfig.WriteString( '���������', '���� ����� � ������-02', ConnectPort );
  FConfig.WriteBool( '�������', '���������� ����� ������', ShowRailLen );
  FConfig.WriteString( '�������', '���� � �� RailLength', RailLenProgFilename );
  FConfig.WriteInteger( 'System', 'FlashDrive', FlashDrive );
  FConfig.WriteInteger( '���������', 'BViewYDirection', Ord( BViewYDirection ) );

  FConfig.WriteInteger( 'System', 'Scheme', Ord( SoundedScheme ) );
  FConfig.WriteString( 'System', 'Language', Language );
  FConfig.WriteFloat( 'System', 'PaintPosition', PaintPosition );
  FConfig.WriteInteger( 'System', 'ZondPosition', Ord( ZondPosition ) );
  FConfig.WriteInteger( 'System', 'BViewPointSize', Ord( BViewPointSize ) );

  FConfig.WriteInteger( 'System', 'PainterState', Ord( PainterState ) );
  FConfig.WriteInteger( 'System', 'PaintSprayerRegim', Ord( PaintSprayer ) );

  FConfig.WriteBool( 'System', 'BViewAmpByColor',  BViewAmpByColor );
  FConfig.WriteBool( 'System', 'BViewAmpBySize', BViewAmpBySize );

  FConfig.WriteBool( '�������', 'ConfirmExitProgram', ConfirmExitProgram );
  FConfig.WriteBool( '�������', 'ConfirmShiftNew', ConfirmShiftNew );
  FConfig.WriteInteger( '�������', 'FileNum', FFileNum);
  FConfig.WriteInteger( '�������', 'Threshold', FThreshold);

  FConfig.WriteFloat( 'System', 'Scale', Scale);

  FConfig.WriteString( 'Registration', 'Operator', RegOper );
  FConfig.WriteString( 'Registration', 'Peregon', RegPeregon );
  FConfig.WriteInteger( 'Registration', 'Napr', RegNapr );
  FConfig.WriteInteger( 'Registration', 'WayNum', RegWayNum );
  FConfig.WriteInteger( 'Registration', 'Km', RegKm );
  FConfig.WriteInteger( 'Registration', 'Pk', RegPk );
  FConfig.WriteInteger( 'Registration', 'M', RegM );

  // ��������� ����������� � �������� �������� �� ����������
  FConfig.WriteInteger( 'Registration', 'TrackDir', TrackDir );
  FConfig.WriteString( 'Registration', 'OperEng', OperEng );
  FConfig.WriteString( 'Registration', 'Line', Line );
  FConfig.WriteString( 'Registration', 'Chainage', Chainage );
  FConfig.WriteInteger( 'Registration', 'TrackNum', TrackNum );
  FConfig.WriteInteger( 'Registration', 'DirEng', DirEng );
  FConfig.WriteInteger( 'Registration', 'TrackID', TrackID );


  FConfig.WriteInteger( 'RailType', 'Tm', Tm );

  FConfig.WriteInteger( 'Sound', 'Volume', SndVolume );

  FConfig.WriteInteger( 'Adjustments', 'Config1', AdjustmentVarArr[0] );
  FConfig.WriteInteger( 'Adjustments', 'Config2', AdjustmentVarArr[1] );
  FConfig.WriteInteger( 'Adjustments', 'Config3', AdjustmentVarArr[2] );
  FConfig.WriteInteger( 'Adjustments', 'Config4', AdjustmentVarArr[3] );

  FConfig.WriteInteger( 'System', 'BottomTimeShift', BottomTimeShift );
  FConfig.WriteBool( 'System', 'SecondaryBottomOn', SecondaryBottomOn );
//
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US) }
  FConfig.WriteBool( 'System', 'MasterBUMLeft', MasterBUMLeft );
  FConfig.WriteBool( 'System', 'DirectionOfMotion', DirectionOfMotion );
  FConfig.WriteInteger( 'System', 'SelectChan0', Ord(SelectChan0) );
{$IFEND}

  FConfig.WriteInteger( 'System', 'ZondAmpl', ZondAmpl );

{$IFDEF USK004R}
  FConfig.WriteBool( 'System', 'Gauge side', WorkSide );
{$ENDIF}

  with Colors do
  begin
    FConfig.WriteString('�����', '����� � �����', ColorToString(Text) );
    FConfig.WriteString('�����', '���', ColorToString(BackGround) );
    FConfig.WriteString('�����', '�����', ColorToString(Border));
    FConfig.WriteString('�����', '���������', ColorToString(SelectedArea));
    FConfig.WriteString('�����', '�����', ColorToString(Selected));
    FConfig.WriteString('�����', '���������', ColorToString(Title));
    FConfig.WriteString('�����', '��� �-���������', ColorToString(AView ) );
    FConfig.WriteString('�����', '������� ��������� - �����', ColorToString( MainTitle_Text ) );
    FConfig.WriteString('�����', '������� ��������� - ����', ColorToString( MainTitle_Border ) );
    FConfig.WriteString('�����', '��� ������', ColorToString( ButtonBackground ) );
  end;

  FConfig.WriteString( 'System', 'AirBrushConfig', AirBrushConfig );

  {$IFDEF Export}
  FConfig.WriteInteger( 'System', 'UnitsSystem', Ord( UnitsSystem ) );
  {$ELSE}
  FConfig.WriteInteger( 'System', 'UnitsSystem', Ord( usRus ) );
  {$ENDIF}

  FConfig.WriteInteger( 'System', 'LeftSide', LeftSide);
  FConfig.UpdateFile;
end;


procedure TRSPConfig.SetAdjustmentVariantNum(Value: Integer);
begin
  {$IFDEF Avikon16_IPV}
  case SoundedScheme of
    Scheme1: AdjustmentVarArr[0]:= Value;
    Scheme2: AdjustmentVarArr[1]:= Value;
    Scheme3: AdjustmentVarArr[2]:= Value;
    Wheels: AdjustmentVarArr[3]:= Value;
  end;
  {$ELSE}
   {$IFDEF EGO_USW}
  case SoundedScheme of
    Scheme1: AdjustmentVarArr[0]:= Value;
    Scheme2: AdjustmentVarArr[1]:= Value;
    Scheme3: AdjustmentVarArr[2]:= Value;
  end;
   {$ELSE}
  AdjustmentVarArr[0]:= Value;
   {$ENDIF}
  {$ENDIF}
end;

procedure TRSPConfig.SetAdjustmentVariantNumAfterBoot(Value: Integer);
begin
  {$IFDEF Avikon16_IPV}
  case SoundedSchemeAfterReboot of
    Scheme1: AdjustmentVarArrAfterReboot[0]:= Value;
    Scheme2: AdjustmentVarArrAfterReboot[1]:= Value;
    Scheme3: AdjustmentVarArrAfterReboot[2]:= Value;
    Wheels: AdjustmentVarArrAfterReboot[3]:= Value;
  end;
  {$ELSE}
   {$IFDEF EGO_USW}
  case SoundedSchemeAfterReboot of
    Scheme1: AdjustmentVarArrAfterReboot[0]:= Value;
    Scheme2: AdjustmentVarArrAfterReboot[1]:= Value;
    Scheme3: AdjustmentVarArrAfterReboot[2]:= Value;
  end;
   {$ELSE}
  AdjustmentVarArrAfterReboot[0]:= Value;
  {$ENDIF}
  {$ENDIF}
end;

procedure TRSPConfig.UpdateAfterRebootParams;
begin
  AdjustmentVarArr[0]:= AdjustmentVarArrAfterReboot[0];
  AdjustmentVarArr[1]:= AdjustmentVarArrAfterReboot[1];
  AdjustmentVarArr[2]:= AdjustmentVarArrAfterReboot[2];
  AdjustmentVarArr[3]:= AdjustmentVarArrAfterReboot[3];

  SoundedScheme:= SoundedSchemeAfterReboot;
end;

function GetModuleFileNameStr(Instance: THandle): string;
var
  buffer: array [0..MAX_PATH] of Char;
begin
  GetModuleFileName( Instance, buffer, MAX_PATH);
  Result := buffer;
end;

constructor TUnitsConverter.Create;
begin
  FKmStr:= '';
  FPKStr:= '';
  FMStr:= '';
end;

class function TUnitsConverter.ConvertMKS2MM(value: integer): integer;
begin
  result:= Max(0, MIN(255, Round(MKS2MM(value, Config.TempAlfa, true))));
end;

class function TUnitsConverter.ConvertMKS2MMToStr(value: integer): string;
var
  valueMM: integer;
begin
  valueMM:= Max(0, MIN(255, Round(MKS2MM(value, Config.TempAlfa, true))));
  result:= IntToStr(valueMM);
end;

class function TUnitsConverter.ConvertMM(value: single): single;
begin
  case Config.UnitsSystem of
    usRus, usMetricTurkish: result:= value;
    usImperial, usMetricA, usMetricB: result:= MMtoInch(value);
  end;
end;

class function TUnitsConverter.ConvertMMToStr(value: single): string;
begin
  result:= ConvertMMToStr(value, false);
end;

class function TUnitsConverter.ConvertMMToStr(value: single;
  NumsAfterDot: Boolean): string;
begin
  case Config.UnitsSystem of
    usRus,usMetricTurkish:
            if NumsAfterDot then
             result:= Format('%.2f', [value])
           else
             result:= Format('%.0f', [value]);
    usImperial, usMetricA, usMetricB: result:= MMtoInchStr(value, NumsAfterDot);
  end;
end;

class function TUnitsConverter.ConvertMMToStrWithFoots(value: single): string;
var
  inch: single;
  foot: integer;
begin
  inch:= MMtoInch(value);
  foot:= Trunc(inch / 12);
  inch:= inch - (12*foot);
  if foot = 0 then
    result:= Format('%3.2f "', [inch])
  else
    result:= Format('%d ft  %3.2f "', [foot, inch]);
end;

constructor TUnitsConverter.Create(KmStr, PKStr, MStr: string);
begin
  FKmStr:= KmStr;
  FPKStr:= PKStr;
  FMStr:= MStr;
end;

class function TUnitsConverter.InchToMM(inch: single): single;
begin
  result:= inch*25.4;
end;

class function TUnitsConverter.InchToMMStr(inch: single): string;
begin
  result:= IntToStr(Round(InchToMM(inch)));
end;

class function TUnitsConverter.MMtoInch(mm: single): single;
begin
  result:= mm/25.4;
end;

class function TUnitsConverter.MMtoInchStr(mm: single;
  ShortForm: Boolean): string;
var
  inch: single;
begin
  inch:= MMtoInch(mm);
  if ((inch>=10) or (inch<=-10)) or (ShortForm) then
    result:= Format('%3.1f', [inch])
  else
    result:= Format('%3.2f', [inch]);
end;

class function TUnitsConverter.MMtoInchStr(mm: single): string;
begin
  MMtoInchStr(mm, false);
end;



initialization
  Config:= TRSPConfig.Create(ExtractFilePath(Application.ExeName) + 'AVIKON14.ini');
  Config.Load;

finalization
  Config.UpdateAfterRebootParams;
  Config.Save;
  Config.Free;
  Config:= nil;

end.

