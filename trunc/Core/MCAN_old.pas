unit MCAN;

interface

{$DEFINE UseVRCHCurve}
{ $DEFINE AKPSimulation}

uses Forms, Classes, ExtCtrls, ConfigObj, Types,
      PublicData, PublicFunc,
      DeviceInterfaceUnit, InterfaceCAN, SoundControlUnit, TextFilesIO;

const
  BVBufferCount = 1000;
  WriteBufferCount = 500;
  AVBuffSize = 232;


type
  AViewArr = array of Byte;
  TAVBuff = record
    AV: array[0..AVBuffSize - 1] of Byte;
    Pos:  Integer;
    AmaxK0: Byte;
    TmaxK0: Byte;
    Tmax: Single;
    Amax: Byte;
  end;

  TBVBuffEl = packed record
    Napr: ShortInt;
    DataCount: Byte;
    Data: array[0..319] of Byte; // ���-�� ���� ������� (1�), �����-���� (1�: ��.���==����), ��������� ����� ������� (���. + ����., ����.)
  end;

  TBVBuff = array [0..BVBufferCount-1] of TBVBuffEl;
  TWBuff = array[0..WriteBufferCount-1] of byte;

  TMBuff = packed record
    BlockCount: array[0..1] of Byte;
    ByteCount: Byte;
    Data: array[0..319] of Byte; // ���-�� ���� ������� (1�), �����-���� (1�: ��.���==����), ��������� ����� ������� (���. + ����., ����.)
  end;
  PMBuff = ^TMBuff;

  PAVBuff = ^TAVBuff;

  TAdjustParamChange = record
     Flag: Boolean;
     CurValue: Integer;
  end;

  TAdjustParams = ( apKy, apATT, apStrobSt, apStrobEd, apVRCH, apTwoTp);

  TAdjustChanges = array[apKy..apTwoTp] of TAdjustParamChange;

  TMCallBack = procedure( MBuff: PMBuff ) of object;

  // �������� CAN: ������ ������ � ������ ������.
  TCAN = class
  private
    KT: Byte;
    WrLog: Log_proc;
    UseLog: Boolean; // ���. ���?

    TestResult: Word;

    AVInBuff  : PAVBuff;  // �� -> �����
    AVOutBuff : PAVBuff;  // ����� -> ������������

    MBuff: TMBuff;
    MCallBack: TMCallBack;
    MReq: Boolean;

    isASDOn: Boolean;
    HandKanal: Integer;

    WinMessage: WinMessage_proc;

    procedure OnBlockReserved( Bl: TBlock );
    procedure OnSendError;

    function SetPar(Takt, ParOfs, ParV : Byte) : Boolean;                     // ���������� �������� ��������� �����

  protected
  public
    Device: TDevice;
    SendError: Boolean;

    ASD: TASD;
    AK: TAK;

    // ��������� B-scan.
    BVBuff: TBVBuff;
    BVWritePos: Integer; // ��������� �� ������� ������������ ������ ������ B-���� ( �.�. ��������� ���������� ������, ������� ��� ��������: BVWriteBuff-1 )
    BVReadPos: Integer;  // ��������� �� ������, ��� �� ���������� ������ ������ B-���� ( �.�. ���������� ���� �������� � BVReadPos )



    CanError: Boolean;

    constructor Create( WrLog_ : Log_proc; WinMessage_: WinMessage_proc; UseLog_ : Boolean = false);
    destructor Destroy; override;

    function BVBuffCount: Integer;
    function Test: Word;

    // �����-�������� �������.
    function SendBL( Bl : TBl) : Boolean; overload;                           // ������� �������
    function SendBL( B1: Byte; B2: Byte = 0; B3: Byte = 0; B4: Byte = 0; B5: Byte = 0; B6: Byte = 0; B7: Byte = 0; B8: Byte = 0; B9: Byte = 0; B10: Byte = 0 ) : Boolean; overload; // ������� �������
    function SendZero: Boolean;  // ������� ������-��-�������� �������.

    // ��������� ������ �� �������.
    function GetAV( var AV: PAVBuff ): Boolean;
    function GetBV( var BV: TBVBuff ): Integer; // �������� �-���������.
    procedure GetMV( CallBack: TMCallBack ); // �������� M-���������.
//    function GetASD( var OutASD: TASD; var OutAK: TAK ): Boolean;

    // ���������������� ���������� �����.
    function InitCikl(KT_ : Byte): Boolean;                            // ������������ ����
    function SetTakt(T, GenL, GenR, ResL, ResR, Tmn, Tmax : Byte; WSA: Word ): Boolean;
    function SetVRU(N, Takt, T1, A1, T2, A2: Byte) : Boolean;                 // ���
    function SetVRU_Point(N, Takt, T, A: Byte) : Boolean;                     // ���
    function SetStrob(N, Takt, Nstr, T1, T2, A, Regim : Byte) : Boolean;      // ��������� �������
    function Set2TP(N, Takt : Byte; l_2TP, r_2TP : Single) : Boolean;         // ����� � ������

     // ���������� ����������� �, B.
    function StartAview(N, Takt, Tst, M : Byte; WorkStrob: Byte; Single: Boolean = False; OnlyMax : Boolean = False): Boolean;  // ��������� A-���������
    function StopAview : Boolean;                                             // ���������� A-���������

    // ���-���� ������� �����.
    function Start : Boolean;                                                 // ��������� ������ ����
    function Stop  : Boolean;                                                 // ���������� ������ ����
    function Reset ( Wait : Boolean = True ) : Boolean;                       // ����������� ������ ����
    function ImitDP( isOn : Boolean ) : Boolean;                              // ���./����. ����. ������� ����
    function ASDTurnOn( HandKanal_: Integer ): Boolean;                           // ���. ���
    procedure ASDTurnOff;                                                     // ����. ���
    function ASDTurnOn_BUM: Boolean;

  end;

  TTakt = class;

  TDirection = ( dForward = 0, dBack = 1 );

//  TAdjRailTypeMode = ( amNone = 0, am0 = 1, am45 = 2 ); // ����� ��������� �� ��� ������.

  // ������ ������: ���� ������ ������������� ������ ������.
  TKanal = class
    private
      FATT: Byte;      // 0..80 �� (��)
      FVRCH: Byte;      // ��� (���)
      FTwoTP: Single;    // (���)
    protected
         procedure SetATT( ATT: Byte );
         procedure SetVRCH( VRCH: Byte );
         procedure SetTwoTP( TwoTP: Single );
    public
      // ����������� �� ����������������� ������� ( + ���� �������� ���� �� ������)
      Ky               : Integer;   // �������� ���������������� (��)
      Ky2              : Integer;   // �������� ���������������� (��) ������ ����� ��� (������ ��� ������� 6 � 7)
      VRCH2            : Integer;   // ������� ���������� ��� (������ ��� ������� 6 � 7)
      Str_st           : Byte;      // ������� ����� (���)
      Str_en           : Byte;
      NR_Start         : Byte;      // NoiseReduction: �������������� - ������ �������.
      NR_Length        : Byte;      // NoiseReduction: �������������� - ����� �������.
      NR_Level         : ShortInt;  // NoiseReduction: �������������� - ������� ������� ������������ ATT.
      AK_Level         : Byte;      // ������� �������� ��.

      // ����������� �� ���������� �������
      Reg              : Byte;      // �����: 0 - ��������������, 1 - ������.
      Nit              : Byte;      // ���� ������ (�, ��������������, �����)
      Num              : Integer;   // ����� ������ (��������� � �������� ������� ������� � �������)
      TktN             : Byte;      // ����� �����. ���� ����� ���������� ���: Takts[Nit, TktNum] (���� Reg=0) ��� HTakts[TktNum] (���� Reg=1)
      Takt             : TTakt;     // ������ �� ����.
      Str_st_r         : Integer;   // ������������� ������� ����� (��� ��)
      Str_en_r         : Integer;
      Ky_r             : Integer;   // ������������� �������� ���������������� (A-view)
      AdjRailType      : Boolean;   // ����������� �� ����� �� ��� ������.

      WaveDirection    : TDirection; // ����������� ����� ��.
      BViewDelayMin    : Integer;    // ������ �-��������� (���)
      BViewDelayMax    : Integer;    // ����� �-��������� (���)
      KanAK            : Integer;    // ����� ������, �� �������� ����� �������� �� (�� ��������� � Num ��� ���������� � ��. ��������� �������)
      InverseASD       : Boolean;    // ���������, ��� ���� ������������� ������ ��� (������������ ��� ������� ������)

      // �������������� (�� �����������)
      NStr_st          : Byte;      // ����������� ����� (��� ��)
      NStr_en          : Byte;
      NStr2TP_st       : Byte;      // ����������� ����� ��� 2 TP.
      NStr2TP_en       : Byte;
      AdjDate          : TDateTime; // ���� ��������� ���������

      // ������ ������ ��� ���������� � ���� ������������� �������� ����� �� ���������
      // � ����������� �������� � ����� �����������
      AdjustChanges: TAdjustChanges;

      property ATT: Byte read FATT write SetATT;  // (��� �������� ����������� �� ����������������� �������)
      property VRCH: Byte read FVRCH write SetVRCH;
      property TwoTP: Single read FTwoTP write SetTwoTP;


      function AdjustKy( N: Byte ): Boolean;

      function SetConfig_KA(S : string): Boolean;    // ����. ��������� ������ (�� ������)
      function SetConfig_KAN(S : string): Boolean;   // ����. ���������������� ������ (�� ������)
      function GetConfig_KAN: string;                 // ������ (��� ����������) ���������������� ������ (� ���� ������)
      procedure InitNStr;
      procedure StartAView;
  end;

  TSingleKanArray = array of TKanal;
  PSingleKanArray = ^TSingleKanArray;

  TKanArray = array[0..1] of TSingleKanArray;
  PKanArray = ^TKanArray;


  TFloatPoint =  record    X: Single;    Y: Single;  end;

  TVRCHCurve = record
    Curve: array[0..231] of TFloatPoint;
    Count: Byte;
  end;


  // ������ �����: ���� ������ ������������� ����� ������ � ������� ������.
  TTakt = class
    private
      VRCHCurve        : TVRCHCurve;          // ������ ��� (������ ������ �����: T <= VRCH)
      _VRCHCurveA_     : Single;    // ����. ������� ���.
      procedure GetNStrob(out St, En : Byte);
      procedure UpdateVRCH(A: Single);
    protected
      function GetAViewTimeMax: Integer;
    public
      // ����������� �� ���������� �������
      Reg              : Byte;      // �����: 0 - ��������������, 1 - ������.
      Num              : Integer;   // ����� ����� (��������� � �������� ������� ������� � �������)
      InstallNum       : Integer;   // ����� ����� ��� �������� ��� � ������ ���� (��������� � Num, ����� �������)
      ConfigNit        : Integer;   // ����, ���������� �� ������� ������ (�������� ���� �������� � N_res)

      SinhTakt         : TTakt;
      SinhTaktNumber   : Integer;

      Gen, Res         : Byte;      // 0..7 (��� ��)
      Gen_pep, Res_pep : string;    // �������� ��� (����. 2.2) (A-view)
      Method           : string;    // ����� (����, ���, ���, ����, ������)
      Alfa             : Integer;   // ���� ����� ���.
      Ten              : Byte;      // ������������ �����.
      Kan1             : ShortInt;  // ����� ������� (������) ������
      Kan2             : ShortInt;  // ����� ������� (�������) ������  ( -1 - ��� )
      Kanal1           : TKanal;    // �������� �� ������ (�����) �����
      Kanal2           : TKanal;    // �������� �� ������ (�� ���� ������) �����
      BStr_st          : Byte;      // ����� B-��������� (��� ��)
      BStr_en          : Byte;
      AV_mash          : Byte;      // ������� �-����: ����� ����� ����� ��������� ��������� �-���� � ������� ����� ����������� (*0.1���)
      AKStr_st         : Byte;
      AKStr_en         : Byte;
      DelayFactor      : Byte;      // ��������� �������� (��� ������� ������ = 3, ����� 1)

      // �������������� (�� �����������)
      N_res            : Byte;      // �������� ����� ���� (�������������� � FinishConfig)
      ZonaM            : Word;      // ��������� ���� �����
      UseZM            : Boolean;
      VRCH             : TVRCH;     // ����� ��� (���)

      VRCH2Level       : Integer;
      VRCH2Time        : Byte;


      property VRCHCurveA: Single read _VRCHCurveA_ write UpdateVRCH;    // ����. ������� ���.
      property AViewTimeMax: Integer read GetAViewTimeMax;

      function GetWSA(KT: Byte): Word;
      function SetConfig(S : string): Boolean;    // ����. ����. ����� (S - ������ ����� RSP.TK)
      function FinishConfig: Boolean;
      function Install(CAN : TCAN; SReg: Byte): Boolean;         // �������� ������ ��� ������ �������� ����!
      function Install2TP(CAN : TCAN): Boolean;                  // ���������� ������ ��� �������� ����.
      function InstallVRU(CAN : TCAN; SReg: Byte): Boolean;      // ���������� ��� ������ ����.
      function InstallStrob(CAN : TCAN; SReg : Byte; Adj2TPMode: Boolean = False ): Boolean;   // ���������� ��� ������ ����.
  end;


  TSingleTaktArray = array of TTakt;
  PSingleTaktArray = ^TSingleTaktArray;

  TTaktArray = array[0..1] of TSingleTaktArray;
  PTaktArray = ^TTaktArray;


  TKanConfig = class
    private
      CurrentUserConfig: Integer;
      UserConfigList: TStringList;
      CodePos: Integer;
      CodeMode: Integer;

      function Code_KAN( Snum : Integer; var S: string): Boolean;
      function Decode_KAN(Snum: Integer; S: string): Boolean;
      function Decode_KA(Snum: Integer; S: string): Boolean;
      function Decode_TK( Snum: Integer; S: string): Boolean;
    public
      _Takts:  PTaktArray;
      _Kanals: PKanArray;
      _HTakts: PSingleTaktArray;
      _HKanals: PSingleKanArray;

      constructor Create;
      function LoadSystemConfig: Boolean;
      function LoadUserConfig(ConfigID: Integer): Boolean;
      function SaveUserConfig(ConfigID: Integer): Boolean;
      function AddNewConfig(Name: string): Boolean;
      procedure InitNStr;
      function FinishConfig: Boolean;
      property ConfigList: TStringList read UserConfigList;
  end;


var
  AllOff: Boolean;
  KConf: TKanConfig;
  Takts:  TTaktArray;
  Kanals: TKanArray;
  HTakts: TSingleTaktArray;
  HKanals: TSingleKanArray;
  CAN: TCAN;


implementation

uses Windows, Dialogs, SysUtils, Math;

// -----------------------------------------------------------------------------
// --------------------  ������ ������������ ������� � ������  -----------------

constructor TKanConfig.Create;
var
  F: TSearchRec;
begin

  _Takts:= @Takts;
  _Kanals:= @Kanals;
  _HTakts:= @HTakts;
  _HKanals:= @HKanals;

  SetLength( _Takts^[0], 0 );
  SetLength( _Takts^[1], 0 );
  SetLength( _Kanals^[0], 0 );
  SetLength( _Kanals^[1], 0 );
  SetLength( _HTakts^, 0 );
  SetLength( _HKanals^, 0 );

  UserConfigList:= TStringList.Create;

  chdir( ExtractFilePath(Application.ExeName) );
  chdir('NASTR');

  // �������� ����� �� ������� ������ � �����������
  if FindFirst('*.kan', faAnyFile, F) = 0 then
    begin
      repeat
        UserConfigList.Add(F.Name);
      until FindNext(F) <> 0;
      FindClose(F);
    end;

  //UserConfigList.Add('���������.KAN');
  CurrentUserConfig:= 0;
end;

function TKanConfig.LoadUserConfig(ConfigID: Integer): Boolean;
var
  FN: string;
  Code: Byte;
  n, i: Integer;
  F: TSearchRec;
  S: string;

  sr: TSearchRec;
  FileAttrs: Integer;

begin
  Result:= False;
  CurrentUserConfig:=ConfigID;
  // ������� ������ ����� � �����������
  chdir( ExtractFilePath(Application.ExeName) );
  chdir('NASTR');

  {// �������� ����� �� ������� ������ � �����������
  if FindFirst('*.kan', faAnyFile, F) = 0 then
    begin
      repeat
        UserConfigList.Add(F.Name);
      until FindNext(F) <> 0;
      FindClose(sr);
    end; }

  // ���� �� ������ ����� ��������� �� ������� �� ������
  if CurrentUserConfig > UserConfigList.Count-1 then CurrentUserConfig:= -1;
  if CurrentUserConfig = -1 then Exit;

  // ������� ��� �� ��������� ������ ���������
  FN:= UserConfigList[ CurrentUserConfig ];

  Code:= LoadTextFile( FN, Decode_KAN );
  If Code <> 0 then
  begin
    WrLog( DecodeLoadError(FN, Code) );
    Exit;
  end;

  Result:= True;
end;

function TKanConfig.FinishConfig: Boolean;
var
  n, i: Integer;

begin
  Result:= False;

  // ������ ����� � ������.
  for n:=0 to 1 do
  for i:=0 to High( _Takts^[n] ) do
  if _Takts^[n, i] <> nil then
  if not _Takts^[n, i].FinishConfig then Exit;

  for i:=0 to High( _HTakts^ ) do
  if not _HTakts^[i].FinishConfig then Exit;

  // ������� ����������� ������.
  InitNStr;

  Result:= True;

end;


function TKanConfig.SaveUserConfig(ConfigID: Integer): Boolean;
begin
  Result:= False;
  CurrentUserConfig:=ConfigID;
  if CurrentUserConfig = -1 then
  begin
    Result:= True;
    Exit;
  end;
  chdir( ExtractFilePath(Application.ExeName) );
  chdir('NASTR');

  CodeMode:= 0;
  CodePos:= 0;
  SaveTextFile( UserConfigList[ CurrentUserConfig ], Code_KAN );
  Result:= True;
end;

function TKanConfig.LoadSystemConfig: Boolean;
const
  TK_FN  =  'RSP.tk';
  KA_FN  =  'RSP.ka';

var
  Code: Byte;

begin
  Result:= False;
  WrLog('Loading config tables...');
  chdir( ExtractFilePath(Application.ExeName) );
  {$I-}
  chdir('SYSTEM');
  if IOResult <> 0  then Exit;
  Code:= LoadTextFile( TK_FN, Decode_TK );
  if Code <> 0 then
  begin
    WrLog( DecodeLoadError(TK_FN, Code) );
    Exit;
  end;
  Code:= LoadTextFile( KA_FN, Decode_KA );
  if Code <> 0 then
  begin
    WrLog( DecodeLoadError(KA_FN, Code) );
    Exit;
  end;

  Result:= True;
end;

function TKanConfig.Code_KAN( Snum : Integer; var S: string ) : boolean;
var
  K: TKanal;
  Pos: Integer;


  procedure CODE( _KanArr: TSingleKanArray );
  begin
    K:= nil;
    while CodePos <= High( _KanArr ) do
    begin
      K:= _KanArr[ CodePos ];
      Inc( CodePos );
      if K <> nil then Break;
    end;
    if K = nil then
    begin
      inc( CodeMode );
      CodePos:= 0;
    end;
  end;

begin
  Result:= False;

  if CodeMode = 0 then
  begin
    S:= '�����          ����           �����          ���            2TP            Ky             ATT            ���.���.       ���.���.       ��-������      ��-�����       ��-�������     ��-����        ���� �����.    ��2     ����. ���';
    CodeMode:= 1;
    CodePos:= 0;
    Result:= True;
    Exit;
  end;

  if CodeMode = 1 then CODE( _Kanals^[0] );
  if CodeMode = 2 then CODE( _Kanals^[1] );
  if CodeMode = 3 then CODE( _HKanals^ );
  if CodeMode = 4 then Exit;

  S:= K.GetConfig_KAN;
  Result:= True;
end;

function TKanConfig.Decode_KAN(Snum: Integer; S: string): Boolean;
var
  n, i: Integer;

begin
  Result:= True;
  if (Snum = 0) then Exit;

  for n:=0 to 1 do
    for i:=0 to High( _Kanals^[n] ) do
      if _Kanals^[n,i] <> nil then
        if _Kanals^[n,i].SetConfig_KAN( S ) then Exit;

  for i:=0 to High( _HKanals^ ) do
    if _HKanals^[i] <> nil then
      if _HKanals^[i].SetConfig_KAN( S ) then Exit;

  Result:= False;
end;

function TKanConfig.Decode_KA(Snum: Integer; S: string): Boolean;
var
  K: TKanal;
  KA: PSingleKanArray;
  HighKA: Integer;
  i: Integer;

begin
  Result:= True;
  if (Snum = 0) then Exit;
  K:= TKanal.Create;
  if K.SetConfig_KA( S ) then
  begin
    Result:= True;
    if K.Reg = 0 then KA:= @_Kanals^[K.Nit] else KA:= _HKanals;
    HighKA:= High( KA^ );
    if K.Num > HighKA then SetLength( KA^, K.Num + 1 );
    for i:= HighKA + 1 to High( KA^ ) - 1 do
      KA^[i]:= nil;    // ������������ nil �������������� (�����������) ������, �� ���� ��� ��������, ���� � ������� ������� ���� ����.
    KA^[ K.Num ]:= K;
  end else
  begin
    Result:= False;
    K.Free;
  end;
end;

function TKanConfig.Decode_TK(Snum: Integer; S: string): Boolean;
var
 T: TTakt;
 TA: PSingleTaktArray;
 HighTA: Integer;
 i: Integer;

begin
  Result:= True;
  if (Snum = 0) then Exit;
  T:= TTakt.Create;
  if T.SetConfig( S ) then
  begin
    Result:= True;
    if T.Reg = 0 then TA:= @_Takts^[T.ConfigNit] else TA:= _HTakts;
    HighTA:= High( TA^ );
    if T.Num > HighTA then SetLength( TA^, T.Num + 1 );
    for i:= HighTA + 1 to High( TA^ ) - 1 do
      TA^[i]:= nil;    // ������������ nil �������������� (�����������) �����, �� ���� ��� ��������, ���� � ������� ������ ���� ����.
    TA^[ T.Num ]:= T;
  end else
  begin
    Result:= False;
    T.Free;
  end;
end;

procedure TKanConfig.InitNStr;
var
  n, i: Integer;
begin
  for n:=0 to 1 do
    for i:=0 to High( Kanals[n] ) do
      if Kanals[n, i] <> nil then Kanals[n, i].InitNStr;
  for i:=0 to High( HKanals ) do
    if HKanals[i] <> nil then HKanals[i].InitNStr;
end;

// -----------------------------------------------------------------------------
// -----------------  ������ ������ � ��������� ��������� CAN  -----------------


function TCAN.SendBl(Bl: TBl) : Boolean;
begin
  Result:= Device.PutBlock( Bl );
end;


function TCAN.SendBl( B1: Byte; B2: Byte = 0; B3: Byte = 0; B4: Byte = 0; B5: Byte = 0; B6: Byte = 0; B7: Byte = 0; B8: Byte = 0; B9: Byte = 0; B10: Byte = 0 ): Boolean;
var
  Bl : TBl;
begin
  Bl[1]:=B1;  Bl[2]:=B2;
  Bl[3]:=B3;  Bl[4]:=B4;
  Bl[5]:=B5;  Bl[6]:=B6;
  Bl[7]:=B7;  Bl[8]:=B8;
  Bl[9]:=B9;  Bl[10]:=B10;
  SendBl:= SendBl(Bl);
end;

function TCAN.SendZero: Boolean;
begin
  Result:= SendBL(2, 1, 0, 0,0,0,0,0,0,0);
end;

{function TCAN.GetBL(var Bl: TBl; Fast : Boolean = False): Boolean;
var
 Bt : Byte;
 B : Boolean;
 Cou,
 i : Byte;
 T : DWord; // TimeOut
begin
  If AllOff then
  begin
    GetBL:= ( random > 0.5 );
    Exit;
  end;

  i:=0;
  Cou:=10;
  GetBL:=false;
  repeat
    T:=GetTickCount;
    repeat
      B:=COM.GetByte(Bt);
      If not B and Fast then exit;
      If B then break;
    until GetTickCount-T > 2000;
    If not B then exit;
    inc(i);
    Bl[i]:=Bt;
    If i=2 then Cou:=Bt and $F + 2;
    If Cou>10 then
     begin
      If UseLog then
        WrLog( 'BAD BLOCK (cou=' + inttostr(Cou) + ').' );
      exit;
     end;
  until i>=Cou;

  GetBl:=true;
  If UseLog then
    WrLog( 'R: ' + Bl2Str(Bl) );
end;
 }



// -----------------------------------------------------------------------------
// ----------------------  ������ ������ � ���������� CAN  ---------------------


constructor TCAN.Create( WrLog_ : Log_proc; WinMessage_: WinMessage_proc; UseLog_ : Boolean = false);
var
  R: Byte;

begin
  // ������.
  new( AVInBuff );
  new( AVOutBuff );
  isASDOn:= False;
  BVReadPos:=0;
  BVWritePos:=-1;
  MReq:= False;


  WinMessage:= WinMessage_;
  UseLog:= UseLog_;
  WrLog:=  WrLog_;

  Device:= TDeviceAKP.Create;
  Device.OnBlockReserved:= OnBlockReserved;
  Device.AddLogProc:= WrLog;
  Device.OnSendError:= OnSendError;

  SendError:= False;

end;

destructor TCAN.Destroy;
begin
  Stop;
  Device.Free;
  inherited;
  UseLog:=False;
end;

function TCAN.GetAV( var AV: PAVBuff ): Boolean;
begin
{  AV:= AVOutBuff;
  Result:= TRUE; Exit; }

  if AVOutBuff.Pos <> 232 then
  begin
    Result:= False;
  end else
  begin
    AVOutBuff.Pos:= 0;
    AV:= AVOutBuff;
    Result:= True;
  end;
end;

function TCAN.GetBV( var BV: TBVBuff ): Integer;
var
  Cou: Integer;

begin


  if ( BVReadPos = BVWritePos ) or ( BVWritePos = -1 ) then
  begin
    Result:= 0;
    Exit;
  end else
  begin
 {
    Result:= 1;
    BV[0].Napr:= 1;
    BV[0].DataCount:= 4;
    BV[0].Data[0]:= 3;
    BV[0].Data[1]:= 0;
    BV[0].Data[2]:= 30;
    BV[0].Data[3]:= 6 shl 4;
}


    if BVReadPos > BVWritePos then
    begin
      Cou:= BVBufferCount - BVReadPos;
      Move( BVBuff[BVReadPos], BV[0], Cou * SizeOf( TBVBuffEl ) );
      Move( BVBuff[0], BV[Cou], BVWritePos * SizeOf( TBVBuffEl ) );
      Result:= Cou + BVWritePos;
    end else
    begin
      Cou:= BVWritePos - BVReadPos;
      Move( BVBuff[BVReadPos], BV[0], Cou * SizeOf( TBVBuffEl ) );
      Result:= Cou;
    end;

    BVReadPos:= BVWritePos;
  end;

end;


procedure TCAN.GetMV( CallBack: TMCallBack ); // �������� M-���������.
var
  StartTime: Cardinal;

begin
  MBuff.BlockCount[0]:= 0;
  MBuff.BlockCount[1]:= 0;
  MBuff.ByteCount:= 0;
  MCallBack:= CallBack;
  StartTime:= GetTickCount;
  MReq:= True;

  while MReq do
  begin
    if GetTickCount - StartTime > 500 then
    begin
      MReq:= False;
      if @MCallBack <> nil then MCallBack( @MBuff );
      Exit;
    end;
    Sleep(200);
  end;

end;


{function TCAN.GetASD( var OutASD: TASD; var OutAK: TAK ): Boolean;
var
  i: Integer;
  N: Integer;
begin
  if ASDReady then
  begin
    Result:= True;
    OutASD:= ASD;
    OutAK:= AK;
    for N:=0 to 1 do
    for i:=0 to High( ASD[N] ) do
      ASD[N, i]:= False;
    ASDReady:= False;
  end else Result:= False;
end;
 }

procedure TCAN.OnSendError;
begin
  SendError:= True;
end;

procedure TCAN.OnBlockReserved( Bl: TBlock );
var
  Buff: PAVBuff;
  i, j, N: Integer;
  Cou: Byte;
  T: Byte;
  MainNit: Boolean;
  WriteNit: Byte;
  K1, K2, AK_: Boolean;
  Kan1, Kan2: Integer;
  Kanal1, Kanal2: TKanal;

begin
  if not Config.WaitHardware then Exit;
//    WrLog( 'R: ' + Bl2Str(Bl) );
  case BL[1] of
    $DB:
      begin
        TestResult:= BL[4] shl 8 + BL[3];
      end;
    // �-���������.
    $7F:
      begin
        Buff:= AVOutBuff;
        AVOutBuff:= AVInBuff;
        AVInBuff:= Buff;
      end;
    $82:
      begin
        AVInBuff.Amax:= BL[3];
        AVInBuff.Tmax:= BL[4] + BL[5] * 0.1;
      end;
    $80, $81:
      with AVInBuff^ do
      begin
        if BL[1] = $80 then Pos:=0;
        if Pos >= 232 then
        begin
          Pos:= 233; // ���������� ������� ���� (���� ������. ��� ������, ���� ����� ��������).
          WrLog('(E) TOO MANY DATA IN A-VIEW');
        end else
        begin
          for j:=0 to 7 do
            AV[ Pos + j ]:=BL[ 3 + j ];
          Pos:= Pos + 8;
        end;
      end;
    $84:  // �������� �������� ������.
      with AVInBuff^ do
      if ( Config.WorkRail = 0 ) xor ( BL[6] <> 0 ) then // ����� �����.
      begin
        if BL[5] > 5 then TmaxK0:= BL[4] + 1 else TmaxK0:= BL[4];
        AmaxK0:= BL[3];
      end;
    // B-���������.
    $3F:
      begin // ������������ ������� ����.
        inc( BVWritePos );
        if BVWritePos > BVBufferCount - 1 then BVWritePos:= 0;
        if BL[3] > 127 then BVBuff[ BVWritePos ].Napr:= BL[3] - 256 else BVBuff[ BVWritePos ].Napr:= BL[3];
        BVBuff[ BVWritePos ].DataCount:= 0;
      end;
    $70, $71:
      begin

        // ������ 1 ����� B-���������.
        if BL[1] = $70 then
        if BVWritePos <> - 1 then
        with BVBuff[ BVWritePos ] do
        begin
          Cou:= ( BL[2] and $F );
          Data[ DataCount ]:= Cou;
          for i:= 1 to Cou do
            Data[ DataCount + i ]:= BL[ i + 2 ];
          DataCount:= DataCount + Cou + 1;
        end;

        // ������ 1 ����� B- ��� M-���������.
        if MReq then
        with MBuff do
        begin
          if BL[3] and $7F = 0 then
          if BlockCount[ Sign(BL[2] and $80) ] < 2 then
          begin
            Cou:= ( BL[2] and $F );
            Data[ ByteCount ]:= Cou;
            for i:= 1 to Cou do
              Data[ ByteCount + i ]:= BL[ i + 2 ];
            ByteCount:= ByteCount + Cou + 1;
            Inc( BlockCount[ Sign(BL[2] and $80) ] );
            if ( BlockCount[0] >= 2 ) and ( BlockCount[1] >= 2 ) then
            begin
              if @MCallBack <> nil then MCallBack( @MBuff );
              MReq:= False;
            end;
          end;
        end;

      end;


    // ���
    $83:
      if isASDOn then
      begin
        T:= BL[4];
        for N:=0 to 1 do // ���� �� ������.  N - ����� ���� � ����� ������ ���: ��� (0), ���� (1).
        begin
          MainNit:= N = Config.WorkRail; // �������� �����?
          WriteNit:= N xor Config.WorkRail; // ����� ���� � �������� ��� (0) / ������. (1)

          // ���������, ����� ������ ���������.
          // ���� �����.
          if N = 0 then
          begin
            K1:= ( BL[3] and 2 = 2 );            // ������������ ������� ������ ����� T ���� 0.
            K2:= ( BL[3] and 4 = 4 );            // ������������ ������� ������ ����� T ���� 0.
            AK_:= ( BL[3] and 8 = 8 );            // ������ �� ����� T ���� 0.
          end else
          begin
            // ���� ������.
            K1:= ( BL[3] and 32 = 32 );          // ������������ ������� ������ ����� T ���� 1.
            K2:= ( BL[3] and 64 = 64 );          // ������������ ������� ������ ����� T ���� 1.
            AK_:= ( BL[3] and 128 = 128 );        // ������ �� ����� T ���� 1.
          end;

          // ��������� ������ �������.
          Kan1:= -1;
          Kan2:= -1;
          if HandKanal in [0, 1] then
          begin
            // � ������� - ������ 0 � 1 �������� � ����� �����.
            if N = 0 then
            begin
              Kan1:= HandKanal;
              Kanal1:= HKanals[Kan1];
            end;
          end else
          if HandKanal <> -1 then
          begin
            // � ������� - ������ 2 ... �������� � ������ �����.
            if N = 1 then
            begin
              Kan1:= HandKanal;
              Kanal1:= HKanals[Kan1];
            end;
          end else
          if ( T <= High( Takts[WriteNit] ) ) and ( Takts[WriteNit, T] <> nil ) then
          begin
            Kan1:= Takts[WriteNit, T].Kan1;
            Kan2:= Takts[WriteNit, T].Kan2;
            Kanal1:= Takts[WriteNit, T].Kanal1;
            Kanal2:= Takts[WriteNit, T].Kanal2;
          end;

          if ( Kan1 <= -1 ) and ( Kan2 <= -1 ) then Continue;  // ��� ������� ����� � ������, �� ��� ����� �������� ���� �������.


          // �������� ��.
          if Kan1 > -1 then AK[WriteNit, Kanal1.KanAK]:= AK_;
          if Kan2 > -1 then AK[WriteNit, Kanal2.KanAK]:= AK_;

{
          // �������� ��.
          if Kan1 <> -1 then
          begin
            if MainNit then
            begin
              // �������� �����.
              if Kan1 <> 5 then AK[WriteNit, Kan1]:= AK_;
              if Kan1 = 3 then AK[WriteNit, 5]:= AK_;
            end else
            begin
              // ����������� ����.
              if Kan1 <> 7 then AK[WriteNit, Kan1]:= AK_;
              if Kan1 = 3 then AK[WriteNit, 7]:= AK_;
            end;
          end;
          if Kan2 <> -1 then AK[WriteNit, Kan2]:= AK_;
 }

          // �������� ���.
//          if MainNit and ( Kan2 = 0 ) then K2:= not K2;   // � ������ 0 ���. ���� ��� �������������.
          if Kanal1.InverseASD then K1:= not K1;
          if ( Kan2 > -1 ) and Kanal2.InverseASD then K2:= not K2;

          if Kan1 = Kan2 then
          begin
            if Kan1 > -1 then ASD[WriteNit, Kan1]:= K1 or K2;
          end else
          begin
            if Kan1 > -1 then ASD[WriteNit, Kan1]:= K1;
            if Kan2 > -1 then ASD[WriteNit, Kan2]:= K2;
          end;

          {
          // �������� �������� ���.
          if not ASDReady then
          begin
            if ( Kan1 <> -1 ) and ASD_s[WriteNit, Kan1] then
              ASD[WriteNit, Kan1]:= True;
            if ( Kan2 <> -1 ) and ASD_s[WriteNit, Kan2] then
              ASD[WriteNit, Kan2]:= True;
          end;
          }

        end;  // ���� �� ������.

        // ���� ���.
        Snd.ProcessASD( ASD );
//        if (T = 6) or (HandKanal <> -1) then ASDReady:= True;

      end; // (���)
  end; // (case)
end;



function TCAN.SetPar(Takt, ParOfs, ParV: Byte): Boolean;
begin
  SetPar:= SendBL($40, 3, Takt, ParOfs, ParV, 0, 0, 0, 0, 0);
end;


function TCAN.SetStrob(N, Takt, Nstr, T1, T2, A, Regim: Byte): Boolean;
begin
  SetStrob:= SendBL($49, 7, $80*(N and 1) + Takt, Nstr and 3, T1, 0, T2, 0, (A and $FE) + (Regim and 1), 0);
end;

function TCAN.SetVRU(N, Takt, T1, A1, T2, A2: Byte): Boolean;
begin
  SetVRU:= SendBL($41, 7, $80*(N and 1) + Takt, T1, 0, A1, T2, 0, A2, 0);
end;

function TCAN.SetVRU_Point(N, Takt, T, A: Byte): Boolean;
begin
  Result:= SetVRU(N, Takt, T, A, T, A);
end;

function TCAN.SetTakt( T, GenL, GenR, ResL, ResR, Tmn, Tmax : Byte; WSA: Word )  : Boolean;
{ ������������� ��������� ����� (����� ��� � ���������� �������).
  �������������� ���� ��������� InitCikl!
  T - ���� (0..KT-1),
  GenL, GenR - ����� ���������� � ����� � ������ ����, ������������� (0..7, ��� 15 - ����.),
  ResL, ResR - ����� ��������� � ����� � ������ ����, ������������� (0..7, ��� 15 - ����.),
  Tmn - ��������� �������� �������� (��� ��-������: 3, ����� 1)
  Tmax - ������������ �����,
  WSA - work space address (2 �����) }


  var
 E : Boolean;
 WSA_h : Byte;
begin
   E:=      SetPar(T, $1, GenL shl 4 + ResL ); // ���/����� ����� ����
   E:=E and SetPar(T, $2, GenR shl 4 + ResR ); // ���/����� ������ ����
   E:=E and SetPar(T, $5, 0); // ������ 0
   // ���������� ������� ���� WSA (� ����������� � ������� �������)
   WSA_h := (WSA shr 8) and $FF;
   If WSA and $FF <> 0 then inc ( WSA_h );
 //  WSA_:=$30 + $10*T  ;     //                                  !!!!!!!!!!!!!!!!!!!!!!!!!!
   E:=E and SetPar(T, $6, WSA_h); // WSA
   E:=E and SetPar(T, $7, Tmax); // ������������ ��������� (���)
   E:=E and SetPar(T, $8, $10); // ������� ������ B-����. ���.����
   E:=E and SetPar(T, $9, $20); // ������� ������ 1 ���.����
   E:=E and SetPar(T, $A, $20); // ������� ������ 2 ���.����
   E:=E and SetPar(T, $B, $20); // ������� ������ 3 ���.����
   E:=E and SetPar(T, $C, $10); // ������� ������ B-����. ����.����
   E:=E and SetPar(T, $D, $20); // ������� ������ 1 ����.����
   E:=E and SetPar(T, $E, $20); // ������� ������ 2 ����.����
   E:=E and SetPar(T, $F, $20); // ������� ������ 3 ����.����
   E:=E and SetPar(T, $10, 7); // ��������� ������������ ���.���� (0..7)
   E:=E and SetPar(T, $11, 7); // ��������� ������������ ����.���� (0..7)
   E:=E and SetPar(T, $12, Tmn); // ��������� �������� �������� (��� ��-������: 3, ����� 1)
   SetTakt:=E;
end;

function TCAN.InitCikl(KT_ : Byte): Boolean;
begin
   KT:=KT_;
   InitCikl:= SendBL($42, 1, KT, 0, 0, 0, 0, 0, 0, 0);  // ���-�� ������
end;

function TCAN.Set2TP(N, Takt: Byte; l_2TP, r_2TP: Single): Boolean;
var
 L, R : Word;
begin
  L:= trunc( l_2TP * 10 );
  R:= trunc( r_2TP * 10 );
  Set2TP:= SendBL( $4A, 5, Takt{ or (N shl 7)}, Lo( L ), Lo( R ), Hi( L ), Hi( R ) );  // 2Tp ��� ����� � ������ ����
end;

function TCAN.StartAview(N, Takt, Tst, M : Byte; WorkStrob: Byte; Single: Boolean = False; OnlyMax : Boolean = False): Boolean;
var
  B: Boolean;
begin
   B:= SendBL($45, 6, $80*(N and 1) + Takt, Tst, 0, M, $80 * ord(Single) + $10 * ord(OnlyMax) , WorkStrob, 0, 0);   // ���� ����� M: 0 - ����������; 1 - ����������.
   Result:= B;
end;

function TCAN.StopAview: Boolean;
begin
   StopAview:= SendBL($46, 1, 0, 0, 0, 0, 0, 0, 0, 0);
end;

function TCAN.ImitDP;
begin
   ImitDP:= SendBL($3E, 1, ord(isOn), 0, 0, 0, 0, 0, 0, 0);
end;

function TCAN.ASDTurnOn_BUM: Boolean;
begin
  SendBL($4B, 0, 0,0,0,0,0,0,0,0);
end;

function TCAN.ASDTurnOn( HandKanal_: Integer ): Boolean;
var
  n, i: Integer;

begin
  Result:= True;
  HandKanal:= HandKanal_;

  for n:= 0 to 1 do
    for i:= 0 to High( ASD ) do
    begin
      ASD[N, i]:= False;
    end;

  if not isASDOn then
  begin
    isASDOn:= True;
  end;
end;

procedure TCAN.ASDTurnOff;
begin
  SND.StopTone;
  isASDOn:= False;
end;

function TCAN.Start: Boolean;
begin
   Result:= SendBL($47, 0, 0, 0, 0, 0, 0, 0, 0, 0);
end;

function TCAN.Reset;
begin
   WrLog('Reset procedure started...');
   Reset:= SendBL($F0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
   If Wait then Sleep(5000);
end;

function TCAN.Stop: Boolean;
begin
   Stop:= SendBL($48, 0, 0, 0, 0, 0, 0, 0, 0, 0);   // ����. B-����.
   StopAview;
end;

{function TCAN.GetMaximum( Nit_, Takt_ : Byte; var T, A : Byte; var AView: AViewArr; GetAView: Boolean ): Boolean;
var
 BL : TBL;
 i  : Integer;
 j  : Integer;
 Cou: Integer;
begin
  WrLog('(i) Getting maximum...');
  GetMaximum:=false;
  StartAview( Nit_, Takt_, 0, 10, 0, True, not GetAView);
  Sleep(100);
  repeat
    If not GetBL(BL) then exit;
  until BL[1]=$82;
  A:=BL[3];
  T:=BL[4];

  if GetAView then
  begin
    // ������� �-���������
    i:=0;
    Cou:=0;
    SetLength( AView, 232);
    while i<232 do
    begin
      inc( Cou );
      if Cou > 100 then exit;
      if not GetBL(BL) then exit;
      if BL[1] or 1 <> $81 then continue;
      for j:=0 to 7 do
        AView[i+j]:=BL[3+j];
      i:=i+8;
    end;
  end;

  GetMaximum:=true;
end;     }



function TCAN.Test: Word;
var
 T: LongInt;
begin
  {$IFDEF AKPSimulation} Result:= 1; Exit; {$ENDIF}
  Result:= 0;
  TestResult:=0;
  SendBL($DE, 0, 0, 0, 0, 0, 0, 0, 0, 0);
  T:= GetTickCount;
  repeat
  until (GetTickCount - T > 1000) or (TestResult <> 0);
  Result:= TestResult;
end;

function TCAN.BVBuffCount: Integer;
begin
  if BVWritePos = -1 then
    Result:= -1
  else if BVReadPos > BVWritePos then
    Result:= BVBufferCount - BVReadPos + BVWritePos
  else
    Result:= BVWritePos - BVReadPos;
end;


// -----------------------------------------------------------------------------
// -------------------------  ������ ����� ������  -----------------------------


function TTakt.Install(CAN : TCAN; SReg: Byte): Boolean;
var
 ResL, ResR,            // �������� ����� � ������ ����
 GenL, GenR  : Byte;    // ��������� ����� � ������ ����
 ThisGen, ThisRes: Byte; // ��������� � �������� ����� �����.
 SinhGen, SinhRes: Byte; // ��������� � �������� ����������� �����.

 E           : Boolean;

begin
{ ��������� ������� ����� � ������ ���� ��� ������� Takt. �������������� ���� ������� CAN.InitCikl.
 � ������ ��������� (SReg=3), ����� 5 �������� � ������ ���. }

// ��� ������� ������ ���������� ������ ��� �������� (� �� ����������) ������, �� ���� ��� ���, � ������� ConfigNit = 0.

    ThisGen:= Gen;
    ThisRes:= Res;
    if ( SReg = 3 ) and ( Kan1 = 5 ) then ThisGen:= ThisRes;

    if SinhTakt = nil then
    begin
      SinhGen:= $F;
      SinhRes:= 0;
    end else
    begin
      SinhGen:= SinhTakt.Gen;
      SinhRes:= SinhTakt.Res;
    end;

    if N_res = 0 then
    begin
      GenL:= ThisGen;
      ResL:= ThisRes;
      GenR:= SinhGen;
      ResR:= SinhRes;
    end else
    begin
      GenR:= ThisGen;
      ResR:= ThisRes;
      GenL:= SinhGen;
      ResL:= SinhRes;
    end;


    E:= CAN.SetTakt( InstallNum, GenL, GenR, ResL, ResR, DelayFactor, Ten, GetWSA(CAN.KT) );

    if Kanal1 <> nil then E:= E and InstallVRU(CAN, SReg);
    if SinhTakt <> nil then E:= E and SinhTakt.InstallVRU( CAN, SReg );

    if Kanal1 <> nil then E:= E and InstallStrob(CAN, SReg);
    if SinhTakt <> nil then E:= E and SinhTakt.InstallStrob(CAN, SReg);

    E:= E and Install2TP(CAN);

    Install:= E;
end;

function TTakt.Install2TP(CAN : TCAN): Boolean;
var
  _2TP_: array[0..1] of Single;

begin
  if Kanal1 <> nil then _2TP_[ N_res ]:= Kanal1.TwoTP else _2TP_[ N_res ]:= 0;
  if ( SinhTakt <> nil ) and ( SinhTakt.Kanal1 <> nil ) then _2TP_[ 1 - N_res ]:= SinhTakt.Kanal1.TwoTP else _2TP_[ 1 - N_res ]:= 0;
  Result:= CAN.Set2TP( N_res, InstallNum, _2TP_[0],  _2TP_[1] );
end;


procedure TTakt.UpdateVRCH(A: Single);
const
  x_step = 0.1;

var
  i: Integer;
  P: TFloatPoint;

begin
  _VRCHCurveA_:= A;

  P.x:= 0;
  i:= 0;
  repeat
    P.y:= power(P.x, A);
    VRCHCurve.Curve[i]:= P;
    VRCHCurve.Count:= i + 1;
    if P.x >= 1 then Break;
    i:= i + 1;
    P.x:= P.x + x_step;
  until False;
end;

function TTakt.InstallVRU(CAN : TCAN; SReg: Byte) : Boolean;

  procedure AddPoint( X, Y: Integer );
  var
    i: Integer;
  begin
    i:= VRCH.Count;
    VRCH.Count:= i + 1;
    VRCH.Curve[i].X:= X;
    VRCH.Curve[i].Y:= Y;
  end;

var
  R: Boolean;
  i: Integer;
  CurveEnd: Integer;
  Level: Integer;
  a, b, L, c: Integer;

begin
  // ������������� ����� ��� (VRCH)
  VRCH.Count:= 0;
  CurveEnd:= Kanal1.VRCH;
  Level:= Kanal1.ATT * 2;

  {$IFDEF UseVRCHCurve}
  R:= True;
  for i:= 0 to VRCHCurve.Count - 1 do
  begin
    AddPoint( Round( CurveEnd * VRCHCurve.Curve[i].X ), Round( Level * VRCHCurve.Curve[i].Y ) );
  end;
  {$ELSE}
  AddPoint( 0, 0 );
  AddPoint( CurveEnd, Level );
  {$ENDIF}

  if Kan2 = -2 then
    begin
      AddPoint( Kanal1.VRCH2, Level );
      c:=Kanal1.ATT-Kanal1.Ky;
      Level:=(c+Kanal1.Ky2)*2;
      AddPoint( Kanal1.VRCH2, Level );
      {AddPoint( VRCH2Time, Level );
      Level:=VRCH2Level;
      AddPoint( VRCH2Time, Level );}
    end;

  if Kan2 <= -1 then
  begin
    // ������ ���� ����� � �����.
{  �������������� !!!
    if ( ConfigNit = 0 ) and ( Kan1 in [2, 3] ) and ( SReg in [rScan] ) and ( Kanal1.NR_Level <> 0 ) then
    begin
      // �������������� (�������)
      L:= Level - Kanal1.NR_Level * 2;
      if L < 0 then L:= 0;
      AddPoint( Kanal1.NR_Start, Level );
      AddPoint( Kanal1.NR_Start, L );
      AddPoint( Kanal1.NR_Start + Kanal1.NR_Length, L );
      AddPoint( Kanal1.NR_Start + Kanal1.NR_Length, Level );
    end;
}
  end else
  begin
    // ��� ������ � �����.
    AddPoint( Kanal1.Str_en, Level );
    Level:= Kanal2.ATT*2;
    AddPoint( Kanal1.Str_en, Level );

  end;

  if AKStr_st <> 0 then
  begin
    // �������� ��.
    AddPoint( AKStr_st + 3, Level );
    AddPoint( AKStr_st + 3, Kanal1.AK_Level * 2 );
    AddPoint( AKStr_en - 3, Kanal1.AK_Level * 2 );
    AddPoint( AKStr_en - 3, 0 );
    AddPoint( Ten, 0 );
  end else
  begin
    AddPoint( TEn, Level );
  end;

  // �������� ��� � ���.
  for i:= 0 to VRCH.Count - 2 do
    R:= R and CAN.SetVRU( N_res, InstallNum, VRCH.Curve[i].X, VRCH.Curve[i].Y, VRCH.Curve[i+1].X, VRCH.Curve[i+1].Y );
  Result:= R;

end;

function TTakt.InstallStrob(CAN : TCAN; SReg: Byte; Adj2TPMode: Boolean = False ) : Boolean;
var
  B: Boolean;
begin
  B:= CAN.SetStrob(N_res, InstallNum, 0, BStr_st, BStr_en, 16, 0);  // ������� ����� - ����� B-���������.
  if SReg in [rNastr, rHandNastr] then
  begin
    // � ������ ��������� - ������ � ������ - ����������� ������ ������� � ������� ������� (��������������)
    // � ������ ��������� 2TP ����������� ������ ��������� 2 TP.

    if Adj2TPMode then
      B:= B and CAN.SetStrob(N_res, InstallNum, 1, Kanal1.NStr_st, Kanal1.NStr_en, 32, 0)
    else
      B:= B and CAN.SetStrob(N_res, InstallNum, 1, Kanal1.NStr2TP_st, Kanal1.NStr2TP_en, 32, 0);

    if Kan2 > -1 then
    begin
      if Adj2TPMode then
        B:= B and CAN.SetStrob(N_res, InstallNum, 2, Kanal2.NStr_st, Kanal2.NStr_en, 32, 0)
      else
        B:= B and CAN.SetStrob(N_res, InstallNum, 2, Kanal2.NStr_st, Kanal2.NStr_en, 32, 0);
    end;

  end else
  begin
    // � ������ ������ - ������ � ������ - ������� ������ ������� � ������� ������� (��������������)
    B:= B and CAN.SetStrob(N_res, InstallNum, 1, Kanal1.Str_st, Kanal1.Str_en, 32, 0);
    if Kan2 > -1 then
      B:= B and CAN.SetStrob(N_res, InstallNum, 2, Kanal2.Str_st, Kanal2.Str_en, 32, 0);
  end;

  // ������ ����� - ����� ���� ����� ��� �������� ��.
  if UseZM then
  begin
    if ZonaM < ZonaMSize then ZonaM:= ZonaMSize;
    B:= B and CAN.SetStrob(N_res, InstallNum, 3, ZonaM - ZonaMSize, ZonaM + ZonaMSize, 32, 0);
  end else
  begin
    B:= B and CAN.SetStrob(N_res, InstallNum, 3, AKStr_st, AKStr_en, 32, 0)
  end;

  Result:= B;

end;

function TTakt.GetWSA(KT: Byte): Word;
begin
  Result:= $3000 + $1000 * InstallNum;
end;



function TTakt.SetConfig(S: string) : Boolean;
begin
  Result:= False;
  GetS(S, Reg);
  GetS(S, ConfigNit);
  GetS(S, Num);
  GetS(S, Gen_pep);
  GetS(S, Res_pep);
  GetS(S, Gen);
  GetS(S, Res);
  GetS(S, Method);
  GetS(S, Alfa);
  GetS(S, TEn);
  GetS(S, Kan1);
  GetS(S, Kan2);
  GetS(S, BStr_st);
  GetS(S, BStr_en);
  GetS(S, AV_Mash);
  GetS(S, AKStr_st);
  GetS(S, AKStr_en);
  GetS(S, DelayFactor);
  Result:=  True;
end;


function TTakt.FinishConfig: Boolean;
var
  KanArray: PSingleKanArray;

begin
  Result:= False;
  // ��������� �������� ����� ���� (N_res) � ����� ������ ��� �������� (InstallNum).
  if Reg = 1 then
  begin
    // ������.
    InstallNum:= 0;
    N_res:= ConfigNit;
    KanArray:= @HKanals;
  end else
  begin
    // ���������.
    InstallNum:= Num;
    KanArray:= @Kanals[ConfigNit];
    N_res:= ConfigNit xor Config.WorkRail; // ����� �� ������� ������!
  end;

  // ��������� ������ �� ������� ������ �� ������� �����.
  try
    if Kan1 <= -1 then Kanal1:= nil else Kanal1:= KanArray^[ Kan1 ];
    if Kan2 <= -1 then Kanal2:= nil else Kanal2:= KanArray^[ Kan2 ];
  except
    WrLog( Format( '�������� ������������. ���� %d.%d.%d (���.����.�����) ��������� �� �������������� ������ (������ � ����� *.tk; ������ ��������� � *.ka)', [ Reg, ConfigNit, Num ] ) );
    Exit;
  end;

  // ��������� ������ �� ������ ���� �� ������� �������.
  if Kanal1 <> nil then Kanal1.Takt:= Self;
  if Kanal2 <> nil then Kanal2.Takt:= Self;

  // ��������� ������ �� ���������� ����.
  SinhTakt:= nil;
  if ConfigNit = 0 then
  begin
    if Num <= High( Takts[1] ) then SinhTakt:= Takts[1, Num];
  end else
    if Num <= High( Takts[0] ) then SinhTakt:= Takts[0, Num];

  {$IFDEF UseVRCHCurve}
  VRCHCurveA:= 0.3;
  {$ENDIF}
  Result:= True;

end;


procedure TTakt.GetNStrob(out St, En : Byte);
begin
  case Alfa of
  0 : begin
        St:=40;
        En:=70;
      end;
  45: begin
        St:=30;
        En:=60;
      end;
  70: begin
        St:=60;
        En:=100;
      end;
  end;
end;


// -----------------------------------------------------------------------------
// -----------------------------  ������ ������  -------------------------------


function TKanal.AdjustKy( N: Byte ): Boolean;
var
  ATT__: Byte;

begin
  // ��������� �� �������� ����������������
  if N in [0, 18] then
  begin
    Result:= False;
    Exit;
  end;

  ATT__:= ATT - N + Ky_r;                  // ���������� �����. ���, ������� ���� ����������.
  ATT:= ATT__;                              // ���. ���.
  Ky:= Ky_r + ( ATT - ATT__ );            // �� �����. �� Ky_r �� �������� ������ ��������� �����������.
  AdjDate:= Now;
  Result:= True;
end;


function TKanal.SetConfig_KAN(S : string): Boolean;    // ����. ���������������� ������ (�� ������)
var
 Num_ : Integer;
 Reg_ : Byte;
 Nit_ : Byte;
 a, b: Integer;
begin
  Result:= False;
  GetS(S, Reg_);
  GetS(S, Nit_);
  GetS(S, Num_);
  if ( Num_ <> Num ) or ( Reg_ <> Reg ) or ( Nit <> Nit_ ) then Exit;
  GetS(S, FVRCH);
  GetS(S, FTwoTP);
  GetS(S, Ky);
  GetS(S, FATT);
  GetS(S, Str_st);
  GetS(S, Str_en);
  GetS(S, NR_Start);
  GetS(S, NR_Length);
  GetS(S, NR_Level);
  GetS(S, AK_Level);
  GetS(S, AdjDate);
  GetS(S, Ky2);
  GetS(S, VRCH2);

  //GetS(S, Takt.VRCH2Level);
  //GetS(S, Takt.VRCH2Time);

  // ������� ��� ���������� �������� � ����� �������������
  AdjustChanges[apKy].Flag:=true;
  AdjustChanges[apKy].CurValue:=Ky;

  AdjustChanges[apATT].Flag:=true;
  AdjustChanges[apATT].CurValue:=FATT;

  AdjustChanges[apStrobSt].Flag:=true;
  AdjustChanges[apStrobSt].CurValue:=Str_st;

  AdjustChanges[apStrobEd].Flag:=true;
  AdjustChanges[apStrobEd].CurValue:=Str_en;

  AdjustChanges[apVRCH].Flag:=true;
  AdjustChanges[apVRCH].CurValue:=FVRCH;

  AdjustChanges[apTwoTp].Flag:=true;
  AdjustChanges[apTwoTp].CurValue:=Round(FTwoTP*10);

  Result:= True;
end;

function TKanal.SetConfig_KA(S : string): Boolean;   // ����. ��������� ������ (�� ������)
var
  I: Integer;
  
begin
  GetS(S, Reg);
  GetS(S, Nit);
  GetS(S, Num);
  GetS(S, TktN);
  GetS(S, Ky_r);
  GetS(S, Str_st_r);
  GetS(S, Str_en_r);
  GetS(S, I);
  WaveDirection:= TDirection( I );
  GetS(S, BViewDelayMin );
  GetS(S, BViewDelayMax );
  GetS(S, InverseASD );
  GetS(S, KanAK );
  if KanAK = -1 then KanAK:= Num;
  GetS( S, AdjRailType );
  Result:= True;
end;

function TKanal.GetConfig_KAN: string;                 // ������ (��� ����������) ���������������� ������ (� ���� ������)
var
  S: string;
begin
  S:='';
  AddS(S, Reg);
  AddS(S, Nit);
  AddS(S, Num);
  AddS(S, VRCH);
  AddS(S, TwoTP);
  AddS(S, Ky);
  AddS(S, FATT);
  AddS(S, Str_st);
  AddS(S, Str_en);
  AddS(S, NR_Start);
  AddS(S, NR_Length);
  AddS(S, NR_Level);
  AddS(S, AK_Level);
  AddS(S, AdjDate);
  AddS(S, Ky2);
  AddS(S, VRCH2);

  Result:= S;
end;

procedure TKanal.SetATT( ATT: Byte );
begin
  if ATT > 80 then FATT:= 80 else FATT:= ATT;
end;

procedure TKanal.SetVRCH( VRCH: Byte );
begin
  FVRCH:= VRCH;
  if Assigned( Takt ) then
  begin
    if Assigned( Takt.Kanal1 ) then Takt.Kanal1.FVRCH:= VRCH;
    if Assigned( Takt.Kanal2 ) then Takt.Kanal2.FVRCH:= VRCH;
  end;
end;

procedure TKanal.SetTwoTP( TwoTP: Single );
begin
  FTwoTP:= TwoTP;
  if Assigned( Takt ) then
  begin
    if Assigned( Takt.Kanal1 ) then Takt.Kanal1.FTwoTP:= TwoTP;
    if Assigned( Takt.Kanal2 ) then Takt.Kanal2.FTwoTP:= TwoTP;
  end;
end;


procedure TKanal.InitNStr;
var
  i: Integer;
begin
  // ������ ����������� ���������.
  if Reg = 0 then
  begin // ���������.
    if Nit = 0 then
    case Num of
      0: // ��������� �������� �����.
        begin
          NStr_st:= Str_st;
          NStr_en:= Str_en;
        end;
      4:
        begin
          NStr_st:= Round( MM2MKS( 10, Takts[ Nit, TktN ].Alfa, True ) );
          NStr_en:= Round( MM2MKS( 16, Takts[ Nit, TktN ].Alfa, True ) );
        end;
      else
        begin
          NStr_st:= Round( MM2MKS( 39, Takts[ Nit, TktN ].Alfa, True ) );
          NStr_en:= Round( MM2MKS( 45, Takts[ Nit, TktN ].Alfa, True ) );
        end;
    end else
    begin
      NStr_st:= Round( MM2MKS( 10, Takts[ Nit, TktN ].Alfa, True ) );
      NStr_en:= Round( MM2MKS( 16, Takts[ Nit, TktN ].Alfa, True ) );
    end;
  end else
  case Num of
    0, 1:  // �������.
      begin
        NStr_st:= Str_st;
        NStr_en:= Str_en;
      end;
    2, 3, 4:
      begin
        NStr_st:= Round( MM2MKS( 39, HTakts[ TktN ].Alfa, True ) );
        NStr_en:= Round( MM2MKS( 45, HTakts[ TktN ].Alfa, True ) );
      end;
    5, 6:
      begin
        NStr_st:= Round( MM2MKS( 10, HTakts[ TktN ].Alfa, True ) );
        NStr_en:= Round( MM2MKS( 16, HTakts[ TktN ].Alfa, True ) );
      end;
  end;

  // ������ ��������� 2 TP.
  if Takt.Alfa = 0 then
  begin
    NStr2TP_st:= 5;
    NStr2TP_en:= 70;
  end else
  begin
    NStr2TP_st:= 20;
    NStr2TP_en:= 50;
  end;

end;


function TTakt.GetAViewTimeMax: Integer;
begin
  Result:= Round( 23.2 * AV_mash );
end;

procedure TKanal.StartAView;
var
  WorkStrob: Integer;

begin
  if Takt.UseZM then WorkStrob:= 3 else if Num = Takt.Kan1 then WorkStrob:= 1 else WorkStrob:= 2;
  CAN.StartAview( Takt.N_res, Takt.Num, 0, Takt.AV_mash, WorkStrob, False, False );
end;

function TKanConfig.AddNewConfig(Name: string): Boolean;
begin
  UserConfigList.Add(Name);
  CurrentUserConfig:=UserConfigList.Count-1;
  Self.SaveUserConfig(CurrentUserConfig);
end;

end.
