/////////////////////////////////////////////////////
//// ���� ������������������ ����� ��������� ��� ////
////                                             ////
//// ������ �.�. ���� 2009                       ////
//// ������ 1.0.1                                ////
////                                             ////
/////////////////////////////////////////////////////

// ������� ���������
// ������ 1.0.1 ��������� ����� ������ ��������� (���/��)

unit DefCoreUnit;

{$DEFINE UseLog}

interface

uses Forms, SysUtils, Windows, MCAN, Avk11Engine, BScanThreadUnit, PublicData, Math, ConfigObj, MyTypes, SoundControlUnit;

type

   TRegParamList = record
     Organization: string;
     Operator: string;
     DirCode: Integer;
     StartKM: Integer;
     StartPK: Integer;
     StartM: Integer;
     TrackNumber: Integer;
     Direction: Integer;
     PeregonName: string;
     ScanStep: Word;
   end;

   TAScanMeasureResult = record
     R, H, L, N, Kd: Integer;
   end;

   // ��������� ��� �������� B-���������
   TEcho = packed record
      Delay: Byte;
      Amp: Byte;
   end;

   TEchoMas = array[0..7] of TEcho;

   TBScanChanel = packed record
     EchoCount: Byte;
     Echos: TEchoBlock;
     //Echos: TEchoMas;
   end;

   TBScanFrame = array[0..1, 0..9] of TBScanChanel;

   TMeasureUnits = (muMKS, muMM);

   TTextMessageEvent = procedure(Msg: string) of object;
   TGetBScanFrame = procedure(var BScanFrame: TBScanFrame) of object;
   TRegStatus = (rsOff, rsOn, rsPause);

   TDefCore = class
     private
       FLogFile:  Text;
       FTickStart: Cardinal;
       FTerminate: Boolean;
       FRegMode: Byte;          // ������� ����� ����������� (���� ������ ��. ������ PublicData)
       FCurRail: Integer;          // ������� �����
       FCurrentKanal: Integer;     // ����� �������� ������
       //FCurKanalMode: Integer;     // ����� � ������� �������� ������� ����� (���� ������ ��. ������ PublicData)
       FCurKanal: TKanal;          // ������ �� ������� �����
       FDataContainer: TAvk11DataContainer; // ������ ��� �����������
       FBScanStoreThread: TBScanStoreThread; // ������ ��� �������� CAN -> DataContainer
       FRegStatus: TRegStatus;        // ������� ������ �����������. ���������, �������� ��� �� �����
       FRegFileType: TAvikonFileType; // ����� ������ ����� ������������ ��� �����������
       FAScanMeasureResult: TAScanMeasureResult;
       // ��� ������� ������
       FHSDat: THandScanData;
       FBScanFrame: TBScanFrame;
       FHandScanData: THandScanData;
       FHandRegistrationProcess: Boolean;
       FCurSample: Word;
       FIsIminDP: Boolean;
       FSndByOneKanal: Boolean;
       FSndOn: Boolean; // ������� �� ����
       FOldVolume: Integer;
       FMeasureUnits: TMeasureUnits;
    FRegistrator: TAvk11DataContainer;
       procedure _WrLog_( Msg: string );
       function WinMessage(Msg: string; WinType: Byte ): Boolean; // WinType = 0 - ������, 1 - ������, 2 - ��������, 3 - ����;
       procedure AddAdjustChanges;
       function InstallTakts(TArray: array of TTakt):Integer;
       function SetBUMMode(RegMode: Byte): Integer;
       procedure ClearHSData;
       procedure ClearBScanFrame;
       procedure LoadBV(BV: TBVBuffEl);
       procedure HandScanReceived(HSItem: THSItem);
    procedure SetRegistrator(const Value: TAvk11DataContainer);
     protected
       procedure SetKu(Value: Integer);
       function GetKu: Integer;
       procedure Set2tp(Value: Single);
       function Get2tp: Single;
       function GetTaktLength: Byte;
       function GetTaktLengthMKS: Byte;
       procedure SetStrobSt(Value: Byte);
       function GetStrobSt: Byte;
       procedure SetStrobEd(Value: Byte);
       function GetStrobEd: Byte;
       procedure SetZMValue(Value: Word);
       function GetZMValue: Word;
       procedure SetZMEnabled(Value: Boolean);
       function GetZMEnabled: Boolean;
       procedure SetVRCH(Value: Byte);
       function GetVRCH: Byte;
       function GetVRCHCurve: TVRCH;
       function GetScale: Integer;
       procedure SetSndKanal(Value: Boolean);
       procedure SetSndOn(Value: Boolean);
       function GetSndVolume: Integer;
       procedure SetSndVolume(Value: Integer);
       function GetASD(Rail, Channel: Integer): Boolean;
       function GetAK(Rail, Channel: Integer): Boolean;
       procedure SetAKLevel(Rail, Channel, Level: Integer);
       function GetAKLevel(Rail, Channel: Integer): Integer;
       function GetKanal(Rail, Num: integer): TKanal;
       function GetCurDisCoord: Integer;
       function GetReg: TAvk11DataContainer;
     public
        OnTextMessage: TTextMessageEvent; // ���������� ���������� ��������� ��������� (� �������� ��� ��������)
        constructor Create;
        destructor Destroy; override;
        function Boot: Integer;      // ������ ������� �����
        procedure Terminate; // ���������� ��������� ����������� ������ (���� ������������ ������ ��� ��������� ��������)
        function GetAScan(var AData: PAVBuff): Integer; // �������� �-���������. �������� ��������� �� ����� �-���������, ������ ��� ���� �������� �� �����
        function GetBScan(var BV: TBVBuff; GetBScanFrame: TGetBScanFrame): Integer; // �������� B-��������� ��������. ������ B-��������� ������� �������� �� ������� ����������� - ��� �����
        function SetMode(Mode: Integer): Integer; // ��������� �������� ������ ������ (�����-������, ���������, ������, ��������� �������)
        function StartRegistration(FileType: TAvikonFileType; FileName: String; RegParamList: TRegParamList): Integer;
        function PauseRegistration(IsPause: Boolean): Integer; // ���������� ������ � ����������� �������� true - ��������� �����, false - ����� �����
        function StopRegistration: Integer; // ��������� �����������
        function SetAScanKanal(Rail, Num: Integer): Integer; // ������ ������� ����� � ����������� � ������� �������
        function StartHandRegistration(Rail, ScanSurf: Byte): Integer; // ������ ����������� �������
        function StopHandRegistration: Integer;  // ���������� ����������� �������
        function AdjustKu(N: Byte): Integer; // ������� ���������� � ������ ��������� ��� ��������� ������
        function ImitDP(isOn: Boolean): Integer; // ���/���� �������� ������� ����
        
        property RegistrationStatus: TRegStatus read FRegStatus; // ������� ��������� ����������� (��� ���� ������ � "�������" �����������, �� � ������)
        property Ku: Integer read GetKu write SetKu; // �������� ���������������� �������� ������
        property TwoTp: Single read Get2Tp write Set2Tp; // ����� � ������ �������� ������
        property Mode: Byte read FRegMode; // ������� ����� ������ ��� (���� ������ ��. ������ PublicData)
        property AScanLength: Byte read GetTaktLength; // ������������ �-��������� (����� ����������� �� ������� ��� ����� �� �����)
        property AScanLengthMKS: Byte read GetTaktLengthMKS; // ������������ �-��������� (����� ����������� �� ������� ��� ����� �� �����)
        property StrobSt: Byte read GetStrobSt write SetStrobSt; // ������ ������ �������� ������
        property StrobEd: Byte read GetStrobEd write SetStrobEd; // ����� ������ �������� ������
        property ZMEnabled: Boolean read GetZMEnabled {write SetZMEnabled}; // ���� ����� ���/����
        property ZMPosition: Word read GetZMValue write SetZMValue;// ������� ���� ����� 
        property VRCH: Byte read GetVRCH write SetVRCH; // �������� ���
        property VRCHCurve: TVRCH read GetVRCHCurve; // ������ ��� ��������� �� ����� (���� read-only, ����� ���� ������� ����� ����� ���� ������������ ������ �������� � ����)
        property MeasureResult: TAScanMeasureResult read FAScanMeasureResult; // ������ � ����������� ����������� (R, H, N, L, Kd)
        property ScaleA: Integer read GetScale; // ������� �-���������
        property SndByOneKanal: Boolean read FSndByOneKanal write SetSndKanal;
        property SndOn: Boolean read FSndOn write SetSndOn; // ���� true �� ���� �������, ����� ��������
        property SndVolume: Integer read GetSndVolume write SetSndVolume;
        property ASD[Index1, Index2: Integer]:Boolean read GetASD;
        property AK[Index1, Index2: Integer] :Boolean read GetAK;
        property AKLevel[Index1, Index2: Integer]: Integer read GetAKLevel write SetAKLevel;
        property MeasureUnits: TMeasureUnits read FMeasureUnits write FMeasureUnits;
        property Kanal[Rail, Num: integer]: TKanal read GetKanal;
        property Registrator: TAvk11DataContainer read GetReg;
   end;

implementation

{ TDefCoreUnit }

procedure TDefCore.AddAdjustChanges;
var
  i, j, a: Integer;
  k: TAdjustParams;

begin
 { if Assigned(FDataContainer) then
    begin
      i:=0;
      a:=High(Kanals[i]);
      for j:=0 to a do
         for k:=apKy to apTwoTp do
            if Kanals[i, j].AdjustChanges[k].Flag then
              begin
                case k of
                   apKy: FDataContainer.AddSens(i, j, Kanals[i, j].AdjustChanges[k].CurValue);
                   apATT: FDataContainer.AddAtt(i, j, Kanals[i, j].AdjustChanges[k].CurValue);
                   apStrobSt: FDataContainer.AddStStr(i, j, Kanals[i, j].AdjustChanges[k].CurValue);
                   apStrobEd: FDataContainer.AddEndStr(i, j, Kanals[i, j].AdjustChanges[k].CurValue);
                   apVRCH: FDataContainer.AddVRU(i, j, Kanals[i, j].AdjustChanges[k].CurValue);
                   apTwoTp: FDataContainer.Add2Tp_Word(i, j, Kanals[i, j].AdjustChanges[k].CurValue);
                 end;
                 Kanals[i, j].AdjustChanges[k].Flag:=false;
              end;
    end;}
    if Assigned(FDataContainer) then
    begin
      for i:=0 to 1 do
        for j:=0 to High(Kanals[i]) do
           for k:=apKy to apTwoTp do
              if Kanals[i, j].AdjustChanges[k].Flag then
                begin
                  case k of
                     apKy: FDataContainer.AddSens(i, j, Kanals[i, j].AdjustChanges[k].CurValue);
                     apATT: FDataContainer.AddAtt(i, j, Kanals[i, j].AdjustChanges[k].CurValue);
                     apStrobSt: FDataContainer.AddStStr(i, j, Kanals[i, j].AdjustChanges[k].CurValue);
                     apStrobEd: FDataContainer.AddEndStr(i, j, Kanals[i, j].AdjustChanges[k].CurValue);
                     apVRCH: FDataContainer.AddVRU(i, j, Kanals[i, j].AdjustChanges[k].CurValue);
                     //apTwoTp: FDataContainer.Add2Tp(i, j, Kanals[i, j].AdjustChanges[k].CurValue);
                     apTwoTp: FDataContainer.Add2Tp_Word(i, j, Kanals[i, j].AdjustChanges[k].CurValue);
                   end;
                   Kanals[i, j].AdjustChanges[k].Flag:=false;
                end;
      end;
end;

function TDefCore.Boot: Integer;
var
  Res: string;
begin
  // ���� ������
  // 0 - ��� ������, �������� ������
  // 1 - �� ������ �������� ���������� �������
  // 2 - �� ������ �������� ��������
  // 4 - �� ������ �������� ������������
  // 8 - �� ������ ������� CAN'� (������� ����� ���� �����������)
  // 16 - �� ������ ���������� � ���'�� (������� ����� ���� �����������)

  // ��������� ��������� ������������
  Result:=0; //
  Res:='������';
  if Assigned(OnTextMessage) then OnTextMessage('�������� ��������� ������������... ');
  if KConf.LoadSystemConfig then Res:='OK\n' else Result:=1;
  if Assigned(OnTextMessage) then OnTextMessage(Res);
  Res:='������';
  if Assigned(OnTextMessage) then OnTextMessage('�������� ��������... ');
  if KConf.LoadUserConfig(0) then Res:='OK\n' else Result:=Result+2;
  if Assigned(OnTextMessage) then OnTextMessage(Res);
  Res:='������';
  if Assigned(OnTextMessage) then OnTextMessage('�������� ������������... ');
  if KConf.FinishConfig then Res:='OK\n' else Result:=Result+4;
  if Assigned(OnTextMessage) then OnTextMessage(Res);
  
  // ������� ������� ����� �����
  Res:='������\n';

  if Assigned(OnTextMessage) then OnTextMessage('�������� ������ �����... ');

  repeat
  Application.ProcessMessages;
  sleep(100);
  until (CAN.Device.Open) or (FTerminate);

  if not FTerminate then Res:='��\n' else Result:=Result+8;
  if Assigned(OnTextMessage) then OnTextMessage(Res);

  Res:='������\n';
  if Assigned(OnTextMessage) then OnTextMessage('���������� � �������������� ������ (���)... ');
  repeat
  Application.ProcessMessages;
  sleep(100);
  until (CAN.Test <> 0) or (FTerminate);

  // ��������� �������� ���
  CAN.ASDTurnOn_BUM;

  if not FTerminate then Res:='��\n' else Result:=Result+16;
  if Assigned(OnTextMessage) then OnTextMessage(Res);

  FTerminate:=false;
end;

constructor TDefCore.Create;
begin
  OnTextMessage:=nil;
  FTerminate:=false;
  FSndByOneKanal:=false;
  FSndOn:=true;
  FMeasureUnits:=muMKS;
  FHandRegistrationProcess:=false;
  FIsIminDP:=false;
  FTickStart:= GetTickCount;
  FOldVolume:=100;
  KConf:= TKanConfig.Create;

  chdir( ExtractFilePath( Application.ExeName ) );
  chdir( 'SYSTEM' );
  Assign( FLogFile, 'log.txt');
  Rewrite( FLogFile );

  CAN:=TCAN.Create(_WrLog_,  WinMessage, true);

  Snd:=TSndControl.Create;
  
  FDataContainer:=nil;
  FCurKanal:=nil;
  FRegStatus:=rsOff;
end;

destructor TDefCore.Destroy;
begin
  if FRegStatus = rsOn then StopRegistration;
  KConf.Free;
  CAN.Free;
  Snd.Free;
  CloseFile(FLogFile);
  inherited;
end;

function TDefCore.InstallTakts(TArray: array of TTakt): Integer;
var
  i: Integer;
  
begin
  // ���� ������
  // 0 - ��������� ������ ������ ���������
  // 1 - ������ ��������� �����
  // 2 - ������ ������������� �����

  Result:=2;

  CAN.Stop;
  if not CAN.InitCikl( Length( TArray ) ) then Exit;
  Result:=1;
  for i:=0 to High( TArray ) do
    if not TArray[i].Install( CAN, FRegMode ) then Exit;

  Result:=0;
end;

function TDefCore.PauseRegistration(IsPause: Boolean): Integer;
begin
  // ���� ������
  // 0 - ��������� �����/������ ����� ������ �������
  // 1 - ������ ����� ����� ��� �������
  // 2 - ����������� � ��� ���������, ����� ������������

  Result:=1;

  if ((IsPause) and (FRegStatus=rsPause)) or
     ((not IsPause) and (FRegStatus<>rsPause)) then Exit;

  Result:=2;

  if FRegStatus = rsOff then Exit;

  if (IsPause) and (FRegStatus<>rsPause) then // ���������� �����
    begin
      //FBScanStoreThread.Suspend;
      FBScanStoreThread.Pause:=true;
      FRegStatus:=rsPause;
    end
  else if (not IsPause) and (FRegStatus=rsPause) then // ������� �����
    begin
      //FBScanStoreThread.Resume;
      FBScanStoreThread.Pause:=false;
      FRegStatus:=rsOn;
      SetMode(rScan);
    end;
  Result:=0;
end;

function TDefCore.SetBUMMode(RegMode: Byte): Integer;
begin
  // ���� ������
  // 0 - ��������� ������ ����������� ������ ���������
  // 1 - ������ ����� ��� �������
  // 2 - �� ������� ��������� ��������������� ����

  Result:=2;
  if RegMode = FRegMode then
    begin
      Result:=1;
      Exit;
    end;

  {if ( FRegMode in [rNastr, rHand] ) and ( RegMode in [rNastr, rHand] ) then
    FRegMode:= rHandNastr
  else  }
    FRegMode:= RegMode;

  KConf.InitNStr;  // ������� ����������� ������.

  case FRegMode of
    rStop:
      begin  // ����������.
        CAN.ASDTurnOff;                             // �������� ��������� ���.
        CAN.Stop;                                   // �������� ���.
      end;
    rScan:
      begin  // ������
        FBScanStoreThread.RegMode:=rScan;
        if InstallTakts( Takts[0] ) <> 0 then Exit;  // �������� ��� ����� (��� �����).
        CAN.ASDTurnOn(-1);                          // ������� ��������� ���.
        CAN.Start;                                  // ������� ���.
      end;
    rAKSetup: // ��������� ��.
      begin
        if InstallTakts( Takts[0] ) <> 0 then Exit;  // �������� ��� ����� (��� �����).
        CAN.ASDTurnOn(-1);                          // ������� ��������� ���.
      end;
    rKorrDP:
      begin  // ������������� ��.
        if InstallTakts( Takts[0] ) <> 0 then Exit;  // �������� ����� � ��� (��� �����).
        CAN.ASDTurnOff;                             // �������� ��������� ���.
        CAN.Start;                                  // ������� ���.
      end;
    rNastr:
      begin  // ���������.
        if InstallTakts( Takts[0] ) <> 0 then Exit;  // �������� ����� � ��� (��� �����).
        CAN.ASDTurnOff;                             // �������� ��������� ���.
        CAN.Start;                                  // ������� ���.
      end;
    rHand, rHandNastr: // ������, ��������� �������.
      begin
        FBScanStoreThread.RegMode:=rHand;
        CAN.Stop;                                   // �������� ���.
        if not CAN.InitCikl( 1 ) then Exit;         // ������, ��� ����� ������ 1 ����.
        if FRegMode = rHand then CAN.ASDTurnOn(0) else CAN.ASDTurnOff; // ���������� � ���.
      end;
  end;
  Result:=0;
end;

function TDefCore.SetAScanKanal(Rail, Num: Integer): Integer;
var
  a, b, c: integer;
begin
  // ���� ������
  // 0 - � ��������� �� ������� ������ ������� ��������
  // 1 - � ��������� �� ����������
  Result:=0;
  //c:=Kanals[1,3].Ky;
  if Assigned(FCurKanal) then AddAdjustChanges;

  FCurRail:=Rail; // � ������ ������� �� ������������
  FCurrentKanal:=Num;
  // FCurKanalMode:=Mode;

  // � ����������� �� ������ �������� ��� ������ � �������
  case FRegMode of
    rScan,
    rNastr: begin
              FCurKanal:=Kanals[FCurRail,FCurrentKanal];
            end;
    rHand,
    rHandNastr: begin
                  FCurKanal:=HKanals[FCurrentKanal];
                  // ������������� �����. ����
                  //CAN.InitCikl( 1 );
                  FCurKanal.Takt.Install(CAN, FRegMode);
                  CAN.Start;
                end;
  end;

  if FSndByOneKanal then
    Snd.SetSndKanal(FCurRail, FCurrentKanal);

  FCurKanal.StartAView;

  {if not HandScan then
    begin
      //if not ((FCurRail = Rail) and (FCurrentKanal = Num)) then
      //  begin
          FCurRail:=Rail;
          FCurrentKanal:=Num;
          FCurKanal:=Kanals[FCurRail,FCurrentKanal];
          FCurKanal.StartAView;
       // end;
    end;}
end;

function TDefCore.StartRegistration(FileType: TAvikonFileType; FileName: String; RegParamList: TRegParamList): Integer;
begin
  // ���� ������
  // 0 - ����������� ������, ��� ������
  // 1 - �� ������ ��������� ������ ���

  Result:=0;

  FRegFileType:=FileType;
  if Assigned(FDataContainer) then FDataContainer.Free;
  FDataContainer:=TAvk11DataContainer.Create(FRegFileType, FileName);

  with RegParamList do
    begin
      FDataContainer.FillHeader( Now, 2, Organization, Operator, PeregonName );
      FDataContainer.Header.DirCode:= DirCode;
      FDataContainer.Header.StartKM:= StartKM;
      FDataContainer.Header.StartPk:= StartPK;
      FDataContainer.Header.StartMetre:= StartM;
      FDataContainer.Header.Path:= TrackNumber;
      FDataContainer.Header.MoveDir:= Direction;
      FDataContainer.Header.Zveno:= 0;
      FDataContainer.Header.ScanStep:=ScanStep;
      FDataContainer.AddHeader;

      FDataContainer.AddValue(0, vtInt, 0, 1);
      FDataContainer.AddValue(0, vtInt, 0, 2);
      FDataContainer.AddValue(0, vtInt, 0, 3);
      FDataContainer.AddValue(0, vtInt, 0, 4);
      FDataContainer.AddValue(0, vtInt, 0, 5);
      FDataContainer.AddValue(0, vtInt, 0, 6);
      FDataContainer.AddValue(0, vtInt, 0, 7);
      FDataContainer.AddValue(0, vtInt, 0, 8);
      FDataContainer.AddValue(0, vtInt, 0, 9);
      FDataContainer.AddValue(0, vtInt, 0, 10);
      FDataContainer.AddValueList;

      //FDataContainer.AddValue();
    end;

  // �������� ������ �������� ������ CAN -> DataContainer.
  FBScanStoreThread.Free;
  FBScanStoreThread:= TBScanStoreThread.Create( FDataContainer );
  FBScanStoreThread.OnHandScanReceived:=HandScanReceived;
  FBScanStoreThread.Pause:=false;
  FBScanStoreThread.RegMode:=rScan;

  AddAdjustChanges;

  SetMode(rScan);


  //if SetBUMMode(rScan) <> 0 then Result:=1;

  //FBScanStoreThread.OnBScanReserved:= OnBScanReserved;

  if Result=0 then
    begin
      FRegStatus:=rsOn;
      if Assigned(OnTextMessage) then OnTextMessage('\n');
      if Assigned(OnTextMessage) then OnTextMessage('����������� ������\n');
    end;
end;

function TDefCore.StopRegistration: Integer;
begin
  // ���� ������
  // 0 - ����������� ������� ���������
  Result:=0;
  if Assigned( FBScanStoreThread ) then
  begin
    FBScanStoreThread.Terminate;
    FBScanStoreThread.Free;
    FBScanStoreThread:= nil;
  end;

  if Assigned(FDataContainer) then
    begin
      FDataContainer.Free;
      FDataContainer:=nil;
    end;

  KConf.SaveUserConfig(0);  

  if Result = 0 then
    begin
      FRegStatus:=rsOff;
      if Assigned(OnTextMessage) then OnTextMessage('\n');
      if Assigned(OnTextMessage) then OnTextMessage('����������� ��������\n');
    end;
end;

procedure TDefCore.Terminate;
begin
  FTerminate:=true;
end;

function TDefCore.WinMessage(Msg: string; WinType: Byte): Boolean;
begin
 { case WinType of
  0: ErrorMsg( Msg, True );
  1: Result:= ConfirmMsg( Msg );
  2: WarringMsg( Msg );
  3: InfoMsg( Msg );
  100:
    begin
      General.Free;
      General:=nil;
    end;
  end;}
end;

procedure TDefCore._WrLog_(Msg: string);
begin
  {$IFDEF UseLog}
  WriteLn( FLogFile, Format('%.1f ', [ (GetTickCount - FTickStart) / 1000 ] ), Msg );
  {$ENDIF}
end;

function TDefCore.GetAScan(var AData: PAVBuff): Integer;
var
  V: Single;
begin
  // ���� ������
  // 0 - �-��������� �������� ���������
  // 1 - CAN �� ����� ������ A-���������
  Result:=1;
  if not CAN.GetAV( AData ) then Exit;
  if Assigned(FCurKanal) then
    begin
      if FCurKanal.Takt.Alfa < 20 then V:= 5.9 else V:= 3.26;
      FAScanMeasureResult.R:= Round( AData.Tmax  * V / 2 );
      FAScanMeasureResult.H:= Round( FAScanMeasureResult.R * cos( FCurKanal.Takt.Alfa / 180 * pi ) );
      FAScanMeasureResult.L:= Round( FAScanMeasureResult.R * sin( FCurKanal.Takt.Alfa / 180 * pi ) );
      if AData.Amax = 0 then FAScanMeasureResult.N:= 0 else FAScanMeasureResult.N:= Max( 0, Round( 20 * log10( AData.Amax / 32 ) ) );
      FAScanMeasureResult.Kd:=FAScanMeasureResult.N-FCurKanal.Ky;
     end;
  Result:=0;
end;

function TDefCore.GetKu: Integer;
begin
  if Assigned(FCurKanal) then result:=FCurKanal.Ky;
end;

procedure TDefCore.SetKu(Value: Integer);
var
  ATT_: Integer;
begin
  if Assigned(FCurKanal) then
    begin
      if FCurKanal.Ky<>Value then
        begin
          ATT_:=FCurKanal.ATT + Value - FCurKanal.Ky;
          if ATT_>80 then exit;
          if ATT_<0 then exit;
          FCurKanal.ATT:= ATT_;
          //FCurKanal.ATT:= FCurKanal.ATT + Value - FCurKanal.Ky;
          FCurKanal.Ky:=Value;
          FCurKanal.AdjustChanges[apKy].Flag:=true;
          FCurKanal.AdjustChanges[apKy].CurValue:=Value;
          SendBum.Add( FCurKanal, cInstallVRU );
        end;
    end;
end;

function TDefCore.Get2tp: Single;
begin
  if Assigned(FCurKanal) then result:=FCurKanal.TwoTP;
end;

procedure TDefCore.Set2tp(Value: Single);
begin
  if Assigned(FCurKanal) then
    begin
      if FCurKanal.TwoTp<>Value then
        begin
          FCurKanal.TwoTp:=Value;
          FCurKanal.AdjustChanges[apTwoTp].Flag:=true;
          FCurKanal.AdjustChanges[apTwoTp].CurValue:=Round(Value*10);
          SendBum.Add( FCurKanal, cInstall2TP );
        end;
    end;
end;

procedure TDefCore.ClearHSData;
var
  i, j: Integer;
begin
  for i:=0 to High(FHSDat.HSItem) do
    begin
      FHSDat.HSItem[i].Sample:=0;
      FHSDat.HSItem[i].Count:=0;
      for j:=0 to 6 do
        FHSDat.HSItem[i].Data[j]:=0;
    end;
  SetLength(FHSDat.HSItem, 0);
end;

function TDefCore.StartHandRegistration(Rail, ScanSurf: Byte): integer;
begin
  if Assigned(FCurKanal) then
    begin
      //PauseRegistration(true);
      FRegMode:=rHand;

      // �������� ��������� ��� ����� ������� ������������

      FHandScanData.HSHead.Rail:=RRail(Rail);
      FHandScanData.HSHead.ScanSurf:=ScanSurf;
      FHandScanData.HSHead.Channel:=FCurKanal.Num;
      FHandScanData.HSHead.Attenuator:=FCurKanal.ATT;
      FHandScanData.HSHead.Sens:=FCurKanal.Ky;
      FHandScanData.HSHead.VRU:=FCurKanal.VRCH;

      FCurSample:=0;
      SetLength(FHandScanData.HSItem, 0);

      FHandRegistrationProcess:=true;
      FBScanStoreThread.RegMode:=FRegMode;

    end;
end;

function TDefCore.StopHandRegistration: integer;
var
  c: Integer;
  HSItem: THSItem;
begin
  //PauseRegistration(false);
  FHandRegistrationProcess:=false;
  FRegMode:=rScan;
  FBScanStoreThread.RegMode:=FRegMode;

  // ����� ������� ����������� c�������� ������ �� �������
  // �������� ���� ������ Item � ������� FFFF
  HSItem.Sample:=$FFFF;
  HSItem.Count:=0;
  FillChar(HSItem.Data, 7, 0);
  c:=Length(FHandScanData.HSItem);
  Inc(c);
  SetLength(FHandScanData.HSItem, c);
  FHandScanData.HSItem[c-1]:=HSItem;
  // ����� ��� ���������
  FDataContainer.AddHandScan(FHandScanData);
end;

function TDefCore.SetMode(Mode: Integer): Integer;
begin
  // ���� ������
  // 0 - �������� ����� ������� ����������
  // 1 - �������� ����� ��� ��� ����������
  // 2 - ������ ������������� ������ � ���
  
  //if Mode=rScan then PauseRegistration(false)
  //else PauseRegistration(true);

  Result:=SetBUMMode(Mode);
  if Result = 0 then FRegMode:=Mode;
end;

function TDefCore.GetTaktLength: Byte;
begin
  // ���������� ������������ �-��������� ������ �� ��������
  if Assigned(FCurKanal) then
    if FMeasureUnits=muMKS then result:=Round(FCurKanal.Takt.AV_mash*23.1)
    else if FMeasureUnits=muMM then result:=Round(MKS2MM(FCurKanal.Takt.AV_mash*23.1, FCurKanal.Takt.Alfa, false));
end;

function TDefCore.GetStrobEd: Byte;
begin
  case FRegMode of
     rScan, rHand: if Assigned(FCurKanal) then result:=FCurKanal.Str_En;
     rNastr, rHandNastr: if Assigned(FCurKanal) then result:=FCurKanal.NStr_En;
  end;
  //result:=FCurKanal.Str_En;
end;

function TDefCore.GetStrobSt: Byte;
begin
  case FRegMode of
     rScan, rHand: if Assigned(FCurKanal) then result:=FCurKanal.Str_St;
     rNastr, rHandNastr: if Assigned(FCurKanal) then result:=FCurKanal.NStr_St;
  end;
  //if Assigned(FCurKanal) then result:=FCurKanal.Str_St;
end;

function TDefCore.GetZMEnabled: Boolean;
begin
  if Assigned(FCurKanal) then result:=FCurKanal.Takt.UseZM;
end;

function TDefCore.GetZMValue: Word;
begin
  if Assigned(FCurKanal) then result:=FCurKanal.Takt.ZonaM;
end;

procedure TDefCore.SetZMEnabled(Value: Boolean);
begin
  {if Assigned(FCurKanal) then
    begin
      if not Value then
        begin
          FCurKanal.Takt.UseZM:=False;
          FCurKanal.Takt.ZonaM:= FCurKanal.Str_st;
        end
      else
        begin
          FCurKanal.Takt.UseZM:=true;
          FCurKanal.Takt.ZonaM:=;
        end;
    end;}
end;

procedure TDefCore.SetZMValue(Value: Word);
begin
  //if Assigned(FCurKanal) then FCurKanal.Takt.ZonaM:=Value;
  if Assigned(FCurKanal) then
    begin
      if FCurKanal.Takt.ZonaM<>Value then
        begin
          if Value=0 then
            begin
              FCurKanal.Takt.UseZM:=False;
              FCurKanal.Takt.ZonaM:= FCurKanal.Str_st;
            end
          else
            begin
              FCurKanal.Takt.UseZM:=true;
              FCurKanal.Takt.ZonaM:=Value;
            end;
          SendBum.Add( FCurKanal, cStartAView );
          SendBum.Add( FCurKanal, cInstallStrob );
        end;
    end;
end;

procedure TDefCore.SetStrobEd(Value: Byte);
begin
  if Assigned(FCurKanal) then
    begin
      case FRegMode of
      rScan, rHand: begin
                      if FCurKanal.Str_en<>Value then
                        begin
                          FCurKanal.Str_en:=Value;
                          FCurKanal.AdjustChanges[apStrobSt].Flag:=true;
                          FCurKanal.AdjustChanges[apStrobSt].CurValue:=FCurKanal.Str_st;
                          FCurKanal.Takt.InstallStrob( CAN, FRegMode, FCurKanal.AdjRailType );
                        end;
                    end;
      rNastr, rHandNastr: begin
                            if FCurKanal.NStr_en<>Value then
                              begin
                                 FCurKanal.NStr_en:=Value;
                                 FCurKanal.Takt.InstallStrob( CAN, FRegMode, FCurKanal.AdjRailType );
                              end;
                          end;
      end;

    end;
end;

procedure TDefCore.SetStrobSt(Value: Byte);
var
  flag: Boolean;
begin
  if Assigned(FCurKanal) then
    begin
      case FRegMode of
      rScan, rHand: begin
                      if FCurKanal.Str_st<>Value then
                        begin
                          FCurKanal.Str_st:=Value;
                          FCurKanal.AdjustChanges[apStrobSt].Flag:=true;
                          FCurKanal.AdjustChanges[apStrobSt].CurValue:=FCurKanal.Str_st;
                          FCurKanal.Takt.InstallStrob( CAN, FRegMode, FCurKanal.AdjRailType );
                        end;
                    end;
      rNastr, rHandNastr: begin
                            if FCurKanal.NStr_st<>Value then
                              FCurKanal.NStr_st:=Value;
                              FCurKanal.Takt.InstallStrob( CAN, FRegMode, FCurKanal.AdjRailType );
                          end;
      end;
   end;
end;

function TDefCore.GetVRCH: Byte;
begin
  if Assigned(FCurKanal) then result:=FCurKanal.VRCH;
  //if Assigned(FCurKanal) then result:=FCurKanal.Takt.VRCH.Time;
end;

procedure TDefCore.SetVRCH(Value: Byte);
begin
  if Assigned(FCurKanal) then
    begin
      if FCurKanal.VRCH<>Value then
        begin
          FCurKanal.VRCH:=Value;
          FCurKanal.AdjustChanges[apVRCH].Flag:=true;
          FCurKanal.AdjustChanges[apVRCH].CurValue:=FCurKanal.VRCH;
          SendBum.Add( FCurKanal, cInstallVRU );
        end;
    end;  
end;

function TDefCore.GetVRCHCurve: TVRCH;
begin
  if Assigned(FCurKanal) then result:=FCurKanal.Takt.VRCH.VRCH;
  //if Assigned(FCurKanal) then result:=FCurKanal.Takt.VRCH.VRCH;
end;

function TDefCore.GetScale: Integer;
begin
  if Assigned(FCurKanal) then result:=FCurKanal.Takt.AV_mash;
end;

function TDefCore.AdjustKu(N: Byte): Integer;
begin
  // ���� ������
  // 0 - ��������� ������ �������
  // 1 - ������ ��� ���������

  Result:=1;
  if Assigned(FCurKanal) then if not FCurKanal.AdjustKy(N) then Exit;
  Result:=0;
end;

function TDefCore.GetBScan(var BV: TBVBuff; GetBScanFrame: TGetBScanFrame): Integer;
var
  BVCount, i: Integer;
begin
  // ���� ������
  // 0 - B-��������� �������� ���������
  // 1 - CAN �� ����� ������ B-���������

  Result:=0;
  BVCount:=CAN.GetBV(BV);
  if BVCount=0 then Result:=1;

  for i:=0 to BVCount-1 do
     begin
       ClearBScanFrame;
       LoadBV(BV[i]);
       GetBScanFrame(FBScanFrame);
     end;
end;

function TDefCore.ImitDP(isOn: Boolean): Integer;
begin
  // ���� ������
  // 0 - �������� ������� �������/����������
  // 1 - ������ �������/��������� ���������

  Result:=0;
  if isOn<>FIsIminDP then
    begin
      if not CAN.ImitDP(isOn) then Result:=1;
      FIsIminDP:=isOn;
    end
  else Result:=1;
end;

procedure TDefCore.LoadBV(BV: TBVBuffEl);
var
  RailTakt, RailTaktPrev, i: Integer;
  Rail, Takt: Byte;
  Kan: Integer;
  A: Byte;

  Ofs: Integer;           // �������� � ������� ������ ��� ������� ���� ��.
  ECount: Byte;           // ���������� ����������� � ������.
  Echos: TEchoBlock;      // ���������� (���-����) - �� 8 ���� (���-�� � ECount)
  BlockSize: Integer;     // ���������� ���� � ����� �� ���.

  Complete: Boolean;

  function GetByte: Byte;
  begin
    Result:= BV.Data[Ofs];
    Inc( Ofs );
  end;

begin
  ECount:= 0;
  Ofs:= 0;
  RailTaktPrev:= -1;
  repeat   // ���� �� ������� ������ �� ����� ���� ��.

    if Ofs < BV.DataCount - 1 then
    begin
      BlockSize:= GetByte;
      RailTakt:= GetByte;
      Complete:= False;
    end else Complete:= True;

    if ( RailTaktPrev <> -1 ) and ( Complete or ( RailTaktPrev <> RailTakt ) ) then
    begin
      //if Kan <> -1 then FDataContainer.AddEcho( Rail, Kan, ECount, Echos );
      if Kan <> -1 then
        begin
          FBScanFrame[Rail, Kan].EchoCount:=ECount;
          FBScanFrame[Rail, Kan].Echos:=Echos;
        end;
      ECount:= 0;
//      Ofs:= 0;
    end;

    if Complete then Exit; // �����.

    RailTaktPrev:= RailTakt;

    Rail:= Ord( RailTakt and $80 = $80 );
 //   Inc( Ofs );
    Takt:= RailTakt and $7F;

    if Takt <= High( Takts[ Rail xor Config.WorkRail ] ) then Kan:= Takts[ Rail xor Config.WorkRail, Takt ].Kan1 else Kan:= -1;

    case BlockSize of
    3:
      begin
        Echos[ECount + 0].T:= GetByte;
        A:= GetByte;
        Echos[ECount + 0].A:= A and $F0 shr 4;
        Inc( ECount, 1 );
      end;
    4:
      begin
        Echos[ECount + 0].T:= GetByte;
        Echos[ECount + 1].T:= GetByte;
        A:= GetByte;
        Echos[ECount + 0].A:= A and $F0 shr 4;
        Echos[ECount + 1].A:= A and $0F;
        Inc( ECount, 2 );
      end;
    6:
      begin
        Echos[ECount + 0].T:= GetByte;
        Echos[ECount + 1].T:= GetByte;
        Echos[ECount + 2].T:= GetByte;
        A:= GetByte;
        Echos[ECount + 0].A:= A and $F0 shr 4;
        Echos[ECount + 1].A:= A and $0F;
        A:= GetByte;
        Echos[ECount + 2].A:= A and $F0 shr 4;
        Inc( ECount, 3 );
      end;
    7:
      begin
        Echos[ECount + 0].T:= GetByte;
        Echos[ECount + 1].T:= GetByte;
        Echos[ECount + 2].T:= GetByte;
        Echos[ECount + 3].T:= GetByte;
        A:= GetByte;
        Echos[ECount + 0].A:= A and $F0 shr 4;
        Echos[ECount + 1].A:= A and $0F;
        A:= GetByte;
        Echos[ECount + 2].A:= A and $F0 shr 4;
        Echos[ECount + 3].A:= A and $0F;
        Inc( ECount, 4 );
      end;
    end; // case.
  until False; // ���� �� ������ Data.
end;

procedure TDefCore.ClearBScanFrame;
var
  i, j: Integer;
begin
  for i:=0 to 1 do
     for j:=0 to 7 do
       begin
         FBScanFrame[i, j].EchoCount:=0;
         FillChar(FBScanFrame[i, j].Echos, 16, 0);
       end;
end;

procedure TDefCore.HandScanReceived(HSItem: THSItem);
var
  c: Integer;

begin
  if FHandRegistrationProcess then
    begin
      if HSItem.Sample=0 then Inc(FCurSample);
      HSItem.Sample:=FCurSample;
      c:=Length(FHandScanData.HSItem);
      Inc(c);
      SetLength(FHandScanData.HSItem, c);
      FHandScanData.HSItem[c-1]:=HSItem;
    end;
end;

procedure TDefCore.SetSndKanal(Value: Boolean);
begin
  if Assigned(FCurKanal) then
    begin
      FSndByOneKanal:=Value;
      if Value then Snd.SetSndKanal(FCurRail, FCurrentKanal)
      else Snd.SetSndKanal(FCurRail, -1);
    end;
end;

procedure TDefCore.SetSndOn(Value: Boolean);
begin
  {if (not FSndOn) and (Value) then Snd.Resume
  else if (FSndOn) and (not Value) then
    begin
      Snd.Suspend;
      Snd.StopTone;
    end; }
  FSndOn:=Value;
  if not FSndOn then
    begin
      FOldVolume:=Snd.Volume;
      Snd.Volume:=0;
    end
  else
    Snd.Volume:=FOldVolume;
end;

function TDefCore.GetSndVolume: Integer;
begin
  Result:=Snd.Volume;
end;

procedure TDefCore.SetSndVolume(Value: Integer);
begin
  Snd.Volume:=Value;
end;

function TDefCore.GetASD(Rail, Channel: Integer): Boolean;
begin
  Result:=CAN.ASD[Rail, Channel];
end;

function TDefCore.GetAK(Rail, Channel: Integer): Boolean;
begin
  Result:=CAN.AK[Rail, Channel];
end;

function TDefCore.GetAKLevel(Rail, Channel: Integer): Integer;
begin
  if Assigned(Kanals[Rail, Channel]) then
    Result:=Kanals[Rail, Channel].AK_Level;
end;

procedure TDefCore.SetAKLevel(Rail, Channel, Level: Integer);
var
  i, j: Integer;
begin
  if Assigned(Kanals[Rail, Channel]) then
    begin
      if Level>MaxAK_Level then Level:=MaxAK_Level;
      if Level<0 then Level:=0;
      if Kanals[Rail, Channel].AK_Level<>Level then
        begin
          Kanals[Rail, Channel].AK_Level:=Level;
          SendBum.Add( Kanals[Rail, Channel], cInstallVRU );
        end;
    end;
end;

function TDefCore.GetTaktLengthMKS: Byte;
begin
  if Assigned(FCurKanal) then result:=Round(FCurKanal.Takt.AV_mash*23.1);
end;

function TDefCore.GetKanal(Rail, Num: integer): TKanal;
begin
  if Length(Kanals)>0 then Result:=Kanals[Rail, Num];
end;

function TDefCore.GetCurDisCoord: Integer;
begin
  if Assigned(FDataContainer) then result:=FDataContainer.MaxDisCoord;
end;

function TDefCore.GetReg: TAvk11DataContainer;
begin
  if Assigned(FDataContainer) then Result:=FDataContainer;
end;

procedure TDefCore.SetRegistrator(const Value: TAvk11DataContainer);
begin
  FRegistrator := Value;
end;

end.
