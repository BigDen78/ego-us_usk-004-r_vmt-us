/// //////////////////////////////////////////////////
/// /                                             ////
/// / ���� ������������������ ����� ��������� ��� ////
/// /                                             ////
/// //////////////////////////////////////////////////

unit DefCoreUnit;
{$I Directives.inc}

interface

uses Forms, SysUtils, Windows, MCAN, { Avk11Engine } DataFileConfig,
  AirBrushUnit,
  AviconDataContainer, AviconTypes, BScanThreadUnit, PublicData, Math,
  ConfigObj,
  ExtCtrls, SyncObjs,
  MyTypes, SoundControlUnit, Dialogs, DeviceInterfaceUnit, Classes, PublicFunc,
  MessageUnit,
  Graphics, AviconDataSource, ProgramProfileConfig;
// NordcoCSV;

const
  MaxCANCount = 15;
  // GranatBUISerialNum = 10999;
  GranatBUIVersB1 = 1;
  GranatBUIVersB2 = 22;

  MaxAirBrushTaskCount = 20;

type
{$IFDEF OldEngine}
  TAviconID = uint64;
{$ENDIF}

  TRegParamList = record
    Organization: string;
    Operator: string;
    DirCode: Integer;
    StartKM: Integer;
    StartPK: Integer;
    StartM: Integer;
    TrackNumber: Integer;
    Direction: Integer;
    PeregonName: string;
    ScanStep: Word;
    Scheme: Byte;
    BUISN: string;
    BUIFV: string;
    BUM1SN: string;
    BUM1FV: string;
    BUM2SN: string;
    BUM2FV: string;
    AKPSN: string;
    AKPFV: string;

    // ������������������ ���� ��� ������
    DirectionID: string;
    TrackID: string;
    Chainage: string;
    LeftSide: Integer;
  end;

  TAScanMeasureResult = record
    R, H, L, N, Kd, Att: Integer;
  end;

  // ��������� ��� �������� B-���������
  TEcho = packed record
    Delay: Byte;
    Amp: Byte;
  end;

  TEchoMas = array [0 .. 7] of TEcho;

  TBScanChanel = packed record
    EchoCount: Byte;
    Echos: TEchoBlock;
  end;

  TBScanFrame = array [0 .. 1, 0 .. 9] of TBScanChanel;

  TMeasureUnits = (muMKS, muMM);

  TTextMessageEvent = procedure(Msg: string; NewLine: Boolean) of object;
  TGetBScanFrame = procedure(var BScanFrame: TBScanFrame) of object;
  TRegStatus = (rsOff, rsOn, rsPause);

  // TEchoBlock
  TBScanArray = array [0 .. 1, 0 .. 15] of TBScanChanel;
  // TBScanArray = array [0 .. 1, 0 .. 13] of TBScanChanel;
  // TBScanArray = array[0..1, 0..9] of TBScanChanel;

  TRegFileModes = (rmNastr = 0, rmNastrHand = 1, rmEvaluation = 2, rmHand = 3,
    rmSearchB = 4, rmSearchM = 5, rmMenu = 6, rmPause = 7, rm2TP = 8,
    rm2TPHand = 9, rmNone = 99);

  TDefCore = class
  private
    FLogFile: Text;
    FTickStart: Cardinal;
    FTerminate: Boolean;
    FRegMode: Byte; // ������� ����� ����������� (���� ������ ��. ������ PublicData)
    FCurRail: Integer; // ������� �����
    FCurrentKanal: Integer; // ����� �������� ������
    // FCurKanalMode: Integer;     // ����� � ������� �������� ������� ����� (���� ������ ��. ������ PublicData)
    FCurKanal: TKanal; // ������ �� ������� �����
    FBScanStoreThread: TBScanStoreThread; // ������ ��� �������� CAN -> DataContainer
    // FHandStoreThread: THandStoreThread; // ������ ��� �������� ������� CAN -> DataContainer
    FRegStatus: TRegStatus; // ������� ������ �����������. ���������, �������� ��� �� �����

    /// / ---- ����� ������ ����������� ----

    ConfigList: TDataFileConfigList;
    FDataContainer: TAviconDataContainer;
    FHandContainer: TAviconDataContainer;
    // FExternFileData: TAviconDataSource;
    ///

    RegFileName: string; // ��� ����� ��� �����������, �������� � StartRegistration(),
    // ����� ��� �����-� ����� .csv �����

    FAScanMeasureResult: TAScanMeasureResult;
    // ��� ������� ������
    // FHSDat: THandScanData;
    FBScanFrame: TBScanFrame;
    // FHandScanData: THandScanData;
    FHandRegistrationProcess: Boolean;
    FCurSample: Word;
    FIsIminDP: Boolean;
    FSndByOneKanal: Boolean;
    FSndOn: Boolean; // ������� �� ����
    FOldVolume: Integer;
    FMeasureUnits: TMeasureUnits;
    // FRegistrator: TAviconDataContainer;
    FAdjustRailTypeProccess: Boolean;

    // ��� ����������
    FECount: Integer;
    Fechos: TEchoBlock;

    // Fechos: TBlokEcho;
{$IF DEFINED(BUMCOUNT2) OR DEFINED(BUMCOUNT4)}
    FUSB: TDeviceMAKP;
{$ELSE}
    FUSB: TDeviceAKP;
{$IFEND}
    FCANList: TCanList;
{$IFDEF SecondUSBRemotePanel}
    RemotePanel: TRemotePanel;
{$ENDIF}
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
    FlSidesChanged: Boolean;
{$IFEND}
    // FNeedToAddBum: Boolean;
    FNewBUMID: Byte;
    // FCANCount: Integer;
    // FCANMas: array[0..MaxCANCount] of TCAN;
    FFRegOnUserLevel: Boolean;
    FLastBScan: TBScanArray;
    ShowErrorTimer: TTimer;
    FSendError: Boolean;
    FSendErrorCount: Integer;

    LogCS: TCriticalSection;
    CS_DC: TCriticalSection;

    FSensorChanged: Boolean;
    FSensorMsg: string;
    FSensorJointCnt: Integer;
    FSensorSwitchCnt: Integer;
    FBoltJointMode: Boolean;

    BM_K1StrEn: array [0 .. 1] of Byte;
    BM_K11StrEn: array [0 .. 1] of Byte;
    BM_K1StrSt: array [0 .. 1] of Byte;
    BM_K11StrSt: array [0 .. 1] of Byte;

    (* <Rud30> *)
    BM_K6StrSt: array [0 .. 1] of Byte;
    BM_K7StrSt: array [0 .. 1] of Byte;
    (* </Rud30> *)

    FHSHead: THSHead;
    FHandRegStartCoord, FHandRegEndCoord: Integer;
    // ��� ����������� ������� ������

    // ��� �������������
    FMasterIsSet: Boolean;

    // ������ ��� �������������� ���������� �������
    RegistrationTimer: TTimer;

    FFullCoord: array [0 .. 1] of Integer;

    FGlobalFileType: TAviconID; // ���� ����� ������� ��� ConfigList'a ���������� � ����� ����� ������� � ����� ������ ������������� �� ������ ��������

    FisPainterOn: Boolean; // ��������� ��������� �����������������
    FisPaintSprayerOn: Boolean; // �������� ������ ��� ���

    FHandRail, FHandSurface: Integer;

    FEnterModeTime: Int64;
    FPrevMode: TRegFileModes;

    FVolumeBeforePause: Integer;

    FAirBrushTaksMas: array [0 .. 1, 0 .. MaxAirBrushTaskCount - 1] of Integer;

    FLastExternCoord: Integer;
    FCurExternCoord: Integer;

    MinCoordTaskIdx: array [0 .. 1] of Integer;

    FOldCoord: Integer;

    FirstTimeStr: Boolean;

    procedure _WrLog_(Msg: string);
    function WinMessage(Msg: string; WinType: Byte): Boolean; // WinType = 0 - ������, 1 - ������, 2 - ��������, 3 - ����;
    function InstallTakts( { TArray: array of TTakt } ): Integer;
    function InstallOneKanal(Kanal: TKanal): Integer;
    function SetBUMMode(RegMode: Byte): Integer;
    procedure ClearBScanFrame;
    procedure LoadBV(BV: TBVBuffEl);
    procedure HandScanReceived(HSItem: THSItem);
    // procedure ReceiveMScan( MBuff: PMBuff );
    // function SignProc(StartDisCoord: Integer): Boolean;
    procedure BScanRecieved(BV: TBVBuffEl);
    procedure AKPConnected(ID: Integer);
    procedure AKPDisconnected(ID: Integer);
    function GetUSBDevicesCount: Integer;
    function GetBUMSN(ID: Byte): Word;
    function GetCAN(ID: Byte): TCAN;
    function GetCANByIdx(Index: Integer): TCAN;
    function GetCANCount: Integer;
    function CANStart: Boolean; // ��������� ������ ����
    function CANStop: Boolean; // ���������� ������ ����
    procedure CANASDTurnOn;
    procedure CANASDTurnOff; // ����. ���
    procedure CANImitDP(isOn: Boolean);
    procedure SetRegistrator(const Value: TAviconDataContainer);
    procedure OnExceptionFinal(LogSaved: Boolean);
    procedure OnMViewDecode(Rail, Takt: Byte; Echos: TDecodedEchos;
      AdditionalData: Integer);
    procedure AdjRailType(Rail: Byte; Tm: Integer);
    procedure AdjRailTypeZeroChannel(Rail: Byte; Tm, KP: Integer);
    procedure AdjRailTypeNOTZeroChannels(Rail: Byte; Tm: Integer);
    procedure AdjRailHand(Tm: Integer);
    procedure UpDateStrobAndVRU(Kanal: TKanal);
    procedure AKPSendError;
    procedure OnShowErrorTimer(Sender: TObject);

    procedure SensorStateChanged(SensorKind: TSensorKind;
      StateChangeDirection: TStateChangeDirection);
    procedure FullCoordChanged(BumID, Coord: Integer);
    procedure OneSensorStateChanged(SensorType, BumID: Integer; State: Boolean);

    procedure SetAllHKanalsNOTActive;

    procedure InitHandRegThread;
    procedure KillHandRegThread;

    procedure SetReleMode(Mode: Byte);

    procedure AirBrushEvent(Rail: TRail; DisCoord, SysCoord: Integer);
    procedure AirBrushEvent2(Rail: TRail; DisCoord, SysCoord: Integer;
      RealCrd: Integer; ABItems: TABItemsList);

    // ������������ ������� ��� ���������� � ���� ����������� ����� �������
    procedure OnRegTimer(Sender: TObject);

    procedure AirBrushDone(CAN_ID: Byte; Coord, ErrCode: Integer; Delta: Word);

  protected
    procedure SetBoltJointMode(Mode: Boolean);
    procedure SetKu(Value: Integer);
    function GetKu: Integer;
    procedure Set2tp(Value: Single);
    function Get2tp: Single;
    function GetTaktLength: TAScanLength;
    function GetTaktLengthMKS: Byte;
    procedure SetStrobSt(Value: Byte);
    function GetStrobSt: Byte;
    procedure SetStrobEd(Value: Byte);
    function GetStrobEd: Byte;
    procedure SetZMValue(Value: Word);
    function GetZMValue: Word;
    procedure SetZMEnabled(Value: Boolean);
    function GetZMEnabled: Boolean;
    procedure SetVRCH(Value: Byte);
    function GetVRCH: Byte;
    function GetVRCHCurve: TVRCH;
    function GetScale: Integer;
    procedure SetSndKanal(Value: Boolean);
    procedure SetSndOn(Value: Boolean);
    function GetSndVolume: Integer;
    procedure SetSndVolume(Value: Integer);
    function GetASD(Rail, Channel: Integer): Boolean;
    function GetAK(Rail, Channel: Integer): Boolean;
    procedure SetAKLevel(Rail, Channel, Level: Integer);
    function GetAKLevel(Rail, Channel: Integer): Integer;
    function GetKanal(Rail, Num: Integer): TKanal;
    function GetCurDisCoord: Integer;
    function GetCurRealCoord: TRealCoord;
    function GetMovingForward: Boolean;
    function GetReg: TAviconDataContainer;
    procedure SetFRegOnUserLevel(Value: Boolean);
  public
    FAirBrush: TAirBrush;
    SensorStateBum1: Integer;
    SensorStateBum2: Integer;
    Adj2Tp: Boolean;
    BumFailed: Boolean;
    BumDPSignalDelta: Integer;
    AV14USBOpened: Boolean;
    OnBScanReserved: procedure(BV: TBVBuffEl) of object;
    OnTextMessage: TTextMessageEvent; // ���������� ���������� ��������� ��������� (� �������� ��� ��������)
    TwoThreadDevice: Boolean;
    CoordMark: Boolean;
    LastDonnEnvelope: array [0 .. 1] of record New: Boolean;
    Ampl: Byte;
  end;

  // {$IFDEF USK004R}
  // FNordcoCSVFile : TNordcoCSVFile;
  // RecNordcoCSV: TNordcoCSVRecordLine;
  // {$ENDIF}
  // CurPainterState : TPainterState;
UseRegistratorCoord :
Boolean;
DisDifExternAndRegFile :
Integer;
AdditionalFileToShow :
Boolean;
FExternFileData :
TAviconDataSource;
ExternStartKM :
Integer;
ExternStartPK :
Integer;
ExternStartM :
Integer;
WaitingForBUM :
Boolean;
constructor Create;
destructor Destroy; override;
procedure ButtonStateChange(Button: TConsolButtons; State: Boolean);
procedure RunKraskopult(Rail: Byte);
function Boot_LoadConfig: Integer; // �������� �������
function Boot_TryRunBUM: Integer; // ������� ������� ���
procedure CAN_SendTest;
procedure AddBUM(ID: Integer);
procedure RemoveBUM(ID: Integer);
procedure StopDeviceSearching;
procedure Terminate; // ���������� ��������� ����������� ������ (���� ������������ ������ ��� ��������� ��������)
function GetAScan(var AData: PAVBuff): Integer; // �������� �-���������. �������� ��������� �� ����� �-���������, ������ ��� ���� �������� �� �����
function GetBScan(ID: Byte; var BV: TBVBuff; GetBScanFrame: TGetBScanFrame)
  : Integer; // �������� B-��������� ��������. ������ B-��������� ������� �������� �� ������� ����������� - ��� �����
function GetBScanDP(ID: Byte; var Count: Integer; var BV: TBVBuff): Integer; // �������� B-��������� ��������. ������ B-��������� ������� �������� �� ������� ����������� - ��� �����
function SetMode(Mode: Integer): Integer; // ��������� �������� ������ ������ (�����-������, ���������, ������, ��������� �������)
function StartTempRegistration: Integer;
function StartRegistration(FileType: TAviconID; FileName: String;
  RegParamList: TRegParamList; RealReg: Boolean): Integer;
function PauseRegistration(IsPause: Boolean): Integer; // ���������� ������ � ����������� �������� true - ��������� �����, false - ����� �����
function StopRegistration: Integer; // ��������� �����������
function SetAScanKanal(Rail, Num: Integer): Integer; // ������ ������� ����� � ����������� � ������� �������
function StopAScan: Integer;
function StartHandRegistration(Rail, ScanSurf: Byte): Integer;
// ������ ����������� �������
function StopHandRegistration: Integer; // ���������� ����������� �������
function AdjustKu(N: Integer): Integer; // ������� ���������� � ������ ��������� ��� ��������� ������
function ImitDP(ID: Byte; isOn: Boolean): Integer;
// ���/���� �������� ������� ����
procedure AdjustRailType;
procedure AdjustDefaultRailType;
// procedure PrepareChannels;
function GetLastBScan: TBScanArray;
procedure Av14USBOpen;
function GetUAKK: Integer;
function GetU2CSN(ID: Byte): Integer;
function RealU2CID(ID: Byte): Byte;
procedure GetButtonSt(B: Byte);
(* <Rud45> *)
function GetU2CVersion(ID: Byte): String;
(* </Rud45> *)

// �����������
procedure StartPainter;
procedure StopPainter;

procedure ChangeModeInRegFile(NewMode: TRegFileModes; Rail, Ch: Integer);

property BoltJointMode: Boolean read FBoltJointMode write SetBoltJointMode;

procedure DonnEnvelopeReserved(Rail, Takt, Amplify: Byte; Delay: Single);

procedure CheckAirBrushTasks(CurDisCoord: Integer);

procedure LoadExternFile(FileName: string);
procedure NextExternCoord(Delta: Integer);
procedure InsertWholeExternFile;
procedure CloseExternFile;
function CountExternAndRegDiff: Integer;

function ProcSign(StartDisCoord: Integer): Boolean;

procedure OnCoordChange(Coord: Integer);

function GetVelocity: Single;

procedure TestRunAirBrush;

procedure HandRailType;

procedure PauseRegThread;
procedure UnPauseRegThread;
function ResetHandContainer: TAviconDataContainer;

procedure Stop;
procedure Resume;
{$IFDEF BUMCOUNT4}
procedure ChangeLeftRightSide(); // ������ ����� � ������ ���� � ������� ������� CAN.ID
{$ENDIF}
procedure OnAlarmEvent(Rail: TRail; Channel: Integer; State: Boolean);
procedure SetPaintSprayer(isOn: Boolean);
property PaintSprayerOn: Boolean read FisPaintSprayerOn write SetPaintSprayer;
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US) }
procedure ChangeFrontAndBack(); // ������ ���������� � ����������� ������ �������
procedure SetChan0Regim(SelectChan0: TSelectChan0);
{$IFEND}

// function MakeNewHandContainer: TAviconDataContainer;

property RegistrationStatus: TRegStatus read FRegStatus; // ������� ��������� ����������� (��� ���� ������ � "�������" �����������, �� � ������)
property Ku: Integer read GetKu write SetKu; // �������� ���������������� �������� ������
property TwoTp: Single read Get2tp write Set2tp;
// ����� � ������ �������� ������
property Mode: Byte read FRegMode; // ������� ����� ������ ��� (���� ������ ��. ������ PublicData)
property AScanLength: TAScanLength read GetTaktLength; // ������������ �-��������� (����� ����������� �� ������� ��� ����� �� �����)
// property AScanLengthMKS: Byte read GetTaktLengthMKS; // ������������ �-��������� (����� ����������� �� ������� ��� ����� �� �����)
property StrobSt: Byte read GetStrobSt write SetStrobSt;
// ������ ������ �������� ������
property StrobEd: Byte read GetStrobEd write SetStrobEd;
// ����� ������ �������� ������
property ZMEnabled: Boolean read GetZMEnabled write SetZMEnabled;
// ���� ����� ���/����
property ZMPosition: Word read GetZMValue write SetZMValue;
// ������� ���� �����
property VRCH: Byte read GetVRCH write SetVRCH; // �������� ���
property VRCHCurve: TVRCH read GetVRCHCurve; // ������ ��� ��������� �� ����� (���� read-only, ����� ���� ������� ����� ����� ���� ������������ ������ �������� � ����)
property MeasureResult: TAScanMeasureResult read FAScanMeasureResult;
// ������ � ����������� ����������� (R, H, N, L, Kd)
property ScaleA: Integer read GetScale; // ������� �-���������
property SndByOneKanal: Boolean read FSndByOneKanal write SetSndKanal;
property SndOn: Boolean read FSndOn write SetSndOn;
// ���� true �� ���� �������, ����� ��������
property SndVolume: Integer read GetSndVolume write SetSndVolume;
property ASD[Index1, Index2: Integer]: Boolean read GetASD;
property AK[Index1, Index2: Integer]: Boolean read GetAK;
property AKLevel[Index1, Index2: Integer]
  : Integer read GetAKLevel write SetAKLevel;
property MeasureUnits: TMeasureUnits read FMeasureUnits write FMeasureUnits;
property Kanal[Rail, Num: Integer]: TKanal read GetKanal;
// property Registrator: TAvk11DataContainer read GetReg;
property Registrator: TAviconDataContainer read GetReg;
property USBDevicesCount: Integer read GetUSBDevicesCount;
property BUMSerialNum[index: Byte]: Word read GetBUMSN;
// property NeedToAddBum: Boolean read FNeedToAddBum write FNeedToAddBum;
property NewBUMID: Byte read FNewBUMID;
property CANs[index: Byte]: TCAN read GetCAN;
property CANList[Index: Integer]: TCAN read GetCANByIdx;
property CanCount: Integer read GetCANCount;
property BScanStoreThread: TBScanStoreThread read FBScanStoreThread;
property RegOnUserLevel: Boolean read FFRegOnUserLevel;
property FRegOnUserLevel
  : Boolean read FFRegOnUserLevel write SetFRegOnUserLevel;
property CurDisCoord: Integer read GetCurDisCoord;
property CurRealCoord: TRealCoord read GetCurRealCoord;
property MovingForward: Boolean read GetMovingForward;
property HandRegistrationOn: Boolean read FHandRegistrationProcess;
property CurrentKanalNum: Integer read FCurrentKanal;
end;

implementation

uses MainUnit;

{ TDefCoreUnit }

procedure TDefCore.ChangeModeInRegFile(NewMode: TRegFileModes;
  Rail, Ch: Integer);
var
  AddChInfo: Boolean;
  TimeInMode: Word;
begin
  case NewMode of
    rmNastr:
      AddChInfo := true;
    rmNastrHand:
      AddChInfo := true;
    rmEvaluation:
      AddChInfo := true;
    rmHand:
      AddChInfo := true;
    rmSearchB:
      AddChInfo := false;
    rmSearchM:
      AddChInfo := false;
    rmMenu:
      AddChInfo := false;
    rmPause:
      AddChInfo := false;
    rm2TP:
      AddChInfo := true;
    rm2TPHand:
      AddChInfo := true;
  end;
  if FPrevMode = rmNone then
    TimeInMode := 0
  else
    TimeInMode := Round((GetTickCount - FEnterModeTime) / 1000);

  FPrevMode := NewMode;
  FEnterModeTime := GetTickCount;

  if (Assigned(FDataContainer)) and (FRegStatus <> rsOff) then
    FDataContainer.AddMode(Ord(NewMode), TimeInMode, AddChInfo, TRail(Rail),
      Ch);
end;

constructor TDefCore.Create;
var
  i: Integer;
begin
  WaitingForBUM := false;
  SensorStateBum1 := -1;
  SensorStateBum2 := -1;
  AdditionalFileToShow := false;
  ExternStartKM := 0;
  ExternStartPK := 0;
  ExternStartM := 0;
  FOldCoord := 0;
  FLastExternCoord := 0;
  FCurExternCoord := 0;
  UseRegistratorCoord := true;
  FExternFileData := nil;
  for i := 0 to High(FAirBrushTaksMas) do
  begin
    FAirBrushTaksMas[0, i] := -1;
    FAirBrushTaksMas[1, i] := -1;
  end;

  FPrevMode := rmNone;
  FisPainterOn := false;
  FisPaintSprayerOn := true;
  Adj2Tp := false;
  // CurPainterState := Config.PainterState;
  RegistrationTimer := TTimer.Create(nil);
  RegistrationTimer.Interval := 1000;
  RegistrationTimer.Enabled := false;
  RegistrationTimer.OnTimer := OnRegTimer;

  FGlobalFileType := Avicon16Scheme1;

  FFullCoord[0] := 0;
  FFullCoord[1] := 0;

  ConfigList := TDataFileConfigList.Create('');
  // FHandStoreThread := nil;
  FMasterIsSet := false;
  CoordMark := false;
  WrLog := Self._WrLog_;
  FSensorJointCnt := 0;
  FSensorSwitchCnt := 0;

  AV14USBOpened := false;

  LogCS := TCriticalSection.Create;
  // FNeedToAddBum:=false;
  FRegOnUserLevel := false;
  FNewBUMID := 0;
  OnBScanReserved := nil;
  OnTextMessage := nil;
  // FCanCount:=0;
  FAdjustRailTypeProccess := false;
  FTerminate := false;
  FSndByOneKanal := false;
  FSndOn := true;
  FMeasureUnits := muMKS;
  FHandRegistrationProcess := false;
  FIsIminDP := false;
  FTickStart := GetTickCount;
  FOldVolume := 100;
  KConf := TKanConfig.Create;

  FSendErrorCount := 0;
  FSendError := false;

  ShowErrorTimer := TTimer.Create(nil);
  ShowErrorTimer.OnTimer := OnShowErrorTimer;
  ShowErrorTimer.Interval := 1000;
  ShowErrorTimer.Enabled := true;

  chdir(ExtractFilePath(Application.ExeName));
  chdir('SYSTEM');
  Assign(FLogFile, 'log.txt');
  Rewrite(FLogFile);
{$IFDEF UseHardware}
{$IF DEFINED(BUMCOUNT2) OR DEFINED(BUMCOUNT4)}
  FUSB := TDeviceMAKP.Create;
  FUSB.AddLogProc := _WrLog_;
  FUSB.AKPConnected := AKPConnected;
  FUSB.AKPDisconnected := AKPDisconnected;
  FUSB.OnInternalThreadException := ProcessException.RaiseException;
{$ELSE}
  FUSB := TDeviceAKP.Create;
  FUSB.AddLogProc := _WrLog_;
  FUSB.OnInternalThreadException := ProcessException.RaiseException;
  FUSB.OnButtonStateChange := ButtonStateChange;
{$IFEND}
  FUSB.OnSendError := AKPSendError;
{$ENDIF}
{$IFDEF SecondUSBRemotePanel}
  RemotePanel := TRemotePanel.Create;
  RemotePanel.OnButtonStateChange := ButtonStateChange;
{$ENDIF}
  ProcessException.OnExceptionFinal := OnExceptionFinal;

  FCANList := TCanList.Create;

  Snd := TSndControl.Create;

  FDataContainer := nil;
  FHandContainer := nil;
  FCurKanal := nil;
  FRegStatus := rsOff;
  CS_DC := TCriticalSection.Create;
end;

destructor TDefCore.Destroy;
var
  i: Integer;
  t1, t2, t3: Int64;
begin
  CS_DC.Free;
  CloseExternFile;
  KillHandRegThread;
  if FRegStatus = rsOn then
  begin
    FRegOnUserLevel := false;
    StopRegistration;
  end;
  ConfigList.Free;

  // KConf.SaveUserConfig(0);
  // KConf.Free;

  // CAN.Free;
  Snd.Free;
  Self.StopPainter;
  for i := 0 to FCANList.Count - 1 do
    TCAN(FCANList.Items[i]).Stop;
  FUSB.Free;

  for i := 0 to FCANList.Count - 1 do
    TCAN(FCANList.Items[i]).Free;
  FCANList.Free;
  // FUSB.Close;
  // FUSB.Free;
{$IFDEF SecondUSBRemotePanel}
  RemotePanel.Free;
{$ENDIF}
  CloseFile(FLogFile);
  inherited;
  LogCS.Free;
end;

procedure TDefCore.AirBrushEvent(Rail: TRail; DisCoord, SysCoord: Integer);
var
  A, B, i, j: Integer;
  CAN: TCAN;
  // CurIdx: Integer;
  // inCoord: array [0 .. 1] of Integer;
  idxCAN: Byte;
begin
{$IFDEF EGO_USW OR IFDEF VMT_US}
  idxCAN := RailToBUM(Byte(Rail), Config.DirectionOfMotion);
  if idxCAN < FCANList.Count then
    CAN := FCANList[idxCAN]
  else
    Exit;
{$ELSE}
  if Ord(Rail) >= FCANList.Count then
    Exit;
  CAN := FCANList[Ord(Rail)];
{$ENDIF}
  if not Assigned(CAN) then
    Exit;

  A := 0;
  if Assigned(FDataContainer) then
    A := FDataContainer.CurSaveSysCoord;

  B := CAN.BUMCoord;

  if (Config.PaintSprayer = prByAlgorithm)
    and FisPainterOn and FisPaintSprayerOn then
    CAN.PainterSetMark(B + (A - SysCoord));
end;

procedure TDefCore.OnRegTimer(Sender: TObject);
begin
  if RegistrationStatus = rsOn then
  begin
    FDataContainer.AddTime(Frac(Now));
  end;
end;

procedure TDefCore.StartPainter;
var
  i, Coord: Integer;
  ConfigFileName, s: string;
begin
  ConfigFileName := Config.AirBrushConfig;
  s := ExtractFilePath(Application.ExeName) + ConfigFileName;
  if not FileExists(ExtractFilePath(Application.ExeName) + ConfigFileName) then
    ConfigFileName := 'default001.abp';

  FAirBrush := TAirBrush.Create(FDataContainer, AirBrushEvent { nil } ,
    { AirBrushEvent2, } nil, ExtractFilePath(Application.ExeName)
      + ConfigFileName, true, tpLower); // true
  // FAirBrush.Tick;
  // FAirBrush.Start; �� ����� ����� ��������
  // FAirBrush.SetJobEvent:= ;
  if Assigned(FDataContainer) then
    Coord := FDataContainer.CurSaveSysCoord
  else
    Coord := 0;

  for i := 0 to FCANList.Count - 1 do
  begin
    TCAN(FCANList.Items[i]).PainterOn(1000);
    // TCAN(FCANList.Items[i]).PainterSetCoord(Coord);
    // TCAN(FCANList.Items[i]).OnOneSensorStateChanged:= OneSensorStateChanged;
    // TCAN(FCANList.Items[i]).OnFullCoordRecieved:= FullCoordChanged;
  end;
  // CS_DC.Acquire;
  for i := 0 to FCANList.Count - 1 do
  begin
    // TCAN(FCANList.Items[i]).FlIsInCriticalSection:=True;
    TCAN(FCANList.Items[i]).PainterSetCoord(Coord);
    // TCAN(FCANList.Items[i]).FlIsInCriticalSection:=False;
  end;
  // CS_DC.Release;

  FAirBrush.AlarmEvent := OnAlarmEvent;
  // FisPainterOn := true;
end;

procedure TDefCore.StopPainter;
var
  i: Integer;
begin
  if Assigned(FAirBrush) then
  begin
    // FAirBrush.SetJobEvent:= nil;
    FAirBrush.Free;
    FAirBrush := nil;
  end;
  for i := 0 to FCANList.Count - 1 do
  begin
    TCAN(FCANList.Items[i]).PainterOff;
    // TCAN(FCANList.Items[i]).OnOneSensorStateChanged:= nil;
    // TCAN(FCANList.Items[i]).OnFullCoordRecieved:= nil;
  end;
  FisPainterOn := false;
end;

function TDefCore.InstallTakts( { TArray: array of TTakt } ): Integer;
var
  i, j, L: Integer;
  KT: Byte;
  CAN: TCAN;
  Arr: TSingleTaktArray;
begin
  // ���� ������
  // 0 - ��������� ������ ������ ���������
  // 1 - ������ ��������� �����
  // 2 - ������ ������������� �����

  for i := 0 to FCANList.Count - 1 do
  begin
    CAN := TCAN(FCANList.Items[i]);
    CAN.Stop;
    Arr := Takts[CAN.ID, 0];
    KT := Length(Arr);
    Result := 2;
    if not CAN.InitCikl(KT) then
      Exit;
    Result := 1;
    // for j := 0 to KT - 1 do
    // Arr[j].SetRegMode(FRegMode);
    for j := 0 to KT - 1 do
      if Assigned(Arr[j]) then
      begin
        if not Arr[j].Install(FRegMode) then
          Exit;
      end
      else if not Takts[CAN.ID, 1, j].Install(FRegMode) then
        Exit; // AK
  end;

  Result := 0;
end;

function TDefCore.PauseRegistration(IsPause: Boolean): Integer;
begin
  // ���� ������
  // 0 - ��������� �����/������ ����� ������ �������
  // 1 - ������ ����� ����� ��� �������
  // 2 - ����������� � ��� ���������, ����� ������������

  Result := 1;

  if FRegOnUserLevel = false then
    Exit;

  if ((IsPause) and (FRegStatus = rsPause)) or
    ((not IsPause) and (FRegStatus <> rsPause)) then
    Exit;

  if FRegStatus = rsOff then
    Exit;

  // !!!!
  { if Assigned(FBScanStoreThread) then
    FBScanStoreThread.Pause := IsPause; }

  // if Assigned(FHandStoreThread) then
  // FHandStoreThread.Pause := IsPause;

  if IsPause then
  begin
    FVolumeBeforePause := Snd.Volume;
    if FRegMode <> rHand then
      Snd.Volume := 0;
    FRegStatus := rsPause;
    if Assigned(FBScanStoreThread) then
      FBScanStoreThread.Pause := true;
  end
  else
  begin
    Snd.Volume := FVolumeBeforePause;
    FRegStatus := rsOn;
    if Assigned(FBScanStoreThread) then
      FBScanStoreThread.Pause := false;
  end;

  Result := 0;
end;

function TDefCore.SetBUMMode(RegMode: Byte): Integer;
const
  MaxBUMWaitingTime = 1000; // ������������ ����� �������� ���� �� ���� ������ ������������� � ���������� �������� �������� �-���������
var
  j, c, i, OldReg: Integer;
  StartT, DebugT: Int64;
  Flag: Boolean;
  BV: TBVBuff;
begin
  // ���� ������
  // 0 - ��������� ������ ����������� ������ ���������
  // 1 - ������ ����� ��� �������
  // 2 - �� ������� ��������� ��������������� ����

  Result := 2;
  if RegMode = FRegMode then
  begin
    Result := 1;
    Exit;
  end;

  // ���� ������ �������� ������ �� ���� � ����� ������
  StartT := GetTickCount;
  Flag := false;
  WaitingForBUM := true;
  if Assigned(FBScanStoreThread) then
  begin
    FBScanStoreThread.WaiteForBUM := true;
    while not Flag do
    begin
      Flag := true;
      for i := 0 to FCANList.Count - 1 do
        Flag := Flag and CANList[i].BScanStopped;

      if (GetTickCount - StartT) >= MaxBUMWaitingTime then
        Flag := true;
      sleep(50);
      Application.ProcessMessages;
      // sleep(50);
    end;
    FBScanStoreThread.WaiteForBUM := false;
  end;

  WaitingForBUM := false;
  DebugT := GetTickCount - StartT; // ��� ������� ������ ������� ������ �������� B-���������
  // ���� ��������� ���� �����, ������ ���� �������� �������������� ���
  // B-��������� �� ����������� ������ ����������� � ����� ������������� �� ����� ����� ������
  OldReg := FRegMode;
  FRegMode := RegMode;

  KConf.InitNStr; // ������� ����������� ������.
  StopAScan(); // AK

  case FRegMode of
    rStop:
      begin // ����������.

        KillHandRegThread;
        SetAllHKanalsNOTActive;
        PauseRegistration(false);
        CANASDTurnOff;
        CANStop;
      end;
    rScan:
      begin // ������
        SetAllHKanalsNOTActive;
        PauseRegistration(false);
        SetReleMode(1);
        if Assigned(FBScanStoreThread) then
          FBScanStoreThread.RegMode := rScan;
        if InstallTakts( { Takts[0] } ) <> 0 then
          Exit; // �������� ��� ����� (��� �����).
        CANASDTurnOn;
        CANStart;
        if OldReg = rHand then
        begin
          CANImitDP(true);
          sleep(500);
          for j := 0 to 5 - 1 do
            for i := 0 to FCANList.Count - 1 do
              c := FCANList[i].GetBV(BV);
          CANImitDP(false);
          for j := 0 to 5 - 1 do
            for i := 0 to FCANList.Count - 1 do
              c := FCANList[i].GetBV(BV);
          KillHandRegThread;
        end;
      end;
    rAKSetup: // ��������� ��.
      begin
        // CANImitDP(false);

        KillHandRegThread;

        SetAllHKanalsNOTActive;
        PauseRegistration(true);
        SetReleMode(1);
        if InstallTakts( { Takts[0] } ) <> 0 then
          Exit; // �������� ��� ����� (��� �����).
        // CAN.ASDTurnOn(-1);                          // ������� ��������� ���.
        // CANASDTurnOn(-1);
      end;
    rKorrDP:
      begin // ������������� ��.
        // CANImitDP(false);

        KillHandRegThread;

        SetAllHKanalsNOTActive;
        PauseRegistration(true);
        SetReleMode(1);
        if InstallTakts( { Takts[0] } ) <> 0 then
          Exit; // �������� ����� � ��� (��� �����).
        // CAN.ASDTurnOff;                             // �������� ��������� ���.
        // CAN.Start;                                  // ������� ���.
        CANASDTurnOff;
        CANStart;
      end;
    rNastr:
      begin // ���������.
        // CANImitDP(false);

        KillHandRegThread;

        SetAllHKanalsNOTActive;
        PauseRegistration(true);
        SetReleMode(1);
        if InstallTakts( { Takts[0] } ) <> 0 then
          Exit; // �������� ����� � ��� (��� �����).
        // CAN.ASDTurnOff;                             // �������� ��������� ���.
        // CAN.Start;                                  // ������� ���.
        CANASDTurnOff;
        CANStart;
      end;
    rHand: // ������, ��������� �������.
      begin
        PauseRegistration(true);
        CANStop;
        { for i := 0 to FCANList.Count - 1 do
          begin
          TCAN(FCANList.Items[i]).Stop;
          //          CAN := TCAN(FCANList.Items[i]);
          //          CAN.Stop;
          end; }

        // CANImitDP(true);
        SetReleMode(2);

        InitHandRegThread;

        // if not CAN.InitCikl( 1 ) then Exit;         // ������, ��� ����� ������ 1 ����.
        // ���������� ����� ���� ������ ������ !!!!
        { FBScanStoreThread.RegMode:=rHand;
          //CAN.Stop;                                   // �������� ���.
          CANStop;
          if not CAN.InitCikl( 1 ) then Exit;         // ������, ��� ����� ������ 1 ����.
          //if FRegMode = rHand then CAN.ASDTurnOn(0) else CAN.ASDTurnOff; // ���������� � ���.
          if FRegMode = rHand then CANASDTurnOn(0) else CAN.ASDTurnOff; // ���������� � ���. }
      end;
    rHandNastr:
      begin
        // CANImitDP(false);
        KillHandRegThread;
        SetReleMode(2);
        PauseRegistration(true);
      end;
  end;

  Result := 0;
end;

function TDefCore.SetAScanKanal(Rail, Num: Integer): Integer;
var
  A, B, c: Integer;
begin
  // ���� ������
  // 0 - � ��������� �� ������� ������ ������� ��������
  // 1 - � ��������� �� ����������
  // KConf.SaveUserConfig(0);
  Result := 0;
  // c:=Kanals[1,3].Ky;

  FCurRail := Rail; // � ������ ������� �� ������������
  FCurrentKanal := Num;
  // FCurKanalMode:=Mode;

  StopAScan(); // AK

  // � ����������� �� ������ �������� ��� ������ � �������
  case FRegMode of
    rScan:
      begin
        FCurKanal := Kanals[FCurRail, FCurrentKanal];
        FCurKanal.Takt.InstallStrob(FRegMode, Adj2Tp);
      end;
    rNastr:
      begin
        SetAllHKanalsNOTActive;
        FCurKanal := Kanals[FCurRail, FCurrentKanal];
        FCurKanal.IsActive := true;
        Self.InstallOneKanal(FCurKanal);
        FCurKanal.Takt.InstallStrob(FRegMode, Adj2Tp);
      end;
    rHand, rHandNastr:
      begin
        SetAllHKanalsNOTActive;
        FCurKanal := HKanals[FCurrentKanal];
        FCurKanal.IsActive := true;
        Self.InstallOneKanal(FCurKanal);
      end;
  end;
  if Assigned(FBScanStoreThread) then
    FBScanStoreThread.SetKanal(FCurrentKanal);

  if FSndByOneKanal then
    Snd.SoundOneChannel(FCurRail, FCurrentKanal);

  FCurKanal.Takt.SetRegMode(Self.FRegMode);
  FCurKanal.StartAView;
end;

function TDefCore.StartRegistration(FileType: TAviconID; FileName: String;
  RegParamList: TRegParamList; RealReg: Boolean): Integer;
var
  Info: TUnitInfo;
  Dir, NewSysCoord: Boolean;
  EvalChIdx, i, EvalCh, ScanSt, A: Integer;
  R: TRail;
  StartCaCoord: TCaCrd;
  ChainageFloat: Single;
  c: TCaCrd;
  CP: TCardinalPoints;
  T: string;
  // StartRealCoord: AviconTypes.TRealCoord;
  bwin: TBScanWindow;
begin
  // ���� ������
  // 0 - ����������� ������, ��� ������
  // 1 - �� ������ ��������� ������ ���

  Result := 0;

  if FRegStatus = rsOn then
    Result := Self.StopRegistration;
  if Result <> 0 then
    Exit;

  FGlobalFileType := FileType;

  NewSysCoord := false;

  if Assigned(FDataContainer) then
    FDataContainer.Free;

  // !!!!!!!!!!!!!!!!!!!!
{$IFDEF OldEngine}
  FDataContainer := TAviconDataContainer.Create
    (ConfigList.GetItemByID(FileType), FileName);
{$ELSE}
  FDataContainer := TAviconDataContainer.Create
    (ConfigList.GetItemByID(FileType, 1), true,
    { false } true, true, false, FileName);
{$ENDIF}
  with RegParamList do
  begin
    FDataContainer.SetPathSectionName(PeregonName); // �������� ������� ������
    FDataContainer.SetRailPathNumber(TrackNumber); // ����� �/� ����
    FDataContainer.SetOperatorName(Operator); // ��� ���������
{$IFNDEF USK004R}
{$IFNDEF EGO_USW}
{$IFNDEF VMT_US}
    if Config.UnitsSystem <> usRus then
    begin
      CP[0] := ' ';
      CP[1] := ' ';
      if DirectionID <> '' then
      begin
        CP[0] := DirectionID[1];
        CP[1] := DirectionID[2];
      end;
      FDataContainer.SetTrackDirection(CP);
      if TrackID <> '' then
      begin
        CP[0] := TrackID[1];
        CP[1] := TrackID[2];
      end;
      FDataContainer.SetTrackID(CP);
    end;
{$ENDIF}
{$ENDIF}
{$ENDIF}
    RegFileName := FileName;
{$IFDEF USK004R}
    FDataContainer.SetWorkRailTypeA(LeftSide); // ������� ���� (��� ������������ ��������): 0 � �����, 1 � ������
    if Config.WorkSide then // Gauge - Right
      FDataContainer.SetGaugeSideRightSide()
    else // Gauge - Left
      FDataContainer.SetGaugeSideLeftSide();
{$ELSE}
    FDataContainer.SetCorrespondenceSides(TCorrespondenceSides(LeftSide));
{$ENDIF}
    FDataContainer.SetDateTime(Now); // ����/����� ��������
    FDataContainer.AddDeviceUnitInfo(dutUpUnit, General.BUISN,
      General.GetProgVer);
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US) }
    if Config.DirectionOfMotion then // A-forward
      FDataContainer.SetControlDirection(cd_Tail_B_Head_A)
    else
      FDataContainer.SetControlDirection(cd_Head_B_Tail_A); // B-forward
{$IFEND}
    bwin := TBScanWindow(General.GetWin('BSCAN'));
    if bwin <> Nil then
      bwin.ReInitBView; // FLastCoord:=0;
{$IFDEF UseHardware}
    (* <Rud45> *)
    A := 0;
    while A < 5 do
    begin
      T := IntToStr(Self.GetU2CSN(0));
      inc(A);
    end;
    FDataContainer.AddDeviceUnitInfo(dutMiddleUnit, T, Self.GetU2CVersion(0));
    (* </Rud45> *)
    FDataContainer.AddDeviceUnitInfo(dutDownUnit, IntToStr(Self.BUMSerialNum[0])
        , Format('%d.%d', [Self.CANList[0].FirmwareInfo.FirmwareVersB1,
        Self.CANList[0].FirmwareInfo.FirmwareVersB2]));
{$IFDEF BUMCOUNT2}
{$IFNDEF USK004R}
    FDataContainer.AddDeviceUnitInfo(dutDownUnit, IntToStr(Self.BUMSerialNum[1])
        , Format('%d.%d', [Self.CANList[1].FirmwareInfo.FirmwareVersB1,
        Self.CANList[1].FirmwareInfo.FirmwareVersB2]));
{$ENDIF}
{$ELSE}
{$IFDEF BUMCOUNT4}
    FDataContainer.AddDeviceUnitInfo(dutDownUnit, IntToStr(Self.BUMSerialNum[1])
        , Format('%d.%d', [Self.CANList[1].FirmwareInfo.FirmwareVersB1,
        Self.CANList[1].FirmwareInfo.FirmwareVersB2]));

    A := 0;
    while A < 5 do
    begin
      T := IntToStr(Self.GetU2CSN(1));
      inc(A);
    end;
    FDataContainer.AddDeviceUnitInfo(dutMiddleUnit, T, Self.GetU2CVersion(1));

    FDataContainer.AddDeviceUnitInfo(dutDownUnit, IntToStr(Self.BUMSerialNum[2])
        , Format('%d.%d', [Self.CANList[2].FirmwareInfo.FirmwareVersB1,
        Self.CANList[2].FirmwareInfo.FirmwareVersB2]));
    FDataContainer.AddDeviceUnitInfo(dutDownUnit, IntToStr(Self.BUMSerialNum[3])
        , Format('%d.%d', [Self.CANList[3].FirmwareInfo.FirmwareVersB1,
        Self.CANList[3].FirmwareInfo.FirmwareVersB2]));
{$ENDIF}
{$ENDIF}
{$ELSE}
    FDataContainer.AddDeviceUnitInfo(dutDownUnit, '000000', '0.0');
{$ENDIF}
    ScanSt := ScanStep;

    if Config.UnitsSystem = usRus then
      FDataContainer.SetStartMRFCrd(StartKM, StartPK, StartM)
    else
    begin
{$IFNDEF OldEngine}
      // {$IFDEF USK004R}
{$IFDEF TURKISH_COORD_WITH_DOT}
      c := AviconTypes.StrToCaCrd(TCoordSys(Ord(usMetricB)), Chainage);
      FDataContainer.SetStartCaCrd(TCoordSys(Ord(usMetricB)), c);
{$ELSE}
      c := AviconTypes.StrToCaCrd(TCoordSys(Ord(Config.UnitsSystem)), Chainage);
      FDataContainer.SetStartCaCrd(TCoordSys(Ord(Config.UnitsSystem)), c);
{$ENDIF}
{$ENDIF}
    end;
    if Direction = 0 then
      Direction := -1;
{$IFDEF OldEngine}
    FDataContainer.AddHeader(Direction, ScanSt, MetricSystem); // ������ ���������: ����������� �������� + ��� ������� ����
{$ELSE}
    FDataContainer.AddHeader(Direction, ScanSt); // ������ ���������: ����������� �������� + ��� ������� ����
{$ENDIF}
{$IF DEFINED(BUMCOUNT2) AND NOT DEFINED(USK004R)}
    for i := 0 to 1 do
{$ELSE}
      for i := 0 to 0 do
{$IFEND}
      // for i := 0 to {$IFDEF BUMCOUNT2}1 {$Else}0 {$ENDIF} do
      begin
        R := TRail(i);
        for EvalChIdx := 0 to High(Kanals) do
        begin
          FDataContainer.AddKu(R, EvalChIdx, Kanals[i, EvalChIdx].Ky);
          // ��������� �������� ����������������
          FDataContainer.AddAtt(R, EvalChIdx, Kanals[i, EvalChIdx].Att); // ��������� ��������� ���� ����������� (0 �� �������� ����������������)
          FDataContainer.AddTVG(R, EvalChIdx, Kanals[i, EvalChIdx].VRCH);
          // ��������� ���
          FDataContainer.AddStStr(R, EvalChIdx, Kanals[i, EvalChIdx].Str_st);
          // ��������� ��������� ������ ������
          FDataContainer.AddEndStr(R, EvalChIdx, Kanals[i, EvalChIdx].Str_en);
          // ��������� ��������� ����� ������
          FDataContainer.AddPrismDelay
            (R, EvalChIdx, Round(Kanals[i, EvalChIdx].TwoTp * 100));
        end;
      end;

  end;
  FDataContainer.AddSensor1State(rLeft, true);
  FDataContainer.AddSensor1State(rRight, true);

  // �������� ������ �������� ������ CAN -> DataContainer.
  FBScanStoreThread.Free;
  FBScanStoreThread := TBScanStoreThread.Create(FDataContainer, FCANList);
  FBScanStoreThread.OnHandScanReceived := HandScanReceived;
  FBScanStoreThread.OnAirBrushAddSysCoord := OnCoordChange;
  // FBScanStoreThread.OnBScanReserved:=BScanRecieved;
  FBScanStoreThread.Pause := false;
  FBScanStoreThread.RegMode := rScan;

  SetMode(rScan);

  if Result = 0 then
  begin
    FRegStatus := rsOn;
    // FRegOnUserLevel := not FRegOnUserLevel;
    FRegOnUserLevel := RealReg;
    // if Assigned(OnTextMessage) then OnTextMessage('\n');
    // if Assigned(OnTextMessage) then OnTextMessage('����������� ������\n');
  end;
  RegistrationTimer.Enabled := true;
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
{$IFDEF UseHardware}
  // {$IFnDEF TESTING_COORD}
  // !28-10-2015   if ((Config.PainterState = psOnWithReg) and (FRegOnUserLevel)) or
  // (Config.PainterState = psAlwaysOn) then
  // {$ENDIF}
  Self.StartPainter;
  if // (Config.PaintSprayer = prByAlgorithm) and
    (((Config.PainterState = psOnWithReg) and (FRegOnUserLevel)) or
      (Config.PainterState = psAlwaysOn)) then
  begin
    FisPainterOn := true;
    FAirBrush.SkipEvents := false;
  end
  else
  begin
    FisPainterOn := false;
    FAirBrush.SkipEvents := true;
  end;
{$ENDIF}
  // if Config.PainterState <> psAlwaysOff then Self.StartPainter;
  SetChan0Regim(Config.SelectChan0);
  if Assigned(FDataContainer) then
    FDataContainer.AddAirBrushTempOff(not FisPaintSprayerOn,
      General.Core.Registrator.MaxDisCoord); // Off
  if Assigned(FDataContainer) then
    FDataContainer.AddAlarmTempOff
      (not SndOn, General.Core.Registrator.MaxDisCoord);
{$IFEND}
{$IFDEF USK004R}
  { TRegParamList = record
    Organization: string;
    Operator: string;
    DirCode: Integer;
    StartKM: Integer;
    StartPK: Integer;
    StartM: Integer;
    TrackNumber: Integer;
    Direction: Integer;
    PeregonName: string;
    ScanStep: Word;
    Scheme: Byte;
    BUISN: string;
    BUIFV: string;
    BUM1SN: string;
    BUM1FV: string;
    BUM2SN: string;
    BUM2FV: string;
    AKPSN: string;
    AKPFV: string;
    // ������������������ ���� ��� ������
    DirectionID: string;
    TrackID: string;
    Chainage: string;
    LeftSide: integer;
    end;
  }
  // if FRegOnUserLevel then
  // FNordcoCSVFile:=TNordcoCSVFile.Create;
  (*
    with RecNordcoCSV do begin
    Line              :=RegParamList.PeregonName;                                                     // �������� ��� ������������� �������������� �����
    Track             :=IntToStr(RegParamList.TrackNumber);                                                     // ������������� ����, � ������� ��������� ������
    {    LocationFrom      : Single;                                                     // ������ ������������� �������
    LocationTo        : Single;                                                     // ����� ������������� �������. � ������ ��������� ������� �� �� �����, ��� ����  "Location From"
    LatitudeFrom      : Double;                                                     // GPS-������ ��� ���� "Location From"
    LatitudeTo        : Double;                                                     // GPS-������ ��� ���� "Location To"
    LongitudeFrom     : Double;                                                     // GPS-������� ��� ���� "Location From"
    LongitudeTo       : Double;                                                     // GPS-������� ��� ���� "Location To"
    Type_             : Integer;                                                    // ��� ������� �� UIC 712
    SurveyDate        : TDateTime;                                                  // ���� ����������� �������
    }    Source            :='Ultrasonic Trolley';                                       // ����� ����������� ������� - �� ������: Ultrasonic Trolley, Visual Inspection
    Status            :='Open';                                                     // ��� ���� ���������, ������ �� ������ (�.�. ������ �� ����) - �� ������: Open, Closed
    //    IsFailure         : Boolean;                                                    // ���� ���������� ������ ������� �����, �� ��� ���� ������ ����� �������� TRUE. ������� ����� ������� ����� ������, ������� �������� ����������� ��������
    if RegParamList.LeftSide=0 then AssetType:='Left Rail'
    else AssetType:='Right Rail';                                                   // ��� �������� �������� ���� - �� ������: Left Rail, Right Rail
    DataEntryDate     :=Now; {TDateTime;}                                                  // ���� ����������� ������ ��������
    {    Size              : Single;                                                     // ������ ������� ���  ����������� �������. ��������, ���� ������ ��� �������, �� �� �����.
    Size2             : Single;                                                     // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
    Size3             : Single;                                                     // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
    }    UnitofMeasurement :='mm';                                                     // ��. ��������� ���� "Size"
    UnitofMeasurement2:='mm';                                                     // ��. ��������� ���� "Size2"
    UnitofMeasurement3:='mm';                                                     // ��. ��������� ���� "Size3"
    //    Comment           : string;                                                     // ����������
    Operator_    :=RegParamList.Operator;                                            // ��������
    Device       :='USK-004R';                                                     // ������
    end;
  *)
{$ENDIF}
end;

function TDefCore.StopRegistration: Integer;
var
  s: string;
  i, j: Integer;
begin
  RegistrationTimer.Enabled := false;
  Self.StopPainter;
  // ���� ������
  // 0 - ����������� ������� ���������
  Result := 0;
  if Assigned(FBScanStoreThread) then
  begin
    if FBScanStoreThread.Suspended then
      FBScanStoreThread.Resume;
    FBScanStoreThread.Terminate;
    FBScanStoreThread.Free;
    FBScanStoreThread := nil;
  end;

  // {$IFDEF USK004R}
  { if FRegOnUserLevel then begin
    s:=ChangeFileExt(RegFileName,'.csv');
    FNordcoCSVFile.SaveToFile(s);
    FNordcoCSVFile.Free;
    end;
  }
  // {$ENDIF}

  if Assigned(FDataContainer) then
  begin
    FDataContainer.Free;
    FDataContainer := nil;
  end;
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US) }
  // �������� ���
  for i := 0 to 1 do
    for j := 0 to High(Kanals[i]) do
      if (Kanals[i, j] <> nil) then
        Kanals[i, j].ASD := false;
  Snd.ResetASD;
  Snd.StopTone;
{$IFEND}

  // KConf.SaveUserConfig(0);

  if Result = 0 then
  begin
    FRegStatus := rsOff;
    if FRegOnUserLevel then
      Self.StartTempRegistration;
    // if Assigned(OnTextMessage) then OnTextMessage('\n');
    // if Assigned(OnTextMessage) then OnTextMessage('����������� ��������\n');
  end;
end;

procedure TDefCore.Terminate;
begin
  FTerminate := true;
end;

function TDefCore.WinMessage(Msg: string; WinType: Byte): Boolean;
begin
  //
end;

procedure TDefCore._WrLog_(Msg: string);
begin
{$IFDEF UseLog}
  LogCS.Acquire;
  WriteLn(FLogFile, Format('%.1f ', [(GetTickCount - FTickStart) / 1000]), Msg);
  LogCS.Release;
{$ENDIF}
end;

function TDefCore.GetAScan(var AData: PAVBuff): Integer;
var
  V: Single;
begin
  // ���� ������
  // 0 - �-��������� �������� ���������
  // 1 - CAN �� ����� ������ A-���������
  Result := 1;
  // if not CAN.GetAV( AData ) then Exit;
  if Assigned(FCurKanal) then
  begin
    if Assigned(FCurKanal.Takt.CANObj) then
    begin
      if not FCurKanal.Takt.CANObj.GetAV(AData) then
        Exit;
      if FCurKanal.Takt.Alfa < 20 then
        V := 5.9
      else
        V := 3.26;
      FAScanMeasureResult.R := Round(AData.Tmax * V / 2);
      FAScanMeasureResult.H := Round
        (FAScanMeasureResult.R * cos(FCurKanal.Takt.Alfa / 180 * pi));
      FAScanMeasureResult.L := Round
{$IF DEFINED(EGO_USW) OR DEFINED(USK004R) OR DEFINED(VMT_US)}
      (FAScanMeasureResult.R * sin(FCurKanal.Takt.Alfa / 180 * pi) * cos
          (FCurKanal.Takt.Gamma / 180 * pi));
{$ELSE}
(FAScanMeasureResult.R * sin(FCurKanal.Takt.Alfa / 180 * pi));
{$IFEND}
      FAScanMeasureResult.Att := FCurKanal.Att;
      if AData.Amax < 31 then
        FAScanMeasureResult.N := -1
      else if AData.Amax = 255 then
        FAScanMeasureResult.N := 19
      else
        FAScanMeasureResult.N := Max(0, Round(20 * log10(AData.Amax / 32)));
      FAScanMeasureResult.Kd := FAScanMeasureResult.N - FCurKanal.Ky;
    end;
  end;
  Result := 0;
end;

function TDefCore.GetKu: Integer;
begin
  if Assigned(FCurKanal) then
    Result := FCurKanal.Ky;
end;

procedure TDefCore.SetKu(Value: Integer);
var
  ATT_: Integer;
begin
  if Assigned(FCurKanal) then
  begin
    if FCurKanal.Ky <> Value then
    begin
      ATT_ := FCurKanal.Att + Value - FCurKanal.Ky;
      if ATT_ > 80 then
        Exit;
      if ATT_ < 0 then
        Exit;
      FCurKanal.Att := ATT_;
      // FCurKanal.ATT:= FCurKanal.ATT + Value - FCurKanal.Ky;
      FCurKanal.Ky := Value;
      FCurKanal.AdjustChanges[apKy].Flag := true;
      FCurKanal.AdjustChanges[apKy].CurValue := Value;
      SendBum.Add(FCurKanal, cInstallVRU);
    end;
  end;
end;

function TDefCore.Get2tp: Single;
begin
  if Assigned(FCurKanal) then
    Result := FCurKanal.TwoTp;
end;

procedure TDefCore.Set2tp(Value: Single);
begin
  if Assigned(FCurKanal) then
  begin
    if FCurKanal.TwoTp <> Value then
    begin
      FCurKanal.TwoTp := Value;
      FCurKanal.AdjustChanges[apTwoTp].Flag := true;
      FCurKanal.AdjustChanges[apTwoTp].CurValue := Round(Value * 10);
      SendBum.Add(FCurKanal, cInstall2TP);
    end;
  end;
end;

function TDefCore.StartHandRegistration(Rail, ScanSurf: Byte): Integer;
begin
  // InitHandRegThread;
  if Assigned(FCurKanal) and Assigned( { FHandStoreThread } FHandContainer) then
  begin
    // FHandRegStartCoord := FHandStoreThread.DataContainer.CurSaveDisCoord;
    FHandRegStartCoord := FHandContainer.CurSaveDisCoord;
    FHandRegistrationProcess := true;

    FHandRail := Rail;
    FHandSurface := ScanSurf;
  end;
end;

function TDefCore.StopHandRegistration: Integer;
var
  c: Integer;
  Num, Att, Ky, VRCH: Byte;
  HSItem: THSItem;
begin
  if Assigned(FCurKanal) and Assigned(
    { FHandStoreThread } FHandContainer) and FHandRegistrationProcess then
  begin
    // if (FHandStoreThread.DataContainer.CurSaveDisCoord - FHandRegStartCoord)
    if (FHandContainer.CurSaveDisCoord - FHandRegStartCoord) < 65534 then
      // FHandRegEndCoord := FHandStoreThread.DataContainer.CurSaveDisCoord
      FHandRegEndCoord := FHandContainer.CurSaveDisCoord
    else
      FHandRegEndCoord := FHandRegStartCoord + 65533;

    if Assigned(FCurKanal) then
    begin
      Num := FCurKanal.Num;
      Att := FCurKanal.Att;
      Ky := FCurKanal.Ky;
      VRCH := FCurKanal.VRCH;
    end
    else
    begin
      Num := 0;
      Att := 0;
      Ky := 0;
      VRCH := 0;
    end;

    FDataContainer.AddHandScanFromDataContainer
      (FHandContainer, TRail(FHandRail), FHandSurface, Num, Att, Ky, VRCH,
      FHandRegStartCoord, FHandRegEndCoord);

    FHandRegStartCoord := 0;
    FHandRegEndCoord := 0;
    FHandRegistrationProcess := false;
  end;
end;

function TDefCore.SetMode(Mode: Integer): Integer;
begin
  // ���� ������
  // 0 - �������� ����� ������� ����������
  // 1 - �������� ����� ��� ��� ����������
  // 2 - ������ ������������� ������ � ���

  // if Mode=rScan then PauseRegistration(false)
  // else PauseRegistration(true);
  if FRegMode <> Mode then
  begin
    Result := SetBUMMode(Mode);
    if Result = 0 then
      FRegMode := Mode;
  end;
end;

function TDefCore.GetTaktLength: TAScanLength;
begin
  // ���������� ������������ �-��������� ������ �� ��������
  // if Assigned(FCurKanal) then
  // if FMeasureUnits=muMKS then result:=Round(FCurKanal.Takt.AV_mash*23.1)
  // else if FMeasureUnits=muMM then result:=Round(MKS2MM(FCurKanal.Takt.AV_mash*23.1, FCurKanal.Takt.Alfa, true));
  if Assigned(FCurKanal) then
  begin
    Result.Mks := Round(FCurKanal.Takt.AV_mash * 23.1);
    Result.MM_H := Round(MKS2MM(FCurKanal.Takt.AV_mash * 23.1,
        FCurKanal.Takt.Alfa, true));
    Result.MM_R := Round(MKS2MM(FCurKanal.Takt.AV_mash * 23.1,
        FCurKanal.Takt.Alfa, false));
  end;
end;

function TDefCore.GetStrobEd: Byte;
begin
  case FRegMode of
    rScan, rHand:
      if Assigned(FCurKanal) then
        Result := FCurKanal.Str_en;
    rNastr, rHandNastr:
      if Assigned(FCurKanal) then
        if Adj2Tp then
          Result := FCurKanal.NStr2TP_en
        else
          Result := FCurKanal.NStr_En;
  end;
end;

function TDefCore.GetStrobSt: Byte;
begin
  case FRegMode of
    rScan, rHand:
      if Assigned(FCurKanal) then
        Result := FCurKanal.Str_st;
    rNastr, rHandNastr:
      if Assigned(FCurKanal) then
        if Adj2Tp then
          Result := FCurKanal.NStr2TP_st
        else
          Result := FCurKanal.NStr_St;
  end;
end;

function TDefCore.GetZMEnabled: Boolean;
begin
  if Assigned(FCurKanal) then
    Result := FCurKanal.Takt.UseZM;
end;

function TDefCore.GetZMValue: Word;
begin
  if Assigned(FCurKanal) then
    Result := FCurKanal.Takt.ZonaM;
end;

procedure TDefCore.SetZMEnabled(Value: Boolean);
begin
  if Assigned(FCurKanal) then
    if FCurKanal.Takt.UseZM <> Value then
    begin
      FCurKanal.Takt.UseZM := Value;
      SendBum.Add(FCurKanal, cStartAView);
      SendBum.Add(FCurKanal, cInstallStrob);
    end;
end;

procedure TDefCore.SetZMValue(Value: Word);
begin
  // if Assigned(FCurKanal) then FCurKanal.Takt.ZonaM:=Value;
  if Assigned(FCurKanal) then
  begin
    if FCurKanal.Takt.ZonaM <> Value then
    begin
      if Value >= 0 then
      begin
        FCurKanal.Takt.UseZM := true;
        FCurKanal.Takt.ZonaM := Value;
      end;
      SendBum.Add(FCurKanal, cStartAView);
      SendBum.Add(FCurKanal, cInstallStrob);
    end;
  end;
end;

procedure TDefCore.SetStrobEd(Value: Byte);
begin
  if Assigned(FCurKanal) then
  begin
    case FRegMode of
      rScan, rHand:
        begin
          if FCurKanal.Str_en <> Value then
          begin
            FCurKanal.Str_en := Value;
            FCurKanal.AdjustChanges[apStrobSt].Flag := true;
            FCurKanal.AdjustChanges[apStrobSt].CurValue := FCurKanal.Str_st;
            // FCurKanal.Takt.InstallStrob( CAN, FRegMode, FCurKanal.AdjRailType );
            // FCurKanal.Takt.InstallStrob(FRegMode, FCurKanal.AdjRailType);
            FCurKanal.Takt.InstallStrob(FRegMode, Adj2Tp);
          end;
        end;
      rNastr, rHandNastr:
        begin
          if FCurKanal.NStr_En <> Value then
          begin
            FCurKanal.NStr_En := Value;
            // FCurKanal.Takt.InstallStrob( CAN, FRegMode, FCurKanal.AdjRailType );
            // FCurKanal.Takt.InstallStrob(FRegMode, FCurKanal.AdjRailType);
            FCurKanal.Takt.InstallStrob(FRegMode, Adj2Tp);

          end;
        end;
    end;
  end;
end;

procedure TDefCore.SetStrobSt(Value: Byte);
var
  Flag: Boolean;
  k: Integer;
begin
  if Assigned(FCurKanal) then
  begin
    // if FMeasureUnits = muMM then
    // Value := Max(0, MIN(255, Round(MM2MKS(Value, FCurKanal.Takt.Alfa, true))));

    case FRegMode of
      rScan, rHand:
        begin
          if FCurKanal.Str_st <> Value then
          begin
            FCurKanal.Str_st := Value;
            FCurKanal.AdjustChanges[apStrobSt].Flag := true;
            FCurKanal.AdjustChanges[apStrobSt].CurValue := FCurKanal.Str_st;
            // FCurKanal.Takt.InstallStrob( CAN, FRegMode, FCurKanal.AdjRailType );
            // FCurKanal.Takt.InstallStrob(FRegMode, FCurKanal.AdjRailType);
            FCurKanal.Takt.InstallStrob(FRegMode, Adj2Tp);
          end;
        end;
      rNastr, rHandNastr:
        begin
          if FCurKanal.NStr_St <> Value then
            FCurKanal.NStr_St := Value;
          // FCurKanal.Takt.InstallStrob( CAN, FRegMode, FCurKanal.AdjRailType );
          // FCurKanal.Takt.InstallStrob(FRegMode, FCurKanal.AdjRailType);
          FCurKanal.Takt.InstallStrob(FRegMode, Adj2Tp);
        end;
    end;
  end;
end;

function TDefCore.GetVRCH: Byte;
begin
  if Assigned(FCurKanal) then
    Result := FCurKanal.VRCH;
  // if Assigned(FCurKanal) then result:=FCurKanal.Takt.VRCH.Time;
end;

procedure TDefCore.SetVRCH(Value: Byte);
begin
  if Assigned(FCurKanal) then
  begin
    if FCurKanal.VRCH <> Value then
    begin
      FCurKanal.VRCH := Value;
      FCurKanal.AdjustChanges[apVRCH].Flag := true;
      FCurKanal.AdjustChanges[apVRCH].CurValue := FCurKanal.VRCH;
      SendBum.Add(FCurKanal, cInstallVRU);
    end;
  end;
end;

function TDefCore.GetVRCHCurve: TVRCH;
var
  EmptyVRCH: TVRCH;
begin
  if Assigned(FCurKanal) then
    Result := FCurKanal.Takt.VRCH.VRCH
  else
  begin
    EmptyVRCH.Count := 0;
    Result := EmptyVRCH;
  end;
  // if Assigned(FCurKanal) then result:=FCurKanal.Takt.VRCH.VRCH;
end;

function TDefCore.GetScale: Integer;
begin
  if Assigned(FCurKanal) then
    Result := FCurKanal.Takt.AV_mash;
end;

function TDefCore.AdjustKu(N: Integer): Integer;
begin
  // ���� ������
  // 0 - ��������� ������ �������
  // 1 - ������ ��� ���������
  Result := 1;
  if N < 0 then
    Exit;
  if Assigned(FCurKanal) then
  begin
    if not FCurKanal.AdjustKy(N) then
      Exit;
    { if FCurKanal.Num in [6, 7] then
      begin

      end; }
    SendBum.Add(FCurKanal, cInstallVRU);
  end;
  Result := 0;
end;

function TDefCore.GetBScan(ID: Byte; var BV: TBVBuff;
  GetBScanFrame: TGetBScanFrame): Integer;
var
  BVCount, i, j: Integer;
begin
  // ���� ������
  // 0 - B-��������� �������� ���������
  // 1 - CAN �� ����� ������ B-���������

  Result := 0;
  for i := 0 to FCANList.Count - 1 do
    if TCAN(FCANList.Items[i]).ID = ID then
    begin
      BVCount := TCAN(FCANList.Items[i]).GetBV(BV, -1);
      for j := 0 to BVCount - 1 do
      begin
        ClearBScanFrame;
        LoadBV(BV[j]);
        GetBScanFrame(FBScanFrame);
      end;
    end;

  { Result:=0;
    BVCount:=CAN.GetBV(BV);
    if BVCount=0 then Result:=1;

    for i:=0 to BVCount-1 do
    begin
    ClearBScanFrame;
    LoadBV(BV[i]);
    GetBScanFrame(FBScanFrame);
    end; }
end;

// ���� ID = 255 �� ������� �������� �� ���� ������, ����� ��
function TDefCore.ImitDP(ID: Byte; isOn: Boolean): Integer;
var
  i: Integer;
begin
  // ���� ������
  // 0 - �������� ������� �������/����������
  // 1 - ������ �������/��������� ���������

  Result := 0;
  if isOn <> FIsIminDP then
  begin
    if ID = 255 then
      for i := 0 to FCANList.Count - 1 do
      begin
        if not TCAN(FCANList.Items[i]).ImitDP(isOn) then
          Result := 1;
      end
      else
      begin
        for i := 0 to FCANList.Count - 1 do
          if TCAN(FCANList.Items[i]).ID = ID then
            if not TCAN(FCANList.Items[i]).ImitDP(isOn) then
              Result := 1;
      end;
    FIsIminDP := isOn;
  end;
end;

procedure TDefCore.LoadBV(BV: TBVBuffEl);
var
  RailTakt, RailTaktPrev, i: Integer;
  Rail, Takt: Byte;
  Kan: Integer;
  A: Byte;

  Ofs: Integer; // �������� � ������� ������ ��� ������� ���� ��.
  ECount: Byte; // ���������� ����������� � ������.
  Echos: TEchoBlock; // ���������� (���-����) - �� 8 ���� (���-�� � ECount)
  BlockSize: Integer; // ���������� ���� � ����� �� ���.

  Complete: Boolean;

  function GetByte: Byte;
  begin
    Result := BV.Data[Ofs];
    inc(Ofs);
  end;

begin
  ECount := 0;
  Ofs := 0;
  RailTaktPrev := -1;
  repeat // ���� �� ������� ������ �� ����� ���� ��.

    if Ofs < BV.DataCount - 1 then
    begin
      BlockSize := GetByte;
      RailTakt := GetByte;
      Complete := false;
    end
    else
      Complete := true;

    if (RailTaktPrev <> -1) and (Complete or (RailTaktPrev <> RailTakt)) then
    begin
      // if Kan <> -1 then FDataContainer.AddEcho( Rail, Kan, ECount, Echos );
      if Kan <> -1 then
      begin
        FBScanFrame[Rail, Kan].EchoCount := ECount;
        FBScanFrame[Rail, Kan].Echos := Echos;
      end;
      ECount := 0;
      // Ofs:= 0;
    end;

    if Complete then
      Exit; // �����.

    RailTaktPrev := RailTakt;

    Rail := Ord(RailTakt and $80 = $80);
    // Inc( Ofs );
    Takt := RailTakt and $7F;


    // ������� ��� ������� � ��� !
    // if Takt <= High( Takts[ Rail xor Config.WorkRail ] ) then Kan:= Takts[ Rail xor Config.WorkRail, Takt ].Kan1 else Kan:= -1;

    case BlockSize of
      3:
        begin
          Echos[ECount + 0].T := GetByte;
          A := GetByte;
          Echos[ECount + 0].A := A and $F0 shr 4;
          inc(ECount, 1);
        end;
      4:
        begin
          Echos[ECount + 0].T := GetByte;
          Echos[ECount + 1].T := GetByte;
          A := GetByte;
          Echos[ECount + 0].A := A and $F0 shr 4;
          Echos[ECount + 1].A := A and $0F;
          inc(ECount, 2);
        end;
      6:
        begin
          Echos[ECount + 0].T := GetByte;
          Echos[ECount + 1].T := GetByte;
          Echos[ECount + 2].T := GetByte;
          A := GetByte;
          Echos[ECount + 0].A := A and $F0 shr 4;
          Echos[ECount + 1].A := A and $0F;
          A := GetByte;
          Echos[ECount + 2].A := A and $F0 shr 4;
          inc(ECount, 3);
        end;
      7:
        begin
          Echos[ECount + 0].T := GetByte;
          Echos[ECount + 1].T := GetByte;
          Echos[ECount + 2].T := GetByte;
          Echos[ECount + 3].T := GetByte;
          A := GetByte;
          Echos[ECount + 0].A := A and $F0 shr 4;
          Echos[ECount + 1].A := A and $0F;
          A := GetByte;
          Echos[ECount + 2].A := A and $F0 shr 4;
          Echos[ECount + 3].A := A and $0F;
          inc(ECount, 4);
        end;
    end; // case.
  until false; // ���� �� ������ Data.
end;

procedure TDefCore.ClearBScanFrame;
var
  i, j: Integer;
begin
  for i := 0 to 1 do
    for j := 0 to 7 do
    begin
      FBScanFrame[i, j].EchoCount := 0;
      FillChar(FBScanFrame[i, j].Echos, 16, 0);
    end;
end;

procedure TDefCore.HandScanReceived(HSItem: THSItem);
var
  c: Integer;

begin
  (* if FHandRegistrationProcess then
    begin
    if HSItem.Sample=0 then Inc(FCurSample);
    HSItem.Sample:=FCurSample;
    c:=Length(FHandScanData.HSItem);
    Inc(c);
    SetLength(FHandScanData.HSItem, c);
    FHandScanData.HSItem[c-1]:=HSItem;
    end; *)
end;

procedure TDefCore.SetSndKanal(Value: Boolean);
begin
  if Assigned(FCurKanal) then
  begin
    FSndByOneKanal := Value;
    if Value then
      Snd.SoundOneChannel(FCurRail, FCurrentKanal)
    else
      Snd.SoundAllChannels;
  end;
end;

procedure TDefCore.SetSndOn(Value: Boolean);
begin
  FSndOn := Value;
  if not FSndOn then
  begin
    FOldVolume := Snd.Volume;
    Snd.Volume := 0;
  end
  else
    Snd.Volume := FOldVolume;

  if Assigned(FDataContainer) then
    FDataContainer.AddAlarmTempOff
      (not FSndOn, General.Core.Registrator.MaxDisCoord);
end;

function TDefCore.GetSndVolume: Integer;
begin
  Result := Snd.Volume;
end;

procedure TDefCore.SetSndVolume(Value: Integer);
begin
  Snd.Volume := Value;
end;

function TDefCore.GetASD(Rail, Channel: Integer): Boolean;
begin
  //
end;

function TDefCore.GetAK(Rail, Channel: Integer): Boolean;
begin
  if Assigned(Kanals[Rail, Channel]) then
    Result := Kanals[Rail, Channel].Takt.CANObj.AK[Rail, Channel];
  // Result:=CAN.AK[Rail, Channel];
end;

function TDefCore.GetAKLevel(Rail, Channel: Integer): Integer;
begin
  if Assigned(Kanals[Rail, Channel]) then
    Result := Kanals[Rail, Channel].AK_Level;
end;

procedure TDefCore.SetAKLevel(Rail, Channel, Level: Integer);
var
  i, j: Integer;
begin
  if Assigned(Kanals[Rail, Channel]) then
  begin
    if Level > MaxAK_Level then
      Level := MaxAK_Level;
    if Level < 0 then
      Level := 0;
    if Kanals[Rail, Channel].AK_Level <> Level then
    begin
      Kanals[Rail, Channel].AK_Level := Level;
      SendBum.Add(Kanals[Rail, Channel], cInstallVRU);
    end;
  end;
end;

function TDefCore.GetTaktLengthMKS: Byte;
begin
  if Assigned(FCurKanal) then
    Result := Round(FCurKanal.Takt.AV_mash * 23.1);
end;

function TDefCore.GetKanal(Rail, Num: Integer): TKanal;
begin
  if Num <= High(Kanals[Rail]) then
    Result := Kanals[Rail, Num]
  else
    Result := nil;
end;

function TDefCore.GetCurDisCoord: Integer;
begin
  if Assigned(FDataContainer) then
    Result := FDataContainer.MaxDisCoord;
end;

function TDefCore.GetCurRealCoord: TRealCoord;
begin
  // ��������� �������� ���������� !!!!!
  // Assigned(FDataContainer) then result:= FDataContainer.DisToRealCoord( FDataContainer.MaxDisCoord );
end;

function TDefCore.GetReg: TAviconDataContainer;
begin
  Result := nil;
  if (FRegMode = rHand) and (Assigned(FHandContainer)) then
    Result := FHandContainer
  else if Assigned(FDataContainer) then
    Result := FDataContainer;
end;

procedure TDefCore.SetRegistrator(const Value: TAviconDataContainer);
begin
  FDataContainer := Value;
end;

function TDefCore.StopAScan: Integer;
var
  i: Integer;
begin
  // {$IFDEF BUMCOUNT4}
  if DevCount > 1 then
  begin
{$IFDEF UseHardware}
    for i := 0 to FCANList.Count - 1 do
      FCANList[i].StopAView;
{$ENDIF}
    // {$ELSE}
  end
  else if Assigned(FCurKanal) then
  begin
{$IFDEF UseHardware}
    FCurKanal.Takt.CANObj.StopAView;
{$ENDIF}
    FCurKanal := nil;
  end;
  // {$ENDIF}
  Result := 0;
end;

procedure TDefCore.AdjustRailType;
var
  MScan: TBScanArray;
  R, KP, Ch, KPCount, BN, RN: Byte;
  i: Integer;
  Amax: Integer;
  Tmax: Integer;
  B: array [0 .. 1] of Boolean;
  H: array [0 .. 1] of Integer;
  s: string;
  Kind: TWinKind;
  Bottoms: array [0 .. 1, 1 .. 2] of TEcho;
  // ��� ������� ������� �������� �������
  Sum, N, MidT: Integer;
  StL1, StL2, StR1, StR2: string;
  StLeft, StRight: string;

  procedure FindBottomSignal(Rail, Ch: Integer; var Amp, Delay: Byte);
  var
    i: Integer;
  begin
    Amp := 0;
    Delay := 0;
    for i := 0 to MScan[Rail, Ch].EchoCount - 1 do
      with MScan[Rail, Ch].Echos[i] do
      begin
        (* <Rud15> *)
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
        if (T >= 40) and (T <= 70) then
{$ELSE}
          if (T >= 30) and (T <= 70) then
{$IFEND}
            if (A > Amp) and (A > 3) then
            begin
              Amp := A;
              Delay := T;
            end;
      end;
  end;

  procedure GetBottomSignalData;
  var
    i, R: Integer;
    Amp, Delay: Byte;
  begin
    Bottoms[0, 1].Delay := 0;
    Bottoms[0, 2].Delay := 0;
    Bottoms[1, 1].Delay := 0;
    Bottoms[1, 2].Delay := 0;

    Bottoms[0, 1].Amp := 0;
    Bottoms[0, 2].Amp := 0;
    Bottoms[1, 1].Amp := 0;
    Bottoms[1, 2].Amp := 0;

    // ���� ����������� 10 ��������� ���������� ������� ������� � ���������� � 100 ��
    // � �������� ������� ���� �������������� ���������� �������� � ��������
    for i := 0 to 9 do
    begin
      // ������� ������ B-���������
      MScan := GetLastBScan;
      // ���� �� �����
      for R := 0 to 1 do
      begin
        // ��� ������ ������ ��� "�������" �������, ������� ���� �� ���� ������ �������������
        // (������ ����� ������ ��� ��������� �������� �����)
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
        FindBottomSignal(R, 1, Amp, Delay);
        if Amp > Bottoms[R, 1].Amp then
        begin
          Bottoms[R, 1].Amp := Amp;
          Bottoms[R, 1].Delay := Delay;
        end;
        // ���� ����� ���� � ��������, �� ����� ��������� ���������� �������� ��� ������� �������
        FindBottomSignal(R, 11, Amp, Delay);
        if Amp > Bottoms[R, 2].Amp then
        begin
          Bottoms[R, 2].Amp := Amp;
          Bottoms[R, 2].Delay := Delay;
        end;
{$ELSE}
        if not(Config.SoundedScheme = Wheels) then
          FindBottomSignal(R, 1, Amp, Delay)
        else
          FindBottomSignal(R, 11, Amp, Delay);
        if Amp > Bottoms[R, 1].Amp then
        begin
          Bottoms[R, 1].Amp := Amp;
          Bottoms[R, 1].Delay := Delay;
        end;
        // ���� ����� ���� � ��������, �� ����� ��������� ���������� �������� ��� ������� �������
        if Config.SoundedScheme = Wheels then
        begin
          FindBottomSignal(R, 1, Amp, Delay);
          if Amp > Bottoms[R, 2].Amp then
          begin
            Bottoms[R, 2].Amp := Amp;
            Bottoms[R, 2].Delay := Delay;
          end;
        end;
{$IFEND}
      end;

      sleep(100);
    end;
  end;

  function MKStoDistanceUnit(msk: Integer): Integer;
  begin
    // ���� ������� ��������� �� ������� ��� ��������, �� �������� ��, � ����� �������� � �����
    if Config.UnitsSystem in [usRus, usMetricTurkish] then
      Result := Round(MKS2MM(msk, 0, true))
    else
      Result := Round(MKS2MM(msk, 0, true) / 25.4);
  end;

  function DistanceUnitToStr(Value: Integer): string;
  var
    Units: string;
  begin
    if Config.UnitsSystem in [usRus, usMetricTurkish] then
      Units := General.LanguageTable['��']
    else
      Units := '"';
    Result := Format('%d %s', [Value, Units]);
  end;

  function BottomSignToStr(R, Ch: Integer): string;
  begin
    Result := DistanceUnitToStr(MKStoDistanceUnit(Bottoms[R, Ch].Delay));
  end;

begin
  if (FRegMode in [rHand, rHandNastr]) then
  begin
    if FCurKanal.Num > 1 then
    begin
      MessageForm.Display(General.LanguageTable[
          '������: ��������� �� ��� ������ � ������ ������ �������� ������ ���� ������� ������ ����� - ������.'], kBad);
      Exit;
    end
    else
    begin
      HandRailType;
      Exit;
    end;
  end;
  // ������� ������ � ������ ��������
  GetBottomSignalData;

  // ���������� ��������� � ������ ������� ����� ������������� � ������ � ������� �������� ���������
  // ��������� �� ��� ������ ������� ��� ������ � �������� ������
  if not(FRegMode in [rHand, rHandNastr]) then
  begin
    // ������� ��� ������ ����
    KPCount := 1;
    // ���� �������� � ��������, �� ������ ���
{$IFNDEF EGO_USW}
{$IFNDEF VMT_US}
    if Config.SoundedScheme = Wheels then
{$ENDIF}
{$ENDIF}
      KPCount := 2;

    // ���������� ��������� ������� ������ ������� �� ���������� ���������� ������ ��������
    // ���� �� ������
    if FRegMode <> rNastr then
    begin
{$IFDEF USK004R}
      R := 0;
{$ELSE}
      for R := 0 to 1 do
{$ENDIF}
        // ���� �� ������
        for i := 1 to KPCount do
          if (Bottoms[R, i].Delay > 0) and (Bottoms[R, i].Amp > 3) then
            AdjRailTypeZeroChannel(R, Bottoms[R, i].Delay, i)
          else
            AdjRailTypeZeroChannel(R, ProgramProfile.DefaultRailTypeTime, i);
    end
    else
    begin
      BN := 1;
      RN := FCurKanal.Nit;
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US) }
      case FCurKanal.Num of
        0, 1:
          BN := 1;
        10, 11:
          BN := 2;
      end;
{$ELSE}
      if Config.SoundedScheme = Wheels then
        case FCurKanal.Num of
          (* <Rud10> *)  (* BN ���� ���������� *)
          10, 11:
            BN := 1;
          0, 1:
            BN := 2;
          (* </Rud10> *)  (* BN ���� ���������� *)
        end;
{$IFEND}
      if (Bottoms[RN, BN].Delay > 0) and (Bottoms[RN, BN].Amp > 3) then
        AdjRailTypeZeroChannel(RN, Bottoms[RN, BN].Delay, BN)
      else
        AdjRailTypeZeroChannel(RN, ProgramProfile.DefaultRailTypeTime, BN);

    end;

    // ���������� ��������� ������� ��������� (�� ������) �������, ��������� �� ���� ������
    // ���������� �������� ������������ �� ������� ���������� ������ ��������� �������
    // ��� �������� ��������:
    // 1) ������ ����� ���� � �������� ������� � ��� �� �������, ����� ������ ����� ��� ��������
    // 2) ������ ������� ��� (������), ����� ���� ������� �������� �� ���� ������, �������� ��� ���� ������� �������� �� ������-���� �� ���� ������
    // 3) �� ���� ������ ������� ������ �� ������ (Delay =0 ��� Amp = 0) ����� ����� �������� ����������� ��� ���� ������ �65.
    Sum := 0;
    N := 0;
    MidT := 0;

    if FRegMode <> rNastr then
    begin
{$IFDEF USK004R}
      R := 0;
{$ELSE}
      for R := 0 to 1 do
{$ENDIF}
      begin
        for i := 1 to KPCount do
          if (Bottoms[R, i].Delay > 0) and (Bottoms[R, i].Amp > 3) then
          begin
            Sum := Sum + Bottoms[R, i].Delay;
            inc(N);
          end;
        if N = 0 then
          MidT := ProgramProfile.DefaultRailTypeTime
        else
          MidT := Round(Sum / N);
        AdjRailTypeNOTZeroChannels(R, MidT);
      end;
    end
    else
    begin
      if (Bottoms[RN, BN].Delay > 0) and (Bottoms[RN, BN].Amp > 3) then
        AdjRailTypeNOTZeroChannels(RN, Bottoms[RN, BN].Delay)
      else
        AdjRailTypeNOTZeroChannels(RN, ProgramProfile.DefaultRailTypeTime);
    end;

    // ��������� ��������� �� �������� (��� ����������) ���������
    Kind := kGood;
    if (Bottoms[0, 1].Delay > 0) and (Bottoms[0, 1].Amp > 0) then
      StL1 := BottomSignToStr(0, 1)
    else
      StL1 := General.LanguageTable['��� �������'];

    if (Bottoms[0, 2].Delay > 0) and (Bottoms[0, 2].Amp > 0) then
      StL2 := BottomSignToStr(0, 2)
    else
      StL2 := General.LanguageTable['��� �������'];

    if (Bottoms[1, 1].Delay > 0) and (Bottoms[1, 1].Amp > 0) then
      StR1 := BottomSignToStr(1, 1)
    else
      StR1 := General.LanguageTable['��� �������'];

    if (Bottoms[1, 2].Delay > 0) and (Bottoms[1, 2].Amp > 0) then
      StR2 := BottomSignToStr(1, 2)
    else
      StR2 := General.LanguageTable['��� �������'];

    (* <Rud21> *)
    // ���� ��������� � ������ ������! ��������
    if FRegMode <> rNastr then
    begin
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
      StLeft := Format('%s: %sA - %s , %sB - %s',
        [General.LanguageTable['����� �����'], General.LanguageTable['��'],
        StL1, General.LanguageTable['��'], StL2]);

      StRight := Format('%s: %sA - %s , %sB - %s',
        [General.LanguageTable['������ �����'], General.LanguageTable['��'],
        StR1, General.LanguageTable['��'], StR2]);
{$ELSE}
      if Config.SoundedScheme = Wheels then
      begin
        StLeft := Format('%s: %s1 - %s , %s2 - %s',
          [General.LanguageTable['����� �����'], General.LanguageTable['��'],
          StL1, General.LanguageTable['��'], StL2]);

        StRight := Format('%s: %s1 - %s , %s2 - %s',
          [General.LanguageTable['������ �����'], General.LanguageTable['��'],
          StR1, General.LanguageTable['��'], StR2]);
      end
      else
      begin
{$IFDEF USK004R}
        StLeft := Format('%s: %s', [General.LanguageTable['������ ������'],
          StL1]);
        StRight := '';
{$ELSE}
        StLeft := Format('%s: %s', [General.LanguageTable['����� �����'], StL1]
          );
        StRight := Format('%s: %s', [General.LanguageTable['������ �����'],
          StR1]);
{$ENDIF}
      end;
{$IFEND}
{$IFDEF USK004R}
      s := Format(General.LanguageTable['%s.'], [StLeft]);
{$ELSE}
      s := Format(General.LanguageTable['%s. %s%s %s.'],
        [StLeft, WCHAR(10), WCHAR(13), StRight]);
{$ENDIF}
      (* <Rud21> *)
    end
    // ���� ��������� � ������ ���������!-��������
    else
    begin
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
      case FCurKanal.Nit of
        0:
          begin
            if FCurKanal.Num in [0, 1] then
              s := Format
                ('%s: %sA - %s', [General.LanguageTable['����� �����'],
                General.LanguageTable['��'], StL1])
            else if FCurKanal.Num in [10, 11] then
              s := Format
                ('%s: %sB - %s', [General.LanguageTable['����� �����'],
                General.LanguageTable['��'], StL2]);
          end;

        1:
          begin
            if FCurKanal.Num in [0, 1] then
              s := Format
                ('%s: %sA - %s', [General.LanguageTable['������ �����'],
                General.LanguageTable['��'], StR1])
            else if FCurKanal.Num in [10, 11] then
              s := Format
                ('%s: %sB - %s', [General.LanguageTable['������ �����'],
                General.LanguageTable['��'], StR2]);
          end;
      end;
{$ELSE}
      if Config.SoundedScheme = Wheels then
      begin
        case FCurKanal.Nit of
          0:
            begin
              if FCurKanal.Num in [10, 11] then
                s := Format
                  ('%s: %s1 - %s', [General.LanguageTable['����� �����'],
                  General.LanguageTable['��'], StL1])
              else if FCurKanal.Num in [0, 1] then
                s := Format
                  ('%s: %s2 - %s', [General.LanguageTable['����� �����'],
                  General.LanguageTable['��'], StL2]);
            end;

          1:
            begin
              if FCurKanal.Num in [10, 11] then
                s := Format('%s: %s1 - %s',
                  [General.LanguageTable['������ �����'],
                  General.LanguageTable['��'], StR1])
              else if FCurKanal.Num in [0, 1] then
                s := Format('%s: %s2 - %s',
                  [General.LanguageTable['������ �����'],
                  General.LanguageTable['��'], StR2]);
            end;
        end;
      end
      // ��� ����������
      else
      begin
{$IFDEF USK004R}
        s := Format('%s: %s', [General.LanguageTable['������ ������'], StL1]);
{$ELSE}
        if FCurKanal.Num in [0, 1] then
          case FCurKanal.Nit of
            0:
              s := Format('%s: %s', [General.LanguageTable['����� �����'], StL1]
                );
            1:
              s := Format('%s: %s', [General.LanguageTable['������ �����'],
                StR1]);
          end;
{$ENDIF}
      end;
{$IFEND}
    end;
    (* </Rud21> *)
  end

  // end;

  // ��������� �� ��� ������, ���� �� ������� �� �������
  else
  begin
    if FCurKanal.Num > 1 then
    begin
      MessageForm.Display(General.LanguageTable[
          '������: ��������� �� ��� ������ � ������ ������ �������� ������ ���� ������� ������ ����� - ������.'], kBad);
      Exit;
    end;
    if (Bottoms[0, 1].Delay > 0) and (Bottoms[0, 1].Amp > 3) then
    begin
      AdjRailHand(Bottoms[0, 1].Delay);
      s := Format(General.LanguageTable['������ � ������ ������ %d ��.'],
        [StrToInt(BottomSignToStr(0, 1))]);
    end
    else
      s := General.LanguageTable['��� ������� �������.'];
  end;
  (* <Rud21> *)
  // ���� � ���������-�������� � ����� �� ����� 0� ��������� �� ��������
  if (FRegMode <> rNastr) or (FCurKanal.Takt.Alfa = 0) then
    (* </Rud21> *)

    MessageForm.Display(s, Kind);
  if Assigned(FDataContainer) then
  begin
    FDataContainer.AddSetRailType;
  end;
end;

procedure TDefCore.AdjRailTypeZeroChannel(Rail: Byte; Tm, KP: Integer);
var
  Ch0, Ch1: Integer;
begin
{$IFDEF EGO_USW}
  case KP of
    1:
      begin
        Ch0 := 0;
        Ch1 := 1;
      end;
    2:
      begin
        Ch0 := 10;
        Ch1 := 11;
      end;
  end;
{$ELSE}
  if Config.SoundedScheme = Wheels then
  begin
    case KP of
      1:
        begin
          Ch0 := 10;
          Ch1 := 11;
        end;
      2:
        begin
          Ch0 := 0;
          Ch1 := 1;
        end;
    end;
  end
  else
  begin
    Ch0 := 0;
    Ch1 := 1;
  end;
{$ENDIF}
  if ( High(Kanals[Rail]) >= Ch0) and ( High(Kanals[Rail]) >= Ch1) then
    if Assigned(Kanals[Rail, Ch0]) and Assigned(Kanals[Rail, Ch1]) then
    begin
      Kanals[Rail, Ch0].Str_st := Tm - 3;
      Kanals[Rail, Ch0].Str_en := Tm + 6;
      Kanals[Rail, Ch0].NStr_St := Kanals[Rail, Ch0].Str_st;
      Kanals[Rail, Ch0].NStr_En := Kanals[Rail, Ch0].Str_en;
      Kanals[Rail, Ch1].Str_en := Kanals[Rail, Ch0].Str_st;
      // Kanals[Rail, Ch0].Takt.InstallStrob(FRegMode, false);
      // Kanals[Rail, Ch0].Takt.InstallVRU(FRegMode);
      UpDateStrobAndVRU(Kanals[Rail, Ch0]);
      UpDateStrobAndVRU(Kanals[Rail, Ch1]);
    end;
end;

procedure TDefCore.AdjRailHand(Tm: Integer);
begin
  HKanals[0].Str_st := Tm - 3;
  HKanals[0].Str_en := Tm + 6;
  HKanals[0].NStr_St := HKanals[0].Str_st;
  HKanals[0].NStr_En := HKanals[0].Str_en;
  HKanals[1].Str_en := HKanals[0].Str_st;
  UpDateStrobAndVRU(HKanals[0]);
end;

procedure TDefCore.UpDateStrobAndVRU(Kanal: TKanal);
var
  Flag: Boolean;
begin
  if not Assigned(Kanal) then
    Exit;
  Flag := not(Self.FRegMode = rNastr);
  if not Flag then
  begin
    if Assigned(FCurKanal) then
      Flag := (Kanal.Nit = FCurKanal.Nit) and (Kanal.Num = FCurKanal.Num);
  end;

  if Flag then
  begin
    Kanal.Takt.InstallStrob(FRegMode, Self.Adj2Tp);
    Kanal.Takt.InstallVRU(FRegMode);
  end;

  (* <Rud20> *)
  if (FRegMode = rScan) and Assigned(FDataContainer) then
  begin
    FDataContainer.AddStStr(TRail(Kanal.Nit), Kanal.Num, Kanal.Str_st);
    FDataContainer.AddEndStr(TRail(Kanal.Nit), Kanal.Num, Kanal.Str_en);
  end;
  (* </Rud20> *)

end;

procedure TDefCore.AdjRailTypeNOTZeroChannels(Rail: Byte; Tm: Integer);
begin
  Kanals[Rail, 6].Str_en := Round((Tm - 10) * 5.9 / 3.26 / cos(42 / 180 * pi))
    + 2;
  // Kanals[Rail, 6].Takt.InstallStrob(FRegMode, false);
  // Kanals[Rail, 6].Takt.InstallVRU(FRegMode);
  UpDateStrobAndVRU(Kanals[Rail, 6]);

  Kanals[Rail, 8].Str_st := Kanals[Rail, 6].Str_en;
  if Kanals[Rail, 8].Str_st > Kanals[Rail, 8].Str_en then
    Kanals[Rail, 8].Str_en := Kanals[Rail, 8].Takt.Ten - 5;
  // Kanals[Rail, 8].Takt.InstallStrob(FRegMode, false);
  // Kanals[Rail, 8].Takt.InstallVRU(FRegMode);
  UpDateStrobAndVRU(Kanals[Rail, 8]);

  Kanals[Rail, 7].Str_en := Round((Tm - 10) * 5.9 / 3.26 / cos(42 / 180 * pi))
    + 2;
  UpDateStrobAndVRU(Kanals[Rail, 7]);
  // Kanals[Rail, 7].Takt.InstallStrob(FRegMode, false);
  // Kanals[Rail, 7].Takt.InstallVRU(FRegMode);

  Kanals[Rail, 9].Str_st := Kanals[Rail, 7].Str_en;
  if Kanals[Rail, 9].Str_st > Kanals[Rail, 9].Str_en then
    Kanals[Rail, 9].Str_en := Kanals[Rail, 9].Takt.Ten - 5;
  UpDateStrobAndVRU(Kanals[Rail, 9]);
  // Kanals[Rail, 9].Takt.InstallStrob(FRegMode, false);
  // Kanals[Rail, 9].Takt.InstallVRU(FRegMode);
end;

procedure TDefCore.AdjRailType(Rail: Byte; Tm: Integer);
begin
{$IFNDEF Avikon15RSP}
  if not(FRegMode in [rHand, rHandNastr]) then
  begin
    Kanals[Rail, 0].Str_st := Tm - 3;
    Kanals[Rail, 0].Str_en := Tm + 6;
    Kanals[Rail, 0].NStr_St := Kanals[Rail, 0].Str_st;
    Kanals[Rail, 0].NStr_En := Kanals[Rail, 0].Str_en;
    Kanals[Rail, 1].Str_en := Kanals[Rail, 0].Str_st;
    Kanals[Rail, 0].Takt.InstallStrob(FRegMode, false);
    Kanals[Rail, 0].Takt.InstallVRU(FRegMode);

    Kanals[Rail, 6].Str_en := Round((Tm - 10) * 5.9 / 3.26 / cos(42 / 180 * pi)
      ) + 2;
    Kanals[Rail, 6].Takt.InstallStrob(FRegMode, false);
    Kanals[Rail, 6].Takt.InstallVRU(FRegMode);

    Kanals[Rail, 8].Str_st := Kanals[Rail, 6].Str_en;
    if Kanals[Rail, 8].Str_st > Kanals[Rail, 8].Str_en then
      Kanals[Rail, 8].Str_en := Kanals[Rail, 8].Takt.Ten - 5;
    Kanals[Rail, 8].Takt.InstallStrob(FRegMode, false);
    Kanals[Rail, 8].Takt.InstallVRU(FRegMode);

    Kanals[Rail, 7].Str_en := Round((Tm - 10) * 5.9 / 3.26 / cos(42 / 180 * pi)
      ) + 2;
    Kanals[Rail, 7].Takt.InstallStrob(FRegMode, false);
    Kanals[Rail, 7].Takt.InstallVRU(FRegMode);

    Kanals[Rail, 9].Str_st := Kanals[Rail, 7].Str_en;
    if Kanals[Rail, 9].Str_st > Kanals[Rail, 9].Str_en then
      Kanals[Rail, 9].Str_en := Kanals[Rail, 9].Takt.Ten - 5;
    Kanals[Rail, 9].Takt.InstallStrob(FRegMode, false);
    Kanals[Rail, 9].Takt.InstallVRU(FRegMode);
{$IFDEF  Avikon14_2Block}
    Kanals[Rail, 10].Str_st := Kanals[Rail, 0].Str_st;
    Kanals[Rail, 10].Str_en := Kanals[Rail, 0].Str_en;
    Kanals[Rail, 10].NStr_St := Kanals[Rail, 0].NStr_St;
    Kanals[Rail, 10].NStr_En := Kanals[Rail, 0].NStr_En;
    Kanals[Rail, 11].Str_en := Kanals[Rail, 1].Str_en;
    Kanals[Rail, 10].Takt.InstallStrob(FRegMode, false);
    Kanals[Rail, 10].Takt.InstallVRU(FRegMode);
{$ENDIF}
{$IF DEFINED(Avikon16_IPV) OR DEFINED(EGO_USW) OR DEFINED(VMT_US)}
    Kanals[Rail, 10].Str_st := Kanals[Rail, 0].Str_st;
    Kanals[Rail, 10].Str_en := Kanals[Rail, 0].Str_en;
    Kanals[Rail, 10].NStr_St := Kanals[Rail, 0].NStr_St;
    Kanals[Rail, 10].NStr_En := Kanals[Rail, 0].NStr_En;
    Kanals[Rail, 11].Str_en := Kanals[Rail, 1].Str_en;
    UpDateStrobAndVRU(Kanals[Rail, 10]);
{$IFEND}
  end
  else
  begin
    HKanals[0].Str_st := Tm - 3;
    HKanals[0].Str_en := Tm + 6;
    HKanals[0].NStr_St := HKanals[0].Str_st;
    HKanals[0].NStr_En := HKanals[0].Str_en;
    HKanals[1].Str_en := HKanals[0].Str_st;
    HKanals[0].Takt.InstallStrob(FRegMode, false);
    HKanals[0].Takt.InstallVRU(FRegMode);
  end;
{$ELSE}
  Kanals[Rail, 0].Str_st := Tm - 3;
  Kanals[Rail, 0].Str_en := Tm + 6;
  Kanals[Rail, 0].NStr_St := Kanals[Rail, 0].Str_st;
  Kanals[Rail, 0].NStr_En := Kanals[Rail, 0].Str_en;
  Kanals[Rail, 1].Str_en := Kanals[Rail, 0].Str_st;
{$ENDIF}
  KConf.InitNStr; // ������� ����������� ������.
end;

procedure TDefCore.AdjustDefaultRailType;
begin
  // AdjRailType(0, 64);
  // AdjRailType(1, 64);
  AdjRailType(0, ProgramProfile.DefaultRailTypeTime);
  AdjRailType(1, ProgramProfile.DefaultRailTypeTime);
end;

procedure TDefCore.OnMViewDecode(Rail, Takt: Byte; Echos: TDecodedEchos;
  AdditionalData: Integer);
var
  BUM: Integer;
  R: Integer;
  Kan: Integer;
  i: Integer;
  s: string;
  Tkt: TTakt;

begin
  // �������� �������������� �-���������. ��������� ���������� � FLastBScan.
  try
    BUM := AdditionalData;
    if CanCount = 1 then
      R := Rail
    else if CanCount = 2 then
      R := BUM
    else
      R := BUMtoRail(BUM);

    if Takt > High(Takts[BUM, Rail xor Config.WorkRail]) then
      Exit;
    Tkt := Takts[BUM, Rail xor Config.WorkRail, Takt];

    if Tkt = Nil then
      Exit; // AK

    Kan := Tkt.Kan1;
    if Kan < 0 then
      Exit;
    s := '';
    FLastBScan[R, Kan].EchoCount := Echos.Count;
    for i := 0 to Echos.Count - 1 do
    begin
      FLastBScan[R, Kan].Echos[i].T := Echos.Echo[i].T div Tkt.DelayFactor;
      FLastBScan[R, Kan].Echos[i].A := Echos.Echo[i].A;
      s := s + Format('(%d, %d) ', [Echos.Echo[i].T, Echos.Echo[i].A]);
    end;
    _WrLog_(Format(
        'MView Decoded: BUM = %d, Rail = %d, Takt = %d, Kan = %d, DATA = %s',
        [BUM, Rail, Takt, Kan, s]));

  except
    on E: Exception do
      _WrLog_('(E) MView Decoded ERROR (' + E.Message + ')!');
  end;
end;

function TDefCore.GetLastBScan: TBScanArray;
var
  i, j: Integer;
  MV: TMVBuffer;

begin
  for i := 0 to 1 do
    for j := 0 to High(FLastBScan[i]) do
      FLastBScan[i, j].EchoCount := 0;

  for i := 0 to CanCount - 1 do
  begin
    CANList[i].GetMV(@MV);
    DecodeBScan(MV.Data, MV.DataCount, OnMViewDecode, i);
  end;
  Result := FLastBScan;
end;

procedure TDefCore.BScanRecieved(BV: TBVBuffEl);
begin
  if Assigned(OnBScanReserved) then
    OnBScanReserved(BV);
end;

function TDefCore.Boot_LoadConfig: Integer;
var
  Res: string;
begin
  // ���� ������
  // 0 - ��� ������, �������� ������� ������
  // 1 - �� ������ �������� ���������� �������
  // 2 - �� ������ �������� ��������
  // 4 - �� ������ �������� ������������

  // ��������� ��������� ������������
  Result := 0; //
  Res := 'ERROR';
  if Assigned(OnTextMessage) then
    OnTextMessage('LoadSysConfig', true);
  if KConf.LoadSystemConfig then
    Res := 'OK'
  else
    Result := 1;
  if Assigned(OnTextMessage) then
    OnTextMessage(Res, false);

  KConf.ClearOldNastr;
  Res := 'ERROR';
  if Assigned(OnTextMessage) then
    OnTextMessage('LoadAdj', true);
  if KConf.LoadUserConfig(0) then
    Res := 'OK'
  else
    Result := Result + 2;
  if Assigned(OnTextMessage) then
    OnTextMessage(Res, false);
  Res := 'ERROR';
  if Assigned(OnTextMessage) then
    OnTextMessage('Che�kConfig', true);
  if KConf.FinishConfig then
    Res := 'OK'
  else
    Result := Result + 4;
  if Assigned(OnTextMessage) then
    OnTextMessage(Res, false);
end;

procedure TDefCore.OnExceptionFinal(LogSaved: Boolean);
begin
  try
    FUSB.Close;
    Application.MessageBox(
      'Program failure. Log have been saved in ERRORS folder.',
      'Program error', 16);
  finally
    Halt;
  end;
end;

function TDefCore.Boot_TryRunBUM: Integer;
var
  // Res: string;
  i: Integer;
  Res: Boolean;
begin
  // ���� ������
  // 0 - ��� ������, �������� ������ ���������
  // 8 - �� ������ ������� CAN'� (������� ����� ���� �����������)
  // 16 - �� ������ ���������� � ���'�� (������� ����� ���� �����������)

  // ������� ������� ����� �����
  // Res:='������\n';

  // �������� ����� ���������
  Result := 8 + 16;
{$IFDEF UseHardware}
{$IFDEF Avikon16_IPV}
  FUSB.Open;
{$ENDIF}
{$IFDEF EGO_USW}
  FUSB.Open;
{$ENDIF}
{$IFDEF USK004R}
  FUSB.Open;
{$ENDIF}
{$IFDEF Avikon14_2Block}
  FUSB.Open;
{$ENDIF}
{$IFDEF RKS_U}
  FUSB.Open;
{$ENDIF}
{$IFDEF VMT_US}
  FUSB.Open;
{$ENDIF}
{$ENDIF}
{$IFDEF SecondUSBRemotePanel}
  RemotePanel.Open;
{$ENDIF}
  FTerminate := false;
end;

procedure TDefCore.AKPConnected(ID: Integer);
begin
  AddBUM(ID);
end;

procedure TDefCore.AKPDisconnected(ID: Integer);
begin
  // ShowMessage(Format('��� � %d ������� !', [ID]));
end;

function TDefCore.GetUSBDevicesCount: Integer;
var
  i: Integer;
begin
  { if Assigned(FUSB) then
    Result:=FUSB.DeviceCount; }
  Result := 0;
  for i := 0 to FCANList.Count - 1 do
    if FCANList[i].BUMReady then
      inc(Result);
end;

procedure TDefCore.AddBUM(ID: Integer);
var
  BUM: TCAN;
  i: Integer;
begin
  BUM := TCAN.Create(FUSB, ID, _WrLog_, WinMessage, true);
  i := 0;
{$IFDEF BUMCOUNT4}
  // BUM.BlockStatus:=
  // if FCANList.Count=4 then //
  // else
{$ELSE}
  if not FMasterIsSet then
  begin
    BUM.BlockStatus := 0;
    FMasterIsSet := true;
  end
  else
  begin
    BUM.BlockStatus := 1;
  end;
{$ENDIF}
  FCANList.Add(BUM);
  BUM.OnSensorStateChanged := SensorStateChanged;
  BUM.OnDonnEnvelopeReserved := DonnEnvelopeReserved;
  BUM.OnOneSensorStateChanged := OneSensorStateChanged;
  BUM.OnFullCoordRecieved := FullCoordChanged;
  BUM.PainterSetCoord(0);
  BUM.OnAirBrushDone := AirBrushDone;
  BUM.PainterOn(1000);
  BUM.PainterSetCoord(0);
end;

function TDefCore.GetBUMSN(ID: Byte): Word;
var
  i: Integer;

begin
  Result := 0;
  for i := 0 to FCANList.Count - 1 do
    if (FCANList[i].ID = ID) and FCANList[i].BUMReady then
      Result := FCANList[i].BUM_SN;
end;

procedure TDefCore.RemoveBUM(ID: Integer);
var
  i: Integer;
begin
  for i := 0 to FCANList.Count - 1 do
    if TCAN(FCANList.Items[i]).ID = ID then
    begin
      TCAN(FCANList.Items[i]).Free;
      FCANList.Delete(i);
    end;
end;

function TDefCore.GetCANByIdx(Index: Integer): TCAN;
begin
  Result := FCANList[Index];
end;

function TDefCore.GetCANCount: Integer;
begin
  Result := FCANList.Count;
end;

function TDefCore.GetCAN(ID: Byte): TCAN;
var
  i: Integer;
begin
  for i := 0 to FCANList.Count - 1 do
    if TCAN(FCANList.Items[i]).ID = ID then
      Result := FCANList.Items[i];
end;

procedure TDefCore.CANASDTurnOn;
var
  i: Integer;
begin
  for i := 0 to FCANList.Count - 1 do
    FCANList[i].ASDTurnOn;
end;

function TDefCore.CANStart: Boolean;
var
  i: Integer;

begin
  for i := 0 to FCANList.Count - 1 do
    TCAN(FCANList.Items[i]).Start;
end;

function TDefCore.CANStop: Boolean;
var
  i: Integer;

begin
  for i := 0 to FCANList.Count - 1 do
    TCAN(FCANList.Items[i]).Stop;
end;

procedure TDefCore.CANASDTurnOff;
var
  i: Integer;

begin
  for i := 0 to FCANList.Count - 1 do
    TCAN(FCANList.Items[i]).ASDTurnOff;
end;

function TDefCore.GetBScanDP(ID: Byte; var Count: Integer; var BV: TBVBuff)
  : Integer;
var
  i: Integer;
begin
  {
    Result:=0;
    for i:=0 to FCANList.Count-1 do
    if TCAN(FCANList.Items[i]).ID=ID then
    Count:=TCAN(FCANList.Items[i]).GetBV(BV, -1);
    }
end;

procedure TDefCore.StopDeviceSearching;
begin
{$IFDEF BUMCOUNT2}
  if Assigned(FUSB) then
    FUSB.StopDeviceSearching;
{$ELSE}
{$IFDEF BUMCOUNT4}
  if Assigned(FUSB) then
    FUSB.StopDeviceSearching;
{$ENDIF}
{$ENDIF}
end;

procedure TDefCore.CAN_SendTest;
var
  i: Integer;
begin
  for i := 0 to FCANList.Count - 1 do
    TCAN(FCANList.Items[i]).SendTestBlock;
end;

function TDefCore.StartTempRegistration: Integer;
var
  Res, i, DPStep: Integer;
  RegParam: TRegParamList;
  FRegFileName: string;
  FileType: TAviconID;
begin

  if Self.RegistrationStatus = rsOn then
    Self.StopRegistration;

  // ������������ ����� �����
  FRegFileName := ExtractFilePath(Application.ExeName)
    + 'Temp' + DeviceFileExtension;

  RegParam.Organization := Config.Organization;
  RegParam.Operator := '';
  RegParam.DirCode := 0;
  // RegParam.StartKM := 0;
  // RegParam.StartPK := 0;
  // RegParam.StartM := 0;
  RegParam.StartKM := ExternStartKM;
  RegParam.StartPK := ExternStartPK;
  RegParam.StartM := ExternStartM;

  RegParam.TrackNumber := 0;
  RegParam.Direction := 1;
  RegParam.PeregonName := '';
  RegParam.Scheme := Ord(Config.SoundedScheme) + 1;

  if ProgramProfile.DeviceFamily = 'Avicon16' then
  begin
    case Config.SoundedScheme of
      Wheels:
        FileType := Avicon16Wheel;
      Scheme1:
        FileType := Avicon16Scheme1;
      Scheme2:
        FileType := Avicon16Scheme2;
      Scheme3:
        FileType := Avicon16Scheme3;
    end;
  end
  else if ProgramProfile.DeviceFamily = 'EGO-USW' then
  begin
    case Config.SoundedScheme of
      Scheme1:
        FileType := EGO_USWScheme1;
      Scheme2:
        FileType := EGO_USWScheme2;
      Scheme3:
        FileType := EGO_USWScheme3;
    end;
  end
  else if ProgramProfile.DeviceFamily = 'Avicon14' then
  begin
    if Config.SoundedScheme = Wheels then
      FileType := Avicon14Wheel
    else
      FileType := Avicon14Scheme1;
  end
  else if ProgramProfile.DeviceFamily = 'RKS-U' then
  begin
    FileType := Avicon16Scheme1;
  end
  else if ProgramProfile.DeviceFamily = 'USK004R' then
  begin
    FileType := USK004R; // Avicon16Scheme1;//
  end;

  FGlobalFileType := FileType;
  DPStep := Round(Config.DPStep * 100);
  if (DPStep < 65534) and (DPStep > 0) then
    RegParam.ScanStep := DPStep
  else
    RegParam.ScanStep := 180;

  // {$IFDEF USK004R}
{$IFDEF TURKISH_COORD_WITH_DOT}
  RegParam.Chainage := '0000.000';
{$ELSE}
  if Config.UnitsSystem = usMetricTurkish then
    RegParam.Chainage := '0000+000'
  else
    RegParam.Chainage := '00000.000';
{$ENDIF}
{$IFDEF USK004R}
  RegParam.LeftSide := 0;
{$ENDIF}
  Res := General.Core.StartRegistration(FileType, FRegFileName, RegParam,
    false);
end;

procedure TDefCore.AKPSendError;
begin
  FSendError := true;
  inc(FSendErrorCount);
end;

procedure TDefCore.FullCoordChanged(BumID, Coord: Integer);
begin
  if (BumID >= 0) and (BumID < FCANList.Count) then
    FFullCoord[BumID] := Coord;
end;

procedure TDefCore.OneSensorStateChanged(SensorType, BumID: Integer;
  State: Boolean);
var
  Side: TRail;
begin
  if not Assigned(FDataContainer) then
    Exit;

  case SensorType of
    0:
{$IFDEF EGO_USW}
      if Config.DirectionOfMotion then
      begin // A-forward
        if (BumID = 0) then
          Side := rLeft
        else if (BumID = 2) then
          Side := rRight;
        if BumID in [0, 2] then
          FDataContainer.AddSensor1State(Side, State);
      end
      else
      begin // B-forward
        if (BumID = 1) then
          Side := rLeft // ???
        else if (BumID = 3) then
          Side := rRight;
        if BumID in [1, 3] then
          FDataContainer.AddSensor1State(Side, State);
      end;
{$ELSE}
    FDataContainer.AddSensor1State(TRail(BumID), State);
{$ENDIF}
    // 1: FDataContainer.AddSensor2State(TRail(BumID), State);
    end;
{$IFDEF TestSensor}
    if SensorType = 0 then begin if BumID =
      0 then begin if State then SensorStateBum1 := 1 else SensorStateBum1 :=
      0; end;

    if BumID = 1 then begin if State then SensorStateBum2 :=
      1 else SensorStateBum2 := 0; end; end;
{$ENDIF}
    end;

    procedure TDefCore.OnShowErrorTimer(Sender: TObject);
    begin if FSendError then begin MessageForm.Display
      (General.LanguageTable['������ �������� ������'] + Format
        (' (%d).', [FSendErrorCount]), kBad);
    MessageForm.WaitTimer.Interval := 2000; end

      else if FSensorChanged then begin
    // MessageForm.Display( Format( 'Joint: %d / Switch: %d', [ FSensorJointCnt, FSensorSwitchCnt ] ), kBad );
    MessageForm.Display(FSensorMsg, kBad);
    MessageForm.WaitTimer.Interval := 500; FSensorChanged := false; end;

    FSendError := false; BumFailed := false; end;

    procedure TDefCore.RunKraskopult(Rail: Byte); var BUM:
      Byte;
    begin // 0-Left, 1-Right
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US) }
      BUM := RailToBUM(Rail, Config.DirectionOfMotion);
      if Config.PaintSprayer <> prAlwaysOff then
        FCANList[BUM].RunKraskopult;

      { edAirBrushItem = packed record // 11 ���� (���� �� �-���������)
        ScanCh: Byte; // ����� ���������
        StartDisCoord: Integer; // ���������� ���������� ������ ����
        EndDisCoord: Integer; // ���������� ���������� ����� ����
        StartDelay: Byte; // �������� ������ ����
        EndDelay: Byte; // �������� ����� ����
        end;

        TABItemsList = array of edAirBrushItem;
      }
      // AddAirBrushMark(Rail: TRail; DisCoord: Integer; Items: TABItemsList);
      FDataContainer.AddAirBrushMark(TRail(Rail), CurDisCoord, Nil);
{$ELSE}
      if (Rail >= 0) and (Rail <= FCANList.Count - 1) then
        FCANList[Rail].RunKraskopult;
{$IFEND}
    end;

    procedure TDefCore.DonnEnvelopeReserved(Rail, Takt, Amplify: Byte;
      Delay: Single);
    begin
      LastDonnEnvelope[Rail].Ampl := Amplify;
      LastDonnEnvelope[Rail].New := true;
    end;

    procedure TDefCore.SensorStateChanged(SensorKind: TSensorKind;
      StateChangeDirection: TStateChangeDirection);
    begin

    end;

    function TDefCore.GetMovingForward: Boolean;
    begin
      if Assigned(FDataContainer) then
        Result := FDataContainer.Header.MoveDir = 1
      else
        Result := true;
    end;

    function TDefCore.InstallOneKanal(Kanal: TKanal): Integer;
    var
      i: Integer;
    begin
{$IFDEF UseHardware}
      if Assigned(Kanal) then
      begin
        for i := 0 to FCANList.Count - 1 do
          TCAN(FCANList.Items[i]).Stop;
        for i := 0 to FCANList.Count - 1 do
        begin // AK
          TCAN(FCANList.Items[i]).InitCikl(1); //
          TCAN(FCANList.Items[i]).SetTakt
            (0, $F, $F, $F, $F, 1, 100, Kanal.Takt.GetWSA(0, 0), 0);
        end;
        // ��� ���������� ����� ��� ���������� ��������� � �������� � 0xFF � ��������� �����
        if not Kanal.Takt.CANObj.InitCikl(1) then
          Exit;
        if not Kanal.Takt.Install(FRegMode) then
          Exit;
        CANASDTurnOn;
        CANStart;
        if FRegMode = rHand then
          ResetHandContainer;
      end;
{$ENDIF}
    end;

    procedure TDefCore.CANImitDP(isOn: Boolean);
    var
      i: Integer;
    begin
      for i := 0 to FCANList.Count - 1 do
        FCANList[i].ImitDP(isOn);
    end;

    procedure TDefCore.SetAllHKanalsNOTActive;
    var
      i: Integer;
    begin
      for i := 0 to High(HKanals) do
        HKanals[i].IsActive := false;
    end;

    procedure TDefCore.Av14USBOpen;
    begin
      if Assigned(FUSB) then
        if FUSB.Open then
        begin
{$IFDEF UseA11Panel}
          FUSB.InitPanel;
{$ENDIF}
          AV14USBOpened := true;
          AddBUM(0);
        end;
    end;

    procedure TDefCore.InitHandRegThread;
    begin
{$IFDEF UseHardware}
      if Assigned(FBScanStoreThread) then
        FBScanStoreThread.Suspend;
      LogCS.Enter;
      // ChangeModeInRegFile(rmHand, 0, 0);
      if Assigned(FHandContainer) then
        FHandContainer.Free;
      FHandContainer := TAviconDataContainer.Create
        (ConfigList.GetItemByID(AviconHandScan, 1), true, true, false, false,
        GetFullPath + 'HS.tmp');
      FHandContainer.AddHeader(1, 180);
      // !!!!!!!!!!!!
      // FHandContainer.AddTextLabel('!');
      // FHandContainer.AddTime(Now);
      if Assigned(FBScanStoreThread) then
      begin
        FBScanStoreThread.SetContainer(FHandContainer);
        // FBScanStoreThread.SetContainer(nil);
        FBScanStoreThread.Hand := true;
        // FBScanStoreThread.HandBUM:= FCurKanal.Takt.BUM;
        FBScanStoreThread.Resume;
      end;
      LogCS.Leave;

      // if not Assigned(FHandStoreThread) then
      // FHandStoreThread := THandStoreThread.Create(FCANList.Items[0], 0);
{$ENDIF}
      sleep(500);
      CANImitDP(true);
      Self.UnPauseRegThread;
    end;

    procedure TDefCore.KillHandRegThread;
    var
      BV: TBVBuff;
      j, i, c: Integer;
    begin
      CANImitDP(false);
      sleep(500);
{$IFDEF UseHardware}
      if not Assigned(FHandContainer) then
        Exit;
      if Assigned(FBScanStoreThread) then
        FBScanStoreThread.Suspend;
      LogCS.Enter;
      if Assigned(FBScanStoreThread) then
      begin
        { FBScanStoreThread.SetContainer(nil);
          CANImitDP(true);
          for j:= 0 to 10-1 do
          begin
          sleep(50);
          for i:= 0 to FCanList.Count - 1 do
          c:= FCANList[i].GetBV(BV);
          end; }

        FBScanStoreThread.SetContainer(FDataContainer);
        // FBScanStoreThread.SetContainer(nil);
        FBScanStoreThread.Hand := false;
        FBScanStoreThread.Resume;
      end;
      if Assigned(FHandContainer) then
      begin
        FHandContainer.Free;
        FHandContainer := nil;
      end;
      LogCS.Leave;
      Self.UnPauseRegThread;
{$ENDIF}
      { if Assigned(FHandStoreThread) then
        begin
        if FHandStoreThread.Suspended then
        FHandStoreThread.Resume;
        FHandStoreThread.Terminate;
        FHandStoreThread.Free;
        FHandStoreThread := nil;
        end; }
    end;

    (* <Rud30> *)
    procedure TDefCore.SetBoltJointMode(Mode: Boolean);
    const
      ECHO_0_STR_EN = 18;
      ECHO_42_STR_ST = 42;
    var
      R: Byte;
    begin
      FBoltJointMode := Mode;

      for R := 0 to {$IFDEF TwoThreadsDevice} 1 {$ELSE} 0 {$ENDIF} do
      begin
        // 2 ��� �� ������� 6, 7.
        Kanal[R, 6].TwoEcho := Mode;
        Kanal[R, 7].TwoEcho := Mode;
        SendBum.Add(Kanal[R, 6], cInstallStrob);
        SendBum.Add(Kanal[R, 7], cInstallStrob);

        // ����. ���� �� ������� 4, 5, 8, 9.
        Kanal[R, 4].SoundOn := not Mode;
        Kanal[R, 5].SoundOn := not Mode;
        Kanal[R, 8].SoundOn := not Mode;
        Kanal[R, 9].SoundOn := not Mode;
        (* {$IFDEF Avikon16_IPV}
          if Config.SoundedScheme = Wheels then
          begin
          Kanal[R, 12].SoundOn := not Mode;
          Kanal[R, 13].SoundOn := not Mode;
          end
          else
          begin
          Kanal[R, 10].SoundOn := not Mode;
          Kanal[R, 11].SoundOn := not Mode;
          end;

          {$ENDIF}
          //{$IFDEF EGO_USW}
          //      Kanal[R, 12].SoundOn := not Mode;
          //      Kanal[R, 13].SoundOn := not Mode;
          //{$ENDIF}
        *)
        // �� ������ 1 ����� ������ 55 ��.
        if Mode then
        begin
          BM_K1StrSt[R] := Kanal[R, 1].Str_st;
          if Kanal[R, 1].Str_st >= ECHO_0_STR_EN then
            Kanal[R, 1].Str_st := ECHO_0_STR_EN - 1;

          BM_K1StrEn[R] := Kanal[R, 1].Str_en;
          Kanal[R, 1].Str_en := ECHO_0_STR_EN;

          BM_K6StrSt[R] := Kanal[R, 6].Str_st;
          Kanal[R, 6].Str_st := ECHO_42_STR_ST;
          BM_K7StrSt[R] := Kanal[R, 7].Str_st;
          Kanal[R, 7].Str_st := ECHO_42_STR_ST;
{$IFDEF Avikon16_IPV}
          if Config.SoundedScheme = Wheels then
          begin
            BM_K11StrSt[R] := Kanal[R, 11].Str_st;
            if Kanal[R, 11].Str_st >= ECHO_0_STR_EN then
              Kanal[R, 11].Str_st := ECHO_0_STR_EN - 1;

            BM_K11StrEn[R] := Kanal[R, 11].Str_en;
            Kanal[R, 11].Str_en := ECHO_0_STR_EN;
          end
{$ENDIF}
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
          BM_K11StrSt[R] := Kanal[R, 11].Str_st;
          if Kanal[R, 11].Str_st >= ECHO_0_STR_EN then
            Kanal[R, 11].Str_st := ECHO_0_STR_EN - 1;

          BM_K11StrEn[R] := Kanal[R, 11].Str_en;
          Kanal[R, 11].Str_en := ECHO_0_STR_EN;
{$IFEND}
        end
        else
        begin
          Kanal[R, 1].Str_st := BM_K1StrSt[R];
          Kanal[R, 1].Str_en := BM_K1StrEn[R];

          Kanal[R, 6].Str_st := BM_K6StrSt[R];
          Kanal[R, 7].Str_st := BM_K7StrSt[R];
{$IFDEF Avikon16_IPV}
          if Config.SoundedScheme = Wheels then
          begin
            Kanal[R, 11].Str_st := BM_K11StrSt[R];
            Kanal[R, 11].Str_en := BM_K11StrEn[R];
          end;
{$ENDIF}
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
          Kanal[R, 11].Str_st := BM_K11StrSt[R];
          Kanal[R, 11].Str_en := BM_K11StrEn[R];
{$IFEND}
        end;
        SendBum.Add(Kanal[R, 1], cInstallStrob);
      end;

      // ������� � ����.
      if Mode then
        Registrator.AddStBoltStyk
      else
        Registrator.AddEdBoltStyk;

    end;
    (* </Rud30> *)

    procedure TDefCore.ButtonStateChange(Button: TConsolButtons;
      State: Boolean);
    begin
      case Button of
        cbCoord:
          begin
            if not State then
            begin
              // General.OpenWindow('PLACEMARK');
              CoordMark := true;
            end;
          end;
        cbBoltJoint:
          SetBoltJointMode(State);
      end;
    end;

    function TDefCore.GetUAKK: Integer;
    begin
      if Assigned(FUSB) then
        Result := FUSB.GetUAKK
      else
        Result := 0;
    end;

    function TDefCore.RealU2CID(ID: Byte): Byte;
    var
      rID: Byte;
    begin
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
      if not FlSidesChanged then
        Result := ID
      else if ID = 0 then
        Result := 1
      else
        Result := 0;
{$ELSE}
      Result := ID;
{$IFEND}
    end;

    function TDefCore.GetU2CSN(ID: Byte): Integer;
    begin
{$IF DEFINED(BUMCOUNT2) OR DEFINED(BUMCOUNT4)}
      if Assigned(FUSB) then
        Result := FUSB.GetU2CSerial(RealU2CID(ID));
{$ELSE}
      Result := 0;
{$IFEND}
    end;

    function TDefCore.GetU2CVersion(ID: Byte): String;
    const
      SizeU2CVersion = 8;
    var
      Buffer: array [0 .. 7] of Byte;
    begin
{$IF DEFINED(BUMCOUNT2) OR DEFINED(BUMCOUNT4)}
      if Assigned(FUSB) then
      begin
        // ���� ������� ������ 0 ��� 0 ����,�� ���� ������� ������ ������
        if FUSB.GetU2CVersion(@Buffer[0], RealU2CID(ID)
          { SizeU2CVersion } ) <= 0 then
          Result := '-'
        else
          Result := IntToStr(Buffer[3]) + '.' + IntToStr(Buffer[2])
            + '.' + IntToStr(Buffer[1]);
      end

      else
{$ELSE}
        Result := '-';
{$IFEND}
    end;

    procedure TDefCore.GetButtonSt(B: Byte);
    var
      But: TConsolButtons;
      State: Boolean;
    begin
      State := (128 and B) = 128;
      if (B = 128) or (B = 0) then
        But := cbCoord;
      if (B = 129) or (B = 1) then
        But := cbBoltJoint;
      if (B = 130) or (B = 2) then
        But := cbWeldJoint;
      // if Assigned(OnButtonStateChange) then
      /// OnButtonStateChange(But, State);
      Self.ButtonStateChange(But, State);
    end;

    procedure TDefCore.SetReleMode(Mode: Byte);
    var
      CAN: TCAN;
      i: Integer;
    begin
      for i := 0 to FCANList.Count - 1 do
      begin
        CAN := TCAN(FCANList.Items[i]);
        CAN.SetReleMode(Mode);
      end;
    end;

    procedure TDefCore.CheckAirBrushTasks(CurDisCoord: Integer);
    // use only for testing
    var
      i, j, k: Integer;
      MinCoord: array [0 .. 1] of Integer;
    begin
      for j := 0 to 1 do
        if (FAirBrushTaksMas[j, MinCoordTaskIdx[j]] >= 0) and
          (FAirBrushTaksMas[j, MinCoordTaskIdx[j]] <= CurDisCoord) then
        begin

          if (Config.PaintSprayer <> prAlwaysOff) and FisPaintSprayerOn then
            Self.RunKraskopult(j);
          // Self.FDataContainer.AddTextLabel('AirBrush');
          Self.FDataContainer.AddDefLabel(TRail(j), 'AirBrush');

          FAirBrushTaksMas[j, MinCoordTaskIdx[j]] := -1;

          MinCoordTaskIdx[0] := 0;
          MinCoordTaskIdx[1] := 0;

          for k := 0 to 1 do
            for i := 0 to { High(FAirBrushTaksMas) } MaxAirBrushTaskCount - 1 do
              if (FAirBrushTaksMas[k, i] < MinCoord[k]) and
                (FAirBrushTaksMas[k, i] > 0) then
              begin
                MinCoordTaskIdx[k] := i;
                MinCoord[k] := FAirBrushTaksMas[k, i];
              end;
        end;
    end;

    procedure TDefCore.LoadExternFile(FileName: string);
    begin
      FExternFileData := TAviconDataSource.Create;
      FExternFileData.LoadFromFile(FileName { ,false } );
      ExternStartKM := FExternFileData.Header.StartKM;
      ExternStartPK := FExternFileData.Header.StartPK;
      ExternStartM := FExternFileData.Header.StartMetre;
      FirstTimeStr := true;
      // ���� ��������� ���������� ��������� ������, ������� � ����.
    end;

    procedure TDefCore.CloseExternFile;
    begin
      if Assigned(FExternFileData) then
      begin
        FExternFileData.Free;
        FExternFileData := nil;
      end;
    end;

    procedure TDefCore.NextExternCoord(Delta: Integer);
    begin
      UseRegistratorCoord := true;
      if Assigned(FExternFileData) then
      begin
        FCurExternCoord := FCurExternCoord + Delta;
        FExternFileData.LoadData(FLastExternCoord, FCurExternCoord, 0,
          ProcSign);
        Self.CheckAirBrushTasks(FDataContainer.MaxDisCoord);
        FLastExternCoord := FCurExternCoord;
      end;
    end;

    function TDefCore.ProcSign(StartDisCoord: Integer): Boolean;
    var
      i, j, k, Count, Count1: Integer;
      EchoBlock: TEchoBlock;
      R: TRail;
      EvalChIdx, EvalCh: Integer;
      Flag: Boolean;
    begin
      if FirstTimeStr then
      begin
        FirstTimeStr := false;
        for R := rLeft to rRight do
          for EvalChIdx := 0 to FDataContainer.Config.EvalChannelCount - 1 do
          begin
            EvalCh := FDataContainer.Config.EvalChannelByIdx[EvalChIdx].Number;
            if EvalChIdx <> 0 then
            begin
              FDataContainer.AddStStr
                (R, EvalCh, FExternFileData.CurParams.Par[R, EvalCh].StStr);
              FDataContainer.AddEndStr
                (R, EvalCh, FExternFileData.CurParams.Par[R, EvalCh].EndStr);
            end
            else
            begin
              FDataContainer.AddStStr(R, EvalCh, 49);
              FDataContainer.AddEndStr(R, EvalCh, 58);
            end;
          end;
      end;

      FDataContainer.AddSysCoord(FExternFileData.CurSysCoord);
      // Self.CheckAirBrushTasks(FExternFileData.CurSysCoord);
      for i := 0 to 1 do
        for j := 0 to 15 do
        begin
          Count := FExternFileData.CurEcho[TRail(i), j].Count;

          Count1 := 0;
          for k := 0 to Count - 1 do
          begin
            Flag := true;
            if j in [0, 1] then
              if FExternFileData.CurEcho[TRail(i), j].Delay[k + 1] < 72 then
                Flag := false;

            if Flag then
            begin
              inc(Count1);
              EchoBlock[Count1 - 1].T := FExternFileData.CurEcho[TRail(i), j]
                .Delay[k + 1];
              EchoBlock[Count1 - 1].A := FExternFileData.CurEcho[TRail(i), j]
                .Ampl[k + 1];
            end;
          end;
          FDataContainer.AddEcho(TRail(i), j, Count1, EchoBlock);
        end;
      Result := true;
    end;

    procedure TDefCore.OnCoordChange(Coord: Integer);
    begin
      // if abs(Coord - FOldCoord) >= 10 then
      // begin

      if Config.PainterState <> psAlwaysOff then
      begin
        if Assigned(FAirBrush) then
        begin
          // FAirBrush.Stop;
          // FAirBrush.Tick;
        end;
        // CheckAirBrushTasks(Coord);
        FOldCoord := Coord;
      end;
      Self.UseRegistratorCoord := true;
      // end;
    end;

    function TDefCore.GetVelocity: Single;
    begin
      if FCANList.Count > 0 then
        Result := FCANList[0].GetVelocity;
    end;

    procedure TDefCore.InsertWholeExternFile;
    begin
      if Assigned(FExternFileData) then
      begin
        // FCurExternCoord:= FCurExternCoord + Delta;
        FExternFileData.LoadData(0, FExternFileData.MaxDisCoord, 0, ProcSign);
        // Self.CheckAirBrushTasks(FDataContainer.MaxDisCoord);
        // FLastExternCoord:= FCurExternCoord;
      end;
    end;

    function TDefCore.CountExternAndRegDiff: Integer;
    var
      CurCaCoord: AviconTypes.TCaCrd;
      CurMRFCoord: AviconTypes.TMRFCrd;
      SysCoordInExternFile, DisCoordInExternFile: Integer;
    begin
      // ����������� ������� � ���������� ���������� ����� ������� ������ ����������� � �������� ������� ������
      if Assigned(FExternFileData) and Assigned(FDataContainer) then
      begin
        if Config.UnitsSystem = usRus then
        begin
          CurMRFCoord := FDataContainer.DisToMRFCrd(FDataContainer.MaxDisCoord);
          FExternFileData.MRFCrdToDisCrd(CurMRFCoord, DisCoordInExternFile);
        end
        else
        begin
          CurCaCoord := FDataContainer.DisToCaCrd(FDataContainer.MaxDisCoord);
          FExternFileData.CaCrdToDis(CurCaCoord, DisCoordInExternFile);
        end;
        DisDifExternAndRegFile := FDataContainer.MaxDisCoord -
          DisCoordInExternFile;
      end;

    end;

    procedure TDefCore.AirBrushDone(CAN_ID: Byte; Coord, ErrCode: Integer;
      Delta: Word);
    begin
      // ��������� � ��������� ���������� � ����������� ������������ ������������
      // a := 1;
      if Assigned(FDataContainer) then
        FDataContainer.AddAirBrushJob(TRail(CAN_ID), Coord, ErrCode, Delta);
    end;

    procedure TDefCore.TestRunAirBrush;
    begin
      if Assigned(FDataContainer) then
      begin
        if (Config.PaintSprayer <> prAlwaysOff)
          and FisPainterOn and FisPaintSprayerOn then
          FCANList[0].PainterSetMark(FCANList[0].BUMCoord);
      end;
    end;

    procedure TDefCore.HandRailType;
    const
      IterationCount = 8;
    var
      AData: PAVBuff;
      i, j: Integer;
      StartSearch, StopSearch: Integer;
      MaxAmp: Byte;
      MaxT: Byte;
      MaxTMas: array [0 .. IterationCount - 1] of Byte;
      ResT, RealC: Integer;
      s: string;
      Kind: TWinKind;
    begin
      StartSearch := 0;
      StopSearch := 232;
      if Assigned(FCurKanal) then
      begin
        StartSearch := Round((40 * 232) / (FCurKanal.Takt.AV_mash * 23.2));
        if StartSearch > 231 then
          StartSearch := 231;
        StopSearch := Round((70 * 232) / (FCurKanal.Takt.AV_mash * 23.2));
        if StopSearch > 231 then
          StopSearch := 231;
      end;
      RealC := 0;
      ResT := 0;
      for i := 0 to IterationCount - 1 do
      begin
        AData := nil;
        Self.GetAScan(AData);
        if Assigned(AData) then
        begin
          MaxAmp := 0;
          MaxT := 0;
          for j := StartSearch to StopSearch do
          begin
            if (AData^.AV[j] > MaxAmp) and (AData^.AV[j] > 32) then
            begin
              MaxAmp := AData^.AV[j];
              MaxT := j;
            end;
          end;
        end;
        if MaxT > 0 then
        begin
          ResT := ResT + MaxT;
          inc(RealC);
        end;
        sleep(200);
      end;
      if RealC <> 0 then
        ResT := Round(ResT / RealC)
      else
        ResT := 0;
      ResT := Round((FCurKanal.Takt.AV_mash * 23.2 * ResT) / 232);
      if (ResT >= 40) and (ResT <= 70) then
      begin
        s := Format(General.LanguageTable['������ � ������ ������ %d ��.'],
          [Round(MKS2MM(ResT, 0, true))]);
        Kind := kGood;
        AdjRailHand(ResT);
      end
      else
      begin
        s := General.LanguageTable['��� ������� �������.'];
        Kind := kBad;
        AdjRailHand(ProgramProfile.DefaultRailTypeTime);
      end;
      MessageForm.Close;
      MessageForm.Display(s, Kind);
    end;

    procedure TDefCore.PauseRegThread;
    begin
      if Assigned(FBScanStoreThread) then
        FBScanStoreThread.Pause := true;
    end;

    procedure TDefCore.UnPauseRegThread;
    begin
      if Assigned(FBScanStoreThread) then
        FBScanStoreThread.Pause := false;
    end;

    function TDefCore.ResetHandContainer: TAviconDataContainer;
    var
      NewContainer: TAviconDataContainer;
    begin
      sleep(300);
      Result := nil;
      if Assigned(FBScanStoreThread) then
        FBScanStoreThread.Suspend;
      LogCS.Enter;

      NewContainer := TAviconDataContainer.Create
        (ConfigList.GetItemByID(AviconHandScan, 1), true, true, false, false,
        GetFullPath + 'HS.tmp');
      NewContainer.AddHeader(1, 180);

      if Assigned(FBScanStoreThread) then
      begin
        FBScanStoreThread.SetContainer(NewContainer);
        FBScanStoreThread.Hand := true;
        FBScanStoreThread.Resume;
      end;

      if Assigned(FHandContainer) then
        FHandContainer.Free;

      FHandContainer := NewContainer;

      LogCS.Leave;
      Self.UnPauseRegThread;
      Result := FHandContainer;
    end;

    procedure TDefCore.Stop;
    begin
      if Assigned(FBScanStoreThread) then
        FBScanStoreThread.SetContainer(nil);
    end;

    procedure TDefCore.Resume;
    begin
      if Assigned(FBScanStoreThread) then
        FBScanStoreThread.SetContainer(FDataContainer);
    end;

    procedure TDefCore.SetFRegOnUserLevel(Value: Boolean);
    begin
      FFRegOnUserLevel := Value;
    end;

    procedure TDefCore.AirBrushEvent2(Rail: TRail; DisCoord, SysCoord: Integer;
      RealCrd: Integer; ABItems: TABItemsList);
    var
      A, B, i, j: Integer;
      CAN: TCAN;
      idxCAN: Byte;
    begin
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US) }
      idxCAN := RailToBUM(Byte(Rail), Config.DirectionOfMotion);
      if idxCAN < FCANList.Count then
        CAN := FCANList[idxCAN]
      else
        Exit;
{$ELSE}
      if Ord(Rail) >= FCANList.Count then
        Exit;
      CAN := FCANList[Ord(Rail)];
{$IFEND}
      if not Assigned(CAN) then
        Exit;

      A := 0;
      if Assigned(FDataContainer) then
        A := FDataContainer.CurSaveSysCoord;

      B := CAN.BUMCoord;

      if (Config.PaintSprayer = prByAlgorithm)
        and FisPainterOn and FisPaintSprayerOn then
        CAN.PainterSetMark(B + (A - SysCoord));
      if Assigned(FDataContainer) and FisPainterOn then
        FDataContainer.AddAirBrushMark(Rail, RealCrd, ABItems);
    end;
{$IFDEF BUMCOUNT4}

    procedure TDefCore.ChangeLeftRightSide(); // ������ ����� � ������ ���� � ������� ������� ������� � ������ CAN.ID
    var
      i: Integer;
    begin
      // 0,1,2,3
      FCANList.Move(0, 3); // 1,2,3,0
      FCANList.Move(0, 3); // 2,3,0,1

      for i := 0 to FCANList.Count - 1 do
      begin
        FCANList[i].SetID(i);
        { case FCANList[i].ID of
          0: FCANList[i].SetID(2);
          1: FCANList[i].SetID(3);
          2: FCANList[i].SetID(0);
          3: FCANList[i].SetID(1);
          end;
        }
      end;
      // Change U2C Adapters
      FlSidesChanged := true;
    end;
{$ENDIF}

    procedure TDefCore.OnAlarmEvent(Rail: TRail; Channel: Integer;
      State: Boolean);
    var
      Kanal1: TKanal;
      BUM: Byte;
      Nit: Byte;
      k: Integer;
    begin
      k := Channel;
      Nit := Byte(Rail);
      if (Nit in [0, 1]) and (k >= 0) and (k <= High(Kanals[Nit])) and
        (Kanals[Nit, k] <> nil) then
      begin

        // Kanal1 := Kanals[Nit, K];

        // Kanal1.ASD := (not Kanal1.InverseASD) { and Kanal1.SoundOn } ;
        // Kanal1.ASD := State;//(State xor Kanal1.InverseASD) { and Kanal1.SoundOn } ;
        Kanals[Nit, k].ASD := State;
        // {$IFDEF EGO_USW}
        // Snd.SetASD( Kanal1.BUM, K, (Kanal1.ASD and Kanal1.SoundOn) );
        // {$ELSE}
        // Snd.SetASD( Nit, K, (Kanal1.ASD and Kanal1.SoundOn) );
        Snd.SetASD(Nit, k, (State and Kanals[Nit, k].SoundOn));
        // {$ENDIF}
      end;
    end;
    // {$ENDIF}
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US) }

    procedure TDefCore.ChangeFrontAndBack(); // ������ ���������� � ����������� ������ �������
    var
      i: Integer;
    begin

    end;

    procedure TDefCore.SetChan0Regim(SelectChan0: TSelectChan0);
    begin
      // ����� BScanLine � �������� 0����

      // ��������� ���
      if (SelectChan0 = scWPA) then
      begin
        Kanals[0, 10].SoundOn := false;
        Kanals[1, 10].SoundOn := false;
        Kanals[0, 0].SoundOn := true;
        Kanals[1, 0].SoundOn := true;
      end
      else if (SelectChan0 = scWPB) then
      begin
        Kanals[0, 0].SoundOn := false;
        Kanals[1, 0].SoundOn := false;
        Kanals[0, 10].SoundOn := true;
        Kanals[1, 10].SoundOn := true;
      end
      else
      begin
        Kanals[0, 0].SoundOn := true;
        Kanals[1, 0].SoundOn := true;
        Kanals[0, 10].SoundOn := true;
        Kanals[1, 10].SoundOn := true;
      end;

      // �������� � �����������
      if Assigned(FDataContainer) then
        FDataContainer.SetZerroProbMode(TZerroProbMode(SelectChan0));
      if Assigned(FAirBrush) then
        FAirBrush.SetZerroProbMode(TZerroProbMode(SelectChan0));
      // !!!!!�����������������
    end;
{$IFEND}

    procedure TDefCore.SetPaintSprayer(isOn: Boolean);
    begin
      FisPaintSprayerOn := isOn;
      if Assigned(FDataContainer) then
        FDataContainer.AddAirBrushTempOff
          (not isOn, General.Core.Registrator.MaxDisCoord); // Off
    end;

end.
