unit MCAN;

interface

{$I Directives.inc}
{ $DEFINE AKPSimulation }
{$DEFINE StartStopProcess}
{$DEFINE GlideEnvelope}

// ��������� ����� (�������� id)  ��� EGO-USW
//      id                                       ����� ��������/���
//      0                                        0/JC-L
//      1                                        0/JC-R
//      2                                        1/JC-L
//      3                                        1/JC-R

uses Forms, Classes, ExtCtrls, ConfigObj, Types, SyncObjs,
  PublicData, PublicFunc,
  DeviceInterfaceUnit, InterfaceCAN, SoundControlUnit, TextFilesIO,
  ProgramProfileConfig;

const
  BVBufferCount = 1000;
  WriteBufferCount = 500;
  AVBuffSize = 232;

  MaxVRCH_Time = 41;
  MaxATT_Level = 80;
  MaxAK_Level = 127;

  MViewTimeout = 2000;
  FBUMTestBlock: TBlock = ($DE, 0, 0, 0, 0, 0, 0, 0, 0, 0);

type
  AViewArr = array of Byte;

  TAVBuff = record
    AV: array [0 .. AVBuffSize - 1] of Byte;
    Pos: Integer;
    AmaxK0: Byte;
    TmaxK0: Byte;
    Tmax: Single;
    Amax: Byte;
  end;

  TBVBuffEl = packed record
    Napr: ShortInt;
    DataCount: Byte;
    Data: TBVBuffer;
{$IFDEF TESTING_COORD}
    FullCoord: Integer;
    DeltaCoord: ShortInt;
{$ENDIF}
  end;

  TBVBuff = array [0 .. BVBufferCount - 1] of TBVBuffEl;
  PBVBuff =^TBVBuff;
  TWBuff = array [0 .. WriteBufferCount - 1] of Byte;


  TMVBuffer = packed record
    DataCount: Byte;
    Data: TBVBuffer;
  end;

  PMVBuffer = ^TMVBuffer;

  {
    TMBuff = packed record
    BlockCount: array[0..1] of Byte;
    ByteCount: Byte;
    Data: array[0..319] of Byte; // ���-�� ���� ������� (1�), �����-���� (1�: ��.���==����), ��������� ����� ������� (���. + ����., ����.)
    end;
    PMBuff = ^TMBuff;
    }

  PAVBuff = ^TAVBuff;

  TAdjustParamChange = record
    Flag: Boolean;
    CurValue: Integer;
  end;

  TAdjustParams = (apKy, apATT, apStrobSt, apStrobEd, apVRCH, apSound, apTwoTp);

  TAdjustChanges = array [apKy .. apTwoTp] of TAdjustParamChange;

  TStateChangeDirection = (sSwitchedOff = 0, sSwitchedOn = 1);
  TSensorKind = (sBoldJointSensor = 0, sSwitchSensor = 1);
  TOnSensorStateChanged = procedure(SensorKind: TSensorKind;
    StateChangeDirection: TStateChangeDirection) of object;
  TOnOneSensorStateChanged = procedure(SensorType, ID: Integer; State: Boolean) of object;

  TOnDonnEnvelopeReserved = procedure(Rail, Takt, Amplify: Byte; Delay: Single)
    of object;

  TOnFullCoordRecieved = procedure(ID, Coord: Integer) of object;

  TAirBrushDone = procedure(CAN_ID: Byte; Coord, ErrCode: integer; Delta: Word) of object;

  // TMCallBack = procedure( MBuff: PMBuff ) of object;

  // ��� ���������� (��������� � ����� DF)
  TDeviceType = (dtAKP, dtBUI, dtBUM);

  // ��������� ��� �������� ���������� � �������� ���
  TBUMFirmwareVerInfo = record
    SN: Integer;
    Day: Byte;
    Month: Byte;
    Year: Byte;
    Hour: Byte;
    Minute: Byte;
    Sec: Byte;
    DeviceType: TDeviceType;
    FirmwareVers: Single;
    FirmwareVersB1: Byte;
    FirmwareVersB2: Byte;
  end;

  T4Bytes = packed array [0 .. 3] of Byte;
  T2Bytes = packed array [0 .. 1] of Byte;
  P4Bytes = ^T4Bytes;
  P2Bytes = ^T2Bytes;

  // �������� CAN: ������ ������ � ������ ������.
  TCAN = class
  private
    KT: Byte;
    WrLog: Log_proc;
    UseLog: Boolean; // ���. ���?

    TestResult: Word;

    AVInBuff: PAVBuff; // �� -> �����
    AVOutBuff: PAVBuff; // ����� -> ������������

    // MBuff: TMBuff;
    // MCallBack: TMCallBack;
    // MReq: Boolean;

    isASDOn: Boolean;
    HandKanal: Integer;

    WinMessage: WinMessage_proc;

    FID: Byte;  // ������������� CAN ������. �������� � ����������� ������������ ���.
    BUMIDX:Byte; // ������������� ������� �����. � ������� ����������� ����� ��������.
    FUSB: TDevice; // ������ �� ���������� USB (��� �� ���� ��������� � ������ �������� CAN'�)
    FBUMReady: Boolean;
    // FTimer: TTimer;
    FLastTestTime: Int64;

    FCount3F: Integer;
    FSummaryWay: Integer;

    // FBScanRegistration: Boolean;
    // FBScanRegDelay: Integer;

    StartStopFlag: Byte;

    CS: TCriticalSection;
    CS_BV: TCriticalSection;

    FFirmwareInfo: TBUMFirmwareVerInfo;

    FStatusIsSet: Boolean;

    FImitDPState: Boolean;

    FBScanStopped: Boolean;

//    procedure SetStatus;

    procedure OnBlockReserved(Bl: TBlock);
    procedure OnSendError;

    function SetPar(Takt, ParOfs, ParV: Byte): Boolean;
    // ���������� �������� ��������� �����
    // procedure OnTimer(Sender: TObject);
    function GetBUM_SN: Integer;
    procedure ParceDF(Bl: TBlock);
  protected
  public
    // Device: TDevice;

    OnSensorStateChanged: TOnSensorStateChanged;
    OnOneSensorStateChanged: TOnOneSensorStateChanged;
    OnDonnEnvelopeReserved: TOnDonnEnvelopeReserved;
    OnAirBrushDone: TAirBrushDone;

    SendError: Boolean;

    FlIsInCriticalSection: Boolean;//23.12.2015

    // ASD: TASD;
    AK: TAK;

    // ��������� B-scan.
    BVBuff: TBVBuff;
    BVWritePos: Integer; // ��������� �� ������� ������������ ������ ������ B-���� ( �.�. ��������� ���������� ������, ������� ��� ��������: BVWriteBuff-1 )
    BVReadPos: Integer; // ��������� �� ������, ��� �� ���������� ������ ������ B-���� ( �.�. ���������� ���� �������� � BVReadPos )

    MView: array [0 .. 1, 0 .. 9, 0 .. 1] of // ����, ����, � �����.
    record
      StoreTick: Cardinal;
      Block: TBlock;
    end;

  CanError : Boolean;

  // ������ ����� Master/Slave - 0/1. - 1 - ������ �� �����
  BlockStatus : Integer;

  FullCoord : Integer;
  LastFullCoord : Integer;
  PainterIsIdle : Boolean;
  SensorState: Boolean;
  SensorStateInt: Integer;
  SensorType: integer;

  OnFullCoordRecieved : TOnFullCoordRecieved;

  FLastPainterMarkTime : Int64;

  // ��� ��������� ��������
  VelMesStartTime : Int64;
  VelMesCurDx : Integer;

  BUMCoord: Integer;

  constructor Create(USB: TDevice; ID: Byte; WrLog_: Log_proc;
    WinMessage_: WinMessage_proc; UseLog_: Boolean = false);
  destructor Destroy; override;

  function BVBuffCount: Integer;
  function Test: Word;
  procedure SendTestBlock;

  // �����-�������� �������.
  function SendBL(Bl: TBl): Boolean; overload; // ������� �������
  function SendBL(B1: Byte; B2: Byte = 0; B3: Byte = 0; B4: Byte = 0;
    B5: Byte = 0; B6: Byte = 0; B7: Byte = 0; B8: Byte = 0; B9: Byte = 0;
    B10: Byte = 0): Boolean; overload; // ������� �������
  function SendZero: Boolean; // ������� ������-��-�������� �������.

  // ��������� ������ �� �������.
  function GetAV(var AV: PAVBuff): Boolean;
  function GetBV(var BV: TBVBuff): Integer; overload;
  function GetBV(var BV: TBVBuff; Count: Integer): Integer; overload;
  // �������� �-���������.
  function GetBVCount: Integer;
  function BVCountToDelta(Count: Integer): Integer;
  function DeltaToBVCount(Delta: Integer): Integer;
  // procedure GetMV( CallBack: TMCallBack ); // �������� M-���������.
  procedure GetMV(MV: PMVBuffer); // �������� M-���������.
  // function GetASD( var OutASD: TASD; var OutAK: TAK ): Boolean;

  // ���������������� ���������� �����.
  function InitCikl(KT_: Byte): Boolean; // ������������ ����
  function SetTakt(T, GenL, GenR, ResL, ResR, Tmn, Tmax: Byte; WSA: Word;
    ZondAmpl: Byte): Boolean;
  function SetVRU(N, Takt, T1, A1, T2, A2: Byte): Boolean; // ���
  function SetVRU_Point(N, Takt, T, A: Byte): Boolean; // ���
  function SetStrob(N, Takt, Nstr, T1, T2, A: Byte; TwoEcho: Boolean): Boolean;
  // ��������� �������
  function Set2TP(N, Takt: Byte; l_2TP, r_2TP: Single): Boolean; // ����� � ������

  // ���������� ����������� �, B.
  function StartAview(N, Takt, Tst, M: Byte; WorkStrob: Byte;
    Single: Boolean = false; OnlyMax: Boolean = false): Boolean;
  // ��������� A-���������
  function StopAview: Boolean; // ���������� A-���������

  // ���-���� ������� �����.
  function Start: Boolean; // ��������� ������ ����
  function Stop: Boolean; // ���������� ������ ����
  function Reset(Wait: Boolean = True): Boolean; // ����������� ������ ����
  function ImitDP(isOn: Boolean): Boolean; // ���./����. ����. ������� ����
  // function ASDTurnOn( HandKanal_: Integer ): Boolean;                           // ���. ���
  procedure ASDTurnOff; // ����. ���
  procedure SelectHandChannel(Ch: Byte);
  function ASDTurnOn: Boolean;
  procedure SetID(ID: Byte);

  property ID: Byte read FID;
  property BUMReady: Boolean read FBUMReady;
  property BUM_SN: Integer read GetBUM_SN;

  property Count3F: Integer read FCount3F;
  property SummaryWay: Integer read FSummaryWay;

  property FirmwareInfo: TBUMFirmwareVerInfo read FFirmwareInfo;
  property BScanStopped: Boolean read FBScanStopped;

  procedure ResetWayCounters;

  procedure RunKraskopult;

  // ��������� �������.
  function InitEnvelope(Takt, StrobNum: Byte): Boolean;

  // ��� ����-�������� ��������, ������������ ����
  // ��������: 1 - ����� ��������� ������������
  // 2 - ������
  procedure SetReleMode(Mode: Byte);

  procedure SetStatus;
  function SetBlockAsMaster(Huge2TP: Boolean): Boolean;
  function SetBlockAsSlave(Huge2TP: Boolean): Boolean;

  // ������� ��� ������ � �������������
  function PainterOn(Delta: WORD): Boolean;
  function PainterOff: Boolean;
  function PainterSetCoord(Coord: Integer): Boolean;
  function PainterSetMark(Coord: Integer): Boolean;

  function GetVelocity: Single;
  function AskMasterSlaveStatus: Boolean; //������ �/� ������� ���-�
end;

TCanList = class(TList)
  protected
    function Get(Index: Integer): TCAN;
    procedure Put(Index: Integer; Item: TCAN);
    procedure Move(CurIndex, NewIndex: Integer);
  public
    property Items[Index: Integer]: TCAN read Get write Put;
    default;
  end;

  TFloatPoint = record X: Single;
  Y: Single;
  end;

  TVRCHCurve = record Curve: array [0 .. 231] of TFloatPoint;
  Count: Byte;
  end;

  // ��������� ����� ����� ���
  TVRCHShelf = record ID: string;
  DependsKy: Boolean; // ������� �� ������� ����� �� �� ��� ������� ����� ����� ���������
  Level: Integer; // ������� ����� ���. ���� DependsKy = true �� �������
  // �������� ������������ �� � ���������� � ���������� ��
  // ���� DependsKy = false �� ������� �� ������� �� ��
  St: Integer; // ������ ����� ��� � ���
  Ed: Integer; // ����� ����� ��� � ���

  end;

  // ������������ ���-�� ����� ��� ������� ������ 232 ������ �� ���� ���
  // ����������� ������ ����� ����� - 1 ���, � ��� �������� �-��������� = 10 (������ ����� � �� ������)
  // ������������ �-��������� ���������� 232 ���
  TVRCHShelfMas = array [0 .. 231] of TVRCHShelf;

  TVRCHEngine = class private FCurve: TVRCHCurve; // ������. �� �� ��� �������� � ��� � ����������� ������� �������� ������
  FK: Single; // ����������� �������� ���. ��� ��������� ���� ���������� ��� ������������
  FVRCHTime: Integer; // ����� �� �������� ���� ��������.
  FKanalATT: Integer; // ��������� ������������ � �������������� ������
  FShelfCount: Integer; // ���-�� ����� ���
  FShelfMas: TVRCHShelfMas; // ������ ����� ���
  procedure CalculateCurve; // ������ ������ ����� ���. �.�. ��������� ����. �������� ���������� ��� ������������ �� � ������ ������ ����� ������ ���� ��� ��� �������� �������
protected
  procedure SetVRCHTime(Value: Integer);
  procedure SetKanalATT(Value: Integer);
  function GetShelfByIdx(Index: Integer): TVRCHShelf;
  function GetShelfByID(ID: string): TVRCHShelf;
  procedure SetShelfByID(ID: string; Value: TVRCHShelf);
public
  VRCH: TVRCH; // ����������� ������ ���, ��� ����� ���������� � ���
  constructor Create;
  destructor Destroy;
  override;
  procedure CalculateVRCH; // ���������� ������ ���� ����� ���, ������� ������ ����� � ��� �����.
  // ��������� ����� ��������� ��� ��� ���� ����������. � �������� � Takt.InstallVRU
  procedure AddShelf(ID: string; Level: Integer; St, Ed: Integer;
    DependsKy: Boolean);
  procedure Clear;
  property Time: Integer read FVRCHTime write SetVRCHTime;
  property ATTLevel: Integer read FKanalATT write SetKanalATT;
  property ShelfsCount: Integer read FShelfCount;
  property Shelfs[Index: Integer]: TVRCHShelf read GetShelfByIdx;
  property ShelfByID[ID: string]
    : TVRCHShelf read GetShelfByID write SetShelfByID;
  end;

  TTakt = class;

  TDirection = (dForward = 0, dBack = 1, dNormal = 2, dNone = 3);

  TAScanLength = record Mks, MM_H, MM_R: Integer;
  end;
  // TAdjRailTypeMode = ( amNone = 0, am0 = 1, am45 = 2 ); // ����� ��������� �� ��� ������.

  // ������ ������: ���� ������ ������������� ������ ������.
  TKanal = class
  private FATT: Byte; // 0..80 �� (��)
    FVRCH: Byte; // ��� (���)
    FTwoTP: Single; // (���)
    FASD: Boolean;
    FASDHaveRead: Boolean;
    FSound: Boolean;
  protected
    procedure SetATT(ATT: Byte);
    procedure SetVRCH(VRCH: Byte);
    function GetVRCH: Byte;
    procedure SetTwoTP(TwoTP: Single);
    function GetTaktLength: TAScanLength;
    function GetASD: Boolean;
    procedure SetASD(B: Boolean);

    procedure SetSound(B: Boolean);

  public
    // ����������� �� ����������������� ������� ( + ���� �������� ���� �� ������)
    Ky: Integer; // �������� ���������������� (��)
    Str_st: Byte; // ������� ����� (���)
    Str_en: Byte;
    NR_Start: Byte; // NoiseReduction: �������������� - ������ �������.
    NR_Length: Byte; // NoiseReduction: �������������� - ����� �������.
    NR_Level: ShortInt; // NoiseReduction: �������������� - ������� ������� ������������ ATT.
    AK_Level: Byte; // ������� �������� ��.

    // ����������� �� ���������� �������
    Reg: Byte; // �����: 0 - ��������������, 1 - ������.
    BUM: Byte;
    Nit: Byte; // ���� ������ (�, ��������������, �����)
    Num: Integer; // ����� ������ (��������� � �������� ������� ������� � �������)
    TktN: Byte; // ����� �����. ���� ����� ���������� ���: Takts[Nit, TktNum] (���� Reg=0) ��� HTakts[TktNum] (���� Reg=1)
    Takt: TTakt; // ������ �� ����.
    Method: string; // ����� ��������
    Str_st_r: Integer; // ������������� ������� ����� (��� ��)
    Str_en_r: Integer;
    Ky_r: Integer; // ������������� �������� ���������������� (A-view)
    AdjRailType: Boolean; // ����������� �� ����� �� ��� ������.

    WaveDirection: TDirection; // ����������� ����� ��.
    BViewDelayMin: Integer; // ������ �-��������� (���)
    BViewDelayMax: Integer; // ����� �-��������� (���)
    KanAK: Integer; // ����� ������, �� �������� ����� �������� �� (�� ��������� � Num ��� ���������� � ��. ��������� �������)
    InverseASD: Boolean; // ���������, ��� ���� ������������� ������ ��� (������������ ��� ������� ������)

    IsActive: Boolean; // ������� ���������� ������.

    // �������������� (�� �����������)
    NStr_st: Byte; // ����������� ����� (��� ��)
    NStr_en: Byte;
    NStr2TP_st: Byte; // ����������� ����� ��� 2 TP.
    NStr2TP_en: Byte;
    AdjDate: TDateTime; // ���� ��������� ���������
    TwoEcho: Boolean; // ����� ������ 2 ���.

    // ������ ������ ��� ���������� � ���� ������������� �������� ����� �� ���������
    // � ����������� �������� � ����� �����������
    AdjustChanges: TAdjustChanges;

    ConnectedStrobSt: TKanal;
    ConnectedStrobEd: TKanal;

    StrobStartMin, StrobStartMax: Integer;
    StrobEndMin, StrobEndMax: Integer;

    constructor Create;

    property SoundOn: Boolean read FSound write SetSound; // ���� ��� ������� (������������ � ������ �������� ����)
    property ATT: Byte read FATT write SetATT; // (��� �������� ����������� �� ����������������� �������)
    property VRCH: Byte read GetVRCH write SetVRCH;
    property TwoTP: Single read FTwoTP write SetTwoTP;

    function AdjustKy(N: Byte): Boolean;

    function SetConfig_KA(S: string): Boolean;
    // ����. ��������� ������ (�� ������)
    function SetConfig_KAN(S: string): Boolean; // ����. ���������������� ������ (�� ������)
    function GetConfig_KAN: string; // ������ (��� ����������) ���������������� ������ (� ���� ������)
    procedure InitNStr;
    procedure StartAview;

    property AScanLength: TAScanLength read GetTaktLength; // ������������ �-��������� (����� ����������� �� ������� ��� ����� �� �����)
    property ASD: Boolean read GetASD write SetASD; // "�����" ���� ���. ������������ � False ������ ����� ������.
  end;

  TSingleKanArray = array of TKanal;
  PSingleKanArray = ^TSingleKanArray;

  TKanArray = array [0 .. 1] of TSingleKanArray;
  PKanArray = ^TKanArray;

  TEdgeOfRail = (waNone = 0, waWorking = 1, waNotWorking = 2);

  // ������ �����: ���� ������ ������������� ����� ������ � ������� ������.
  TTakt = class private
  // VRCHCurve        : TVRCHCurve;          // ������ ��� (������ ������ �����: T <= VRCH)
  // _VRCHCurveA_     : Single;    // ����. ������� ���.
  FRealInstNum: integer;
    procedure GetNStrob(out St, En: Byte);
  // procedure UpdateVRCH(A: Single);
  procedure StartUpVRCH;
protected
  function GetAViewTimeMax: Integer;

public
  // ����������� �� ���������� �������
  Reg: Byte; // �����: 0 - ��������������, 1 - ������.
  BUM: Byte; // ����� ������� �����
  Num: Integer; // ����� ����� (��������� � �������� ������� ������� � �������)
  InstallNum: Integer; // ����� ����� ��� �������� ��� � ������ ���� (��������� � Num, ����� �������)
  ConfigNit: Integer; // ����, ���������� �� ������� ������ (�������� ���� �������� � N_res)

  SinhTakt: TTakt;
  SinhTaktNumber: Integer;

  Gen, Res: Byte; // 0..7 (��� ��)
  Gen_pep, Res_pep: string; // �������� ��� (����. 2.2) (A-view)
  // Method           : string;    // ����� (����, ���, ���, ����, ������)
  Alfa: Integer; // ���� ����� ���.
  Ten: Byte; // ������������ �����.
  Kan1: ShortInt; // ����� ������� (������) ������
  Kan2: ShortInt; // ����� ������� (�������) ������  ( -1 - ��� )
  Kanal1: TKanal; // �������� �� ������ (�����) �����
  Kanal2: TKanal; // �������� �� ������ (�� ���� ������) �����
  BStr_st: Byte; // ����� B-��������� (��� ��)
  BStr_en: Byte;
  AV_mash: Byte; // ������� �-����: ����� ����� ����� ��������� ��������� �-���� � ������� ����� ����������� (*0.1���)
  AKStr_st: Byte;
  AKStr_en: Byte;
  DelayFactor: Byte; // ��������� �������� (��� ������� ������ = 3, ����� 1)

  Edge: TEdgeOfRail;
  Gamma: Integer; // ���� ��������� ��� ������������ ���������� ��� ������
  // �������������� (�� �����������)
  N_res: Byte; // �������� ����� ���� (�������������� � FinishConfig)
  ZonaM: Word; // ��������� ���� �����
  UseZM: Boolean;

  VRCH: TVRCHEngine; // ������ ��� ������� � �������� ���
  // VRCH             : TVRCH;     // ����� ��� (���)

  CANObj: TCAN;
  constructor Create;
  property AViewTimeMax: Integer read GetAViewTimeMax;

  function GetWSA(KT: Byte; InstNum: Byte): Word;
  function SetConfig(S: string): Boolean; // ����. ����. ����� (S - ������ ����� RSP.TK)
  function FinishConfig: Boolean;
  function Install( { CAN : TCAN; } SReg: Byte): Boolean;
  // �������� ������ ��� ������ �������� ����!
  function Install2TP { (CAN : TCAN) } : Boolean;
  // ���������� ������ ��� �������� ����.
  function InstallVRU( { CAN : TCAN; } SReg: Byte): Boolean;
  // ���������� ��� ������ ����.
  function InstallStrob(
    { CAN : TCAN; } SReg: Byte; Adj2TPMode: Boolean = false): Boolean;
  // ���������� ��� ������ ����.
  function InstallEnvelop: Boolean;
  procedure SetRegMode(Reg: integer);
  end;

  TSingleTaktArray = array of TTakt;
  PSingleTaktArray = ^TSingleTaktArray;
{$IFDEF BUMCOUNT4}
  TTaktArray = array [0 .. 3, 0 .. 1] of TSingleTaktArray;
{$ELSE}
  TTaktArray = array [0 .. 1, 0 .. 1] of TSingleTaktArray;
{$ENDIF}
  // [��� �, ���� � ���]
  PTaktArray = ^TTaktArray;

  TKanConfig = class
  private
    CurrentUserConfig: Integer;
    UserConfigList: TStringList;
    CodePos: Integer;
    CodeMode: Integer;
    FUserConfigLoaded: Boolean;
    FConfigOK: Boolean;

    function Code_KAN(Snum: Integer; var S: string): Boolean;
    function Decode_KAN(Snum: Integer; S: string): Boolean;
    function Decode_KA(Snum: Integer; S: string): Boolean;
    function Decode_TK(Snum: Integer; S: string): Boolean;
  public
    _Takts: PTaktArray;
    _Kanals: PKanArray;
    _HTakts: PSingleTaktArray;
    _HKanals: PSingleKanArray;

    DoNotSaveUserConfig: Boolean;

    constructor Create;
    function LoadSystemConfig: Boolean;
    function LoadUserConfig(ConfigID: Integer): Boolean;
    function SaveUserConfig(ConfigID: Integer): Boolean;
    function AddNewConfig(Name: string): Boolean;
    procedure InitNStr;
    function FinishConfig: Boolean;
    procedure ClearOldNastr;
    property ConfigList: TStringList read UserConfigList;
  end;

var
  AllOff: Boolean;
  KConf: TKanConfig;
  Takts: TTaktArray;
  Kanals: TKanArray;
  HTakts: TSingleTaktArray;
  HKanals: TSingleKanArray;

implementation

uses Windows, Dialogs, SysUtils, Math;

// -----------------------------------------------------------------------------
// --------------------  ������ ������������ ������� � ������  -----------------

constructor TKanConfig.Create;
var
  F: TSearchRec;
  ii: Integer;
  jj: Integer;
  kk: Integer;
begin
  FUserConfigLoaded := false;
  DoNotSaveUserConfig := false;

  _Takts := @Takts;
  _Kanals := @Kanals;
  _HTakts := @HTakts;
  _HKanals := @HKanals;

  SetLength(_Takts^[0, 0], 0);
  SetLength(_Takts^[0, 1], 0);
  SetLength(_Takts^[1, 0], 0);
  SetLength(_Takts^[1, 1], 0);
  SetLength(_Kanals^[0], 0);
  SetLength(_Kanals^[1], 0);
  SetLength(_HTakts^, 0);
  SetLength(_HKanals^, 0);

  UserConfigList := TStringList.Create;

  chdir(ExtractFilePath(Application.ExeName));
  chdir('NASTR');

{$IFDEF Avikon14}
  UserConfigList.Add('Av14.KAN');
{$ENDIF}
{$IFDEF Avikon14_2Block}
  UserConfigList.Add('Av14_2B.KAN');
{$ENDIF}
{$IFDEF Avikon11}
  UserConfigList.Add('Av11.KAN');
{$ENDIF}
{$IFDEF Avikon12}
  UserConfigList.Add('Av12.KAN');
{$ENDIF}
{$IFDEF Avikon15RSP}
  UserConfigList.Add('Av15RSP.KAN');
{$ENDIF}
{$IFDEF Avikon16_IPV}
  case Config.SoundedScheme of
    Scheme1:
      UserConfigList.Add('AV16.KAN');
    Scheme2:
      UserConfigList.Add('AV16.KAN');
    Scheme3:
      UserConfigList.Add('AV16.KAN');
    Wheels:
      UserConfigList.Add('AV16W.KAN');
  end;
{$ENDIF}
{$IFDEF EGO_USW}
  UserConfigList.Add('AV16W.KAN');
{$ENDIF}
{$IFDEF USK004R}
  UserConfigList.Add('AV16.KAN');
{$ENDIF}
{$IFDEF RKS_U}
  UserConfigList.Add('rks_u_5.KAN');
{$ENDIF}
{$IFDEF VMT_US}
  UserConfigList.Add('AV16.KAN');
{$ENDIF}


  CurrentUserConfig := 0;
end;

function TKanConfig.LoadUserConfig(ConfigID: Integer): Boolean;
var
  FN, FNBackUp: string;
  Code: Byte;
  N, i: Integer;
  F: TSearchRec;
  S: string;

  sr: TSearchRec;
  FileAttrs: Integer;

  FileNameBegining: string;
  FindRes: Integer; // ���������� ��� ������ ���������� ������
  Num, MaxNum: integer;
  NumStr: string;
  MS: TMemoryStream;
begin
  Result := false;
  CurrentUserConfig := ConfigID;
  // ������� ������ ����� � �����������
  system.chdir(ExtractFilePath(Application.ExeName));
  system.chdir('NASTR');

  // ���� �� ������ ����� ��������� �� ������� �� ������
  if CurrentUserConfig > UserConfigList.Count - 1 then
    CurrentUserConfig := -1;
  if CurrentUserConfig = -1 then
    Exit;

  // ������� ��� �� ��������� ������ ���������
  system.chdir(Config.NastrFolder);
  case Config.AdjustmentVariantNum of
    0:
      FileNameBegining := 'Var1';
    1:
      FileNameBegining := 'Var2';
    2:
      FileNameBegining := 'Var3';
  end;

  if ProgramProfile.DeviceFamily = 'RKS-U' then
    FileNameBegining := 'Var1';

  // ��������� ������������ �����
  MaxNum:=0;
  FindRes := FindFirst(FileNameBegining+'_*.kan', faAnyFile, SR);
  while FindRes = 0 do
  begin
    if ( SR.Name <> '.' ) and ( SR.Name <> '..' ) then
      begin
        NumStr:= '';
        for i:= 6 to Length(SR.Name)-4 do
          if (SR.Name[i]>='0') and (SR.Name[i]<='9') then
              NumStr:= NumStr + SR.Name[i];
          try
            Num:= StrToInt(NumStr);
          except
          end;
      end;
      if Num > MaxNum then
        MaxNum:= Num;
    FindRes := FindNext(SR);
  end;
  FindClose(SR);

  FNBackUp:= Format('%s_%d.kan', [FileNameBegining, MaxNum]);

  if not FileExists(FNBackUp) then
    FNBackUp:= FileNameBegining+'.kan';

  FN:= Format('%s_%d.kan', [FileNameBegining, MaxNum+1]);

  MS:= TMemoryStream.Create;
  MS.LoadFromFile(FNBackUp);
  MS.SaveToFile(FN);
  MS.Free;

  if not FileExists(FN) then
    FN:= FileNameBegining+'.kan';

  Code := LoadTextFile(FN, Decode_KAN);
  if Code <> 0 then
  begin
    WrLog(DecodeLoadError(FN, Code));
    Exit;
  end;

  FUserConfigLoaded := Code = 0;
  Result := Code = 0;
end;

function TKanConfig.FinishConfig: Boolean;
var
  N, i, j, k: Integer;
  SMax, SMin, EMax, EMin: Integer;
begin
  Result := false;

  // ������ ����� � ������.

{$IFDEF BUMCOUNT4}
  for k := 0 to 3 do
{$ELSE}
  for k := 0 to 1 do
{$ENDIF}
    for N := 0 to 1 do
      for i := 0 to High(_Takts^[k, N]) do
        if _Takts^[k, N, i] <> nil then
          if not _Takts^[k, N, i].FinishConfig then
            Exit;

  for i := 0 to High(_HTakts^) do
    if not _HTakts^[i].FinishConfig then
      Exit;

  // ������� ����������� ������.
  InitNStr;
{$IFDEF TrueAV14}
  for i := 0 to 1 do
    for j := 0 to High(Kanals[i]) do
    begin
      case Kanals[i, j].Num of
        0, 10:
          begin
            SMin := 40;
            SMax := 65;
            EMin := 66;
            EMax := 70;
          end;
        1, 11:
          begin
            SMin := 2;
            SMax := 10;
            EMin := 40;
            EMax := 65;
          end;
        2, 3, 12, 13:
          begin
            SMin := 20;
            SMax := 30;
            EMin := 90;
            EMax := 140;
          end;
        4, 5:
          begin
            SMin := 2;
            SMax := 10;
            EMin := 60;
            EMax := 100;
          end;
        6, 7:
          begin
            SMin := 4;
            SMax := 10;
            EMin := -1;
            EMax := -1;
          end;
        8, 9:
          begin
            SMin := -1;
            SMax := -1;
            EMin := 160;
            EMax := 183;
          end;
      end;
      Kanals[i, j].StrobStartMin := SMin;
      Kanals[i, j].StrobStartMax := SMax;
      Kanals[i, j].StrobEndMin := EMin;
      Kanals[i, j].StrobEndMax := EMax;
    end;
{$ELSE}
  for i := 0 to 1 do
    for j := 0 to High(Kanals[i]) do
    begin
      Kanals[i, j].StrobStartMin := 0;
      Kanals[i, j].StrobStartMax := 200;
      Kanals[i, j].StrobEndMin := 0;
      Kanals[i, j].StrobEndMax := 200;
    end;
{$ENDIF}
  Result := True;
end;

function TKanConfig.SaveUserConfig(ConfigID: Integer): Boolean;
var
  FileName, BakName1, BakName2, BakName3, FileNameBegining: string;
  MS: TMemoryStream;
  i, j: Integer;
  Flag: Boolean;

  SR: TSearchRec; // ��������� ����������
  FindRes: Integer; // ���������� ��� ������ ���������� ������
  Num, MaxNum: integer;
  NumStr: string;

begin
  if DoNotSaveUserConfig then
  begin
    Result := True;
    Exit;
  end;
  Result := false;
  CurrentUserConfig := ConfigID;
  if CurrentUserConfig = -1 then
  begin
    Result := True;
    Exit;

  end;

  // ������ �� ������ ����� �� ����������/
  // ��������� ��� ������ � ���� �� ���� ������� �� ����� ����,
  // �� ������� ������ ������ (FConfigOK = false)
  FConfigOK := True;
  for i := 0 to 1 do
    for j := 0 to High(_Kanals^[i]) do
      FConfigOK := FConfigOK and (_Kanals^[i, j].Ky = 0);
  FConfigOK := not FConfigOK;

  if (not FUserConfigLoaded) or (not FConfigOK) then
    Exit;
  chdir(ExtractFilePath(Application.ExeName));
  chdir('NASTR');

  CodeMode := 0;
  CodePos := 0;

  chdir(Config.NastrFolder);

  // ����� ��������� ���������� ��������

  // ���������� ����� ��� ���� ������� ������ �����
  case Config.AdjustmentVariantNum of
    0:
      FileNameBegining:= 'Var1';
    1:
      FileNameBegining:= 'Var2';
    2:
      FileNameBegining:= 'Var3';
  end;

  // ��������� ������������ �����
  MaxNum:=0;
  FindRes := FindFirst(FileNameBegining+'_*.kan', faAnyFile, SR);
  while FindRes = 0 do
  begin
    if ( SR.Name <> '.' ) and ( SR.Name <> '..' ) then
      begin
        NumStr:= '';
        for i:= 6 to Length(SR.Name)-4 do
          if (SR.Name[i]>='0') and  (SR.Name[i]<='9') then
              NumStr:= NumStr + SR.Name[i];
          try
            Num:= StrToInt(NumStr);
          except
          end;
      end;
      if Num > MaxNum then
        MaxNum:= Num;
    FindRes := FindNext(SR);
  end;
  FindClose(SR);

  Inc(MaxNum);

  FileName := Format('%s_%d.kan', [FileNameBegining, MaxNum]);

  SaveTextFile(FileName, Code_KAN);

  sleep(200);
  Result := True;
end;

function TKanConfig.LoadSystemConfig: Boolean;

var
  Code: Byte;
  FN: string;
  TK_FN: string;
  KA_FN: string;

begin
  Result := false;
  FN := Config.SystemFileName;

  TK_FN := FN + '.tk';
  KA_FN := FN + '.ka';

  chdir(ExtractFilePath(Application.ExeName));
{$I-}
  chdir('SYSTEM');
  if IOResult <> 0 then
    Exit;
  Code := LoadTextFile(TK_FN, Decode_TK);
  if Code <> 0 then
  begin
    WrLog(DecodeLoadError(TK_FN, Code));
    Exit;
  end;
  Code := LoadTextFile(KA_FN, Decode_KA);
  if Code <> 0 then
  begin
    WrLog(DecodeLoadError(KA_FN, Code));
    Exit;
  end;

  Result := True;
end;

procedure TKanConfig.ClearOldNastr;
const
    MaxFilesCount = 100;
var
  SR: TSearchRec; // ��������� ����������
  FindRes, i, j, Num, MaxNum, MaxIdx: Integer; // ���������� ��� ������ ���������� ������
  FilesCount: integer;
  FileNameBegining: string;
  FileTable :array of
  record
     FileName: string;
     Num: integer;
  end;
  NumStr, CurStr: string;
  CurNum: integer;
  //SL: TStringList;
begin
  // ����� �������� � ����� � �����������
  chdir(ExtractFilePath(Application.ExeName));
  chdir('NASTR');
  chdir(Config.NastrFolder);
  FilesCount:= 0;

  case Config.AdjustmentVariantNum of
    0:
      FileNameBegining:= 'Var1';
    1:
      FileNameBegining:= 'Var2';
    2:
      FileNameBegining:= 'Var3';
  end;

  // ������������ ������ ���� ������� �������� ������ � ��� ����� � �����
  // ����������� ������� ������ �����
  // ����� ������� �� ������ �������� ���������� ������� ������ ������������
  FindRes := FindFirst(FileNameBegining+'_*.kan', faAnyFile, SR);
  //SL:= TStringList.Create;
  SetLength(FileTable, 0);
  while FindRes = 0 do
  begin
    if ( SR.Name <> '.' ) and ( SR.Name <> '..' ) then
      begin
        if SR.Size<>0 then
          begin
            inc(FilesCount);
            SetLength(FileTable, FilesCount);
            FileTable[FilesCount-1].FileName:= SR.Name;
            NumStr:= '';
            for i:= 6 to Length(SR.Name)-4 do
              if (SR.Name[i]>='0') and  (SR.Name[i]<='9') then
                NumStr:= NumStr + SR.Name[i];
            try
              Num:= StrToInt(NumStr);
            except
            end;
            FileTable[FilesCount-1].Num:= Num;
          end
        else
            DeleteFile(SR.Name);
      end;
    FindRes := FindNext(SR);
  end;
  FindClose(SR);

  // ��������� �������, ����� �� ������� ������ ��������� (�� ���������� ������)
  if FilesCount > MaxFilesCount then
    begin
      // ������ ������� �����, ���� �������.
      // ��� ���� ����� ������� ������ ������ �����, ������� ������� � ������� �������
      // ��� ����� ������ � ���� �� ��������. ����� ��������� ������ �� ����������� �� ������ ����� � ������� �������� ����� ������ �������
      //SL.Sort;

      // ����������
      MaxNum:= 0;
      MaxIdx:= 0;
      for i:= 0 to High(FileTable)-1 do
        for j:= 0 to High(FileTable)-1 do
           begin
             if FileTable[j].Num > FileTable[j+1].Num then
               begin
                 CurNum:= FileTable[j].Num;
                 CurStr:= FileTable[j].FileName;
                 FileTable[j].Num:= FileTable[j+1].Num;
                 FileTable[j].FileName:= FileTable[j+1].FileName;
                 FileTable[j+1].Num:= CurNum;
                 FileTable[j+1].FileName:= CurStr;
               end;
           end;

      // ������� ��������� ������ ������
      for i:= 0 to FilesCount-MaxFilesCount-1 do
        begin
          DeleteFile(FileTable[i].FileName);
        end;

      //DeleteFile(FileTable[i].FileName);
    end;
  SetLength(FileTable, 0);
  //SL.Free;
end;

function TKanConfig.Code_KAN(Snum: Integer; var S: string): Boolean;
var
  k: TKanal;
  Pos: Integer;

  procedure Code(_KanArr: TSingleKanArray);
  begin
    k := nil;
    while CodePos <= High(_KanArr) do
    begin
      k := _KanArr[CodePos];
      Inc(CodePos);
      if k <> nil then
        Break;
    end;
    if k = nil then
    begin
      Inc(CodeMode);
      CodePos := 0;
    end;
  end;

begin

  Result := false;

  if CodeMode = 0 then
  begin
    S := '�����          ����           �����          ���            2TP            Ky             ATT            ���.���.       ���.���.       ��-������      ��-�����       ��-�������     ��-����        ���� �����.';
    CodeMode := 1;
    CodePos := 0;
    Result := True;
    Exit;
  end;

  if CodeMode = 1 then
    Code(_Kanals^[0]);
  if CodeMode = 2 then
    Code(_Kanals^[1]);
  if CodeMode = 3 then
    Code(_HKanals^);
  if CodeMode = 4 then
    Exit;

  S := k.GetConfig_KAN;
  Result := True;
end;

function TKanConfig.Decode_KAN(Snum: Integer; S: string): Boolean;
var
  N, i: Integer;

begin
  Result := True;
  if (Snum = 0) then
    Exit;

  for N := 0 to 1 do
    for i := 0 to High(_Kanals^[N]) do
      if _Kanals^[N, i] <> nil then
        if _Kanals^[N, i].SetConfig_KAN(S) then
          Exit;

  for i := 0 to High(_HKanals^) do
    if _HKanals^[i] <> nil then
      if _HKanals^[i].SetConfig_KAN(S) then
        Exit;

  Result := false;
end;

function TKanConfig.Decode_KA(Snum: Integer; S: string): Boolean;
var
  k: TKanal;
  KA: PSingleKanArray;
  HighKA: Integer;
  i: Integer;

begin
  Result := True;
  if (Snum = 0) then
    Exit;
  k := TKanal.Create;
  if k.SetConfig_KA(S) then
  begin
    Result := True;
    if k.Reg = 0 then
      KA := @_Kanals^[k.Nit]
    else
      KA := _HKanals;
    HighKA := High(KA^);
    if k.Num > HighKA then
      SetLength(KA^, k.Num + 1);
    for i := HighKA + 1 to High(KA^) do
      KA^[i] := nil; // ������������ nil �������������� (�����������) ������, �� ���� ��� ��������, ���� � ������� ������� ���� ����.
    KA^[k.Num] := k;
  end
  else
  begin
    Result := false;
    k.Free;
  end;
end;
//
function TKanConfig.Decode_TK(Snum: Integer; S: string): Boolean;
var
  T: TTakt;
  TA: PSingleTaktArray;
  HighTA: Integer;
  i: Integer;
  nit: Integer;
  j: Integer;

begin
  Result := True;
  if (Snum = 0) then
    Exit;
  T := TTakt.Create;
  if T.SetConfig(S) then
  begin
    Result := True;
    if T.Reg = 0 then
       begin
           TA := @_Takts^[T.BUM, T.ConfigNit];
// ��� �������� _Takts^[i, j] ����� ����� ���������� ������
// �������������� ����� nil
           if (T.Num > High(_Takts^[0, 0])) then
               begin
{$IFDEF BUMCOUNT4}
                   for i := 0 to 3 do
{$ELSE}
                   for i := 0 to 1 do
{$ENDIF}
                       for nit := 0 to 1 do
                           begin
                               HighTA:= High(_Takts^[i, nit]);
                               SetLength(_Takts^[i, nit], T.Num + 1);
                               for j := HighTA + 1 to T.Num do
                                   _Takts^[i, nit,j]:= nil;
                           end;
               end;
       end
    else
        begin
            TA := _HTakts;
            if (T.Num > High(_HTakts^)) then
               begin
                   HighTA:= High(_HTakts^);
                   SetLength(_HTakts^, T.Num + 1);
                   for j := HighTA + 1 to T.Num do
                      _HTakts^[j]:= nil;
               end;
        end;
    TA^[T.Num] := T;
  end
  else
  begin
    Result := false;
    T.Free;
  end;
end;
//
procedure TKanConfig.InitNStr;
var
  N, i: Integer;
begin
  for N := 0 to 1 do
    for i := 0 to High(Kanals[N]) do
      if Kanals[N, i] <> nil then
        Kanals[N, i].InitNStr;
  for i := 0 to High(HKanals) do
    if HKanals[i] <> nil then
      HKanals[i].InitNStr;
end;

// -----------------------------------------------------------------------------
// -----------------  ������ ������ � ��������� ��������� CAN  -----------------

function TCAN.SendBL(Bl: TBl): Boolean;
var
  T: LongInt;
  T0: Cardinal;
  F: Boolean;

begin
//  if not FlIsInCriticalSection then CS.Acquire;//AK
  CS.Acquire;
{$IFDEF StartStopProcess}
  T := GetTickCount;
  F := false;
  while StartStopFlag <> 0 do // ����, ���� ����� "�����������".
  begin
    F := True;
    Sleep(0);
    if GetTickCount - T > 5000 then
    begin
      WrLog
        ('(E) Start flag have not reserved in 5000 ms. Stop flag will drop!');
      StartStopFlag := 0;
      Break;
    end;
  end;
{$ENDIF}
  if F then
    WrLog('(i) Wait for start flag in ' + IntToStr(GetTickCount - T) + ' ms.');
{$IFDEF BUMCOUNT1}
  Result := FUSB.PutBlock(Bl);
{$ELSE}
  Result := TDeviceMAKP(FUSB).PutBlock(Bl, BUMIDX{FID});
{$ENDIF}
//  if not FlIsInCriticalSection then CS.Release;//AK
  CS.Release;
end;

function TCAN.SendBL(B1: Byte; B2: Byte = 0; B3: Byte = 0; B4: Byte = 0;
  B5: Byte = 0; B6: Byte = 0; B7: Byte = 0; B8: Byte = 0; B9: Byte = 0;
  B10: Byte = 0): Boolean;
var
  Bl: TBl;
begin
  Bl[1] := B1;
  Bl[2] := B2;
  Bl[3] := B3;
  Bl[4] := B4;
  Bl[5] := B5;
  Bl[6] := B6;
  Bl[7] := B7;
  Bl[8] := B8;
  Bl[9] := B9;
  Bl[10] := B10;
  SendBL := SendBL(Bl);
end;

function TCAN.SendZero: Boolean;
begin
  Result := SendBL(2, 1, 0, 0, 0, 0, 0, 0, 0, 0);
end;

{ function TCAN.GetBL(var Bl: TBl; Fast : Boolean = False): Boolean;
  var
  Bt : Byte;
  B : Boolean;
  Cou,
  i : Byte;
  T : DWord; // TimeOut
  begin
  If AllOff then
  begin
  GetBL:= ( random > 0.5 );
  Exit;
  end;

  i:=0;
  Cou:=10;
  GetBL:=false;
  repeat
  T:=GetTickCount;
  repeat
  B:=COM.GetByte(Bt);
  If not B and Fast then exit;
  If B then break;
  until GetTickCount-T > 2000;
  If not B then exit;
  inc(i);
  Bl[i]:=Bt;
  If i=2 then Cou:=Bt and $F + 2;
  If Cou>10 then
  begin
  If UseLog then
  WrLog( 'BAD BLOCK (cou=' + inttostr(Cou) + ').' );
  exit;
  end;
  until i>=Cou;

  GetBl:=true;
  If UseLog then
  WrLog( 'R: ' + Bl2Str(Bl) );
  end;
  }



// -----------------------------------------------------------------------------
// ----------------------  ������ ������ � ���������� CAN  ---------------------

constructor TCAN.Create(USB: TDevice; ID: Byte; WrLog_: Log_proc;
  WinMessage_: WinMessage_proc; UseLog_: Boolean = false);
var
  R: Byte;

begin
  FlIsInCriticalSection:=False;
  FBScanStopped:= true;
  OnAirBrushDone:= nil;
  FImitDPState := false;

  VelMesStartTime := 0;
  VelMesCurDx := 0;

  SensorState := false;
  FullCoord := 0;
  PainterIsIdle := True;
  FStatusIsSet := false;
  BlockStatus := -1;

  CS := TCriticalSection.Create;
  CS_BV := TCriticalSection.Create;

  FCount3F := 0;
  FSummaryWay := 0;
  StartStopFlag := 0;

  // FBScanRegistration:= True;
  // FBScanRegDelay:= 0;

  FBUMReady := false;
  FUSB := USB;
{$IF DEFINED(BUMCOUNT2) OR DEFINED(BUMCOUNT4)}
  TDeviceMAKP(FUSB).OnBlockReservedMas[ID] := OnBlockReserved;
{$ELSE}
  FUSB.OnBlockReserved := OnBlockReserved;
{$IFEND}
  FID := ID;
  BUMIDX := ID;
  FBUMReady := false;
  // ������.
  // MReq:= False;

  new(AVInBuff);
  new(AVOutBuff);
  isASDOn := false;
  BVReadPos := 0;
  BVWritePos := -1;

  WinMessage := WinMessage_;
  UseLog := UseLog_;
  WrLog := WrLog_;

  { Device:= TDeviceAKP.Create;
    Device.OnBlockReserved:= OnBlockReserved;
    Device.AddLogProc:= WrLog;
    Device.OnSendError:= OnSendError; }

  SendError := false;

  TestResult := 0;

  { FTimer:=TTimer.Create(nil);
    FTimer.Interval:=500;
    FTimer.OnTimer:=OnTimer;
    FTimer.Enabled:=true; }
  // ShowMessage('CAN !!');
  SendBL(FBUMTestBlock);
  FLastTestTime := GetTickCount;
end;

destructor TCAN.Destroy;
begin
  // Stop;
  // FTimer.Free;
  // Device.Free;

  inherited;
  UseLog := false;
  CS.Free;
  CS_BV.Free;
end;

function TCAN.GetAV(var AV: PAVBuff): Boolean;
begin
  { AV:= AVOutBuff;
    Result:= TRUE; Exit; }

  if AVOutBuff.Pos <> 232 then
  begin
    Result := false;
  end
  else
  begin
    AVOutBuff.Pos := 0;
    AV := AVOutBuff;
    Result := True;
  end;
end;

function TCAN.GetBV(var BV: TBVBuff): Integer;
var
  Cou: Integer;

begin
  Result := GetBV(BV, -1);

  {
    if ( BVReadPos = BVWritePos ) or ( BVWritePos = -1 ) then
    begin
    Result:= 0;
    Exit;
    end;

    if BVReadPos > BVWritePos then
    begin
    Cou:= BVBufferCount - BVReadPos;
    Move( BVBuff[BVReadPos], BV[0], Cou * SizeOf( TBVBuffEl ) );
    Move( BVBuff[0], BV[Cou], BVWritePos * SizeOf( TBVBuffEl ) );
    Result:= Cou + BVWritePos;
    end else
    begin
    Cou:= BVWritePos - BVReadPos;
    Move( BVBuff[BVReadPos], BV[0], Cou * SizeOf( TBVBuffEl ) );
    Result:= Cou;
    end;
    BVReadPos:= BVWritePos;
    }
end;

function TCAN.GetBV(var BV: TBVBuff; Count: Integer): Integer;
var
  C: Integer;

begin
  try
    CS_BV.Enter;

    if Count = -1 then
      Count := GetBVCount
    else
      Count := Min(GetBVCount, Count);

    if Count = 0 then
    begin
      CS_BV.Release;
      Result := 0;
      Exit;
    end;

    if BVReadPos + Count > BVBufferCount then
    begin
      // � ���������.
      C := BVBufferCount - BVReadPos; // �����.
      Move(BVBuff[BVReadPos], BV[0], C * SizeOf(TBVBuffEl)); // ������� �����.
      Move(BVBuff[0], BV[C], (Count - C) * SizeOf(TBVBuffEl));
      // � ������ ������.
      BVReadPos := Count - C;
    end
    else
    begin
      // ��� ��������.
      Move(BVBuff[BVReadPos], BV[0], Count * SizeOf(TBVBuffEl));
      BVReadPos := BVReadPos + Count;
      if BVReadPos = BVBufferCount then
        BVReadPos := 0;
    end;

    Result := Count;

    CS_BV.Release;

  except
  end;
end;

{ function TCAN.GetASD( var OutASD: TASD; var OutAK: TAK ): Boolean;
  var
  i: Integer;
  N: Integer;
  begin
  if ASDReady then
  begin
  Result:= True;
  OutASD:= ASD;
  OutAK:= AK;
  for N:=0 to 1 do
  for i:=0 to High( ASD[N] ) do
  ASD[N, i]:= False;
  ASDReady:= False;
  end else Result:= False;
  end;
  }

procedure TCAN.OnSendError;
begin
  SendError := True;
end;

procedure TCAN.OnBlockReserved(Bl: TBlock);
var
  Buff: PAVBuff;
  i, j, N: Integer;
  Cou: Byte;
  T: Byte;
  MainNit: Boolean;
  WriteNit: Byte;
  K1, K2, AK_: Boolean;
  Kan1, Kan2: Integer;
  Kanal1, Kanal2: TKanal;
  R: Byte;
  Delta: Integer;
  HandKanalIdx: Integer;
  BytesMasPtr: P4Bytes;

  Dt: Int64;
  Dx: Integer;
  ADCoord: integer;
  ADDelta: Word;
  TempBV: TBVBuff;
begin

  if not Config.WaitHardware then
    Exit;
  case Bl[1] of
{$IFDEF StartStopProcess}
    $DF:
      ParceDF(Bl);
    $01:
      if Bl[3] in [0, 1] then
      begin
        if Bl[3] = 1 then
          StartStopFlag := 1
        else
          StartStopFlag := 0;
      end;
{$ENDIF}
    $85: // ��������� �������.
      begin
{$IFDEF GlideEnvelope}
        if Assigned(OnDonnEnvelopeReserved) then
          OnDonnEnvelopeReserved(Sign(Bl[3] and $80), Bl[3] and $7F,
            { round(Random * 255) } Bl[4], Bl[5] + Bl[6] / 10);
        // BL[3] - ����, ����.
        // BL[4] - ����.
        // BL[5] - ���. ��������.
        // BL[6] - ��������, ������� ����.
{$ENDIF}
      end;
    $60:
      begin
        // if Assigned( OnSensorStateChanged ) then OnSensorStateChanged( TSensorKind( BL[3] ), TStateChangeDirection( BL[4] shr BL[3] ) );
        SensorType:= Bl[3]; // 0 - ������� �����, 1 - ������ �������
        case SensorType of
            0: begin // ������ ��������� ������� ��������� �����
                 SensorStateInt:= Bl[4] and 1;
               end;
            1: begin // ������ ��������� ������� �������
                 SensorStateInt:= Bl[4] and 2;
               end;
        end;
        SensorState:= SensorStateInt = 1;
        //SensorState := Bl[4] <> 0;
        if Assigned(OnOneSensorStateChanged) then
          OnOneSensorStateChanged(SensorType, Self.ID, SensorState);
      end;
    $DB:
      begin
        TestResult := Bl[4] shl 8 + Bl[3];
        if not FBUMReady then
        begin
          FBUMReady := TestResult <> 0;
{$IFDEF BUMCOUNT4}
//          if TestResult<1610 then
          if FFirmwareInfo.FirmwareVers<16.10 then BlockStatus:=2;

{$ELSE}
          if (not FStatusIsSet) and (FBUMReady) then
            SetStatus;
{$ENDIF}
        end;
      end;
{$IFDEF BUMCOUNT4}
    $19: //Inverted State BUM Master/Slave Pin (0-Master/1-Slave)
      begin
//         BUMMasterSlaveStatus:=Bl[3];
         BlockStatus:=Bl[3];
      end;
{$ENDIF}
    // �-���������.
    $7F:
      begin
        Buff := AVOutBuff;
        AVOutBuff := AVInBuff;
        AVInBuff := Buff;

      end;
    $82:
      begin
        AVInBuff.Amax := Bl[3];
        AVInBuff.Tmax := Bl[4] + Bl[5] * 0.1;
      end;
    $80, $81:
      with AVInBuff^ do
      begin
        if Bl[1] = $80 then
          Pos := 0;
        if Pos >= 232 then
        begin
          Pos := 233; // ���������� ������� ���� (���� ������. ��� ������, ���� ����� ��������).
          WrLog('(E) TOO MANY DATA IN A-VIEW');
        end
        else
        if Pos<0 then begin
          Pos := 233;
          WrLog('(E) NEGATIVE INDEX OF A-SCAN');
        end
        else
        begin
          for j := 0 to 7 do
            AV[Pos + j] := Bl[3 + j];
          Pos := Pos + 8;
        end;
      end;
    $84: // �������� �������� ������.
      with AVInBuff^ do
        if (Config.WorkRail = 0) xor (Bl[6] <> 0) then // ����� �����.
        begin
          if Bl[5] > 5 then
            TmaxK0 := Bl[4] + 1
          else
            TmaxK0 := Bl[4];
          AmaxK0 := Bl[3];
        end;
    // B-���������.
    $3F:
      begin // ������������ ������� ����.
        // ��� ���� �� �������� ������������ � ������� 3C
        CS_BV.Enter;

        // inc( FCount3F );

        if Bl[3] > 127 then
          Delta := Bl[3] - 256
        else
          Delta := Bl[3];

      if Delta<>0 then begin

{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
        if not Config.DirectionOfMotion then Delta:=-Delta;//B-Forward
{$IFEND}
//
{$IFDEF USK004R}
        if Config.WorkSide then Delta:=-Delta;
//WorkSide: Boolean; //False = Gauge Left / True=Gauge Right  (��� ������������ ��������)
{$ENDIF}


//!{$IFnDEF TESTING_COORD}
        FSummaryWay := FSummaryWay + Delta;
//!{$ENDIF}

        // ��� ��������� ��������
        VelMesCurDx := VelMesCurDx + Delta;


        for i := 1 to Abs(Delta) do
        begin
          Inc(BVWritePos);
          if BVWritePos > BVBufferCount - 1 then
            BVWritePos := 0;
          BVBuff[BVWritePos].Napr := Sign(Delta);
          BVBuff[BVWritePos].DataCount := 0;
{$IFDEF TESTING_COORD}
//          FSummaryWay:=FSummaryWay+Sign(Delta);   //For Testing
//          BVBuff[BVWritePos].FullCoord:=FSummaryWay + i*Sign(Delta);
         BVBuff[BVWritePos].FullCoord:=0;
         BVBuff[BVWritePos].DeltaCoord:=Sign(Delta);
{          if i=1 then begin BVBuff[BVWritePos].DeltaCoord:= Delta
          end
          else BVBuff[BVWritePos].DeltaCoord:=0;
}
{$ENDIF}
        end;

//{$IFDEF TESTING_COORD} //2
//         BVBuff[BVWritePos].FullCoord:=FSummaryWay;
//{$ENDIF}

       end; //Delta<>0
        CS_BV.Release;

      end;

    // ������ ���������� (��������� ������������)
    $3C:
      begin
        ADCoord:=0;
        if (Bl[2] and $F) = 4 then begin
          ADCoord:= ADCoord or Bl[6];
          ADCoord:= (ADCoord shl 8) or Bl[5];
          ADCoord:= (ADCoord shl 8) or Bl[4];
          ADCoord:= (ADCoord shl 8) or Bl[3];
          Self.BUMCoord:= ADCoord;
{$IFDEF TESTING_COORD}
//       FSummaryWay:=ADCoord;   //For Testing
          CS_BV.Enter;
          BVBuff[BVWritePos].FullCoord:=ADCoord;
          CS_BV.Release;
{$ENDIF}
        end;
      end;
    // ������������� ��������� �-���������
    $48: begin
           FBScanStopped:= true;
           Self.GetBV(TempBV);
         end;
    // ������� � ���������� ������� �� ������������� (��������� ������������)
    $64:
      begin
        PainterIsIdle := True;
        ADCoord:=0;
        ADCoord:= ADCoord or Bl[6];
        ADCoord:= (ADCoord shl 8) or Bl[5];
        ADCoord:= (ADCoord shl 8) or Bl[4];
        ADCoord:= (ADCoord shl 8) or Bl[3];
        ADDelta:= 0;
        ADDelta:= ADDelta or Bl[9];
        ADDelta:= (ADDelta shl 8) or Bl[8];
        if Assigned(OnAirBrushDone) then
          OnAirBrushDone(Self.ID, ADCoord, Bl[7], ADDelta);
      end;

    $70, $71:
      begin
        // ������ 1 ����� B-���������.
        if Bl[1] = $70 then
          if BVWritePos <> -1 then
            with BVBuff[BVWritePos] do
            begin
              Cou := (Bl[2] and $F);
              if Cou > 8 then Cou:=8;
              Data[DataCount] := Cou;
              for i := 1 to Cou do
                Data[DataCount + i] := Bl[i + 2];
              DataCount := DataCount + Cou + 1;
            end;

        // ���� �-���������.
        R := Sign(Bl[3] and $80);
        T := Bl[3] and $7F;

        if T <= High(MView[R]) then
        begin
          if MView[R, T, 0].StoreTick <= MView[R, T, 1].StoreTick then
            i := 0
          else
            i := 1;
          MView[R, T, i].Block := Bl;
          MView[R, T, i].StoreTick := GetTickCount;
        end;

      end;

    // ���
    $83:
      // if isASDOn then
      begin
        T := Bl[4];
        for N := 0 to 1 do // ���� �� ������.  N - ����� ���� � ����� ������ ���: ��� (0), ���� (1).
        begin
          MainNit := N = Config.WorkRail; // �������� �����?
          WriteNit := N xor Config.WorkRail; // ����� ���� � �������� ��� (0) / ������. (1)

          // ���������, ����� ������ ���������.
          // ���� �����.
          if N = 0 then
          begin
            K1 := (Bl[3] and 2 = 2); // ������������ ������� ������ ����� T ���� 0.
            K2 := (Bl[3] and 4 = 4); // ������������ ������� ������ ����� T ���� 0.
            AK_ := (Bl[3] and 8 = 8); // ������ �� ����� T ���� 0.
          end
          else
          begin
            // ���� ������.
            K1 := (Bl[3] and 32 = 32); // ������������ ������� ������ ����� T ���� 1.
            K2 := (Bl[3] and 64 = 64); // ������������ ������� ������ ����� T ���� 1.
            AK_ := (Bl[3] and 128 = 128); // ������ �� ����� T ���� 1.
          end;

          // ��������� ������ �������.
          Kan1 := -1;
          Kan2 := -1;

          HandKanalIdx := -1;
          for i := 0 to High(HKanals) do
            if HKanals[i].IsActive then
              HandKanalIdx := i;

          if HandKanalIdx <> -1 then
          begin
            // ������.
            Kanal1 := HKanals[HandKanalIdx];
            if Kanal1.Takt.CANObj = Self then // ���.
              if Kanal1.Takt.N_res = N then // ���� � ���.
              begin
                if Kanal1.Takt.Kanal1 = Kanal1 then
                  Snd.SetHandASD(K1 xor Kanal1.InverseASD)
                else
                  Snd.SetHandASD(((K2 xor Kanal1.InverseASD)
                      { and Kanal1.SoundOn } ));
              end;

          end
          else if (T <= High(Takts[FID, WriteNit])) and
            (Takts[FID, WriteNit, T] <> nil) then
          begin
            // ��������.
            Kan1 := Takts[FID, WriteNit, T].Kan1;
            Kan2 := Takts[FID, WriteNit, T].Kan2;
            Kanal1 := Takts[FID, WriteNit, T].Kanal1;
            Kanal2 := Takts[FID, WriteNit, T].Kanal2;

{$IFnDEF EGO_USW} //��� EGO_USW ���� ���������� FID � ���� ( BUMtoRail(Rail) )
//{$IFnDEF VMT_US}
            if Assigned(Kanal1) then
              Kanal1.ASD := (K1 xor Kanal1.InverseASD) { and Kanal1.SoundOn } ;
            if Assigned(Kanal2) then
              Kanal2.ASD := (K2 xor Kanal2.InverseASD) { and Kanal2.SoundOn } ;
            if Kan1 > -1 then
              Snd.SetASD({$IFDEF BUMCOUNT1} N {$ELSE} FID
                {$ENDIF}, Kan1, ((K1 xor Kanal1.InverseASD) and Kanal1.SoundOn)
                );
            if Kan2 > -1 then
              Snd.SetASD({$IFDEF BUMCOUNT1} N {$ELSE} FID
                {$ENDIF}, Kan2, ((K2 xor Kanal2.InverseASD) and Kanal2.SoundOn)
                );
//{$ENDIF}
{$ENDIF}
          end;
        end;
      end; // (���)
    else i:=i+1;
  end; // (case)
end;

function TCAN.SetPar(Takt, ParOfs, ParV: Byte): Boolean;
begin
  SetPar := SendBL($40, 3, Takt, ParOfs, ParV, 0, 0, 0, 0, 0);
end;

function TCAN.SetStrob(N, Takt, Nstr, T1, T2, A: Byte; TwoEcho: Boolean)
  : Boolean;
begin
  SetStrob := SendBL($49, 7, $80 * (N and 1) + Takt, Nstr and 3, T1, 0, T2, 0,
    (A and $FE) + Ord(TwoEcho), 0);
end;

function TCAN.SetVRU(N, Takt, T1, A1, T2, A2: Byte): Boolean;
begin
  SetVRU := SendBL($41, 7, $80 * (N and 1) + Takt, T1, 0, A1, T2, 0, A2, 0);
end;

function TCAN.SetVRU_Point(N, Takt, T, A: Byte): Boolean;
begin
  Result := SetVRU(N, Takt, T, A, T, A);
end;

function TCAN.SetTakt(T, GenL, GenR, ResL, ResR, Tmn, Tmax: Byte; WSA: Word;
  ZondAmpl: Byte): Boolean;
{ ������������� ��������� ����� (����� ��� � ���������� �������).
  �������������� ���� ��������� InitCikl!
  T - ���� (0..KT-1),
  GenL, GenR - ����� ���������� � ����� � ������ ����, ������������� (0..7, ��� 15 - ����.),
  ResL, ResR - ����� ��������� � ����� � ������ ����, ������������� (0..7, ��� 15 - ����.),
  Tmn - ��������� �������� �������� (��� ��-������: 3, ����� 1)
  Tmax - ������������ �����,
  WSA - work space address (2 �����) }
const
{$IFDEF Avikon15RSP}
  PorogB = $8; // -12 ��
{$ELSE}
  PorogB = $10; // -6 ��
{$ENDIF}
var
  E: Boolean;
  WSA_h: Byte;
begin
  E := SetPar(T, $1, GenL shl 4 + ResL); // ���/����� ����� ����
  E := E and SetPar(T, $2, GenR shl 4 + ResR); // ���/����� ������ ����
  E := E and SetPar(T, $5, 0); // ������ 0
  // ���������� ������� ���� WSA (� ����������� � ������� �������)
  WSA_h := (WSA shr 8) and $FF;
  If WSA and $FF <> 0 then
    Inc(WSA_h);
  // WSA_:=$30 + $10*T  ;     //                                  !!!!!!!!!!!!!!!!!!!!!!!!!!
  E := E and SetPar(T, $6, WSA_h); // WSA
  E := E and SetPar(T, $7, Tmax); // ������������ ��������� (���)
  E := E and SetPar(T, $8, { $10 } PorogB); // ������� ������ B-����. ���.����
  E := E and SetPar(T, $9, $20); // ������� ������ 1 ���.����
  E := E and SetPar(T, $A, $20); // ������� ������ 2 ���.����
  E := E and SetPar(T, $B, $20); // ������� ������ 3 ���.����
  E := E and SetPar(T, $C, { $10 } PorogB); // ������� ������ B-����. ����.����
  E := E and SetPar(T, $D, $20); // ������� ������ 1 ����.����
  E := E and SetPar(T, $E, $20); // ������� ������ 2 ����.����
  E := E and SetPar(T, $F, $20); // ������� ������ 3 ����.����
  E := E and SetPar(T, $10, ZondAmpl); // ��������� ������������ ���.���� (0..7)
  E := E and SetPar(T, $11, ZondAmpl); // ��������� ������������ ����.���� (0..7)
  E := E and SetPar(T, $12, Tmn); // ��������� �������� �������� (��� ��-������: 3, ����� 1)
  SetTakt := E;
end;

function TCAN.InitCikl(KT_: Byte): Boolean;
begin
  KT := KT_;
  InitCikl := SendBL($42, 1, KT, 0, 0, 0, 0, 0, 0, 0); // ���-�� ������
end;

function TCAN.Set2TP(N, Takt: Byte; l_2TP, r_2TP: Single): Boolean;
var
  L, R: Word;
begin
  L := trunc(l_2TP * 10);
  R := trunc(r_2TP * 10);
  Set2TP := SendBL($4A, 5, Takt { or (N shl 7) } , Lo(L), Lo(R), Hi(L), Hi(R));
  // 2Tp ��� ����� � ������ ����
end;

function TCAN.StartAview(N, Takt, Tst, M: Byte; WorkStrob: Byte;
  Single: Boolean = false; OnlyMax: Boolean = false): Boolean;
var
  B: Boolean;
begin
  B := SendBL($45, 6, $80 * (N and 1) + Takt, Tst, 0, M, $80 * Ord(Single)
      + $10 * Ord(OnlyMax), WorkStrob, 0, 0); // ���� ����� M: 0 - ����������; 1 - ����������.
  Result := B;
end;

function TCAN.StopAview: Boolean;
begin
  StopAview := SendBL($46, 1, 0);
end;

function TCAN.ImitDP;
begin
  if FImitDPState <> isOn then
  begin
    ImitDP := SendBL($3E, 1, Ord(isOn));
    FImitDPState := isOn;
  end;
end;

function TCAN.ASDTurnOn: Boolean;
begin
  SendBL($4B);
end;

procedure TCAN.SetID(ID: Byte);
begin
   FID:=ID;
end;

{
  function TCAN.ASDTurnOn( HandKanal_: Integer ): Boolean;
  var
  n, i: Integer;

  begin
  Result:= True;
  HandKanal:= HandKanal_;

  for n:= 0 to 1 do
  for i:= 0 to High( ASD ) do
  begin
  ASD[N, i]:= False;
  end;

  if not isASDOn then
  begin
  isASDOn:= True;
  end;
  end;
}

procedure TCAN.ASDTurnOff;
begin
  Snd.StopTone;
  isASDOn := false;
end;

function TCAN.Start: Boolean;
begin
  Result := SendBL($47, 0, 0, 0, 0, 0, 0, 0, 0, 0);
end;

function TCAN.Reset;
begin
  WrLog('Reset procedure started...');
  Reset := SendBL($F0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
  If Wait then
    Sleep(5000);
end;

function TCAN.AskMasterSlaveStatus: Boolean; //������ �/� ������
begin
  Result := SendBL($18, 0, 0, 0, 0, 0, 0, 0, 0, 0);
end;


function TCAN.Stop: Boolean;
begin
  FBScanStopped:= false;
  Stop := SendBL($48, 0, 0, 0, 0, 0, 0, 0, 0, 0); // ����. B-����.
  StopAview;
end;

{ function TCAN.GetMaximum( Nit_, Takt_ : Byte; var T, A : Byte; var AView: AViewArr; GetAView: Boolean ): Boolean;
  var
  BL : TBL;
  i  : Integer;
  j  : Integer;
  Cou: Integer;
  begin
  WrLog('(i) Getting maximum...');
  GetMaximum:=false;
  StartAview( Nit_, Takt_, 0, 10, 0, True, not GetAView);
  Sleep(100);
  repeat
  If not GetBL(BL) then exit;
  until BL[1]=$82;
  A:=BL[3];
  T:=BL[4];

  if GetAView then
  begin
  // ������� �-���������
  i:=0;
  Cou:=0;
  SetLength( AView, 232);
  while i<232 do
  begin
  inc( Cou );
  if Cou > 100 then exit;
  if not GetBL(BL) then exit;
  if BL[1] or 1 <> $81 then continue;
  for j:=0 to 7 do
  AView[i+j]:=BL[3+j];
  i:=i+8;
  end;
  end;

  GetMaximum:=true;
  end; }

function TCAN.Test: Word;
var
  T: LongInt;
begin
{$IFDEF AKPSimulation} Result := 1;
  Exit; {$ENDIF}
  Result := 0;
  TestResult := 0;
  SendBL($DE, 0, 0, 0, 0, 0, 0, 0, 0, 0);
  T := GetTickCount;
  repeat
  until (GetTickCount - T > 1000) or (TestResult <> 0);
  Result := TestResult;
end;

function TCAN.BVBuffCount: Integer;
begin
  if BVWritePos = -1 then
    Result := 0
  else if BVReadPos > BVWritePos then
    Result := BVBufferCount - BVReadPos + BVWritePos
  else
    Result := BVWritePos - BVReadPos;
end;


// -----------------------------------------------------------------------------
// -------------------------  ������ ����� ������  -----------------------------

function TTakt.Install( { CAN : TCAN; } SReg: Byte): Boolean;
var
  ResL, ResR, // �������� ����� � ������ ����
  GenL, GenR: Byte; // ��������� ����� � ������ ����
  ThisGen, ThisRes: Byte; // ��������� � �������� ����� �����.
  SinhGen, SinhRes: Byte; // ��������� � �������� ����������� �����.
  ZondAmpl: Byte; // ��������� ������������ (��� ������ ������� - 2, ��� ��������� - 7)
  InstallNumByBUMMode: integer;
  E: Boolean;

begin
  { ��������� ������� ����� � ������ ���� ��� ������� Takt. �������������� ���� ������� CAN.InitCikl.
    � ������ ��������� (SReg=3), ����� 5 �������� � ������ ���. }

  // ��� ������� ������ ���������� ������ ��� �������� (� �� ����������) ������, �� ���� ��� ���, � ������� ConfigNit = 0.
  SetRegMode(SReg);
  ThisGen := Gen;
  ThisRes := Res;

  if (SReg = 3) and ((Kan1 = 5) or (Kan1 = 2) or (Kan1 = 10) or (Kan1 = 12)) then
    ThisGen := ThisRes;

  if SinhTakt = nil then
  begin
    SinhGen := $F;
    SinhRes := $F;//0; AK 18-06-2015
  end
  else
  begin
    if (SReg = 3) and ((Kan1 = 5) or (Kan1 = 2) or (Kan1 = 10)) then
    begin
      SinhGen := SinhTakt.Res;
      SinhRes := SinhTakt.Res;
    end
    else
    begin
      SinhGen := SinhTakt.Gen;
      SinhRes := SinhTakt.Res;
    end;
  end;

  if N_res = 0 then
  begin
    if (SReg = 3) then
      begin
        GenL := ThisGen;
        ResL := ThisRes;
        GenR := 15;
        ResR := 15;
      end
    else
      begin
        GenL := ThisGen;
        ResL := ThisRes;
        GenR := SinhGen;
        ResR := SinhRes;
      end
  end
  else
    begin
      if (SReg = 3) then
        begin
          GenR := ThisGen;
          ResR := ThisRes;
          GenL := 15;
          ResL := 15;
        end
      else
        begin
          GenR := ThisGen;
          ResR := ThisRes;
          GenL := SinhGen;
          ResL := SinhRes;
        end;
  end;

  if Self.Alfa = 0 then
    if (Config.ZondAmpl>=0) and (Config.ZondAmpl<=7) then
      ZondAmpl := Config.ZondAmpl//7
    else ZondAmpl := 7
  else
    ZondAmpl := 7;
  // E:= CAN.SetTakt( InstallNum, GenL, GenR, ResL, ResR, DelayFactor, Ten, GetWSA(CAN.KT) );

  // E:= CANObj.SetTakt( InstallNum, GenL, GenR, ResL, ResR, DelayFactor, Ten, GetWSA(CANObj.KT) );
  if Assigned(CANObj) then
    E := CANObj.SetTakt(InstallNum, GenL, GenR, ResL, ResR, DelayFactor, Ten,
      GetWSA(CANObj.KT, InstallNum), ZondAmpl);

  if Kanal1 <> nil then
    E := E and InstallVRU(SReg);
  if SinhTakt <> nil then
    E := E and SinhTakt.InstallVRU(SReg);

  if Kanal1 <> nil then
    E := E and InstallStrob(SReg);
  if SinhTakt <> nil then
    E := E and SinhTakt.InstallStrob(SReg);

  { if Kanal1 <> nil then E:= E and InstallVRU(CAN, SReg);
    if SinhTakt <> nil then E:= E and SinhTakt.InstallVRU( CAN, SReg );

    if Kanal1 <> nil then E:= E and InstallStrob(CAN, SReg);
    if SinhTakt <> nil then E:= E and SinhTakt.InstallStrob(CAN, SReg); }

  // E:= E and Install2TP(CAN);
  E := E and Install2TP;

  // InstallEnvelop();

  Install := E;
end;

function TTakt.Install2TP { (CAN : TCAN) } : Boolean;
var
  _2TP_: array [0 .. 1] of Single;
  InstNum: integer;
begin
  if Kanal1 <> nil then
    _2TP_[N_res] := Kanal1.TwoTP
  else
    _2TP_[N_res] := 0;
  if (SinhTakt <> nil) and (SinhTakt.Kanal1 <> nil) then
    _2TP_[1 - N_res] := SinhTakt.Kanal1.TwoTP
  else
    _2TP_[1 - N_res] := 0;
  // Result:= CAN.Set2TP( N_res, InstallNum, _2TP_[0],  _2TP_[1] );
  if Assigned(CANObj) then
    Result := CANObj.Set2TP(N_res, InstallNum, _2TP_[0], _2TP_[1]);
end;

function TTakt.InstallEnvelop: Boolean;
begin
{$IFDEF GlideEnvelope}
  if Assigned(CANObj) and (Kanal2 <> nil) and (Kanal2.Method = '���') then
    Result := CANObj.InitEnvelope(InstallNum, 0 { 2 } );
{$ENDIF}
end;


function TTakt.InstallVRU(SReg: Byte): Boolean;
var
  R: Boolean;
  i: Integer;
  AkShelf: TVRCHShelf;
  Kan2: TVRCHShelf;

begin
  if Assigned(Self.Kanal1) then begin
    VRCH.ATTLevel := Kanal1.ATT;
    VRCH.Time := Kanal1.VRCH;
  end;

  if Assigned(Self.Kanal2) then
  begin
    Kan2 := VRCH.ShelfByID['Kan2'];
    Kan2.St := Self.Kanal2.Str_st;
    Kan2.Ed := AKStr_st + 2;
    Kan2.Level := Self.Kanal2.ATT;
    VRCH.ShelfByID['Kan2'] := Kan2;
  end;

  if (AKStr_st <> 0) and Assigned(Self.Kanal1) then
  begin
    AkShelf := VRCH.ShelfByID['AK'];
    AkShelf.St := AKStr_st + 3;
    AkShelf.Ed := AKStr_en - 3;
    AkShelf.Level := Kanal1.AK_Level * 2;
    VRCH.ShelfByID['AK'] := AkShelf;
  end;

  VRCH.CalculateVRCH;
  // �������� ��� � ���.
  R := True;
  for i := 0 to VRCH.VRCH.Count - 2 do
    if Assigned(CANObj) then
      R := R and CANObj.SetVRU(N_res, InstallNum, VRCH.VRCH.Curve[i].X,
        VRCH.VRCH.Curve[i].Y, VRCH.VRCH.Curve[i + 1].X,
        VRCH.VRCH.Curve[i + 1].Y);
  Result := R;
end;

function TTakt.InstallStrob(
  { CAN : TCAN; } SReg: Byte; Adj2TPMode: Boolean = false): Boolean;
var
  B: Boolean;
begin
  // B:= CAN.SetStrob(N_res, InstallNum, 0, BStr_st, BStr_en, 16, 0);  // ������� ����� - ����� B-���������.
  if Assigned(CANObj) then
    B := CANObj.SetStrob(N_res, InstallNum, 0, BStr_st, BStr_en, 16, false);
  // ������� ����� - ����� B-���������.
  if SReg in [rNastr, rHandNastr] then
  begin
    // � ������ ��������� - ������ � ������ - ����������� ������ ������� � ������� ������� (��������������)
    // � ������ ��������� 2TP ����������� ������ ��������� 2 TP.

    { if Adj2TPMode then
      B:= B and CAN.SetStrob(N_res, InstallNum, 1, Kanal1.NStr_st, Kanal1.NStr_en, 32, 0)
      else
      B:= B and CAN.SetStrob(N_res, InstallNum, 1, Kanal1.NStr2TP_st, Kanal1.NStr2TP_en, 32, 0);

      if Kan2 <> -1 then
      begin
      if Adj2TPMode then
      B:= B and CAN.SetStrob(N_res, InstallNum, 2, Kanal2.NStr_st, Kanal2.NStr_en, 32, 0)
      else
      B:= B and CAN.SetStrob(N_res, InstallNum, 2, Kanal2.NStr_st, Kanal2.NStr_en, 32, 0);
      end; }

    // B:= B and CAN.SetStrob(N_res, InstallNum, 1, Kanal1.NStr_st, Kanal1.NStr_en, 32, 0);
    if Assigned(CANObj) and Assigned(Kanal1)then
      if Adj2TPMode then
        B := B and CANObj.SetStrob(N_res, InstallNum, 1, Kanal1.NStr2TP_st,
          Kanal1.NStr2TP_en, 32, false)
      else
        B := B and CANObj.SetStrob(N_res, InstallNum, 1, Kanal1.NStr_st,
          Kanal1.NStr_en, 32, false);
    if Kan2 > -1 then
      if Assigned(CANObj) then
        if Adj2TPMode then
        begin
          if Self.Alfa <> 0 then
            B := B and CANObj.SetStrob(N_res, InstallNum, 2, Kanal2.NStr2TP_st,
              Kanal2.NStr2TP_en, 32, false)
          else
            CANObj.SetStrob(N_res, InstallNum, 2, Kanal2.Str_st, Kanal2.Str_en,
              32, Kanal2.TwoEcho);
        end
        else
          B := B and CANObj.SetStrob(N_res, InstallNum, 2, Kanal2.NStr_st,
            Kanal2.NStr_en, 32, false);
    // B:= B and CAN.SetStrob(N_res, InstallNum, 2, Kanal2.NStr_st, Kanal2.NStr_en, 32, 0);

  end
  else
  begin
    // � ������ ������ - ������ � ������ - ������� ������ ������� � ������� ������� (��������������)
    // B:= B and CAN.SetStrob(N_res, InstallNum, 1, Kanal1.Str_st, Kanal1.Str_en, 32, 0);
    if Assigned(CANObj) and Assigned(Kanal1) then
      B := B and CANObj.SetStrob(N_res, InstallNum, 1, Kanal1.Str_st,
        Kanal1.Str_en, 32, Kanal1.TwoEcho);
    if Kan2 > -1 then
      // B:= B and CAN.SetStrob(N_res, InstallNum, 2, Kanal2.Str_st, Kanal2.Str_en, 32, 0);
      if Assigned(CANObj) then
        B := B and CANObj.SetStrob(N_res, InstallNum, 2, Kanal2.Str_st,
          Kanal2.Str_en, 32, Kanal2.TwoEcho);
  end;

  // ������ ����� - ����� ���� ����� ��� �������� ��.
  if UseZM then
  begin
    if ZonaM < ZonaMSize then
      ZonaM := ZonaMSize;
    // B:= B and CAN.SetStrob(N_res, InstallNum, 3, ZonaM - ZonaMSize, ZonaM + ZonaMSize, 32, 0);
    if Assigned(CANObj) then
      B := B and CANObj.SetStrob(N_res, InstallNum, 3, ZonaM - ZonaMSize,
        ZonaM + ZonaMSize, 32, false);
  end
  else
  begin
    // B:= B and CAN.SetStrob(N_res, InstallNum, 3, AKStr_st, AKStr_en, 32, 0)
    if Assigned(CANObj) then
      B := B and CANObj.SetStrob(N_res, InstallNum, 3, AKStr_st, AKStr_en, 32,
        false)
  end;

  Result := B;

end;

function TTakt.GetWSA(KT: Byte; InstNum: Byte): Word;
begin
  Result := $3000 + $1000 * InstallNum;
  //Result := $3000 + $1000 * InstNum;
end;

function TTakt.SetConfig(S: string): Boolean;
var
  Method: string;
  EdgeInt: integer;
begin
  EdgeInt:= -1;
  Result := false;
  // Adj2Tp:=false;
  GetS(S, Reg);
{$IF DEFINED(BUMCOUNT2) OR DEFINED(BUMCOUNT4)}
  GetS(S, BUM);
{$ELSE}
  BUM := 0;
{$IFEND}
  GetS(S, ConfigNit);
  GetS(S, Num);
  GetS(S, Gen_pep);
  GetS(S, Res_pep);
  GetS(S, Gen);
  GetS(S, Res);
  GetS(S, Method);
  GetS(S, Alfa);
  GetS(S, Ten);
  GetS(S, Kan1);
  GetS(S, Kan2);
  GetS(S, BStr_st);
  GetS(S, BStr_en);
  GetS(S, AV_mash);
  GetS(S, AKStr_st);
  GetS(S, AKStr_en);
  GetS(S, DelayFactor);
  GetS(S, EdgeInt);
{$IF DEFINED(EGO_USW) OR DEFINED(USK004R) DEFINED(VMT_US)}
  GetS(S, Gamma);
{$IFEND}
  Edge:= TEdgeOfRail(EdgeInt);
  Result := True;
end;

procedure TTakt.SetRegMode(Reg: integer);
begin
  if (Reg=rHand) or (Reg=rHandNastr) then
    Exit;
  if Reg=3 then
    InstallNum:= 0
  else
    InstallNum:= Self.Num;

  if Assigned(Self.SinhTakt) then
    Self.SinhTakt.InstallNum:= Self.InstallNum;
 {if Reg=3 then
    begin
      FRealInstNum:= InstallNum;
      InstallNum:= 0;
    end
  else
    begin
      if FRealInstNum >= 0 then
        InstallNum:= FRealInstNum;
    end;}
end;

constructor TTakt.Create;
begin
  FRealInstNum:= -1;
end;

function TTakt.FinishConfig: Boolean;
var
  KanArray: PSingleKanArray;

begin
  Result := false;
  // ��������� �������� ����� ���� (N_res) � ����� ������ ��� �������� (InstallNum).
  if Reg = 1 then
  begin
    // ������.
    InstallNum := 0;
    N_res := ConfigNit;
    KanArray := @HKanals;
  end
  else
  begin
    // ���������.
    InstallNum := Num;
{$IFDEF BUMCOUNT4}
  {$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
    KanArray := @Kanals[BUMtoRail(BUM)];//BUM=0,1-left  2,3-right
  {$ELSE}
    KanArray := @Kanals[BUM];
  {$IFEND}
{$ELSE}
{$IFDEF BUMCOUNT2}
    KanArray := @Kanals[BUM];
{$ELSE}
    KanArray := @Kanals[ConfigNit];
{$ENDIF}
{$ENDIF}
    N_res := ConfigNit xor Config.WorkRail; // ����� �� ������� ������!
  end;

  // ��������� ������ �� ������� ������ �� ������� �����.
  try
    if Kan1 <= -1 then
      Kanal1 := nil
    else
      Kanal1 := KanArray^[Kan1];
    if Kan2 <= -1 then
      Kanal2 := nil
    else
      Kanal2 := KanArray^[Kan2];
  except
    WrLog(Format(
        '�������� ������������. ���� %d.%d.%d (���.����.�����) ��������� �� �������������� ������ (������ � ����� *.tk; ������ ��������� � *.ka)', [Reg, ConfigNit, Num]));
    Exit;
  end;

  // ��������� ������ �� ������ ���� �� ������� �������.
  if Kanal1 <> nil then
    Kanal1.Takt := Self;
  if Kanal2 <> nil then
    Kanal2.Takt := Self;

  // ��������� ������ �� ���������� ����.
  SinhTakt := nil;
  if Reg <> 1 then
    if ConfigNit = 0 then
    begin
      if Num <= High(Takts[BUM, 1]) then
        SinhTakt := Takts[BUM, 1, Num];
    end
    else if Num <= High(Takts[BUM, 0]) then
      SinhTakt := Takts[BUM, 0, Num];

  StartUpVRCH;
  Result := True;
end;

procedure TTakt.GetNStrob(out St, En: Byte);
begin
  case Alfa of
    0:
      begin
        St := 40;
        En := 70;
      end;
    45:
      begin
        St := 30;
        En := 60;
      end;
    70:
      begin
        St := 60;
        En := 100;
      end;
  end;
end;


// -----------------------------------------------------------------------------
// -----------------------------  ������ ������  -------------------------------

function TKanal.AdjustKy(N: Byte): Boolean;
var
  ATT__: Byte;
  k: TKanal;

begin
  // ��������� �� �������� ����������������
  if (N < 1) or (N > 18) then
  begin
    Result := false;
    Exit;
  end;

  ATT__ := ATT - N + Ky_r; // ���������� �����. ���, ������� ���� ����������.
  ATT := ATT__; // ���. ���.
  Ky := Ky_r + (ATT - ATT__); // �� �����. �� Ky_r �� �������� ������ ��������� �����������.
  AdjDate := Now;

  if (Takt.Kanal2 <> nil) and (Takt.Kanal2.Method = Takt.Kanal1.Method) then
  begin
    if Takt.Kanal1 = Self then
      k := Takt.Kanal2
    else
      k := Takt.Kanal1;
    k.ATT := ATT + k.Ky_r - Ky_r; // ���������� �����. ���, ������� ���� ����������.
    k.Ky := k.Ky_r;
    k.AdjDate := Now;
  end;

  Result := True;
end;

function TKanal.SetConfig_KAN(S: string): Boolean;
// ����. ���������������� ������ (�� ������)
var
  Num_: Integer;
  Reg_: Byte;
  Nit_: Byte;
begin
  Result := false;
  GetS(S, Reg_);
  GetS(S, Nit_);
  GetS(S, Num_);
  if (Num_ <> Num) or (Reg_ <> Reg) or (Nit <> Nit_) then
    Exit;
  GetS(S, FVRCH);
  Self.VRCH := FVRCH;
  GetS(S, FTwoTP);
  GetS(S, Ky);
  GetS(S, FATT);
  GetS(S, Str_st);
  GetS(S, Str_en);
  GetS(S, NR_Start);
  GetS(S, NR_Length);
  GetS(S, NR_Level);
  GetS(S, AK_Level);
  GetS(S, AdjDate);

  // ������� ��� ���������� �������� � ����� �������������
  AdjustChanges[apKy].Flag := True;
  AdjustChanges[apKy].CurValue := Ky;

  AdjustChanges[apATT].Flag := True;
  AdjustChanges[apATT].CurValue := FATT;

  AdjustChanges[apStrobSt].Flag := True;
  AdjustChanges[apStrobSt].CurValue := Str_st;

  AdjustChanges[apStrobEd].Flag := True;
  AdjustChanges[apStrobEd].CurValue := Str_en;

  AdjustChanges[apVRCH].Flag := True;
  AdjustChanges[apVRCH].CurValue := FVRCH;

  AdjustChanges[apTwoTp].Flag := True;
  AdjustChanges[apTwoTp].CurValue := Round(FTwoTP * 10);

  Result := True;
end;

function TKanal.SetConfig_KA(S: string): Boolean;
// ����. ��������� ������ (�� ������)
var
  i: Integer;

begin
  IsActive := false;
  GetS(S, Reg);
  GetS(S, Nit);
  GetS(S, Num);
  GetS(S, TktN);
  GetS(S, Method);
  GetS(S, Ky_r);
  GetS(S, Str_st_r);
  GetS(S, Str_en_r);
  GetS(S, i);
  WaveDirection := TDirection(i);
  GetS(S, BViewDelayMin);
  GetS(S, BViewDelayMax);
  GetS(S, InverseASD);
  GetS(S, KanAK);
  if KanAK = -1 then
    KanAK := Num;
  GetS(S, AdjRailType);
  Result := True;
end;

function TKanal.GetConfig_KAN: string; // ������ (��� ����������) ���������������� ������ (� ���� ������)
var
  S: string;
begin
  S := '';
  AddS(S, Reg);
  AddS(S, Nit);
  AddS(S, Num);
  AddS(S, VRCH);
  AddS(S, TwoTP);
  AddS(S, Ky);
  AddS(S, FATT);
  AddS(S, Str_st);
  AddS(S, Str_en);
  AddS(S, NR_Start);
  AddS(S, NR_Length);
  AddS(S, NR_Level);
  AddS(S, AK_Level);
  AddS(S, AdjDate);
  Result := S;
end;

procedure TKanal.SetATT(ATT: Byte);
begin
  if ATT > 80 then
    FATT := 80
  else
    FATT := ATT;
end;

procedure TKanal.SetVRCH(VRCH: Byte);
begin
  FVRCH := VRCH;
  if Assigned(Takt) then
  begin
    if Assigned(Takt.Kanal1) then
      Takt.Kanal1.FVRCH := VRCH;
    if Assigned(Takt.Kanal2) then
      Takt.Kanal2.FVRCH := VRCH;
    Takt.VRCH.Time := FVRCH;
    // Takt.VRCH.CalculateVRCH;
  end;
end;

procedure TKanal.SetTwoTP(TwoTP: Single);
begin
  FTwoTP := TwoTP;
  if Assigned(Takt) then
  begin
    if Assigned(Takt.Kanal1) then
      Takt.Kanal1.FTwoTP := TwoTP;
    if Assigned(Takt.Kanal2) then
      Takt.Kanal2.FTwoTP := TwoTP;
  end;
end;

procedure TKanal.InitNStr;
var
  i: Integer;
begin
  if assigned(Takt) then
  // ������ ����������� ���������.
  if Reg = 0 then
  begin // ���������.
    {case Self.Takt.Alfa of
       0: begin
          NStr_st := Str_st;
          NStr_en := Str_en;
        end;
       70, 65: begin
                 NStr_st := Round(MM2MKS(10, Takts[BUM, Nit, TktN].Alfa, True));
                 NStr_en := Round(MM2MKS(16, Takts[BUM, Nit, TktN].Alfa, True));
              end;
       else
          begin
            NStr_st := Round(MM2MKS(39, Takts[BUM, Nit, TktN].Alfa, True));
            NStr_en := Round(MM2MKS(45, Takts[BUM, Nit, TktN].Alfa, True));
          end;
    end;}
    // if Nit = 0 then
    case Num of
      0: // ��������� �������� �����.
        begin
          NStr_st := Str_st;
          NStr_en := Str_en;
        end;
      4, 5:
        begin
          //NStr_st := Round(MM2MKS(10, Takts[BUM, Nit, TktN].Alfa, True));
          //NStr_en := Round(MM2MKS(16, Takts[BUM, Nit, TktN].Alfa, True));
          NStr_st := Round(MM2MKS(10, Takt.Alfa, True));
          NStr_en := Round(MM2MKS(16, Takt.Alfa, True));
        end;
    else
      begin
        //NStr_st := Round(MM2MKS(39, Takts[BUM, Nit, TktN].Alfa, True));
        //NStr_en := Round(MM2MKS(45, Takts[BUM, Nit, TktN].Alfa, True));
        NStr_st := Round(MM2MKS(39, Takt.Alfa, True));
        NStr_en := Round(MM2MKS(45, Takt.Alfa, True));
      end;
    end;
    if ((Num = 10) and (Self.Method = 'MSM')) then
      begin
        NStr_st := Str_st;
        NStr_en := Str_en;
      end;
    if Num = 5 then
      if Self.Takt.Alfa = 58 then
      begin
        //NStr_st := Round(MM2MKS(39, Takts[BUM, Nit, TktN].Alfa, True));
        //NStr_en := Round(MM2MKS(45, Takts[BUM, Nit, TktN].Alfa, True));
        NStr_st := Round(MM2MKS(39, Takt.Alfa, True));
        NStr_en := Round(MM2MKS(45, Takt.Alfa, True));
      end;


    { else
      begin
      NStr_st:= Round( MM2MKS( 10, Takts[ Nit, TktN ].Alfa, True ) );
      NStr_en:= Round( MM2MKS( 16, Takts[ Nit, TktN ].Alfa, True ) );
      end; }
  end
  else
    case Num of
      0: // �������.
        begin
          NStr_st := Str_st;
          NStr_en := Str_en;
        end;
      1: begin
           NStr_st := Round(MM2MKS(39, Takt.Alfa, True));
            NStr_en := Round(MM2MKS(45, Takt.Alfa, True));
         end;
      2, 3, 4:
        begin
          NStr_st := Round(MM2MKS(39, HTakts[TktN].Alfa, True));
          NStr_en := Round(MM2MKS(45, HTakts[TktN].Alfa, True));
        end;
      5, 6:
        begin
          NStr_st := Round(MM2MKS(10, HTakts[TktN].Alfa, True));
          NStr_en := Round(MM2MKS(16, HTakts[TktN].Alfa, True));
        end;
    end;

  // ������ ��������� 2 TP.
  if not Assigned(Takt) then
  begin
    WrLog(Format
        ('�������� ������������. ����� %d ���� %d �� ��������� �� ����.',
        [Nit, Num]));
    Exit;
  end;

  if Takt.Alfa = 0 then
  begin
    NStr2TP_st := 17;
    NStr2TP_en := 23;
  end
  else
  begin
    NStr2TP_st := 33;
    NStr2TP_en := 39;
  end;

end;

function TTakt.GetAViewTimeMax: Integer;
begin
  Result := Round(23.2 * AV_mash);
end;

procedure TKanal.StartAview;
var
  WorkStrob: Integer;

begin
  if Takt.UseZM then
    WorkStrob := 3
  else if Num = Takt.Kan1 then
    WorkStrob := 1
  else
    WorkStrob := 2;
  // CAN.StartAview( Takt.N_res, Takt.Num, 0, Takt.AV_mash, WorkStrob, False, False );
  // CAN.StartAview( Takt.N_res, Takt.InstallNum, 0, Takt.AV_mash, WorkStrob, False, False );
  if Assigned(Takt.CANObj) then
    Takt.CANObj.StartAview(Takt.N_res, Takt.InstallNum, 0, Takt.AV_mash,
      WorkStrob, false, false);
end;

function TKanConfig.AddNewConfig(Name: string): Boolean;
begin
  UserConfigList.Add(Name);
  CurrentUserConfig := UserConfigList.Count - 1;
  Self.SaveUserConfig(CurrentUserConfig);
end;

{ TVRCHEngine }

procedure TVRCHEngine.AddShelf(ID: string; Level, St, Ed: Integer;
  DependsKy: Boolean);
var
  T: Integer;
begin
  if St > Ed then
  begin
    T := St;
    St := Ed;
    Ed := T;
  end;
  Inc(FShelfCount);
  FShelfMas[FShelfCount - 1].ID := ID;
  FShelfMas[FShelfCount - 1].DependsKy := DependsKy;
  FShelfMas[FShelfCount - 1].Level := Level;
  FShelfMas[FShelfCount - 1].St := St;
  FShelfMas[FShelfCount - 1].Ed := Ed;

end;

procedure TVRCHEngine.CalculateCurve;

const
  x_step = 0.1;

var
  i: Integer;
  P: TFloatPoint;

begin
  FCurve.Count := 0;
  P.X := 0;
  i := 0;
  repeat
    P.Y := power(P.X, FK);
    FCurve.Curve[i] := P;
    FCurve.Count := i + 1;
    if P.X >= 1 then
      Break;
    i := i + 1;
    P.X := P.X + x_step;
  until false;
end;

procedure TVRCHEngine.CalculateVRCH;

  procedure AddPoint(X, Y: Integer);
  var
    i: Integer;
  begin
    i := VRCH.Count;
    VRCH.Count := i + 1;
    VRCH.Curve[i].X := X;
    VRCH.Curve[i].Y := Y;
  end;

var
  CurveEnd: Integer;
  Level, i, MinIdx, j, k: Integer;
  Temp: TVRCHShelf;
  LastTime, CurLevel, LastLevel, MaxShelfBorder: Integer;

begin
  // ������ � �������������� ����� ��� ������ �����
  VRCH.Count := 0;
  CurveEnd := FVRCHTime;
  Level := FKanalATT * 2;

  for i := 0 to FCurve.Count - 1 do
  begin
    AddPoint(Round(CurveEnd * FCurve.Curve[i].X), Round
        (Level * FCurve.Curve[i].Y));
  end;
  LastTime := CurveEnd;
  // ��������� ������ �����, ����� ������� ����� ��� �� �����������
  for i := 0 to FShelfCount - 1 do
  begin
    MinIdx := i;
    for j := i to FShelfCount - 1 do
      if FShelfMas[MinIdx].St > FShelfMas[j].St then
        MinIdx := j;
    Temp := FShelfMas[i];
    FShelfMas[i] := FShelfMas[MinIdx];
    FShelfMas[MinIdx] := Temp;
  end;

  // ��������� ��������� ����� � ����� ���
  for i := 0 to FShelfCount - 1 do
  begin
    if i = 0 then
      LastLevel := Level
    else
    begin
      LastLevel := VRCH.Curve[VRCH.Count - 1].Y;
      LastTime := VRCH.Curve[VRCH.Count - 1].X;
    end;
    // ���� ������ ������� ����� ������� �� ����� ���������� �� ���� �������� �����
    // � ����� ���������� ����� �� ������ ������� �� ������ Level
    if FShelfMas[i].St > LastTime then
    begin
      // �������� ����� �� ������ Level (���� ��� ����� ������ �����, � �� ������ �����)
      if i > 0 then
        AddPoint(LastTime, Level);
      // ����� ����� �� ������ Level �� ������ ������� �����
      AddPoint(FShelfMas[i].St, Level);
    end;
    // ��������� ����� �����. ������ ������� ����� � ������� ������ ������� �����
    if FShelfMas[i].DependsKy then
      CurLevel := Level + FShelfMas[i].Level * 2
    else
      CurLevel := FShelfMas[i].Level * 2;
    if CurLevel > 255 then
      CurLevel := 255;
    if CurLevel < 0 then
      CurLevel := 0;
    AddPoint(FShelfMas[i].St, CurLevel);
    // ��������� ����� �����. ������ ������� ����� � ������� ����� ������� �����
    AddPoint(FShelfMas[i].Ed, CurLevel);
  end;
  LastTime := VRCH.Curve[VRCH.Count - 1].X;
  // ���� ��������� �� ������ �� ����� �� 232 ���, �� �������� ����� �� ������ Level � ���������� �� 232 ���
  if LastTime < 232 then
  begin
    // ��������� �����, ���������� ������� �� Level
    AddPoint(LastTime, Level);
    // ��������� ��������� �����, ������������ ����� �� 232 ��� �� ������ Level
    AddPoint(232, Level);
  end;
end;

procedure TVRCHEngine.Clear;
begin
  FShelfCount := 0;
  VRCH.Count := 0;
end;

constructor TVRCHEngine.Create;
begin
  FK := 0.4;
  FKanalATT := 0;
  CalculateCurve;
  FShelfCount := 0;
end;

destructor TVRCHEngine.Destroy;
begin
  //
  inherited;
end;

function TVRCHEngine.GetShelfByID(ID: string): TVRCHShelf;
var
  i: Integer;
begin
  for i := 0 to FShelfCount - 1 do
  begin
    if FShelfMas[i].ID = ID then
    begin
      Result := FShelfMas[i];
      Break;
    end;
  end;
end;

function TVRCHEngine.GetShelfByIdx(Index: Integer): TVRCHShelf;
begin
  Result := FShelfMas[Index];
end;

procedure TVRCHEngine.SetKanalATT(Value: Integer);
begin
  if (Value > 0) and (Value <= MaxATT_Level) then
    FKanalATT := Value;
end;

procedure TVRCHEngine.SetShelfByID(ID: string; Value: TVRCHShelf);
var
  i: Integer;
begin
  for i := 0 to FShelfCount - 1 do
  begin
    if FShelfMas[i].ID = ID then
    begin
      FShelfMas[i] := Value;
      Break;
    end;
  end;
end;

procedure TVRCHEngine.SetVRCHTime(Value: Integer);
begin
  if (Value > 0) and (Value <= MaxVRCH_Time) then
    FVRCHTime := Value;
end;

procedure TTakt.StartUpVRCH;
begin
  // procedure TVRCHEngine.AddShelf(ID: string; Level, St, Ed: Integer; DependsKy: Boolean);
  VRCH := TVRCHEngine.Create;
  if Self.Kan2 > -1 then
    VRCH.AddShelf('Kan2', 0, 0, 0, false);
  // VRCH.AddShelf('Test1', -60, 20, 40, true);
  // VRCH.AddShelf('Test2', 60, 80, 100, true);

  VRCH.AddShelf('AK', 0, 0, 0, false);
  VRCH.CalculateVRCH;
end;

function TKanal.GetVRCH: Byte;
begin
  // Result:=Self.Takt.VRCH.Time;
  Result := FVRCH;
end;

procedure TCAN.GetMV(MV: PMVBuffer); // �������� M-���������.
var
  R, tk, i, j: Integer;
  T: Cardinal;
  S: string;

begin
  T := GetTickCount;
  MV.DataCount := 0;

  // ������ ��� �� ������ ������� �-��������� � �����.
  for R := 0 to 1 do
    for tk := 0 to 9 do
      for i := 0 to 1 do
        if
        { MView[r, tk, i].StoreTick <> 0 } Abs
          (Integer(T) - Integer(MView[R, tk, i].StoreTick)) < 1000 then
        begin
          WrLog(Format('GetMV: Rail = %d, Takt = %d, B = %d. DATA = %s',
              [R, tk, i, BlockAsString(MView[R, tk, i].Block)]));
          for j := 2 to 2 + (MView[R, tk, i].Block[2] and $0F) do
          begin
            MV.Data[MV.DataCount] := MView[R, tk, i].Block[j];
            Inc(MV.DataCount);
          end;
        end;

end;

function TCAN.GetVelocity: Single;
var
  Dt: Int64;
begin
  Result := 0;
  Dt := GetTickCount - VelMesStartTime;
  if Dt > 1000 then
  begin
    if Dt < { 60000 } 300000 then
      Result := (Abs(VelMesCurDx) * Config.DPStep * 0.0036) / (Dt / 1000);

    VelMesStartTime := GetTickCount;
    VelMesCurDx := 0;
  end;
end;

{

  procedure TCAN.GetMV( CallBack: TMCallBack ); // �������� M-���������.
  var
  StartTime: Cardinal;

  begin
  MBuff.BlockCount[0]:= 0;
  MBuff.BlockCount[1]:= 0;
  MBuff.ByteCount:= 0;
  MCallBack:= CallBack;
  StartTime:= GetTickCount;
  MReq:= True;

  while MReq do
  begin
  if GetTickCount - StartTime > 500 then
  begin
  MReq:= False;
  if @MCallBack <> nil then MCallBack( @MBuff );
  Exit;
  end;
  Sleep(200);
  end;

  end;
  }
{
  procedure TCAN.OnTimer(Sender: TObject);
  begin
  if not FBUMReady then
  begin
  Self.SendBL(FBUMTestBlock);
  //ShowMessage('Poslal TEST Block !!');
  end
  else
  FTimer.Enabled:=false;
  end;
  }
function TCAN.GetBUM_SN: Integer;
begin
  Result := TestResult;
end;

function TCAN.GetBVCount: Integer;
begin
  if (BVReadPos = BVWritePos) or (BVWritePos = -1) then
    Result := 0
  else if BVReadPos > BVWritePos then
    Result := BVBufferCount - BVReadPos + BVWritePos
  else
    Result := BVWritePos - BVReadPos;
end;

function TCAN.BVCountToDelta(Count: Integer): Integer;
var
  i, Cou: Integer;

begin
  Result := 0;
  if (BVReadPos = BVWritePos) or (BVWritePos = -1) then
    Exit
  else
  begin
    if BVReadPos > BVWritePos then
    begin
      Cou := BVBufferCount - BVReadPos;
      if Count <= Cou then
      begin
        for i := BVReadPos to BVReadPos + Cou do
          Result := Result + Abs(BVBuff[i].Napr);
      end
      else
      begin
        for i := BVReadPos to BVReadPos + Cou do
          Result := Result + Abs(BVBuff[i].Napr);
        for i := 0 to BVWritePos do
          Result := Result + Abs(BVBuff[i].Napr);
      end;
      // Result:= Cou + BVWritePos;
    end
    else
    begin
      Cou := BVWritePos - BVReadPos;
      if Count < Cou then
        Cou := Count;
      for i := BVReadPos to BVReadPos + Cou do
        Result := Result + Abs(BVBuff[i].Napr);
    end;
  end;
end;

function TCAN.DeltaToBVCount(Delta: Integer): Integer;
var
  i, D, Cou: Integer;
begin
  Result := 0;
  D := 0;
  if (BVReadPos = BVWritePos) or (BVWritePos = -1) then
    Exit
  else
  begin
    if BVReadPos > BVWritePos then
    begin
      Cou := BVBufferCount - BVReadPos;
      for i := BVReadPos to BVReadPos + Cou do
        if D + Abs(BVBuff[i].Napr) > Delta then
          Break
        else
        begin
          D := D + Abs(BVBuff[i].Napr);
          Result := Result + 1;
        end;
      for i := 0 to BVWritePos do
        if D + Abs(BVBuff[i].Napr) > Delta then
          Break
        else
        begin
          D := D + Abs(BVBuff[i].Napr);
          Result := Result + 1;
        end;
    end
    else
    begin
      Cou := BVWritePos - BVReadPos;
      for i := BVReadPos to BVReadPos + Cou do
        if D + Abs(BVBuff[i].Napr) > Delta then
          Break
        else
        begin
          D := D + Abs(BVBuff[i].Napr);
          Result := Result + 1;
        end;
    end;
  end;
end;

function TKanal.GetTaktLength: TAScanLength;
begin
  if Assigned(Self.Takt) then
  begin
    Result.Mks := Round(Self.Takt.AV_mash * 23.1);
    Result.MM_H := Round(MKS2MM(Self.Takt.AV_mash * 23.1, Self.Takt.Alfa, True)
      );
    Result.MM_R := Round(MKS2MM(Self.Takt.AV_mash * 23.1, Self.Takt.Alfa, false)
      );
  end;
end;

procedure TCAN.SendTestBlock;
begin
  if ((GetTickCount - FLastTestTime) >= 500) and (not FBUMReady) then
  begin
    Self.SendBL(FBUMTestBlock);
    FLastTestTime := GetTickCount;
  end;
end;

procedure TCAN.ResetWayCounters;
begin
  FCount3F := 0;
  FSummaryWay := 0;
end;

{ TCanList }

function TCanList.Get(Index: Integer): TCAN;
begin
  Result := inherited Get(Index);
end;

procedure TCanList.Put(Index: Integer; Item: TCAN);
begin
  inherited Put(Index, Item);
end;

procedure TCanList.Move(CurIndex, NewIndex: Integer);
begin
  inherited Move(CurIndex, NewIndex);
end;

{ TKanal }
function TKanal.GetASD: Boolean;
begin
  FASDHaveRead := True;
  Result := FASD;
end;

procedure TKanal.SetASD(B: Boolean);
begin
  if B or FASDHaveRead then
    FASD := B;
  FASDHaveRead := false;
end;

procedure TCAN.SelectHandChannel(Ch: Byte);
begin
{$IFDEF UseHardware}
  SendBL($4F, 5, Ch, $FF, $FF, $FF, $FF);
  //Sleep(1000);
{$ENDIF}
end;

procedure TCAN.RunKraskopult;
begin
  SendBL($1F, 1, 0);
end;

constructor TKanal.Create;
begin
  inherited;
  TwoEcho := false;
  SoundOn := True;

  ConnectedStrobSt := nil;
  ConnectedStrobEd := nil;
end;

procedure TKanal.SetSound(B: Boolean);
begin
  FSound := B;
  AdjustChanges[apSound].Flag := True;
  AdjustChanges[apSound].CurValue := Ord(B);
end;

function TCAN.PainterOff: Boolean;
begin
  PainterOff := SendBL($1D, 3, 0, 0, 0, 0, 0, 0, 0, 0); // ����.
end;

function TCAN.PainterOn(Delta: WORD): Boolean;
begin
  PainterIsIdle := True;
  PainterOn := SendBL($1D, 3, 1, Delta and 255, (Delta shr 8) and 255, 0, 0, 0, 0, 0);
end;

function TCAN.PainterSetCoord(Coord: Integer): Boolean;
//var
//  BytesMasPtr: P4Bytes;
begin
  LastFullCoord := Coord;
//  BytesMasPtr := P4Bytes(@Coord);
//  PainterSetCoord := SendBL($63, BytesMasPtr^[3], BytesMasPtr^[2],
  //  BytesMasPtr^[1], BytesMasPtr^[0], 0, 0, 0, 0, 0);
  PainterSetCoord := SendBL($63, 4,
                            Coord and 255,
                            (Coord shr 8) and 255,
                            (Coord shr 16) and 255,
                            (Coord shr 24) and 255,
                            0, 0, 0, 0);
  //  BytesMasPtr^[1], BytesMasPtr^[0], 0, 0, 0, 0, 0);
end;

function TCAN.PainterSetMark(Coord: Integer): Boolean;
//var
  //BytesMasPtr: P4Bytes;
begin
  // PainterIsIdle:= false;
  //if (GetTickCount - FLastPainterMarkTime) > 5000 then
  //begin
    //FLastPainterMarkTime := GetTickCount;
    //BytesMasPtr := P4Bytes(@Coord);
    //PainterSetMark := SendBL($1B, BytesMasPtr^[3], BytesMasPtr^[2],
     // BytesMasPtr^[1], BytesMasPtr^[0], 0, 0, 0, 0, 0);
  //end;
  PainterSetMark := SendBL($1B, 4,
                           Coord and 255,
                           (Coord shr 8) and 255,
                           (Coord shr 16) and 255,
                           (Coord shr 24) and 255,
                           0, 0, 0, 0);
     // BytesMasPtr^[1], BytesMasPtr^[0], 0, 0, 0, 0, 0);
end;

procedure TCAN.ParceDF(Bl: TBlock);
var
  D: Byte;
begin
  with FFirmwareInfo do
  begin
    SN := TestResult;
    Day := Bl[7];
    Month := Bl[6];
    Year := Bl[5];
    Hour := Bl[8];
    Minute := Bl[9];
    Sec := Bl[10];
    FirmwareVers := Bl[4];
    FirmwareVersB1 := Bl[4];
    D := Bl[3] and 15;
    FirmwareVersB2 := D;
    FirmwareVers := FirmwareVers + D / 100;
    D := (Bl[3] shr 4) and 15;
    DeviceType := TDeviceType(D);
  end;
end;

function TCAN.InitEnvelope(Takt, StrobNum: Byte): Boolean;
begin
  Result := SendBL($4D, 4, Takt, Takt, StrobNum, StrobNum);
end;

procedure TCAN.SetReleMode(Mode: Byte);
begin
{$IFDEF UseHardware}
  SendBL($4F, 5, Mode, $FF, $FF, $FF, $FF);
{$ENDIF}
end;

function TCAN.SetBlockAsMaster(Huge2TP: Boolean): Boolean;
begin
  {$IFDEF RKS_U}
  Huge2TP := true;
  {$ENDIF}
  // ��������� ����� ��������
  if Huge2TP then
    SetBlockAsMaster := SendBL($1E, 1, 2, 0, 0, 0, 0, 0, 0, 0)
  else
    SetBlockAsMaster := SendBL($1E, 1, 0, 0, 0, 0, 0, 0, 0, 0);
end;

function TCAN.SetBlockAsSlave(Huge2TP: Boolean): Boolean;
begin
  // ��������� ����� �������
  if Huge2TP then
    SetBlockAsSlave := SendBL($1E, 1, 3, 0, 0, 0, 0, 0, 0, 0)
  else
    SetBlockAsSlave := SendBL($1E, 1, 1, 0, 0, 0, 0, 0, 0, 0);
end;

procedure TCAN.SetStatus;
var
  i: Integer;
  R: Boolean;
begin
  // �������� ��� �����
  //StatusIsSet := True;
  if ProgramProfile.NoSynchronization then
    Exit;
  i := 0;
  R := false;
  while ((not R) and (i < 100)) do
  begin
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
    if (BlockStatus = 0) then
      R := Self.SetBlockAsMaster(True)
    else if (BlockStatus = 1) then
      R := Self.SetBlockAsSlave(True);
{$ELSE}
    if (BlockStatus = 0) then
      R := Self.SetBlockAsMaster(Config.SoundedScheme = Wheels)
    else if (BlockStatus = 1) then
      R := Self.SetBlockAsSlave(Config.SoundedScheme = Wheels);
{$IFEND}
    Inc(i);
  end;
  if (R) then
    FStatusIsSet := True;

end;

end.
