unit PublicData;

interface

uses Classes, SysUtils, Types, Math, Graphics, PublicFunc{, PAKDataUnit};


const
//  CONNECT_USB = FALSE;


  Copyright_ru = '��� "�������������", 2006-2007';
  MadeIn_ru = '������, �����-���������';

  Copyright_eng = 'Radioavionika inc., 2006-2007';
  MadeIn_eng = 'Russia, Saint-Petersburg';

  ProgrammedBy1 = 'Mike Keskinov';
  ProgrammedBy2 = 'Denis Fedorenko';

  ZonaMSize = 3; // ������ ���� ����� (���)

  rScan = 1;
  rNastr = 3;
  rHand = 4;
  rHandNastr = 5;
  rKorrDP =6;
  rAKSetup = 7;
  rStop = 10;


var
  ManufactureNumber: Word;

const
  VK_ANY = 0;

type

  TKey = Word; //TWMKey;

  TASD = array[0..1, 0..11] of Boolean;
  TAK = array[0..1, 0..11] of Boolean;

  TVRCH = record
    Curve: array[0..231] of TPoint;
    Count: Byte;
  end;

  // ��������� ����������� �������.
  THandRegOptions =
  record
    PovNo: Byte;
    KanNo: Byte;
    ATT: Byte;
    Ky: ShortInt;
    VRCH: Byte;
    StartScrPos: Integer;
    EndScrPos: Integer;
  end;


  TSearchOptions =
  record
    Date: string;
    RailNum: string;
    PartNum: string;
    Oper: string;
    Master: string;
    Def: string;
    DefExactFlag: Boolean
  end;

       {
  TRegOptions = record
    BUM_Serial: Word;
    isInControl: Boolean; // ������� ��������?
    PartNo: LongWord;
    RelsNo: LongWord;
    CheckYear: Word;
    RelsHeight: Single;  // ��.
    RelsLength: Single;  // �.
    Nit: Byte;        // 0 - ���, 1 - ����.
    Tverd: string;
    Master: string;
    Oper: string;
    GrGodn: string;
  end;  }

  Log_proc = procedure ( Msg : String ) of object; // ��������� ���������� ������ ����
  WinMessage_proc = function( Msg : string; WinType: Byte ): Boolean of object; // WinType = 0 - ������, 1 - ������, 2 - ��������.


  function ManufactureNumberAsString: string;
  function MKS2MM( Sou: Single; Alfa: Byte; CalkH: Boolean ): Single;
  function MM2MKS( Sou: Single; Alfa: Byte; CalkH: Boolean ): Single;
  procedure TurnOffPC;
  function TestProgramAlreadyStart: Boolean;

  function AmplIndexToDB( AmplIndex: Integer ): Integer;
  function AmplDBToIndex( AmplDB: Integer ): Integer;
  function AmplDBToLin( AmplDB: Integer; Level: Single = 1/8 ): Integer;

  function Ticker( Index, TickCount: Integer ): Boolean;
  procedure TickerReset( Index: Integer );

  procedure FPSCalkulator( OutCanvas: TCanvas );


var
  WrLog: Log_proc;



implementation
uses Windows, ShellAPI, Forms, Messages;

type
  TFPSCalkulatorData =
  record
    Count: Integer;
    Tick: Cardinal;
    FPS: Single;
  end;


var
  Ticks: array[0..20] of Cardinal;
  FPS: TFPSCalkulatorData;


function Ticker( Index, TickCount: Integer ): Boolean;
begin
  if GetTickCount - Ticks[Index] > TickCount then
  begin
    Ticks[Index]:= GetTickCount;
    Result:= True;
  end else Result:= False;
end;

procedure TickerReset( Index: Integer );
begin
  Ticks[Index]:= GetTickCount;
end;

procedure FPSCalkulator( OutCanvas: TCanvas );
begin
           {
  Inc( FPS.Count );
  if GetTickCount - FPS.Tick > 2000 then
  begin
    FPS.FPS:= FPS.Count / 2;
    FPS.Count:= 0;
    FPS.Tick:= GetTickCount;
  end;

  if Assigned( OutCanvas ) then
  begin
    OutCanvas.Brush.Color:= clWhite;
    if Random < 0.5 then OutCanvas.Font.Color:= clBlue else OutCanvas.Font.Color:= clRed;
    OutCanvas.Font.Size:= 16;
    OutCanvas.TextOut( 0, 0, Format( 'FPS: %.1f    ', [ FPS.FPS ] ) );
  end;
         }
end;



function AmplIndexToDB( AmplIndex: Integer ): Integer;
begin
  case AmplIndex of
    0: Result:= -12;
    1: Result:= -10;
    2: Result:= -8;
    3: Result:= -6;
    4: Result:= -4;
    5: Result:= -2;
    6: Result:= 0;
    7: Result:= 2;
    8: Result:= 4;
    9: Result:= 6;
    10: Result:= 8;
    11: Result:= 10;
    12: Result:= 12;
    13: Result:= 14;
    14: Result:= 16;
    15: Result:= 18;
  end;
end;

function AmplDBToIndex( AmplDB: Integer ): Integer;
begin
  case AmplDB of
    -12:  Result:= 0;
    -10:  Result:= 1;
    -8:   Result:= 2;
    -6:   Result:= 3;
    -4:   Result:= 4;
    -2:   Result:= 5;
     0:   Result:= 6;
     2:   Result:= 7;
     4:   Result:= 8;
     6:   Result:= 9;
     8:   Result:= 10;
     10:  Result:= 11;
     12:  Result:= 12;
     14:  Result:= 13;
     16:  Result:= 14;
     18:  Result:= 15;
  end;
end;

function AmplDBToLin( AmplDB: Integer; Level: Single = 1/8 ): Integer;
begin
  Result:= Max( 0, Min( Round( Power( 10, AmplDB / 20 ) * 256 * Level ), 255 ) );
end;

procedure TurnOffPC;
var
  hWnd_: hWnd;
begin
  ShellExecute( hWnd_, 'open', PChar('shutdown'), PChar('/s /t 00'), nil, SW_NORMAL);
end;

function ManufactureNumberAsString: string;
var
 S: string;
begin
  S:= IntToStr( ManufactureNumber );
  while Length( S ) < 5 do
    S:= '0' + S;
  Result:= S;
end;

function MKS2MM( Sou: Single; Alfa: Byte; CalkH: Boolean ): Single;       // ��������� ������
// Alfa - ���� ����� � ��������.
// CalkH: True - ������ ������� ��������� (H), False - ������ ���������� �� ���������� (R).

var
  R,       // ���������.
  V        // V = 5.9 ��� ������� �����, 3.26 ��� ����������.
    : Single;

begin
  if Alfa < 20 then V:= 5.9 else V:= 3.26;
  // r(��) = t(���) * v/2
  R :=   Sou * V / 2;
  if CalkH then R:= R * cos( Alfa / 180 * pi );
  if R > 0 then Result:= R else Result:= 0;
end;


function MM2MKS( Sou: Single; Alfa: Byte; CalkH: Boolean ): Single;       // ��������� ������
// Alfa - ���� ����� � ��������.
// CalkH: True: Sou - ������� ��������� (H), False: Sou - ���������� �� ���������� (R).

var
  R,       // ���������.
  V        // V = 5.9 ��� ������� �����, 3.26 ��� ����������.
    : Single;

begin
  if Alfa < 20 then V:= 5.9 else V:= 3.26;
  if CalkH then R:= Sou / cos( Alfa / 180 * pi );
  // t(���)  = 2r(��) / v.
  R := 2 * R / V;
  if R > 0 then Result:= R else Result:= 0;
end;

function MM2MKS_2tp( Sou, TwoTp: Single; Alfa: Byte; CalkH: Boolean ): Single;       // ��������� ������
// Alfa - ���� ����� � ��������.
// CalkH: True: Sou - ������� ��������� (H), False: Sou - ���������� �� ���������� (R).

var
  R,       // ���������.
  V        // V = 5.9 ��� ������� �����, 3.26 ��� ����������.
    : Single;

begin
  if Alfa < 20 then V:= 5.9 else V:= 3.26;
  if CalkH then R:= Sou / cos( Alfa / 180 * pi );
  // t(���)  = 2r(��) / v.
  R := (2 * R / V){+TwoTp};
  if R > 0 then Result:= R else Result:= 0;
end;



function TestProgramAlreadyStart: Boolean;
var
  Wnd : hWnd;
  clss,buff,mainS: array [0..255] of Char;
  hMutex : integer;
begin
  //********************  �������������� ���������� ������� ********************
  // ������ "NameProgramm" ������� ���� ������
  StrCopy(mainS,'AV11RSP'); SetWindowText(Application.Handle,mainS);
  hMutex:=CreateMutex(nil,TRUE,'AV11RSP_semafor');       // ������� �������
  if GetLastError <> 0 then                             // ������ �������� - ������ ��� ������
  begin
    GetClassName(Application.Handle, clss,sizeof(clss)); // �������� ��� ������
    Wnd := GetWindow(Application.Handle, gw_HWndFirst);  // �������� ����� �� ����
    while Wnd <> 0 do
    begin
      if (Wnd<>Application.Handle)and(GetWindow(Wnd, gw_Owner)=0) then // ����� �� ���� �����
      begin
        GetClassName(Wnd,buff, sizeof(buff));    // �������� ����� ����
        if StrComp(buff,clss)=0 then             // ���������� � ����� �������
        begin
          GetWindowText(Wnd, buff, sizeof(buff)); // �������� ����� ���������� ����
          if StrComp(buff,mainS)=0 then           // ���������� � ����� ����
          begin
            SendMessage(Wnd,WM_SETFOCUS,Wnd,0);
            ShowWindow(Wnd,SW_SHOWNORMAL);
          end;
        end;
      end;
    Wnd := GetWindow(Wnd, gw_hWndNext); // ����� ���������
  end;
  Result:= True; // ����� ��� ��������.
  Exit;
 end;
 Result:= False; // ����� ��� �� ��������.
end;


initialization
end.
