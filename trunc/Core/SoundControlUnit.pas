unit SoundControlUnit;
interface
uses Types, Classes, SysUtils, MMSystem, Math,
     PublicData, PublicFunc, ConfigObj;

{$I Directives.inc}

const
    MaxAmp = 127;
    SampleLength = 3000;

type
  // �������� �����.
  TWave = record  { format of WAV file header }
    rId             : longint; { 'RIFF'  4 characters }
    rLen            : longint; { length of DATA + FORMAT chunk }
    { FORMAT CHUNK }
    wId             : longint; { 'WAVE' }
    fId             : longint; { 'fmt ' }
    fLen            : longint; { length of FORMAT DATA = 16 }
    { format data }
    wFormatTag      : word;    { $01 = PCM }
    nChannels       : word;    { 1 = mono, 2 = stereo }
    nSamplesPerSec  : longint; { Sample frequency ie 11025}
    nAvgBytesPerSec : longint; { = nChannels * nSamplesPerSec * (nBitsPerSample/8) }
    nBlockAlign     : word;    { = nChannels * (nBitsPerSAmple / 8 }
    wBitsPerSample  : word;    { 8 or 16 }
    { DATA CHUNK }
    dId             : longint; { 'data' }
    wSampleLength   : longint; { length of SAMPLE DATA }
    { sample data : offset 44 }
    Data: array [1..SampleLength, 1..2] of Byte;
    { for 8 bit mono = s[0],s[1]... :byte}
    { for 8 bit stereo = sleft[0],sright[0],sleft[1],sright[1]... :byte}
    { for 16 bit mono = s[0],s[1]... :word}
    { for 16 bit stereo = sleft[0],sright[0],sleft[1],sright[1]... :word}
  end;


  TSoundFlags = array[0..1, 0..3] of Boolean; // [���, �������]


  TSndControl = class( TThread )
  private
    FSoundOn: Boolean;
    //FASD: array[0..1, 0..11] of Boolean;
//    FASD: array[0..1, 0..13] of Boolean;
    //FASD: array[0..1, 0..15] of Boolean;
    FASD: array[0..1, 0..50] of Boolean;
    Snd: TSoundFlags;
    OldSnd: TSoundFlags;
    Wave: TWave;
    FRail: Integer; // ���� �� ������� �������� ��� (���� ������� ���� ������ �� ������ ������)
    FKanal: Integer; // ����� �� �������� �������� ���.
    FOneChannel: Boolean;
    FAmp: Byte; // ��������� ��������� ������� ������ (�� ��������� 127)
    FVolume: Integer; // ��������� � ���������
    procedure Execute; override;
    procedure SetVolume(Value: Integer);
    function GetVolume: Integer;
    procedure SetTone;
    function KanalToIndex( Kanal: Byte ): Byte;
    function IsSndEquial( Snd1, Snd2: TSoundFlags ): Boolean;

  public
    procedure ResetASD;

    constructor Create;
    destructor Destroy; override;

    procedure SetASD( Rail, Kanal: Byte; Value: Boolean );
//    procedure SetHandASD( Rail: Byte; Value: Boolean );
    procedure SetHandASD( Value: Boolean );

    procedure SoundOneChannel(Rail, Kanal: Integer);
    procedure SoundAllChannels;

    procedure StopTone;

    property Volume: Integer read GetVolume write SetVolume;

  end;

var
  Snd     : TSndControl;      // ����.

implementation
uses MainUnit;

// -----------------------------------------------------------------------------
// -------------------------  �������� �����  ----------------------------------

procedure TSndControl.Execute;
begin
Exit;
  try
  while not Terminated do
  begin
    SetTone;
    Sleep( 100 );
  end;
  except
    on E: Exception do ProcessException.RaiseException( 'SoundControl', E );
  end;
end;

function TSndControl.KanalToIndex( Kanal: Byte ): Byte;
begin
{$IFDEF Avikon16_IPV}
  if (Config.SoundedScheme = Scheme1) or (Config.SoundedScheme = Scheme2) then
    begin
      case Kanal of
        0: Result:= 0;
        1: Result:= 3;
        6, 7, 8, 9: Result:= 1;  //6, 7: Result:= 1;  //AK
        2, 3, 4, 5, 10, 11: Result:= 2;//2, 3, 8, 9, 4, 5, 10, 11: Result:= 2;
        else Result:= 0;
      end;
    end
  else if Config.SoundedScheme = Wheels then
    begin
      case Kanal of
        0, 10: Result:= 0;
        1, 11: Result:= 3;
        6, 7, 8, 9: Result:= 1;//6, 7: Result:= 1;
        2, 3, 4, 5, 12, 13: Result:= 2;//2, 3, 8, 9, 4, 5, 12, 13: Result:= 2;
        else Result:= 0;
      end;
    end;
{$ELSE}
  {$IFDEF EGO_USW}
  case Kanal of
    0, 10: Result:= 0;
    1, 11: Result:= 3;
    6, 7, 8, 9: Result:= 1;
    2, 3,  4, 5, 12, 13: Result:= 2;
    else Result:= 0;
  end;
//     Result:=0;
  {$ELSE}
  case Kanal of
    0: Result:= 0;
    1: Result:= 3;
    6, 7, 8, 9: Result:= 1;//6, 7: Result:= 1;  //AK
    2, 3, 4, 5: Result:= 2;//2, 3, 8, 9, 4, 5: Result:= 2;
    else Result:= 0;
  end;
  {$ENDIF}
{$ENDIF}
end;


{
procedure TSndControl.SetHandASD( Rail: Byte; Value: Boolean );
var
  j: Integer;

begin
  for j:= 0 to High( Snd[Rail] ) do
    Snd[Rail, j]:= False;
  Snd[Rail, 1]:= Value;
  SetTone;
end;
 }

procedure TSndControl.SetHandASD( Value: Boolean );
var
  j: Integer;

begin
  ResetASD;
  Snd[0, 1]:= Value;
  Snd[1, 1]:= Value;
  SetTone;
end;


procedure TSndControl.SetASD(Rail, Kanal: Byte; Value: Boolean);
var
  R, K: Byte;

begin
  // ������� ���.

  if FOneChannel then
  begin
    if ( FRail = Rail ) and ( FKanal = Kanal ) then
      Snd[ FRail, KanalToIndex( FKanal ) ]:= Value;
  end else
  begin

//{$IFDEF EGO_USW}   //��������� �����-� ���� � MCAN � DefCoreUnit
//    if Kanal in [0,10] then FASD[BUMtoRail(Rail), Kanal]:= False else//!!!!!!������
//    FASD[BUMtoRail(Rail), Kanal]:= Value;
//{$ELSE}
//    FASD[Rail, Kanal]:= Value;
//{$ENDIF}
{$IFDEF VMT_US}   //��������� �����-� ���� � MCAN � DefCoreUnit
    FASD[BUMtoRail(Rail), Kanal]:= Value;
{$ELSE}
{$IFDEF USK004R}
    FASD[0, Kanal]:= Value;
    FASD[1, Kanal]:= Value;
{$ELSE}
    FASD[Rail, Kanal]:= Value;
{$ENDIF}
{$ENDIF}

    // ������, ��� ��� �� ������� ������ �� ���� ( ����� -> ������� ����� )
    ResetASD;

    for R:= 0 to 1 do
    for K:= 0 to High( FASD[R] ) do
      Snd[R, KanalToIndex(K)]:= Snd[R, KanalToIndex(K)] or FASD[R, K];
  end;

  SetTone;
end;

procedure TSndControl.SetTone;
var
  i, j: Integer;
  fLeftVolume: Smallint;
  fRightVolume: Smallint;
  newvol: DWORD;
  DeviceID: Integer;
  F: Boolean;
  lr: Integer;
  B: Boolean;
  zn: Integer;


begin
//  if not Config.Sound then Exit;

  if IsSndEquial( Snd, OldSnd ) then Exit;

  // �������� ������� ������.
  F:= False;
  for i:= 0 to 1 do
  for j:= 0 to High( Snd[i] ) do
    F:= F or Snd[i, j];

  OldSnd:= Snd;

  if not F then
  begin
    StopTone;
    Exit;
  end;

  Wave.rId:= $46464952;       { 'RIFF' }
  Wave.rLen:= 2 * SampleLength + 36;  { length of sample + format }
  Wave.wId:= $45564157;       { 'WAVE' }
  Wave.fId:= $20746d66;       { 'fmt ' }
  Wave.fLen:= 16;             { length of format chunk }
  Wave.wFormatTag:= 1;        { PCM data }
  Wave.nChannels:= 2;         { mono/stereo }
  Wave.nSamplesPerSec:= 20000;{ sample rate }
  Wave.nAvgBytesPerSec:= 2 * 20000 * 1;
  Wave.nBlockAlign:= 2; // channels * (resolution div 8);
  Wave.wBitsPerSample:= 8;{ resolution 8/16 }
  Wave.dId:= $61746164;       { 'data' }
  Wave.wSampleLength:= 2 * SampleLength;         { sample size }

  for lr:= 0 to 1 do
  for I:= 1 to SampleLength do
  begin
    if Snd[lr,3] then
      B:= I > SampleLength div 2
    else
      B:= Snd[lr,0];

    zn:= Ord(B) + Ord(Snd[lr,1]) + Ord(Snd[lr,2]);

    if zn = 0 then
      Wave.Data[I, lr+1]:= 127
    else
      Wave.Data[I, lr+1]:= Round( FAmp * (1 + (
        Ord(B) * Sin(2 * Pi * (I - 1) / 40) +
        Ord(Snd[lr,1]) * Sin(2 * Pi * (I - 1) / 20) +
        Ord(Snd[lr,2]) * Sin(2 * Pi * (I - 1) / 10)
          ) / zn ));
  end;

  fLeftVolume:= 100;
  fRightVolume:= 100;



//{$R-}
//  newvol:=(($ffff * fLeftVolume) div 100)+((($ffff * fRightVolume) div 100) shl $10);
//{$R+}
//  DeviceID:= 0;
//  waveOutSetVolume(DeviceID, newvol);
  PlaySound(nil, 0, 0);
  PlaySound(@Wave, 0, SND_ASYNC + SND_MEMORY + SND_LOOP);
end;

procedure TSndControl.StopTone;
begin
  PlaySound(nil, 0, 0);
end;

constructor TSndControl.Create;
begin
  FSoundOn:=True;
  FOneChannel:= False;
  FAmp:=MaxAmp;
  inherited Create( True );
end;

destructor TSndControl.Destroy;
begin
  inherited;
  StopTone;
end;



procedure TSndControl.SetVolume(Value: Integer);
var
  S: TSoundVolume;
begin
  S.Left:= Min( Max( 0, Value ), 100 );
  S.Right:= S.Left;
  SetSoundVolume( S );
end;

function TSndControl.GetVolume: Integer;
var
  S: TSoundVolume;
begin
  Result:= GetSoundVolume.Left;
end;

procedure TSndControl.SoundAllChannels;
begin
  FOneChannel:= False;
  ResetASD;
end;

procedure TSndControl.SoundOneChannel(Rail, Kanal: Integer);
begin
  FRail:= Rail;
  FKanal:= Kanal;
  FOneChannel:= True;
  ResetASD;
end;

procedure TSndControl.ResetASD;
var
  i, j: Integer;

begin
  for i:= 0 to 1 do
  for j:= 0 to High( Snd[i] ) do
    Snd[i, j]:= False;
end;

function TSndControl.IsSndEquial(Snd1, Snd2: TSoundFlags): Boolean;
var
  i, j: Integer;

begin
  Result:= True;
  for i:= 0 to 1 do
    for j:= 0 to High( Snd1[i] ) do
      if Snd1[i, j] <> Snd2[i, j] then begin Result:= False; exit; end;
end;


end.
