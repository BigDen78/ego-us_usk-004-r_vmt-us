unit Unit2;
{$I Directives.inc}

{$M 163840,1048576}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, MainUnit, SkinSupport, sSkinManager, sButton,
  ComCtrls, sComboBox, ShellAPI,
  sLabel;

type
  TMainFrmAV14 = class(TForm)
    Panel1: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShortCut(var Msg: TWMKey; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ExitProgram;
    procedure RebootProgram;
  public
    { Public declarations }
  end;

var
  MainFrmAV14: TMainFrmAV14;

implementation

{$R *.dfm}

procedure TMainFrmAV14.ExitProgram;
begin
  Self.Close;
end;

procedure TMainFrmAV14.RebootProgram;
var
  Path, FSelfExeName, ExecCommand: string;
begin
  Path:= ExtractFilePath(Application.ExeName);
  FSelfExeName:= ExtractFileName(Application.ExeName);
  ExecCommand:= Path+ 'ReBootUtility.exe';
  ShellExecute(Self.Handle, nil, PChar (ExecCommand), PChar(Self.Name + ' ' + FSelfExeName), nil, SW_SHOW);
  Self.Close;
end;

procedure TMainFrmAV14.FormCreate(Sender: TObject);
begin
  Self.DoubleBuffered := true;
  Self.Panel1.ParentDoubleBuffered := false;
  Self.Panel1.DoubleBuffered := true;

  Self.ControlStyle := Self.ControlStyle - [csOpaque];
  Self.Top := 0;
  Self.Left := 0;
  Self.BorderStyle := bsNone;
  Self.Width := Screen.Width;
  Self.Height := Screen.Height;
  Self.Panel1.ControlStyle := Self.Panel1.ControlStyle - [csOpaque];

  DecimalSeparator:= '.';


  General := TGeneral.Create(Self);

  General.PixelFormat := pf32bit;
  General.Panel := Panel1;
  General.OnExitProgram := ExitProgram;
  General.OnRebootProgram := RebootProgram;

  Self.KeyPreview:=False;
end;

procedure TMainFrmAV14.FormDestroy(Sender: TObject);
begin
  if Assigned(General) then
    General.Free;
  {$IFDEF ShutDown}
    ShellExecute(hWnd_, 'open', PChar('shutdown'), PChar('/s /t 00'), nil,
      SW_NORMAL);
  {$ENDIF}
end;

procedure TMainFrmAV14.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Assigned(General) and Assigned(General.Core) then
    if (Key = 114) and General.Core.BoltJointMode then
      General.Core.BoltJointMode := false;
end;

procedure TMainFrmAV14.FormShortCut(var Msg: TWMKey; var Handled: Boolean);
begin
  if Assigned(General) then
  begin
{$IFDEF EnableScreenShot}
    if Msg.CharCode = 113 then begin
      General.MakeScreenShot;
      Handled := true;
    end;
{$ENDIF}
    if Msg.CharCode = 112 then begin
      General.ShowHelp;
      Handled := true;
    end;
  if Assigned(General.Core) then
  begin
{$IFDEF TESTKEYS}
    // ��� �������� ������������
    if (Msg.CharCode = Ord('z')) or (Msg.CharCode = Ord('Z')) or
       (Msg.CharCode = Ord('�')) or (Msg.CharCode = Ord('�')) then
      begin
        General.Core.LoadExternFile('D:\TestFile.a16');
      end;
    if (Msg.CharCode = VK_RIGHT) then
      begin
        General.Core.NextExternCoord(40);
      end;
{$ENDIF}
    General.OnKeyPressed(Msg.CharCode, Handled );
  end;

{$IFDEF TESTKEYS}
  // ���� �����-����� !!! ��������� ������ ������������ ����� !!!
   if Assigned(General.Core) then
    begin
      if ((Msg.CharCode >= Ord('0')) and (Msg.CharCode <= Ord('9'))) then
        begin
          General.TestMnemoDelay:=0;
          General.TestMnemoKanal:=StrToInt(Chr(Msg.CharCode));
        end
      else if (Msg.CharCode = Ord('q')) or (Msg.CharCode = Ord('Q')) or(Msg.CharCode = Ord('�')) or (Msg.CharCode >= Ord('�')) then
        begin
          General.TestMnemoDelay:=0;
          General.TestMnemoKanal:=10;
        end
      else if (Msg.CharCode = Ord('w')) or (Msg.CharCode = Ord('W')) or (Msg.CharCode = Ord('�')) or (Msg.CharCode = Ord('�')) then
        begin
          General.TestMnemoDelay:=0;
          General.TestMnemoKanal:=11;
        end
      else if (Msg.CharCode = Ord('e')) or (Msg.CharCode = Ord('E')) or (Msg.CharCode = Ord('�')) or (Msg.CharCode = Ord('�')) then
        begin
          General.TestMnemoDelay:=0;
          General.TestMnemoKanal:=12;
        end
      else if (Msg.CharCode = Ord('r')) or (Msg.CharCode = Ord('R')) or (Msg.CharCode = Ord('�')) or (Msg.CharCode = Ord('�')) then
        begin
          General.TestMnemoDelay:=0;
          General.TestMnemoKanal:=13;
        end;

      if Msg.CharCode = VK_LEFT then
        General.TestMnemoDelay:=General.TestMnemoDelay-1;
      if Msg.CharCode = VK_RIGHT then
        General.TestMnemoDelay:=General.TestMnemoDelay+1;
      if (Msg.CharCode = VK_TAB) then
        begin
          if General.TestMnemoNit=1 then
            General.TestMnemoNit:=0
          else
            General.TestMnemoNit:=1;
        end;
      General.TestMnemo;
    end;
{$ENDIF}
  end;

//  Handled := true;
end;

procedure TMainFrmAV14.FormShow(Sender: TObject);
begin
  if Assigned(General) then
    General.Boot;
end;
{procedure TMainFrmAV14.FormContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
var
  Control: TControl;
begin
  Control := FindVCLWindow(Mouse.CursorPos) ;
  if Control = Form1 then begin
    PopupMenu1.Popup(Mouse.CursorPos.X, Mouse.CursorPos.Y);
    Handled := True;
  end;
end;
}
end.
