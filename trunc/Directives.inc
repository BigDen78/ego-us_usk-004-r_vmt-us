
{ $DEFINE UseHardware}

{ $DEFINE Debug}
{ $DEFINE ShutDown}
{$DEFINE EnableScreenShot}
{ $DEFINE OnlyEnglish}
{ $DEFINE OnlyRussian}
{ $DEFINE SecondUSBRemotePanel}
{$DEFINE PainterInterfaceOn}
{$DEFINE NoHelp}
{$ASSERTIONS ON}

{ $DEFINE TestSensor}

// ------- ��� �������, ���� ������ �� ������������ ������� � �� ������������� � �����
// ------- �������� !!! ������ ������������ ini-������

{ $DEFINE TrueAV14} // ���t���-14 ��� ������, ������ ���������� �����


// --------

{$DEFINE Export}
{ $DEFINE OldEngine}

{ $DEFINE UseA11Panel}

// ���� �������
{ $DEFINE Avikon11}
{ $DEFINE Avikon12}
{ $DEFINE Avikon14}
{ $DEFINE Avikon14_2Block}
{ $DEFINE Avikon15}
{ $DEFINE Avikon15RSP}
{ $DEFINE Avikon16_IPV}
{ $DEFINE EGO_USW}
{ $DEFINE USK004R}
{$DEFINE VMT_US}


{ $DEFINE RKS_U} //��� �������� �������� 2013 (������ �������� ������ - ��������������)

// -------------------------

{$IFDEF Avikon11}
  {$DEFINE TwoThreadsDevice}
  {$DEFINE BUMCOUNT1}
  {$DEFINE SimpleMnemo}
{$ENDIF}

{$IFDEF Avikon12}
  {$DEFINE BUMCOUNT1}
{$ENDIF}

{$IFDEF Avikon14}
  {$DEFINE TwoThreadsDevice}
  {$DEFINE BUMCOUNT1}
  {$DEFINE DemoVersion}
  {$DEFINE SimpleMnemo}
{$ENDIF}

{$IFDEF Avikon14_2Block}
  {$DEFINE TwoThreadsDevice}
  {$DEFINE BUMCOUNT2}
  {$DEFINE INVERT_HAND_RELE}
{$ENDIF}

{$IFDEF Avikon15}
  {$DEFINE BUMCOUNT1}
{$ENDIF}

{$IFDEF Avikon15RSP}
  {$DEFINE BUMCOUNT1}
{$ENDIF}

{$IFDEF USK004R}
//  {$DEFINE Avikon16_IPV}
//  {$DEFINE TwoThreadsDevice}
  {$DEFINE BUMCOUNT2}
  {$DEFINE SimpleMnemo}
  {$DEFINE GPS}
  {$DEFINE TURKISH_COORD_WITH_DOT} // ������� ���������� � ������� ����.���
  {$DEFINE INPUTSTR_IN_LISTWIN} // ���������� ��������� � ������ ����� ������ ����� � ����. �����������
  {$DEFINE THIN_CHANBUT} // ����������� ������ ������ �������
//  {$DEFINE HEADCHECKING}
{$ENDIF}

{$IFDEF Avikon16_IPV}
  {$DEFINE TwoThreadsDevice}
  {$DEFINE BUMCOUNT2}
//  {$DEFINE BUMCOUNT4}
  {$DEFINE SimpleMnemo}
  {$DEFINE PAINTER} // �����������
{$ENDIF}

{$IFDEF EGO_USW}
  {$DEFINE TwoThreadsDevice}
  {$DEFINE BUMCOUNT4}
  {$DEFINE SimpleMnemo}
  {$DEFINE PAINTER} // �����������
  {$DEFINE INPUTSTR_IN_LISTWIN}
//  {$DEFINE TESTING_COORD} //!!!!!!!!! FULL COORD in BUM and AviconContainer
//  {$DEFINE FULL_BUM_COORD} //??? don`t use
//  {$DEFINE TESTKEYS} // �������� ������� �� ������� ������
{$ENDIF}

{$IFDEF VMT_US}
  {$DEFINE TwoThreadsDevice}
  {$DEFINE BUMCOUNT4}
  {$DEFINE SimpleMnemo}
  {$DEFINE PAINTER} // �����������
  {$DEFINE INPUTSTR_IN_LISTWIN}
//  {$DEFINE TESTING_COORD} //!!!!!!!!! FULL COORD in BUM and AviconContainer
//  {$DEFINE FULL_BUM_COORD} //??? don`t use
//  {$DEFINE TESTKEYS} // �������� ������� �� ������� ������
{$ENDIF}

{$IFDEF RKS_U}
  {$DEFINE BUMCOUNT1}
{$ENDIF}

{ $DEFINE UseLog}

{$IFDEF Debug}
  { $ DEFINE UseLog}
  { $DEFINE SHOWASD}
  { $DEFINE TestSend}  // ������� �������� ������.
  { $DEFINE SensorTest} // ������� �������� ������� � �����.
  { $BScanLinePlot}
  { $DEFINE TESTKEYS} // �������� ������� �� ������� ������
{$ENDIF}
