object MessageForm: TMessageForm
  Left = 449
  Top = 299
  AlphaBlend = True
  BorderIcons = []
  BorderStyle = bsNone
  ClientHeight = 150
  ClientWidth = 600
  Color = clMaroon
  TransparentColor = True
  TransparentColorValue = clMaroon
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClick = FormClick
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 600
    Height = 150
    Align = alClient
    BevelOuter = bvNone
    Color = clMaroon
    TabOrder = 0
    OnClick = Panel1Click
    OnMouseUp = Panel1MouseUp
    object Shape1: TShape
      Left = 0
      Top = 0
      Width = 600
      Height = 49
      Align = alTop
      Brush.Color = clOlive
      Pen.Width = 5
      Shape = stRoundRect
      OnMouseUp = Shape1MouseUp
    end
    object Label1_old: TLabel
      Left = 40
      Top = 9
      Width = 477
      Height = 29
      Alignment = taCenter
      Caption = #1051#1077#1074#1099#1081' '#1088#1077#1083#1100#1089' 180 '#1084#1084', '#1087#1088#1072#1074#1099#1081' - '#1085#1077#1090' '#1089#1080#1075#1085#1072#1083#1072
      Color = clBlack
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -24
      Font.Name = 'Impact'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
      Visible = False
      WordWrap = True
      OnClick = Label1_oldClick
      OnMouseUp = Label1_oldMouseUp
    end
    object Label1: TsLabel
      Left = 0
      Top = 56
      Width = 66
      Height = 29
      Alignment = taCenter
      AutoSize = False
      Caption = 'Label1'
      Color = clBlack
      ParentColor = False
      ParentFont = False
      WordWrap = True
      OnMouseUp = Label1MouseUp
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -24
      Font.Name = 'Impact'
      Font.Style = [fsBold]
      UseSkinColor = False
    end
    object PaintBox1: TPaintBox
      Left = 200
      Top = 55
      Width = 105
      Height = 105
    end
  end
  object WaitTimer: TTimer
    Enabled = False
    OnTimer = WaitTimerTimer
    Left = 64
    Top = 8
  end
  object FadeTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = FadeTimerTimer
    Left = 120
    Top = 8
  end
end
