program Avicon14;

uses
  Forms,
  Dialogs,
  PublicFunc,
  MessageUnit,
  Unit2 in 'Unit2.pas' {MainFrmAV14},
  MainUnit in 'Interface\MainUnit.pas',
  DefCoreUnit in 'Core\DefCoreUnit.pas',
  MCAN in 'Core\MCAN.pas',
  AScanUnit in 'Interface\AScanUnit.pas',
  BScanUnit in 'Interface\BScanUnit.pas',
  MnemoUnit in 'Interface\MnemoUnit.pas',
  FileTransfer in '..\Delphy.PublicLibraries\FileTransfer.pas',
  BScanThreadUnit in 'Core\BScanThreadUnit.pas',
  ProgramProfileConfig in 'Interface\ProgramProfileConfig.pas',
  UtilsUnit in 'Interface\UtilsUnit.pas',
  CustomInterfaceUnit in 'Interface\CustomInterfaceUnit.pas',
  MainMenuUnit in 'WindowClasses\MainMenuUnit.pas',
  CustomWindowUnit in 'WindowClasses\CustomWindowUnit.pas',
  ConfigObj in 'Core\ConfigObj.pas',
  AScanFrameUnit in 'WindowClasses\AScanFrameUnit.pas',
  MnemoFrameUnit in 'WindowClasses\MnemoFrameUnit.pas',
  GPSModule in 'Components\GPSModule.pas';

{$R *.res}

begin
  if TestProgramAlreadyStart('Avikon14') or
   TestProgramAlreadyStart('Avikon16') or
   TestProgramAlreadyStart('EGOUS') or
   TestProgramAlreadyStart('EGO-USW') then begin
//     ShowMessage(General.FLanguageTable.Caption['��������� ��� ��������']);
     ShowMessage('The program is already running');
     Exit;
   end;

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainFrmAV14, MainFrmAV14);
  Application.CreateForm(TMessageForm, MessageForm);
  Application.Run;
end.
