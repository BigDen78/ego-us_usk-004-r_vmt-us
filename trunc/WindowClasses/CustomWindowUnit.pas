//////////////////////////////////////////////////////////////////
///                                                            ///
///  Unit ����������� ������ �� ������ ������� ��������� ����  ///
///   (�������� �� MainUnit , ����� ��������� GeneralUnit      ///
///                                                            ///
//////////////////////////////////////////////////////////////////


unit CustomWindowUnit;

interface

uses
  ExtCtrls, CfgTablesUnit_2010, Classes, Forms, Controls, Graphics, StdCtrls,
  SkinSupport, Dialogs,
  UtilsUnit, ConfigObj;

const
  DefaultFontName = 'Impact';

type

  TPanelOrientation = (poVertical, poHorizontal);
  TControlAlign = (caNone, caLeft, caRight, caWholeSize, caTop, caBottom);

  TCustomWindow = class(TPanel)
  private
    FID: string;
  protected
    Obj: TList;
    FTimer: TTimer;
    procedure OnTimer(Sender: TObject); virtual;
  public
    FLT: TLanguageTable;
    FCT: TColorTable;
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
      virtual;
    destructor Destroy; override;
    procedure Open; virtual;
    procedure Hide; virtual;
    property id: string read FID write FID;
  end;

  TControlList = class(TList)
  private
    function Get(Index: integer): TControl;
    procedure Put(Index: integer; Item: TControl);
  public
    property Items[Index: integer]: TControl read Get write Put; default;
  end;

  TControlPanel = class(TPanel)
  private
    FOrientation: TPanelOrientation;
    FControlSize: integer;
    FControlAlign: TControlAlign;
    FID: string;
    procedure Clear;
    procedure RecountControls;
    procedure Resize; override;
  protected
    FControlList: TControlList;
    procedure RecountControl(Control: TControl; Left, Top, Width,
      Height: integer); virtual;
    procedure SetOrientation(Value: TPanelOrientation);
    procedure SetControlAlignment(Value: TControlAlign);
    function GetItemByID(id: string): TControl;
  public
    constructor Create(_ID: string; PanType: TPanelOrientation;
      ControlAlign: TControlAlign; ControlSize: integer);
    destructor Destroy; override;
    function Add(Control: TControl): TControl;
    procedure CResize;

    property id: string read FID;
    property Orientation: TPanelOrientation read FOrientation write
      SetOrientation;
    property ControlAlign: TControlAlign read FControlAlign write
      SetControlAlignment;
    property Item[id: string]: TControl read GetItemByID;
  end;

  TButtonPanel = class(TControlPanel)
  protected
    procedure RecountControl(Control: TControl; Left, Top, Width,
      Height: integer); override;
  public
    FontKoef: Single;
    constructor Create(_ID: string; PanType: TPanelOrientation;
      ControlAlign: TControlAlign; ControlSize: integer); overload;
    function AddButton(Caption: string; _OnClick: TNotifyEvent): TButton;
      overload;
    function AddButton(id, Caption: string; _OnClick: TNotifyEvent): TButton;
      overload;
    procedure EnableButtons;
    procedure DisableButtons;
  end;


  TControlPanelManager = class
  private
    FParent: TWinControl;
    FCT: TColorTable;
    FLT: TLanguageTable;
    FPanList: TList;
    FColor: TColor;
  protected
    procedure SetColor(Value: TColor);
    function GetPanel(id: string): TControlPanel;
    function GetButtonPanel(id: string): TButtonPanel;
  public
    constructor Create(Parent: TWinControl; CT: TColorTable;
      LT: TLanguageTable);
    destructor Destroy; override;
    function AddPanel(Panel: TControlPanel): TControlPanel;
    procedure SwitchPanel(id: string);
    property Color: TColor read FColor write SetColor;
    property Panel[id: string]: TControlPanel read GetPanel;
    property ButtonPanel[id: string]: TButtonPanel read GetButtonPanel;
  end;

  TWindowMode = class;

  TPaneledWindow = class(TCustomWindow)
  private
    FCMode: TWindowMode;
    procedure SetCMode(Mode: TWindowMode);
  public
    PanelManager: TControlPanelManager;
    property CMode: TWindowMode read FCMode write SetCMode;
  end;

  TWindowMode = class
  public
    FParent: TPaneledWindow;
    CPanel: TControlPanel;
    constructor Create(Parent: TPaneledWindow);
    destructor Destroy; override;
    procedure SetActive; virtual;
  end;

implementation

{ TCustomWindow }

constructor TCustomWindow.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);
begin
  inherited Create(nil);
  Self.Visible := false;
  Self.Width := Screen.Width;
  Self.Height := Screen.Height;
  Self.Caption := '��������...';
  FTimer := TTimer.Create(nil);
  FTimer.Interval := 10;
  FTimer.Enabled := false;
  FTimer.OnTimer := OnTimer;

  Self.Parent := Panel;
  Self.ParentColor := false;
  Self.ParentDoubleBuffered := false;
  Self.DoubleBuffered := true;
  Self.ControlStyle := Self.ControlStyle - [csOpaque];
  Self.BevelOuter := bvNone;
  Self.Align := alClient;
  FCT := CT;
  FLT := LT;
  Obj := TList.Create;
  Self.Color := FCT.Color['Main_BackGr'];
  Self.Visible := false;
end;

destructor TCustomWindow.Destroy;
var
  i: integer;
begin
  FTimer.Enabled := false;
  FTimer.Free;
  for i := 0 to Obj.Count - 1 do
    TControl(Obj.Items[i]).Free;
  Obj.Free;
  inherited;
end;

procedure TCustomWindow.OnTimer(Sender: TObject);
begin
  //
end;

procedure TCustomWindow.Open;
begin
  Self.BringToFront;
  Self.FTimer.Enabled := true;
  Self.Visible := true;
end;

procedure TCustomWindow.Hide;
begin
  Self.FTimer.Enabled := false;
end;

{ TControlList }

function TControlList.Get(Index: integer): TControl;
begin
  Result := inherited Get(Index);
end;

procedure TControlList.Put(Index: integer; Item: TControl);
begin
  inherited Put(Index, Item);
end;

{ TControlPanel }

constructor TControlPanel.Create(_ID: string; PanType: TPanelOrientation;
  ControlAlign: TControlAlign; ControlSize: integer);
begin
  inherited Create(nil);
  Self.Align := alClient;
  FOrientation := PanType;
  FControlAlign := ControlAlign;
  FControlSize := ControlSize;
  FControlList := TControlList.Create;
  FID := _ID;
end;


destructor TControlPanel.Destroy;
begin
  Clear;
  FControlList.Free;
  inherited;
end;

procedure TControlPanel.CResize;
begin
   Resize;
end;

procedure TControlPanel.Clear;
var
  i: integer;
begin
  for i := 0 to FControlList.Count - 1 do
    FControlList[i].Free;
end;

procedure TControlPanel.RecountControls;
const
  HLeftMargin = 0.01;
  HTopMargin = 0.1;
  VLeftMargin = 0.05;
  VTopMargin = 0.01;

var
  i, w, H, BCount, BTop, BLeft, BSpace, LeftDelta, TopDelta: integer;

begin
  if FControlAlign = caNone then
    Exit;
  BCount := FControlList.Count;
  if BCount > 0 then
    case FOrientation of
      poVertical:
        begin
          w := Round(Self.Width - Self.Width * VLeftMargin * 2);
          H := Round((Self.Height - Self.Height * VTopMargin * 2)
              * FControlSize / 100);
          BLeft := Round(Self.Width * VLeftMargin);
          LeftDelta := 0;
          case FControlAlign of
            caTop:
              begin
                BTop := Round(Self.Height * VTopMargin);
                BSpace := BTop;
              end;
            caBottom:
              begin
                BSpace := Round(Self.Height * VTopMargin);
                BTop := Round(Self.Height - (BSpace + H) * BCount);
              end;
            caWholeSize:
              begin
                BSpace := Round((Self.Height - H * BCount) / (BCount + 1));
                BTop := BSpace;
              end;
          end;
          TopDelta := H + BSpace;

        end;
      poHorizontal:
        begin
          BTop := Round(Self.Height * HTopMargin);
          TopDelta := 0;
          w := Round((Self.Width - Self.Width * HLeftMargin * 2)
              * FControlSize / 100);
          H := Round(Self.Height - Self.Height * HTopMargin * 2);
          case FControlAlign of
            caLeft:
              begin
                BLeft := Round(Self.Width * HLeftMargin);
                BSpace := BLeft;
              end;
            caRight:
              begin
                BSpace := Round(Self.Width * HLeftMargin); ;
                BLeft := Round(Self.Width - (BSpace + w) * BCount);
              end;
            caWholeSize:
              begin
                BSpace := Round((Self.Width - w * BCount) / (BCount + 1));
                BLeft := BSpace;
              end;
          end;
          LeftDelta := w + BSpace;
        end;
    end;
  for i := 0 to BCount - 1 do
  begin
    RecountControl(FControlList[i], BLeft, BTop, w, H);
    BLeft := BLeft + LeftDelta;
    BTop := BTop + TopDelta;
  end;
end;

procedure TControlPanel.Resize;
begin
  inherited;
  RecountControls;
end;

function TControlPanel.Add(Control: TControl): TControl;
begin
  Control.Parent := Self;
  if Control is TPanel then
    TPanel(Control).Color := Color;
  FControlList.Add(Control);
  RecountControls;
  Result := Control;
end;

procedure TControlPanel.SetOrientation(Value: TPanelOrientation);
begin
  FOrientation := Value;
  RecountControls;
end;

function TControlPanel.GetItemByID(id: string): TControl;
var
  i: integer;
begin
  Result := nil;
  for i := 0 to FControlList.Count - 1 do
    if TIDController.GetID(FControlList[i]) = id then
      Result := FControlList[i];
end;

procedure TControlPanel.RecountControl(Control: TControl; Left, Top, Width,
  Height: integer);
begin
  Control.Left := Left;
  Control.Top := Top;
  Control.Width := Width;
  Control.Height := Height;
end;

procedure TControlPanel.SetControlAlignment(Value: TControlAlign);
begin
  FControlAlign := Value;
  RecountControls;
end;

{ TButtonPanel }

function TButtonPanel.AddButton(Caption: string; _OnClick: TNotifyEvent)
  : TButton;
var
  NewBut: TButton;
begin
  NewBut := ControlFactory.CreateButton(Self);
  NewBut.OnClick := _OnClick;
  NewBut.Font.Name := DefaultFontName;
  NewBut.Font.Color := clBlack; ;
  NewBut.Caption := Caption;
  Add(NewBut);
  Result := NewBut;
end;

function TButtonPanel.AddButton(id, Caption: string; _OnClick: TNotifyEvent)
  : TButton;
begin
  Result := AddButton(Caption, _OnClick);
  TIDController.SetID(Result, id);
end;

constructor TButtonPanel.Create(_ID: string; PanType: TPanelOrientation;
  ControlAlign: TControlAlign; ControlSize: integer);
begin
  inherited;
  FontKoef := 0.7;
  if Config.Language = 'French' then
    FontKoef := 0.5;
  if Config.Language = 'Spanish' then
    FontKoef := 0.5;
end;

procedure TButtonPanel.DisableButtons;
var
  i: integer;
begin
  for i:=0 to FControlList.Count -1 do
     FControlList[i].Enabled:= false;
end;

procedure TButtonPanel.EnableButtons;
var
  i: integer;
begin
    for i:=0 to FControlList.Count -1 do
     FControlList[i].Enabled:= true;
end;

procedure TButtonPanel.RecountControl(Control: TControl; Left, Top, Width,
  Height: integer);
begin
  inherited;
  if Control is TButton then
    TButton(Control).Font.Height := Round(Height * FontKoef);
end;

{ TControlPanelManager }

function TControlPanelManager.AddPanel(Panel: TControlPanel): TControlPanel;
begin
  Panel.Color := FColor;
  Panel.Parent := FParent;
  Panel.BevelInner := bvNone;
  Panel.BevelOuter := bvNone;
  Panel.Ctl3D := false;
  Panel.Visible := true;
  FPanList.Add(Panel);
  Result := Panel;
end;

function TControlPanelManager.GetPanel(id: string): TControlPanel;
var
  i: integer;

begin
  Result := nil;
  for i := 0 to FPanList.Count - 1 do
    if TControlPanel(FPanList[i]).id = id then
      Result := TControlPanel(FPanList[i]);
end;

function TControlPanelManager.GetButtonPanel(id: string): TButtonPanel;
var
  i: integer;

begin
  Result := nil;
  for i := 0 to FPanList.Count - 1 do
    if TControlPanel(FPanList[i]).id = id then
      if TControlPanel(FPanList[i]) is TButtonPanel then
        Result := TButtonPanel(FPanList[i]);
end;

constructor TControlPanelManager.Create(Parent: TWinControl; CT: TColorTable;
  LT: TLanguageTable);
begin
  FParent := Parent;
  FCT := CT;
  FLT := LT;
  FPanList := TList.Create;
end;

destructor TControlPanelManager.Destroy;
var
  i: integer;
begin
  for i := 0 to FPanList.Count - 1 do
    TButtonPanel(FPanList.Items[i]).Free;
  FPanList.Free;
  inherited;
end;

procedure TControlPanelManager.SetColor(Value: TColor);
var
  i: integer;
begin
  if FColor <> Value then
  begin
    FColor := Value;
    for i := 0 to FPanList.Count - 1 do
      TButtonPanel(FPanList.Items[i]).Color := FColor;
  end;
end;

procedure TControlPanelManager.SwitchPanel(id: string);
var
  i: integer;
begin
  for i := 0 to FPanList.Count - 1 do
    if TControlPanel(FPanList.Items[i]).id = id then
      TControlPanel(FPanList.Items[i]).BringToFront;
end;

{ TPaneledWindow }

procedure TPaneledWindow.SetCMode(Mode: TWindowMode);
begin
  FCMode := Mode;
  FCMode.SetActive;
end;

{ TWindowMode }

constructor TWindowMode.Create(Parent: TPaneledWindow);
begin
  FParent := Parent;
  CPanel := nil;
end;

destructor TWindowMode.Destroy;
begin
  CPanel.Free;
  inherited;
end;

procedure TWindowMode.SetActive;
begin
  FParent.PanelManager.SwitchPanel(CPanel.ID);
end;

end.
