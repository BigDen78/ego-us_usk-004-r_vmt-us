//////////////////////////////////////////////////////////////////
///                                                            ///
///            Unit ����������� ������� ����                   ///
///              � �� �� ���� ��� ���������                    ///
///                                                            ///
//////////////////////////////////////////////////////////////////

unit MainMenuUnit;

interface

{$I Directives.inc}

uses
  StdCtrls, Controls, Dialogs, ExtCtrls, ComCtrls, Classes,
  Forms, Graphics, Windows,SysUtils, Math,
  CfgTablesUnit_2010, SkinSupport,
  AviconTypes, AviconDataSource,
  CustomWindowUnit, UtilsUnit, ConfigObj, DefCoreUnit, PublicFunc,
  ProgramProfileConfig, BScanUnit, MessageUnit, FileTransfer, MCan,
  EjectUSB,PublicData;

type

  TMainMenuWindow = class;
//  TBootFlashMenu = class;

  TMainMenuModeReg = class(TWindowMode)
  private
    KM: TRegNumber;
    Pk: TRegNumber;
    Metr: TRegNumber;
    Oper: TRegComboBox;
    // Code: TRegNum;
    Way: TRegComboBox;
    Side: TRegComboBox;
    Dir: TRegComboBox;
    BottomPanel: TPanel;
    RegOn, RegOff: TButton;
    FIsOn: Boolean;

    // ���������� ��������� �����������
    TrackDir: TRegComboBox;
    OperEng: TCustomInputItem;
    Line: TCustomInputItem;
    TrackNum: TRegNumber;
    Chainage: TRegChainage;
    DirEng: TRegComboBox;
    TrackID: TRegComboBox;

    FPeregonBut: TRegButton;
    FOperatorBut: TRegButton;

    FPeregonString: string;
    FOperatorString: string;

    procedure SetIsOn(B: Boolean);
    procedure OnRegOn(Sender: TObject);
    procedure OnRegOff(Sender: TObject);

    procedure ShowPeregonListWin(Sender: TObject);
    procedure OnPeregonSelected(id: string);

    procedure ShowOperatorListWin(Sender: TObject);
    procedure OnOperatorSelected(id: string);
    procedure DirChanged(Sender: TObject);

    procedure FillRusSettings; // ��������� ����������� ������� �������
//    procedure FillTurkishSettings; // ��������� ����������� �������� �������
    procedure FillAnotherSettings; // ��������� ����������� ������ ������
  public
    constructor Create(MainMenu: TMainMenuWindow);
    procedure SetActive; override;
    property IsOn: Boolean read FIsOn write SetIsOn;
  end;

  TMainMenuModeAdj = class(TWindowMode)
  private
    procedure OnAdjKan(Sender: TObject);
    procedure OnAdjDPBut(Sender: TObject);
    procedure OnAdjTableBut(Sender: TObject);
    procedure OnAdjSoftBut(Sender: TObject);
    procedure ItemSelected(id: string);
  public
    constructor Create(MainMenu: TMainMenuWindow);
    procedure SetActive; override;
  end;

  TMainMenuModeInfo = class(TWindowMode)
  private
    RegStat: TRegValue;
    AKBUI: TRegValue;
    AKBUM: TRegValue;
    SNBUI: TRegValue;
    SNBUM: TRegValue;
    POBUI: TRegValue;
    POBUM: TRegValue;
    U2CSN: TRegValue;
  {$IFDEF BUMCOUNT2}
    SNBUM2: TRegValue;
    POBUM2: TRegValue;
  {$ELSE}
   {$IFDEF BUMCOUNT4}
    SNBUM2: TRegValue;
    POBUM2: TRegValue;
    U2CSN2: TRegValue;
    SNBUM3: TRegValue;
    POBUM3: TRegValue;
    SNBUM4: TRegValue;
    POBUM4: TRegValue;
   {$ENDIF}
  {$ENDIF}
  {$IFDEF USK004R}
    U2CVer: TRegValue;
  {$ENDIF}
  {$IFDEF GPS}
    SLatitude: TRegValue;
    SLongitude: TRegValue;
  {$ENDIF}

    CURScheme: TRegValue;
    OpenFile: TRegButton;
    FRegFileName: string;
    procedure OnOpenFilePress(Sender: TObject);
    procedure FileSelected(id: string);
    procedure OpenFileButPress(Sender: TObject);
  public
    constructor Create(MainMenu: TMainMenuWindow);
    procedure SetActive; override;
  end;

  TMainMenuModeFlashInProgress = class(TWindowMode)
  private
    Lb: TCustomLabel;
    PB: TProgressBar;
    But: TButton;
    PrevMode: TWindowMode;
    IsCancel: Boolean;
    FProgress: integer;
    ProgressTimer: TTimer;
    procedure OnCancel(Sender: TObject);
    procedure OnProgress(Progress: integer);
    procedure OnTimer(Sender: TObject);
  public
    constructor Create(MainMenu: TMainMenuWindow);
    destructor Destroy; override;
    procedure SetActive; override;
    procedure Transfer(FlashLetter: Char; MoveFiles: Boolean);
    procedure ExportSettings(FlashLetter: Char);
    procedure ImportSettings(FileName: string);
    procedure FlashLoosed;
  end;

  TMainMenuModeFlash = class(TWindowMode)
  private
    FlashLetter: Char;
    PrevMode: TWindowMode;
    ModeHaveSet: Boolean;
    ModeProgress: TMainMenuModeFlashInProgress;

    procedure OnCopy(Sender: TObject);
    procedure OnMove(Sender: TObject);
    procedure OnNastrToFlash(Sender: TObject);
    procedure OnFromFlashToNastr(Sender: TObject);
    procedure OnFirmareSelected(id: string);
  public
    constructor Create(MainMenu: TMainMenuWindow);
    procedure SetActive; override;
    procedure LookForFlash;
  end;


  TMainMenuWindow = class(TPaneledWindow)
  strict private
    function CreatePrototypeButton : TButton; // ������� ������ �������� ������

    procedure CreateButtons;
    procedure CreateTop;
    procedure CreateBottom;

    procedure CreateButtonsNoHardware;//F!
  private
    FGP: TGroupBox;
    ModeAdj: TMainMenuModeAdj;      // ���������
    ModeInfo: TMainMenuModeInfo;    // ����������
    ModeReg: TMainMenuModeReg;      // �����������
    ModeFlash: TMainMenuModeFlash;  // ������

    Pan: TPanel;
    ButList: TList;
    FlNoHardware: Boolean;

    procedure OnInfoBut(Sender: TObject);
    procedure OnAdjustBut(Sender: TObject);
    procedure OnRegistBut(Sender: TObject);
    procedure OnExit(Sender: TObject);
    procedure OnBScan(Sender: TObject);
    procedure OnMnemo(Sender: TObject);
    procedure OnAScan(Sender: TObject);
    procedure UpDateStatusInfo;

    procedure SetButtonsEnabled(Enabled: Boolean);
    procedure SetButtonsEnabledNoHardware(Enabled: Boolean);//F!
  protected
    procedure OnTimer(Sender: TObject); override;
  public
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable{; NoHardware: Boolean=false});
      override;
    destructor Destroy; override;
  end;
 (*
  TBootModeFlash = class(TMainMenuModeFlash) //TWindowMode
{  private
    FlashLetter: Char;
    PrevMode: TWindowMode;
    ModeHaveSet: Boolean;
    ModeProgress: TMainMenuModeFlashInProgress;

    procedure OnCopy(Sender: TObject);
    procedure OnMove(Sender: TObject);
    procedure OnNastrToFlash(Sender: TObject);
    procedure OnFromFlashToNastr(Sender: TObject);
    procedure OnFirmareSelected(id: string);
}
  public
    constructor Create(BootMenu: TBootFlashMenu);
//    procedure SetActive; override;
//    procedure LookForFlash;
  end;

  TBootFlashMenu = class(TPaneledWindow)
  strict private
    function CreatePrototypeButton : TButton; // ������� ������ �������� ������
    procedure CreateButtons;
    procedure CreateTop;
    procedure CreateBottom;

  private
    FGP: TGroupBox;
//    ModeAdj: TMainMenuModeAdj;      // ���������
//    ModeInfo: TMainMenuModeInfo;    // ����������
//    ModeReg: TMainMenuModeReg;      // �����������
    ModeFlash: TMainMenuModeFlash;  // ������

    Pan: TPanel;
    ButList: TList;

//    procedure OnInfoBut(Sender: TObject);
//    procedure OnAdjustBut(Sender: TObject);
//    procedure OnRegistBut(Sender: TObject);
//    procedure OnExit(Sender: TObject);
//    procedure OnBScan(Sender: TObject);
//    procedure OnMnemo(Sender: TObject);
//    procedure OnAScan(Sender: TObject);
//    procedure UpDateStatusInfo;

//    procedure SetButtonsEnabled(Enabled: Boolean);
  protected
    procedure OnTimer(Sender: TObject); override;
  public
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);override;
    destructor Destroy; override;
  end;
*)
implementation

uses MainUnit;

{ TMainMenuModeReg }

constructor TMainMenuModeReg.Create(MainMenu: TMainMenuWindow);
begin
  inherited Create(MainMenu);

  FIsOn := false;

  CPanel := FParent.PanelManager.AddPanel
    (TControlPanel.Create('Reg', poVertical, caWholeSize, 11));
  CPanel.BevelOuter := bvNone;

  BottomPanel := TPanel.Create(nil);
  BottomPanel.ParentColor := false;
  BottomPanel.Parent := TMainMenuWindow(FParent).FGP;
  BottomPanel.ParentBackground := false;
  BottomPanel.Align := alBottom;
  BottomPanel.Height := TMainMenuWindow(FParent).FGP.Height div 5;
  BottomPanel.Color := TMainMenuWindow(FParent).FGP.Color;
  BottomPanel.BevelOuter := bvNone;

  if Config.UnitsSystem = usRus then
    FillRusSettings
//  else if Config.UnitsSystem = usEuro then
//{$IFDEF EGO_USW}
//  else FillEuroSettings;
//{$ELSE}
  else FillAnotherSettings;
//{$ENDIF}


  // ������ - �������� �����������.
  RegOn := ControlFactory.CreateButton(BottomPanel);
  RegOn.Parent := BottomPanel;
  RegOn.Width := Round(BottomPanel.Width / 2.2);
  RegOn.Left := Round(BottomPanel.Width * LabelLeft);
  RegOn.Height := Round(BottomPanel.Height * 0.95);
  RegOn.Top := Round(BottomPanel.Height * LabelLeft);
  RegOn.Font.Height := Round(TMainMenuWindow(FParent).FGP.Height * ButFontH);
  RegOn.Font.Name := DefaultFontName;
  RegOn.Font.Color := TMainMenuWindow(FParent).FCT.Color['BootWin_ButtonText'];
  RegOn.Caption := FParent.FLT.Caption['Reg_But_On'];
  RegOn.Visible := true;
  RegOn.OnClick := OnRegOn;

  // ������ - ��������� �����������.
  RegOff := ControlFactory.CreateButton(BottomPanel);
  RegOff.Parent := BottomPanel;
  RegOff.Width := Round(BottomPanel.Width / 2.2);
  RegOff.Left := Round
    (RegOn.Left + RegOn.Width + BottomPanel.Width * LabelLeft);
  RegOff.Height := Round(BottomPanel.Height * 0.95);
  RegOff.Top := Round(BottomPanel.Height * LabelLeft);
  RegOff.Font.Height := Round(TMainMenuWindow(FParent).FGP.Height * ButFontH);
  RegOff.Font.Name := DefaultFontName;
  RegOff.Font.Color := TMainMenuWindow(FParent).FCT.Color['BootWin_ButtonText'];
  RegOff.Caption := TMainMenuWindow(FParent).FLT.Caption['Reg_But_Off'];
  RegOff.Visible := true;
  RegOff.OnClick := OnRegOff;
  RegOff.Enabled := false;

end;

procedure TMainMenuModeReg.FillRusSettings;
begin
    // ������ ��� ������ ������ ����������
    FOperatorBut := TRegButton.Create('OperatorList', CPanel,
      FParent.FLT['����� ���������']);
    CPanel.Add(FOperatorBut);
    FOperatorBut.OnClick := ShowOperatorListWin;
    FOperatorBut.SetTextBeforeButton(FParent.FLT.Caption['Operator'] + ': ');

    // ������ ��� ������ ������ ���������
    FPeregonBut := TRegButton.Create('PeregonList', CPanel,
      FParent.FLT['����� ��������']);
    CPanel.Add(FPeregonBut);
    FPeregonBut.OnClick := ShowPeregonListWin;
    FPeregonBut.SetTextBeforeButton(FParent.FLT.Caption['Line'] + ': ');

    // ����������� ��������.
    Dir := TRegComboBox.Create('Dir', CPanel);
    CPanel.Add(Dir);
    Dir.Add(FParent.FLT['������ ���� ��']); // 0 - � ������� ����������.
    Dir.Add(FParent.FLT['�� ���� ��']); // 1 - � ������� ����������.
    Dir.ItemIndex := 1;
    Dir.OnChange:= DirChanged;
    Dir.Caption := FParent.FLT.Caption['Reg_Dir'] + ': ';
    Dir.ObjWidth := Round(FParent.Width * 0.3);

    // ����.
    Way := TRegComboBox.Create('Way', CPanel);
    CPanel.Add(Way);
    Way.LoadFromFile(ExtractFilePath(Application.ExeName) + 'NASTR\WayNum.txt');
    Way.Caption := FParent.FLT.Caption['Reg_WayNum'] + ': ';

    // KM.
    KM := TRegNumber.Create('Km', CPanel);
    CPanel.Add(KM);
    KM.Caption := FParent.FLT.Caption['Reg_Km'] + ': ';

    // �����.
    Pk := TRegNumber.Create('Pk', CPanel);
    CPanel.Add(Pk);
    Pk.Caption := FParent.FLT.Caption['Reg_Pk'] + ': ';
    Pk.MinValue := 1;
    Pk.MaxValue := 10;

    // ����.
    Metr := TRegNumber.Create('M', CPanel);
    CPanel.Add(Metr);
    Metr.Caption := FParent.FLT.Caption['Reg_M'] + ': ';
    Metr.MaxValue := 999;

    // ������� �������.
    Side := TRegComboBox.Create('Side', CPanel);
    CPanel.Add(Side);
    Side.Add(FParent.FLT['Left string']);
    Side.Add(FParent.FLT['Right string']);
    Side.Caption := FParent.FLT.Caption['Left side'] + ': ';
    Side.ObjWidth := Round(FParent.Width * 0.3);

    // �������� ���������� ���������� ����������� �� ������������ �����
    if Config.RegOper = '' then
      FOperatorString:= FParent.FLT['�� ������']
    else
      FOperatorString:= Config.RegOper;
    if Config.RegPeregon = '' then
      FPeregonString := FParent.FLT['�� ������']
    else
      FPeregonString:= Config.RegPeregon;

    Dir.ItemIndex := Config.RegNapr;
    Way.ItemIndex := Config.RegWayNum;
    KM.Value := Config.RegKm;
    Pk.Value := Config.RegPk;
    Metr.Value := Config.RegM;
    Side.ItemIndex := Config.LeftSide;
end;

procedure TMainMenuModeReg.FillAnotherSettings;
var c: TCaCrd;
    s:string;
begin
{$IFnDEF USK004R}
{$IFnDEF EGO_USW}
{$IFnDEF VMT_US}
  // Track direction
    TrackDir := TRegComboBox.Create('TrackDirEng', CPanel);
    CPanel.Add(TrackDir);
    TrackDir.Add('NB');
    TrackDir.Add('SB');
    TrackDir.Add('EB');
    TrackDir.Add('WB');
    TrackDir.Add('CT');
    TrackDir.Add('PT');
    TrackDir.Add('RA');
    TrackDir.Add('TT');
    TrackDir.Caption := FParent.FLT.Caption['Track direction'] + ': ';
    TrackDir.ObjWidth := Round(FParent.Width * 0.3);
{$ENDIF}
{$ENDIF}
{$ENDIF}
    // ������ ��� ������ ������ ����������
    FOperatorBut := TRegButton.Create('OperatorList', CPanel,
      FParent.FLT['����� ���������']);
    CPanel.Add(FOperatorBut);
    FOperatorBut.OnClick := ShowOperatorListWin;
    FOperatorBut.SetTextBeforeButton(FParent.FLT.Caption['Operator'] + ': ');

    // ������ ��� ������ ������ ���������
    FPeregonBut := TRegButton.Create('PeregonList', CPanel,
      FParent.FLT['����� ��������']);
    CPanel.Add(FPeregonBut);
    FPeregonBut.OnClick := ShowPeregonListWin;
    FPeregonBut.SetTextBeforeButton(FParent.FLT.Caption['Line'] + ': ');

    // ���������� (Chainage)
{$IFDEF TURKISH_COORD_WITH_DOT}//USK004R_COORD_DOT}
      Chainage := TRegChainage.Create('Coordinate'{'TrackNum'}, CPanel, DefaultFontName,
        FParent.FCT.Color['BootWin_ButtonText'],'',2);
      Chainage.Caption := FParent.FLT.Caption['Coordinate'] + ': ';
      c := AviconTypes.StrToCaCrd(TCoordSys(Ord(usMetricB)), Config.Chainage);
      s:=AviconTypes.CaCrdToStr0(TCoordSys(Ord(usMetricB)), c);
      Chainage.LoadFromString( s );
 {$ELSE}
    if Config.UnitsSystem <> usMetricTurkish then begin
      Chainage := TRegChainage.Create('Coordinate'{'TrackNum'}, CPanel, DefaultFontName,
        FParent.FCT.Color['BootWin_ButtonText']);
      Chainage.Caption := FParent.FLT.Caption['Chainage'] + ': ';
      c := AviconTypes.StrToCaCrd(TCoordSys(Ord(Config.UnitsSystem)), Config.Chainage);
      Chainage.LoadFromString( AviconTypes.CaCrdToStr0(TCoordSys(Ord(Config.UnitsSystem)), c) );
    end
    else begin
      Chainage := TRegChainage.Create('Coordinate'{'TrackNum'}, CPanel, DefaultFontName,
        FParent.FCT.Color['BootWin_ButtonText'], '',1);
      Chainage.Caption := FParent.FLT.Caption['Coordinate'] + ': ';
      c := AviconTypes.StrToCaCrd(TCoordSys(Ord(Config.UnitsSystem)), Config.Chainage);
      Chainage.LoadFromString( AviconTypes.CaCrdToStr0(TCoordSys(Ord(Config.UnitsSystem)), c) );
    end;
{$ENDIF}

    CPanel.Add(Chainage);
    Chainage.ObjWidth := Round(FParent.Width * 0.22);

//    Chainage.LoadFromString(Config.Chainage);

    // ����� ���� (track num)
    TrackNum := TRegNumber.Create('TrackNum', CPanel);
    TrackNum.Caption := FParent.FLT.Caption['Track number'] + ': ';
    TrackNum.Value := Config.TrackNum;
    CPanel.Add(TrackNum);
    TrackNum.MinValue := 1;
    TrackNum.MaxValue := 99;

    // ����������� ��������
    DirEng := TRegComboBox.Create('DirEng', CPanel);
    CPanel.Add(DirEng);
    DirEng.Add(FParent.FLT['������ ���� ��']); // 0 - � ������� ����������.
    DirEng.Add(FParent.FLT['�� ���� ��']); // 1 - � ������� ����������.
    DirEng.ItemIndex := 1;
    DirEng.Caption := FParent.FLT.Caption['Direction'] + ': ';
    DirEng.ObjWidth := Round(FParent.Width * 0.3);
    DirEng.ItemIndex := Config.DirEng;

{$IFnDEF USK004R}
{$IFnDEF EGO_USW}
{$IFnDEF VMT_US}
    // Track direction
    TrackID := TRegComboBox.Create('Track ID', CPanel);
    CPanel.Add(TrackID);
    TrackID.Add('NR');
    TrackID.Add('SR');
    TrackID.Add('ER');
    TrackID.Add('WR');
    TrackID.Caption := FParent.FLT.Caption['Track ID'] + ': ';
    TrackID.ObjWidth := Round(FParent.Width * 0.3);
    TrackDir.ItemIndex := Config.TrackDir;
{$ENDIF}
{$ENDIF}
{$ENDIF}
    if Config.OperEng = '' then
       FOperatorString:= FParent.FLT['�� ������']
    else
      FOperatorString:= Config.OperEng;
    if Config.Line = '' then
      FPeregonString:= FParent.FLT['�� ������']
    else
      FPeregonString:= Config.Line;
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
    // ������� �������.
    Side := TRegComboBox.Create('Side', CPanel);
    CPanel.Add(Side);
    Side.Add(FParent.FLT['Left string']);
    Side.Add(FParent.FLT['Right string']);
    Side.Caption := FParent.FLT.Caption['Left side'] + ': ';
    Side.ObjWidth := Round(FParent.Width * 0.3);
    Side.ItemIndex :=  Config.LeftSide;//0; //
{$IFEND}

{$IFDEF USK004R}
    // ������� ����
    Side := TRegComboBox.Create('Side', CPanel);
    CPanel.Add(Side);
    Side.Add(FParent.FLT['�����']); //����� �����
    Side.Add(FParent.FLT['������']);//������ �����
    Side.Caption := FParent.FLT.Caption['����'] + ': ';//Rail
    Side.ObjWidth := Round(FParent.Width * 0.3);
    Side.ItemIndex :=  Config.LeftSide;// ������� ���� (��� ������������ ��������): 0 � �����, 1 � ������
{$ENDIF}

end;
(*
procedure TMainMenuModeReg.FillEuroSettings;
begin
    // ������ ��� ������ ������ ����������
    FOperatorBut := TRegButton.Create('OperatorList', CPanel,
      FParent.FLT['����� ���������']);
    CPanel.Add(FOperatorBut);
    FOperatorBut.OnClick := ShowOperatorListWin;
    FOperatorBut.SetTextBeforeButton(FParent.FLT.Caption['Operator'] + ': ');

    // ������ ��� ������ ������ ���������
    FPeregonBut := TRegButton.Create('PeregonList', CPanel,
      FParent.FLT['����� ��������']);
    CPanel.Add(FPeregonBut);
    FPeregonBut.OnClick := ShowPeregonListWin;
    FPeregonBut.SetTextBeforeButton(FParent.FLT.Caption['Line'] + ': ');

    // ���������� (Chainage)
{    if Config.UnitsSystem <> usMetricTurkish then
      Chainage := TRegChainage.Create('TrackNum', CPanel, DefaultFontName,
        FParent.FCT.Color['BootWin_ButtonText'])
    else}
      Chainage := TRegChainage.Create('TrackNum', CPanel, DefaultFontName,
        FParent.FCT.Color['BootWin_ButtonText'], '',true);
    CPanel.Add(Chainage);
    Chainage.Caption := FParent.FLT.Caption['Coordinate'] + ': ';
    Chainage.ObjWidth := Round(FParent.Width * 0.22);

    Chainage.LoadFromString(Config.Chainage);

    // ����� ���� (track num)
    TrackNum := TRegNumber.Create('TrackNum', CPanel);
    CPanel.Add(TrackNum);
    TrackNum.Caption := FParent.FLT.Caption['Track number'] + ': ';
    TrackNum.MinValue := 1;
    TrackNum.MaxValue := 99;
    TrackNum.Value := Config.TrackNum;

    // ����������� ��������
    DirEng := TRegComboBox.Create('DirEng', CPanel);
    CPanel.Add(DirEng);
    DirEng.Add(FParent.FLT['������ ���� ��']); // 0 - � ������� ����������.
    DirEng.Add(FParent.FLT['�� ���� ��']); // 1 - � ������� ����������.
    DirEng.ItemIndex := 1;
    DirEng.Caption := FParent.FLT.Caption['Direction'] + ': ';
    DirEng.ObjWidth := Round(FParent.Width * 0.3);
    DirEng.ItemIndex := Config.DirEng;

    if Config.OperEng = '' then
       FOperatorString:= FParent.FLT['�� ������']
    else
      FOperatorString:= Config.OperEng;
    if Config.Line = '' then
      FPeregonString:= FParent.FLT['�� ������']
    else
      FPeregonString:= Config.Line;

    // ������� �������.
    Side := TRegComboBox.Create('Side', CPanel);
    CPanel.Add(Side);
    Side.Add(FParent.FLT['Left string']);
    Side.Add(FParent.FLT['Right string']);
    Side.Caption := FParent.FLT.Caption['Left side'] + ': ';
    Side.ObjWidth := Round(FParent.Width * 0.3);
end;
*)

procedure TMainMenuModeReg.DirChanged(Sender: TObject);
begin
  case Dir.ItemIndex of
    0: Side.ItemIndex:= 1;
    1: Side.ItemIndex:= 0;
  end;
end;

procedure TMainMenuModeReg.OnOperatorSelected(id: string);
begin
  FOperatorBut.Caption := id;
  FOperatorString := id;
  FOperatorBut.RePaint;
  FOperatorBut.SetFocus;
  General.OpenWindow('MAINMENU');
  FOperatorBut.RePaint;
end;

procedure TMainMenuModeReg.OnPeregonSelected(id: string);
begin
  FPeregonBut.Caption := id;
  FPeregonString := id;
  FPeregonBut.RePaint;
  FPeregonBut.SetFocus;
  General.OpenWindow('MAINMENU');
  FPeregonBut.RePaint;
end;

procedure TMainMenuModeReg.OnRegOff(Sender: TObject);
var
  res: integer;
begin
  res := General.Core.StopRegistration;
  if res = 0 then
    IsOn := false;
end;

procedure TMainMenuModeReg.OnRegOn(Sender: TObject);
var
  res, j, DPStep,tmpTrackNumber: integer;
  RegParam: TRegParamList;
  FRegFileName, DateSt, Ext: string;
  FileType: TAviconID;
begin
  RegOn.Enabled := false;

  // ������������ ����� �����
  DateSt := FormatDateTime('dd_mm_yyyy', Date);

  CD(ResultFilePath, true );

  if ProgramProfile.RegistrationFileExt[1] <> '.' then
    Ext := '.' + ProgramProfile.RegistrationFileExt
  else
    Ext := ProgramProfile.RegistrationFileExt;

  FRegFileName := ResultFilePath + DateSt + '_' + IntToStr(Config.FileNum)
    + Ext;

  Config.FileNum := Config.FileNum + 1;
  if Config.FileNum > 100000 then
    Config.FileNum := 0;
  FRegFileName := ExtractFilePath(Application.ExeName) + FRegFileName;

  j := 0;

  while (FileExists(FRegFileName)) and (j <= 1000) do
  begin
    FRegFileName := ResultFilePath + DateSt + '_' + IntToStr
      (Config.FileNum + j) + Ext;
    FRegFileName := ExtractFilePath(Application.ExeName) + FRegFileName;
    Inc(j, 1);
  end;

  if Config.UnitsSystem = usRus then
  begin
    if FOperatorString <> FParent.FLT['�� ������'] then
      Config.RegOper := FOperatorString;
    if FPeregonString <> FParent.FLT['�� ������'] then
      Config.RegPeregon := FPeregonString;
    Config.RegNapr := Dir.ItemIndex;
    Config.RegWayNum := Way.ItemIndex;
    Config.RegKm := KM.Value;
    Config.RegPk := Pk.Value;
    Config.RegM := Metr.Value;
    Config.LeftSide:= Side.ItemIndex;
    Config.Save;
  end
  else
  begin
{$IFnDEF USK004R}
{$IFnDEF EGO_USW}
{$IFnDEF VMT_US}
    Config.TrackDir := TrackDir.ItemIndex;
{$ENDIF}
{$ENDIF}
{$ENDIF}
    if FOperatorString <> FParent.FLT['�� ������'] then
      Config.OperEng := FOperatorString;
    if FPeregonString <> FParent.FLT['�� ������'] then
      Config.Line := FPeregonString;
    Config.Chainage := Chainage.ToString;
    Config.TrackNum := TrackNum.Value;
    Config.DirEng := DirEng.ItemIndex;
{$IFnDEF USK004R}
{$IFnDEF EGO_USW}
{$IFnDEF VMT_US}
    Config.TrackID := TrackID.ItemIndex;
{$ENDIF}
{$ENDIF}
{$ENDIF}

{$IF DEFINED(EGO_USW) OR DEFINED(USK004R) OR DEFINED(VMT_US)}
    Config.LeftSide:= Side.ItemIndex;
{$IFEND}

  end;

  RegParam.Organization := '';

  if Config.UnitsSystem = usRus then
  begin
    RegParam.Operator := FOperatorString;
    RegParam.PeregonName := FPeregonString;
    RegParam.DirCode := 0;
    RegParam.StartKM := KM.Value;
    RegParam.StartPK := Pk.Value;

(* <Rud9> *)
    tmpTrackNumber:=0;
    try
       tmpTrackNumber := StrToInt(Way.Value);
    except

    end;

    RegParam.TrackNumber := tmpTrackNumber;
(* </Rud9> *)

    RegParam.Direction := Dir.ItemIndex;

    if (Dir.ItemIndex = 0) then
      RegParam.StartM := Metr.Value
    else
      RegParam.StartM := Metr.Value;

    RegParam.LeftSide:= Side.ItemIndex;
  end
  else
  begin
{$IFnDEF USK004R}
{$IFnDEF EGO_USW}
{$IFnDEF VMT_US}
    RegParam.DirectionID := TrackDir.Value;
{$ENDIF}
{$ENDIF}
{$ENDIF}
    RegParam.Operator := FOperatorString;
    RegParam.PeregonName := FPeregonString;
    RegParam.TrackNumber := TrackNum.Value;
    RegParam.Chainage := Chainage.ToString;
    RegParam.Direction := DirEng.ItemIndex;
{$IFnDEF USK004R}
{$IFnDEF EGO_USW}
{$IFnDEF VMT_US}
    RegParam.TrackID := TrackID.Value;
{$ENDIF}
{$ENDIF}
{$ENDIF}

{$IF DEFINED(EGO_USW) OR DEFINED(USK004R) OR DEFINED(VMT_US)}
    if Assigned(Side) then
      RegParam.LeftSide:= Side.ItemIndex;
{$IFEND}
  end;

  DPStep := Round(Config.DPStep * 100);
  if (DPStep < 65534) and (DPStep > 0) then
    RegParam.ScanStep := DPStep
  else
    RegParam.ScanStep := 180;
  RegParam.Scheme := Ord(Config.SoundedScheme) + 1;

  if ProgramProfile.DeviceFamily = 'Avicon16' then
    begin
      case Config.SoundedScheme of
         Wheels:  FileType := Avicon16Wheel;
         Scheme1: FileType := Avicon16Scheme1;
         Scheme2: FileType := Avicon16Scheme2;
         Scheme3: FileType := Avicon16Scheme3;
      end;
    end;
  if ProgramProfile.DeviceFamily = 'EGO-USW' then
    begin
      case Config.SoundedScheme of
         Scheme1: FileType := EGO_USWScheme1;
         Scheme2: FileType := EGO_USWScheme2;
         Scheme3: FileType := EGO_USWScheme3;
      end;
    end;
  if ProgramProfile.DeviceFamily = 'Avicon14' then
    begin
      if Config.SoundedScheme = Wheels then
        FileType := Avicon14Wheel
      else
        FileType := Avicon14Scheme1;
    end;
  if ProgramProfile.DeviceFamily = 'USK004R' then FileType := USK004R; //Avicon16Scheme1;//

  res := General.Core.StartRegistration(FileType, FRegFileName, RegParam, true);
  if res = 0 then
    IsOn := true;

  General.Mode := bmSearch;
end;

procedure TMainMenuModeReg.SetActive;
begin
  TMainMenuWindow(FParent).PanelManager.SwitchPanel(CPanel.ID);
  BottomPanel.Visible := true;
  TMainMenuWindow(FParent).FGP.Caption := FParent.FLT['�����������'];
  FPeregonBut.Caption := FPeregonString;
  FOperatorBut.Caption := FOperatorString;
end;

procedure TMainMenuModeReg.SetIsOn(B: Boolean);
begin
  FIsOn := B;
  RegOn.Enabled := not B;
  RegOff.Enabled := B;
  if Config.UnitsSystem = usRus then
  begin
    KM.Enabled := not B;
    Pk.Enabled := not B;
    Metr.Enabled := not B;
    FPeregonBut.Enabled := not B;
    FOperatorBut.Enabled := not B;
    Way.Enabled := not B;
    Dir.Enabled := not B;
    Side.Enabled:= not B;
  end
  else
  begin
{$IFnDEF USK004R}
{$IFnDEF EGO_USW}
{$IFnDEF VMT_US}
    TrackDir.Enabled := not B;
{$ENDIF}
{$ENDIF}
{$ENDIF}
    FPeregonBut.Enabled := not B;
    FOperatorBut.Enabled := not B;
    TrackNum.Enabled := not B;
    Chainage.Enabled := not B;
    DirEng.Enabled := not B;

{$IFnDEF USK004R}
{$IFnDEF EGO_USW}
{$IFnDEF VMT_US}
    TrackID.Enabled := not B;
{$ENDIF}
{$ENDIF}
{$ENDIF}
  end;
end;

procedure TMainMenuModeReg.ShowOperatorListWin(Sender: TObject);
var
  Win: TListWindow;
  SL: TStringList;
  i: integer;
  fname: string;
begin
  Win := General.GetListWindow;
  Win.Clear;
  Win.ListHeader := FParent.FLT.Caption['�������� ���������'];
  Win.OnItemSelected := OnOperatorSelected;
  Win.WinToExit := 'MAINMENU';
  Win.EnableInputStr:=True;

  SL := TStringList.Create;
  fname:=ExtractFilePath(Application.ExeName)
      + 'NASTR\OperatorList.txt';
  SL.LoadFromFile(fname);
  for i := 0 to SL.Count - 1 do
    Win.AddItem(SL.Strings[i], SL.Strings[i]);
  SL.Free;
{$IFDEF INPUTSTR_IN_LISTWIN}
  Win.FileName:=fname;
{$ENDIF}
  General.OpenWindow('ListWin');
end;

procedure TMainMenuModeReg.ShowPeregonListWin(Sender: TObject);
var
  Win: TListWindow;
  SL: TStringList;
  i: integer;
  fname: string;
begin
  Win := General.GetListWindow;
  Win.Clear;
  Win.ListHeader := FParent.FLT.Caption['�������� �������'];
  Win.OnItemSelected := OnPeregonSelected;
  Win.WinToExit := 'MAINMENU';
  Win.EnableInputStr:=True;

  SL := TStringList.Create;
  fname:=ExtractFilePath(Application.ExeName)
      + 'NASTR\PeregonList.txt';
  SL.LoadFromFile(fname);
  for i := 0 to SL.Count - 1 do
    Win.AddItem(SL.Strings[i], SL.Strings[i]);
  SL.Free;
{$IFDEF INPUTSTR_IN_LISTWIN}
  Win.FileName:=fname;
{$ENDIF}
  General.OpenWindow('ListWin');
end;

{ TMainMenuModeAdj }

constructor TMainMenuModeAdj.Create(MainMenu: TMainMenuWindow);
var
  TableButton: TButton;
begin
  inherited Create(MainMenu);
  CPanel := FParent.PanelManager.AddPanel
    (TButtonPanel.Create('Adj', poVertical, caWholeSize, 18));
  CPanel.BevelOuter := bvNone;
  TButtonPanel(CPanel).FontKoef := 0.5;

  with TButtonPanel(CPanel) do
  begin
    AddButton(FParent.FLT.Caption['Channels'], OnAdjKan);
    AddButton(FParent.FLT.Caption['DP'], OnAdjDPBut);
    AddButton(FParent.FLT.Caption['Prog'], OnAdjSoftBut);
    TableButton:= AddButton(FParent.FLT.Caption['������� ��������'], OnAdjTableBut);
  end;

  if ProgramProfile.DeviceFamily = 'RKS-U' then
    TableButton.Visible := False;
end;

procedure TMainMenuModeAdj.ItemSelected(id: string);
begin
  ShowMessage(id);
end;

procedure TMainMenuModeAdj.OnAdjDPBut(Sender: TObject);
begin
  if General.Core.RegOnUserLevel then
    MessageForm.Display(FParent.FLT.Caption['AdjDP_Err'], clRed)
  else
    General.OpenWindow('ADJDP');
end;

procedure TMainMenuModeAdj.OnAdjKan(Sender: TObject);
begin
  General.Core.PauseRegThread;
  Sleep(600);
  General.Mode := bmNastr;
end;

procedure TMainMenuModeAdj.OnAdjSoftBut(Sender: TObject);
begin
  General.OpenWindow('ADJSOFT');
end;

procedure TMainMenuModeAdj.OnAdjTableBut(Sender: TObject);
begin
  General.OpenWindow('ADJTABLE');
end;

procedure TMainMenuModeAdj.SetActive;
begin
  inherited;
  if Assigned(TMainMenuWindow(FParent).ModeReg) then
    TMainMenuWindow(FParent).ModeReg.BottomPanel.Visible := false;
  TMainMenuWindow(FParent).FGP.Caption := FParent.FLT['���������'];
end;

{ TMainMenuModeInfo }

constructor TMainMenuModeInfo.Create(MainMenu: TMainMenuWindow);
var
  K: Single;
begin
  inherited Create(MainMenu);

//  if Config.Language = 'Spanish' then K := 0.3
//  else
  K := 0.35;
  FRegFileName := '';

{$IFDEF BUMCOUNT4}
  CPanel := FParent.PanelManager.AddPanel
    (TControlPanel.Create('Info', poVertical, caTop, 5{8}));
{$ELSE}
  CPanel := FParent.PanelManager.AddPanel
    (TControlPanel.Create('Info', poVertical, caTop, 8));
{$ENDIF}
  CPanel.BevelOuter := bvNone;

  RegStat := TRegValue.Create('RegStat', CPanel,
    FParent.FLT.Caption['�����������']);
  CPanel.Add(RegStat);
  RegStat.ObjLeft := Round(Screen.Width * K);

  AKBUI := TRegValue.Create('AKBUI', CPanel, FParent.FLT.Caption['����� ���']);
  CPanel.Add(AKBUI);
//  if Config.Language = 'Spanish' then
//    AKBUI.ObjLeft := Round(Screen.Width * 0.3)
//  else
  AKBUI.ObjLeft := Round(Screen.Width * K);

  if ProgramProfile.DeviceFamily <> 'RKS-U' then
  begin
    AKBUM := TRegValue.Create('AKBUM', CPanel, FParent.FLT.Caption
          ['����. ���. ���']);
    CPanel.Add(AKBUM);
    AKBUM.ObjLeft := Round(Screen.Width * K);
  end;
  SNBUI := TRegValue.Create('SNBUI', CPanel, FParent.FLT.Caption['�/� ���']);
  CPanel.Add(SNBUI);
  SNBUI.ObjLeft := Round(Screen.Width * K);
{$IFDEF BUMCOUNT4}
  SNBUM := TRegValue.Create('SNBUM', CPanel, FParent.FLT.Caption['�/� ���1']);
  CPanel.Add(SNBUM);
  SNBUM.ObjLeft := Round(Screen.Width * K);
  SNBUM2 := TRegValue.Create('SNBUM2', CPanel, FParent.FLT.Caption['�/� ���2']);
  CPanel.Add(SNBUM2);
  SNBUM2.ObjLeft := Round(Screen.Width * K);
  SNBUM3 := TRegValue.Create('SNBUM3', CPanel, FParent.FLT.Caption['�/� ���3']);
  CPanel.Add(SNBUM3);
  SNBUM3.ObjLeft := Round(Screen.Width * K);
  SNBUM4 := TRegValue.Create('SNBUM4', CPanel, FParent.FLT.Caption['�/� ���4']);
  CPanel.Add(SNBUM4);
  SNBUM4.ObjLeft := Round(Screen.Width * K);
{$ELSE}
{$IF DEFINED(BUMCOUNT2) AND NOT DEFINED(USK004R)}
  SNBUM := TRegValue.Create('SNBUM', CPanel, FParent.FLT.Caption['�/� ���1']);
  CPanel.Add(SNBUM);
  SNBUM.ObjLeft := Round(Screen.Width * K);
  SNBUM2 := TRegValue.Create('SNBUM2', CPanel, FParent.FLT.Caption['�/� ���2']);
  CPanel.Add(SNBUM2);
  SNBUM2.ObjLeft := Round(Screen.Width * K);
{$ELSE}
  SNBUM := TRegValue.Create('SNBUM', CPanel, FParent.FLT.Caption['�/� ���']);
  CPanel.Add(SNBUM);
  SNBUM.ObjLeft := Round(Screen.Width * K);
{$IFEND}
{$ENDIF}

  if ProgramProfile.DeviceFamily <> 'RKS-U' then
  begin
    U2CSN := TRegValue.Create('U2CSN', CPanel, FParent.FLT.Caption
        ['USB adapter s/n']);
    CPanel.Add(U2CSN);
    U2CSN.ObjLeft := Round(Screen.Width * K);
  end;
{$IFDEF BUMCOUNT4}
  if ProgramProfile.DeviceFamily <> 'RKS-U' then
  begin
    U2CSN2 := TRegValue.Create('U2CSN2', CPanel, FParent.FLT.Caption
        ['USB adapter 2 s/n']);
    CPanel.Add(U2CSN2);
    U2CSN2.ObjLeft := Round(Screen.Width * K);
  end;
{$ENDIF}

  POBUI := TRegValue.Create('POBUI', CPanel, FParent.FLT.Caption['�������� ���']
    );
  CPanel.Add(POBUI);
  POBUI.ObjLeft := Round(Screen.Width * K);
{$IFDEF BUMCOUNT4}
  POBUM := TRegValue.Create
    ('POBUM', CPanel, FParent.FLT.Caption['�������� ���1']);
  CPanel.Add(POBUM);
  POBUM.ObjLeft := Round(Screen.Width * K);
  POBUM2 := TRegValue.Create('POBUM2', CPanel, FParent.FLT.Caption
      ['�������� ���2']);
  CPanel.Add(POBUM2);
  POBUM2.ObjLeft := Round(Screen.Width * K);
  POBUM3 := TRegValue.Create
    ('POBUM3', CPanel, FParent.FLT.Caption['�������� ���3']);
  CPanel.Add(POBUM3);
  POBUM3.ObjLeft := Round(Screen.Width * K);
  POBUM4 := TRegValue.Create('POBUM4', CPanel, FParent.FLT.Caption
      ['�������� ���4']);
  CPanel.Add(POBUM4);
  POBUM4.ObjLeft := Round(Screen.Width * K);
{$ELSE}
//{$IFDEF BUMCOUNT2}
{$IF DEFINED(BUMCOUNT2) AND NOT DEFINED(USK004R)}
  POBUM := TRegValue.Create
    ('POBUM', CPanel, FParent.FLT.Caption['�������� ���1']);
  CPanel.Add(POBUM);
  POBUM.ObjLeft := Round(Screen.Width * K);
  POBUM2 := TRegValue.Create('POBUM', CPanel, FParent.FLT.Caption
      ['�������� ���2']);
  CPanel.Add(POBUM2);
  POBUM2.ObjLeft := Round(Screen.Width * K);
{$ELSE}
  POBUM := TRegValue.Create('POBUM', CPanel, FParent.FLT.Caption['�������� ���']
    );
  CPanel.Add(POBUM);
  POBUM.ObjLeft := Round(Screen.Width * K);
{$IFEND}
{$ENDIF}
(*
{$IFDEF USK004R}
    U2CVer := TRegValue.Create('U2CVER', CPanel, FParent.FLT.Caption
        ['USB adapter ver.']);
    CPanel.Add(U2CSN);
    U2CSN.ObjLeft := Round(Screen.Width * K);
{$ENDIF}
*)
{$IFDEF GPS}
  SLatitude := TRegValue.Create
    ('Latitude', CPanel, FParent.FLT.Caption['GPS Latitude']);
  CPanel.Add(SLatitude);
  SLatitude.ObjLeft := Round(Screen.Width * K);

  SLongitude := TRegValue.Create
    ('Longitude', CPanel, FParent.FLT.Caption['GPS Longitude']);
  CPanel.Add(SLongitude);
  SLongitude.ObjLeft := Round(Screen.Width * K);
{$ENDIF}

  if (ProgramProfile.DeviceFamily <> 'RKS-U') and
     (ProgramProfile.DeviceFamily <> 'USK004R') then
  begin
    CURScheme := TRegValue.Create
     ('CURScheme', CPanel, FParent.FLT.Caption['����� �������������']);
    CPanel.Add(CURScheme);
    CURScheme.ObjLeft := Round(Screen.Width * K);
  end;

  if ProgramProfile.ShowOldFiles then
  begin
    OpenFile := TRegButton.Create('OpenFile', CPanel,
      FParent.FLT.Caption['���������� �������']);
    CPanel.Add(OpenFile);
    OpenFile.ObjLeft := Round(Screen.Width * K);
    OpenFile.OnClick := OnOpenFilePress;
  end;
end;

procedure TMainMenuModeInfo.FileSelected(id: string);
var
  Win2: TListWindow;
  FDataFile: TAviconDataSource;

begin
  FRegFileName := id;

  FDataFile := TAviconDataSource.Create;
  try
    FDataFile.LoadFromFile(FRegFileName{,false}); //03-2015 AK
  Except
    FDataFile.Free;
    Exit;
  end;

  Win2 := General.GetListWindow2;
  Win2.Clear;
  Win2.ListHeader := FParent.FLT.Caption['��������� �������'];
  Win2.SelectEnable := false;
  Win2.WinToExit := 'ListWin';
  Win2.SecondButtonHeader := FParent.FLT.Caption['������� ������'];
  Win2.ShowSecondButton;
  Win2.OnSecondButtonPress := OpenFileButPress;

  Win2.AddItem('1', '��� �����: ' + ExtractFileName(FRegFileName));
  Win2.AddItem('2', '��������: ' + HeaderStrToString
      (FDataFile.Header.OperatorName));
  Win2.AddItem('3', '�������: ' + HeaderStrToString
      (FDataFile.Header.PathSectionName));
  case FDataFile.Header.MoveDir of
    1:
      Win2.AddItem('4', '����.: � ������� ����������');
    -1:
      Win2.AddItem('4', '����.: � ������� ����������');
  end;
  Win2.AddItem('5', Format('����� ����: %d', [FDataFile.Header.RailPathNumber])
    );
  Win2.AddItem('6', Format('��: %d  ��: %d  �: %d', [FDataFile.Header.StartKM,
      FDataFile.Header.StartPK, FDataFile.Header.StartMetre]));
  Win2.AddItem('7', Format('���� �������: %.2d.%.2d.%.4d',
      [FDataFile.Header.Day, FDataFile.Header.Month, FDataFile.Header.Year]));
  Win2.AddItem('8', Format('����� �������: %.2d:%.2d', [FDataFile.Header.Hour,
      FDataFile.Header.Minute]));

  FDataFile.Free;

  General.OpenWindow('ListWin2');
end;

procedure TMainMenuModeInfo.OnOpenFilePress(Sender: TObject);
var
  Win: TListWindow;
  sr: TSearchRec;
  Path: string;
begin
  Win := General.GetListWindow;
  Win.Clear;
  Win.ListHeader := FParent.FLT.Caption['�������'];
  Win.OnItemSelected := FileSelected;
  Win.WinToExit := 'MAINMENU';
  Win.EnableInputStr:=False;

  Path := ExtractFilePath(Application.ExeName) + ResultFilePath;

  if FindFirst(Path + '*.a??', faAnyFile, sr) = 0 then
  begin
    repeat
      if (sr.Name <> '.') and (sr.Name <> '..') then
        Win.AddItem(Path + sr.Name, sr.Name);
    until FindNext(sr) <> 0;
    FindClose(sr);
  end;

  General.OpenWindow('ListWin');
end;

procedure TMainMenuModeInfo.OpenFileButPress(Sender: TObject);
begin
  General.LoadFile(FRegFileName);
end;

procedure TMainMenuModeInfo.SetActive;
begin
  inherited;
  if assigned(TMainMenuWindow(FParent).ModeReg) then
    TMainMenuWindow(FParent).ModeReg.BottomPanel.Visible := false;
  TMainMenuWindow(FParent).FGP.Caption := FParent.FLT['����������'];
end;

{ TMainMenuModeFlashInProgress }

constructor TMainMenuModeFlashInProgress.Create(MainMenu: TMainMenuWindow);
begin
  inherited Create(MainMenu);

  ProgressTimer := TTimer.Create(nil);
  ProgressTimer.Enabled := false;

  CPanel := FParent.PanelManager.AddPanel
    (TControlPanel.Create('FlashDataTransfer', poVertical, caWholeSize, 15));
  CPanel.BevelOuter := bvNone;

  Lb := ControlFactory.CreateLabel(FParent);
  ControlFactory.InitializeLabel(Lb, FParent.Height div 12);
  Lb.Caption := FParent.FLT['�������������...'];
  CPanel.Add(Lb);

  PB := ControlFactory.CreateProgressBar(FParent);
  CPanel.Add(PB);

  But := ControlFactory.CreateButton(FParent);
  But.OnClick := OnCancel;
  But.Font.Name := DefaultFontName;
  But.Font.Color := clBlack;
  But.Font.Height := FParent.Height div 15;
  But.Caption := FParent.FLT['������'];
  CPanel.Add(But);

end;

destructor TMainMenuModeFlashInProgress.Destroy;
begin
  ProgressTimer.Free;
  inherited;
end;

(*<Rud53>*)
procedure TMainMenuModeFlashInProgress.ExportSettings(FlashLetter: Char);

    function FindLastNastr(Path, Template: string): string;
    var
      sr: TSearchRec;
      FindRes, i: Integer; // ���������� ��� ������ ���������� ������
      Num, MaxNum: integer;
      NumStr, MaxFileName: string;
    begin
      // ��������� ������������ �����
      MaxNum:=0;
      Num := 0;
      MaxFileName:= '';
      FindRes := FindFirst(Path+Template+'_*.kan', faAnyFile, SR);
      while FindRes = 0 do
      begin
        if ( SR.Name <> '.' ) and ( SR.Name <> '..' ) then
          begin
            NumStr:= '';
            for i:= 6 to Length(SR.Name)-4 do
              if (SR.Name[i]>='0') and (SR.Name[i]<='9') then
                  NumStr:= NumStr + SR.Name[i];
              try
                Num:= StrToInt(NumStr);
              except
              end;
          end;
          if Num > MaxNum then
            begin
              MaxNum:= Num;
              MaxFileName:= SR.Name;
            end;
        FindRes := FindNext(SR);
      end;
      FindClose(SR);
      result:= MaxFileName;
    end;

    procedure AddNastrFileToFileList(Files: TFileList; SrcPath, SubFolder, Template: string);
    var
      NastrFile: String;
    begin
      NastrFile:= FindLastNastr(SrcPath + SubFolder, Template);
      if (NastrFile <> '') and Assigned(Files) then
        Files.AddFile('NASTR\'+SubFolder + NastrFile, SubFolder + NastrFile);
    end;
var
  SrcPath, SrcPathAirBrush: String;
  BurnBaseName, BurnName: String;
  Files: TFileList;
  Transfer: TTransferEngine;
  i: integer;
  res: boolean;
begin
  FProgress := 0;
  PB.Position := 0;
  PB.Visible := true;
  IsCancel := false;
  Lb.Caption := FParent.FLT['�������� ��������'];
  But.Caption := FParent.FLT['������'];
  SrcPathAirBrush := ExtractFilePath(Application.ExeName);
  SrcPath := ExtractFilePath(Application.ExeName) + 'NASTR\';

  Transfer := TTransferEngine.Create;
  Transfer.OnTranferProgress := OnProgress;

  // ��������� ��������
  BurnBaseName := ProgramProfile.ProgramTitle;
  i := 0;
  repeat
    Inc(i);
    BurnName := Format('%s_%s_%d.ego', [BurnBaseName, General.BUISN, i]);
  until not FileExists(FlashLetter + ':\' + BurnName);

  Files := TFileList.Create;
  Files.AddFile('NASTR\OperatorList.txt', 'OperatorList.txt');
  Files.AddFile('NASTR\DefCodes.txt',     'DefCodes.txt');
  Files.AddFile('NASTR\PeregonList.txt',  'PeregonList.txt');
  Files.AddFile('NASTR\TextMarks.txt',    'TextMarks.txt');
  Files.AddFile('NASTR\Organization.txt', 'Organization.txt');

{$IFDEF PAINTER}
  Files.AddFile(Config.AirBrushConfig, Config.AirBrushConfig);
{$ENDIF}

{$IFDEF EGO_USW}
  AddNastrFileToFileList(Files, SrcPath, 'EGO_USW1\', 'Var1');
  AddNastrFileToFileList(Files, SrcPath, 'EGO_USW1\', 'Var2');
  AddNastrFileToFileList(Files, SrcPath, 'EGO_USW1\', 'Var3');

  AddNastrFileToFileList(Files, SrcPath, 'EGO_USW2\', 'Var1');
  AddNastrFileToFileList(Files, SrcPath, 'EGO_USW2\', 'Var2');
  AddNastrFileToFileList(Files, SrcPath, 'EGO_USW2\', 'Var3');

  AddNastrFileToFileList(Files, SrcPath, 'EGO_USW3\', 'Var1');
  AddNastrFileToFileList(Files, SrcPath, 'EGO_USW3\', 'Var2');
  AddNastrFileToFileList(Files, SrcPath, 'EGO_USW3\', 'Var3');
{$ELSE}
  {$IFDEF Avikon16_IPV}
  AddNastrFileToFileList(Files, SrcPath, 'IPV_SCH1\', 'Var1');
  AddNastrFileToFileList(Files, SrcPath, 'IPV_SCH1\', 'Var2');
  AddNastrFileToFileList(Files, SrcPath, 'IPV_SCH1\', 'Var3');

  AddNastrFileToFileList(Files, SrcPath, 'IPV_SCH2\', 'Var1');
  AddNastrFileToFileList(Files, SrcPath, 'IPV_SCH2\', 'Var2');
  AddNastrFileToFileList(Files, SrcPath, 'IPV_SCH2\', 'Var3');

  AddNastrFileToFileList(Files, SrcPath, 'IPV_SCH3\', 'Var1');
  AddNastrFileToFileList(Files, SrcPath, 'IPV_SCH3\', 'Var2');
  AddNastrFileToFileList(Files, SrcPath, 'IPV_SCH3\', 'Var3');

  AddNastrFileToFileList(Files, SrcPath, 'IPV_W\', 'Var1');
  AddNastrFileToFileList(Files, SrcPath, 'IPV_W\', 'Var2');
  AddNastrFileToFileList(Files, SrcPath, 'IPV_W\', 'Var3');
  {$ELSE}
    {$IFDEF USK004R}
  AddNastrFileToFileList(Files, SrcPath, 'USK004R\', 'Var1');
  AddNastrFileToFileList(Files, SrcPath, 'USK004R\', 'Var2');
  AddNastrFileToFileList(Files, SrcPath, 'USK004R\', 'Var3');
    //����� ������� �� ��������� ������ ���������
     //������ ������ ��� ����� ��������� � �� �� �����
    {$ELSE}
     {$IFDEF VMT_US}
         AddNastrFileToFileList(Files, SrcPath, 'VMT_US\', 'Var1');
         AddNastrFileToFileList(Files, SrcPath, 'VMT_US\', 'Var2');
         AddNastrFileToFileList(Files, SrcPath, 'VMT_US\', 'Var3');
     {$ENDIF}
    {$ENDIF}
  {$ENDIF}
{$ENDIF}
  res := Transfer.PackFiles(Files, BurnName, FlashLetter + ':\');

  Transfer.Free;
  Files.Free;

  if res = true then
  begin
    Transfer.OnTranferProgress := nil;
    Lb.Caption := FParent.FLT['�������� ��������'];
    But.Caption := FParent.FLT['Ok'];
    ProgressTimer.Enabled := false;
    FProgress := 100;
    PB.Position := 100;
  end
  else
  begin
    Lb.Caption := FParent.FLT['������ �������� ��������!'];
    But.Caption := FParent.FLT['Ok'];
  end;
end;
(*</Rud53>*)

procedure TMainMenuModeFlashInProgress.FlashLoosed;
begin
  if FParent.CMode = Self then
    OnCancel(nil);
end;

procedure TMainMenuModeFlashInProgress.ImportSettings(FileName: string);
var
  Transfer: TTransferEngine;
  BurnFileName, UnPackFolder: string;
begin
  FProgress := 0;
  PB.Position := 0;
  PB.Visible := true;
  IsCancel := false;
  Lb.Caption := FParent.FLT['�������� ��������'];
  But.Caption := FParent.FLT['������'];

  BurnFileName := FileName;
  UnPackFolder := ExtractFilePath(Application.ExeName);// + 'NASTR\'; //AK
  UnPackFolder:=ExcludeTrailingBackslash(UnPackFolder);

  Transfer := TTransferEngine.Create;

  if Transfer.UnPackFiles(BurnFileName, UnPackFolder) then
  begin
    Transfer.OnTranferProgress := nil;
    Lb.Caption := FParent.FLT['�������� ��������'];
    But.Caption := FParent.FLT['Ok'];
    FProgress := 100;
    PB.Position := 100;
  end
  else
  begin
    Lb.Caption := FParent.FLT['������ �������� ��������!'];
    But.Caption := FParent.FLT['Ok'];
  end;

  Transfer.Free;
  KConf.DoNotSaveUserConfig := true;
  if TMainMenuWindow(FParent).FlNoHardware then TMainMenuWindow(FParent).SetButtonsEnabledNoHardware(true)
  else TMainMenuWindow(FParent).SetButtonsEnabled(true);
  FParent.CMode := PrevMode;
  IsCancel := true;
  General.OpenWindow('RebootConfirm');
  But.Caption := FParent.FLT['������'];
end;

procedure TMainMenuModeFlashInProgress.OnCancel(Sender: TObject);
begin
  ProgressTimer.Enabled := false;
  But.Caption := FParent.FLT['������'];
  if TMainMenuWindow(FParent).FlNoHardware then begin
    TMainMenuWindow(FParent).SetButtonsEnabledNoHardware(true);
    IsCancel := true;
    if PrevMode<>Nil then FParent.CMode := PrevMode
    else General.RebootProgram;
//    General.DisposeUSBFlashBeforeStart;
    if Sender=Nil then //FlashLoosed;
       General.RebootProgram;
  end
  else begin TMainMenuWindow(FParent).SetButtonsEnabled(true);
    FParent.CMode := PrevMode;
    IsCancel := true;
  end;
end;

procedure TMainMenuModeFlashInProgress.OnProgress(Progress: integer);
begin
  FProgress := Progress;
end;

procedure TMainMenuModeFlashInProgress.OnTimer(Sender: TObject);
begin
  PB.Position := FProgress;
end;

procedure TMainMenuModeFlashInProgress.SetActive;
begin
  inherited;
  if assigned(TMainMenuWindow(FParent).ModeReg) then
    TMainMenuWindow(FParent).ModeReg.BottomPanel.Visible := false;
  TMainMenuWindow(FParent).FGP.Caption := FParent.FLT['�������� ������...'];
  TMainMenuWindow(FParent).SetButtonsEnabled(false);
end;

procedure TMainMenuModeFlashInProgress.Transfer(FlashLetter: Char;
  MoveFiles: Boolean);
var
  Files: TStringList;
  sr: TSearchRec;
  PhFr: string;
  CopyCount: integer;
  i: integer;
  Fn: string;

begin
  PB.Position := 0;
  PB.Visible := false;
  IsCancel := false;

//  General.Core.PauseRegistration(True);
//  General.Core.SetMode(rStop);
//  General.Core.Stop;

  if not CD('ResFiles') then
  begin
    Lb.Caption := FParent.FLT['��� ��������.'];
    But.Caption := FParent.FLT['OK'];
    Exit;
  end;

  PhFr := AddSlash(GetCurrentDir);

  // ���������� ������ ������ ��� ��������.
  Files := TStringList.Create;
  if FindFirst('*.a??', faAnyFile, sr) = 0 then
  begin
    repeat
      if (sr.Name <> '.') and (sr.Name <> '..') then
        Files.Add(sr.Name);
    until FindNext(sr) <> 0;
    FindClose(sr);
  end;

{$IFDEF USK004R}
  if FindFirst('*.csv', faAnyFile, sr) = 0 then
  begin
    repeat
      if (sr.Name <> '.') and (sr.Name <> '..') then
        Files.Add(sr.Name);
    until FindNext(sr) <> 0;
    FindClose(sr);
  end;
{$ENDIF}

  if Files.Count = 0 then
  begin
    Files.Free;
    Lb.Caption := FParent.FLT['��� ��������.'];
    But.Caption := FParent.FLT['OK'];
    Exit;
  end;

  // �������� �������� �����.
  if not CD(FlashLetter + ':\DefResults', true) then
  begin
    Files.Free;
    Lb.Caption := FParent.FLT['������ ������� � Flash.'];
    But.Caption := FParent.FLT['OK'];
    Exit;
  end;

  if MoveFiles then
    Lb.Caption := FParent.FLT['�����������...']
  else
    Lb.Caption := FParent.FLT['�����������...'];

  // ��������� �����.
  PB.Visible := true;
  CopyCount := 0;
  for i := 0 to Files.Count - 1 do
  begin
    Fn := Files[i];
//    PB.Position := (i {+ 1}) * 100 div Files.Count;
    if IsCancel then
      Exit;
    Application.ProcessMessages;
    if not CopyFile(PWideChar(PhFr + Fn), PWideChar(Fn), false) then
      Lb.Caption := FParent.FLT['������: '] + Fn
    else
    begin
      Inc(CopyCount);
      if MoveFiles then
        DeleteFile(PhFr + Fn);
    end;
    PB.Position := (i + 1) * 100 div Files.Count;
  end;
//    Application.ProcessMessages;
(*///  sleep(100);//AK
  //���������, ��� ����������� �� ���� �������� ���������
{  hDrive := CreateFile(PAnsiChar(@DeviceInterfaceDetailData.DevicePath[0]),
         0, FILE_SHARE_READ or FILE_SHARE_WRITE, nil, OPEN_EXISTING, 0, 0);
       if hDrive = INVALID_HANDLE_VALUE then
}
///  Eject_USB(FlashLetter,4,300,True,False{True});
*)
  sleep(2000);
  Lb.Caption := FParent.FLT['�������� ������: '] + IntToStr(CopyCount);
  But.Caption := FParent.FLT['OK'];
  Files.Free;

//  General.Core.Resume;
//  General.Core.PauseRegistration(False);

end;

{ TMainMenuModeFlash }

constructor TMainMenuModeFlash.Create(MainMenu: TMainMenuWindow);
begin
  inherited Create(MainMenu);
  ModeHaveSet := false;
//  if General.FlWorkOnlyFlashDisk  then
//  ModeHaveSet := General.FlashDiskModeHaveSet;

  CPanel := FParent.PanelManager.AddPanel
    (TButtonPanel.Create('FlashFileTransfer', poVertical, caWholeSize, 18));
  CPanel.BevelOuter := bvNone;
  TButtonPanel(CPanel).FontKoef := 0.5;

  with TButtonPanel(CPanel) do
  begin
    AddButton(FParent.FLT.Caption['����������� �������'], OnCopy);
    AddButton(FParent.FLT.Caption['��������� �������'], OnMove);
    AddButton(FParent.FLT.Caption['�������� ��������� �� Flash'],
      OnNastrToFlash);
    AddButton(FParent.FLT.Caption['��������� ��������� � Flash'],
      OnFromFlashToNastr);
  end;

  ModeProgress := TMainMenuModeFlashInProgress.Create(MainMenu);
end;

procedure TMainMenuModeFlash.LookForFlash;
begin
  if GetFlashDriveNew(Config.FlashDrive, FlashLetter) then
  begin
    if not ModeHaveSet then
    begin
      PrevMode := FParent.CMode;
      FParent.CMode := Self;
      ModeHaveSet := true;
    end;
  end
  else
  begin
    ModeHaveSet := false;
    if FParent.CMode = Self then
    begin
      if TMainMenuWindow(FParent).FlNoHardware then General.RebootProgram
      else FParent.CMode := PrevMode;
    end;
    ModeProgress.FlashLoosed;
  end;
  ModeProgress.PrevMode := PrevMode;
end;

procedure TMainMenuModeFlash.OnCopy(Sender: TObject);
begin
  FParent.CMode := ModeProgress;
  ModeProgress.Transfer(FlashLetter, false);
end;

procedure TMainMenuModeFlash.OnFirmareSelected(id: string);
begin
  FParent.CMode := ModeProgress;
  ModeProgress.ImportSettings(id);
end;

procedure TMainMenuModeFlash.OnFromFlashToNastr(Sender: TObject);
var
  Win: TListWindow;
  sr: TSearchRec;
  Path: string;

begin
  Win := General.GetListWindow;
  Win.Clear;
  Win.ListHeader := FParent.FLT.Caption['�������� ���� � �����������'];
  Win.SelectEnable := true;
  Win.OnItemSelected := OnFirmareSelected;
  Win.WinToExit := 'MAINMENU';
  Win.EnableInputStr:=False;

  Path := FlashLetter + ':\';

  if FindFirst(Path + '*.ego', faAnyFile, sr) = 0 then
  begin
    repeat
      if (sr.Name <> '.') and (sr.Name <> '..') then
        Win.AddItem(Path + sr.Name, sr.Name);
    until FindNext(sr) <> 0;
    FindClose(sr);
  end;

  General.OpenWindow('ListWin');
end;

procedure TMainMenuModeFlash.OnMove(Sender: TObject);
begin
  FParent.CMode := ModeProgress;
  ModeProgress.Transfer(FlashLetter, true);
end;

procedure TMainMenuModeFlash.OnNastrToFlash(Sender: TObject);
begin
  FParent.CMode := ModeProgress;
  ModeProgress.ExportSettings(FlashLetter);
end;

procedure TMainMenuModeFlash.SetActive;
begin
  inherited;
  if assigned(TMainMenuWindow(FParent).ModeReg) then
    TMainMenuWindow(FParent).ModeReg.BottomPanel.Visible := false;
//  if ModeHaveSet then
    TMainMenuWindow(FParent).FGP.Caption := FParent.FLT['��������� Flash-����'];
end;
(*
{ TBootModeFlash }

constructor TBootModeFlash.Create(BootMenu: TBootFlashMenu);
begin
end;

procedure TBootModeFlash.SetActive;
begin
  inherited;
  if assigned(TBootFlashMenu(FParent).ModeReg) then
    TBootFlashMenu(FParent).ModeReg.BottomPanel.Visible := false;
  TBootFlashMenu(FParent).FGP.Caption := FParent.FLT['��������� Flash-����'];
end;

{ TBootFlashMenu }

constructor TBootFlashMenu.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);

begin
  inherited;

  ButList := TList.Create;

  FTimer.Enabled := false;
  FTimer.Interval := 500;

  CreateTop;
  CreateBottom;

  FGP := TGroupBox.Create(nil);
  FGP.Parent := Self;
  FGP.Left := Round(Self.Width * (ButW + 0.02));
  FGP.Top := Round(Self.Height * (ButTop - 0.02));
  FGP.Width := Round(Self.Width - Self.Width * ButW - Self.Width * 0.03);
  FGP.Height := Round(Self.Height * (1- FootH) - FGP.Top);
  FGP.Caption := '';//FLT.Caption[''];
  FGP.Font.Height := Round(Self.Height * ButFontH);
  FGP.Font.Name := DefaultFontName;
  FGP.Font.Color := FCT.Color['BootWin_ButtonText'];
  FGP.Visible := true;
  Obj.Add(FGP);

  PanelManager := TControlPanelManager.Create(FGP, CT, LT);
  PanelManager.Color := FGP.Color;

  CreateButtons;

//  UpDateStatusInfo;
  FTimer.Enabled := true;
//  OnInfoBut(nil);
end;

procedure TBootFlashMenu.CreateButtons;
var
  BsBut, AdBut, MBut, IBut, RgBut, ExBut: TButton;
begin

  ExBut := CreatePrototypeButton();
  ExBut.Caption := FLT.Caption['PowerOff'];
  ExBut.OnClick := OnExit;
  ExBut.Top := Round(Self.Height * (1 - FootH - ButH) );

  ModeFlash := TMainMenuModeFlash.Create(Self);
end;

procedure TBootFlashMenu.CreateTop;
var
  PanTitle: TPanel;
  Title, SubTitle, Vers: TLabel;
begin
  PanTitle := TPanel.Create(nil);
  PanTitle.ParentColor := false;
  PanTitle.Parent := Self;
  PanTitle.ParentBackground := false;
  PanTitle.BevelOuter := bvNone;
  PanTitle.Color := Self.Color;
  PanTitle.Height := Round(Self.Height * PanTitleH);
  PanTitle.Align := alTop;
  Obj.Add(PanTitle);

  Title := TLabel.Create(PanTitle);
  Title.Parent := PanTitle;
  Title.Font.Color := FCT.Color['BootWin_TitleText'];
  Title.Font.Name := DefaultFontName;
  Title.Font.Height := Round(Self.Height * TitleFontH);
  Title.Left := Round(Self.Width * TitleLeft);
  Title.Top := Round(Self.Height * TitleTop);
  Title.Caption := ProgramProfile.ProgramTitle;
  Title.Visible := true;

  SubTitle := TLabel.Create(PanTitle);
  SubTitle.Parent := PanTitle;
  SubTitle.Font.Color := FCT.Color['BootWin_TitleText'];
  SubTitle.Font.Name := DefaultFontName;
  SubTitle.Font.Height := Round(Self.Height * SubTitleFontH);
  SubTitle.Top := Round(Self.Height * SubTitleTop);
  SubTitle.Left := Round(Self.Width * SubTitleLeft);
{$IFNDEF Avikon16_IPV}
  SubTitle.Caption := FLT.Caption['RadAvion'];
{$ENDIF}
  SubTitle.Visible := true;

  Vers := TLabel.Create(PanTitle);
  Vers.Parent := PanTitle;
  Vers.Font.Color := FCT.Color['BootWin_LogText'];
  Vers.Font.Name := DefaultFontName;
  Vers.Font.Height := Round(Self.Height * VerseFontH);
  Vers.Left := Round(Self.Width * VerseLeft);
  Vers.Top := Round(Self.Height * VerseTop);
  Vers.Transparent := true;
{$IFDEF DemoVersion}
  Vers.Caption := FLT.Caption['ProgVersDemo'] + General.ProgramVersion +
    ProgVersStatus;
{$ELSE}
  Vers.Caption := FLT.Caption['ProgVers'] + General.ProgramVersion +
    ProgVersStatus;
{$ENDIF}
  Vers.Visible := true;
end;

procedure TBootFlashMenu.CreateBottom;
var
  Foot: TLabel;
begin
  Foot := TLabel.Create(nil);
  Foot.Parent := Self;
  Foot.Font.Color := clGray;
  Foot.Font.Name := DefaultFontName;
  Foot.Font.Height := Round(Self.Height * FootFontH);
  Foot.Height := Round(Self.Height * FootH); ;
  Foot.Align := alBottom;
  Foot.Alignment := taCenter;
  Foot.Caption := '';
  Foot.Visible := true;
  Obj.Add(Foot);
end;

destructor TBootFlashMenu.Destroy;
begin
//  ModeAdj.Free;
//  ModeInfo.Free;
//  ModeReg.Free;
  ButList.Free;
  inherited;
end;
*)
{ TMainMenuWindow }

constructor TMainMenuWindow.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable{; NoHardware: Boolean=false});

begin
  inherited;// Create(Panel, CT, LT );

  FlNoHardware:=General.FlWorkOnlyFlashDisk;//NoHardware;//F!

  ButList := TList.Create;

  FTimer.Enabled := false;
  FTimer.Interval := 500;

  CreateTop;
  CreateBottom;

  FGP := TGroupBox.Create(nil);
  FGP.Parent := Self;
  FGP.Left := Round(Self.Width * (ButW + 0.02));
  FGP.Top := Round(Self.Height * (ButTop - 0.02));
  FGP.Width := Round(Self.Width - Self.Width * ButW - Self.Width * 0.03);
  FGP.Height := Round(Self.Height * (1- FootH) - FGP.Top);
  FGP.Caption := FLT.Caption['AdjustMode'];
  FGP.Font.Height := Round(Self.Height * ButFontH);
  FGP.Font.Name := DefaultFontName;
  FGP.Font.Color := FCT.Color['BootWin_ButtonText'];
  FGP.Visible := true;
  Obj.Add(FGP);

  PanelManager := TControlPanelManager.Create(FGP, CT, LT);
  PanelManager.Color := FGP.Color;


  if FlNoHardware then begin CreateButtonsNoHardware;
//    SetButtonsEnabled(False)
  end
  else CreateButtons;
  if not FlNoHardware then UpDateStatusInfo;

  FTimer.Enabled := true;
  if not FlNoHardware then  OnInfoBut(nil);
end;

function TMainMenuWindow.CreatePrototypeButton : TButton;
var
  Button: TButton;
  LastButton: TButton;
begin
  // � ���� ���������
  Button := ControlFactory.CreateButton(nil);
  Button.Parent := Self;
  Button.Width := Round(Self.Width * ButW);
  Button.Left := Round(Self.Width * ButLeft);
  Button.Height := Round(Self.Height * ButH);
  Button.Font.Height := Round(Self.Height * ButFontH);
  Button.Font.Name := DefaultFontName;
  Button.Font.Color := FCT.Color['BootWin_ButtonText'];
  Button.Visible := true;

  // � ���� ������
  if ButList.Count = 0 then
    Button.Top := Round(Self.Height * ButTop)
  else
  begin
    LastButton := TButton (ButList.Items[ ButList.Count - 1]);
    Button.Top := Round(LastButton.Top + LastButton.Height * (ButTop +1 ));
  end;
  Obj.Add(Button);
  ButList.Add(Button);

  result := Button;
end;

procedure TMainMenuWindow.CreateButtonsNoHardware;
var
  BsBut, AdBut, MBut, IBut, RgBut, ExBut: TButton;
begin
  BsBut := CreatePrototypeButton();
  BsBut.Caption := FLT.Caption['BScanMode'];
  BsBut.OnClick := Nil;//OnBScan;
  BsBut.Enabled:=False;

  if ProgramProfile.DeviceFamily <> 'RKS-U' then
  begin
    MBut := CreatePrototypeButton();
    MBut.Caption := FLT.Caption['Mnemo'];
    MBut.OnClick := OnMnemo;
    MBut.Enabled := false;
  end;

  AdBut := CreatePrototypeButton();
  AdBut.Caption := FLT.Caption['AdjustMode'];
  AdBut.OnClick := Nil;//OnAdjustBut;
//  ModeAdj := TMainMenuModeAdj.Create(Self);
  AdBut.Enabled:=False;

  if ProgramProfile.DeviceFamily <> 'RKS-U' then
  begin
    RgBut := CreatePrototypeButton();
    RgBut.Caption := FLT.Caption['Registration'];
    RgBut.OnClick := Nil;//OnRegistBut;
//    ModeReg := TMainMenuModeReg.Create(Self);
    RgBut.Enabled:=False;
  end;

  IBut := CreatePrototypeButton();
  IBut.Caption := FLT.Caption['Info'];
  IBut.OnClick := Nil;//OnInfoBut;
//  ModeInfo := TMainMenuModeInfo.Create(Self);
  IBut.Enabled:=False;

  ExBut := CreatePrototypeButton();
  ExBut.Caption := FLT.Caption['PowerOff']; //FLT.Caption['Continue']; //
  ExBut.OnClick := OnExit; //OnContinue;//
  ExBut.Top := Round(Self.Height * (1 - FootH - ButH) );


  ModeFlash := TMainMenuModeFlash.Create(Self);
  CMode:=ModeFlash;
end;

procedure TMainMenuWindow.CreateButtons;
var
  BsBut, AdBut, MBut, IBut, RgBut, ExBut: TButton;
begin
  BsBut := CreatePrototypeButton();
  BsBut.Caption := FLT.Caption['BScanMode'];
  BsBut.OnClick := OnBScan;

  if ProgramProfile.DeviceFamily <> 'RKS-U' then
  begin
    MBut := CreatePrototypeButton();
    MBut.Caption := FLT.Caption['Mnemo'];
    MBut.OnClick := OnMnemo;
    {$IFNDEF USK004R}
    {$IFNDEF TwoThreadsDevice}
    MBut.Enabled := false;
    {$ENDIF}
    {$ENDIF}
  end;

  AdBut := CreatePrototypeButton();
  AdBut.Caption := FLT.Caption['AdjustMode'];
  AdBut.OnClick := OnAdjustBut;
  ModeAdj := TMainMenuModeAdj.Create(Self);

  if ProgramProfile.DeviceFamily <> 'RKS-U' then
  begin
    RgBut := CreatePrototypeButton();
    RgBut.Caption := FLT.Caption['Registration'];
    RgBut.OnClick := OnRegistBut;
    ModeReg := TMainMenuModeReg.Create(Self);
  end;

  IBut := CreatePrototypeButton();
  IBut.Caption := FLT.Caption['Info'];
  IBut.OnClick := OnInfoBut;
  ModeInfo := TMainMenuModeInfo.Create(Self);

  ExBut := CreatePrototypeButton();
  ExBut.Caption := FLT.Caption['PowerOff'];
  ExBut.OnClick := OnExit;
  ExBut.Top := Round(Self.Height * (1 - FootH - ButH) );

  ModeFlash := TMainMenuModeFlash.Create(Self);
end;

procedure TMainMenuWindow.CreateTop;
var
  PanTitle: TPanel;
  Title, SubTitle, Vers: TLabel;
begin
  PanTitle := TPanel.Create(nil);
  PanTitle.ParentColor := false;
  PanTitle.Parent := Self;
  PanTitle.ParentBackground := false;
  PanTitle.BevelOuter := bvNone;
  PanTitle.Color := Self.Color;
  PanTitle.Height := Round(Self.Height * PanTitleH);
  PanTitle.Align := alTop;
  Obj.Add(PanTitle);

  Title := TLabel.Create(PanTitle);
  Title.Parent := PanTitle;
  Title.Font.Color := FCT.Color['BootWin_TitleText'];
  Title.Font.Name := DefaultFontName;
  Title.Font.Height := Round(Self.Height * TitleFontH);
  Title.Left := Round(Self.Width * TitleLeft);
  Title.Top := Round(Self.Height * TitleTop);
  Title.Caption := ProgramProfile.ProgramTitle;
  Title.Visible := true;

  SubTitle := TLabel.Create(PanTitle);
  SubTitle.Parent := PanTitle;
  SubTitle.Font.Color := FCT.Color['BootWin_TitleText'];
  SubTitle.Font.Name := DefaultFontName;
  SubTitle.Font.Height := Round(Self.Height * SubTitleFontH);
  SubTitle.Top := Round(Self.Height * SubTitleTop);
  SubTitle.Left := Round(Self.Width * SubTitleLeft);
{$IFNDEF Avikon16_IPV}
  SubTitle.Caption := FLT.Caption['RadAvion'];
{$ENDIF}
  SubTitle.Visible := true;

  Vers := TLabel.Create(PanTitle);
  Vers.Parent := PanTitle;
  Vers.Font.Color := FCT.Color['BootWin_LogText'];
  Vers.Font.Name := DefaultFontName;
  Vers.Font.Height := Round(Self.Height * VerseFontH);
  Vers.Left := Round(Self.Width * VerseLeft);
  Vers.Top := Round(Self.Height * VerseTop);
  Vers.Transparent := true;
{$IFDEF DemoVersion}
  Vers.Caption := FLT.Caption['ProgVersDemo'] + General.ProgramVersion +
    ProgVersStatus;
{$ELSE}
  Vers.Caption := FLT.Caption['ProgVers'] + General.ProgramVersion +
    ProgVersStatus;
{$ENDIF}
  Vers.Visible := true;
end;

procedure TMainMenuWindow.CreateBottom;
var
  Foot: TLabel;
begin
  Foot := TLabel.Create(nil);
  Foot.Parent := Self;
  Foot.Font.Color := clGray;
  Foot.Font.Name := DefaultFontName;
  Foot.Font.Height := Round(Self.Height * FootFontH);
  Foot.Height := Round(Self.Height * FootH); ;
  Foot.Align := alBottom;
  Foot.Alignment := taCenter;
  Foot.Caption := '';
  Foot.Visible := true;
  Obj.Add(Foot);
end;


destructor TMainMenuWindow.Destroy;
begin
  if not FlNoHardware then begin
    ModeAdj.Free;
    ModeInfo.Free;
    ModeReg.Free
  end;
//  if Assigned(ModeAdj) then ModeAdj.Free;
//  if Assigned(ModeInfo) then ModeInfo.Free;
//  if Assigned(ModeReg) then ModeReg.Free;
  ButList.Free;
  inherited;
end;

procedure TMainMenuWindow.OnAdjustBut(Sender: TObject);
begin
  CMode := ModeAdj;
  FGP.Caption := FLT.Caption['AdjustMode'];
  FGP.Visible := true;
end;

procedure TMainMenuWindow.OnAScan(Sender: TObject);
begin
  General.OpenWindow('ASCAN');
end;

procedure TMainMenuWindow.OnBScan(Sender: TObject);
begin
  General.Mode := bmSearch;
end;

procedure TMainMenuWindow.OnExit(Sender: TObject);
begin
  General.OpenWindow('ExitConfirm');
end;

procedure TMainMenuWindow.OnInfoBut(Sender: TObject);
begin
  CMode := ModeInfo;
  FGP.Visible := true;
{$IFDEF TestSend}
  if Sender <> nil then
  begin
    SendBum.TestSend := not SendBum.TestSend;
    if SendBum.TestSend then
      TButton(Sender).Caption := '����... :)'
    else
      TButton(Sender).Caption := '�� ���� :(';
  end;
{$ENDIF}
end;

procedure TMainMenuWindow.OnMnemo(Sender: TObject);
begin
  General.Mode := bmMnemo;
end;

procedure TMainMenuWindow.OnRegistBut(Sender: TObject);
begin
  CMode := ModeReg;
  FGP.Visible := true;
end;

procedure TMainMenuWindow.OnTimer(Sender: TObject);
begin
  inherited;
  if not FlNoHardware then UpDateStatusInfo;
  ModeFlash.LookForFlash;
end;


procedure TMainMenuWindow.SetButtonsEnabledNoHardware(Enabled: Boolean);
var
  i: integer;
begin
  for i := 0 to ButList.Count - 1 do
    if TButton(ButList[i]).Caption = FLT.Caption['PowerOff'] then begin
      TButton(ButList[i]).Enabled := Enabled;
      break;
    end;
end;

procedure TMainMenuWindow.SetButtonsEnabled(Enabled: Boolean);
var
  i: integer;
begin
  for i := 0 to ButList.Count - 1 do
    TButton(ButList[i]).Enabled := Enabled;
end;

procedure TMainMenuWindow.UpDateStatusInfo;
var
  AK: Byte;
  CColor: TColor;
  RegStat: string;
  BUMUakk: Single;
  tmp: string;
begin
  // ���������� ����������
  if General.Core.RegOnUserLevel then
  begin
    CColor := FCT.Color['AK_Good'];
    RegStat := FLT.Caption['RegOn'];
  end
  else
  begin
    CColor := FCT.Color['AK_BAD'];
    RegStat := FLT.Caption['RegOff'];
  end;
  if General.Core.RegistrationStatus = rsPause then
  begin
    CColor := FCT.Color['AK_Warning'];
    RegStat := FLT.Caption['RegPaused'];
  end;

  ModeInfo.RegStat.TextColor := CColor;
  ModeInfo.RegStat.Value := RegStat;

  if Assigned(General.PowerManager) then
  begin
    AK := General.PowerManager.BatteryLifePercent;
    if AK < 20 then
      CColor := FCT.Color['AK_BAD']
    else if ((AK >= 20) and (AK < 50)) then
      CColor := FCT.Color['AK_Warning']
    else
      CColor := FCT.Color['AK_Good'];

    ModeInfo.AKBUI.TextColor := CColor;
    if AK = 255 then begin
      ModeInfo.AKBUI.Value := FLT.Caption['�� ����'];
      if Config.Language = 'Spanish' then
        ModeInfo.AKBUI.ObjLeft := Round(Screen.Width * 0.3)
      else ModeInfo.AKBUI.ObjLeft := Round(Screen.Width * 0.35)
    end
    else begin
      ModeInfo.AKBUI.Value := Format('%d%%', [Min(AK, 100)]);
      ModeInfo.AKBUI.ObjLeft := Round(Screen.Width * 0.35)
    end;
  end;

  // ���� �������� ���������� ������������ ��� !
  (*<Rud45>*)
{$IFDEF USK004R}
//  ModeInfo.U2CVer.Value:=Format('%d', [General.Core.GetU2CVersion(0)]);
  tmp:=General.Core.GetU2CVersion(0);
{$ENDIF}
  (*</Rud45>*)
  BUMUakk:= General.Core.GetUAKK / 10;
  if ProgramProfile.DeviceFamily <> 'RKS-U' then
  begin
    if BUMUakk > 0.5 then
      ModeInfo.AKBUM.Value := Format('%2.1f %s', [BUMUakk, FLT.Caption['�']])
    else
      ModeInfo.AKBUM.Value := Format('%s', [FLT.Caption['No data']]);
  end;


{$IFDEF UseHardware}

  {$IFDEF Avikon16_IPV}
  ModeInfo.U2CSN.Value := Format('%d', [General.Core.GetU2CSN(0)]);
   {$IFDEF BUMCOUNT4}
  ModeInfo.U2CSN2.Value := Format('%d', [General.Core.GetU2CSN(1)]);
   {$ENDIF}
  {$ENDIF}
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
  ModeInfo.U2CSN.Value := Format('%d', [General.Core.GetU2CSN(0)]);
   {$IFDEF BUMCOUNT4}
  ModeInfo.U2CSN2.Value := Format('%d', [General.Core.GetU2CSN(1)]);
   {$ENDIF}
{$IFEND}

{$IFDEF USK004R}
  ModeInfo.U2CSN.Value := Format('%d', [General.Core.GetU2CSN(0)]);
{$ENDIF}


{$IFDEF SecondUSBRemotePanel}

{$IF NOT DEFINED(Avikon16_IPV) AND NOT DEFINED(EGO_USW) AND NOT DEFINED(VMT_US)}
  BUMUakk:= General.Core.GetUAKK;
  if BUMUakk > 0.5 then
    ModeInfo.AKBUM.Value := Format('%d %s', [General.Core.GetUAKK, FLT.Caption['�']])
  else
    ModeInfo.AKBUM.Value := Format('%s', [FLT.Caption['No data']]);
{$IFEND}
{$ENDIF}
  ModeInfo.SNBUM.Value := General.Core.BUMSerialNum[0];
{$IFDEF BUMCOUNT2}
  {$IFnDEF USK004R}
  ModeInfo.SNBUM2.Value := General.Core.BUMSerialNum[1];
  {$ENDIF}
{$ELSE}
 {$IFDEF BUMCOUNT4}
  ModeInfo.SNBUM2.Value := General.Core.BUMSerialNum[1];
  ModeInfo.SNBUM3.Value := General.Core.BUMSerialNum[2];
  ModeInfo.SNBUM4.Value := General.Core.BUMSerialNum[3];
 {$ENDIF}
{$ENDIF}

  ModeInfo.SNBUI.Value := General.BUISN;
  ModeInfo.POBUM.Value := Format
    ('%1.2f', [General.Core.CANList[0].FirmwareInfo.FirmwareVers]);
{$IFDEF BUMCOUNT2}
  {$IFnDEF USK004R}
  ModeInfo.POBUM2.Value := Format
    ('%1.2f', [General.Core.CANList[1].FirmwareInfo.FirmwareVers]);
  {$ENDIF}
{$ELSE IF BUMCOUNT4}
  ModeInfo.POBUM2.Value := Format
    ('%1.2f', [General.Core.CANList[1].FirmwareInfo.FirmwareVers]);
  ModeInfo.POBUM3.Value := Format
    ('%1.2f', [General.Core.CANList[2].FirmwareInfo.FirmwareVers]);
  ModeInfo.POBUM4.Value := Format
    ('%1.2f', [General.Core.CANList[3].FirmwareInfo.FirmwareVers]);
{$ENDIF}
  ModeInfo.POBUI.Value := General.GetProgVer;
{$ENDIF}
  if ProgramProfile.DeviceFamily = 'Avicon16' then
    begin
      case Config.SoundedScheme of
        Scheme1: ModeInfo.CURScheme.Value := FLT['�����-1'];
        Scheme2: ModeInfo.CURScheme.Value := FLT['�����-2'];
        Scheme3: ModeInfo.CURScheme.Value := FLT['�����-3'];
        Wheels: ModeInfo.CURScheme.Value := FLT['���'];
      end;
    end;
  if ProgramProfile.DeviceFamily = 'EGO-USW' then
    begin
      case Config.SoundedScheme of
        Scheme1: ModeInfo.CURScheme.Value := FLT['�����-1'];
        Scheme2: ModeInfo.CURScheme.Value := FLT['�����-2'];
        Scheme3: ModeInfo.CURScheme.Value := FLT['�����-3'];
      end;
    end;

  if ProgramProfile.DeviceFamily = 'Avicon14' then
    begin
      case Config.SoundedScheme of
        Scheme1: ModeInfo.CURScheme.Value := FLT['����������'];
        Wheels: ModeInfo.CURScheme.Value := FLT['�������'];
      end;
    end;

{$IFDEF GPS}
 with General do
  if Assigned(GPSM) then
  if GPSM.Connected then
    if GPSM.State then begin
      ModeInfo.SLatitude.Value:=GPSSingleToText( GPSM.Latitude, True );
      ModeInfo.SLongitude.Value:=GPSSingleToText( GPSM.Longitude, False );
    end
    else begin
      ModeInfo.SLatitude.Value:={'0';}GPSSingleToText( 0, True );
      ModeInfo.SLongitude.Value:={'0';}GPSSingleToText( 0, False );
    end
  else  begin
      ModeInfo.SLatitude.Value:=FLT['No connection'];
      ModeInfo.SLongitude.Value:=FLT['No connection'];
  end;
{$ENDIF}
end;

end.
