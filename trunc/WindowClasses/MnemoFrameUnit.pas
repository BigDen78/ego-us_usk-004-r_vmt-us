unit MnemoFrameUnit;

interface
{$I Directives.inc}

uses
  Grids, Windows, ExtCtrls, StdCtrls, Forms, Graphics, Controls, SysUtils,
  Classes,
  CfgTablesUnit_2010, AviconTypes,
  CustomWindowUnit, DefCoreUnit, MCan, MnemoUnit, ConfigObj;

type
  TWheelPos = array [0 .. 1, 1 .. 2] of integer; // ��������� ����� � ��������. 0-1 - ����� 1-2 - ������ ��� ������ ������
  THeadY = array [0 .. 1] of integer; // ���������� Y ����������� �������, 0-1 �����

  TSimpleMnemoSegmentsPics = array [0 .. 1, 0 .. 9] of TImage;

  TSimpleMnemoMode = (mmDefault, mmTwoEcho);

  TAKYesNoPic = record
    Yes, No: TPicture;
  end;

  TAKPics = array [0 .. 1, 1 .. 2] of TAKYesNoPic;

  TCustomSimpleMnemoFrame_Old = class(TCustomWindow)
  private
    FPB: TPaintBox;
    FBmp: TBitMap;
    FBmp2: TBitMap;
    FTestKanals: TTestKanalsASD;
  protected
  public
    FSegments: TMnemoSegmentsContainer;
    procedure PBPaint(Sender: TObject);
    procedure Refresh;
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
    destructor Destroy; override;
    procedure Resize; override;
    procedure TestKanal(Rail, Num: integer);
  end;

  TCustomSimpleMnemoFrame = class(TCustomWindow)
  private
    FMnemo: TCustomMnemoScheme;
  public
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
    procedure TestKanal(Nit, Kanal, Delay, Amp: integer);
    procedure Refresh;
  end;

  TIPVMnemo = class(TCustomSimpleMnemoFrame_Old)
  private
    FSchemeNum: integer;
  protected
  public
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable;
      SchemeNum: integer);
    destructor Destroy; override;
  end;

  TAv11Mnemo = class(TCustomSimpleMnemoFrame_Old)
  private
    F2Echo: Boolean;
    F1EchoSeg: TMnemoSegmentsContainer;
    F2EchoSeg: TMnemoSegmentsContainer;
  protected
    procedure Set2Echo(Value: Boolean);
    function Get2Echo: Boolean;
  public
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
    destructor Destroy; override;
    property TwoEcho: Boolean read Get2Echo write Set2Echo;
  end;

  TMnemoFrame = class(TCustomWindow)
  private
    FTempSG: TStringGrid;
    FPic: TPicture;
    FAKPic: TAKPics;
    FPicRect: TRect;
    FBmp: TBitMap;
    FBmp2: TBitMap;
    FPB: TPaintBox;
    FKW, FKH: Single;
    // ������ ������ � ���
    FBottomT: Byte;
    // ���������� � �������� ����� ��������� ����� � ������ �� ��������
    // FWheel1Left, FWheel2Left, FWheel1Right, FWheel2Right: Integer;
    // FWheelPos: TWheelPos;
    FWheelX, FWheelY: TWheelPos;
    // ����������� ����������� �������
    // FHeadYLeft, FHeadYRight: Integer;
    FHeadY: THeadY;
    // ������ ������� � ������ ������ � �������� (��� ������ 0)
    FHeadHeight, FRailHeight: integer;
    // ������ ������� � �������� (��� ������ 58)
    FHeadHeight58: integer;
    // ������ ������ � �������� (��� ������ 43)
    FRailHeight42: integer;
    // ������ ������� � �������� (��� ������ 70)
    FHeadHeight70: integer;
    // ��������� ��������
    FPicWidth, FPicHeight: integer;
    FHeadSecondPoint58: integer;
    FTgAlfa, FTgBetta, FTgGamma: Single;

    Alfa42_1, Alfa42_2, Alfa58_1, Alfa58_1_P1, Alfa58_1_P2, Alfa58_2,
      Alfa58_2_P1, Alfa58_2_P2, Alfa70_1, Alfa70_2: Single;
    Max42_1, Max42_2, Max58_1, Max58_1_P1, Max58_1_P2, Max58_2, Max58_2_P1,
      Max58_2_P2, Max70_1, Max70_2: integer;
    PointF58_1X, PointF58_2X, PointB58_1X, PointB58_2X: THeadY;

    FPointR: integer;

    function BotY(X: integer): integer;
    function TopY(X: integer): integer;
    function HBotY(X: integer): integer;
    procedure DrawChannel(Nit, Kanal, Delay, Amp: integer);
    procedure DrawWheel(Nit, WheelNum: integer; AK: Boolean);
    procedure CountLines;
  protected
  public
    procedure Refresh;
    procedure Resize; override;
    procedure PBPaint(Sender: TObject);
    procedure Test(Nit, Kanal, Delay, Amp: integer);
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
      override;
    destructor Destroy; override;
  end;

implementation

uses
  MainUnit;


{ TCustomSimpleMnemoFrame_Old }

constructor TCustomSimpleMnemoFrame_Old.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);
var
  i, j: integer;
  BaseFileName, Kanal2LFileName, Kanal2RFileName: String;

begin
  inherited Create(Panel, CT, LT);

  FSegments := nil;

  Self.Color := clWhite;
  Self.ParentDoubleBuffered := false;
  Self.DoubleBuffered := true;

  FPB := TPaintBox.Create(Self);
  FPB.Parent := Self;
  FPB.Align := alBottom;
  FPB.Visible := true;

  FBmp := TBitMap.Create;
  FBmp2 := TBitMap.Create;

  FPB.OnPaint := PBPaint;
  FPB.RePaint;

  for i := 0 to 1 do
    for j := 0 to 11 do
      FTestKanals[i, j].flag := false;
end;

destructor TCustomSimpleMnemoFrame_Old.Destroy;
begin
  FBmp.Free;
  FBmp2.Free;
  if Assigned(FSegments) then
    FSegments.Free;
  inherited;
end;

procedure TCustomSimpleMnemoFrame_Old.PBPaint(Sender: TObject);
begin
  FPB.Canvas.Draw(0, 0, FBmp2);
end;

procedure TCustomSimpleMnemoFrame_Old.Refresh;
var
  i, j, a: integer;
begin
  FBmp2.Canvas.Draw(0, 0, FBmp);
  for i := 0 to 1 do
    for j := 0 to 11 do
    begin
      if j <= High(Kanals[i]) then
        if Assigned(Kanals[i, j]) then
        begin
          if (Kanals[i, j].ASD) then
          begin
            a := j;
            if j in [10, 11] then
              a := j - 8;
            if Assigned(FSegments) then
              if Assigned(FSegments[i, a]) then
                FBmp2.Canvas.StretchDraw( Rect(0, 0, FBmp2.Width, FBmp2.Height),
                                          FSegments[i, a].Picture.Graphic);
          end;
        end;
    end;
  FPB.RePaint;
end;

procedure TCustomSimpleMnemoFrame_Old.Resize;
begin
  inherited;
  FPB.Height := Self.Height;
  FBmp.Width := FPB.Width;
  FBmp.Height := FPB.Height;
  FBmp2.Width := FPB.Width;
  FBmp2.Height := FPB.Height;
  if Assigned(FSegments) then
    if Assigned(FSegments.Base) then
    begin
      FBmp.Canvas.Brush.Color := clWhite;
      FBmp.Canvas.FillRect(Rect(0, 0, FBmp.Width, FBmp.Height));
      FBmp.Canvas.StretchDraw(Rect(0, 0, FBmp.Width, FBmp.Height),
         FSegments.Base.Picture.Metafile);
    end;
  FBmp2.Canvas.Draw(0, 0, FBmp);
  FPB.RePaint;
end;

procedure TCustomSimpleMnemoFrame_Old.TestKanal(Rail, Num: integer);
begin
  FTestKanals[Rail, Num].flag := not FTestKanals[Rail, Num].flag;
  Self.Refresh;
end;

{ TCustomSimpleMnemoFrame }

constructor TCustomSimpleMnemoFrame.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);
begin
  inherited Create(Panel, CT, LT);

  Self.Color := clWhite;
  Self.ParentDoubleBuffered := false;
  Self.DoubleBuffered := true;
{$IFDEF Avikon16_IPV}
  if Config.SoundedScheme = Scheme1 then
    FMnemo := TAV16Scheme1Mnemo.Create
      (Self, 2, 12, ExtractFilePath(Application.ExeName) + 'Pic\SimpleMnemo\')
  else if Config.SoundedScheme = Scheme2 then
    FMnemo := TAV16Scheme2Mnemo.Create
      (Self, 2, 12, ExtractFilePath(Application.ExeName) + 'Pic\SimpleMnemo\')

  else if Config.SoundedScheme = Scheme3 then
    FMnemo := TAV16Scheme3Mnemo.Create
      (Self, 2, 13, ExtractFilePath(Application.ExeName) + 'Pic\SimpleMnemo\')

  else if Config.SoundedScheme = Wheels then
    FMnemo := TAV16WheelsMnemo.Create
      (Self, 2, 14, ExtractFilePath(Application.ExeName) + 'Pic\');
{$ENDIF}
{$IFDEF Avikon14}
  FMnemo := TAV16WheelsMnemo.Create(Self, 2, 14, ExtractFilePath
      (Application.ExeName) + 'Pic\');
{$ENDIF}
{$IFDEF Avikon14_2Block}
  FMnemo := TAV16WheelsMnemo.Create(Self, 2, 14, ExtractFilePath
      (Application.ExeName) + 'Pic\');
{$ENDIF}
//
{$IF DEFINED(EGO_USW) OR DEFINED(VMT_US)}
  FMnemo := TAV16WheelsMnemo.Create(Self, 2, 14, ExtractFilePath
      (Application.ExeName) + 'Pic\');
      //add other schemes!!!
{$IFEND}
//
{$IFDEF USK004R}
    FMnemo := TUSK004RSchemeMnemo.Create
      (Self, 2, 12, ExtractFilePath(Application.ExeName) + 'Pic\USKMnemo\');
{$ENDIF}


  if Assigned(General.Core) then
    FMnemo.Core := General.Core;
  FMnemo.Parent := Self;
  FMnemo.Colors := CT;
  FMnemo.Language := LT;
  FMnemo.Align := alClient;
end;

procedure TCustomSimpleMnemoFrame.Refresh;
begin
  if Assigned(FMnemo) then
    FMnemo.Refresh;

end;

procedure TCustomSimpleMnemoFrame.TestKanal(Nit, Kanal, Delay, Amp: integer);
begin
  if Assigned(FMnemo) then
    FMnemo.TestKanal(Nit, Kanal, Delay, Amp);
end;

{ TIVPMnemo }

constructor TIPVMnemo.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable; SchemeNum: integer);
begin
  inherited Create(Panel, CT, LT);
  Panel.ParentDoubleBuffered := false;
  Panel.DoubleBuffered := true;
  FSchemeNum := SchemeNum;

  FSegments := TMnemoSegmentsContainer.Create
    (2, 10, ExtractFilePath(Application.ExeName) + 'Pic\SimpleMnemo\');
  if FSchemeNum = 1 then
  begin
    FSegments.AddBase('1-NULL.wmf');
    FSegments.AddSegment(0, 2, '1-58N-L.wmf');
    FSegments.AddSegment(1, 2, '1-58N-R.wmf');
  end
  else
  begin
    FSegments.AddBase('2-NULL.wmf');
    FSegments.AddSegment(0, 2, '2-58N-L.wmf');
    FSegments.AddSegment(1, 2, '2-58N-R.wmf');
  end;

  FSegments.AddSegment(0, 0, '0ZTM-L.wmf');
  FSegments.AddSegment(0, 1, '1-echo-L.wmf');
  FSegments.AddSegment(0, 3, '1-58O-L.wmf');
  FSegments.AddSegment(0, 4, '1-70N-L.wmf');
  FSegments.AddSegment(0, 5, '1-70O-L.wmf');
  FSegments.AddSegment(0, 6, '1-42NW-L.wmf');
  FSegments.AddSegment(0, 7, '1-42OW-L.wmf');
  FSegments.AddSegment(0, 8, '1-42NB-L.wmf');
  FSegments.AddSegment(0, 9, '1-42OB-L.wmf');

  FSegments.AddSegment(1, 0, '0ZTM-R.wmf');
  FSegments.AddSegment(1, 1, '1-echo-R.wmf');
  FSegments.AddSegment(1, 3, '1-58O-R.wmf');
  FSegments.AddSegment(1, 4, '1-70N-R.wmf');
  FSegments.AddSegment(1, 5, '1-70O-R.wmf');
  FSegments.AddSegment(1, 6, '1-42NW-R.wmf');
  FSegments.AddSegment(1, 7, '1-42OW-R.wmf');
  FSegments.AddSegment(1, 8, '1-42NB-R.wmf');
  FSegments.AddSegment(1, 9, '1-42OB-R.wmf');
end;

destructor TIPVMnemo.Destroy;
begin
  inherited;
end;

{ TAv11Mnemo }

constructor TAv11Mnemo.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);
begin
  inherited Create(Panel, CT, LT);

  F1EchoSeg := TMnemoSegmentsContainer.Create
    (2, 10, ExtractFilePath(Application.ExeName) + 'Pic\Av11Mnemo\');
  F2EchoSeg := TMnemoSegmentsContainer.Create
    (2, 10, ExtractFilePath(Application.ExeName) + 'Pic\Av11Mnemo\');

  F1EchoSeg.AddBase('Base1.wmf');

  F1EchoSeg.AddSegment(0, 0, '0ZTML.wmf');
  F1EchoSeg.AddSegment(0, 1, '0echoL.wmf');
  F1EchoSeg.AddSegment(0, 2, '58HL.wmf');
  F1EchoSeg.AddSegment(0, 3, '58OL.wmf');
  F1EchoSeg.AddSegment(0, 4, '70L.wmf');
  F1EchoSeg.AddSegment(0, 5, '58ZL.wmf');
  F1EchoSeg.AddSegment(0, 6, '42WHL.wmf');
  F1EchoSeg.AddSegment(0, 7, '42WOL.wmf');
  F1EchoSeg.AddSegment(0, 8, '42BHL.wmf');
  F1EchoSeg.AddSegment(0, 9, '42BOL.wmf');

  F1EchoSeg.AddSegment(1, 0, '0ZTMR.wmf');
  F1EchoSeg.AddSegment(1, 1, '0echoR.wmf');
  F1EchoSeg.AddSegment(1, 2, '58HR.wmf');
  F1EchoSeg.AddSegment(1, 3, '58OR.wmf');
  F1EchoSeg.AddSegment(1, 4, '70R.wmf');
  F1EchoSeg.AddSegment(1, 5, '58ZR.wmf');
  F1EchoSeg.AddSegment(1, 6, '42WHR.wmf');
  F1EchoSeg.AddSegment(1, 7, '42WOR.wmf');
  F1EchoSeg.AddSegment(1, 8, '42BHR.wmf');
  F1EchoSeg.AddSegment(1, 9, '42BOR.wmf');

  F2EchoSeg.AddBase('Base2.wmf');

  F2EchoSeg.AddSegment(0, 0, '0ZTML.wmf');
  F2EchoSeg.AddSegment(0, 1, '0echoL-BJ.wmf');
  F2EchoSeg.AddSegment(0, 2, '58HL.wmf');
  F2EchoSeg.AddSegment(0, 3, '58OL.wmf');
  F2EchoSeg.AddSegment(0, 4, '70L.wmf');
  F2EchoSeg.AddSegment(0, 5, '58ZL.wmf');
  F2EchoSeg.AddSegment(0, 6, '42WHL-BJ.wmf');
  F2EchoSeg.AddSegment(0, 7, '42WOL-BJ.wmf');
  F2EchoSeg.AddSegment(0, 8, '42BHL.wmf');
  F2EchoSeg.AddSegment(0, 9, '42BOL.wmf');

  F2EchoSeg.AddSegment(1, 0, '0ZTMR.wmf');
  F2EchoSeg.AddSegment(1, 1, '0echoR-BJ.wmf');
  F2EchoSeg.AddSegment(1, 2, '58HR.wmf');
  F2EchoSeg.AddSegment(1, 3, '58OR.wmf');
  F2EchoSeg.AddSegment(1, 4, '70R.wmf');
  F2EchoSeg.AddSegment(1, 5, '58ZR.wmf');
  F2EchoSeg.AddSegment(1, 6, '42WHR-BJ.wmf');
  F2EchoSeg.AddSegment(1, 7, '42WOR-BJ.wmf');
  F2EchoSeg.AddSegment(1, 8, '42BHR.wmf');
  F2EchoSeg.AddSegment(1, 9, '42BOR.wmf');

  F2Echo := false;
  FSegments := F1EchoSeg;
end;

destructor TAv11Mnemo.Destroy;
begin
  FSegments := nil;
  F1EchoSeg.Free;
  F2EchoSeg.Free;
  inherited;
end;

function TAv11Mnemo.Get2Echo: Boolean;
begin
  Result := F2Echo;
end;

procedure TAv11Mnemo.Set2Echo(Value: Boolean);
begin
  if F2Echo <> Value then
  begin
    F2Echo := Value;
    if F2Echo then
      FSegments := F2EchoSeg
    else
      FSegments := F1EchoSeg;
    Self.Resize;
  end;
end;

{ TMnemoFrame }

function TMnemoFrame.BotY(X: integer): integer;
begin
  if X < FBmp.Width / 2 then
    Result := Round(FBmp.Height - X * FTgAlfa)
  else
    Result := Round(FBmp.Height - (FBmp.Width - X) * FTgAlfa)
end;

function TMnemoFrame.TopY(X: integer): integer;
begin
  if X < FBmp.Width / 2 then
    Result := Round(FBmp.Height - FRailHeight + (X - FBmp.Width * 0.049)
        * FTgBetta)
  else
    Result := Round(FBmp.Height - FRailHeight +
        ((FBmp.Width - X) - FBmp.Width * 0.049) * FTgBetta)
end;

function TMnemoFrame.HBotY(X: integer): integer;
begin
  if X < FBmp.Width / 2 then
    Result := Round(FBmp.Height - FHeadHeight + (X - FBmp.Width * 0.09)
        * FTgGamma)
  else
    Result := Round(FBmp.Height - FHeadHeight +
        ((FBmp.Width - X) - FBmp.Width * 0.09) * FTgGamma)
end;

constructor TMnemoFrame.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);
var
  R, w, H, X42, X58, X70, i, j: integer;
  s, a, B, c, D: Single;

begin
  inherited;
  FTimer.Enabled := false;
  Self.ParentDoubleBuffered := false;
  Self.DoubleBuffered := true;
  Self.BevelOuter := bvNone;
  Self.Align := alClient;
  FTempSG := TStringGrid.Create(Self);
  FTempSG.Parent := Self;
  FTempSG.Left := 20;
  FTempSG.Top := 20;
  FTempSG.Width := 300;
  FTempSG.Height := 500;
  FTempSG.RowCount := 17;
  FTempSG.Cells[0, 0] := '�����';
  FTempSG.Cells[1, 0] := '������������';
  FTempSG.Cells[2, 0] := '��������';
  FTempSG.Cells[3, 0] := '���������';
  FTempSG.Cells[0, 1] := '1_0';
  FTempSG.Cells[0, 2] := '1_1';
  FTempSG.Cells[0, 3] := '1_2';
  FTempSG.Cells[0, 4] := '1_3';
  FTempSG.Cells[0, 5] := '1_4';
  FTempSG.Cells[0, 6] := '1_5';
  FTempSG.Cells[0, 7] := '1_6';
  FTempSG.Cells[0, 8] := '1_7';
  FTempSG.Cells[0, 9] := '2_0';
  FTempSG.Cells[0, 10] := '2_1';
  FTempSG.Cells[0, 11] := '2_2';
  FTempSG.Cells[0, 12] := '2_3';
  FTempSG.Cells[0, 13] := '2_4';
  FTempSG.Cells[0, 14] := '2_5';
  FTempSG.Cells[0, 15] := '2_6';
  FTempSG.Cells[0, 16] := '2_7';
  // FTempSG.Visible:=true;
  FTempSG.Visible := false;

  // ������ ����� �� ������, ������� ����������� �������� ������������
  FPointR := 15;

  Self.Color := clWhite;

  FPB := TPaintBox.Create(Self);
  FPB.Parent := Self;
  FPB.Align := alBottom;
  FPB.Visible := true;

  FPic := TPicture.Create;
  FPic.LoadFromFile(ExtractFilePath(Application.ExeName) + 'Pic\M.wmf');

  for i := 0 to 1 do
    for j := 1 to 2 do
    begin
      FAKPic[i, j].Yes := TPicture.Create;
      FAKPic[i, j].Yes.LoadFromFile(ExtractFilePath(Application.ExeName)
          + 'Pic\' + Format('W%d_%d_%s.wmf', [i, j, 'YES']));
      FAKPic[i, j].No := TPicture.Create;
      FAKPic[i, j].No.LoadFromFile(ExtractFilePath(Application.ExeName)
          + 'Pic\' + Format('W%d_%d_%s.wmf', [i, j, 'NO']));
    end;

  FBmp := TBitMap.Create;
  FBmp.Width := 1024;
  FBmp.Height := 768;

  Self.CountLines;

  FBmp2 := TBitMap.Create;
  FBmp2.Width := FPB.Width;
  FBmp2.Height := FPB.Height;
  FBmp2.Canvas.Brush.Color := clWhite;

  FPB.OnPaint := PBPaint;
  FPB.RePaint;
end;

destructor TMnemoFrame.Destroy;
begin
  FBmp.Free;
  FBmp2.Free;
  FPic.Free;
  inherited;
end;

procedure TMnemoFrame.PBPaint(Sender: TObject);
var
  a, B: integer;
begin
  FPB.Canvas.Draw(0, 0, FBmp);
  a := FPB.Top;
  B := FPB.Height;
end;

procedure TMnemoFrame.Refresh;
var
  Scan: TBScanArray;
  i, j, c: integer;
  Max: array [0 .. 1] of TEchoBlock;
  MaxA, MaxIdx: Byte;
  Wheel1Shift, Wheel2Shift: integer;
begin
  // ���� �������� ���� - ��� ��������� �������� !! �������� ��� �������� !
  Scan := General.Core.GetLastBScan;
  for i := 0 to 1 do
    for j := 0 to 7 do
    begin
      MaxA := Scan[i, j].Echos[0].a;
      MaxIdx := 0;
      for c := 0 to Scan[i, j].EchoCount - 1 do
        if Scan[i, j].Echos[c].a > MaxA then
        begin
          MaxA := Scan[i, j].Echos[c].a;
          MaxIdx := c;
        end;
      Max[i, j].t := Scan[i, j].Echos[MaxIdx].t;
      Max[i, j].a := Scan[i, j].Echos[MaxIdx].a;
    end;

  // ���������� ��� ������
  FBottomT := Round((General.Core.Kanal[0, 0].Str_en - General.Core.Kanal[0, 0]
        .Str_st) / 2);

  FBmp2.Canvas.Draw(0, 0, FBmp);
  // ���������� ��
  if { General.Core.ASD[0,0] } Kanals[0, 0].ASD then
  begin
    DrawWheel(0, 1, false);
    DrawWheel(0, 2, false);
  end
  else
  begin
    DrawWheel(0, 1, true);
    DrawWheel(0, 2, true);
  end;
  if { General.Core.ASD[1,0] } Kanals[1, 0].ASD then
  begin
    DrawWheel(1, 1, false);
    DrawWheel(1, 2, false);
  end
  else
  begin
    DrawWheel(1, 1, true);
    DrawWheel(1, 2, true);
  end;
  for i := 0 to 1 do
    for j := 1 to 7 do
      if Kanals[i, j].ASD then
        DrawChannel(i, j, Max[i, j].t, Max[i, j].a);
  FPB.RePaint;
end;

procedure TMnemoFrame.Resize;
var
  K: Single;
begin
  inherited;
  FKW := Self.Width / FPic.Metafile.Width;
  FKH := (Self.Width * 411 / 755) / FPic.Metafile.Height;

  FPicWidth := Self.Width;
  K := FPicWidth / FBmp.Width;

  FPB.Height := Self.Height;
  FBmp2.Width := FPB.Width;
  FBmp2.Height := FPB.Height;
  Self.CountLines;
  Self.Test(0, 0, 0, 0);
end;

procedure TMnemoFrame.Test(Nit, Kanal, Delay, Amp: integer);
var
  c, j, dY, X, Y, R, Max: integer;
  Alfa: Single;
  DrawAllow: Boolean;
  NitK: integer;
begin
  R := 20;
  FBottomT := 60; // ��� ������������ ���� ������ ������������� �65
  if Assigned(FTempSG) then
  begin
    if Nit = 0 then
      c := 0
    else
      c := 8;
    j := Kanal;
    FTempSG.Cells[1, j + 1 + c] := '���� !';
    FTempSG.Cells[2, j + 1 + c] := IntToStr(Delay);
    FTempSG.Cells[3, j + 1 + c] := IntToStr(Amp);
  end;

  FBmp2.Canvas.Draw(0, 0, FBmp);
  DrawWheel(0, 1, true);
  DrawWheel(0, 2, false);
  DrawWheel(1, 1, true);
  DrawWheel(1, 2, true);

  if Nit = 0 then
    NitK := 1
  else
    NitK := -1;
  if Delay < 0 then
    DrawAllow := false
  else
    DrawAllow := true;
  case Kanal of
    1:
      begin
        X := FWheelX[Nit, 2];
        Y := TopY(X) - Round(((TopY(X) - BotY(X)) / FBottomT) * Delay);
        if Delay >= FBottomT then
          DrawAllow := false;
      end;
    6:
      begin
        X := Round(NitK * sin(Alfa42_1) * (((Max42_1) / 184) * Delay))
          + FWheelX[Nit, 1];
        Y := Round(cos(Alfa42_1) * (((Max42_1) / 184) * Delay)) + TopY
          (FWheelX[Nit, 1]);
      end;
    7:
      begin
        X := Round(-NitK * sin(Alfa42_2) * (((Max42_2) / 184) * Delay))
          + FWheelX[Nit, 2];
        Y := Round(cos(Alfa42_2) * (((Max42_2) / 184) * Delay)) + TopY
          (FWheelX[Nit, 2]);
      end;
    4:
      begin
        X := Round(NitK * sin(Alfa70_1) * (((Max70_1) / 91) * Delay))
          + FWheelX[Nit, 1];
        Y := Round(cos(Alfa70_1) * (((Max70_1) / 91) * Delay)) + TopY
          (FWheelX[Nit, 1]);
      end;
    5:
      begin
        X := Round(NitK * sin(Alfa70_2) * (((Max70_2) / 91) * Delay))
          + FWheelX[Nit, 2];
        Y := Round(cos(Alfa70_2) * (((Max70_2) / 91) * Delay)) + TopY
          (FWheelX[Nit, 2]);
      end;
    2:
      begin
        if (Delay > 0) and (Delay <= 40) then
        begin
          X := Round(NitK * sin(Alfa58_1) * (((Max58_1) / 40) * Delay))
            + FWheelX[Nit, 1];
          Y := Round(cos(Alfa58_1) * (((Max58_1) / 40) * Delay)) + TopY
            (FWheelX[Nit, 1]);
        end;
        if (Delay > 40) and (Delay <= 80) then
        begin
          X := Round(-NitK * sin(Alfa58_1_P1) *
              (((Max58_1_P1) / 40) * (Delay - 40))) + PointF58_1X[Nit];
          Y := Round(-cos(Alfa58_1_P1) * (((Max58_1_P1) / 40) * (Delay - 40)))
            + HBotY(FWheelX[Nit, 1]);
        end;
        if (Delay > 80) and (Delay <= 130) then
        begin
          X := Round(NitK * sin(Alfa58_1_P2) *
              (((Max58_1_P2) / 40) * (Delay - 80))) + PointF58_2X[Nit];
          Y := Round(cos(Alfa58_1_P2) * (((Max58_1_P2) / 40) * (Delay - 80)))
            + TopY(PointF58_2X[Nit]);
        end;
      end;
    3:
      begin
        if (Delay > 0) and (Delay <= 40) then
        begin
          X := Round(NitK * sin(Alfa58_2) * (((Max58_2) / 40) * Delay))
            + FWheelX[Nit, 2];
          Y := Round(cos(Alfa58_2) * (((Max58_2) / 40) * Delay)) + TopY
            (FWheelX[Nit, 2]);
        end;
        if (Delay > 40) and (Delay <= 80) then
        begin
          X := Round(-NitK * sin(Alfa58_2_P1) *
              (((Max58_2_P1) / 40) * (Delay - 40))) + PointB58_1X[Nit];
          Y := Round(-cos(Alfa58_2_P1) * (((Max58_2_P1) / 40) * (Delay - 40)))
            + HBotY(FWheelX[Nit, 2]);
        end;
        if (Delay > 80) and (Delay <= 130) then
        begin
          X := Round(NitK * sin(Alfa58_2_P2) *
              (((Max58_2_P2) / 40) * (Delay - 80))) + PointB58_2X[Nit];
          Y := Round(cos(Alfa58_2_P2) * (((Max58_2_P2) / 40) * (Delay - 80)))
            + TopY(PointB58_2X[Nit]);
        end;
      end;

  end;

  if DrawAllow then
  begin
    FBmp2.Canvas.Brush.Color := clRed;
    FBmp2.Canvas.Ellipse(X - R, Y - R, X + R, Y + R);
  end;
  FPB.RePaint;
end;

procedure TMnemoFrame.DrawChannel(Nit, Kanal, Delay, Amp: integer);
var
  NitK, X, Y: integer;
  DrawAllow: Boolean;
begin
  if Nit = 0 then
    NitK := 1
  else
    NitK := -1;
  if Delay < 0 then
    DrawAllow := false
  else
    DrawAllow := true;
  case Kanal of
    1:
      begin
        X := FWheelX[Nit, 2];
        Y := TopY(X) - Round(((TopY(X) - BotY(X)) / FBottomT) * Delay);
        if Delay >= FBottomT then
          DrawAllow := false;
      end;
    6:
      begin
        X := Round(NitK * sin(Alfa42_1) * (((Max42_1) / 184) * Delay))
          + FWheelX[Nit, 1];
        Y := Round(cos(Alfa42_1) * (((Max42_1) / 184) * Delay)) + TopY
          (FWheelX[Nit, 1]);
      end;
    7:
      begin
        X := Round(-NitK * sin(Alfa42_2) * (((Max42_2) / 184) * Delay))
          + FWheelX[Nit, 2];
        Y := Round(cos(Alfa42_2) * (((Max42_2) / 184) * Delay)) + TopY
          (FWheelX[Nit, 2]);
      end;
    4:
      begin
        X := Round(NitK * sin(Alfa70_1) * (((Max70_1) / 91) * Delay))
          + FWheelX[Nit, 1];
        Y := Round(cos(Alfa70_1) * (((Max70_1) / 91) * Delay)) + TopY
          (FWheelX[Nit, 1]);
      end;
    5:
      begin
        X := Round(NitK * sin(Alfa70_2) * (((Max70_2) / 91) * Delay))
          + FWheelX[Nit, 2];
        Y := Round(cos(Alfa70_2) * (((Max70_2) / 91) * Delay)) + TopY
          (FWheelX[Nit, 2]);
      end;
    2:
      begin
        if (Delay > 0) and (Delay <= 40) then
        begin
          X := Round(NitK * sin(Alfa58_1) * (((Max58_1) / 40) * Delay))
            + FWheelX[Nit, 1];
          Y := Round(cos(Alfa58_1) * (((Max58_1) / 40) * Delay)) + TopY
            (FWheelX[Nit, 1]);
        end;
        if (Delay > 40) and (Delay <= 80) then
        begin
          X := Round(-NitK * sin(Alfa58_1_P1) *
              (((Max58_1_P1) / 40) * (Delay - 40))) + PointF58_1X[Nit];
          Y := Round(-cos(Alfa58_1_P1) * (((Max58_1_P1) / 40) * (Delay - 40)))
            + HBotY(FWheelX[Nit, 1]);
        end;
        if (Delay > 80) and (Delay <= 130) then
        begin
          X := Round(NitK * sin(Alfa58_1_P2) *
              (((Max58_1_P2) / 40) * (Delay - 80))) + PointF58_2X[Nit];
          Y := Round(cos(Alfa58_1_P2) * (((Max58_1_P2) / 40) * (Delay - 80)))
            + TopY(PointF58_2X[Nit]);
        end;
      end;
    3:
      begin
        if (Delay > 0) and (Delay <= 40) then
        begin
          X := Round(NitK * sin(Alfa58_2) * (((Max58_2) / 40) * Delay))
            + FWheelX[Nit, 2];
          Y := Round(cos(Alfa58_2) * (((Max58_2) / 40) * Delay)) + TopY
            (FWheelX[Nit, 2]);
        end;
        if (Delay > 40) and (Delay <= 80) then
        begin
          X := Round(-NitK * sin(Alfa58_2_P1) *
              (((Max58_2_P1) / 40) * (Delay - 40))) + PointB58_1X[Nit];
          Y := Round(-cos(Alfa58_2_P1) * (((Max58_2_P1) / 40) * (Delay - 40)))
            + HBotY(FWheelX[Nit, 2]);
        end;
        if (Delay > 80) and (Delay <= 130) then
        begin
          X := Round(NitK * sin(Alfa58_2_P2) *
              (((Max58_2_P2) / 40) * (Delay - 80))) + PointB58_2X[Nit];
          Y := Round(cos(Alfa58_2_P2) * (((Max58_2_P2) / 40) * (Delay - 80)))
            + TopY(PointB58_2X[Nit]);
        end;
      end;
  end;
  if DrawAllow then
  begin
    FBmp2.Canvas.Brush.Color := clRed;
    FBmp2.Canvas.Ellipse(X - FPointR, Y - FPointR, X + FPointR, Y + FPointR);
  end;
end;

procedure TMnemoFrame.DrawWheel(Nit, WheelNum: integer; AK: Boolean);
var
  X, Y, w, H: integer;
  X1, Y1, X2, Y2: integer;
begin
  X := FWheelX[Nit, WheelNum];
  Y := TopY(X);
  w := Round((FAKPic[0, 1].Yes.Width + 4) * FKW);
  H := Round((FAKPic[0, 1].Yes.Height + 4) * FKH);
  if Nit = 0 then
    X1 := Round(X - w * 0.55)
  else
    X1 := Round(X - w * 0.45);
  Y1 := Round(Y - H * 0.955);
  X2 := X1 + w;
  Y2 := Y1 + H;
  if AK then
    FBmp2.Canvas.StretchDraw(Rect(X1, Y1, X2, Y2),
      FAKPic[Nit, WheelNum].Yes.Metafile)
  else
    FBmp2.Canvas.StretchDraw(Rect(X1, Y1, X2, Y2),
      FAKPic[Nit, WheelNum].No.Metafile)
end;

procedure TMnemoFrame.CountLines;
var
  R, w, H, X42, X58, X70: integer;
  s, a, B, c, D: Single;
begin
  FWheelX[0, 2] := Round(Self.Width * 0.19);
  FWheelX[0, 1] := Round(Self.Width * 0.26);
  FWheelX[1, 2] := Round(Self.Width - Self.Width * 0.19);
  FWheelX[1, 1] := Round(Self.Width - Self.Width * 0.26);

  FRailHeight := Round((Self.Width * 411 / 755) * 15.1 / 16.5);
  FHeadHeight := Round((Self.Width * 411 / 755) * 11.9 / 16.5);

  s := sin(2.9 / 9.5);
  FTgAlfa := s / Sqrt(1 - s * s);
  s := sin(1.8 / 9.2);
  FTgBetta := s / Sqrt(1 - s * s);
  s := sin(7.47 / 0.8);
  FTgGamma := s / Sqrt(1 - s * s);

  FBmp.Width := Self.Width;
  FBmp.Height := Self.Height;
  FBmp.Canvas.FillRect(Rect(0, 0, FBmp.Width, FBmp.Height));

  FPicHeight := Round(FBmp.Width * 411 / 755);
  FBmp.Canvas.StretchDraw(Rect(0, FBmp.Height - FPicHeight
      { Round(FBmp.Height-FBmp.Width*411/755) } , FBmp.Width, FBmp.Height),
    FPic.Metafile);

  FBmp.Canvas.Pen.Width := 3;
  FBmp.Canvas.Pen.Color := clMaroon;

  FBmp.Canvas.MoveTo(FWheelX[0, 2], TopY(FWheelX[0, 2]));
  FBmp.Canvas.LineTo(FWheelX[0, 2], BotY(FWheelX[0, 2]));

  FBmp.Canvas.MoveTo(FWheelX[1, 2], TopY(FWheelX[1, 2]));
  FBmp.Canvas.LineTo(FWheelX[1, 2], BotY(FWheelX[1, 2]));

  // ���� 42

  FBmp.Canvas.Pen.Color := clBlue;
  X42 := Round(FWheelX[0, 2] * 0.7);

  a := X42;
  B := BotY(FWheelX[0, 2] - X42) - TopY(FWheelX[0, 2]);

  FBmp.Canvas.MoveTo(FWheelX[0, 2], TopY(FWheelX[0, 2]));
  FBmp.Canvas.LineTo(FWheelX[0, 2] - X42, BotY(FWheelX[0, 2] - X42));

  a := FWheelX[0, 2];
  B := TopY(FWheelX[0, 2]);
  c := FWheelX[0, 2] + X42;
  D := BotY(FWheelX[0, 2] - X42);

  Alfa42_2 := ArcTan((c - a) / (D - B));
  Max42_2 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  a := FWheelX[0, 1];
  B := TopY(FWheelX[0, 1]);
  c := FWheelX[0, 1] + X42;
  D := BotY(FWheelX[0, 1] + X42);

  Alfa42_1 := ArcTan((c - a) / (D - B));
  Max42_1 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  FBmp.Canvas.MoveTo(FWheelX[0, 1], TopY(FWheelX[0, 1]));
  FBmp.Canvas.LineTo(FWheelX[0, 1] + X42, BotY(FWheelX[0, 1] + X42));

  FBmp.Canvas.MoveTo(FWheelX[1, 2], TopY(FWheelX[1, 2]));
  FBmp.Canvas.LineTo(FWheelX[1, 2] + X42, BotY(FWheelX[1, 2] + X42));

  FBmp.Canvas.MoveTo(FWheelX[1, 1], TopY(FWheelX[1, 1]));
  FBmp.Canvas.LineTo(FWheelX[1, 1] - X42, BotY(FWheelX[1, 1] - X42));

  // ���� 58

  FBmp.Canvas.Pen.Color := clRed;
  X58 := Round(FWheelX[0, 2] * 0.35);

  FBmp.Canvas.MoveTo(FWheelX[0, 2], TopY(FWheelX[0, 2]));
  FBmp.Canvas.LineTo(FWheelX[0, 2] - X58, HBotY(FWheelX[0, 2] - X58));
  FBmp.Canvas.LineTo(FWheelX[0, 2] - 2 * X58, TopY(FWheelX[0, 2] - 2 * X58));
  FBmp.Canvas.LineTo(FWheelX[0, 2] - 3 * X58, HBotY(FWheelX[0, 2] - 3 * X58));

  PointB58_1X[0] := FWheelX[0, 2] - X58;
  PointB58_2X[0] := FWheelX[0, 2] - 2 * X58;

  a := FWheelX[0, 2];
  B := TopY(FWheelX[0, 2]);
  c := FWheelX[0, 2] - X58;
  D := HBotY(FWheelX[0, 2] - X58);

  Alfa58_2 := ArcTan((c - a) / (D - B));
  Max58_2 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  a := FWheelX[0, 2] - X58;
  B := HBotY(FWheelX[0, 2] - X58);
  c := FWheelX[0, 2] - 2 * X58;
  D := TopY(FWheelX[0, 2] - 2 * X58);

  Alfa58_2_P1 := ArcTan((c - a) / (D - B));
  Max58_2_P1 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  a := FWheelX[0, 2] - 2 * X58;
  B := TopY(FWheelX[0, 2] - 2 * X58);
  c := FWheelX[0, 2] - 3 * X58;
  D := HBotY(FWheelX[0, 2] - 3 * X58);

  Alfa58_2_P2 := ArcTan((c - a) / (D - B));
  Max58_2_P2 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  FBmp.Canvas.MoveTo(FWheelX[0, 1], TopY(FWheelX[0, 1]));
  FBmp.Canvas.LineTo(FWheelX[0, 1] + X58, HBotY(FWheelX[0, 1] + X58));
  FBmp.Canvas.LineTo(FWheelX[0, 1] + 2 * X58, TopY(FWheelX[0, 1] + 2 * X58));
  FBmp.Canvas.LineTo(FWheelX[0, 1] + 3 * X58, HBotY(FWheelX[0, 1] + 3 * X58));

  PointF58_1X[0] := FWheelX[0, 1] + X58;
  PointF58_2X[0] := FWheelX[0, 1] + 2 * X58;

  a := FWheelX[0, 1];
  B := TopY(FWheelX[0, 1]);
  c := FWheelX[0, 1] + X58;
  D := HBotY(FWheelX[0, 1] + X58);

  Alfa58_1 := ArcTan((c - a) / (D - B));
  Max58_1 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  a := FWheelX[0, 1] + X58;
  B := HBotY(FWheelX[0, 1] + X58);
  c := FWheelX[0, 1] + 2 * X58;
  D := TopY(FWheelX[0, 1] + 2 * X58);

  Alfa58_1_P1 := ArcTan((c - a) / (D - B));
  Max58_1_P1 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  a := FWheelX[0, 1] + 2 * X58;
  B := TopY(FWheelX[0, 1] + 2 * X58);
  c := FWheelX[0, 1] + 3 * X58;
  D := HBotY(FWheelX[0, 1] + 3 * X58);

  Alfa58_1_P2 := ArcTan((c - a) / (D - B));
  Max58_1_P2 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  FBmp.Canvas.MoveTo(FWheelX[1, 2], TopY(FWheelX[1, 2]));
  FBmp.Canvas.LineTo(FWheelX[1, 2] + X58, HBotY(FWheelX[1, 2] + X58));
  FBmp.Canvas.LineTo(FWheelX[1, 2] + 2 * X58, TopY(FWheelX[1, 2] + 2 * X58));
  FBmp.Canvas.LineTo(FWheelX[1, 2] + 3 * X58, HBotY(FWheelX[1, 2] + 3 * X58));

  PointB58_1X[1] := FWheelX[1, 2] + X58;
  PointB58_2X[1] := FWheelX[1, 2] + 2 * X58;

  FBmp.Canvas.MoveTo(FWheelX[1, 1], TopY(FWheelX[1, 1]));
  FBmp.Canvas.LineTo(FWheelX[1, 1] - X58, HBotY(FWheelX[1, 1] - X58));
  FBmp.Canvas.LineTo(FWheelX[1, 1] - 2 * X58, TopY(FWheelX[1, 1] - 2 * X58));
  FBmp.Canvas.LineTo(FWheelX[1, 1] - 3 * X58, HBotY(FWheelX[1, 1] - 3 * X58));

  PointF58_1X[1] := FWheelX[1, 1] - X58;
  PointF58_2X[1] := FWheelX[1, 1] - 2 * X58;

  // ���� 70

  FBmp.Canvas.Pen.Color := clGreen;
  X70 := Round(FWheelX[0, 2] * 0.9);

  FBmp.Canvas.MoveTo(FWheelX[0, 2], TopY(FWheelX[0, 2]));
  FBmp.Canvas.LineTo(FWheelX[0, 2] - X70, HBotY(FWheelX[0, 2] - X70));

  a := FWheelX[0, 2];
  B := TopY(FWheelX[0, 2]);
  c := FWheelX[0, 2] - X70;
  D := HBotY(FWheelX[0, 2] - X70);

  Alfa70_2 := ArcTan((c - a) / (D - B));
  Max70_2 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  a := FWheelX[0, 1];
  B := TopY(FWheelX[0, 1]);
  c := FWheelX[0, 1] + X70;
  D := HBotY(FWheelX[0, 1] + X70);

  Alfa70_1 := ArcTan((c - a) / (D - B));
  Max70_1 := Round(Sqrt(Sqr(c - a) + Sqr(D - B)));

  FBmp.Canvas.MoveTo(FWheelX[0, 1], TopY(FWheelX[0, 1]));
  FBmp.Canvas.LineTo(FWheelX[0, 1] + X70, HBotY(FWheelX[0, 1] + X70));

  FBmp.Canvas.MoveTo(FWheelX[1, 1], TopY(FWheelX[1, 1]));
  FBmp.Canvas.LineTo(FWheelX[1, 1] - X70, HBotY(FWheelX[1, 1] - X70));

  FBmp.Canvas.MoveTo(FWheelX[1, 2], TopY(FWheelX[1, 2]));
  FBmp.Canvas.LineTo(FWheelX[1, 2] + X70, HBotY(FWheelX[1, 2] + X70));
end;

end.
