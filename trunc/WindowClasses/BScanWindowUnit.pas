///////////////////////////////////////////////////
///                                             ///
///       Unit ����������� ���� BScan           ///
///                                             ///
///////////////////////////////////////////////////


unit BScanWindowUnit;

interface

uses
  ExtCtrls, Controls, Classes, StdCtrls, Windows, SysUtils, Graphics,
  CfgTablesUnit_2010, AviconTypes,
  CustomWindowUnit, BScanUnit, AScanUnit, MCan, ConfigObj, AScanFrameUnit,
  MnemoFrameUnit, UtilsUnit, DefCoreUnit, PublicData, MessageUnit,
  SoundControlUnit, ProgramProfileConfig;

{$I Directives.inc}

const
  BScanHeadPanelHeight = 6;
  BScanFootPanelHeight = 8;
  BScanChPanelWidth = 250;

{$IFDEF Avikon15RSP}
  ChButPos: array [0 .. 5, 1 .. 2] of integer =
    ((2, -1), (-1, -1), (-1, -1), (3, -1), (0, -1), (1, -1));
{$ENDIF}
{$IFDEF Avikon14_2Block}
  ChButPos: array [0 .. 7, 1 .. 2] of integer =
    ((2, 12), (3, 13), (4, -1), (5, -1), (6, 8), (7, 9), (0, 1), (10, 11));
{$ENDIF}
{$IFDEF Avikon11}
  ChButPos: array [0 .. 7, 1 .. 2] of integer =
    ((2, 10), (3, 11), (4, -1), (5, -1), (6, 8), (7, 9), (0, -1), (1, -1));
{$ENDIF}
{$IFDEF Avikon12}
  ChButPos: array [0 .. 7, 1 .. 2] of integer =
    ((2, 10), (3, 11), (4, -1), (5, -1), (6, 8), (7, 9), (0, -1), (1, -1));
{$ENDIF}
{$IFDEF Avikon14}
  ChButPos: array [0 .. 7, 1 .. 2] of integer =
    ((2, 10), (3, 11), (4, -1), (5, -1), (6, 8), (7, 9), (0, -1), (1, -1));
{$ENDIF}
{$IFDEF Avikon16_IPV}
  ChButPosS: array [0 .. 7, 1 .. 2] of integer =
    ((2, 10), (3, 11), (4, -1), (5, -1), (6, 8), (7, 9), (0, -1), (1, -1));

  ChButPosS3: array [0 .. 7, 1 .. 2] of integer =
    ((2, 10), (3, 11), (4, -1), (5, 12), (6, 8), (7, 9), (0, -1), (1, -1));

  ChButPosW: array [0 .. 7, 1 .. 2] of integer =
    ((2, 12), (3, 13), (4, -1), (5, -1), (6, 8), (7, 9), (10, 11), (0, 1));
{$ENDIF}
{$IFDEF RKS_U}
  ChButPos: array [0 .. 7, 1 .. 2] of integer =
    ((2, 10), (3, 11), (4, -1), (5, -1), (6, 8), (7, 9), (0, -1), (1, -1));
{$ENDIF}

{$IFnDEF RKS_U}
  HChButPos: array [1 .. 2, 0 .. 7] of integer =
    ((0, -1, 1, -1, 2, -1, 3, -1), (4, -1, 5, -1, 6, -1, -1, -1));
  // 0   1  2  3   4   5  6  7     0   1  2   3  4   5   6   7
{$ELSE}
  HChButPos: array [1 .. 2, 0 .. 7] of integer =
    ((-1, -1, -1, -1, -1, -1, -1, -1), (-1, -1, -1, -1, -1, -1, -1, -1));
{$ENDIF}

type

  TBScanWindow = class(TCustomWindow)
  strict private
    // ������� ������������ � ���������� BScanWindow
    function CreateStandartPanel : TPanel;
    function CreateHeadPanel : TPanel;
    function CreateFootPanel : TPanel;
    function CreateSidePanel(Align: TAlign) : TPanel;
    function CreateMiddlePanel : TPanel;

    procedure CreateChannelButtons;
  private
    FMode: TBScanWinMode;
    FKanButWidth: integer;

    FButList: TList;
    FChButHelpVisible: Boolean;

    FBScan: TBScan;
    FAScanFrame: TAScanFrame;
{$IFDEF SimpleMnemo}
    FMenemoFrame: TCustomSimpleMnemoFrame;
{$ELSE}
    FMenemoFrame: TMnemoFrame;
{$ENDIF}
    FFirstShow: Boolean;
    FPrevSelectedBut: TChanelButton;
    FTestLabel: TLabel;
    FScaleButInc: TButton;
    FScaleButDec: TButton;

    FLeftPan: TPanel;
    FRightPan: TPanel;
    FMiddleWorkPan: TPanel;

    // HeadPanel
    HeadPanel: TPanel;
    FTimeLabel: TPanel;
    FBUIAkkLabel: TPanel;
    FBUMAkkLabel: TPanel;
    FRegLabel: TPanel;
    FVelLabel: TPanel;
    BUIAkkIndicator: TIndicator;
    BUMAkkIndicator: TIndicator;

    // FootPan
    FootPanel: TPanel;
    ButPanelManager: TControlPanelManager;
    ThresholdBut: TButton;

    FTVel: integer;
    FS: integer;
    FCoordLineH: integer;
    WorkH: integer;
    FCurKanalNum: integer;
    FModeChange: Boolean;
    FCurRail: integer;
    FBoltJointMode: Boolean;
    FButPressedTime: Int64;
    FBS: Boolean;

    FMaxAdjustPeriod: TDateTime;

    SparkTime: Int64;
    SparkTick: Boolean;

    FLastCoord: integer;
    ABItems: TABItemsList;
    procedure OnExit(Sender: TObject);
    // procedure OnShowHelp(Sender: TObject);
    procedure Resize; override;
    procedure SetChButHelpVisible(Value: Boolean);
    procedure ConfigureBScan;
    procedure OnTimer(Sender: TObject); override;
    procedure OnChButPress(Sender: TObject);
    procedure OnSearchBButPress(Sender: TObject);
    procedure OnSearchMButPress(Sender: TObject);
    procedure OnPlaceMarkButPress(Sender: TObject);
    procedure OnThresholdButPress(Sender: TObject);
    procedure OnScalePlusPress(Sender: TObject);
    procedure OnScaleMinusPress(Sender: TObject);
    procedure OnHandScan(Sender: TObject);
    procedure OnAdjustRailTypePress(Sender: TObject);
    procedure OnTest(Sender: TObject);
    procedure OnTest2(Sender: TObject);
    procedure OnPause(Sender: TObject);
    procedure OnBoltStik(Sender: TObject);
    procedure SetAScanKanal(Kanal: TKanal);
    procedure KanalAdjusted(Rail, Num: integer);
    procedure ConfigChannelButtons;
    procedure OnCancelPause;

    function IsChAdjusted(Kanal: TKanal): Boolean;

    procedure EnableHandKanals(KanalsState: Boolean);
  protected
    procedure SetWinMode(Value: TBScanWinMode);
  public
    procedure OnShowHelp(Sender: TObject);
    procedure Open; override;
    procedure Test(Nit, Kanal, Delay, Amp: integer);
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
      override;
    destructor Destroy; override;
    property Mode: TBScanWinMode read FMode write SetWinMode;
  end;

implementation

uses
  MainUnit;


{ TBScanWindow }

procedure TBScanWindow.ConfigChannelButtons;
var
  i: integer;
begin
  for i := 0 to FButList.Count - 1 do
    if not(FMode in [bmHand, bmNastrHand]) then
    begin
      TChanelButton(FButList.Items[i]).KanalType := rkContinuous;
      TChanelButton(FButList.Items[i]).Enabled := true;
    end
    else
    begin
      TChanelButton(FButList.Items[i]).KanalType := rkHand;
      if TChanelButton(FButList.Items[i]).KanalType <> rkHand then
        TChanelButton(FButList.Items[i]).Enabled := false;
    end;
end;

procedure TBScanWindow.ConfigureBScan;
var
  CoordH, ButCountOnThread, t: integer;
  Line: BScanUnit.TBScanLine;
begin
{$IFDEF TwoThreadsDevice}
  t := 1;
{$ELSE}
  t := 0;
{$ENDIF}
  ButCountOnThread := 4;
{$IFDEF Avikon15RSP}
  ButCountOnThread := 3;
{$ENDIF}
  CoordH := WorkH - (TChanelButton(FButList.Items[0]).Height *
                      ButCountOnThread * (t + 1));
  with (General.Core) do
  begin

{$IFDEF UseHardware}
{$IFDEF Avikon15RSP}
    FBScan.AddLine(Kanal[0, 2], nil);
    FBScan.AddLine(nil, Kanal[0, 3]);
    FBScan.AddLine(Kanal[0, 0], Kanal[0, 1]);
    FBScan.AddCoordLine(CoordH);
{$ELSE}
    Line := FBScan.AddLine;
    if Config.SoundedScheme = Wheels then
    begin
      Line.AddKanal(Kanal[0, 2]);
      Line.AddKanal(Kanal[0, 3]);
      Line.AddKanal(Kanal[0, 12]);
      Line.AddKanal(Kanal[0, 13]);
    end
    else
    begin
      Line.AddKanal(Kanal[0, 2]);
      Line.AddKanal(Kanal[0, 3]);
      Line.AddKanal(Kanal[0, 10]);
      Line.AddKanal(Kanal[0, 11]);
    end;

    Line := FBScan.AddLine;
    Line.AddKanal(Kanal[0, 4]);
    Line.AddKanal(Kanal[0, 5]);
    if Config.SoundedScheme = Scheme3 then
    begin
      Line.AddKanal(Kanal[0, 12]);
    end;

    Line := FBScan.AddLine;
    Line.AddKanal(Kanal[0, 6]);
    Line.AddKanal(Kanal[0, 7]);

    Line := FBScan.AddLine;
    Line.AddKanal(Kanal[0, 0]);
    Line.AddKanal(Kanal[0, 1]);

    if Config.SoundedScheme = Wheels then
    begin
      Line.AddKanal(Kanal[0, 10]);
      Line.AddKanal(Kanal[0, 11]);
    end;

    FBScan.AddCoordLine(CoordH);
{$IFDEF TwoThreadsDevice}
    Line := FBScan.AddLine;
    if Config.SoundedScheme = Wheels then
    begin
      Line.AddKanal(Kanal[1, 2]);
      Line.AddKanal(Kanal[1, 3]);
      Line.AddKanal(Kanal[1, 12]);
      Line.AddKanal(Kanal[1, 13]);
    end
    else
    begin
      Line.AddKanal(Kanal[1, 2]);
      Line.AddKanal(Kanal[1, 3]);
      Line.AddKanal(Kanal[1, 10]);
      Line.AddKanal(Kanal[1, 11]);
    end;

    Line := FBScan.AddLine;
    Line.AddKanal(Kanal[1, 4]);
    Line.AddKanal(Kanal[1, 5]);
    if Config.SoundedScheme = Scheme3 then
    begin
      Line.AddKanal(Kanal[1, 12]);
    end;

    Line := FBScan.AddLine;
    Line.AddKanal(Kanal[1, 6]);
    Line.AddKanal(Kanal[1, 7]);

    Line := FBScan.AddLine;
    Line.AddKanal(Kanal[1, 0]);
    Line.AddKanal(Kanal[1, 1]);

    if Config.SoundedScheme = Wheels then
    begin
      Line.AddKanal(Kanal[1, 10]);
      Line.AddKanal(Kanal[1, 11]);
    end;
{$ENDIF}
{$ENDIF}
{$ELSE}
{$IFDEF Avikon15RSP}
    FBScan.AddLine(nil, nil);
    FBScan.AddLine(nil, nil);
    FBScan.AddLine(nil, nil);
    FBScan.AddCoordLine(CoordH);
{$ELSE}
    Line := FBScan.AddLine;
    Line := FBScan.AddLine;
    Line := FBScan.AddLine;
    Line := FBScan.AddLine;
    FBScan.AddCoordLine(CoordH);
{$IFDEF TwoThreadsDevice}
    Line := FBScan.AddLine;
    Line := FBScan.AddLine;
    Line := FBScan.AddLine;
    Line := FBScan.AddLine;
{$ENDIF}
{$ENDIF}
{$ENDIF}
  end;
end;

constructor TBScanWindow.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);
const
  ButHeight = 70;
  ButWidth = 0.15;
var
  Align: TAlign;
begin
  inherited;
  SetLength(ABItems, 1);
  FLastCoord:= 0;
  SparkTime := GetTickCount;
  SparkTick := false;
  FBS := false;
  FBoltJointMode := false;
  FTimer.Interval := 40;
  FCurKanalNum := -1;
  FCurRail := -1;
  FModeChange := false;
  Self.DoubleBuffered := true;
  Self.ControlStyle := Self.ControlStyle - [csOpaque];
  FPrevSelectedBut := nil;
  FFirstShow := true;
  FButList := TList.Create;
  FChButHelpVisible := true;

  HeadPanel := CreateHeadPanel();
  Obj.Add(HeadPanel);

  FootPanel := CreateFootPanel();
  Obj.Add(FootPanel);

  FLeftPan := CreateSidePanel(alLeft);
  Obj.Add(FLeftPan);

  FRightPan := CreateSidePanel(alRight);
  Obj.Add(FRightPan);

  CreateChannelButtons;

  FMiddleWorkPan := CreateMiddlePanel();
  Obj.Add(FMiddleWorkPan);

  SetChButHelpVisible(false);

  FMaxAdjustPeriod := EncodeDate(2010, 2, 1) - EncodeDate(2010, 1, 1);

  FTimer.Enabled := true;
end;

function TBScanWindow.CreateStandartPanel : TPanel;
var
  Panel : TPanel;
begin
  Panel := TPanel.Create(nil);
  Panel.ParentColor := false;
  Panel.Parent := Self;
  Panel.ParentBackground := false;
  Panel.ParentDoubleBuffered := false;
  Panel.DoubleBuffered := true;
  Panel.Color := Self.Color;
  Panel.DoubleBuffered := true;

  Result := Panel;
end;

function TBScanWindow.CreateHeadPanel : TPanel;
var
  HeadPan : TPanel;
begin
  HeadPan := CreateStandartPanel();

  HeadPan.Align := alTop;
  HeadPan.Height := Round((Self.Height / 100) * BScanHeadPanelHeight);

  FTimeLabel := TPanel.Create(HeadPan);
  FTimeLabel.ParentColor := false;
  FTimeLabel.Parent := HeadPan;
  FTimeLabel.ParentBackground := false;
  FTimeLabel.BevelOuter := bvNone;
  FTimeLabel.Align := alLeft;
  FTimeLabel.Width := Round(Self.Width / 6);
  FTimeLabel.Alignment := taLeftJustify;
  FTimeLabel.Font.Name := DefaultFontName;
  FTimeLabel.Font.Color := FCT.Color['BootWin_ButtonText'];
  FTimeLabel.Font.Height := Round(Self.Height * 0.05);
  FTimeLabel.Visible := true;
  FTimeLabel.Color := Self.Color;
  FTimeLabel.Caption := '����� !';

  FBUIAkkLabel := TPanel.Create(HeadPan);
  FBUIAkkLabel.ParentColor := false;
  FBUIAkkLabel.Parent := HeadPan;
  FBUIAkkLabel.ParentBackground := false;
  FBUIAkkLabel.ParentDoubleBuffered:= false;
  FBUIAkkLabel.DoubleBuffered:= true;
  FBUIAkkLabel.BevelOuter := bvNone;
  FBUIAkkLabel.Align := alLeft;
  FBUIAkkLabel.Width := Round(Self.Width / 6);
  FBUIAkkLabel.Alignment := taLeftJustify;
  FBUIAkkLabel.Font.Name := DefaultFontName;
  FBUIAkkLabel.Font.Color := FCT.Color['BootWin_ButtonText'];
  FBUIAkkLabel.Font.Height := Round(Self.Height * 0.05);
  FBUIAkkLabel.Visible := true;
  FBUIAkkLabel.Color := Self.Color;

  FBUMAkkLabel := TPanel.Create(HeadPan);
  FBUMAkkLabel.ParentColor := false;
  FBUMAkkLabel.Parent := HeadPan;
  FBUMAkkLabel.ParentBackground := false;
  FBUMAkkLabel.ParentDoubleBuffered:= false;
  FBUMAkkLabel.DoubleBuffered:= true;
  FBUMAkkLabel.BevelOuter := bvNone;
  FBUMAkkLabel.Align := alLeft;
  FBUMAkkLabel.Width := Round(Self.Width / 6);
  FBUMAkkLabel.Alignment := taRightJustify;
  FBUMAkkLabel.Font.Name := DefaultFontName;
  FBUMAkkLabel.Font.Color := FCT.Color['BootWin_ButtonText'];
  FBUMAkkLabel.Font.Height := Round(Self.Height * 0.05);
  FBUMAkkLabel.Visible := true;
  FBUMAkkLabel.Color := Self.Color;

  BUIAkkIndicator:= TIndicator.Create('BUIAkkInd', FBUIAkkLabel, FLT.Caption['���']+':');
  BUIAkkIndicator.Width:= FBUIAkkLabel.Width;
  BUIAkkIndicator.Style:= isBattery;
  BUIAkkIndicator.Percent:= 100;

  BUMAkkIndicator:= TIndicator.Create('BUMAkkInd', FBUMAkkLabel, FLT.Caption['���']+':');
  BUMAkkIndicator.Width:= FBUMAkkLabel.Width;
  BUMAkkIndicator.Style:= isBattery;
  BUMAkkIndicator.Percent:= 100;

  FVelLabel := TPanel.Create(HeadPan);
  FVelLabel.ParentColor := false;
  FVelLabel.Parent := HeadPan;
  FVelLabel.ParentBackground := false;
  FVelLabel.BevelOuter := bvNone;
  FVelLabel.Align := alRight;
  FVelLabel.Width := Round(Self.Width / 6);
  FVelLabel.Alignment := taRightJustify;
  FVelLabel.Font.Name := DefaultFontName;
  FVelLabel.Font.Color := FCT.Color['BootWin_ButtonText'];
  FVelLabel.Font.Height := Round(Self.Height * 0.05);
  FVelLabel.Color := Self.Color;
  FVelLabel.Visible := true;
  FVelLabel.Caption := 'V=60 ��/�';

  FRegLabel := TPanel.Create(HeadPan);
  FRegLabel.ParentColor := false;
  FRegLabel.Parent := HeadPan;
  FRegLabel.ParentBackground := false;
  FRegLabel.BevelOuter := bvNone;
  FRegLabel.Align := alClient;
  FRegLabel.Alignment := taCenter;
  FRegLabel.Font.Name := DefaultFontName;
  FRegLabel.Font.Color := FCT.Color['BootWin_ButtonText'];
  FRegLabel.Font.Height := Round(Self.Height * 0.04);
  FRegLabel.Color := Self.Color;
  FRegLabel.Visible := true;
  FRegLabel.Caption := FLT.Caption['�����������'];

  result := HeadPan;
end;

function TBScanWindow.CreateFootPanel : TPanel;
var
  FootPan: TPanel;
begin
  FootPan := CreateStandartPanel();

  FootPan.Align := alBottom;
  FootPan.Height := Round((Self.Height / 100) * BScanFootPanelHeight);


  ButPanelManager := TControlPanelManager.Create(FootPan, FCT, FLT);
  ButPanelManager.Color := Self.Color;

  ButPanelManager.AddPanel(TButtonPanel.Create('BScan', poHorizontal,
      caWholeSize, 12));

  with ButPanelManager.ButtonPanel['BScan'] do
  begin
    AddButton(FLT.Caption['����'], OnExit);
    AddButton(FLT.Caption['Search_B'],OnSearchBButPress);
    AddButton(FLT.Caption['������'], OnHandScan);

    AddButton('Marks', FLT.Caption['Marks'], OnPlaceMarkButPress);
    AddButton('RailType', FLT.Caption['RailType'], OnAdjustRailTypePress);
    ThresholdBut := AddButton('Threshold', '--', OnThresholdButPress);
    AddButton('PauseBtn', FLT.Caption['�����'], OnPause);

    (*<Rud30> ������� id*)
    AddButton('2echo',FLT.Caption['2 ���'], OnBoltStik);
    Item['2echo'].Enabled:= false;
      //TButton(ButPanelManager.ButtonPanel['BScan'].Item['2echo']).Enabled:= false;
    (*</Rud30>*)
  {$IFNDEF NoHelp}
    ButPanelManager.ButtonPanel['BScan'].AddButton
      (FLT.Caption['������'], OnShowHelp);
  {$ENDIF}
  end;
  ButPanelManager.SwitchPanel('BScan');

  if ProgramProfile.DeviceFamily = 'RKS-U' then
    with ButPanelManager.ButtonPanel['BScan'] do
    begin
      Item['Marks'].Visible := false;
      Item['RailType'].Visible := false;
      Item['Threshold'].Visible := false;
      Item['PauseBtn'].Visible := false;
      Item['2echo'].Visible := false;
    end;

  result:= FootPan;
end;

function TBScanWindow.CreateSidePanel(Align: TAlign) : TPanel;
var
  SidePanel: TPanel;
begin
  WorkH := Self.Height - FootPanel.Height - HeadPanel.Height;

  SidePanel := CreateStandartPanel();
  SidePanel.Align := Align;

  SidePanel.Width := BScanChPanelWidth;
  SidePanel.Height := WorkH;
  SidePanel.BevelOuter := bvNone;

  result := SidePanel;
end;

function TBScanWindow.CreateMiddlePanel : TPanel;
var
  MiddlePanel: TPanel;
begin
  MiddlePanel := CreateStandartPanel();
  MiddlePanel.Align := alClient;
  MiddlePanel.BevelOuter := bvNone;

  FBScan := TBScan.Create(MiddlePanel);
  if Config.SoundedScheme = Wheels then
    FBScan.HasSecondBottom := true
  else
    FBScan.HasSecondBottom := false;
  if Assigned(General.Core) then
    FBScan.Core := General.Core;
  FBScan.PixelFormat := General.PixelFormat;
  FBScan.Parent := MiddlePanel;
  FBScan.Scale := FBScanDefaultScale;
  FBScan.Colors := FCT;
  FBScan.Language := FLT;
  FBScan.Align := alClient;
  FBScan.Height := MiddlePanel.Height;
  FBScan.Visible := false;
  ConfigureBScan;
  FBScan.Visible := true;

  ThresholdBut.Caption := IntToStr(FBScan.Threshold) + ' ' + FLT['��'];

  FAScanFrame := TAScanFrame.Create(MiddlePanel, FCT, FLT);
  GAScanFrame := FAScanFrame;
  FAScanFrame.OnKanalAdjusted := KanalAdjusted;
  FAScanFrame.OnEnableHandKanals:= EnableHandKanals;
  FAScanFrame.Visible := true;

{$IFDEF SimpleMnemo}
{$IFDEF Avikon16_IPV}
  FMenemoFrame := TCustomSimpleMnemoFrame.Create(MiddlePanel, FCT, FLT);
{$ENDIF}
{$IFDEF Avikon14}
  FMenemoFrame := TCustomSimpleMnemoFrame.Create(MiddlePanel, FCT, FLT);
{$ENDIF}
{$IFDEF Avikon11}
  FMenemoFrame := TAv11Mnemo.Create(MiddlePanel, FCT, FLT);
{$ENDIF}
{$ELSE}
  FMenemoFrame := TMnemoFrame.Create(MiddlePanel, FCT, FLT);
{$ENDIF}
  FMenemoFrame.Visible := true;

  result := MiddlePanel;
end;

procedure TBScanWindow.CreateChannelButtons;
var
  KanBut: TChanelButton;
  ButPan: TPanel;
  ButKind: TChButKind;
  Kan1, Kan2, HKan: TKanal;
  i, j, t, c, ButCountOnThread: integer;
{$IFDEF Avikon16_IPV}
  ChButPos: array [0 .. 7, 1 .. 2] of integer;
{$ENDIF}
  LeftChannelsSet, RightChannelsSet: set of Byte;
begin
  LeftChannelsSet := [0, 2, 4, 6];
  RightChannelsSet := [1, 3, 5, 7];
  // ������ ��������� ��������
  ButCountOnThread := 4;
{$IFDEF TwoThreadsDevice}
  t := 1;
  FCoordLineH := 50;
{$ELSE}
  t := 0;
  FCoordLineH := 40;
{$ENDIF}
{$IFDEF Avikon15RSP}
  c := 5;
  FCoordLineH := 100;
  ButCountOnThread := 3;
{$ELSE}
  c := 7;
{$ENDIF}
{$IFDEF Avikon12}
  FCoordLineH := 70;
{$ENDIF}
{$IFDEF Avikon16_IPV}
  if (Config.SoundedScheme = Scheme1) or (Config.SoundedScheme = Scheme2) then
  begin
    for i := 0 to 7 do
      for j := 1 to 2 do
        ChButPos[i, j] := ChButPosS[i, j];
  end
  else if Config.SoundedScheme = Scheme3 then
  begin
    for i := 0 to 7 do
      for j := 1 to 2 do
        ChButPos[i, j] := ChButPosS3[i, j];
  end
  else if Config.SoundedScheme = Wheels then
  begin
    for i := 0 to 7 do
      for j := 1 to 2 do
        ChButPos[i, j] := ChButPosW[i, j];
    LeftChannelsSet := [10, 2, 4, 6];
    RightChannelsSet := [0, 3, 5, 7];
  end;
{$ENDIF}
  for j := 0 to t do
    for i := 0 to c do
    begin
      if j = 0 then
        Align := alTop
      else
        Align := alBottom;
      if ChButPos[i, 1] in LeftChannelsSet then
      begin
        ButPan := FLeftPan;
        ButKind := bkLeft;
      end
      else if ChButPos[i, 1] in RightChannelsSet then
      begin
        ButPan := FRightPan;
        ButKind := bkRight;
      end

      else if ChButPos[i, 1] mod 2 = 0 then
      begin
        ButPan := FLeftPan;
        ButKind := bkLeft;
      end
      else
      begin
        ButPan := FRightPan;
        ButKind := bkRight;
      end;

      Kan1 := nil;
      Kan2 := nil;
      HKan := nil;
      if ((Assigned(General.Core)) and (ChButPos[i, 1] > -1)) then
        Kan1 := General.Core.Kanal[j, ChButPos[i, 1]];
      if ChButPos[i, 2] <> -1 then
        Kan2 := General.Core.Kanal[j, ChButPos[i, 2]];

      if HChButPos[j + 1, i] <> -1 then
        HKan := HKanals[HChButPos[j + 1, i]];

      KanBut := TChanelButton.Create(ButPan, Kan1, Kan2, HKan, FCT, FLT);

      KanBut.Parent := ButPan;
      KanBut.KanalType := rkContinuous;
      KanBut.OnClick := OnChButPress;
      KanBut.Align := Align;
      KanBut.Height := Round
        ((WorkH - FCoordLineH) /
          (ButCountOnThread * (t + 1)));
      KanBut.Top := Parent.Height - 1;
      KanBut.Ctl3D := false;
      KanBut.Kind := ButKind;
      KanBut.Visible := false;
      KanBut.ShowHelp := true;

      if (not Assigned(Kan1)) and (not Assigned(Kan2)) then
        KanBut.Lock;

      FButList.Add(KanBut);
    end;

  FLeftPan.Width := KanBut.CountWidth(KanBut.Height);
  FRightPan.Width := KanBut.CountWidth(KanBut.Height);

  // �������� ������ �������� �-���������
  FScaleButDec := TButton.Create(FLeftPan);
  FScaleButDec.Parent := FLeftPan;
  FScaleButDec.Align := alClient;
  FScaleButDec.Font.Name := DefaultFontName;
  FScaleButDec.Font.Color := FCT.Color['BootWin_ButtonText'];
  FScaleButDec.Font.Height := Round(Self.Height * (0.1 / (t + 1)));
  FScaleButDec.Caption := '-';
  FScaleButDec.Visible := true;
  FScaleButDec.OnClick := OnScaleMinusPress;

  FScaleButInc := TButton.Create(FRightPan);
  FScaleButInc.Parent := FRightPan;
  FScaleButInc.Align := alClient;
  FScaleButInc.Font.Name := DefaultFontName;
  FScaleButInc.Font.Color := FCT.Color['BootWin_ButtonText'];
  FScaleButInc.Font.Height := Round(Self.Height * (0.1 / (t + 1)));
  FScaleButInc.Caption := '+';
  FScaleButInc.Visible := true;
  FScaleButInc.OnClick := OnScalePlusPress;
end;

destructor TBScanWindow.Destroy;
begin
  FTimer.Enabled := false;
  ButPanelManager.Free;

  FButList.Free;
  SetLength(ABItems, 0);
  inherited;
end;

procedure TBScanWindow.EnableHandKanals(KanalsState: Boolean);
var
  i: integer;
  B: TButton;
begin
  if not KanalsState then
    begin
      for i := 0 to FButList.Count - 1 do
         TChanelButton(FButList.Items[i]).Enabled := false;
      ButPanelManager.ButtonPanel['BScan'].DisableButtons;

    end
  else
    begin
      ButPanelManager.ButtonPanel['BScan'].EnableButtons;
      B := TButton(ButPanelManager.ButtonPanel['BScan'].Item['Threshold']);
      if Assigned(B) then
        B.Enabled := not(FMode in [bmHand, bmNastrHand]);
      if General.Core.RegOnUserLevel then
        begin
          ButPanelManager.ButtonPanel['BScan'].Item['PauseBtn'].Enabled := true;
          ButPanelManager.ButtonPanel['BScan'].Item['Marks'].Enabled := true;
        end
      else
        begin
          ButPanelManager.ButtonPanel['BScan'].Item['PauseBtn'].Enabled := false;
          ButPanelManager.ButtonPanel['BScan'].Item['Marks'].Enabled := false;
        end;

      for i := 0 to FButList.Count - 1 do
        if FMode in [bmHand, bmNastrHand] then
          begin
            TChanelButton(FButList.Items[i]).KanalType := rkHand;
            if TChanelButton(FButList.Items[i]).KanalType <> rkHand then
              TChanelButton(FButList.Items[i]).Enabled := false
            else
              TChanelButton(FButList.Items[i]).Enabled := true
          end
        else
          TChanelButton(FButList.Items[i]).Enabled := true;
    end;
end;

function TBScanWindow.IsChAdjusted(Kanal: TKanal): Boolean;
var
  CurDateTime: TDateTime;
begin
  Result := true;
  if (Kanal.ATT < 15) or (Kanal.ATT > 65) then
    Result := false;
  if (Kanal.Ky < 10) or (Kanal.Ky > 35) then
    Result := false;
  CurDateTime := Now;
  if ((CurDateTime - Kanal.AdjDate) > FMaxAdjustPeriod) then
    Result := false;
end;

procedure TBScanWindow.KanalAdjusted(Rail, Num: integer);
var
  i: integer;
  But: TChanelButton;
begin
  for i := 0 to FButList.Count - 1 do
  begin
    But := TChanelButton(FButList.Items[i]);
    if Assigned(But) and (But.Enabled) then
      if (But.Kanal.Nit = Rail) and (But.Kanal.Num = Num) then
        TChanelButton(FButList.Items[i]).Adjusted := true;
  end;
end;

procedure TBScanWindow.OnAdjustRailTypePress(Sender: TObject);
begin
  if (GetTickCount - FButPressedTime) < ButtonPressTimeOut then
    Exit;

  FButPressedTime := GetTickCount;
  General.Core.AdjustRailType;
  FAScanFrame.UpDateKanalParams;
  (*<Rud35>*)
  {$IFDEF UseHardware}
    FBScan.UpDateStrob;
    FBScan.Refresh(0, true, true);
  {$ENDIF}
  (*</Rud35>*)
end;

procedure TBScanWindow.OnBoltStik(Sender: TObject);
begin
  FBS := not FBS;
  if FBS then
  begin
    FootPanel.Color := clYellow;
    FTimeLabel.Color := clYellow;
    FVelLabel.Color := clYellow;
    FRegLabel.Color := clYellow;

  end
  else
  begin
    FootPanel.Color := HeadPanel.Color;
    FTimeLabel.Color := HeadPanel.Color;
    FVelLabel.Color := HeadPanel.Color;
    FRegLabel.Color := HeadPanel.Color;
  end;
{$IFDEF UseHardware}
  if Assigned(General.Core) then
    General.Core.BoltJointMode := FBS;
  (*<Rud30>*)
  //�������� �������� � ������� ������ � ����� ������
  if assigned(FPrevSelectedBut) then
  begin
    FAScanFrame.AdjPanWork.Panels['1'].Button['StrSt'].Value := FPrevSelectedBut.Kanal.Str_st;
    FAScanFrame.AdjPanWork.Panels['1'].Button['StrEd'].Value := FPrevSelectedBut.Kanal.Str_en;
  end;
  (*</Rud30>*)
{$ENDIF}
end;

procedure TBScanWindow.OnCancelPause;
begin
  EnableHandKanals(true);
  FAScanFrame.EnableParamButtons;
  General.Core.PauseRegistration(false);
end;

procedure TBScanWindow.OnChButPress(Sender: TObject);
var
  But: TChanelButton;
  i: integer;
begin
  if Assigned(FPrevSelectedBut) then
    if FPrevSelectedBut <> Sender then
      FPrevSelectedBut.Selected := false;
  But := TChanelButton(Sender);
  But.Selected := true;
  FPrevSelectedBut := But;
  General.CurLogicalKanal:= But.Kanal;
  if (FMode = bmSearch) or (FMode = bmMnemo) then
  begin
    Mode := bmEstimate;
    FModeChange := true;
  end;
  if FMode in [bmNastr, bmNastrHand]  then
    KConf.SaveUserConfig(0);
  SetAScanKanal(But.Kanal);

  FAScanFrame.BScanClear;
end;

procedure TBScanWindow.OnExit(Sender: TObject);
begin
  FCurKanalNum := -1;
  FCurRail := -1;
  (*<Rud36>*)
  if FBS = true then
    OnBoltStik(nil);
  (*</Rud36>*)
  if Self.Mode in [bmNastr, bmNastrHand, bmEstimate, bmSearch]  then
    KConf.SaveUserConfig(0);
  if Assigned(General.Core) then
    General.Core.ChangeModeInRegFile(rmMenu, FCurRail, FCurKanalNum);
  General.OpenWindow('MAINMENU');
end;

procedure TBScanWindow.OnHandScan(Sender: TObject);
var
  i: integer;
  Kan: TKanal;
begin
  (*<Rud36>*)
  if FBS = true then
    OnBoltStik(nil);
  (*</Rud36>*)

  if (Self.Mode = bmHand) or (Self.Mode = bmNastrHand) then
    Exit;
  General.Core.PauseRegThread;
  Sleep(300);
  if (GetTickCount - FButPressedTime) < ButtonPressTimeOut then
    Exit;
  FButPressedTime := GetTickCount;
  if General.Mode in [bmNastr, bmNastrHand] then
  begin
    General.Mode := bmNastrHand;
    General.Core.SetMode(rHandNastr);
  end
  else
  begin
    General.Mode := bmHand;
    General.Core.SetMode(rHand);
    (*<Rud16>*)
    FCurKanalNum:= - 1;
    (*</Rud16>*)
  end;

  Kan := nil;
  if Assigned(FPrevSelectedBut) then
    if TChanelButton(FPrevSelectedBut).KanalType = rkHand then
    begin
      Kan := TChanelButton(FPrevSelectedBut).Kanal;
      TChanelButton(FPrevSelectedBut).Selected := true;
      FPrevSelectedBut := TChanelButton(FPrevSelectedBut);
    end;
  if not Assigned(Kan) then
    for i := 0 to FButList.Count - 1 do
      if TChanelButton(FButList.Items[i]).KanalType = rkHand then
      begin
        Kan := TChanelButton(FButList.Items[i]).Kanal;
        TChanelButton(FButList.Items[i]).Selected := true;
        FPrevSelectedBut := TChanelButton(FButList.Items[i]);
        Break;
      end;
  SetAScanKanal(Kan);
  FAScanFrame.BScanClear;
end;

(*<Rud50>*)
procedure TBScanWindow.OnPause(Sender: TObject);
var
  PauseMessage: string;
begin
  if General.Core.RegistrationStatus <> rsPause then
    begin

    EnableHandKanals(false);
    FAScanFrame.DisableParamButtons;
    ButPanelManager.ButtonPanel['BScan'].Item['PauseBtn'].Enabled:= true;
    General.Core.PauseRegistration(true);
    PauseMessage := Format(FLT.Caption[
        '������� ����� �����. %s%s ������� �� ������ ��������� ��� ����������� ������'
        ], [WCHAR(10), WCHAR(13)]);
    MessageForm.Display(PauseMessage, clRed, -1, OnCancelPause);
  end

  else if General.Core.RegistrationStatus = rsPause then
    OnCancelPause;

end;
(*</Rud50>*)

procedure TBScanWindow.OnPlaceMarkButPress(Sender: TObject);
begin
  if (GetTickCount - FButPressedTime) < ButtonPressTimeOut then
    Exit;
  FButPressedTime := GetTickCount;
  General.OpenWindow('PLACEMARK');
end;

procedure TBScanWindow.OnScaleMinusPress(Sender: TObject);
begin
  if (GetTickCount - FButPressedTime) < ButtonPressTimeOut then
    Exit;
  FButPressedTime := GetTickCount;
  FScaleButInc.Enabled := false;
  FScaleButDec.Enabled := false;
  if FMode = bmSearch then
  begin
    if Assigned(FBScan) then
    begin
      if FBScan.Scale > 1 then
      begin
        FBScan.Scale := FBScan.Scale - 1;
        Config.Scale := FBScan.Scale;
      end;
    end;
  end
  else if FMode in [bmEstimate, bmHand] then
  begin
    if FAScanFrame.BScanScale > 1 then
    begin
      FAScanFrame.BScanScale := FAScanFrame.BScanScale - 1;
      Config.Scale := FAScanFrame.BScanScale;
    end;
  end;
end;

procedure TBScanWindow.OnScalePlusPress(Sender: TObject);
begin
  if (GetTickCount - FButPressedTime) < ButtonPressTimeOut then
    Exit;
  FScaleButInc.Enabled := false;
  FScaleButDec.Enabled := false;
  FButPressedTime := GetTickCount;
  if FMode = bmSearch then
  begin
    if Assigned(FBScan) then
    begin
      if FBScan.Scale < 10 then
      begin
        FBScan.Scale := FBScan.Scale + 1;
        Config.Scale := FBScan.Scale;
      end;
    end;
  end
  else if FMode in [bmEstimate, bmHand] then
  begin
    if FAScanFrame.BScanScale < 10 then
    begin
      FAScanFrame.BScanScale := FAScanFrame.BScanScale + 1;
      Config.Scale := FAScanFrame.BScanScale;
    end;
  end;
end;

procedure TBScanWindow.OnSearchBButPress(Sender: TObject);
begin
  if (GetTickCount - FButPressedTime) < ButtonPressTimeOut then
    Exit;
  if (Self.Mode = bmHand) or (Self.Mode = bmNastrHand) then
    General.Core.PauseRegThread;
  FButPressedTime := GetTickCount;
  if Assigned(FPrevSelectedBut) then
    FPrevSelectedBut.Selected := false;
  if Assigned(General.Core) then
    General.Core.StopAScan;
  FCurKanalNum := -1;
  FCurRail := -1;
  General.Mode := bmSearch;
{$IFDEF UseHardware}
  FBScan.Refresh(0, true, true);
{$ENDIF}
end;

procedure TBScanWindow.OnSearchMButPress(Sender: TObject);
begin
  if (GetTickCount - FButPressedTime) < ButtonPressTimeOut then
    Exit;
  FButPressedTime := GetTickCount;
  if Assigned(FPrevSelectedBut) then
    FPrevSelectedBut.Selected := false;
  if Assigned(General.Core) then
    General.Core.StopAScan;
  General.Mode := bmMnemo;
end;

procedure TBScanWindow.OnShowHelp(Sender: TObject);
begin
  if (GetTickCount - FButPressedTime) < ButtonPressTimeOut then
    Exit;
  FButPressedTime := GetTickCount;
  SetChButHelpVisible(not FChButHelpVisible);
  Self.Open;
{$IFDEF UseHardware}
  if Assigned(FBScan) then
    FBScan.Refresh(0, true, true);
{$ENDIF}
end;

procedure TBScanWindow.OnTest(Sender: TObject);
begin
  ButPanelManager.SwitchPanel('Second');
end;

procedure TBScanWindow.OnTest2(Sender: TObject);
begin
  ButPanelManager.SwitchPanel('BScan');
end;

procedure TBScanWindow.OnThresholdButPress(Sender: TObject);
begin
  if Assigned(FBScan) then
  begin
    FBScan.IncThreshold;
    TButton(Sender).Caption := IntToStr(FBScan.Threshold) + ' ' + FLT['��'];
    FBScan.Refresh(0, true, true);
  end;
  if Assigned(Self.FAScanFrame) then
  begin
    Self.FAScanFrame.IncTreshold;
  end;

end;

procedure TBScanWindow.OnTimer(Sender: TObject);
var
  H, i: integer;
  T1, T2, t, s: integer;
  Hour, Min, Sec, MSec: Word;
  CColor: TColor;
  T3: integer;
  V: Single;
  RegStat: string;
  BStik, S1, S2: string;
  AK: Byte;
  BUMUakk: Single;
  A, B: integer;
  CAN: TCAN;
begin
  inherited;
  if Assigned(General.Core.Registrator) then
    begin
      if (General.Core.Registrator.MaxDisCoord - FLastCoord) >= 20 then
        begin
          // ����� AirBrush
          if Assigned(General.Core.FAirBrush) then
            General.Core.FAirBrush.Tick_;
          FLastCoord:= General.Core.Registrator.MaxDisCoord;
        end;
    end;


  if (GetTickCount - SparkTime) > 50 then
  begin
    SparkTime := GetTickCount;
    SparkTick := not SparkTick;
  end;

  // ���� ������ �����
  FTimer.Enabled := false;
  if Assigned(General.Core) then
    if General.Core.CoordMark then
    begin
      General.Core.CoordMark := false;
      General.OpenWindow('PLACEMARK');
    end;
  FTimeLabel.Caption := TimeToStr(Now);

  if (General.Core.RegOnUserLevel) and  (not General.Core.WaitingForBUM)then
  begin
    CColor := FCT.Color['AK_Good'];
    RegStat := FLT.Caption['RegOn'];
  end
  else
  begin
    CColor := FCT.Color['AK_BAD'];
    RegStat := FLT.Caption['RegOff'];
  end;
  if General.Core.RegistrationStatus = rsPause then
  begin
    CColor := FCT.Color['AK_Warning'];
    RegStat := FLT.Caption['RegPaused'];
  end;

  // FRegLabel.Color:=CColor;
  FRegLabel.Font.Color := CColor;
  if FBS then
  begin
    BStik := '(����� 2 ���)';
    FRegLabel.Caption := Format(FLT.Caption['�����������'] + ' %s %s',
      [RegStat, FLT.Caption[BStik]]);
  end
  else
    FRegLabel.Caption := Format(FLT.Caption['�����������'] + ' %s', [RegStat]);

  // ����� ��� ���
  AK:= 100;
  if Assigned(General.PowerManager) then
  begin
    (*<Rud17>*)
    General.PowerManager.RefreshAPMStats;
    (*</Rud17>*)
    AK := General.PowerManager.BatteryLifePercent;
  end;

  if AK < 20 then
    BUIAkkIndicator.TextColor:= FCT.Color['AK_BAD']
  else if ((AK >= 20) and (AK < 50)) then
    BUIAkkIndicator.TextColor:= FCT.Color['AK_Warning']
  else
    BUIAkkIndicator.TextColor:= clBlack;

  if AK = 255 then
    AK:= 100;

  if AK < 50 then
    BUIAkkIndicator.Text:= Format('%d %%', [AK]);

  BUIAkkIndicator.Percent:= AK;


  // ���������� ��� ���
  BUMUakk:= General.Core.GetUAKK / 10;
  //BUMUakk:= 10.3;
  if BUMUakk < 10.5 then
    BUMAkkIndicator.TextColor:= FCT.Color['AK_BAD']
  else if ((BUMUakk >= 10.5) and (BUMUakk <= 11)) then
    BUMAkkIndicator.TextColor:= FCT.Color['AK_Warning']
  else
    BUMAkkIndicator.TextColor:= clBlack;

  // ������ 11 - �������
  // �� 11 �� 10,5 - ������. Max ����� � ������ ���� ~ 1,5 ����
  // ���� 10,5 - �������. Max ����� � ������� ���� ~ 0,5 ����
  if BUMUakk > 0.5 then
    BUMAkkIndicator.Text:= Format('%3.1f %s', [BUMUakk, FLT.Caption['�']])
  else
   BUMAkkIndicator.Text:= '';
  if BUMUakk < 0.5 then
    begin
      (*<Rud48>*)
      BUMAkkIndicator.Visible := False;
      (*</Rud48>*)
      BUMAkkIndicator.TextColor:= clBlack;
      BUMAkkIndicator.Percent:= 100;
    end
  else
    begin
      (*<Rud48>*)
      BUMAkkIndicator.Visible := true;
      (*</Rud48>*)
      if BUMUakk > 13 then
        BUMAkkIndicator.Percent:= 100
      else
        BUMAkkIndicator.Percent:= Round(((BUMUakk-11)*50/2)+50);
      if (BUMUakk >= 10.5) and (BUMUakk < 11) then
        BUMAkkIndicator.Percent:= Round(((BUMUakk-10.5)*30/0.5)+20);
      if BUMUakk < 10.5 then
          BUMAkkIndicator.Percent:= 10;
    end;

  {$IFDEF TestSensor}
  case General.Core.SensorStateBum1 of
     -1: S1:= '��� ������';
     1: S1:= '������';
     0: S1:= '�����';
  end;
  case General.Core.SensorStateBum2 of
     -1: S2:= '��� ������';
     1: S2:= '������';
     0: S2:= '�����';
  end;
  FRegLabel.Caption := Format('���. ���: %s ����. ���: %s', [S1, S2]);
  {$ENDIF}

  if General.Core.RegistrationStatus = rsOn then
  begin
    T3 := GetTickCount;
    if (T3 - FTVel) >= 1000 then
    begin
      s := General.Core.Registrator.CurLoadDisCoord[1] - FS;
      V := General.Core.GetVelocity;
      if V >= 5 then
      begin
        if SparkTick then
          FVelLabel.Font.Color := clRed
        else
          FVelLabel.Font.Color := clSkyBlue;
      end
      else
      begin
        FVelLabel.Font.Color := clBlack;
        FVelLabel.Visible := true;
      end;

      FS := General.Core.Registrator.CurLoadDisCoord[1];
      FVelLabel.Caption := Format('%3.1f ' + FLT.Caption['��/�'], [V]);
      FTVel := T3;

    end;
  end;

  if FFirstShow then
  begin
    FFirstShow := false;
    if Assigned(FButList) then
      if FButList.Count > 0 then
      begin
        H := TChanelButton(FButList.Items[0]).Height;
        for i := 0 to FBScan.LinesCount - 1 do
          FBScan.Lines[i].Height := H;
      end;
  end;
{$IFDEF UseHardware}
  if FMode = bmSearch then
  begin
    if FBoltJointMode <> General.Core.BoltJointMode then
    begin
      FBoltJointMode := General.Core.BoltJointMode;
      FBScan.ForceResize;
      FBScan.Refresh(0, true, true);
    end;
    T1 := GetTickCount;
    FTestLabel.Caption := 'Test';
    if Assigned(FBScan) then
      FBScan.Refresh(0, false, false);
    T2 := GetTickCount;
    t := T2 - T1;
    FTestLabel.Caption := IntToStr(t);
  end;

  for i := 0 to FButList.Count - 1 do
    TChanelButton(FButList.Items[i]).RefreshParams;
  if (FMode <> bmSearch) and (FMode <> bmMnemo) and (FMode <> bmOff) then
  begin
    FAScanFrame.Refresh;
  end;
  if (FMode = bmMnemo) then
  begin
    FMenemoFrame.Refresh;
{$IFDEF Avikon11}
    TAv11Mnemo(FMenemoFrame).TwoEcho := General.Core.BoltJointMode;
{$ENDIF}
  end;
{$ENDIF}
  FTimer.Enabled := true;
  if (GetTickCount - FButPressedTime) >= ButtonPressTimeOut then
  begin
    FScaleButInc.Enabled := true;
    FScaleButDec.Enabled := true;
  end;

end;

procedure TBScanWindow.Open;
var
  i: integer;
  KanalType: TKanType;
  B: TButton;
begin
  inherited;
  B := TButton(ButPanelManager.ButtonPanel['BScan'].Item['Marks']);
  if Assigned(B) then
    B.Enabled := General.Core.RegOnUserLevel;

  if Assigned(FButList) then
  begin
    if not(FMode in [bmHand, bmNastrHand]) then
      KanalType := rkContinuous
    else
      KanalType := rkHand;
    for i := 0 to FButList.Count - 1 do
      if TChanelButton(FButList.Items[i]).KanalType = KanalType then
        TChanelButton(FButList.Items[i]).Visible := true;
  end;
  FBScan.ForceResize;
  FBScan.Refresh(0, true, true);

//    General.OpenWindow('MAINMENU');
end;

procedure TBScanWindow.Test(Nit, Kanal, Delay, Amp: integer);
begin
{$IFDEF SimpleMnemo}
  FMenemoFrame.TestKanal(Nit, Kanal, Delay, Amp);
{$ELSE}
  FMenemoFrame.Test(Nit, Kanal, Delay, Amp);
{$ENDIF}
end;

procedure TBScanWindow.Resize;
var
  H, i: integer;
begin
  inherited;
  FVelLabel.Left:= FVelLabel.Parent.Width;

  BUIAkkIndicator.Width:= FBUIAkkLabel.Width;
  BUIAkkIndicator.Height:= FBUIAkkLabel.Height;
  BUIAkkIndicator.ObjWidth:= Round(FBUIAkkLabel.Width*0.4);
  BUIAkkIndicator.Percent:= 100;

  BUMAkkIndicator.Width:= FBUMAkkLabel.Width;
  BUMAkkIndicator.Height:= FBUMAkkLabel.Height;
  BUMAkkIndicator.ObjWidth:= Round(FBUMAkkLabel.Width*0.4);
  BUMAkkIndicator.Percent:= 100;

  if Assigned(FBScan) then
    FBScan.ForceResize;
end;

procedure TBScanWindow.SetAScanKanal(Kanal: TKanal);
var
  RegMode: TRegFileModes;
begin
  Config.TempAlfa := Kanal.Takt.Alfa;

  FModeChange := false;
  if (FCurKanalNum <> Kanal.Num) or (FCurRail <> Kanal.Nit) then
  begin
    if Assigned(General.Core) then
      General.Core.SetAScanKanal(Kanal.Nit, Kanal.Num);
    FAScanFrame.BringToFront;
    FAScanFrame.Kanal := Kanal;
    FAScanFrame.Refresh;
    FCurKanalNum := Kanal.Num;
    FCurRail := Kanal.Nit;
  end;

  if (FMode in [bmNastr, bmNastrHand]) then
    SND.SoundOneChannel(Kanal.Nit, Kanal.Num)
  else
    SND.SoundAllChannels;

  case FMode of
    bmNastr:
      RegMode := rmNastr;
    bmNastrHand:
      RegMode := rmNastrHand;
    bmHand:
      RegMode := rmHand;
    bmEstimate:
      RegMode := rmEvaluation;
  end;

  if Assigned(General.Core) then
    General.Core.ChangeModeInRegFile(RegMode, FCurRail, FCurKanalNum);
end;

procedure TBScanWindow.SetChButHelpVisible(Value: Boolean);
var
  i, w: integer;
begin
  if FChButHelpVisible <> Value then
  begin
    FChButHelpVisible := Value;
    for i := 0 to FButList.Count - 1 do
      TChanelButton(FButList.Items[i]).ShowHelp := FChButHelpVisible;
    w := TChanelButton(FButList.Items[0]).CountWidth
      (TChanelButton(FButList.Items[0]).Height);
    FLeftPan.Width := w;
    FRightPan.Width := w;
  end;
end;

procedure TBScanWindow.SetWinMode(Value: TBScanWinMode);
var
  i: integer;
  B: TButton;
  But: TChanelButton;
begin
  // FBScan.ForceResize;
  General.CurLogicalMode:= Value;
  if General.Core.RegOnUserLevel then
    ButPanelManager.ButtonPanel['BScan'].Item['PauseBtn'].Enabled := true
  else
    ButPanelManager.ButtonPanel['BScan'].Item['PauseBtn'].Enabled := false;

  FBScan.BScanDirection := Config.ZondPosition;
  FBScan.PointSize := Config.BViewPointSize;
  FBScan.AmpByColor := Config.BViewAmpByColor;
  FBScan.AmplBySize := Config.BViewAmpBySize;
  FBScan.Scale := Config.Scale;
  if FMode <> Value then
  begin
    FMode := Value;
    General.Core.ZMEnabled := false;
    if General.Core.RegistrationStatus = rsOn then
      FS := General.Core.Registrator.CurLoadDisCoord[1];
    ConfigChannelButtons;
    for i := 0 to FButList.Count - 1 do
      TChanelButton(FButList.Items[i]).Mode := FMode;

    (*<Rud30>*)
    B := TButton(ButPanelManager.ButtonPanel['BScan'].Item['2echo']);
    if Assigned(B) then
      B.Enabled := (FMode in [bmSearch,bmEstimate,bmMnemo]);
    B := nil;
    (*</Rud30>*)

    B := TButton(ButPanelManager.ButtonPanel['BScan'].Item['Threshold']);
    if Assigned(B) then
      B.Enabled := not(FMode in [bmHand, bmNastrHand]);
    General.Core.StopHandRegistration;
    case FMode of
      bmSearch:
        begin
          if Assigned(General.Core) then
          begin
            if General.Core.Mode <> rScan then
              General.Core.SetMode(rScan);
            General.Core.ChangeModeInRegFile(rmSearchB, 0, 0);
          end;

          for i := 0 to FButList.Count - 1 do
          begin
            But:= TChanelButton(FButList.Items[i]);
            But.Selected := false;
          end;
          FMode := bmSearch;
          FAScanFrame.Mode := FMode;
          FBScan.Visible := true;
{$IFDEF UseHardware}
          FAScanFrame.Stop;
          FBScan.Clear;
          FBScan.UpDateStrob;
          FBScan.Refresh(0, true, true);
          Self.Resize;
          SND.SoundAllChannels;
{$ENDIF}
          FScaleButDec.Enabled := true;
          FScaleButInc.Enabled := true;
          ButPanelManager.SwitchPanel('BScan');
          Self.FBScan.BringToFront;
          General.Core.UnPauseRegThread;
        end;
      bmMnemo:
        begin
          if Assigned(General.Core) then
          begin
            if General.Core.Mode <> rScan then
              General.Core.SetMode(rScan);
            General.Core.ChangeModeInRegFile(rmSearchM, 0, 0);
          end;
          if Assigned(FPrevSelectedBut) then
            FPrevSelectedBut.Selected := false;
          FMenemoFrame.Visible := true;
          FScaleButDec.Enabled := false;
          FScaleButInc.Enabled := false;
          FMenemoFrame.BringToFront;
          SND.SoundAllChannels;
          ButPanelManager.SwitchPanel('BScan');
        end;
      bmEstimate:
        begin
          FScaleButDec.Enabled := true;
          FScaleButInc.Enabled := true;
          FMode := bmEstimate;
          FAScanFrame.BScanDirection := Config.ZondPosition;
          FAScanFrame.PointSize := Config.BViewPointSize;
          FAScanFrame.AmpByColor := Config.BViewAmpByColor;
          FAScanFrame.AmplBySize := Config.BViewAmpBySize;
          FAScanFrame.BScanScale := Round(FBScan.Scale);
          FAScanFrame.Mode := FMode;
          SND.SoundAllChannels;
          ButPanelManager.SwitchPanel('BScan');
        end;
      bmNastr:
        begin
          if Assigned(General.Core) then
            General.Core.SetMode(rNastr);
          if Assigned(FPrevSelectedBut) then
            FPrevSelectedBut.Selected := false;
          (*<Rud27>*)
          FPrevSelectedBut := TChanelButton(FButList.Items[8]);
          (*</Rud27>*)
          FPrevSelectedBut.Selected := true;
          FScaleButDec.Enabled := false;
          FScaleButInc.Enabled := false;
          FMode := bmNastr;
          FCurKanalNum := -1;
          FCurRail := -1;
          FAScanFrame.Visible := true;
          FAScanFrame.Mode := FMode;
          FAScanFrame.BringToFront;
          SetAScanKanal(FPrevSelectedBut.Kanal);
          FScaleButDec.Enabled := false;
          FScaleButInc.Enabled := false;
          SND.SoundOneChannel(FPrevSelectedBut.Kanal.Nit,
            FPrevSelectedBut.Kanal.Num);
          ButPanelManager.SwitchPanel('Nastr');
          General.Core.PauseRegThread;
          for i := 0 to FButList.Count - 1 do
          begin
            But := TChanelButton(FButList.Items[i]);
            (*<Rud32>*)
            if  (ProgramProfile.DeviceFamily = 'Avicon16') or
                (ProgramProfile.ProgramTitle = 'Filus X27W') then
              But.Adjusted := false
            else
              But.Adjusted := true;
            (*</Rud32>*)
          end;
        end;
      bmHand:
        begin
          ButPanelManager.SwitchPanel('Nastr');
          FAScanFrame.BScanDirection := Config.ZondPosition;
          FAScanFrame.PointSize := Config.BViewPointSize;
          FAScanFrame.AmpByColor := Config.BViewAmpByColor;
          FAScanFrame.AmplBySize := Config.BViewAmpBySize;
          FAScanFrame.Mode := FMode;
        end;
      bmNastrHand:
        begin
          ButPanelManager.SwitchPanel('Nastr');
          FAScanFrame.Mode := FMode;
          FScaleButDec.Enabled := false;
          FScaleButInc.Enabled := false;
          for i := 0 to FButList.Count - 1 do
          begin
            But := TChanelButton(FButList.Items[i]);
            (*<Rud32>*)
            if (ProgramProfile.DeviceFamily = 'Avicon16') or
               (ProgramProfile.ProgramTitle = 'Filus X27W') then
              But.Adjusted := false
            else
              But.Adjusted := true;
            (*</Rud32>*)
          end;
        end;
    end;
  end;
end;


end.
