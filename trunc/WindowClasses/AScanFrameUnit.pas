unit AScanFrameUnit;

interface

{$I Directives.inc}

uses
  StdCtrls, ExtCtrls, Buttons, Windows, Classes, SysUtils, Controls,
  CfgTablesUnit_2010, AviconTypes,
  AScanUnit, BScanUnit, MCan, ConfigObj, CustomWindowUnit, BScanThreadUnit,
  DefCoreUnit;

const
  AScanFrame_BScanHeight = 30; // � ��������� �� ������ AScanFrame
  AScanFrame_MeasurePanHeight = 10; // � ��������� �� ������ AScanFrame
  AScanFrame_OptionsPanWidth = 250; // � ��������� �� ������ AScanFrame

type
  TKanalAdjustedEvent = procedure(Rail, Num: integer) of object;
  TEnableHandKanals = procedure(ButtonsState: Boolean) of object;

  TAScanFrame = class(TCustomWindow)
  private
    FAScan: TAScan;
    FBScan: TBScan;
    FOptionsPan: TPanel;
    FMeasurePan: TPanel;
    FMiddlePan: TPanel;
    AdjustBut: TSpeedButton;
    MksToMMBut: TButton;
    FAdjPanWork: TAdjustFrame;
    FAdjPanNastr: TAdjustFrame;
    FAdjPanHandWork: TAdjustFrame;
    FAdjPanHandNastr: TAdjustFrame;
    FKanal: TKanal;
    FMeasurePanel: TMeasureFrame;
    FMode: TBScanWinMode;

    FHandSurface: integer;
    FHandRail: integer;

    FHandRegMaxTime: Int64;
    FHandRegMaxTimeSecSave: integer;

    FBScanLine: BScanUnit.TBScanLine;

    procedure OnExit(Sender: TObject);
    procedure ConfigureBScan;
    procedure Resize; override;
    function ApproveValue(NewValue: Variant; id: string): Boolean;
    procedure OnAdjustParamChange(id: string; Value: Variant);
    procedure OnAdjustButPressed(Sender: TObject);
    procedure OnMksToMMButPressed(Sender: TObject);
    procedure CenterPress(id: string);
    procedure PanelChange(Num: integer);
    procedure AdjButtonDrawText(Text: string);
    procedure AdjButtonStatus(Stat: string);
    procedure SetScanDir(Value: TZondPosition);
    procedure SetPointSize(Value: TBViewPointSize);
  protected
    procedure SetKanal(Value: TKanal);
    procedure SetWinMode(Value: TBScanWinMode);
    procedure SetBScale(Value: integer);
    function GetBScale: integer;

    function GetAmpByColor: Boolean;
    procedure SetAmpByColor(Value: Boolean);

    function GetAmpBySize: Boolean;
    procedure SetAmpBySize(Value: Boolean);
    (*<Rud12>*)
    procedure BlockChangeStrob;
    (*</Rud12>*)
  public
    OnKanalAdjusted: TKanalAdjustedEvent;
    OnEnableHandKanals: TEnableHandKanals;
    procedure UpDateKanalParams;
    procedure UpDateKanalParamsNew;
    procedure Refresh;
    procedure Stop;
    procedure BScanClear;
    procedure IncTreshold;
    procedure DecTreshold;
    procedure DisableParamButtons;
    procedure EnableParamButtons;
    (*<Rud23>*)
    procedure SetMMScaleType;
    (*</Rud23>*)
    constructor Create(Panel: TPanel; CT: TColorTable; LT: TLanguageTable);
      override;
    property Kanal: TKanal read FKanal write SetKanal;
    property Mode: TBScanWinMode read FMode write SetWinMode;
    property BScanScale: integer read GetBScale write SetBScale;
    property BScanDirection: TZondPosition write SetScanDir;
    property PointSize: TBViewPointSize write SetPointSize;
    property AmpByColor: Boolean read GetAmpByColor write SetAmpByColor;
    property AmplBySize: Boolean read GetAmpBySize write SetAmpBySize;
    property AdjPanWork: TAdjustFrame read FAdjPanWork;
  end;

implementation

uses
  MainUnit;

{ TAScanFrame }

procedure TAScanFrame.AdjButtonDrawText(Text: string);
var
  R: TRect;
  N: integer;
  Buff: array [0 .. 255] of Char;

begin
  if Assigned(AdjustBut) then
    with AdjustBut do
    begin
      Glyph.Canvas.Font := AdjustBut.Font;
      Glyph.Width := AdjustBut.Width - 6;
      Glyph.Height := AdjustBut.Height - 6;
      Glyph.Canvas.FillRect(Rect(0, 0, Glyph.Width, Glyph.Height));
      R := Bounds(0, 0, Glyph.Width, 0);
      StrPCopy(Buff, Text);
      Caption := '';
      DrawText(Glyph.Canvas.Handle, Buff, StrLen(Buff), R,
        DT_CENTER or DT_WORDBREAK or DT_CALCRECT);
      OffsetRect(R, (Glyph.Width - R.Right) div 2, (Glyph.Height - R.Bottom)
          div 2);
      DrawText(Glyph.Canvas.Handle, Buff, StrLen(Buff), R,
        DT_CENTER or DT_WORDBREAK);
    end;
end;

procedure TAScanFrame.AdjButtonStatus(Stat: string);
begin
  if AdjustBut.Visible then
    if Assigned(AdjustBut) then
    begin
      if Stat = 'Start' then
      begin
        AdjustBut.Font.Color := FCT.Color['AdjButFontColorNorm'];
        AdjButtonDrawText
          (FLT.Caption['�����'] + ' ' + FLT.Caption['�����������']);
      end;
      if Stat = 'RegOn' then
      begin
        AdjustBut.Font.Color := FCT.Color['AdjButFontColorOK'];
        AdjButtonDrawText
          (FLT.Caption['����'] + ' ' + FLT.Caption['Registration']);
      end;
      if Stat = 'Clear' then
      begin
        AdjustBut.Glyph.Width := 0;
        AdjustBut.Glyph.Height := 0;
        AdjustBut.Font.Color := FCT.Color['AdjButFontColorNorm'];
        AdjustBut.Caption := FLT.Caption['���������'];
      end;
      if Stat = 'Adj_OK' then
      begin
        AdjustBut.Font.Color := FCT.Color['AdjButFontColorOK'];
        AdjButtonDrawText(FLT.Caption['���������'] + ' ' + FLT.Caption
            ['��������']);
      end;
      if Stat = 'Adj_Error' then
      begin
        AdjustBut.Font.Color := FCT.Color['AdjButFontColorError'];
        AdjButtonDrawText(FLT.Caption['���������'] + ' ' + FLT.Caption['������']
          );
      end;
    end;
end;

function TAScanFrame.ApproveValue(NewValue: Variant; id: string): Boolean;
var
  NewATT, R, Num, TktLen: integer;
  NewValueMKS: integer;
begin
  Result := true;
  if Assigned(FKanal) then
  begin
    R := FKanal.Nit;
    Num := FKanal.Num;
    TktLen := Round(FKanal.Takt.AV_mash * 23.2);

    NewValueMKS := NewValue;

    if id = 'Ku' then
    begin
      NewATT := FKanal.ATT + NewValue - FKanal.Ky;
      Result := (NewATT >= 0) and (NewATT <= 80);
    end;
    if id = 'StrSt' then
    begin
      Result := (NewValueMKS > 0) and (NewValueMKS < TktLen) and
        (NewValueMKS < FKanal.Str_en { CurStrobEn } );
      if Result then
        case FKanal.Num of
          // ������ ��2 (� �� �������� ����� ������� ������)
          0:
            Result := Result and (NewValueMKS > MCAN.Kanals[R, 1].Str_en
              { StrobsEn[1] } );
          1:
            Result := Result and (NewValueMKS < MCAN.Kanals[R, 0].Str_st
              { StrobsSt[0] } );
          // 45 ����������
          6:
            Result := Result and (NewValueMKS < MCAN.Kanals[R, 8].Str_st
              { StrobsSt[8] } );
          8:
            Result := Result and (NewValueMKS > MCAN.Kanals[R, 6].Str_en
              { StrobsEn[6] } );
          // 45 �����������
          7:
            Result := Result and (NewValueMKS < MCAN.Kanals[R, 9].Str_st
              { StrobsSt[9] } );
          9:
            Result := Result and (NewValueMKS > MCAN.Kanals[R, 7].Str_en
              { StrobsEn[7] } );
        end;
{$IFnDEF EGO_USW}
{$IFnDEF VMT_US}
      if Config.SoundedScheme = Wheels then
{$ENDIF}
{$ENDIF}
      begin
        case FKanal.Num of
          // ������ ��1
          10:
            Result := Result and (NewValueMKS > MCAN.Kanals[R, 11].Str_en
              { StrobsEn[11] } );
          11:
            Result := Result and (NewValueMKS < MCAN.Kanals[R, 10].Str_st
              { StrobsSt[10] } );
        end;
      end;
    end;
    if id = 'StrEd' then
    begin
      Result := (NewValueMKS > 0) and (NewValueMKS < TktLen) and
        (NewValueMKS > FKanal.Str_st);
      if Result then
        case FKanal.Num of
          // ������ ��2 (� �� �������� ����� ������� ������)
          0:
            Result := Result and (NewValueMKS > MCAN.Kanals[R, 1].Str_en
              { StrobsEn[1] } );
          1:
            Result := Result and (NewValueMKS < MCAN.Kanals[R, 0].Str_st
              { StrobsSt[0] } );
          // 45 ����������
          6:
            Result := Result and (NewValueMKS < MCAN.Kanals[R, 8].Str_st
              { StrobsSt[8] } );
          8:
            Result := Result and (NewValueMKS > MCAN.Kanals[R, 6].Str_en
              { StrobsEn[6] } );
          // 45 �����������
          7:
            Result := Result and (NewValueMKS < MCAN.Kanals[R, 9].Str_st
              { StrobsSt[9] } );
          9:
            Result := Result and (NewValueMKS > MCAN.Kanals[R, 7].Str_en
              { StrobsEn[7] } );
        end;
{$IFnDEF EGO_USW}
{$IFnDEF VMT_US}
      if Config.SoundedScheme = Wheels then
{$ENDIF}
{$ENDIF}
      begin
        case FKanal.Num of
          // ������ ��1
          10:
            Result := Result and (NewValueMKS > MCAN.Kanals[R, 11].Str_en
              { StrobsEn[11] } );
          11:
            Result := Result and (NewValueMKS < MCAN.Kanals[R, 10].Str_st
              { StrobsSt[10] } );
        end;
      end;
    end;
  end;

end;

procedure TAScanFrame.BScanClear;
begin
  FBScan.Lines[0].Clear;
  FBScan.Refresh(0, true, true);
end;

procedure TAScanFrame.CenterPress(id: string);
begin
  if id = 'Metka' then
    General.Core.ZMEnabled := not General.Core.ZMEnabled;
end;

procedure TAScanFrame.ConfigureBScan;
begin
  FBScanLine := FBScan.AddLine;
  FBScan.AddCoordLine(50);
end;

constructor TAScanFrame.Create(Panel: TPanel; CT: TColorTable;
  LT: TLanguageTable);
var
  But: TButton;
  AdjPan: TAdjustPanel;
  AdjBut: TAdjustButton;
begin
  inherited;
  FBScanLine := nil;
  Self.ControlStyle := Self.ControlStyle - [csOpaque];
  Self.BevelOuter := bvNone;
  Self.Align := alClient;

  OnKanalAdjusted := nil;

  FOptionsPan := TPanel.Create(nil);
  FOptionsPan.Parent := Self;
  FOptionsPan.ParentColor := false;
  FOptionsPan.ParentBackground := false;
  FOptionsPan.ParentDoubleBuffered := false;
  FOptionsPan.DoubleBuffered := true;
  FOptionsPan.Align := alRight;
  FOptionsPan.Width := AScanFrame_OptionsPanWidth;
  FOptionsPan.BevelOuter := bvNone;

  Obj.Add(FOptionsPan);

  AdjustBut := TSpeedButton.Create(FOptionsPan);
  AdjustBut.Parent := FOptionsPan;
  AdjustBut.Align := alBottom;
  AdjustBut.Font.Name := DefaultFontName;
  AdjustBut.Caption := LT.Caption['Adjust'];
  AdjustBut.Font.Color := FCT.Color['AdjButFontColorNorm'];
  AdjustBut.OnClick := OnAdjustButPressed;
  AdjustBut.Visible := false;

  // �������� ������ ��� ������ ������
  FAdjPanWork := TAdjustFrame.Create(FOptionsPan, CT, LT);
  FAdjPanWork.OnApprove := ApproveValue;
  FAdjPanWork.onPanelChange := PanelChange;
  FAdjPanWork.Align := alClient;
  FAdjPanWork.ParamChangeEvent := OnAdjustParamChange;
  FAdjPanWork.Visible := true;
  FAdjPanWork.OnCenterPress := CenterPress;

  // ������ ������
  AdjPan := FAdjPanWork.AddPanel;
  AdjPan.id := '1';
  // ������ ������
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'StrSt';
  AdjBut.Max := 300;
  AdjBut.Min := 0;
  AdjBut.Value := 5;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['���. ���.'];
  AdjBut.Units := FLT['���'];

  // ����� ������
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'StrEd';
  AdjBut.Max := 300;
  AdjBut.Min := 0;
  AdjBut.Value := 15;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['���. ���.'];
  AdjBut.Units := FLT['���'];

  // ��
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'Ku';
  AdjBut.Max := 100;
  AdjBut.Min := -100;
  AdjBut.Value := 23;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['��'];
  AdjBut.Units := FLT['��'];

  // �����
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'Metka';
  AdjBut.Max := 200;
  AdjBut.Min := 0;
  AdjBut.Value := 100;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['�����'];
  AdjBut.Units := FLT['���'];

  // ������ ������
  AdjPan := FAdjPanWork.AddPanel;
  AdjPan.id := '2';

  // ����� � ������
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtReal;
  AdjBut.id := 'PrTime';
  AdjBut.Max := 200;
  AdjBut.Delta := 0.1;
  AdjBut.Min := 0;
  AdjBut.Value := 110;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['t �����.'];
  AdjBut.Units := FLT['���'];

  // ���
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'VRCH';
  AdjBut.Max := 100;
  AdjBut.Min := 0;
  AdjBut.Value := 110;
  AdjBut.Circled := false;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['���'];
  AdjBut.Units := FLT['���'];

  // ��
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'Ku';
  AdjBut.Max := 100;
  AdjBut.Min := -100;
  AdjBut.Value := 23;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['��'];
  AdjBut.Units := FLT['��'];

  // ���� (����� �������)
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtText;
  AdjBut.id := 'Snd';
  AdjBut.Max := 1;
  AdjBut.Min := 0;
  AdjBut.Value := 0;
  SetLength(AdjBut.TextArr, 2);
  AdjBut.TextArr[0] := FLT['���.'];
  AdjBut.TextArr[1] := FLT['����.'];
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['����'];
  AdjBut.Units := '';

  FAdjPanWork.SetCurPanel(1);

  // �������� ������ ��� ������ ���������
  FAdjPanNastr := TAdjustFrame.Create(FOptionsPan, CT, LT);
  FAdjPanNastr.OnApprove := ApproveValue;
  FAdjPanNastr.onPanelChange := PanelChange;
  FAdjPanNastr.Align := alClient;
  FAdjPanNastr.ParamChangeEvent := OnAdjustParamChange;
  FAdjPanNastr.Visible := true;

  // ������ ������
  AdjPan := FAdjPanNastr.AddPanel;
  AdjPan.id := '1';

  // ������ ������������ ������
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'StrSt';
  AdjBut.Max := 300;
  AdjBut.Min := 0;
  AdjBut.Value := 5;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['���. ���.'];
  AdjBut.Units := FLT['���'];

  // ���
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'VRCH';
  AdjBut.Max := 100;
  AdjBut.Min := 0;
  AdjBut.Value := 110;
  AdjBut.Circled := false;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['���'];
  AdjBut.Units := FLT['���'];

  // ��
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'Ku';
  AdjBut.Max := 100;
  AdjBut.Min := -100;
  AdjBut.Value := 23;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['��'];
  AdjBut.Units := FLT['��'];

  // ������ ������
  AdjPan := FAdjPanNastr.AddPanel;
  AdjPan.id := '2';

  // ����� � ������
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtReal;
  AdjBut.id := 'PrTime';
  AdjBut.Max := 200;
  AdjBut.Delta := 0.1;
  AdjBut.Min := 0;
  AdjBut.Value := 110;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['t �����.'];
  AdjBut.Units := FLT['���'];

  // ���
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'VRCH';
  AdjBut.Max := 100;
  AdjBut.Min := 0;
  AdjBut.Value := 110;
  AdjBut.Circled := false;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['���'];
  AdjBut.Units := FLT['���'];

  // ��
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'Ku';
  AdjBut.Max := 100;
  AdjBut.Min := -100;
  AdjBut.Value := 23;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['��'];
  AdjBut.Units := FLT['��'];

  FAdjPanNastr.SetCurPanel(1);

  FAdjPanHandWork := TAdjustFrame.Create(FOptionsPan, CT, LT);
  FAdjPanHandWork.OnApprove := ApproveValue;
  FAdjPanHandWork.onPanelChange := PanelChange;
  FAdjPanHandWork.Align := alClient;
  FAdjPanHandWork.ParamChangeEvent := OnAdjustParamChange;
  FAdjPanHandWork.Visible := true;
  FAdjPanHandWork.OnCenterPress := CenterPress;

  // ������ ������
  AdjPan := FAdjPanHandWork.AddPanel;
  AdjPan.id := '1';
  // ������ ������
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'StrSt';
  AdjBut.Max := 300;
  AdjBut.Min := 0;
  AdjBut.Value := 5;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['���. ���.'];
  AdjBut.Units := FLT['���'];

  // ����� ������
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'StrEd';
  AdjBut.Max := 300;
  AdjBut.Min := 0;
  AdjBut.Value := 15;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['���. ���.'];
  AdjBut.Units := FLT['���'];

  // ��
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'Ku';
  AdjBut.Max := 100;
  AdjBut.Min := -100;
  AdjBut.Value := 23;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['��'];
  AdjBut.Units := FLT['��'];

  // �����
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'Metka';
  AdjBut.Max := 200;
  AdjBut.Min := 0;
  AdjBut.Value := 100;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['�����'];
  AdjBut.Units := FLT['���'];

  // ������ ������
  AdjPan := FAdjPanHandWork.AddPanel;
  AdjPan.id := '2';

  // ����� � ������
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtReal;
  AdjBut.id := 'PrTime';
  AdjBut.Max := 200;
  AdjBut.Delta := 0.1;
  AdjBut.Min := 0;
  AdjBut.Value := 110;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['t �����.'];
  AdjBut.Units := FLT['���'];

  // ���
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'VRCH';
  AdjBut.Max := 100;
  AdjBut.Min := 0;
  AdjBut.Value := 110;
  AdjBut.Circled := false;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['���'];
  AdjBut.Units := FLT['���'];

  // ��
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'Ku';
  AdjBut.Max := 100;
  AdjBut.Min := -100;
  AdjBut.Value := 23;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['��'];
  AdjBut.Units := FLT['��'];

  // {$IFDEF Avikon16_IPV}
{$IFDEF BUMCOUNT4}
  // ���
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtText;
  AdjBut.id := 'HBum';
  AdjBut.Max := 3;
  AdjBut.Min := 0;
  AdjBut.Value := 0;
  SetLength(AdjBut.TextArr, 4);
  AdjBut.TextArr[0] := FLT['1'];
  AdjBut.TextArr[1] := FLT['2'];
  AdjBut.TextArr[2] := FLT['3'];
  AdjBut.TextArr[3] := FLT['4'];
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['BUM'];
  AdjBut.Units := '';
{$ELSE}
{$IFDEF BUMCOUNT2}
  // ���
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtText;
  AdjBut.id := 'HBum';
  AdjBut.Max := 1;
  AdjBut.Min := 0;
  AdjBut.Value := 0;
  SetLength(AdjBut.TextArr, 2);
  AdjBut.TextArr[0] := FLT['�.'];
  AdjBut.TextArr[1] := FLT['�.'];
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['BUM'];
  AdjBut.Units := '';
{$ELSE}
  // �����
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'Metka';
  AdjBut.Max := 200;
  AdjBut.Min := 0;
  AdjBut.Value := 100;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['�����'];
  AdjBut.Units := FLT['���'];
{$ENDIF}
{$ENDIF}
  // ������ ������
  AdjPan := FAdjPanHandWork.AddPanel;
  AdjPan.id := '3';

  // �����������
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'Surface';
  AdjBut.Max := 9;
  AdjBut.Delta := 1;
  AdjBut.Min := 0;
  AdjBut.Value := 0;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['������.'];
  AdjBut.Units := '';

  // �����
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtText;
  AdjBut.id := 'HRail';
  AdjBut.Max := 1;
  AdjBut.Min := 0;
  AdjBut.Value := 0;
  SetLength(AdjBut.TextArr, 2);
  AdjBut.TextArr[0] := FLT['�.'];
  AdjBut.TextArr[1] := FLT['�.'];
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['����'];
  AdjBut.Units := '';

  // ��
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'Ku';
  AdjBut.Max := 100;
  AdjBut.Min := -100;
  AdjBut.Value := 23;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['��'];
  AdjBut.Units := FLT['��'];

  // ������
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'Timer';
  AdjBut.Max := 40;
  AdjBut.Min := 0;
  AdjBut.Value := 30;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['�����'];
  AdjBut.Units := FLT['���'];

  FAdjPanHandWork.SetCurPanel(1);

  // �������� ������ ��� ������ ���������
  FAdjPanHandNastr := TAdjustFrame.Create(FOptionsPan, CT, LT);
  FAdjPanHandNastr.OnApprove := ApproveValue;
  FAdjPanHandNastr.onPanelChange := PanelChange;
  FAdjPanHandNastr.Align := alClient;
  FAdjPanHandNastr.ParamChangeEvent := OnAdjustParamChange;
  FAdjPanHandNastr.Visible := true;

  // ������ ������
  AdjPan := FAdjPanHandNastr.AddPanel;
  AdjPan.id := '1';

  // ������ ������������ ������
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'StrSt';
  AdjBut.Max := 300;
  AdjBut.Min := 0;
  AdjBut.Value := 5;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['���. ���.'];
  AdjBut.Units := FLT['���'];

  // ���
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'VRCH';
  AdjBut.Max := 100;
  AdjBut.Min := 0;
  AdjBut.Value := 110;
  AdjBut.Circled := false;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['���'];
  AdjBut.Units := FLT['���'];

  // ��
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'Ku';
  AdjBut.Max := 100;
  AdjBut.Min := -100;
  AdjBut.Value := 23;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['��'];
  AdjBut.Units := FLT['��'];

  // ������ ������
  AdjPan := FAdjPanHandNastr.AddPanel;
  AdjPan.id := '2';

  // ����� � ������
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtReal;
  AdjBut.id := 'PrTime';
  AdjBut.Max := 200;
  AdjBut.Delta := 0.1;
  AdjBut.Min := 0;
  AdjBut.Value := 110;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['t �����.'];
  AdjBut.Units := FLT['���'];

{$IFDEF BUMCOUNT4}
  // ���
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtText;
  AdjBut.id := 'HBum';
  AdjBut.Max := 3;
  AdjBut.Min := 0;
  AdjBut.Value := 0;
  SetLength(AdjBut.TextArr, 4);
  AdjBut.TextArr[0] := FLT['1'];
  AdjBut.TextArr[1] := FLT['2'];
  AdjBut.TextArr[2] := FLT['3'];
  AdjBut.TextArr[3] := FLT['4'];
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['BUM'];
  AdjBut.Units := '';
{$ELSE}
{$IFDEF BUMCOUNT2}
  // ���
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtText;
  AdjBut.id := 'HBum';
  AdjBut.Max := 1;
  AdjBut.Min := 0;
  AdjBut.Value := 0;
  SetLength(AdjBut.TextArr, 2);
  AdjBut.TextArr[0] := FLT['�.'];
  AdjBut.TextArr[1] := FLT['�.'];
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['BUM'];
  AdjBut.Units := '';
{$ELSE}
  // ���
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'VRCH';
  AdjBut.Max := 100;
  AdjBut.Min := 0;
  AdjBut.Value := 110;
  AdjBut.Circled := false;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['���'];
  AdjBut.Units := FLT['���'];
{$ENDIF}
{$ENDIF}
  // ��
  AdjBut := AdjPan.AddButton;
  AdjBut.AdjustValueType := vtInt;
  AdjBut.id := 'Ku';
  AdjBut.Max := 100;
  AdjBut.Min := -100;
  AdjBut.Value := 23;
  AdjBut.Kind := akTwoStr;
  AdjBut.Caption := FLT['��'];
  AdjBut.Units := FLT['��'];

  FAdjPanHandNastr.SetCurPanel(1);

  FMiddlePan := TPanel.Create(nil);
  FMiddlePan.Parent := Self;
  FMiddlePan.ParentColor := false;
  FMiddlePan.ParentBackground := false;
  FMiddlePan.ParentDoubleBuffered := false;
  FMiddlePan.DoubleBuffered := true;
  FMiddlePan.Align := alClient;
  FMiddlePan.Color := Self.Color;
  FMiddlePan.BevelOuter := bvNone;
  Obj.Add(FMiddlePan);

  FMeasurePan := TPanel.Create(FMiddlePan);
  FMeasurePan.ParentColor := false;
  FMeasurePan.Parent := FMiddlePan;
  FMiddlePan.ParentBackground := false;
  FMiddlePan.ParentDoubleBuffered := false;
  FMiddlePan.DoubleBuffered := true;
  FMeasurePan.Align := alTop;
  FMeasurePan.Color := Self.Color;
  FMeasurePan.Height := Round
    ((Self.Height / 100) * AScanFrame_MeasurePanHeight);
  FMeasurePan.BevelOuter := bvNone;
  FBScan := TBScan.Create(FMiddlePan);
  if Assigned(General.Core) then
    FBScan.Core := General.Core;
  FBScan.Parent := FMiddlePan;
  FBScan.PixelFormat := General.PixelFormat;
  FBScan.Colors := FCT;
  FBScan.Language := FLT;
  FBScan.Align := alBottom;
  FBScan.Height := Round((Self.Height / 100) * AScanFrame_BScanHeight);
  FBScan.Visible := false;
  ConfigureBScan;
  FBScan.ShowIcons:= false;
  FBScan.Scale := 2;
  FBScan.Visible := true;

  FAScan := TAScan.Create(FMiddlePan);
  if Assigned(General.Core) then
    FAScan.Core := General.Core;
  FAScan.Parent := FMiddlePan;
  FAScan.Colors := FCT;
  FAScan.Language := FLT;
  FAScan.Align := alClient;
  FAScan.ScaleLineClick := OnMksToMMButPressed;

  FMeasurePanel := TMeasureFrame.Create(FMeasurePan, FCT, FLT);
  FMeasurePanel.AScanRef := FAScan;
  FMeasurePanel.Mode := FMode;
  FMeasurePanel.Visible := true;

  FAdjPanWork.BringToFront;
end;

procedure TAScanFrame.DecTreshold;
begin
  if Assigned(FBScan) then
  begin
    FBScan.DecThreshold;
    FBScan.Refresh(0, true, true);
  end;
end;

procedure TAScanFrame.DisableParamButtons;
begin
  FAdjPanWork.ChangesEnabled:= false;
  FAdjPanNastr.ChangesEnabled:= false;
  FAdjPanHandWork.ChangesEnabled:= false;
  FAdjPanHandNastr.ChangesEnabled:= false;
end;

procedure TAScanFrame.EnableParamButtons;
begin
  FAdjPanWork.ChangesEnabled:= true;
  FAdjPanNastr.ChangesEnabled:= true;
  FAdjPanHandWork.ChangesEnabled:= true;
  FAdjPanHandNastr.ChangesEnabled:= true;
end;

function TAScanFrame.GetAmpByColor: Boolean;
begin
  Result := FBScan.AmpByColor;
end;

function TAScanFrame.GetAmpBySize: Boolean;
begin
  Result := FBScan.AmplBySize;
end;

function TAScanFrame.GetBScale: integer;
begin
  if Assigned(FBScan) then
    Result := Round(FBScan.Scale);
end;

procedure TAScanFrame.IncTreshold;
begin
  if Assigned(FBScan) then
  begin
    FBScan.IncThreshold;
    FBScan.Refresh(0, true, true);
  end;
end;

procedure TAScanFrame.OnAdjustButPressed(Sender: TObject);
begin
{$IFDEF UseHardware}
  if FMode in [bmHand] then
  begin
    if not General.Core.HandRegistrationOn then
    begin
      // �������� ����������� ��������� ���������� ����� ��������� ����������� �������
      if Assigned(OnEnableHandKanals) then
        OnEnableHandKanals(false);
      FAdjPanHandWork.ChangesEnabled:= false;
      General.Core.StartHandRegistration(FHandRail, FHandSurface);
      FHandRegMaxTimeSecSave := FAdjPanHandWork.ParamValue['Timer', 'Value'];
      FHandRegMaxTime := GetTickCount + FHandRegMaxTimeSecSave * 1000;
      AdjButtonStatus('RegOn');
    end
    else
    begin
      General.Core.StopHandRegistration;
      // ������� ������� ����������� ��������� ���������� ����� ��������� ����������� �������
      if Assigned(OnEnableHandKanals) then
        OnEnableHandKanals(true);
      FAdjPanHandWork.ChangesEnabled:= true;
      FBScan.Clear;
      FBScan.Refresh(0, true, true);
      FAdjPanHandWork.ParamValue['Timer', 'Value'] := FHandRegMaxTimeSecSave;
      AdjButtonStatus('Start');
    end;
  end
  else
  begin
    if Assigned(General.Core) then
      // ������ N ������������ � General.Core.MeasureResult ���������
      // N ������ �� ��������� ��������, ������������� ��������
      // if General.Core.AdjustKu(General.Core.MeasureResult.N)=0 then
      if General.Core.AdjustKu(FAScan.FixN) = 0 then
      begin
        if FMode = bmNastr then
          FAdjPanNastr.ParamValue['Ku', 'Value'] := FKanal.Ky
        else if FMode = bmNastrHand then
          FAdjPanHandNastr.ParamValue['Ku', 'Value'] := FKanal.Ky;

        if Assigned(General.Core.Registrator) then
          begin
            General.Core.Registrator.AddKu(TRail(FKanal.Nit), FKanal.Num, FKanal.Ky);
            General.Core.Registrator.AddAtt(TRail(FKanal.Nit), FKanal.Num, FKanal.ATT);
          end;

        AdjButtonStatus('Adj_OK');
        if Assigned(OnKanalAdjusted) then
          OnKanalAdjusted(FKanal.Nit, FKanal.Num);
        FAScan.ClearPlus;
      end
      else
        AdjButtonStatus('Adj_Error');
  end;
{$ENDIF}
end;

procedure TAScanFrame.OnAdjustParamChange(id: string; Value: Variant);
var
  c, j: integer;
  CAN: TCAN;
  KanNumToChange: integer;
begin
  if id = 'StrSt' then
  begin
    General.Core.StrobSt := Value;
    if (FMode in [bmHand, bmNastrHand]) and (FKanal.Num in [0, 1]) then
    begin
      if FKanal.Num = 0 then
      begin
        HKanals[1].Str_en := FKanal.Str_st;
        SendBum.Add(HKanals[1], cInstallStrob);
        SendBum.Add(FKanal, cInstallVRU);
      end;
    end;
{$IFDEF Avikon16_IPV}
    if Config.SoundedScheme = Wheels then
      begin
        if (FKanal.Num in [0, 8, 9, 10]) and (not(FMode in [bmHand, bmNastrHand]))  then
          begin
            case FKanal.Num of
              0:
                KanNumToChange := 1;
              8:
                KanNumToChange := 6;
              9:
                KanNumToChange := 7;
              10:
                KanNumToChange := 11;
            end;
            MCAN.Kanals[FKanal.Nit, KanNumToChange].Str_en := FKanal.Str_st;
            SendBum.Add(MCAN.Kanals[FKanal.Nit, KanNumToChange], cInstallStrob);
            SendBum.Add(FKanal, cInstallVRU);
          end;

      end
    else
      if (FKanal.Num in [0, 8, 9]) and (not(FMode in [bmHand, bmNastrHand]))  then
        begin
          case FKanal.Num of
            0:
              KanNumToChange := 1;
            8:
              KanNumToChange := 6;
            9:
              KanNumToChange := 7;
          end;
          MCAN.Kanals[FKanal.Nit, KanNumToChange].Str_en := FKanal.Str_st;
          SendBum.Add(MCAN.Kanals[FKanal.Nit, KanNumToChange], cInstallStrob);
          SendBum.Add(FKanal, cInstallVRU);
        end;
{$ENDIF}
{$IFDEF EGO_USW}
        if (FKanal.Num in [0, 8, 9, 10]) and (not(FMode in [bmHand, bmNastrHand]))  then
          begin
            case FKanal.Num of
              0:
                KanNumToChange := 1;
              8:
                KanNumToChange := 6;
              9:
                KanNumToChange := 7;
              10:
                KanNumToChange := 11;
            end;
            MCAN.Kanals[FKanal.Nit, KanNumToChange].Str_en := FKanal.Str_st;
            SendBum.Add(MCAN.Kanals[FKanal.Nit, KanNumToChange], cInstallStrob);
            SendBum.Add(FKanal, cInstallVRU);
          end;
{$ENDIF}

    FAdjPanWork.ParamValue['StrEd', 'Min'] := Value;
    FBScan.UpDateStrob;
    FBScan.Refresh(0, true, true);
  end;
  if id = 'StrEd' then
  begin
    General.Core.StrobEd := Value;
    if (FMode in [bmHand, bmNastrHand]) and (FKanal.Num in [0, 1]) then
    begin
      if FKanal.Num = 1 then
      begin
        HKanals[0].Str_st := FKanal.Str_en;
        SendBum.Add(HKanals[0], cInstallStrob);
        SendBum.Add(FKanal, cInstallVRU);
      end;
    end;
{$IFDEF Avikon16_IPV}
    if Config.SoundedScheme = Wheels then
      begin
        if (FKanal.Num in [1, 6, 7, 11]) and (not(FMode in [bmHand, bmNastrHand])) then
          begin
            case FKanal.Num of
              1:
                KanNumToChange := 0;
              6:
                KanNumToChange := 8;
              7:
                KanNumToChange := 9;
              11:
                KanNumToChange := 10;
            end;
            MCAN.Kanals[FKanal.Nit, KanNumToChange].Str_st := FKanal.Str_en;
            SendBum.Add(MCAN.Kanals[FKanal.Nit, KanNumToChange], cInstallStrob);
            SendBum.Add(FKanal, cInstallVRU);
          end;
      end
    else
      begin
        if (FKanal.Num in [1, 6, 7]) and (not(FMode in [bmHand, bmNastrHand])) then
          begin
            case FKanal.Num of
              1:
                KanNumToChange := 0;
              6:
                KanNumToChange := 8;
              7:
                KanNumToChange := 9;
            end;
            MCAN.Kanals[FKanal.Nit, KanNumToChange].Str_st := FKanal.Str_en;
            SendBum.Add(MCAN.Kanals[FKanal.Nit, KanNumToChange], cInstallStrob);
            SendBum.Add(FKanal, cInstallVRU);
          end;
      end;
{$ENDIF}
{$IFDEF EGO_USW}
        if (FKanal.Num in [1, 6, 7, 11]) and (not(FMode in [bmHand, bmNastrHand])) then
          begin
            case FKanal.Num of
              1:
                KanNumToChange := 0;
              6:
                KanNumToChange := 8;
              7:
                KanNumToChange := 9;
              11:
                KanNumToChange := 10;
            end;
            MCAN.Kanals[FKanal.Nit, KanNumToChange].Str_st := FKanal.Str_en;
            SendBum.Add(MCAN.Kanals[FKanal.Nit, KanNumToChange], cInstallStrob);
            SendBum.Add(FKanal, cInstallVRU);
          end;
{$ENDIF}

    FAdjPanWork.ParamValue['StrSt', 'Max'] := Value;
    FBScan.UpDateStrob;
    FBScan.Refresh(0, true, true);
  end;
  if id = 'Ku' then
  begin
    FAdjPanWork.ParamValue['Ku', 'Value'] := Value;
    FAdjPanNastr.ParamValue['Ku', 'Value'] := Value;
    FAdjPanHandWork.ParamValue['Ku', 'Value'] := Value;
    FAdjPanHandNastr.ParamValue['Ku', 'Value'] := Value;
    General.Core.Ku := Value;
  end;
  if id = 'Metka' then
    General.Core.ZMPosition := Value;
  if id = 'PrTime' then
    General.Core.TwoTp := Value;

  if id = 'Snd' then
    if Value = 1 then
      FKanal.SoundOn := false
    else
      FKanal.SoundOn := true;

  if id = 'VRCH' then
  begin
    FAdjPanNastr.ParamValue['VRCH', 'Value'] := Value;
    General.Core.VRCH := Value;
  end;
  if id = 'HRail' then
    FHandRail := FAdjPanHandWork.ParamValue['Rail', 'Value'];
  if id = 'Surface' then
    FHandSurface := FAdjPanHandWork.ParamValue['Surface', 'Value'];
  if id = 'HBum' then
  begin
    c := Length(HTakts);
    for j := 0 to c - 1 do
    begin
      CAN := General.Core.CANS[Value];
      if (Assigned(CAN)) and (Assigned(HTakts[j])) then
        HTakts[j].CANObj := General.Core.CANS[Value];
    end;
    //����� ��� ���� ����� Value
{$IFDEF UseHardware}
    if Assigned(General) then
      if Assigned(General.Core) then
        if Assigned(FKanal) then
          General.Core.SetAScanKanal(FKanal.Nit, FKanal.Num);
{$ENDIF}
  end;
end;

procedure TAScanFrame.OnExit(Sender: TObject);
begin
  General.OpenWindow('BOOT');
end;

procedure TAScanFrame.OnMksToMMButPressed(Sender: TObject);
var
  StrobSt, StrobEd: integer;
  MUnits: string;
begin
  if General.Core.MeasureUnits = muMKS then
    begin
      General.Core.MeasureUnits := muMM;
      if Config.UnitsSystem in [usRus, usMetricTurkish] then
        MUnits := '��'
      else
        MUnits := '"';
    end
  else
  begin
    General.Core.MeasureUnits := muMKS;
    MUnits := '���';
  end;

  if Assigned(FKanal) then
  begin
    Config.TempAlfa := FKanal.Takt.Alfa;
    FAdjPanWork.ParamValue['StrSt', 'Units'] := FLT.Caption[MUnits];
    FAdjPanWork.ParamValue['StrEd', 'Units'] := FLT.Caption[MUnits];
    FAdjPanWork.Update;

    FAdjPanNastr.ParamValue['StrSt', 'Units'] := FLT.Caption[MUnits];
    FAdjPanNastr.ParamValue['StrEd', 'Units'] := FLT.Caption[MUnits];
    FAdjPanNastr.Update;

    FAdjPanHandWork.ParamValue['StrSt', 'Units'] := FLT.Caption[MUnits];
    FAdjPanHandWork.ParamValue['StrEd', 'Units'] := FLT.Caption[MUnits];
    FAdjPanHandWork.Update;

    FAdjPanHandNastr.ParamValue['StrSt', 'Units'] := FLT.Caption[MUnits];
    FAdjPanHandNastr.ParamValue['StrEd', 'Units'] := FLT.Caption[MUnits];
    FAdjPanHandNastr.Update;
  end;

  FAScan.ReBuild;
end;

procedure TAScanFrame.PanelChange(Num: integer);
begin
  if Assigned(FAScan) then
  begin
    FAScan.ClearPlus;
    if Self.Mode in [bmNastr, bmNastrHand] then
    begin
      if Num = 1 then
      begin
        FAScan.ClearPlus;
        General.Core.Adj2Tp := false;
        FAScan.Kanal.Takt.InstallStrob(General.Core.Mode, false);
        FAScan.Kanal.Takt.InstallVRU(General.Core.Mode);
        SendBum.Add(FKanal, cInstallVRU);
        if Self.Mode = bmNastr then
          begin
            FAdjPanNastr.ParamValue['StrSt', 'Value'] := FAScan.Kanal.NStr_st;
            FAdjPanNastr.ParamValue['StrEd', 'Value'] := FAScan.Kanal.NStr_en;

            FAdjPanNastr.ParamValue['StrSt', 'Min'] := 0;
            FAdjPanNastr.ParamValue['StrSt', 'Max'] := FAScan.Kanal.NStr_en;
            FAdjPanNastr.ParamValue['StrEd', 'Min'] := FAScan.Kanal.NStr_en;
            FAdjPanNastr.ParamValue['StrEd', 'Max'] := FAScan.Kanal.NStr_en;
          end;
        if Self.Mode = bmNastrHand then
          begin
            FAdjPanHandNastr.ParamValue['StrSt', 'Value'] := FAScan.Kanal.NStr2TP_st;
            FAdjPanHandNastr.ParamValue['StrEd', 'Value'] := FAScan.Kanal.NStr2TP_en;

            FAdjPanHandNastr.ParamValue['StrSt', 'Min'] := 0;
            FAdjPanHandNastr.ParamValue['StrSt', 'Max'] := FAScan.Kanal.NStr2TP_en;
            FAdjPanHandNastr.ParamValue['StrEd', 'Min'] := FAScan.Kanal.NStr2TP_en;
            FAdjPanHandNastr.ParamValue['StrEd', 'Max'] := FAScan.Kanal.NStr2TP_en;
          end;
        AdjustBut.Enabled := true;
      end
      else
      begin
        FAScan.ClearPlus;

        General.Core.Adj2Tp := true;
        FAScan.Kanal.Takt.InstallStrob(General.Core.Mode, true);
        FAScan.Kanal.Takt.InstallVRU(General.Core.Mode);
        SendBum.Add(FKanal, cInstallVRU);
        if Self.Mode = bmNastr then
          begin
            FAdjPanNastr.ParamValue['StrSt', 'Min'] := 0;
            FAdjPanNastr.ParamValue['StrSt', 'Max'] := FAScan.Kanal.NStr_en;
            FAdjPanNastr.ParamValue['StrEd', 'Min'] := FAScan.Kanal.NStr_en;
            FAdjPanNastr.ParamValue['StrEd', 'Max'] := FAScan.Kanal.NStr_en;
          end;
        if Self.Mode = bmNastrHand then
          begin
            FAdjPanHandNastr.ParamValue['StrSt', 'Min'] := 0;
            FAdjPanHandNastr.ParamValue['StrSt', 'Max'] := FAScan.Kanal.NStr2TP_en;
            FAdjPanHandNastr.ParamValue['StrEd', 'Min'] := FAScan.Kanal.NStr2TP_en;
            FAdjPanHandNastr.ParamValue['StrEd', 'Max'] := FAScan.Kanal.NStr2TP_en;
          end;
        AdjustBut.Enabled := false;
      end;
    end;

    SetMMScaleType;
    FAScan.ClearPlus;
  end;
end;

(*<Rud23>*)
procedure TAScanFrame.SetMMScaleType;
var
  NewType: TScaleType;
begin
  if Assigned(FAScan) then
  begin
    (*<Rud37>*)
    if Self.Mode in [bmEstimate, bmHand] then
        NewType := stH

    else if Self.Mode = bmNastr then
              if FAdjPanNastr.CurPanel = 1 then
                NewType := stH
              else NewType := stR

    else if Self.Mode = bmNastrHand then
              if FAdjPanHandNastr.CurPanel = 1 then
                NewType := stH
              else NewType := stR;
   (*</Rud37>*)


    FAScan.MMScaleType := NewType;
    FAScan.ReBuild;
    FMeasurePanel.MMScaleType := NewType;
  end;
end;
(*</Rud23>*)

procedure TAScanFrame.Refresh;
var
  D: Int64;
begin
  if FMode in [bmEstimate, bmHand] then
    FBScan.Refresh(0, true, true);
  FAScan.Refresh;
  D := FHandRegMaxTime - GetTickCount;
  FMeasurePanel.Refresh;

  if General.Core.HandRegistrationOn then
  begin
    if D > 0 then
      FAdjPanHandWork.ParamValue['Timer', 'Value'] := Round(D / 1000)
    else
    begin
      General.Core.StopHandRegistration;
      if Assigned(OnEnableHandKanals) then
        OnEnableHandKanals(true);
      FAdjPanHandWork.ChangesEnabled:= true;
      FBScan.Clear;
      FBScan.Refresh(0, true, true);
      AdjButtonStatus('Start');
      FAdjPanHandWork.ParamValue['Timer', 'Value'] := FHandRegMaxTimeSecSave;
    end;
  end;
end;

procedure TAScanFrame.Resize;
begin
  inherited;
  if FMode in [bmNastr, { bmHand, } bmNastrHand] then
  begin
    FBScan.Height := 0;
    AdjustBut.Height := Round(FOptionsPan.Height * 0.2);
    AdjustBut.Font.Height := Round(AdjustBut.Height * 0.3);
    AdjustBut.Visible := true;
    AdjButtonStatus('Clear');
  end
  else if FMode in [bmHand] then
  begin
    FBScan.Height := Round((Self.Height / 100) * AScanFrame_BScanHeight);
    AdjustBut.Height := Round(FOptionsPan.Height * 0.2);
    AdjustBut.Font.Height := Round(AdjustBut.Height * 0.2);
    AdjustBut.Visible := true;
    AdjButtonStatus('Start');
  end
  else
  begin
    FBScan.Height := Round((Self.Height / 100) * AScanFrame_BScanHeight);
    AdjustBut.Height := 0;
    AdjustBut.Visible := false;
  end;

  FMeasurePan.Height := Round
    ((Self.Height / 100) * AScanFrame_MeasurePanHeight);

  FOptionsPan.Width := AScanFrame_OptionsPanWidth;
end;

procedure TAScanFrame.SetAmpByColor(Value: Boolean);
begin
  FBScan.AmpByColor := Value;
end;

procedure TAScanFrame.SetAmpBySize(Value: Boolean);
begin
  FBScan.AmplBySize := Value;
end;

procedure TAScanFrame.SetBScale(Value: integer);
begin
  if Assigned(FBScan) then
    FBScan.Scale := Value;
end;

procedure TAScanFrame.UpDateKanalParams;
begin
  if not Assigned(FKanal) then
    Exit;

  FBScan.UpDateStrob;

  case FMode of
    bmEstimate:
      begin
        if FKanal.StrobStartMin >= 0 then
          FAdjPanWork.ParamValue['StrSt', 'Min'] := FKanal.StrobStartMin
        else
          FAdjPanWork.ParamValue['StrSt', 'Min'] := 0;

        if FKanal.StrobStartMax >= 0 then
          FAdjPanWork.ParamValue['StrSt', 'Max'] := FKanal.StrobStartMax
        else
          FAdjPanWork.ParamValue['StrSt', 'Max'] := General.Core.StrobEd;

        if FKanal.StrobEndMin >= 0 then
          FAdjPanWork.ParamValue['StrEd', 'Min'] := FKanal.StrobEndMin
        else
          FAdjPanWork.ParamValue['StrEd', 'Min'] := General.Core.StrobSt;

        if FKanal.StrobEndMax >= 0 then
          FAdjPanWork.ParamValue['StrEd', 'Max'] := FKanal.StrobEndMax
        else
          FAdjPanWork.ParamValue['StrEd', 'Max'] := FKanal.Takt.Ten;

        if (General.Core.StrobSt < FAdjPanWork.ParamValue['StrSt', 'Min']) or
          (General.Core.StrobSt > FAdjPanWork.ParamValue['StrSt', 'Max']) then
          General.Core.StrobSt := FAdjPanWork.ParamValue['StrSt', 'Min'];

        if (General.Core.StrobEd < FAdjPanWork.ParamValue['StrEd', 'Min']) or
          (General.Core.StrobEd > FAdjPanWork.ParamValue['StrEd', 'Max']) then
          General.Core.StrobEd := FAdjPanWork.ParamValue['StrEd', 'Max'];

        FAdjPanWork.ParamValue['StrSt', 'Value'] := General.Core.StrobSt;
        FAdjPanWork.ParamValue['StrEd', 'Value'] := General.Core.StrobEd;

        FAdjPanWork.ParamValue['Ku', 'Value'] := FKanal.Ky;
        FAdjPanWork.ParamValue['Metka', 'Value'] := FKanal.Takt.ZonaM;
        FAdjPanWork.ParamValue['PrTime', 'Value'] := FKanal.TwoTp;
        FAdjPanWork.ParamValue['VRCH', 'Max'] := 30;

        if FKanal.VRCH > FAdjPanWork.ParamValue['VRCH', 'Max'] then
          FKanal.VRCH := FAdjPanWork.ParamValue['VRCH', 'Max'];

        FAdjPanWork.ParamValue['VRCH', 'Value'] := FKanal.VRCH;
        if FKanal.SoundOn then
          FAdjPanWork.ParamValue['Snd', 'Value'] := 0
        else
          FAdjPanWork.ParamValue['Snd', 'Value'] := 1;
        if FKanal.Num < 2 then
          FAdjPanWork.ParamValue['VRCH', 'Max'] := 20
        else
          FAdjPanWork.ParamValue['VRCH', 'Max'] := 40;
        FAdjPanWork.ParamValue['snd', 'Value'] := 0;
      end;
    bmHand:
      begin
        FAdjPanHandWork.ParamValue['StrSt', 'Value'] := General.Core.StrobSt;
        FAdjPanHandWork.ParamValue['StrEd', 'Value'] := General.Core.StrobEd;
        FAdjPanHandWork.ParamValue['StrSt', 'Max'] := General.Core.StrobEd;
        FAdjPanHandWork.ParamValue['StrEd', 'Min'] := General.Core.StrobSt;
        FAdjPanHandWork.ParamValue['Ku', 'Value'] := FKanal.Ky;
        FAdjPanHandWork.ParamValue['Metka', 'Value'] := FKanal.Takt.ZonaM;
        FAdjPanHandWork.ParamValue['PrTime', 'Value'] := FKanal.TwoTp;


        FAdjPanWork.ParamValue['VRCH', 'Max'] := 30;

        if FKanal.VRCH > FAdjPanWork.ParamValue['VRCH', 'Max'] then
          FKanal.VRCH := FAdjPanWork.ParamValue['VRCH', 'Max'];

        FAdjPanHandWork.ParamValue['VRCH', 'Value'] := FKanal.VRCH;
        if FKanal.Num < 2 then
          FAdjPanHandWork.ParamValue['VRCH', 'Max'] := 20
        else
          FAdjPanHandWork.ParamValue['VRCH', 'Max'] := 40;
        FAdjPanHandWork.ParamValue['snd', 'Value'] := 0;
        FAdjPanHandWork.ParamValue['Surface', 'Value'] := 0;
        FAdjPanHandWork.ParamValue['HRail', 'Value'] := 0;
        FHandSurface := 0;
        FHandRail := 0;
      end;
    bmNastr:
      begin
        FAdjPanNastr.ParamValue['StrSt', 'Max'] := General.Core.StrobEd;
        FAdjPanNastr.ParamValue['StrSt', 'Min'] := 0;
        FAdjPanNastr.ParamValue['StrSt', 'Value'] := General.Core.StrobSt;
        FAdjPanNastr.ParamValue['Ku', 'Value'] := FKanal.Ky;
        FAdjPanNastr.ParamValue['Ku', 'ConstValue'] := FKanal.Ky_r;
        FAdjPanNastr.ParamValue['PrTime', 'Value'] := FKanal.TwoTp;
        FAdjPanWork.ParamValue['VRCH', 'Max'] := 30;

        if FKanal.VRCH > FAdjPanWork.ParamValue['VRCH', 'Max'] then
          FKanal.VRCH := FAdjPanWork.ParamValue['VRCH', 'Max'];

        FAdjPanNastr.ParamValue['VRCH', 'Value'] := FKanal.VRCH;

      end;
    bmNastrHand:
      begin
        FAdjPanHandNastr.ParamValue['StrSt', 'Max'] := General.Core.StrobEd;
        FAdjPanHandNastr.ParamValue['StrSt', 'Min'] := 0;
        FAdjPanHandNastr.ParamValue['StrSt', 'Value'] := General.Core.StrobSt;
        FAdjPanHandNastr.ParamValue['Ku', 'Value'] := FKanal.Ky;
        FAdjPanHandNastr.ParamValue['Ku', 'ConstValue'] := FKanal.Ky_r;
        FAdjPanHandNastr.ParamValue['PrTime', 'Value'] := FKanal.TwoTp;
        FAdjPanWork.ParamValue['VRCH', 'Max'] := 30;

        if FKanal.VRCH > FAdjPanWork.ParamValue['VRCH', 'Max'] then
          FKanal.VRCH := FAdjPanWork.ParamValue['VRCH', 'Max'];

        FAdjPanHandNastr.ParamValue['VRCH', 'Value'] := FKanal.VRCH;
      end;
  end;
end;

procedure TAScanFrame.UpDateKanalParamsNew;
begin
  if not Assigned(FKanal) then
    Exit;

  FBScan.UpDateStrob;

  case FMode of
    bmEstimate:
      begin
        FAdjPanWork.ParamValue['StrSt', 'Min'] := 0;
        FAdjPanWork.ParamValue['StrSt', 'Max'] := 200;

        FAdjPanWork.ParamValue['StrEd', 'Min'] := 0;
        FAdjPanWork.ParamValue['StrEd', 'Max'] := 200;

        FAdjPanWork.ParamValue['StrSt', 'Value'] := General.Core.StrobSt;
        FAdjPanWork.ParamValue['StrEd', 'Value'] := General.Core.StrobEd;
        FAdjPanWork.ParamValue['Ku', 'Value'] := FKanal.Ky;
        FAdjPanWork.ParamValue['Metka', 'Value'] := FKanal.Takt.ZonaM;
        FAdjPanWork.ParamValue['PrTime', 'Value'] := FKanal.TwoTp;

        FAdjPanWork.ParamValue['VRCH', 'Max'] := 30;

        if FKanal.VRCH > FAdjPanWork.ParamValue['VRCH', 'Max'] then
          FKanal.VRCH := FAdjPanWork.ParamValue['VRCH', 'Max'];

        FAdjPanWork.ParamValue['VRCH', 'Value'] := FKanal.VRCH;
        if FKanal.SoundOn then
          FAdjPanWork.ParamValue['Snd', 'Value'] := 0
        else
          FAdjPanWork.ParamValue['Snd', 'Value'] := 1;
        FAdjPanWork.ParamValue['snd', 'Value'] := 0;
      end;
    bmHand:
      begin
        FAdjPanHandWork.ParamValue['StrSt', 'Min'] := 0;
        FAdjPanHandWork.ParamValue['StrSt', 'Max'] := 200;

        FAdjPanHandWork.ParamValue['StrEd', 'Min'] := 0;
        FAdjPanHandWork.ParamValue['StrEd', 'Max'] := 200;

        FAdjPanHandWork.ParamValue['StrSt', 'Value'] := General.Core.StrobSt;
        FAdjPanHandWork.ParamValue['StrEd', 'Value'] := General.Core.StrobEd;
        FAdjPanHandWork.ParamValue['StrSt', 'Max'] := General.Core.StrobEd;
        FAdjPanHandWork.ParamValue['StrEd', 'Min'] := General.Core.StrobSt;
        FAdjPanHandWork.ParamValue['Ku', 'Value'] := FKanal.Ky;
        FAdjPanHandWork.ParamValue['Metka', 'Value'] := FKanal.Takt.ZonaM;
        FAdjPanHandWork.ParamValue['PrTime', 'Value'] := FKanal.TwoTp;

        FAdjPanWork.ParamValue['VRCH', 'Max'] := 30;

        if FKanal.VRCH > FAdjPanWork.ParamValue['VRCH', 'Max'] then
          FKanal.VRCH := FAdjPanWork.ParamValue['VRCH', 'Max'];

        FAdjPanHandWork.ParamValue['VRCH', 'Value'] := FKanal.VRCH;
        FAdjPanHandWork.ParamValue['snd', 'Value'] := 0;
        FAdjPanHandWork.ParamValue['Surface', 'Value'] := 0;
        FAdjPanHandWork.ParamValue['HRail', 'Value'] := 0;
        FHandSurface := 0;
        FHandRail := 0;
      end;
    bmNastr:
      begin
        FAdjPanNastr.ParamValue['StrSt', 'Min'] := 0;
        FAdjPanNastr.ParamValue['StrSt', 'Max'] := 200;

        FAdjPanNastr.ParamValue['StrEd', 'Min'] := 0;
        FAdjPanNastr.ParamValue['StrEd', 'Max'] := 200;

        FAdjPanNastr.ParamValue['StrSt', 'Max'] := General.Core.StrobEd;
        FAdjPanNastr.ParamValue['StrSt', 'Min'] := 0;
        FAdjPanNastr.ParamValue['StrSt', 'Value'] := General.Core.StrobSt;
        FAdjPanNastr.ParamValue['Ku', 'Value'] := FKanal.Ky;

        FAdjPanNastr.ParamValue['Ku', 'ConstValue'] := FKanal.Ky_r;
        FAdjPanNastr.ParamValue['PrTime', 'Value'] := FKanal.TwoTp;
        FAdjPanNastr.ParamValue['VRCH', 'Max'] := 30;

        if FKanal.VRCH > FAdjPanNastr.ParamValue['VRCH', 'Max'] then
          FKanal.VRCH := FAdjPanNastr.ParamValue['VRCH', 'Max'];

        FAdjPanNastr.ParamValue['VRCH', 'Value'] := FKanal.VRCH;

      end;
    bmNastrHand:
      begin
        FAdjPanHandNastr.ParamValue['StrSt', 'Min'] := 0;
        FAdjPanHandNastr.ParamValue['StrSt', 'Max'] := 200;

        FAdjPanHandNastr.ParamValue['StrEd', 'Min'] := 0;
        FAdjPanHandNastr.ParamValue['StrEd', 'Max'] := 200;

        FAdjPanHandNastr.ParamValue['StrSt', 'Max'] := General.Core.StrobEd;
        FAdjPanHandNastr.ParamValue['StrSt', 'Min'] := 0;
        FAdjPanHandNastr.ParamValue['StrSt', 'Value'] := General.Core.StrobSt;
        FAdjPanHandNastr.ParamValue['Ku', 'Value'] := FKanal.Ky;
        FAdjPanHandNastr.ParamValue['Ku', 'ConstValue'] := FKanal.Ky_r;
        FAdjPanHandNastr.ParamValue['PrTime', 'Value'] := FKanal.TwoTp;
        FAdjPanHandNastr.ParamValue['VRCH', 'Max'] := 30;

        if FKanal.VRCH > FAdjPanHandNastr.ParamValue['VRCH', 'Max'] then
          FKanal.VRCH := FAdjPanHandNastr.ParamValue['VRCH', 'Max'];

        FAdjPanHandNastr.ParamValue['VRCH', 'Value'] := FKanal.VRCH;
      end;
  end;
end;

procedure TAScanFrame.SetKanal(Value: TKanal);
begin
  FKanal := Value;
  if FMode = bmHand then
    AdjButtonStatus('Start')
  else
    AdjButtonStatus('Clear');
  // �������� �-��������� ��� ��������� �����
  FAScan.Kanal := FKanal;
  // �������� �-��������� ��� ��������� �����
  FBScan.Lines[0].ClearKanals;
  if Assigned(FKanal) then
  begin
    // if (FKanal.Num = 0) or (FKanal.Num = 1)  then
    if (FKanal.Takt.Alfa = 0) then
    begin
      // FBScan.Lines[0].Kanal1 := FKanal.Takt.Kanal2;
      // FBScan.Lines[0].Kanal2 := FKanal.Takt.Kanal1;
      FBScan.Lines[0].AddKanal(FKanal.Takt.Kanal2);
      FBScan.Lines[0].AddKanal(FKanal.Takt.Kanal1);
    end
    else if FKanal.Num mod 2 = 0 then
      // FBScan.Lines[0].Kanal1 := FKanal
      FBScan.Lines[0].AddKanal(FKanal)
    else
    begin
      FBScan.Lines[0].AddKanal(nil);
      FBScan.Lines[0].AddKanal(FKanal);
    end;

    (*<Rud12>*)
    BlockChangeStrob;
    (*</Rud12>*)
  end;

  // ������� �� ������ �������� ��� ������� ��������� ������
  // Self.UpDateKanalParams;
  Self.UpDateKanalParamsNew;
end;

procedure TAScanFrame.SetPointSize(Value: TBViewPointSize);
begin
  FBScan.PointSize := Value;
end;

procedure TAScanFrame.SetScanDir(Value: TZondPosition);
begin
  FBScan.BScanDirection := Value;
end;

procedure TAScanFrame.SetWinMode(Value: TBScanWinMode);
begin
  FAScan.FixMaximum := false;
  case Value of
    bmEstimate:
      begin
        FMode := Value;
        FBScan.CoordUnits := cuWay;
        FMeasurePanel.AttVis := false;
        FAdjPanWork.BringToFront;
        Resize;
      end;
    bmNastr:
      begin
        FMode := Value;
        FAScan.FixMaximum := true;
        FBScan.CoordUnits := cuWay;
        FMeasurePanel.AttVis := true;
        FAdjPanNastr.BringToFront;
        if FAdjPanNastr.CurPanel = 1 then
          AdjustBut.Enabled := true
        else
          AdjustBut.Enabled := false;
        Resize;
      end;
    bmHand:
      begin
        FMode := Value;
        FBScan.CoordUnits := cuSec;
        FMeasurePanel.AttVis := false;
        FAdjPanHandWork.BringToFront;
        if General.Core.RegOnUserLevel then
          AdjustBut.Enabled := true
        else
          AdjustBut.Enabled := false;
        // ������� ����� �������� ��� ������� !!!
        Resize;
      end;
    bmNastrHand:
      begin
        FMode := Value;
        FAScan.FixMaximum := true;
        FBScan.CoordUnits := cuWay;
        FMeasurePanel.AttVis := true;
        FAdjPanHandNastr.BringToFront;
        if FAdjPanNastr.CurPanel = 1 then
          AdjustBut.Enabled := true
        else
          AdjustBut.Enabled := false;
        // ������� ����� �������� ��� ������� !!!
        Resize;
      end;
  end;
  Self.Resize;
  FMeasurePanel.Mode := Value;
  FAScan.ReBuild;
  FAScan.RePaint;
  FBScan.UpDateStrob;
  Self.Visible := true;
  Self.BringToFront;
  (*<Rud37>*)
  GAScanFrame.SetMMScaleType;
  (*<Rud37>*)
end;

(*<Rud12>*)
procedure TAScanFrame.BlockChangeStrob;
begin
  {$IFnDEF RKS_U}
    if (FKanal.Takt.Alfa = 0) or (FKanal.Takt.Alfa = 42) then
    begin
      // ��� 0� ��� � 42� ����� ����������� ������ ����� ������
      if FKanal.Num in [1,11,6,7] then
      begin
        FAdjPanWork.Panels['1'].Button['StrSt'].Enabled := true;
        FAdjPanWork.Panels['1'].Button['StrEd'].Enabled := false;
      end

      // ��� 42� ������� ����������� ������ ������ ������
      else if FKanal.Num in [8,9] then
      begin
        FAdjPanWork.Panels['1'].Button['StrSt'].Enabled := false;
        FAdjPanWork.Panels['1'].Button['StrEd'].Enabled := true;
      end

      // ��� 0� ��� ����������� ������ � ����� ������
      else if FKanal.Num in [0,10] then
      begin
        FAdjPanWork.Panels['1'].Button['StrSt'].Enabled := false;
        FAdjPanWork.Panels['1'].Button['StrEd'].Enabled := false;
      end;

    end
    else
      begin
        FAdjPanWork.Panels['1'].Button['StrSt'].Enabled := true;
        FAdjPanWork.Panels['1'].Button['StrEd'].Enabled := true;
      end;

    FAdjPanWork.Panels['1'].Button['StrSt'].ReDraw;
    FAdjPanWork.Panels['1'].Button['StrEd'].ReDraw;
 {$ENDIF}
end;
(*</Rud12>*)

procedure TAScanFrame.Stop;
begin
  FAScan.Stop;
  Self.Kanal := nil;
  Self.Visible := false;
end;

end.
