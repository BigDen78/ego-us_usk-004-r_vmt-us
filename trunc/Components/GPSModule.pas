unit GPSModule;

interface


uses
  {WinApi.}Windows, GPS, CPortCtl, CPort, Dialogs, Forms,

   Messages, SysUtils, Variants, Classes, Graphics, Controls,
  StdCtrls,  ExtCtrls;


type

  TOnCoordChange = procedure (Latitude, Longitude, Speed: Single) of object;
  TOnStateChange = procedure (State: Boolean; SatUsedCount: Integer) of object;

  TGPSModule = class
  private
      FOnCoordChange: TOnCoordChange;
      FOnStateChange: TOnStateChange;
      FGPS: TGPS;
      FOldValid: Boolean;
      FOldSatsUsed: Integer;
      FSendPeriod: Integer;
      FSetTime: DWord;
      FLatitude: Single;
      FLongitude: Single;
      FSpeed: Single;

      function GetConnected: Boolean;
      procedure GPSDatasChange(Sender: TObject; GPSDatas: TGPSDatas);
      procedure GPSSatEvent(Sender: TObject; NbSat, NbSatUse: Shortint; Sats: TSatellites);

  public

      constructor Create;
      destructor Destroy;
      procedure Start; // ������ ������
      procedure Stop; // ��������� ������
      property OnCoordChange: TOnCoordChange read FOnCoordChange write FOnCoordChange; // ������� ��������� ����������
      property OnStateChange: TOnStateChange read FOnStateChange write FOnStateChange; // ������� ��������� c��������
      property Latitude: Single read FLatitude; // ������� ������
      property Longitude: Single read FLongitude; // ������� �������
      property Speed: Single read FSpeed;
      property State: Boolean read FOldValid; // ��������� (�������� GPS ����������)
      property Connected: Boolean read GetConnected; // ���� ������� �����������

  end;

implementation

// -----------------------------------------------------------------------------

constructor TGPSModule.Create;
begin
  FGPS:= TGPS.Create(nil);
  FGPS.Port:= 'COM3';
  FGPS.BaudRate:= br4800;
  FGPS.OnGPSDatasChange:= GPSDatasChange;
  FGPS.OnSatellitesChange:= GPSSatEvent;
  FOldValid:= False;
  FOldSatsUsed:= 0;
  FOnCoordChange:= nil;
  FOnCoordChange:= nil;
  FSetTime:= 0;
  FSendPeriod:= 2000;
end;

destructor TGPSModule.Destroy;
begin
  FGPS.Free;
end;

function TGPSModule.GetConnected: Boolean;
begin
  Result:= FGPS.Connected;
end;

procedure TGPSModule.GPSDatasChange(Sender: TObject; GPSDatas: TGPSDatas);
begin

  FLatitude:= GPSDatas.Latitude;
  FLongitude:= GPSDatas.Longitude;
  FSpeed:= GPSDatas.Speed;

  if (FOldValid <> GPSDatas.Valid) or (FOldSatsUsed <> GPSDatas.NbrSatsUsed) then
  begin
    if Assigned(FOnStateChange) then
      FOnStateChange(GPSDatas.Valid, GPSDatas.NbrSatsUsed);
    FOldValid:= GPSDatas.Valid;
    FOldSatsUsed:= GPSDatas.NbrSatsUsed;
  end;

  if GetTickCount() - FSetTime >= FSendPeriod then
  begin
    if GPSDatas.Valid and Assigned(FOnCoordChange) then
      FOnCoordChange(FLatitude, FLongitude, FSpeed);
  	FSetTime:= GetTickCount();
  end;
end;

procedure TGPSModule.GPSSatEvent(Sender: TObject; NbSat, NbSatUse: Shortint; Sats: TSatellites);
begin
  if FOldSatsUsed <> NbSatUse then
  begin
    if Assigned(FOnStateChange) then
      FOnStateChange(FOldValid, NbSatUse);
    FOldSatsUsed:= NbSatUse;
  end;
end;

procedure TGPSModule.Start;
begin
  FGPS.Open();
end;

procedure TGPSModule.Stop;
begin
  FGPS.Close();
end;

end.
