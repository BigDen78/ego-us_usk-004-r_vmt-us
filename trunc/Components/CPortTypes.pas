//https://bitbucket.org/wpostma/tcomport2010/commits/4e29d376dae5c766a3da826a4ec6cd3626c84457
//https://bitbucket.org/wpostma/tcomport2010/src/4e29d376dae5c766a3da826a4ec6cd3626c84457/source/CPortTypes.pas

unit CPortTypes;
(******************************************************
 * TComPort AnsiString Library Version 5              *
 * for Delphi                                         *
 * original by Dejan Crnila, 1998 - 2002              *
 * version 4, version 5, by Warren Postma             *
 *                                                    *
 * Tested in Delphi 2009,2010,XE,XE2,XE3, and         *
 * still works in Delphi 7 and 2007 also.             *
 *****************************************************)

 { Data Type Declarations used in various CPort code units }

interface

uses   Windows, Messages, Classes, SysUtils, IniFiles, Registry;

type

// this is the type Warren changed most of the untyped-parameters used
// in CPort.pas and CPortCtl.pas to:
  PCPortAnsiChar = PAnsiChar;

  // various types formerly in CPort.pas:
  TCPortChar = AnsiChar;
  TPort = string;
  TBaudRate = (brCustom, br110, br300, br600, br1200, br2400, br4800, br9600, br14400,
    br19200, br38400, br56000, br57600, br115200, br128000, br256000);
  TStopBits = (sbOneStopBit, sbOne5StopBits, sbTwoStopBits);
  TDataBits = (dbFive, dbSix, dbSeven, dbEight);
  TParityBits = (prNone, prOdd, prEven, prMark, prSpace);
  TDTRFlowControl = (dtrDisable, dtrEnable, dtrHandshake);
  TRTSFlowControl = (rtsDisable, rtsEnable, rtsHandshake, rtsToggle);
  TFlowControl = (fcHardware, fcSoftware, fcNone, fcCustom);
  TComEvent = (evRxChar, evTxEmpty, evRxFlag, evRing, evBreak, evCTS, evDSR,
    evError, evRLSD, evRx80Full);
  TComEvents = set of TComEvent;
  TComSignal = (csCTS, csDSR, csRing, csRLSD);
  TComSignals = set of TComSignal;
  TComError = (ceFrame, ceRxParity, ceOverrun, ceBreak, ceIO, ceMode, ceRxOver,
    ceTxFull);
  TComErrors = set of TComError;
  TSyncMethod = (smThreadSync, smWindowSync, smNone, smDisableEvents);
  TStoreType = (stRegistry, stIniFile);
  TStoredProp = (spBasic, spFlowControl, spBuffer, spTimeouts, spParity,
    spOthers);
  TStoredProps = set of TStoredProp;
  TComLinkEvent = (leConn, leCTS, leDSR, leRLSD, leRing, leRx, leTx,
    leTxEmpty, leRxFlag);
  TRxCharEvent = procedure(Sender: TObject; Count: Integer) of object;
  TRxBufEvent = procedure(Sender: TObject; const Buffer:PCPortAnsiChar;
    Count: Integer) of object;
  TComErrorEvent = procedure(Sender: TObject; Errors: TComErrors) of object;
  TComSignalEvent = procedure(Sender: TObject; OnOff: Boolean) of object;



  // types for asynchronous calls
  TOperationKind = (okWrite, okRead);
  TCPortAsync = record
    Overlapped: TOverlapped;
    Kind: TOperationKind;
    Data: PCPortAnsiChar;
    Size: Integer;
  end;
  PCPortAsync = ^TCPortAsync;

  TComPortAbstract = class;

  // parity settings
  TComParity = class(TPersistent)
  private
    FComPort: TComPortAbstract;
    FBits: TParityBits;
    FCheck: Boolean;
    FReplace: Boolean;
    FReplaceChar: TCPortChar;
    procedure SetBits(const Value: TParityBits);
    procedure SetCheck(const Value: Boolean);
    procedure SetReplace(const Value: Boolean);
    procedure SetReplaceChar(const Value: TCPortChar);
  protected
    procedure AssignTo(Dest: TPersistent); override;
    procedure SetComPort(const AComPort: TComPortAbstract);

  public
    constructor Create;
    property ComPort: TComPortAbstract read FComPort;
  published
    property Bits: TParityBits read FBits write SetBits;
    property Check: Boolean read FCheck write SetCheck default False;
    property Replace: Boolean read FReplace write SetReplace default False;
    property ReplaceChar: TCPortChar read FReplaceChar write SetReplaceChar default #0;
  end;

  // timoeout properties for read/write operations
  TComTimeouts = class(TPersistent)
  private
    FComPort: TComPortAbstract;
    FReadInterval: Integer;
    FReadTotalM: Integer;
    FReadTotalC: Integer;
    FWriteTotalM: Integer;
    FWriteTotalC: Integer;
    procedure SetComPort(const AComPort: TComPortAbstract);
    procedure SetReadInterval(const Value: Integer);
    procedure SetReadTotalM(const Value: Integer);
    procedure SetReadTotalC(const Value: Integer);
    procedure SetWriteTotalM(const Value: Integer);
    procedure SetWriteTotalC(const Value: Integer);
  protected
    procedure AssignTo(Dest: TPersistent); override;
  public
    constructor Create;
    property ComPort: TComPortAbstract read FComPort;
  published
    property ReadInterval: Integer read FReadInterval write SetReadInterval default -1;
    property ReadTotalMultiplier: Integer read FReadTotalM write SetReadTotalM default 0;
    property ReadTotalConstant: Integer read FReadTotalC write SetReadTotalC default 0;
    property WriteTotalMultiplier: Integer
      read FWriteTotalM write SetWriteTotalM default 100;
    property WriteTotalConstant: Integer
      read FWriteTotalC write SetWriteTotalC default 1000;
  end;
  //--------------------------------------------------------------------------
  // TComPortAbstract
  //
  // New abstract base class allows us to substitute one com port
  // class for another network-aware one, without rewrite our entire apps,
  // if we stick to this basic interface:
  //--------------------------------------------------------------------------
  TComPortAbstract = class(TCOmponent)
   private
    FBaudRate: TBaudRate;
    FCustomBaudRate: Integer;
    FPort: TPort;
    FStopBits: TStopBits;
    FDataBits: TDataBits;
    FParity: TComParity;


   protected
    FTimeouts: TComTimeouts;


     {property accessors}
    procedure SetConnected(const Value: Boolean); virtual; abstract;
    function  GetConnected:Boolean; virtual; abstract;

    procedure SetBaudRate(const Value: TBaudRate);
    procedure SetCustomBaudRate(const Value: Integer);
    procedure SetPort(const Value: TPort);
    procedure SetStopBits(const Value: TStopBits);
    procedure SetDataBits(const Value: TDataBits);
    procedure SetParity(const Value: TComParity);
    procedure SetTimeouts(const Value: TComTimeouts);


    procedure CreateHandle; virtual; abstract;
    procedure DestroyHandle; virtual; abstract;
    procedure ApplyDCB; virtual; abstract;
    procedure ApplyTimeouts; virtual; abstract;
    procedure ApplyBuffer; virtual; abstract;
    procedure SetupComPort; virtual; abstract;

   public
   constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    function Write(const mBuffer:PCPortAnsiChar; Count: Integer): Integer;  virtual; abstract;
    function WriteStr(const Str: AnsiString): Integer; virtual; abstract;
    function Read(mBuffer:PCPortAnsiChar; Count: Integer): Integer; virtual; abstract;
    function ReadStr(var Str: AnsiString; Count: Integer): Integer; virtual; abstract;

    procedure Open; virtual; abstract;
    procedure Close; virtual; abstract;

    property Connected: Boolean read GetConnected write SetConnected default False;
    property BaudRate: TBaudRate read FBaudRate write SetBaudRate;
    property CustomBaudRate: Integer
      read FCustomBaudRate write SetCustomBaudRate;
    property Port: TPort read FPort write SetPort;
    property Parity: TComParity read FParity write SetParity;
    property StopBits: TStopBits read FStopBits write SetStopBits;
    property DataBits: TDataBits read FDataBits write SetDataBits;
    property Timeouts: TComTimeouts read FTimeouts write SetTimeouts;

  end;

function ComErrorsToStr(Errors:TComErrors):String;


implementation



function ComErrorsToStr(Errors:TComErrors):String;
  procedure e(msg:String);
  begin
     if result='' then
        result := msg
     else
        result := result+','+msg;
  end;
begin
   result := '';
   if ceFrame    in Errors then e('Frame');
   if ceRxParity in Errors then e('Parity');
   if ceOverrun  in Errors then e('Overrun');
   if ceBreak    in Errors then e('Break');
   if ceIO       in Errors then e('IO');
   if ceMode     in Errors then e('Mode');
   if ceRxOver   in Errors then e('RxOver');
   if ceTxFull   in Errors then e('TxFull');
   if result = '' then
      result := '<Ok>'
   else
      result := '<ComError:'+result+'>';
end;


(*****************************************
 * TComParity class                      *
 *****************************************)

// create class
constructor TComParity.Create;
begin
  inherited Create;
  FBits := prNone;
end;

// copy properties to other class
procedure TComParity.AssignTo(Dest: TPersistent);
begin
  if Dest is TComParity then
  begin
    with TComParity(Dest) do
    begin
      FBits        := Self.Bits;
      FCheck       := Self.Check;
      FReplace     := Self.Replace;
      FReplaceChar := Self.ReplaceChar;
    end
  end
  else
    inherited AssignTo(Dest);
end;

// select TComPortAbstract to own this class
procedure TComParity.SetComPort(const AComPort: TComPortAbstract);
begin
  FComPort := AComPort;
end;

// set parity bits
procedure TComParity.SetBits(const Value: TParityBits);
begin
  if Value <> FBits then
  begin
    FBits := Value;
    if FComPort <> nil then
      FComPort.ApplyDCB;
  end;
end;

// set check parity
procedure TComParity.SetCheck(const Value: Boolean);
begin
  if Value <> FCheck then
  begin
    FCheck := Value;
    if FComPort <> nil then
      FComPort.ApplyDCB;
  end;
end;

// set replace on parity error
procedure TComParity.SetReplace(const Value: Boolean);
begin
  if Value <> FReplace then
  begin
    FReplace := Value;
    if FComPort <> nil then
      FComPort.ApplyDCB;
  end;
end;

// set replace char
procedure TComParity.SetReplaceChar(const Value: TCPortChar);
begin
  if Value <> FReplaceChar then
  begin
    FReplaceChar := Value;
    if FComPort <> nil then
      FComPort.ApplyDCB;
  end;
end;


(*****************************************
 * TComTimeouts class                    *
 *****************************************)

// create class
constructor TComTimeouts.Create;
begin
  inherited Create;
  FReadInterval := -1;
  FWriteTotalM := 100;
  FWriteTotalC := 1000;
end;

// copy properties to other class
procedure TComTimeouts.AssignTo(Dest: TPersistent);
begin
  if Dest is TComTimeouts then
  begin
    with TComTimeouts(Dest) do
    begin
      FReadInterval := Self.ReadInterval;
      FReadTotalM   := Self.ReadTotalMultiplier;
      FReadTotalC   := Self.ReadTotalConstant;
      FWriteTotalM  := Self.WriteTotalMultiplier;
      FWriteTotalC  := Self.WriteTotalConstant;
    end
  end
  else
    inherited AssignTo(Dest);
end;

// select TComPortAbstract to own this class
procedure TComTimeouts.SetComPort(const AComPort: TComPortAbstract);
begin
  FComPort := AComPort;
end;

// set read interval
procedure TComTimeouts.SetReadInterval(const Value: Integer);
begin
  if Value <> FReadInterval then
  begin
    FReadInterval := Value;
    // if possible, apply the changes
    if FComPort <> nil then
      FComPort.ApplyTimeouts;
  end;
end;

// set read total constant
procedure TComTimeouts.SetReadTotalC(const Value: Integer);
begin
  if Value <> FReadTotalC then
  begin
    FReadTotalC := Value;
    if FComPort <> nil then
      FComPort.ApplyTimeouts;
  end;
end;

// set read total multiplier
procedure TComTimeouts.SetReadTotalM(const Value: Integer);
begin
  if Value <> FReadTotalM then
  begin
    FReadTotalM := Value;
    if FComPort <> nil then
      FComPort.ApplyTimeouts;
  end;
end;

// set write total constant
procedure TComTimeouts.SetWriteTotalC(const Value: Integer);
begin
  if Value <> FWriteTotalC then
  begin
    FWriteTotalC := Value;
    if FComPort <> nil then
      FComPort.ApplyTimeouts;
  end;
end;

// set write total multiplier
procedure TComTimeouts.SetWriteTotalM(const Value: Integer);
begin
  if Value <> FWriteTotalM then
  begin
    FWriteTotalM := Value;
    if FComPort <> nil then
      FComPort.ApplyTimeouts;
  end;
end;

//-----------------------------------------------------------------
// TComPortAbstract methods
//-----------------------------------------------------------------


// Set one of the normal Baud Rates (BPS Rates)
constructor TComPortAbstract.Create(AOwner: TComponent);
begin
  inherited;
  FBaudRate := br9600;
  FCustomBaudRate := 9600;
  FPort := 'COM1';
  FStopBits := sbOneStopBit;
  FDataBits := dbEight;
  FParity := TComParity.Create;
  FParity.SetComPort(Self);
  FTimeouts := TComTimeouts.Create;
  FTimeouts.SetComPort(Self);
end;

destructor TComPortAbstract.Destroy;
begin
  FParity.Free;
  FTimeouts.Free;
  inherited;
end;

procedure TComPortAbstract.SetBaudRate(const Value: TBaudRate);
begin
  if Value <> FBaudRate then
  begin
    FBaudRate := Value;
    // if possible, apply settings
    ApplyDCB;
  end;
end;

// Set custom baud rate (BPS Rate)
procedure TComPortAbstract.SetCustomBaudRate(const Value: Integer);
begin
  if Value <> FCustomBaudRate then
  begin
    FCustomBaudRate := Value;
    ApplyDCB;
  end;
end;

// Set Data Bits (Serial Word Length)
procedure TComPortAbstract.SetDataBits(const Value: TDataBits);
begin
  if Value <> FDataBits then
  begin
    FDataBits := Value;
    ApplyDCB;
  end;
end;

// set parity
procedure TComPortAbstract.SetParity(const Value: TComParity);
begin
  FParity.Assign(Value);
  ApplyDCB;
end;



// set port
procedure TComPortAbstract.SetPort(const Value: TPort);
begin
  // 11.1.2001 Ch. Kaufmann; removed function ComString, because there can be com ports
  // with names other than COMn.
  if Value <> FPort then
  begin
    FPort := Value;
    if GetConnected and not ((csDesigning in ComponentState) or
      (csLoading in ComponentState)) then
    begin
      Close;
      Open;
    end;
  end;
end;

// set stop bits
procedure TComPortAbstract.SetStopBits(const Value: TStopBits);
begin
  if Value <> FStopBits then
  begin
    FStopBits := Value;
    ApplyDCB;
  end;
end;



// set timeouts
procedure TComPortAbstract.SetTimeouts(const Value: TComTimeouts);
begin
  FTimeouts.Assign(Value);
  ApplyTimeouts;
end;



end.

